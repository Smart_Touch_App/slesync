
#Configurations

# Use this utility program to generate "ApplicationDB's Install Scripts"
################################################################################################
$curPath="\\ibm-1\Virtual_Directories\SalesLogicExpress\Docs\SLESync\"
#set location of the folder to execute
Set-Location $curPath
# initialize the items variable with the contents of a directory - check the folder with the name "control" (control file) only filter file with .xml extension
################################################################################
$items = Get-Childitem -filter *ControlFile*PYU.1.3.0* -Recurse -include "*.xml"
################################################################################
#Set Schema Name
################
$schema="BUSDTA"
################

#should not be modified as this is dedicated only for ApplicaitonDB
$OrgPath="Source\PYU"

################################################################################################

# enumerate the items array
foreach ($item in $items)
{
    #get xml content
    [xml]$xml=get-content $item
    
    #Environment
    $Env=$xml.DBSchema.ReleaseEnvironment;
    
    #Set Target Folder
    $SaveFolder="{0}\Releases\{1}\{2}" -f $curPath, $Env, $xml.DBSchema.ModelRelease;
    #Write-Host $SaveFolder;

    #Remove ROOT directory if exists  -- to recreate
    if(Test-Path -Path $SaveFolder){
        Remove-Item -Path $SaveFolder -Force -Recurse #Delete contents of the directory without confirmation
    }
    
    #create ROOT Directory if doesn't exists  -- recreate
    #Write-Host $SaveFolder;
    if(!(Test-Path -Path $SaveFolder)){ 
        New-Item -Path $SaveFolder -ItemType directory
    }

    #Set Target Folder
    $SaveFolder="{0}\Releases\{1}\{2}\Install_Scripts" -f $curPath, $xml.DBSchema.ReleaseEnvironment, $xml.DBSchema.ModelRelease;
    #Write-Host $SaveFolder;
    
    #Save Script File
    $Script_log="{0}\Change_Track.log" -f $SaveFolder;
        
    #create ROOT Directory if doesn't exists  -- recreate
    #Write-Host $SaveFolder;
    if(!(Test-Path -Path $SaveFolder)){ 
        New-Item -Path $SaveFolder -ItemType directory
    }
    
    #Create Generation_Source directory
    $Generation_Source="{0}\Releases\{1}\{2}\Generation Source" -f $curPath, $xml.DBSchema.ReleaseEnvironment, $xml.DBSchema.ModelRelease;
    #Write-Host $ConsSetup;
    if(!(Test-Path -Path $Generation_Source)){ 
        New-Item -Path $Generation_Source -ItemType directory
    }
    
    #Create Cons_Setup directory
    #create new folder for ConSetup Scripts if doesn't exists
    $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "ConsDB_Setup";
    #Write-Host $ConsSetup;
    if(!(Test-Path -Path $ConsSetup)){ 
        New-Item -Path $ConsSetup -ItemType directory
    }
    
    $cdbConn="EXEC ml_add_lang_connection_script
	'{0}',
	'authenticate_user',
	'sql',
	null
GO

EXEC ml_add_lang_connection_script
	'{0}',
	'begin_connection',
	'sql',
    'SET NOCOUNT ON'
GO

EXEC ml_add_dnet_connection_script
    '{1}',
    'handle_DownloadData',
    'DRH.DownloadHandler.HandleDownload'
GO

EXEC ml_add_dnet_connection_script
    '{1}',
    'handle_UploadData',
    'DRH.UploadHandler.HandleUpload'
GO
" -f $xml.DBSchema.ScriptVersion, $xml.DBSchema.SysDBScriptVersion;
    
    #Set filename to save consolidate event script -- Adding Evnet 
    $CDBEvent_SaveFile="{0}\8.Consolidated_Sync_Scripts.sql" -f $ConsSetup;
    Add-Content $CDBEvent_SaveFile $cdbConn
    
    Write-Host "Creating Schema to create table table "
    $CDBSchema_SaveFile="{0}\2.Consolidate_Schema.sql" -f $ConsSetup;
    $cdbCreateSchema = " IF (SCHEMA_ID('{0}') IS NULL) 
BEGIN
    EXEC ('CREATE SCHEMA [{0}] AUTHORIZATION [dbo]')
END" -f $schema;
    Add-Content $CDBSchema_SaveFile $cdbCreateSchema
    
    #Iterating Table for Consolidated Script - Start
    foreach ($t in $xml.DBSchema.ConsolidatedSetup.Tables.Table) {
        #Consolidate Script Starts
        ###############################################################################
        #
        
        #Get consolidate schema file name - Current Version
        $CDBSchemafile="{2}\Tables\{0}\{0}_SchemaScripts\{0}_CDBSchema_{1}.sql" -f $t.Name, $t.Scripts.Schema.Consolidated.Version, $OrgPath;
        if(Test-Path $CDBSchemafile){
            #Create Generation_Source_Table directory
            $Generation_Source_Table="{0}\Tables" -f $Generation_Source;
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_Table)){ 
                New-Item -Path $Generation_Source_Table -ItemType directory
            }
            $Generation_Source_Table="{0}\{1}" -f $Generation_Source_Table, $t.Name;
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_Table)){ 
                New-Item -Path $Generation_Source_Table -ItemType directory
            }
            $Generation_Source_Table="{0}\{1}" -f $Generation_Source_Table, "SchemaScripts";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_Table)){ 
                New-Item -Path $Generation_Source_Table -ItemType directory
            }
            $cpyTagetFile="{0}\CDB.{1}_{2}.sql"-f $Generation_Source_Table, $t.Name, $t.Scripts.Schema.Consolidated.Version;
            Copy-Item -Path $CDBSchemafile -Destination $cpyTagetFile
            
            Write-Host "Writing CDB schema script for table : " $t.Name
            #Set filename to save consolidate table(schema) script
            $CDBSchema_SaveFile="{0}\2.Consolidate_Schema.sql" -f $ConsSetup;
            
            #Change Event Log - CDB Table
            if(($t.Scripts.Schema.Consolidated.LastVersion -ne $null) -And ($t.Scripts.Schema.Consolidated.LastVersion -ne $t.Scripts.Schema.Consolidated.Version))
            {
                #Set filename to save change script
                $CDBSchema_AlterFile="{0}\2.Consolidate_Schema_AlterScripts.sql" -f $ConsSetup;
                
                $logTxt = "CDB Table - {0}: {1}" -f $t.Name, "Schema Script Changed"
                Add-Content $Script_log $logTxt
                $cdbSchemaScriptChange = "/**** Table Schema script changed from last version ****/
--======================================================";
                Add-Content $CDBSchema_SaveFile $cdbSchemaScriptChange
                
                #Get consolidate schema file name - Previous Version
                $CDBSchemaLastVerfile="{2}\Tables\{0}\{0}_SchemaScripts\{0}_CDBSchema_{1}.sql" -f $t.Name, $t.Scripts.Schema.Consolidated.LastVersion, $OrgPath;
                if(Test-Path $CDBSchemaLastVerfile){
                    $cdbScript1 = "/*
-- DDL {0} - {1} --Start
-- *********************************************************" -f $t.Name, $t.Scripts.Schema.Consolidated.LastVersion;
                    Add-Content $CDBSchema_AlterFile $cdbScript1;
                
                    #Get Content of the file - Last/Previous version 
                    $cdbSchema = Get-Content -Path $CDBSchemafile
                    Add-Content $CDBSchema_AlterFile $cdbSchema
                    
                    $cdbScript1 = "*/
-- DDL {0} - {1} --End

/*
-- DDL {0} - {2} --Start
-- *********************************************************" -f $t.Name, $t.Scripts.Schema.Consolidated.LastVersion, $t.Scripts.Schema.Consolidated.Version;
                    Add-Content $CDBSchema_AlterFile $cdbScript1;

                    #Get Content of the file - Current version 
                    $cdbSchema = Get-Content -Path $CDBSchemafile
                    Add-Content $CDBSchema_AlterFile $cdbSchema
                    Add-Content $CDBSchema_SaveFile $cdbSchema
                    
                    $cdbScript1 = "*/
-- DDL {0} - {1} --End

/*
-- Change Schema/ Alter Scripts






*/
" -f $t.Name, $t.Scripts.Schema.Consolidated.Version;
                    Add-Content $CDBSchema_AlterFile $cdbScript1;
                }
                else {
                    $RespMsg="WARNING:   CDB Table '{0}'- last version schema script does not exist " -f $t.Name
                    Write-Host $RespMsg
                }

            }
            else {
                #Get Content of the file - version NOT changed
                $cdbSchema = Get-Content -Path $CDBSchemafile

                Add-Content $CDBSchema_SaveFile $cdbSchema
            }
        }
        else {
            $RespMsg="WARNING:   CDB Table '{0}'- schema script does not exist " -f $t.Name
            Write-Host $RespMsg
        }
        #Set filename to save consolidate event script
        $CDBEvent_SaveFile="{0}\8.Consolidated_Sync_Scripts.sql" -f $ConsSetup;
        #Write-Host "Events for" $t.Name
$tblVerEvent="
/* Table :{0} */
/* Version : {1} */
"-f $t.Name, $xml.DBSchema.ScriptVersion;

        foreach ($a in $t.Scripts.Events.Event){
            
            #Change Event Log - CDB Events
            if(($a.LastVersion -ne $null) -And ($a.LastVersion -ne $a.Version))
            {
                $logTxt = "CDB Table - {2}: Event - {0}: {1}" -f $a.Name, "Event Script Modified", $t.Name
                Add-Content $Script_log $logTxt
            }
        
        $buildEvent="
/* {0} - Start */
EXEC ml_add_lang_table_script
    '{2}',
	'{1}',
	'{0}',
	'sql',
" -f $a.Name, $t.Name, $xml.DBSchema.ScriptVersion;
            #Get consolidate event file name
            $CDBEventfile="{3}\Tables\{0}\{0}_Events\{0}_{1}_{2}.sql" -f $t.Name, $a.Name, $a.Version, $OrgPath;
            
            if(!(Test-Path $CDBEventfile)){ 
                $cdbEvents="--{ml_ignore}"
            }
            else{
                #Get Content of the file
                #$evt = Get-Content -Path $CDBEventfile
                $cdbEvents = Get-Content -Path $CDBEventfile
                #$cdbEvents=
#"'"+
#$evt+
#"'"
                
                #Copy Source File into Generation source file
                #Create Generation_Source_Table directory
                $Generation_Source_Schema="{0}\Tables" -f $Generation_Source;
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_Table)){ 
                    New-Item -Path $Generation_Source_Table -ItemType directory
                }
                $Generation_Source_Schema="{0}\{1}" -f $Generation_Source_Schema, $t.Name;
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_Schema)){ 
                    New-Item -Path $Generation_Source_Schema -ItemType directory
                }
                $Generation_Source_Schema="{0}\{1}" -f $Generation_Source_Schema, "Events";
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_Schema)){ 
                    New-Item -Path $Generation_Source_Schema -ItemType directory
                }
                $cpyTagetFile="{0}\CDB.{1}_{2}_{3}.sql"-f $Generation_Source_Schema, $t.Name, $a.Name, $a.Version;
                Copy-Item -Path $CDBEventfile -Destination $cpyTagetFile
            }
            #$cdbEvents=
            #$tblVerEvent+
            #$buildEvent+
            #$cdbEvents+"/* {0} - End */" -f $a.Name
            
            $endEvents="
/* {0} - End */" -f $a.Name
            Add-Content $CDBEvent_SaveFile $tblVerEvent;
            Add-Content $CDBEvent_SaveFile $buildEvent;
            Add-Content $CDBEvent_SaveFile "'";
            Add-Content $CDBEvent_SaveFile $cdbEvents;
            Add-Content $CDBEvent_SaveFile "'";
            Add-Content $CDBEvent_SaveFile $endEvents;

        $tblVerEvent=""; #Empty
        $msg_Evt = "Writing CDB events - '{1}' script for table : {0}" -f $t.Name, $a.Name
        Write-Host $msg_Evt
        }
        #
        ###############################################################################
        #Consolidate Script Ends
    }
    
    #############################
    $GroupNo=2;
    #Iterating Other Objects for Consolidated Script - Start
    foreach ($objGroup in ($xml.SelectNodes("DBSchema/ConsolidatedSetup/*") | Select-Object -Expand Name  | Where-Object { ($_ -ne "Connection") -and ($_ -ne "Tables") })) {
        #
        #Get consolidate script file name
        #
        $GroupNo=$GroupNo+1
        $objType=$objGroup.Substring(0,$objGroup.length-1);
        $node="DBSchema/ConsolidatedSetup/{0}/{1}" -f $objGroup, $objType;

        $objArray=$xml.SelectNodes($node)| Select-Object -Property Name, Version, LastVersion | ForEach-Object {
            $Name = $_.Name; $Version=$_.Version; $LastVersion=$_.LastVersion;
            if($objGroup -eq "Procedures"){$objGroup="StoredProcedures";}
         
            $CDBGroupfile="{3}\{2}\CDB.{0}_{1}.sql" -f $Name, $Version, $objGroup, $OrgPath;
            #Write-Host $CDBGroupfile;
            if(Test-Path $CDBGroupfile){
            
                #Create Generation_Source_obj directory
                $Generation_Source_obj="{0}\{1}" -f $Generation_Source, $objGroup;
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_obj)){ 
                    New-Item -Path $Generation_Source_obj -ItemType directory
                }
                $cpyTagetFile="{0}\CDB.{1}_{2}.sql"-f $Generation_Source_obj, $Name, $Version;
                Copy-Item -Path $CDBGroupfile -Destination $cpyTagetFile
                
                #Get Content of the file
                $cdbScript = Get-Content -Path $CDBGroupfile
                
                #Set filename to save change script
                $CDBSchema_AlterFile="{0}\{2}.Consolidated_{1}_AlterScripts.sql" -f $ConsSetup,  $objGroup, $GroupNo;
                
                #Change other Object Log - if Version Changed
                if(($LastVersion -ne $null) -And ($LastVersion -ne $Version))
                {
                    $CDBGroupLastVer="{3}\{2}\CDB.{0}_{1}.sql" -f $Name, $LastVersion, $objGroup, $OrgPath;
                    if(Test-Path $CDBGroupLastVer){
                        $objLastScript = Get-Content -Path $CDBGroupLastVer
                        $CDBSchema_AlterScr = "/*
-- {0} : {1}-{2} --Start
-- *********************************************************" -f $objGroup, $Name, $LastVersion;
                        Add-Content $CDBSchema_AlterFile $CDBSchema_AlterScr;
                        
                        Add-Content $CDBSchema_AlterFile $objLastScript;
                        $CDBSchema_AlterScr = "/*
-- {0} : {1}-{2} --End" -f $objGroup, $Name, $LastVersion;
                        Add-Content $CDBSchema_AlterFile $CDBSchema_AlterScr;
                        
                    }
                    
                    $CDBGroupCurVer="{3}\{2}\CDB.{0}_{1}.sql" -f $Name, $Version, $objGroup, $OrgPath;
                    if(Test-Path $CDBGroupCurVer){
                        $objCurScript = Get-Content -Path $CDBGroupCurVer
                        $CDBSchema_AlterScr = "/*
-- {0} : {1}-{2} --Start
-- *********************************************************" -f $objGroup, $Name, $Version;
                        Add-Content $CDBSchema_AlterFile $CDBSchema_AlterScr;
                        
                        Add-Content $CDBSchema_AlterFile $objCurScript;
                        $CDBSchema_AlterScr = "-- {0} : {1}-{2} --End
*/
/*
-- Alter Script





*/
" -f $objGroup, $Name, $Version;
                        Add-Content $CDBSchema_AlterFile $CDBSchema_AlterScr;
                        
                    }
                    
                    $logTxt = "CDB {2} - {0}: {1}" -f $Name, "Modified", $objGroup;
                    Add-Content $Script_log $logTxt;
                }
                
                #Create Cons_Setup directory
                #create new folder for ConSetup Scripts if doesn't exists
                $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "ConsDB_Setup";
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $ConsSetup)){ 
                    New-Item -Path $ConsSetup -ItemType directory
                }
                
                #Set filename to save consolidate event script
                $cdbScript_SaveFile="{0}\{2}.Consolidated_{1}.sql" -f $ConsSetup, $objGroup, $GroupNo;
                Add-Content $cdbScript_SaveFile $cdbScript;
                $RespMsg="Writing CDB {0} : {1} " -f $objGroup, $Name;
                Write-Host $RespMsg;
            }
            else {
                $RespMsg="WARNING:   {0} '{1}'- script does not exist" -f $objGroup, $Name;
                Write-Host $RespMsg;
            }
        }
        #
    }
    #Iterating Other Objects for Consolidated Script - End
    #############################
    
    
#Add Connection -Start
$rdbSchema = 
"IF NOT EXISTS (
	SELECT 1
	FROM SYS.SYSUSER
	WHERE user_name = 'BUSDTA'
) THEN
	GRANT CONNECT TO ""BUSDTA"";
END IF
GO
SET OPTION Public.DEFAULT_TIMESTAMP_INCREMENT=10000;
SET OPTION Public.TRUNCATE_TIMESTAMP_VALUES='ON';
GO
GRANT CREATE TABLE TO ""BUSDTA"";
GO
";
#Add Connection -End

    #Create Remote_Setup directory
    #create new folder for Remote Setup Scripts if doesn't exists
    $RemoteSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
    #Write-Host $RemoteSetup;
    if(!(Test-Path -Path $RemoteSetup)){ 
        New-Item -Path $RemoteSetup -ItemType directory
    }
    #Set filename to save remote table(schema) script
    $RDBSchema_SaveFile="{0}\2.0.Remote_Schema.sql" -f $RemoteSetup;
    Add-Content $RDBSchema_SaveFile $rdbSchema
        
    
    
    
    #Iterating Table for Remote Script - Start
    foreach ($t in $xml.DBSchema.RemoteSetup.Tables.Table) {
        #Remote Script Starts
        ###############################################################################
        #
        
        #Change Event Log - RDB Table
        if(($t.LastVersion -ne $null) -And ($t.LastVersion -ne $t.Version))
        {
            $logTxt = "RDB Table - {0}: {1}" -f $t.Name, "Schema Script Changed"
            Add-Content $Script_log $logTxt
        
            #Get remote schema Prev-Version file name
            $RDBSchemaPrevfile="{2}\Tables\{0}\{0}_SchemaScripts\{0}_RDBSchema_{1}.sql" -f $t.Name, $t.LastVersion, $OrgPath;
            if(Test-Path $RDBSchemaPrevfile){
                
                #Get Content of the file
                $rdbSchema = Get-Content -Path $RDBSchemaPrevfile
                
                #Create Remote_Setup directory
                #create new folder for Remote Setup Scripts if doesn't exists
                $RemoteSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
                #Write-Host $RemoteSetup;
                if(!(Test-Path -Path $RemoteSetup)){ 
                    New-Item -Path $RemoteSetup -ItemType directory
                }
                #Set filename to save remote table(schema) script
                $RDBSchema_PrevFile="{0}\2.0.Remote_Schema_AlterScript.sql" -f $RemoteSetup;
                $rdbSchemaScriptChange = "/*
/**** RDB DDL {0} ****/
--======================================================"-f $t.LastVersion;
                Add-Content $RDBSchema_PrevFile $rdbSchemaScriptChange
                Add-Content $RDBSchema_PrevFile $rdbSchema
                $rdbSchemaScriptChange = "*/";
                Add-Content $RDBSchema_PrevFile $rdbSchemaScriptChange
                
                Write-Host "Writing RDB Last Version schema script for table : " $t.Name
            }
            else {
                $RespMsg="WARNING:   Table '{0}'- RDB Last Version schema script does not exist " -f $t.Name
                Write-Host $RespMsg
            }
            #
            
            #Get remote schema Curr-Version file name
            $RDBSchemaPrevfile="{2}\Tables\{0}\{0}_SchemaScripts\{0}_RDBSchema_{1}.sql" -f $t.Name, $t.Version, $OrgPath;
            if(Test-Path $RDBSchemaPrevfile){
                           
                #Get Content of the file
                $rdbSchema = Get-Content -Path $RDBSchemaPrevfile
                
                #Create Remote_Setup directory
                #create new folder for Remote Setup Scripts if doesn't exists
                $RemoteSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
                #Write-Host $RemoteSetup;
                if(!(Test-Path -Path $RemoteSetup)){ 
                    New-Item -Path $RemoteSetup -ItemType directory
                }
                #Set filename to save remote table(schema) script
                $RDBSchema_PrevFile="{0}\2.0.Remote_Schema_AlterScript.sql" -f $RemoteSetup;
                $rdbSchemaScriptChange = "/*
/**** RDB DDL {0} ****/
--======================================================"-f $t.Version;
                Add-Content $RDBSchema_PrevFile $rdbSchemaScriptChange
                Add-Content $RDBSchema_PrevFile $rdbSchema
                $rdbSchemaScriptChange = "*/
/*
-- RDB Change Schema/ Alter Scripts





*/
";
                Add-Content $RDBSchema_PrevFile $rdbSchemaScriptChange
                
                
                #Set filename to save remote table(schema) script
                $RDBSchema_SaveFile="{0}\2.0.Remote_Schema.sql" -f $RemoteSetup;
                Add-Content $RDBSchema_SaveFile "
/**** Table Schema script changed from last version ****/
--======================================================
";
                Write-Host "Writing RDB Last Version schema script for table : " $t.Name
            }
            else {
                $RespMsg="WARNING:   Table '{0}'- RDB Last Version schema script does not exist " -f $t.Name
                Write-Host $RespMsg
            }
            #
        }
        #Get remote schema file name
        $RDBSchemafile="{2}\Tables\{0}\{0}_SchemaScripts\{0}_RDBSchema_{1}.sql" -f $t.Name, $t.Version, $OrgPath;
        if(Test-Path $RDBSchemafile){
        
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source, "Tables";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source_obj, $t.Name;
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source_obj, "SchemaScripts";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            $cpyTagetFile="{0}\RDB.{1}_{2}.sql"-f $Generation_Source_obj, $t.Name, $t.Version;
            Copy-Item -Path $RDBSchemafile -Destination $cpyTagetFile
            
            #Get Content of the file
            $rdbSchema = Get-Content -Path $RDBSchemafile
            
            #Create Remote_Setup directory
            #create new folder for Remote Setup Scripts if doesn't exists
            $RemoteSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
            #Write-Host $RemoteSetup;
            if(!(Test-Path -Path $RemoteSetup)){ 
                New-Item -Path $RemoteSetup -ItemType directory
            }
            #Set filename to save remote table(schema) script
            $RDBSchema_SaveFile="{0}\2.0.Remote_Schema.sql" -f $RemoteSetup;
            Add-Content $RDBSchema_SaveFile $rdbSchema
            
            Write-Host "Writing RDB schema script for table : " $t.Name
        }
        else {
            $RespMsg="WARNING:   Table '{0}'- RDB schema script does not exist " -f $t.Name
            Write-Host $RespMsg
        }
        #
        ###############################################################################
        #Remote Script Ends
    }
    #Iterating Table for Remote Script - End
    
    #Iterating Publication Info for Remote Script - Start
    foreach ($p in $xml.DBSchema.RemoteSetup.Publications.Publication) {
        $pubName=$p.PubName;
        $subsName=$p.Subscription.Name;
        $syncUser=$p.Synchronization.User; $syncProfile=$p.Synchronization.Profile;
        $verName=$p.Version;
        $syncProfileName=$p.Profile.Name;
        
        #Change Event Log - RDB Publication
        if(($p.LastVersion -ne $null) -And ($p.LastVersion -ne $p.Version))
        {
            $logTxt = "RDB Publication - {0}: {1}" -f $p.PubName, "Publication Altered"
            Add-Content $Script_log $logTxt
        }
        
        #check folder exist for Remote Setup Scripts, create if doesn't exists
        $RemoteSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
        if(!(Test-Path -Path $RemoteSetup)){ 
            New-Item -Path $RemoteSetup -ItemType directory
        }
        $RemTargetFile="{0}\7.Remote_{1}.sql" -f $RemoteSetup, $pubName;
        
        $RDBPubFile="{2}\Publications\{0}_{1}.sql" -f $pubName, $verName, $OrgPath;
        if(Test-Path $RDBPubFile){
        
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source, "Publication";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            $cpyTagetFile="{0}\RDB.{1}_{2}.sql"-f $Generation_Source_obj, $pubName, $verName;
            Copy-Item -Path $RDBPubFile -Destination $cpyTagetFile
            
            Copy-Item -Path $RDBPubFile -Destination $RemTargetFile
            $pubMsg="Remote publication {0} script copied into con setup directory" -f $pubName;
            Write-Host $pubMsg
        }
        else {
            $pubMsg="WARNING:   Remote publication {0} script not exist" -f $pubName;
            Write-Host $pubMsg
        }

$tcp=$xml.DBSchema.RemoteSetup.Publications.MobilinkServerDetails;

$pubContent=$pubContent+"
/** Create the user '{0}'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = '{0}') THEN
	CREATE SYNCHRONIZATION USER ""{0}"";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = '{1}') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION ""{1}"";
END IF
GO

/** Create subscription '{1}' to '{2}' for '{0}'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION ""{1}"" TO ""{2}"" FOR ""{0}"" 
TYPE tcpip ADDRESS 'host={5};port={6}'
	OPTION lt='{7}'
	SCRIPT VERSION '{3}'
GO
CREATE OR REPLACE SYNCHRONIZATION PROFILE ""{4}""
'Subscription={1}';


" -f $syncUser, $subsName, $pubName, $xml.DBSchema.ScriptVersion, $syncProfileName, $tcp.TCPIP.host, $tcp.TCPIP.port, $tcp.OPTION.lt;
        Add-Content $RemTargetFile $pubContent;
        
        Write-Host "Writing RDB sync script for publication : " $pubName
    $pubContent="";
    
        #Sync Script 
        $RDBName=$xml.DBSchema.RemoteSetup.Database.Name;
        $user=$xml.DBSchema.RemoteSetup.Database.UserName;
        $pass=$xml.DBSchema.RemoteSetup.Database.Password;
        $SyncScript="dbmlsync -c ""SERVER={0};UID={1};PWD={2}"" -s ""{3}"" -o rem1.dbs -v+" -f $RDBName, $user, $pass, $subsName;
        #Sync Script Batch File
        $SyncTargetFile="{0}\8.Sync_{1}.bat" -f $RemoteSetup, $pubName;
        Add-Content $SyncTargetFile $SyncScript
    }
    #Iterating Publication Info for Remote Script - End
    ## End Remote Publication Iterate
        
    #Copy DRH Library to target folder in consolidated folder
    $drhfile="{0}\{2}\{1}" -f $curPath, "DRH_Libraries", $OrgPath;
    #Write-Host $drhfile;
    if(Test-Path -Path $drhfile){
        $drhFileName="{0}\SyncHandlerLib.dll" -f $drhfile;
        if(Test-Path $drhFileName){
            $tarPath="{0}\drh_dll" -f $ConsSetup;
            if(!(Test-Path -Path $tarPath)){ 
                New-Item -Path $tarPath -ItemType directory
            }
            $tarPath="{0}\drh_dll\SyncHandlerLib.dll" -f $ConsSetup;
            Copy-Item -Path $drhFileName -Destination $tarPath
            Write-Host "DRH Setup DLL copied into con setup directory";
        }
        else {
            Write-Host "WARNING:   DRH Setup file not Copied into target folder " 
        }
    }
    else {
        Write-Host "WARNING:   DRH Setup file not exist " 
    }

    #Copy mobilink setup script to target folder for consolidated
    $mlfile="{0}\{2}\{1}" -f $curPath, "ML_Setup", $OrgPath;
    #Write-Host $mlfile;
    if(Test-Path -Path $mlfile){
        $mlFileName="{0}\5.syncmss.sql" -f $mlfile;
        if(Test-Path $mlFileName){
            $tarPath="{0}\7.syncmss.sql" -f $ConsSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "MLSetup file copied into con setup directory";
        }
        else {
            Write-Host "WARNING:   ML Setup file not Copied into target folder " 
        }
    }
    else {
        Write-Host "WARNING:   ML Setup file not exist " 
    }
    
    #Copy connection script to target folder for consolidated
    $CDBconnVer=$xml.DBSchema.ConsolidatedSetup.Connection.Version;
    $mlfile="{0}\{2}\{1}" -f $curPath, "ML_Setup", $OrgPath;
    #Write-Host $mlfile;
    if(Test-Path -Path $mlfile){
        $mlFileName="{0}\1.Consolidated_Connection_Info_{1}.bat" -f $mlfile, $CDBconnVer;
        if(Test-Path $mlFileName){
            $tarPath="{0}\1.Consolidated_Setup.bat" -f $ConsSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "consolidated connection script copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     consolidate connection script not exist" 
        }
        
        $mlFileName="{0}\Consolidated_Connection_Vars.bat" -f $mlfile;
        if(Test-Path $mlFileName){
            $tarPath="{0}\Consolidated_Connection_Vars.bat" -f $ConsSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "consolidated connection variables copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     consolidate connection variables not exist" 
        }
    }
    else {
        Write-Host "WARNING:     consolidate connection script folder not exist"
    }
    
    #Copy connection script to target folder for Remote
    $RDBconnVer=$xml.DBSchema.RemoteSetup.Connection.Version;
    $mlfile="{0}\{2}\{1}" -f $curPath, "ML_Setup", $OrgPath;
    #Write-Host $mlfile;
    if(Test-Path -Path $mlfile) {
        $mlFileName="{0}\1.0.Start_ML_Server_{1}.bat" -f $mlfile, $RDBconnVer;
        if(Test-Path $mlFileName){
            $CDBtarPath="{0}\9.Start_ML_Server.bat" -f $ConsSetup;
            Copy-Item -Path $mlFileName -Destination $CDBtarPath
            Write-Host "Remote connection script copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     Remote connection script not exist" 
        }
        $mlFileName="{0}\Remote_Connection_Vars.bat" -f $mlfile, $RDBconnVer;
        if(Test-Path $mlFileName){
            $CDBtarPath="{0}\Remote_Connection_Vars.bat" -f $ConsSetup;
            Copy-Item -Path $mlFileName -Destination $CDBtarPath
            Write-Host "Remote connection Variables copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     Remote connection variables not exist" 
        }
        
        $mlFileName="{0}\1.1.Remote_Connection_Info_{1}.bat" -f $mlfile, $RDBconnVer;
        if(Test-Path $mlFileName){
            $tarPath="{0}\1.1.Remote_Setup.bat" -f $RemoteSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "Remote connection script copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     Remote connection script not exist" 
        }
        
        $mlFileName="{0}\Remote_Connection_Vars.bat" -f $mlfile, $RDBconnVer;
        if(Test-Path $mlFileName){
            $tarPath="{0}\Remote_Connection_Vars.bat" -f $RemoteSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "Remote connection variables copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     Remote connection variables not exist" 
        }
        
        $mlFileName="{0}\Create_Remote_Database.bat" -f $mlfile, $RDBconnVer;
        if(Test-Path $mlFileName){
            $tarPath="{0}\Create_Remote_Database.bat" -f $RemoteSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "Remote create database script copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     create database script not exist" 
        }
        $mlFileName="{0}\Start_Remote_Database.bat" -f $mlfile, $RDBconnVer;
        if(Test-Path $mlFileName){
            $tarPath="{0}\Start_Remote_Database.bat" -f $RemoteSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "Remote start database script copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     start database script not exist" 
        }
        $mlFileName="{0}\Stop_Remote_Database.bat" -f $mlfile, $RDBconnVer;
        if(Test-Path $mlFileName){
            $tarPath="{0}\Stop_Remote_Database.bat" -f $RemoteSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "Remote stop database script copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     stop database script not exist" 
        }
    }
    else {
        Write-Host "WARNING:     Remote connection script not exist"
    }
    
    #Iterating Stored Procedure for Remote Script - Start
    foreach ($proc in $xml.DBSchema.RemoteSetup.Procedures.Procedure) {
        #
        
        #Change Event Log - RDB Procedure
        if(($proc.LastVersion -ne $null) -And ($proc.LastVersion -ne $proc.Version))
        {
            $logTxt = "RDB Procedure - {0}: {1}" -f $proc.Name, "Modified"
            Add-Content $Script_log $logTxt
        }
        
        #Get Remote SP script file name
        $RDBSPfile="{2}\StoredProcedures\RDB.{0}_{1}.sql" -f $proc.Name, $proc.Version, $OrgPath;
        if(Test-Path $RDBSPfile){
        
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source, "StoredProcedures";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            $cpyTagetFile="{0}\RDB.{1}_{2}.sql"-f $Generation_Source_obj, $proc.Name, $proc.Version;
            Copy-Item -Path $RDBSPfile -Destination $cpyTagetFile
            
            #Get Content of the file
            $rdbSP = Get-Content -Path $RDBSPfile
            
            #Create Rem_Setup directory
            #create new folder for RemSetup Scripts if doesn't exists
            $RemSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
            #Write-Host $RemSetup;
            if(!(Test-Path -Path $RemSetup)){ 
                New-Item -Path $RemSetup -ItemType directory
            }
            
            #Set filename to save consolidate event script
            $RDBSP_SaveFile="{0}\5.Remote_Stored_Procedures.sql" -f $RemSetup;
            Add-Content $RDBSP_SaveFile $rdbSP
            
            Write-Host "Writing RDB script for SP : " $proc.Name
        }
        else {
            $RespMsg="WARNING:   RDB Procedure '{0}'- script does not exist" -f $proc.Name
            Write-Host $RespMsg
        }
        #
    }
    #Iterating Stored Procedure for Remote Script - End

    #Iterating Function for Remote Script - Start
    foreach ($fn in $xml.DBSchema.RemoteSetup.Functions.Function) {
        #
        
        #Change Event Log - RDB Functions
        if(($fn.LastVersion -ne $null) -And ($fn.LastVersion -ne $fn.Version))
        {
            $logTxt = "RDB Functions - {0}: {1}" -f $fn.Name, "Modified"
            Add-Content $Script_log $logTxt
        }
        
        #Get Remote Function script file name
        $RDBFNfile="{2}\Functions\RDB.{0}_{1}.sql" -f $fn.Name, $fn.Version, $OrgPath;
        if(Test-Path $RDBFNfile){
        
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source, "Functions";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            $cpyTagetFile="{0}\RDB.{1}_{2}.sql"-f $Generation_Source_obj, $fn.Name, $fn.Version;
            Copy-Item -Path $RDBFNfile -Destination $cpyTagetFile
            
            #Get Content of the file
            $rdbFN = Get-Content -Path $RDBFNfile
            
            #Create Rem_Setup directory
            #create new folder for RemSetup Scripts if doesn't exists
            $RemSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
            #Write-Host $RemSetup;
            if(!(Test-Path -Path $RemSetup)){ 
                New-Item -Path $RemSetup -ItemType directory
            }
            
            #Set filename to save remote function script
            $RDBFN_SaveFile="{0}\4.Remote_Functions.sql" -f $RemSetup;
            Add-Content $RDBFN_SaveFile $rdbFN
            
            Write-Host "Writing RDB script for Function : " $fn.Name
        }
        else {
            $RespMsg="WARNING:   RDB Function '{0}'- script does not exist" -f $fn.Name
            Write-Host $RespMsg
        }
        #
    }
    #Iterating Function for Remote Script - End

    
    #Iterationg SystemTables for Remote Script - Start
    $OrgPath="Source\SystemDB"
    Write-Host "Writing Script For SystemDB" $OrgPath

    foreach ($t in $xml.DBSchema.RemoteSetup.SystemTables.Table) {
        #Remote Script Starts
        ###############################################################################
        #
        
        $remote_table_name=$t.Name.split(".")[1]; #Write-Host $table_name
        $remote_table_schema=$t.Name.split(".")[0]; #Write-Host $table_schema
        
        if(!$remote_table_name) {
            $remote_table_name=$remote_table_schema;
        }
        Write-Host "SystemDB Table" $remote_table_name
        #Get remote schema file name
        $RDBSchemafile="{2}\Tables\{0}\{0}_SchemaScripts\{0}_RDBSchema_{1}.sql" -f $remote_table_name, $t.Version, $OrgPath;
        if(Test-Path $RDBSchemafile){
        
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source, "Tables";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source_obj, $t.Name;
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            #Create Generation_Source_obj directory
            $Generation_Source_obj="{0}\{1}" -f $Generation_Source_obj, "SchemaScripts";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_obj)){ 
                New-Item -Path $Generation_Source_obj -ItemType directory
            }
            $cpyTagetFile="{0}\RDB.{1}_{2}.sql"-f $Generation_Source_obj, $t.Name, $t.Version;
            Copy-Item -Path $RDBSchemafile -Destination $cpyTagetFile
            
            #Get Content of the file
            $rdbSchema = Get-Content -Path $RDBSchemafile
            
            #Create Remote_Setup directory
            #create new folder for Remote Setup Scripts if doesn't exists
            $RemoteSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "RemoteDB_Setup";
            #Write-Host $RemoteSetup;
            if(!(Test-Path -Path $RemoteSetup)){ 
                New-Item -Path $RemoteSetup -ItemType directory
            }
            #Set filename to save remote table(schema) script
            $RDBSchema_SaveFile="{0}\2.1.Remote_SystemDB_Schema.sql" -f $RemoteSetup;
            Add-Content $RDBSchema_SaveFile $rdbSchema
            
            Write-Host "Writing RDB schema script for table : " $t.Name
        }
        else {
            $RespMsg="WARNING:   Table '{0}'- RDB schema script does not exist " -f $t.Name
            Write-Host $RespMsg
        }
        #
        ###############################################################################
        #Remote Script Ends
    }
    #Iterationg SystemTables for Remote Script - End

    #Create Cons_Setup directory
    #create new folder for ConSetup Scripts if doesn't exists
    $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "ConsDB_Setup";
    #Write-Host $ConsSetup;
    if(!(Test-Path -Path $ConsSetup)){ 
        New-Item -Path $ConsSetup -ItemType directory
    }
    
}

