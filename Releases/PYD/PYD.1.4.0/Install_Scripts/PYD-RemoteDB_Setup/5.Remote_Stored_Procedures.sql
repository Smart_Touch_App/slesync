CREATE OR REPLACE PROCEDURE "BUSDTA"."calculateEnergySurcharge" (CustomerNum varchar(10), OrderTotal numeric(10,4))
BEGIN
	DECLARE  FreightHandlingCode varchar(10);
	DECLARE  SpecialHandlingCode varchar(10);
	DECLARE  DescriptionCode varchar(10);
	DECLARE  HandlingCodeLength INTEGER ;

	Declare str_drsy varchar(10);
	Declare str_drrt varchar(10);
	--Declare UPto varchar(10);
  DECLARE upto numeric(10,4);   DECLARE surcharge numeric(10,4);
  DECLARE FinalSurcharge numeric(10,4); DECLARE PrevUpto numeric(10,4); DECLARE PrevSurcharge numeric(10,4);
	
 --DECLARE qry LONG VARCHAR;
 
     DECLARE cur_surcharge CURSOR FOR
        select  DRDL01,  DRDL02  from busdta.F0005 where DRSY = str_drsy and DRRT = str_drrt;
	set FinalSurcharge = 0;

    Select ltrim(rtrim(AIFRTH)) into FreightHandlingCode 
           from busdta.F03012 where aian8 = CustomerNum;
	IF FreightHandlingCode = '' or FreightHandlingCode is null THEN
		select 0 from dummy;
	END IF;

	select  ltrim(rtrim(DRDL02)), ltrim(rtrim(DRSPHD)) into DescriptionCode, SpecialHandlingCode 
            from busdta.F0005 where DRSY = '42' and DRRT = 'FR' and DRKY like '%'+FreightHandlingCode+'';
	-- Handle for Static Code 
	IF DescriptionCode = 'MB|STATIC' THEN
		set FinalSurcharge =  (select SpecialHandlingCode from dummy);
    -- Logic for UDC values
	ELSEIF DescriptionCode = 'MB|UDC' THEN
		set str_drsy = substring(SpecialHandlingCode, 1, charindex('|',SpecialHandlingCode)-1);
		set str_drrt = substring(SpecialHandlingCode,  charindex('|',SpecialHandlingCode)+1);
        set PrevUpto = 0; set FinalSurcharge = 0;

          set PrevSurcharge = (select CONVERT(numeric(10,4), max(DRDL02)) from busdta.F0005 where DRSY = str_drsy and DRRT = str_drrt);

          OPEN cur_surcharge;
          lp: LOOP
            FETCH NEXT cur_surcharge INTO upto, surcharge;
            IF SQLCODE <> 0 THEN LEAVE lp END IF;
            IF (OrderTotal > PrevUpto and OrderTotal <= upto) OR OrderTotal = 0  THEN
                --PRINT 'Surcharge '+CONVERT( nvarchar, PrevSurcharge );
                set FinalSurcharge = PrevSurcharge;
                LEAVE lp;
            END IF;
            set PrevUpto = upto;
            set Prevsurcharge = surcharge;
          END LOOP;
          CLOSE cur_surcharge;
        IF FinalSurcharge = 0 THEN
            set FinalSurcharge = (select CONVERT(numeric(10,4), min(DRDL02)) from busdta.F0005 where DRSY = str_drsy and DRRT = str_drrt);
        END IF;
    -- No surcharge if description is 0.
	ELSEIF DescriptionCode like '' THEN
		set FinalSurcharge =  0;
    -- This is additional check 
	ELSE
		if OrderTotal <= 100 then
		  select 45 from dummy
		elseif OrderTotal > 100 and OrderTotal <= 250 then
		  select 30 from dummy
		else
		  select 0 from dummy
		end if
	END IF;
select FinalSurcharge from dummy;
END
GO
CREATE OR REPLACE PROCEDURE "BUSDTA"."SaveMasterInvoice"( @InvoiceNo varchar(10),@OrderQty integer ,@UM varchar(2) ,@ItemCode varchar(10) ,@ProductDesc varchar(30),
@UnitPrice float,@ExtendedPrice float,@OrderDate varchar(50), @SalesCat1 varchar(3)
 )
/* RESULT( column_name column_type, ... ) */
BEGIN
	insert into BUSDTA.InvoiceDetails(
InvoiceNo,
OrderQty,
UM,
ItemCode,
ProductDesc,
UnitPrice,
ExtendedPrice,
OrderDate,
SalesCat1
)values(
@InvoiceNo,
@OrderQty,
@UM,
@ItemCode,
@ProductDesc,
@UnitPrice,
@ExtendedPrice,
@OrderDate,
@SalesCat1
);
RETURN  1;
END
GO
CREATE OR REPLACE PROCEDURE "BUSDTA"."getPreOrderDatesForCustomer" (In CustomerNum numeric(8,0), In SelectedDate date,In BaseDate date, In Years int default 2) 
BEGIN
	DECLARE  WeekNumber numeric(2,0); -- Week Number for SelectedDate
	DECLARE  DayNumber numeric(2,0); -- Day Number for SelectedDate
    DECLARE DeliveryDayCode nvarchar(3);
    DECLARE i INTEGER = 1;
    DECLARE loopUpTo INTEGER = (Years * 366);
    DECLARE nextDate date;
    /*This cursor gets the pattern, for a Delivery Day Code of the customer.*/
    DECLARE cur_DayCodePattern CURSOR FOR
      SELECT  DCWN, DCDN
      FROM BUSDTA.M56M0002 
      WHERE DCDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerNum);

    DECLARE  Week numeric(4,0); -- Week Number for Delivery Day Code
    DECLARE  Day numeric(4,0); -- Day Number for Delivery Day Code
    DECLARE  WeeksPerCycle integer;

    DECLARE LOCAL TEMPORARY TABLE TempTab ( val as nchar(50) ); -- Temporary table to store the next dates
    
    Select AISTOP into DeliveryDayCode from busdta.F03012 where AIAN8 = CustomerNum;

    select DMWPC into WeeksPerCycle from busdta.M56M0001 where DMDDC = DeliveryDayCode;

    /*Outer loop: Generates the Calendar dates from the Base date. These dates will be compared to the Pattern dates.*/    
    lp: LOOP
        IF i = loopUpTo THEN LEAVE lp END IF;
        select dateadd(day, i, BaseDate) into nextDate from dummy;

        /*Get the Period, Week and Day number for the given date, with respect to BaseDate*/
        select busdta.GetWeekForDate(nextDate, BaseDate, WeeksPerCycle) into WeekNumber from dummy;
        select busdta.GetDayForDate(nextDate, BaseDate) into DayNumber from dummy ;
    
        /*Inner Loop: This loop generates the dates for pattern of Delivery Day Code, to be compared with the calendar dates.*/
        OPEN cur_DayCodePattern;    
        ilp: LOOP
            FETCH next cur_DayCodePattern into Week, Day;
                IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                    /*If Calendar date mathes the Pattern dates, it is a qualified date.*/
                    IF Week = WeekNumber AND Day = DayNumber THEN
                        /*If qualified date is greater than than the selected date, then it is the next valid date.*/
                        IF SelectedDate < nextDate THEN
                            insert into busdta.TempTab values(nextDate);
                        END IF;
                    END IF;
        END LOOP; -- End Inner Loop    
        CLOSE cur_DayCodePattern;

        SET i = i + 1;
    END LOOP; -- End Outer Loop
    select val from TempTab; -- All valid next dates.
END
GO

/*------------------------------------------------------------------------------
*             Procedure to pull history records for given customer. 
*	This procedure will show the history for orders created using SLE Application.
*-----------------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE "BUSDTA"."prepareHistoryRecords"( @shipTo varchar(25) )
begin
  declare "i" nvarchar(2);
  declare "j" nvarchar(2);
  declare "countHist" integer;
  declare "strQuery" nvarchar(20000);
  drop table if exists "busdta"."localLastOrders";
 
select "NUMBER"() as "chronology",'00800', OrderID, OrderTypeId, OrderDate into "busdta"."localLastOrders" from BUSDTA.Order_Header
where CustShipToId = @shipTo and
OrderTypeId = (select StatusTypeId from busdta.Status_Type where StatusTypeCD = 'SALORD') and
OrderStateId = (select StatusTypeId from busdta.Status_Type where StatusTypeCd = 'CMTD')
order by OrderDate desc;
   
  /* Order Item Last */
  drop table if exists "busdta"."localLastItems";
   select "NUMBER"() as "ranking","ItemId" into "busdta"."localLastItems"
    from "busdta"."localLastOrders" llo join "busdta"."Order_Detail" od on llo.OrderID = od.OrderID
    where "chronology" <= 10  and ISNULL(od.OrderQty,0)<>0 group by "ItemId" order by "Count"() desc,"ItemId" asc;
  /* Order History Panel Data */
  drop table if exists "busdta"."localOrderHistoryPanel";
  select "count"() into "countHist" from "busdta"."localLastOrders";
  if "countHist" <> null or "countHist" <> '' then
    // To limit the procedure to generate only 10 history records. The value will of countHist will also affect the size of 'strQuery'
    if "countHist" > 10 then
      set "countHist" = 10
    end if;
    set "strQuery" = 'select CAST(im.imlitm AS VARCHAR(20)) as ItemCode,im.imdsc1 AS ItemDesc
 , ';
    set "i" = 1;
    while "i" <= "countHist" loop
      set "strQuery" = "strQuery"+'h'+"i"+'d.OrderQty as H'+"i"+'_Qty';
      if "i" <> "countHist" then
        set "strQuery" = "strQuery"+' , '
      end if;
      set "i" = "i"+1
    end loop;
    set "strQuery" = "strQuery"+' into busdta.localOrderHistoryPanel from busdta.localLastItems li ';
    set "strQuery" = "strQuery"+'join busdta.F4101 im on li.ItemId=im.IMITM  ';
    set "j" = 1;
    while "j" <= "countHist" loop
   set strQuery = strQuery+' join busdta.localLastOrders h'+j+' on h'+j+'.chronology='+j ;
   set strQuery = strQuery+' left join busdta.Order_Detail h'+j+'d on h'+j+'.OrderID=h'+j+'d.OrderID and im.imitm=h'+j+'d.ItemId ';
      set "j" = "j"+1
    end loop;
    set "strQuery" = convert(varchar,"strQuery" || ' Order By ranking;')
  else
     set strQuery = 'drop table if exists "busdta"."localOrderHistoryPanel";create table busdta.localOrderHistoryPanel  (ItemCode varchar(20), ItemDesc nvarchar, H1_Qty numeric(8,0));';
  end if;
  execute immediate "strQuery";
end

GO

/*--------------------------------------------------------------------------------------------
*             This procedure pre-populates table M56M0004 tables, which is table for daily stops.
*			  This procedure executes the very first time when daily stop is hit and then generates 
*			  data for pre-determined future dates.
*--------------------------------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE "BUSDTA"."generateDailyStopData" (In SelectedDate date,In BaseDate date, In FutureDays int default 500) 
BEGIN
    DECLARE rowCount INTEGER;
    DECLARE i INTEGER;
    
	select count(1) into rowCount from BUSDTA.M56M0004 where  RPSTDT = SelectedDate;
	--Check if future stops present
	--Else if Data present for few days
	--No data present at all
	SET i = 1;
    IF rowCount = 0 THEN
    	WHILE i <= FutureDays LOOP
     		insert into busdta.M56M0004(RPROUT, RPAN8, RPSTDT, RPSN, RPVTTP, RPSTTP,RPCRBY,RPCRDT,RPUPBY,RPUPDT)
    		(select r.RSROUT, r.RSAN8, cast(SelectedDate as date), r.RSSN, 'Cust', 'Planned','SYS',GETDATE(),'SYS',GETDATE()  
    		from BUSDTA.F03012 c join BUSDTA.M56M0003 r on c.aian8 = r.RSAN8 join BUSDTA.M56M0002 d on d.DCDDC = c.AISTOP 
			join busdta.Customer_Route_Map crm on  c.aian8 = crm.CustomerShipToNumber 
    		where   
    		R.RSWN = BUSDTA.GetWeekForDate(SelectedDate, BaseDate) and 
    		d.DCDN = BUSDTA.GetDayForDate(SelectedDate, BaseDate) and 
			crm.IsActive = 'Y' and crm.RelationshipType = 'AN85');
    		set SelectedDate = dateadd(day, 1, SelectedDate);
    		SET i = i + 1;
    	END LOOP;   
        /*Set all past stop dates as NOSALE*/
        update busdta.M56M0004
        set RPACTID = 'NOSALE', RPRCID = 7, RPCACT = 1, RPPACT = 0
        WHERE RPSTDT < cast(getdate() as date);
    END IF;
END

GO
