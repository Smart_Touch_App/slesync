
/* [BUSDTA].[Customer_Route_Map] - begins */

/* TableDDL - [BUSDTA].[Customer_Route_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Route_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Route_Map]
	(
	  [BranchNumber] NCHAR(12) NOT NULL
	, [CustomerShipToNumber] NUMERIC(8,0) NOT NULL
	, [RelationshipType] NCHAR(10) NOT NULL
	, [RelatedAddressBookNumber] NUMERIC(8,0) NULL
	, [IsActive] NCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_CUSTOMER_ROUTE_MAP_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_CUSTOMER_ROUTE_MAP_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_CUSTOMER_ROUTE_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Customer_Route_Map] PRIMARY KEY ([BranchNumber] ASC, [CustomerShipToNumber] ASC, [RelationshipType] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Customer_Route_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Customer_Route_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Route_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Route_Map_del]
	(
	  [BranchNumber] NCHAR(12)
	, [CustomerShipToNumber] NUMERIC(8,0)
	, [RelationshipType] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([BranchNumber] ASC, [CustomerShipToNumber] ASC, [RelationshipType] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Customer_Route_Map] - End */
/* TRIGGERS FOR Customer_Route_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Customer_Route_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Customer_Route_Map_ins
	ON BUSDTA.Customer_Route_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Customer_Route_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Customer_Route_Map_del.BranchNumber= inserted.BranchNumber AND BUSDTA.Customer_Route_Map_del.CustomerShipToNumber= inserted.CustomerShipToNumber AND BUSDTA.Customer_Route_Map_del.RelationshipType= inserted.RelationshipType
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Customer_Route_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Route_Map_upd
	ON BUSDTA.Customer_Route_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Customer_Route_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Customer_Route_Map.BranchNumber= inserted.BranchNumber AND BUSDTA.Customer_Route_Map.CustomerShipToNumber= inserted.CustomerShipToNumber AND BUSDTA.Customer_Route_Map.RelationshipType= inserted.RelationshipType');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Customer_Route_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Route_Map_dlt
	ON BUSDTA.Customer_Route_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Customer_Route_Map_del (BranchNumber, CustomerShipToNumber, RelationshipType, last_modified )
	SELECT deleted.BranchNumber, deleted.CustomerShipToNumber, deleted.RelationshipType, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Customer_Route_Map - END */

