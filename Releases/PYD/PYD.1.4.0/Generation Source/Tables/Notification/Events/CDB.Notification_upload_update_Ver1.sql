 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Notification"
SET "Title" = {ml r."Title"}, "Description" = {ml r."Description"}, "TypeID" = {ml r."TypeID"}, "Category" = {ml r."Category"}, "SubCategory" = {ml r."SubCategory"}, "Read" = {ml r."Read"}, "ValidTill" = {ml r."ValidTill"}, "Acknowledged" = {ml r."Acknowledged"}, "Initiator" = {ml r."Initiator"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "NotificationID" = {ml r."NotificationID"} AND "RouteID" = {ml r."RouteID"}
