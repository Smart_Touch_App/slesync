/* [BUSDTA].[Amount_Range_Index] - begins */

/* TableDDL - [BUSDTA].[Amount_Range_Index] - Start */
IF OBJECT_ID('[BUSDTA].[Amount_Range_Index]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Amount_Range_Index]
	(
	  [RequestCode] NVARCHAR (1) NOT NULL
	, [AuthorizationFormatCode] NVARCHAR (1) NOT NULL
	, [RangeStart] DECIMAL(15,4) NOT NULL
	, [RangeEnd] DECIMAL(15,4) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC)
	)
END
/* TableDDL - [BUSDTA].[Amount_Range_Index] - End */
