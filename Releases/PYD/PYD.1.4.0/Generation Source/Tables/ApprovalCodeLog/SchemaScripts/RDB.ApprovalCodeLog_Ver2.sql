/* [BUSDTA].[ApprovalCodeLog] - begins */

/* TableDDL - [BUSDTA].[ApprovalCodeLog] - Start */
IF OBJECT_ID('[BUSDTA].[ApprovalCodeLog]') IS NULL
BEGIN
CREATE TABLE "BUSDTA"."ApprovalCodeLog" (
    "LogID"                          DECIMAL(15,0) NOT NULL DEFAULT autoincrement
   ,"RouteID"                        numeric(8,0) NOT NULL
   ,"ApprovalCode"                   nvarchar(8) NOT NULL
   ,"RequestCode"                    nvarchar(8) NOT NULL
   ,"Feature"                        nvarchar(50) NOT NULL
   ,"Amount"                         DECIMAL(15,4) NOT NULL
   ,"RequestedByUser"                numeric(8,0) NOT NULL
   ,"RequestedForCustomer"           numeric(8,0) NOT NULL
   ,"Payload"                        long nvarchar NULL
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("LogID" ASC,"RouteID" ASC) 
)    
END
/* TableDDL - [BUSDTA].[ApprovalCodeLog] - End */
