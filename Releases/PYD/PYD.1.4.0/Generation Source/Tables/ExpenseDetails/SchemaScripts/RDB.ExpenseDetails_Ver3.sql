
/* [BUSDTA].[ExpenseDetails] - begins */

/* TableDDL - [BUSDTA].[ExpenseDetails] - Start */
IF OBJECT_ID('[BUSDTA].[ExpenseDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ExpenseDetails]
	(
	  [ExpenseId] DECIMAL(15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionId] DECIMAL(15,0) NULL
	, [CategoryId] NUMERIC(8,0) NOT NULL
	, [ExpensesExplanation] NVARCHAR (100) NULL
	, [ExpenseAmount] DECIMAL(15,0) NULL
	, [StatusId] NUMERIC(8,0) NULL
	, [ExpensesDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC(8,0) NULL
	, [RouteSettlementId] DECIMAL(15,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([ExpenseId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[ExpenseDetails] - End */

