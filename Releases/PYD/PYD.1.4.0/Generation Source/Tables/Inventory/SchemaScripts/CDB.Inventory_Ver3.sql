
/* [BUSDTA].[Inventory] - begins */

/* TableDDL - [BUSDTA].[Inventory] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory]
	(
	  [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OnHandQuantity] DECIMAL(15,0) NULL
	, [CommittedQuantity] DECIMAL(15,0) NULL
	, [HeldQuantity] DECIMAL(15,0) NULL
	, [ParLevel] DECIMAL(15,0) NULL
	, [LastReceiptDate] DATETIME NULL
	, [LastConsumeDate] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_INVENTORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Inventory] PRIMARY KEY ([ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Inventory] - End */

/* SHADOW TABLE FOR [BUSDTA].[Inventory] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_del]
	(
	  [ItemId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Inventory] - End */
/* TRIGGERS FOR Inventory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Inventory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Inventory_ins
	ON BUSDTA.Inventory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Inventory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Inventory_del.ItemId= inserted.ItemId AND BUSDTA.Inventory_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Inventory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_upd
	ON BUSDTA.Inventory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Inventory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Inventory.ItemId= inserted.ItemId AND BUSDTA.Inventory.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Inventory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_dlt
	ON BUSDTA.Inventory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Inventory_del (ItemId, RouteId, last_modified )
	SELECT deleted.ItemId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Inventory - END */
