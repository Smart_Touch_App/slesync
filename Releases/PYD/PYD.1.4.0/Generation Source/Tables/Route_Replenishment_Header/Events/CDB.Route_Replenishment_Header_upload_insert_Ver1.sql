 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Replenishment_Header"
("ReplenishmentID", "RouteId", "StatusId", "ReplenishmentTypeId", "TransDateFrom", "TransDateTo", "RequestedBy", "FromBranchId", "ToBranchId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ReplenishmentID"}, {ml r."RouteId"}, {ml r."StatusId"}, {ml r."ReplenishmentTypeId"}, {ml r."TransDateFrom"}, {ml r."TransDateTo"}, {ml r."RequestedBy"}, {ml r."FromBranchId"}, {ml r."ToBranchId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
