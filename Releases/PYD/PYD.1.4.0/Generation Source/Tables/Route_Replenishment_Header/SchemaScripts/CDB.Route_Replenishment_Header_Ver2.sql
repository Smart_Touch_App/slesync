/* [BUSDTA].[Route_Replenishment_Header] - begins */

/* TableDDL - [BUSDTA].[Route_Replenishment_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Header]
	(
	  [ReplenishmentID] decimal (15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(8,0) NOT NULL
	, [ReplenishmentTypeId] NUMERIC(8,0) NOT NULL
	, [TransDateFrom] DATETIME NULL
	, [TransDateTo] DATETIME NULL
	, [RequestedBy] NUMERIC(8,0) NULL
	, [FromBranchId] NUMERIC(8,0) NULL
	, [ToBranchId] decimal (15,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_REPLENISHMENT_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Replenishment_Header] PRIMARY KEY ([ReplenishmentID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Replenishment_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Header_del]
	(
	  [ReplenishmentID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ReplenishmentID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Header] - End */
/* TRIGGERS FOR Route_Replenishment_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Replenishment_Header_ins
	ON BUSDTA.Route_Replenishment_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Replenishment_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Replenishment_Header_del.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Header_upd
	ON BUSDTA.Route_Replenishment_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Replenishment_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Replenishment_Header.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Header_dlt
	ON BUSDTA.Route_Replenishment_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Replenishment_Header_del (ReplenishmentID, RouteId, last_modified )
	SELECT deleted.ReplenishmentID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Replenishment_Header - END */
