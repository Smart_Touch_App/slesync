/* [BUSDTA].[tblCoffeeProspect] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeProspect]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeProspect]
	(
	  [CoffeeProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, [CoffeeBlendID] INT NULL
	, [CompetitorID] INT NULL
	, [UOMID] INT NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] decimal (15,4) NULL
	, [UsageMeasurementID] INT NULL
	, [LiqCoffeeTypeID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEPROSPECT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeProspect] PRIMARY KEY ([CoffeeProspectID] ASC,[ProspectID] ASC)
	)



END
GO
/* TableDDL - [BUSDTA].[tblCoffeeProspect] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeProspect_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeProspect_del]
	(
	  [CoffeeProspectID] INT NOT NULL
	,[ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeProspect] - End */
/* TRIGGERS FOR tblCoffeeProspect - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeProspect_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCoffeeProspect_ins
	ON BUSDTA.tblCoffeeProspect AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCoffeeProspect_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCoffeeProspect_del.CoffeeProspectID= inserted.CoffeeProspectID AND  BUSDTA.tblCoffeeProspect_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCoffeeProspect_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeProspect_upd
	ON BUSDTA.tblCoffeeProspect AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCoffeeProspect
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCoffeeProspect.CoffeeProspectID= inserted.CoffeeProspectID AND BUSDTA.tblCoffeeProspect.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeProspect_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeProspect_dlt
	ON BUSDTA.tblCoffeeProspect AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCoffeeProspect_del (CoffeeProspectID,ProspectID, last_modified )
	SELECT deleted.CoffeeProspectID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeProspect - END */
