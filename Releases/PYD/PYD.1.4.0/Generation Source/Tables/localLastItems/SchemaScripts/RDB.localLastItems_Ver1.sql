IF OBJECT_ID('[BUSDTA].[localLastItems]') IS NULL
BEGIN

CREATE TABLE BUSDTA.localLastItems 
(
    ranking                        integer NOT NULL
   ,ItemId                         numeric(8,0) NOT NULL
)
END
