 

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_PriceAdj"
SET "AdjName" = {ml r."AdjName"}, "AdjDesc" = {ml r."AdjDesc"}, "FactorValue" = {ml r."FactorValue"}, "UnitPrice" = {ml r."UnitPrice"}, "OP" = {ml r."OP"}, "BC" = {ml r."BC"}, "BasicDesc" = {ml r."BasicDesc"}, "PriceAdjustmentKeyID" = {ml r."PriceAdjustmentKeyID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"} AND "AdjustmentSeq" = {ml r."AdjustmentSeq"}
