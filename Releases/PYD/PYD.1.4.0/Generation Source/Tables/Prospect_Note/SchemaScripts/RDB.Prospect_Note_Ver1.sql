/* [BUSDTA].[Prospect_Note] - begins */

/* TableDDL - [BUSDTA].[Prospect_Note] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Note]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Note]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [ProspectNoteId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ProspectNoteType] NUMERIC(3,0) NOT NULL
	, [ProspectNoteDetail] NVARCHAR (1000) NULL
	, [IsDefault] NVARCHAR (1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ProspectId] ASC, [ProspectNoteId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Prospect_Note] - End */
