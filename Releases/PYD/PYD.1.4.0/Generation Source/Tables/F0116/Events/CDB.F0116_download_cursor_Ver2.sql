
     SELECT "BUSDTA"."F0116"."ALAN8"  ,"BUSDTA"."F0116"."ALEFTB"  ,"BUSDTA"."F0116"."ALEFTF"  ,"BUSDTA"."F0116"."ALADD1"  ,"BUSDTA"."F0116"."ALADD2"  ,
     "BUSDTA"."F0116"."ALADD3"  ,"BUSDTA"."F0116"."ALADD4"  ,"BUSDTA"."F0116"."ALADDZ"  ,"BUSDTA"."F0116"."ALCTY1"  ,"BUSDTA"."F0116"."ALCOUN"  ,
     "BUSDTA"."F0116"."ALADDS"    
     FROM "BUSDTA"."F0116", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."F0116"."last_modified" >= {ml s.last_table_download} or rm."last_modified" >= {ml s.last_table_download}  
      or crm."last_modified" >= {ml s.last_table_download})   AND ALAN8=crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber  
      AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     