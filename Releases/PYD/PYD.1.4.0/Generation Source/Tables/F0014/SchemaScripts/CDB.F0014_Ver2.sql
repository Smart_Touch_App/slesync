
/* [BUSDTA].[F0014] - begins */

/* TableDDL - [BUSDTA].[F0014] - Start */
IF OBJECT_ID('[BUSDTA].[F0014]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0014]
	(
	  [PNPTC] NCHAR(3) NOT NULL
	, [PNPTD] NCHAR(30) NULL
	, [PNDCP] FLOAT NULL
	, [PNDCD] FLOAT NULL
	, [PNNDTP] FLOAT NULL
	, [PNNSP] FLOAT NULL
	, [PNDTPA] FLOAT NULL
	, [PNPXDM] FLOAT NULL
	, [PNPXDD] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0014_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0014] PRIMARY KEY ([PNPTC] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0014] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0014] - Start */
IF OBJECT_ID('[BUSDTA].[F0014_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0014_del]
	(
	  [PNPTC] NCHAR(3)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PNPTC] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0014] - End */
/* TRIGGERS FOR F0014 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0014_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0014_ins
	ON BUSDTA.F0014 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0014_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0014_del.PNPTC= inserted.PNPTC
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0014_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0014_upd
	ON BUSDTA.F0014 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0014
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0014.PNPTC= inserted.PNPTC');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0014_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0014_dlt
	ON BUSDTA.F0014 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0014_del (PNPTC, last_modified )
	SELECT deleted.PNPTC, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0014 - END */

