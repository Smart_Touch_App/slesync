/* [BUSDTA].[Status_Type] - begins */

/* TableDDL - [BUSDTA].[Status_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Status_Type]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Status_Type]
	(
	  [StatusTypeID] NUMERIC(8,0) NOT NULL
	, [StatusTypeCD] VARCHAR(10) NOT NULL
	, [StatusTypeDESC] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_STATUS_TYPE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_STATUS_TYPE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_STATUS_TYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Status_Type] PRIMARY KEY ([StatusTypeCD] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Status_Type] - End */

/* SHADOW TABLE FOR [BUSDTA].[Status_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Status_Type_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Status_Type_del]
	(
	  [StatusTypeCD] VARCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([StatusTypeCD] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Status_Type] - End */
/* TRIGGERS FOR Status_Type - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Status_Type_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Status_Type_ins
	ON BUSDTA.Status_Type AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Status_Type_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Status_Type_del.StatusTypeCD= inserted.StatusTypeCD
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Status_Type_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Status_Type_upd
	ON BUSDTA.Status_Type AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Status_Type
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Status_Type.StatusTypeCD= inserted.StatusTypeCD');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Status_Type_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Status_Type_dlt
	ON BUSDTA.Status_Type AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Status_Type_del (StatusTypeCD, last_modified )
	SELECT deleted.StatusTypeCD, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Status_Type - END */
