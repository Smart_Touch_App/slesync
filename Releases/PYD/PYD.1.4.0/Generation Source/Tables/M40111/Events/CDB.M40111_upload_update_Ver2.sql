
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M40111"
SET "PCAR1" = {ml r."PCAR1"}, "PCPH1" = {ml r."PCPH1"}, "PCEXTN1" = {ml r."PCEXTN1"}, "PCPHTP1" = {ml r."PCPHTP1"}, "PCDFLTPH1" = {ml r."PCDFLTPH1"}, "PCREFPH1" = {ml r."PCREFPH1"}, "PCAR2" = {ml r."PCAR2"}, "PCPH2" = {ml r."PCPH2"}, "PCEXTN2" = {ml r."PCEXTN2"}, "PCPHTP2" = {ml r."PCPHTP2"}, "PCDFLTPH2" = {ml r."PCDFLTPH2"}, "PCREFPH2" = {ml r."PCREFPH2"}, "PCAR3" = {ml r."PCAR3"}, "PCPH3" = {ml r."PCPH3"}, "PCEXTN3" = {ml r."PCEXTN3"}, "PCPHTP3" = {ml r."PCPHTP3"}, "PCDFLTPH3" = {ml r."PCDFLTPH3"}, "PCREFPH3" = {ml r."PCREFPH3"}, "PCAR4" = {ml r."PCAR4"}, "PCPH4" = {ml r."PCPH4"}, "PCEXTN4" = {ml r."PCEXTN4"}, "PCPHTP4" = {ml r."PCPHTP4"}, "PCDFLTPH4" = {ml r."PCDFLTPH4"}, "PCREFPH4" = {ml r."PCREFPH4"}, "PCEMAL1" = {ml r."PCEMAL1"}, "PCETP1" = {ml r."PCETP1"}, "PCDFLTEM1" = {ml r."PCDFLTEM1"}, "PCREFEM1" = {ml r."PCREFEM1"}, "PCEMAL2" = {ml r."PCEMAL2"}, "PCETP2" = {ml r."PCETP2"}, "PCDFLTEM2" = {ml r."PCDFLTEM2"}, "PCREFEM2" = {ml r."PCREFEM2"}, "PCEMAL3" = {ml r."PCEMAL3"}, "PCETP3" = {ml r."PCETP3"}, "PCDFLTEM3" = {ml r."PCDFLTEM3"}, "PCREFEM3" = {ml r."PCREFEM3"}, "PCGNNM" = {ml r."PCGNNM"}, "PCMDNM" = {ml r."PCMDNM"}, "PCSRNM" = {ml r."PCSRNM"}, "PCTITL" = {ml r."PCTITL"}, "PCACTV" = {ml r."PCACTV"}, "PCDFLT" = {ml r."PCDFLT"}, "PCSET" = {ml r."PCSET"}, "PCCRBY" = {ml r."PCCRBY"}, "PCCRDT" = {ml r."PCCRDT"}, "PCUPBY" = {ml r."PCUPBY"}, "PCUPDT" = {ml r."PCUPDT"}
WHERE "PCAN8" = {ml r."PCAN8"} AND "PCIDLN" = {ml r."PCIDLN"}
 

