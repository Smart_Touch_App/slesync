
/* [BUSDTA].[Route_User_Map] - begins */

/* TableDDL - [BUSDTA].[Route_User_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_User_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_User_Map]
	(
	  [App_user_id] INT NOT NULL
	, [Route_Id] VARCHAR(8) NOT NULL
	, [Active] INT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_USER_MAP_last_modified DEFAULT(getdate())
	, [Default_Route] INT NULL
	, CONSTRAINT [PK_Route_User_Map] PRIMARY KEY ([App_user_id] ASC, [Route_Id] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_User_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_User_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_User_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_User_Map_del]
	(
	  [App_user_id] INT
	, [Route_Id] VARCHAR(8)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([App_user_id] ASC, [Route_Id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_User_Map] - End */
/* TRIGGERS FOR Route_User_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_User_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_User_Map_ins
	ON BUSDTA.Route_User_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_User_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_User_Map_del.App_user_id= inserted.App_user_id AND BUSDTA.Route_User_Map_del.Route_Id= inserted.Route_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_User_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_User_Map_upd
	ON BUSDTA.Route_User_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_User_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_User_Map.App_user_id= inserted.App_user_id AND BUSDTA.Route_User_Map.Route_Id= inserted.Route_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_User_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_User_Map_dlt
	ON BUSDTA.Route_User_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_User_Map_del (App_user_id, Route_Id, last_modified )
	SELECT deleted.App_user_id, deleted.Route_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_User_Map - END */
