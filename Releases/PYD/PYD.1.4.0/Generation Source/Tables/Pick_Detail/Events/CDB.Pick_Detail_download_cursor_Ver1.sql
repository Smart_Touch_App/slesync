

SELECT "BUSDTA"."Pick_Detail"."PickDetailID"
,"BUSDTA"."Pick_Detail"."RouteId"
,"BUSDTA"."Pick_Detail"."TransactionID"
,"BUSDTA"."Pick_Detail"."TransactionDetailID"
,"BUSDTA"."Pick_Detail"."TransactionTypeId"
,"BUSDTA"."Pick_Detail"."TransactionStatusId"
,"BUSDTA"."Pick_Detail"."ItemID"
,"BUSDTA"."Pick_Detail"."TransactionQty"
,"BUSDTA"."Pick_Detail"."TransactionUOM"
,"BUSDTA"."Pick_Detail"."TransactionQtyPrimaryUOM"
,"BUSDTA"."Pick_Detail"."PrimaryUOM"
,"BUSDTA"."Pick_Detail"."PickedQty"
,"BUSDTA"."Pick_Detail"."PickQtyPrimaryUOM"
,"BUSDTA"."Pick_Detail"."LastScanMode"
,"BUSDTA"."Pick_Detail"."ItemScanSeq"
,"BUSDTA"."Pick_Detail"."IsOnHold"
,"BUSDTA"."Pick_Detail"."PickAdjusted"
,"BUSDTA"."Pick_Detail"."AdjustedQty"
,"BUSDTA"."Pick_Detail"."ReasonCodeId"
,"BUSDTA"."Pick_Detail"."ManuallyPickCount"
,"BUSDTA"."Pick_Detail"."IsInException"
,"BUSDTA"."Pick_Detail"."ExceptionReasonCodeId"
,"BUSDTA"."Pick_Detail"."CreatedBy"
,"BUSDTA"."Pick_Detail"."CreatedDatetime"
,"BUSDTA"."Pick_Detail"."UpdatedBy"
,"BUSDTA"."Pick_Detail"."UpdatedDatetime"

FROM "BUSDTA"."Pick_Detail"
WHERE "BUSDTA"."Pick_Detail"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Pick_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 