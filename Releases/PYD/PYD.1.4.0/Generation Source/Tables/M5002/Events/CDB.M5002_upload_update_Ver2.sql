
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M5002"
SET "TNID" = {ml r."TNID"}, "TNTYP" = {ml r."TNTYP"}, "TNACTN" = {ml r."TNACTN"}, "TNADSC" = {ml r."TNADSC"}, "TNACTR" = {ml r."TNACTR"}, "TNILDGRD" = {ml r."TNILDGRD"}, "TNCRBY" = {ml r."TNCRBY"}, "TNCRDT" = {ml r."TNCRDT"}, "TNUPBY" = {ml r."TNUPBY"}, "TNUPDT" = {ml r."TNUPDT"}
WHERE "TNKEY" = {ml r."TNKEY"}
 

