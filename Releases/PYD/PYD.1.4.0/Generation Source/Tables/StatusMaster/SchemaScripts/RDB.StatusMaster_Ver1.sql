
/* TableDDL - [BUSDTA].[StatusMaster] - Start */

IF OBJECT_ID('[BUSDTA].[StatusMaster]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[StatusMaster]
(	  
  [StatusId] INTEGER NOT NULL DEFAULT AUTOINCREMENT
, [StatusCode] VARCHAR(5) NOT NULL
, [StatusDescription] NVARCHAR(100) NULL
, [StatusForType] NVARCHAR(50) NULL	
, [CreatedBy] NUMERIC(8,0) NULL	
, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())	
, [UpdatedBy] NUMERIC(8,0) NULL	
, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())	
, PRIMARY KEY ([StatusId] ASC)
	
)
END

/* TableDDL - [BUSDTA].[StatusMaster] - End */
