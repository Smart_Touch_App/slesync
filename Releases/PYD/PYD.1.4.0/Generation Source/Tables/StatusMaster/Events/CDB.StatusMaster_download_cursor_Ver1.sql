

SELECT "BUSDTA"."StatusMaster"."StatusId"
,"BUSDTA"."StatusMaster"."StatusCode"
,"BUSDTA"."StatusMaster"."StatusDescription"
,"BUSDTA"."StatusMaster"."StatusForType"
,"BUSDTA"."StatusMaster"."CreatedBy"
,"BUSDTA"."StatusMaster"."CreatedDatetime"
,"BUSDTA"."StatusMaster"."UpdatedBy"
,"BUSDTA"."StatusMaster"."UpdatedDatetime"
FROM "BUSDTA"."StatusMaster"
WHERE "BUSDTA"."StatusMaster"."last_modified">= {ml s.last_table_download}