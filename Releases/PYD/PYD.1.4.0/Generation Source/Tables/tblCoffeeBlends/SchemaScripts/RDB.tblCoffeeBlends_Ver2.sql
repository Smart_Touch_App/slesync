/* [BUSDTA].[tblCoffeeBlends] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeBlends]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeBlends]
	(
	  [CoffeeBlendID] INTEGER NOT NULL 
	, [CoffeeBlend] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CoffeeBlendID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblCoffeeBlends] - End */
