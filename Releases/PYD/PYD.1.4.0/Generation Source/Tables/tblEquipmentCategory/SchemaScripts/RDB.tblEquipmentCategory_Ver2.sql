
/* [BUSDTA].[tblEquipmentCategory] - begins */

/* TableDDL - [BUSDTA].[tblEquipmentCategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentCategory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentCategory]
	(
	  [EquipmentCategoryID] INTEGER NOT NULL 
	, [EquipmentCategory] VARCHAR(500) NULL
	, [EquipmentTypeID] INTEGER NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([EquipmentCategoryID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblEquipmentCategory] - End */
