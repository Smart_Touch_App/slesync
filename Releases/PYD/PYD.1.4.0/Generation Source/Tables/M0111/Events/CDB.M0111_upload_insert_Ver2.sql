
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M0111"
("CDAN8", "CDID", "CDIDLN", "CDRCK7", "CDCNLN", "CDAR1", "CDPH1", "CDEXTN1", "CDPHTP1", "CDDFLTPH1", "CDREFPH1", "CDAR2", "CDPH2", "CDEXTN2", "CDPHTP2", "CDDFLTPH2", "CDREFPH2", "CDAR3", "CDPH3", "CDEXTN3", "CDPHTP3", "CDDFLTPH3", "CDREFPH3", "CDAR4", "CDPH4", "CDEXTN4", "CDPHTP4", "CDDFLTPH4", "CDREFPH4", "CDEMAL1", "CDETP1", "CDDFLTEM1", "CDREFEM1", "CDEMAL2", "CDETP2", "CDDFLTEM2", "CDREFEM2", "CDEMAL3", "CDETP3", "CDDFLTEM3", "CDREFEM3", "CDGNNM", "CDMDNM", "CDSRNM", "CDTITL", "CDACTV", "CDDFLT", "CDSET", "CDCRBY", "CDCRDT", "CDUPBY", "CDUPDT")
VALUES({ml r."CDAN8"}, {ml r."CDID"}, {ml r."CDIDLN"}, {ml r."CDRCK7"}, {ml r."CDCNLN"}, {ml r."CDAR1"}, {ml r."CDPH1"}, {ml r."CDEXTN1"}, {ml r."CDPHTP1"}, {ml r."CDDFLTPH1"}, {ml r."CDREFPH1"}, {ml r."CDAR2"}, {ml r."CDPH2"}, {ml r."CDEXTN2"}, {ml r."CDPHTP2"}, {ml r."CDDFLTPH2"}, {ml r."CDREFPH2"}, {ml r."CDAR3"}, {ml r."CDPH3"}, {ml r."CDEXTN3"}, {ml r."CDPHTP3"}, {ml r."CDDFLTPH3"}, {ml r."CDREFPH3"}, {ml r."CDAR4"}, {ml r."CDPH4"}, {ml r."CDEXTN4"}, {ml r."CDPHTP4"}, {ml r."CDDFLTPH4"}, {ml r."CDREFPH4"}, {ml r."CDEMAL1"}, {ml r."CDETP1"}, {ml r."CDDFLTEM1"}, {ml r."CDREFEM1"}, {ml r."CDEMAL2"}, {ml r."CDETP2"}, {ml r."CDDFLTEM2"}, {ml r."CDREFEM2"}, {ml r."CDEMAL3"}, {ml r."CDETP3"}, {ml r."CDDFLTEM3"}, {ml r."CDREFEM3"}, {ml r."CDGNNM"}, {ml r."CDMDNM"}, {ml r."CDSRNM"}, {ml r."CDTITL"}, {ml r."CDACTV"}, {ml r."CDDFLT"}, {ml r."CDSET"}, {ml r."CDCRBY"}, {ml r."CDCRDT"}, {ml r."CDUPBY"}, {ml r."CDUPDT"})
 

