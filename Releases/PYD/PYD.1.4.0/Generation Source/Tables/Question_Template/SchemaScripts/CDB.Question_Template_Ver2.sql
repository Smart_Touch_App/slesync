/* [BUSDTA].[Question_Template] - begins */

/* TableDDL - [BUSDTA].[Question_Template] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Template]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Template]
	(
	  [TemplateId] NUMERIC(8,0) NOT NULL
	, [TemplateName] NCHAR(100) NULL
	, [QuestionId] NUMERIC(8,0) NOT NULL
	, [ResponseID] NUMERIC(8,0) NOT NULL
	, [RevisionId] NUMERIC(2,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_QUESTION_TEMPLATE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Question_Template] PRIMARY KEY ([TemplateId] ASC, [QuestionId] ASC, [ResponseID] ASC, [RevisionId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Question_Template] - End */

/* SHADOW TABLE FOR [BUSDTA].[Question_Template] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Template_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Template_del]
	(
	  [TemplateId] NUMERIC(8,0)
	, [QuestionId] NUMERIC(8,0)
	, [ResponseID] NUMERIC(8,0)
	, [RevisionId] NUMERIC(2,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TemplateId] ASC, [QuestionId] ASC, [ResponseID] ASC, [RevisionId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Question_Template] - End */
/* TRIGGERS FOR Question_Template - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Question_Template_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Question_Template_ins
	ON BUSDTA.Question_Template AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Question_Template_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Question_Template_del.QuestionId= inserted.QuestionId AND BUSDTA.Question_Template_del.ResponseID= inserted.ResponseID AND BUSDTA.Question_Template_del.RevisionId= inserted.RevisionId AND BUSDTA.Question_Template_del.TemplateId= inserted.TemplateId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Question_Template_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Template_upd
	ON BUSDTA.Question_Template AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Question_Template
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Question_Template.QuestionId= inserted.QuestionId AND BUSDTA.Question_Template.ResponseID= inserted.ResponseID AND BUSDTA.Question_Template.RevisionId= inserted.RevisionId AND BUSDTA.Question_Template.TemplateId= inserted.TemplateId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Question_Template_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Template_dlt
	ON BUSDTA.Question_Template AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Question_Template_del (QuestionId, ResponseID, RevisionId, TemplateId, last_modified )
	SELECT deleted.QuestionId, deleted.ResponseID, deleted.RevisionId, deleted.TemplateId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Question_Template - END */
