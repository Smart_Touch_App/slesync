/* [BUSDTA].[Entity_Number_Status] - begins */

/* TableDDL - [BUSDTA].[Entity_Number_Status] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Number_Status]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Number_Status]
	(
	  [EntityNumberStatusID] DECIMAL(8,0) NOT NULL IDENTITY(1,1)
	, [RouteId] DECIMAL(8,0) NULL
	, [Bucket] VARCHAR(30) NULL
	, [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [RangeStart] DECIMAL(8,0) NULL
	, [RangeEnd] DECIMAL(8,0) NULL
	, [CurrentAllocated] DECIMAL(8,0) NULL
	, [IsConsumed] BIT NULL
	, [ConsumedOnDate] SMALLDATETIME NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ENTITY_NUMBER_STATUS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Entity_Number_Status] PRIMARY KEY ([EntityNumberStatusID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Entity_Number_Status] - End */

/* SHADOW TABLE FOR [BUSDTA].[Entity_Number_Status] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Number_Status_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Number_Status_del]
	(
	  [EntityNumberStatusID] DECIMAL(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EntityNumberStatusID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Entity_Number_Status] - End */
/* TRIGGERS FOR Entity_Number_Status - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Entity_Number_Status_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Entity_Number_Status_ins
	ON BUSDTA.Entity_Number_Status AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Entity_Number_Status_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Entity_Number_Status_del.EntityNumberStatusID= inserted.EntityNumberStatusID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Entity_Number_Status_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Number_Status_upd
	ON BUSDTA.Entity_Number_Status AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Entity_Number_Status
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Entity_Number_Status.EntityNumberStatusID= inserted.EntityNumberStatusID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Entity_Number_Status_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Number_Status_dlt
	ON BUSDTA.Entity_Number_Status AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Entity_Number_Status_del (EntityNumberStatusID, last_modified )
	SELECT deleted.EntityNumberStatusID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Entity_Number_Status - END */
