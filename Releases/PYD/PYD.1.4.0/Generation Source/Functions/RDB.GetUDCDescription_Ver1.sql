CREATE OR REPLACE FUNCTION "BUSDTA"."GetUDCDescription" (@DRSY varchar(3), @DRRT varchar(3), @DRKY varchar(3))
RETURNS varchar(30)
AS BEGIN
	DECLARE @DRDL01 varchar(30)

	select  @DRDL01 = DRDL01  from BUSDTA.F0005 where  DRSY = @DRSY and DRRT = @DRRT and ltrim(DRKY) = @DRKY

	RETURN @DRDL01
END
/*------------------------------------------------------------------------------*/
GO