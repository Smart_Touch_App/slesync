
/* [BUSDTA].[M0140] - begins */

/* TableDDL - [BUSDTA].[M0140] - Start */
IF OBJECT_ID('[BUSDTA].[M0140]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0140]
	(
	  [RMMCU] NCHAR(12) NOT NULL
	, [RMAN8] NUMERIC(8,0) NOT NULL
	, [RMDTAI] NCHAR(10) NOT NULL
	, [RMCAN8] NUMERIC(8,0) NULL
	, [RMYN] NCHAR(2) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M0140_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M0140] PRIMARY KEY ([RMMCU] ASC, [RMAN8] ASC, [RMDTAI] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M0140] - End */

/* SHADOW TABLE FOR [BUSDTA].[M0140] - Start */
IF OBJECT_ID('[BUSDTA].[M0140_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0140_del]
	(
	  [RMMCU] NCHAR(12)
	, [RMAN8] NUMERIC(8,0)
	, [RMDTAI] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RMMCU] ASC, [RMAN8] ASC, [RMDTAI] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M0140] - End */
/* TRIGGERS FOR M0140 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M0140_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M0140_ins
	ON BUSDTA.M0140 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M0140_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M0140_del.RMAN8= inserted.RMAN8 AND BUSDTA.M0140_del.RMDTAI= inserted.RMDTAI AND BUSDTA.M0140_del.RMMCU= inserted.RMMCU
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M0140_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0140_upd
	ON BUSDTA.M0140 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M0140
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M0140.RMAN8= inserted.RMAN8 AND BUSDTA.M0140.RMDTAI= inserted.RMDTAI AND BUSDTA.M0140.RMMCU= inserted.RMMCU');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M0140_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0140_dlt
	ON BUSDTA.M0140 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M0140_del (RMAN8, RMDTAI, RMMCU, last_modified )
	SELECT deleted.RMAN8, deleted.RMDTAI, deleted.RMMCU, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M0140 - END */

