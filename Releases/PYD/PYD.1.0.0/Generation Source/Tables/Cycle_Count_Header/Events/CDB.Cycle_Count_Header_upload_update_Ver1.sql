
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Cycle_Count_Header"
SET "StatusId" = {ml r."StatusId"}, "IsCountInitiated" = {ml r."IsCountInitiated"}, "CycleCountTypeId" = {ml r."CycleCountTypeId"}, "InitiatorId" = {ml r."InitiatorId"}, "CycleCountDatetime" = {ml r."CycleCountDatetime"}, "ReasonCodeId" = {ml r."ReasonCodeId"}, "JDEQty" = {ml r."JDEQty"}, "JDEUM" = {ml r."JDEUM"}, "JDECycleCountNum" = {ml r."JDECycleCountNum"}, "JDECycleCountDocType" = {ml r."JDECycleCountDocType"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CycleCountID" = {ml r."CycleCountID"} AND "RouteId" = {ml r."RouteId"}
 

