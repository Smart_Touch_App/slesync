/* [dbo].[tblGroupTables] - begins */

/* TableDDL - [dbo].[tblGroupTables] - Start */
IF OBJECT_ID('[dbo].[tblGroupTables]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGroupTables]
	(
	  [GroupTableID] INT NOT NULL IDENTITY(1,1)
	, [RefDBSchema] VARCHAR(128) NULL
	, [RefDBTable] VARCHAR(128) NULL
	, [ApplicationGroupID] INT NULL
	, [ConDBTable] VARCHAR(128) NULL
	, [IsMandatorySync] BIT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLGROUPTABLES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGroupTables] PRIMARY KEY ([GroupTableID] ASC)
	)

ALTER TABLE [dbo].[tblGroupTables] WITH CHECK ADD CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID] FOREIGN KEY([ApplicationGroupID]) REFERENCES [dbo].[tblApplicationGroup] ([ApplicationGroupID])
ALTER TABLE [dbo].[tblGroupTables] CHECK CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID]

END
/* TableDDL - [dbo].[tblGroupTables] - End */

/* SHADOW TABLE FOR [dbo].[tblGroupTables] - Start */
IF OBJECT_ID('[dbo].[tblGroupTables_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGroupTables_del]
	(
	  [GroupTableID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([GroupTableID] ASC)
	)

END
/* SHADOW TABLE FOR [dbo].[tblGroupTables] - End */
/* TRIGGERS FOR tblGroupTables - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblGroupTables_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblGroupTables_ins
	ON dbo.tblGroupTables AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblGroupTables_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblGroupTables_del.GroupTableID= inserted.GroupTableID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblGroupTables_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblGroupTables_upd
	ON dbo.tblGroupTables AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblGroupTables
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblGroupTables.GroupTableID= inserted.GroupTableID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblGroupTables_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblGroupTables_dlt
	ON dbo.tblGroupTables AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblGroupTables_del (GroupTableID, last_modified )
	SELECT deleted.GroupTableID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblGroupTables - END */
