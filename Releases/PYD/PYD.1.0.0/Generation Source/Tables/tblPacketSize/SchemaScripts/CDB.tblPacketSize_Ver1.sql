/* [dbo].[tblPacketSize] - begins */

/* TableDDL - [dbo].[tblPacketSize] - Start */
IF OBJECT_ID('[dbo].[tblPacketSize]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblPacketSize]
	(
	  [PacketSizeID] INT NOT NULL IDENTITY(1,1)
	, [Size] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblPacketSize] PRIMARY KEY ([PacketSizeID] ASC)
	)

END
/* TableDDL - [dbo].[tblPacketSize] - End */
GO
/* SHADOW TABLE FOR [dbo].[tblPacketSize] - Start */
IF OBJECT_ID('[dbo].[tblPacketSize_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblPacketSize_del]
	(
	  [PacketSizeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PacketSizeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblPacketSize] - End */
/* TRIGGERS FOR tblPacketSize - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblPacketSize_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblPacketSize_ins
	ON dbo.tblPacketSize AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblPacketSize_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblPacketSize_del.PacketSizeID= inserted.PacketSizeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblPacketSize_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblPacketSize_upd
	ON dbo.tblPacketSize AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblPacketSize
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblPacketSize.PacketSizeID= inserted.PacketSizeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblPacketSize_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblPacketSize_dlt
	ON dbo.tblPacketSize AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblPacketSize_del (PacketSizeID, last_modified )
	SELECT deleted.PacketSizeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblPacketSize - END */
