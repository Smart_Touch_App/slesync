
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Inventory_Adjustment"
SET "ItemNumber" = {ml r."ItemNumber"}, "TransactionQty" = {ml r."TransactionQty"}, "TransactionQtyUM" = {ml r."TransactionQtyUM"}, "TransactionQtyPrimaryUM" = {ml r."TransactionQtyPrimaryUM"}, "ReasonCode" = {ml r."ReasonCode"}, "IsApproved" = {ml r."IsApproved"}, "IsApplied" = {ml r."IsApplied"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "InventoryAdjustmentId" = {ml r."InventoryAdjustmentId"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

