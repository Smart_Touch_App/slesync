
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Inventory_Ledger"
SET "ItemNumber" = {ml r."ItemNumber"}, "TransactionQty" = {ml r."TransactionQty"}, "TransactionQtyUM" = {ml r."TransactionQtyUM"}, "TransactionQtyPrimaryUM" = {ml r."TransactionQtyPrimaryUM"}, "TransactionType" = {ml r."TransactionType"}, "TransactionId" = {ml r."TransactionId"}, "SettlementID" = {ml r."SettlementID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "InventoryLedgerID" = {ml r."InventoryLedgerID"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

