/* [BUSDTA].[Notification] - begins */

/* TableDDL - [BUSDTA].[Notification] - Start */
IF OBJECT_ID('[BUSDTA].[Notification]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Notification]
	(
	  [NotificationID] NUMERIC(8,0) NOT NULL IDENTITY(1,1)
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [Title] NCHAR(200) NOT NULL
	, [Description] NCHAR(300) NOT NULL
	, [TypeID] NUMERIC(4,0) NULL
	, [Category] NUMERIC(4,0) NULL
	, [SubCategory] NUMERIC(4,0) NULL
	, [Read] NCHAR(1) NULL
	, [ValidTill] DATE NULL
	, [Acknowledged] NCHAR(1) NULL
	, [Initiator] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_NOTIFICATION_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Notification] PRIMARY KEY ([NotificationID] ASC, [RouteID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Notification] - End */

/* SHADOW TABLE FOR [BUSDTA].[Notification] - Start */
IF OBJECT_ID('[BUSDTA].[Notification_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Notification_del]
	(
	  [NotificationID] NUMERIC(8,0)
	, [RouteID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([NotificationID] ASC, [RouteID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Notification] - End */
/* TRIGGERS FOR Notification - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Notification_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Notification_ins
	ON BUSDTA.Notification AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Notification_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Notification_del.NotificationID= inserted.NotificationID AND BUSDTA.Notification_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Notification_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Notification_upd
	ON BUSDTA.Notification AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Notification
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Notification.NotificationID= inserted.NotificationID AND BUSDTA.Notification.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Notification_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Notification_dlt
	ON BUSDTA.Notification AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Notification_del (NotificationID, RouteID, last_modified )
	SELECT deleted.NotificationID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Notification - END */
