
IF OBJECT_ID('[BUSDTA].[SYNC_QUEUE]') IS NULL
BEGIN
CREATE TABLE "BUSDTA"."SYNC_QUEUE" (
    "transaction"                    varchar(50) NULL
   ,"publicationProfile"             varchar(50) NULL
   ,"priority"                       varchar(50) NULL
)
END