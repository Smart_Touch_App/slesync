CREATE OR REPLACE FUNCTION "BUSDTA"."getNextStopDate"(CustomerId numeric(8), SelectedDate date, BaseDate date, WithUnplanned bit)
RETURNS DATE
BEGIN

	DECLARE  WeekNumber numeric(2,0); -- Week Number for SelectedDate
	DECLARE  DayNumber numeric(2,0); -- Day Number for SelectedDate
    DECLARE nextDate date;
    DECLARE nextStopDate date;
    DECLARE MovedCreateStopDate date;
    DECLARE StopType nvarchar(15);
    DECLARE WeeksPerCycle integer;
    /*This cursor gets the pattern, for a Delivery Day Code of the customer.*/
    DECLARE i INTEGER = 1;
    DECLARE Years INTEGER = 1;
    DECLARE loopUpTo INTEGER = (Years * 366); 
    DECLARE cur_DayCodePattern CURSOR FOR
      SELECT DCWN, DCDN
      FROM BUSDTA.M56M0002 
      WHERE DCDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerId);

    DECLARE  Week numeric(4,0); -- Week Number for Delivery Day Code
    DECLARE  Day numeric(4,0); -- Day Number for Delivery Day Code

    DECLARE LOCAL TEMPORARY TABLE TempTab ( val as nchar(50) ); -- Temporary table to store the next dates
    select DMWPC into WeeksPerCycle from busdta.M56M0001 WHERE DMDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerId);
    IF WeeksPerCycle = 0 THEN return null; END IF;
    lp: Loop
        IF i = loopUpTo THEN LEAVE lp END IF;
        select dateadd(day, i, SelectedDate) into nextDate from dummy;

        /*Get the Period, Week and Day number for the given date, with respect to BaseDate*/
        select busdta.GetWeekForDate(nextDate, BaseDate,WeeksPerCycle) into WeekNumber from dummy;
        select busdta.GetDayForDate(nextDate, BaseDate) into DayNumber from dummy ;
    
        /*Inner Loop: This loop generates the dates for pattern of Delivery Day Code, to be compared with the calendar dates.*/
        OPEN cur_DayCodePattern;    
        ilp: LOOP
            FETCH next cur_DayCodePattern into Week, Day;
                IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                    /*If Calendar date matches the Pattern dates, it is a qualified date.*/
                    IF Week = WeekNumber AND Day = DayNumber THEN
                        /*If qualified date is greater than than the selected date, then it is the next valid date.*/
                        IF SelectedDate < nextDate THEN
                            IF WithUnplanned=1 THEN
                                /*Check whether there is any moved or created stopped before planned next stop date. If there is, it is the next date*/
                                select first RPSTDT, RPSTTP into MovedCreateStopDate, StopType from busdta.M56M0004 
                                where RPSTDT > SelectedDate and RPSTDT <= nextDate and RPAN8 = CustomerId order by 1 asc;
                                IF StopType = 'Moved' THEN 
                                    set SelectedDate = MovedCreateStopDate;
				                    set StopType = '';
                                    set MovedCreateStopDate = null;
                                    continue ilp 
                                END IF;
                                IF MovedCreateStopDate < nextDate THEN
                                    select MovedCreateStopDate into nextStopDate from dummy;
                                ELSE
                                    select nextDate into nextStopDate from dummy;
                                END IF;
                                RETURN nextStopDate;
                             ELSE
                                /*Check whether there is any moved or created stopped before planned next stop date. If there is, it is the next date*/
                                select first RPSTDT, RPSTTP into MovedCreateStopDate, StopType from busdta.M56M0004 
                                where RPSTDT > SelectedDate and RPSTDT <= nextDate and RPAN8 = CustomerId and RPSTTP <> 'Unplanned' order by 1 asc;
                                IF StopType = 'Moved' THEN 
                                    set SelectedDate = MovedCreateStopDate;
				                    set StopType = '';
                                    set MovedCreateStopDate = null;
                                    continue ilp 
                                END IF;
                                IF MovedCreateStopDate < nextDate THEN
                                    select MovedCreateStopDate into nextStopDate from dummy;
                                ELSE
                                    select nextDate into nextStopDate from dummy;
                                END IF;
                                RETURN nextStopDate;
                            END IF;
                        END IF;
                    END IF;
        END LOOP; -- End Inner Loop    

        CLOSE cur_DayCodePattern;
        SET i = i + 1;
    END LOOP; -- End Outer Loop
RETURN nextStopDate;
END
GO