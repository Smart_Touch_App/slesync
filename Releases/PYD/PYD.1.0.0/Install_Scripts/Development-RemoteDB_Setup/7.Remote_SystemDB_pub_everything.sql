DROP PUBLICATION IF EXISTS "SystemDB_pub_everything";
GO
/** Create publication 'SystemDB_pub_everything'. **/
CREATE PUBLICATION IF NOT EXISTS "SystemDB_pub_everything" (
    TABLE "BUSDTA"."Device_Environment_Map" ,
    TABLE "BUSDTA"."Device_Master" ,
    TABLE "BUSDTA"."Environment_Master" ,
    TABLE "BUSDTA"."User_Environment_Map" ,
    TABLE "BUSDTA"."user_master" 
)
GO

/** Create the user 'validate_user'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = 'validate_user') THEN
	CREATE SYNCHRONIZATION USER "validate_user";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = 'SystemDB_subs_everything') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "SystemDB_subs_everything";
END IF
GO

/** Create subscription 'SystemDB_subs_everything' to 'SystemDB_pub_everything' for 'validate_user'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION "SystemDB_subs_everything" TO "SystemDB_pub_everything" FOR "validate_user" 
TYPE tcpip ADDRESS 'host=192.168.1.93;port=2439'
	OPTION lt='OFF'
	SCRIPT VERSION 'SLE_SyncScript_PYD.1.0.0'
GO


