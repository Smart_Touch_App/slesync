parameters ml_userid;
GO
DROP PUBLICATION IF EXISTS pub_everything;
GO
/** Create publication 'pub_everything'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_everything" 
(
	TABLE "BUSDTA"."Amount_Range_Index" ,
	TABLE "BUSDTA"."ApprovalCodeLog" ,
	TABLE "BUSDTA"."Cash_Master" ,
	TABLE "BUSDTA"."Cash_Verification_Detail" ,
	TABLE "BUSDTA"."Category_Type" ,
	TABLE "BUSDTA"."Check_Verification_Detail" ,
	TABLE "BUSDTA"."CheckDetails" ,
	TABLE "BUSDTA"."Customer_Ledger" ,
	TABLE "BUSDTA"."Customer_Quote_Detail" ,
	TABLE "BUSDTA"."Customer_Quote_Header" ,
	TABLE "BUSDTA"."Customer_Route_Map" ,
	TABLE "BUSDTA"."Cycle_Count_Detail" ,
	TABLE "BUSDTA"."Cycle_Count_Header" ,
	TABLE "BUSDTA"."Entity_Bucket_Master" ,
	TABLE "BUSDTA"."Entity_Number_Status" ,
	TABLE "BUSDTA"."Entity_Range_Master" ,
	TABLE "BUSDTA"."ExpenseDetails" ,
	TABLE "BUSDTA"."F0004" ,
	TABLE "BUSDTA"."F0005" ,
	TABLE "BUSDTA"."F0006" ,
	TABLE "BUSDTA"."F0014" ,
	TABLE "BUSDTA"."F0101" ,
	TABLE "BUSDTA"."F0150" ,
	TABLE "BUSDTA"."F40073" ,
	TABLE "BUSDTA"."F4013" ,
	TABLE "BUSDTA"."F4070" ,
	TABLE "BUSDTA"."F4071" ,
	TABLE "BUSDTA"."F4072" ,
	TABLE "BUSDTA"."F4075" ,
	TABLE "BUSDTA"."F4076" ,
	TABLE "BUSDTA"."F4092" ,
	TABLE "BUSDTA"."F40941" ,
	TABLE "BUSDTA"."F40942" ,
	TABLE "BUSDTA"."F41002" ,
	TABLE "BUSDTA"."F4101" ,
	TABLE "BUSDTA"."F4102" ,
	TABLE "BUSDTA"."F4106" ,
	TABLE "BUSDTA"."F42019" ,
	TABLE "BUSDTA"."F42119" ,
	TABLE "BUSDTA"."F56M0000" ,
	TABLE "BUSDTA"."F56M0001" ,
	TABLE "BUSDTA"."Feature_Code_Mapping" ,
	TABLE "BUSDTA"."F90CA003" ,
	TABLE "BUSDTA"."F90CA042" ,
	TABLE "BUSDTA"."F90CA086" ,
	TABLE "BUSDTA"."Inventory" ,
	TABLE "BUSDTA"."Inventory_Adjustment" ,
	TABLE "BUSDTA"."Inventory_Ledger" ,
	TABLE "BUSDTA"."Invoice_Header" ,
	TABLE "BUSDTA"."ItemConfiguration" ,
	TABLE "BUSDTA"."ItemCrossReference" ,
	TABLE "BUSDTA"."ItemUoMs" ,
	TABLE "BUSDTA"."M0140" ,
	TABLE "BUSDTA"."M03011" ,
	TABLE "BUSDTA"."M080111" ,
	TABLE "BUSDTA"."M5001" ,
	TABLE "BUSDTA"."M50012" ,
	TABLE "BUSDTA"."M5002" ,
	TABLE "BUSDTA"."M5005" ,
	TABLE "BUSDTA"."M56M0001" ,
	TABLE "BUSDTA"."M56M0002" ,
	TABLE "BUSDTA"."M56M0003" ,
	TABLE "BUSDTA"."M56M0004" ,
	TABLE "BUSDTA"."Metric_CustHistDemand" ,
	TABLE "BUSDTA"."MoneyOrder_Verification_Detail" ,
	TABLE "BUSDTA"."MoneyOrderDetails" ,
	TABLE "BUSDTA"."Notification" ,
	TABLE "BUSDTA"."Payment_Ref_Map" ,
	TABLE "BUSDTA"."Pick_Detail" ,
	TABLE "BUSDTA"."PreTrip_Inspection_Detail" ,
	TABLE "BUSDTA"."PreTrip_Inspection_Header" ,
	TABLE "BUSDTA"."Prospect_Address" ,
	TABLE "BUSDTA"."Prospect_Contact" ,
	TABLE "BUSDTA"."Prospect_Master" ,
	TABLE "BUSDTA"."Prospect_Note" ,
	TABLE "BUSDTA"."Prospect_Quote_Detail" ,
	TABLE "BUSDTA"."Prospect_Quote_Header" ,
	TABLE "BUSDTA"."Question_Master" ,
	TABLE "BUSDTA"."Question_Template" ,
	TABLE "BUSDTA"."ReasonCodeMaster" ,
	TABLE "BUSDTA"."Receipt_Header" ,
	TABLE "BUSDTA"."Request_Authorization_Format" ,
	TABLE "BUSDTA"."Response_Master" ,
	TABLE "BUSDTA"."Route_Master" ,
	TABLE "BUSDTA"."Route_Replenishment_Detail" ,
	TABLE "BUSDTA"."Route_Replenishment_Header" ,
	TABLE "BUSDTA"."Route_Settlement" ,
	TABLE "BUSDTA"."Route_Settlement_Detail" ,
	TABLE "BUSDTA"."SalesOrder_ReturnOrder_Mapping" ,
	TABLE "BUSDTA"."Status_Type" ,
	TABLE "BUSDTA"."UDCKEYLIST" ,
	TABLE "BUSDTA"."UoMFactorReference" ,
	TABLE "BUSDTA"."Vehicle_Master" ,
	TABLE "BUSDTA"."tblAlliedProspCategory" ,
	TABLE "BUSDTA"."tblAlliedProspect" ,
	TABLE "BUSDTA"."tblAlliedProspSubcategory" ,
	TABLE "BUSDTA"."tblBrandLable" ,
	TABLE "BUSDTA"."tblCoffeeBlends" ,
	TABLE "BUSDTA"."tblCoffeeProspect" ,
	TABLE "BUSDTA"."tblCoffeeVolume" ,
	TABLE "BUSDTA"."tblCompetitor" ,
	TABLE "BUSDTA"."tblCSPKLB" ,
	TABLE "BUSDTA"."tblEquipment" ,
	TABLE "BUSDTA"."tblEquipmentCategory" ,
	TABLE "BUSDTA"."tblEquipmentSubcategory" ,
	TABLE "BUSDTA"."tblEquipmentType" ,
	TABLE "BUSDTA"."tblLiqCoffeeType" ,
	TABLE "BUSDTA"."tblPacketSize" ,
	TABLE "BUSDTA"."tblUOM" ,
	TABLE "BUSDTA"."tblUsageMeasurement" ,
	TABLE "BUSDTA"."F40205",	
	TABLE "BUSDTA"."StatusMaster",
	TABLE "BUSDTA"."MasterNotification",
	TABLE "BUSDTA"."Order_Templates",
	TABLE "BUSDTA"."F0101_Audit",
	TABLE "BUSDTA"."F0116_Audit"
)
GO
/** Create the user '{ml_userid}'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = '{ml_userid}') THEN
	CREATE SYNCHRONIZATION USER "{ml_userid}";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = 'subs_everything') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "subs_everything";
END IF
GO

/** Create subscription 'subs_everything' to 'pub_everything' for '{ml_userid}'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION "subs_everything" TO "pub_everything" FOR "{ml_userid}" 
TYPE tcpip ADDRESS 'host=172.30.6.170;port=4002'
	OPTION lt='OFF'
	SCRIPT VERSION 'SLE_SyncScript_PYD.1.3.0'
GO
DROP PUBLICATION IF EXISTS pub_immediate;
GO
/** Create publication 'pub_immediate'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_immediate" 

(
TABLE "BUSDTA"."CustomerConfig" ,
	TABLE "BUSDTA"."F0116" ,
	TABLE "BUSDTA"."F03012" ,
	TABLE "BUSDTA"."F4015" ,
	TABLE "BUSDTA"."M0111" ,
	TABLE "BUSDTA"."M0112" ,
	TABLE "BUSDTA"."M03042" ,
	TABLE "BUSDTA"."M04012" ,
	TABLE "BUSDTA"."M40111" ,
	TABLE "BUSDTA"."M40116" ,
	TABLE "BUSDTA"."M4016" ,
	TABLE "BUSDTA"."Order_Detail" ,
	TABLE "BUSDTA"."Order_Detail_BETA" ,
	TABLE "BUSDTA"."Order_Header" ,
	TABLE "BUSDTA"."Order_Header_BETA" ,
	TABLE "BUSDTA"."Order_PriceAdj" ,
	TABLE "BUSDTA"."Order_PriceAdj_BETA" ,
	TABLE "BUSDTA"."PickOrder" ,
	TABLE "BUSDTA"."PickOrder_Exception"    

)
GO

/** Create the user '{ml_userid}'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = '{ml_userid}') THEN
	CREATE SYNCHRONIZATION USER "{ml_userid}";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = 'subs_immediate') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "subs_immediate";
END IF
GO

/** Create subscription 'subs_immediate' to 'pub_immediate' for '{ml_userid}'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION "subs_immediate" TO "pub_immediate" FOR "{ml_userid}" 
TYPE tcpip ADDRESS 'host=172.30.6.170;port=4002'
	OPTION lt='OFF'
	SCRIPT VERSION 'SLE_SyncScript_PYD.1.3.0'
GO


/*------------------------------------------------------------------------------
*                Create synchronization profile 'busdta_{ml_userid}'.
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE SYNCHRONIZATION PROFILE "prof_everything" 'Subscription=subs_everything';
GO

CREATE OR REPLACE SYNCHRONIZATION PROFILE "prof_immediate" 'Subscription=subs_immediate';
GO



