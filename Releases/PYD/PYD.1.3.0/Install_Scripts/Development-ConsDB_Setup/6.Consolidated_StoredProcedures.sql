/****** Object:  StoredProcedure [BUSDTA].[CheckForNumberReplenishment]    Script Date: 8/14/2015 12:49:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- Entity Property
---- Columns
--EntityName
--NumberRange
--NumberOfBuckets
--Padding
--Format
/* 
select * from BUSDTA.Entity_Bucket_Master
select *  from BUSDTA.Entity_Number_Status
select *  from BUSDTA.Entity_Range_Master
 */ --drop type EntityNumberUpdateType
--*******************************************************************************************************
--Create type EntityNumberUpdateType as table
--(
--    [ID] [int]  NOT NULL,
--	[RouteId] [varchar](10) NULL,
--	[Bucket] [varchar](30) NULL,
--	[EntityBucketMasterId] [varchar](30) NULL,
--	[RangeStart] [int] NULL,
--	[RangeEnd] [int] NULL,
--	[Current] [int] NULL,
--	[IsConsumed] [bit] NULL ,
--	[ConsumedOnDate] [datetime] NULL,
--	[CreatedDate] [datetime] NULL
--)
-- [ID],RouteId,Bucket,EntityBucketMasterId,RangeStart,RangeEnd,[Current],[IsConsumed],[ConsumedOnDate],[CreatedDate]
----*******************************************************************************************************
CREATE procedure [BUSDTA].[CheckForNumberReplenishment]
(
    @UpdateDetails busdta.EntityNumberUpdateType readonly
)
as
Begin
		--select [Current] from @UpdateDetails
--		print getdate()
--End
		--*******************************************************************************************************
		declare  @lastConsumed numeric(8,0);
		declare  @routeId varchar(50);
		
		declare  @count int= 0;
		declare  @countTemp int= 1;
		--select @lastConsumed
		select distinct top 1 @routeId = [RouteId] from @UpdateDetails;
		--select @routeId
		select *  from @UpdateDetails;
		select @count=COUNT([EntityBucketMasterId]) from @UpdateDetails where IsConsumed=1;
		if @count=0
			begin
				return ;
			end
		set @count=0;	
		
		-- Check for Entities Who's buckets are consumed
		--*******************************************************************************************************
		DECLARE @userData TABLE(
			[ID] [int] IDENTITY(1,1) NOT NULL,
			[RouteId] varchar(10) NOT NULL,
			[Bucket] varchar(30) NOT NULL,
			[EntityBucketMasterId] varchar(30) NOT NULL, -- This is the ID of the Entity, which is in the Entity_Bucket_Master
			[RangeEnd] int NOT NULL,
			[BucketSize] numeric(18,0),
			[Replenished] bit  default(0)
		);
		;WITH cte AS
		(
		   SELECT ens.*,ebp.BucketSize, ROW_NUMBER() OVER (PARTITION BY ens.[RouteId] order BY ens.Bucket DESC) AS rn
		   FROM @UpdateDetails ens left outer join busdta.Entity_Bucket_Master ebp
		   on ens.EntityBucketMasterId = ebp.EntityBucketMasterId
		   where IsConsumed=1 and ens.RouteId = @routeId
		   --having COUNT(ens.Entity) < (select BucketSize from dbo.EntityBucketProperties where Entity = ens.Entity)
		)
		insert into @userData SELECT RouteId, Bucket,EntityBucketMasterId ,RangeEnd,BucketSize,0 FROM cte  order by Bucket asc--WHERE rn = 1
		SELECT * FROM @userData
		--*******************************************************************************************************
		-- Get the number of bucket entries which are to be replenished
		SELECT @count = COUNT([Replenished])  FROM @userData where [Replenished]=0
		--*******************************************************************************************************
		--select @count 
		declare @entity varchar(50);
		declare @currentBucket varchar(50);
		declare @newRange numeric(18,0);
		declare @bucketExists int;
		-- Iterate through the entries for refilling bucket with new number range
		while @countTemp <= @count  
			begin 
				--1. Get the Entity which is to be updated
				select @entity=[EntityBucketMasterId] from @userData where ID=@countTemp --2
				select @currentBucket=[Bucket] from @userData where ID=@countTemp --bucket 2
				
				--2. Get the last allocated number from the EntityRangeMaster
				SELECT @lastConsumed = isnull(LastAlloted,RangeStart) FROM busdta.Entity_Range_Master em where em.EntityBucketMasterId =@entity--501
				
				--3. Calculate the new range to be alloted
				select @newRange=(@lastConsumed + BucketSize) from @userData where ID=@countTemp --501 +500
				
				select @bucketExists = COUNT(*) from busdta.Entity_Number_Status where 
				EntityBucketMasterId=@entity
				and isconsumed=0
				and RouteId=@routeId
				and [Bucket]=@currentBucket
				
				if @bucketExists =0 
					Begin
						--4. Insert a new entry for bucket with new range 
						insert into busdta.Entity_Number_Status([RouteId],[Bucket],[EntityBucketMasterId],[RangeStart],[RangeEnd],CreatedDatetime)
						select [RouteId],[Bucket],[EntityBucketMasterId],@lastConsumed+1 ,@newRange,getdate() from @userData where ID=@countTemp
						
						--5. Update EntityRangeMaster with the new range for the current entity
						update busdta.Entity_Range_Master set LastAlloted=@newRange where EntityBucketMasterId =@entity
					End
				--6. Update the replensihed flag in table var
				update @userData set [Replenished] = 1
				
				--7. Update the loop counter and continue
				set @countTemp = @countTemp +1
			end
		--*******************************************************************************************************
		if @count > 0 
		begin
			select * from busdta.Entity_Number_Status --where IsConsumed=0
		end
End

GO


CREATE procedure [BUSDTA].[ConsDBDataClean] as 
   BEGIN
	   truncate table BUSDTA.Order_Header;
	   truncate table BUSDTA.Order_Header_del;
	   truncate table BUSDTA.Order_Detail;
	   truncate table BUSDTA.Order_Detail_del;
	   truncate table BUSDTA.F4015_del;
	   truncate table BUSDTA.PickOrder;
	   truncate table BUSDTA.PickOrder_del;
	   truncate table BUSDTA.PickOrder_Exception;
	   truncate table BUSDTA.PickOrder_Exception_del;
	   truncate table BUSDTA.M4016; -- preorder
	   truncate table BUSDTA.M4016_del;
	   truncate table BUSDTA.M0112; -- Notes Details
	   truncate table BUSDTA.M0112_del;
	   truncate table BUSDTA.M03011; -- Customer Transaction Summary
	   truncate table BUSDTA.M03011_del;
	   truncate table BUSDTA.M03042; -- Customer Payment Detail
	   truncate table BUSDTA.M03042_del;
	   truncate table BUSDTA.M50012; -- Transaction Activity Detail
	   truncate table BUSDTA.M50012_del;
	   truncate table BUSDTA.M50052; -- Sync Detail
	   truncate table BUSDTA.M56M0004;
	   truncate table BUSDTA.M56M0004_del;
	   truncate table BUSDTA.Pretrip_Inspection_Header;
	   truncate table BUSDTA.Pretrip_Inspection_Header_del;
	   truncate table BUSDTA.Pretrip_Inspection_Detail;
	   truncate table BUSDTA.Pretrip_Inspection_Detail_del;
	   truncate table BUSDTA.Check_Verification_Detail;
	   truncate table BUSDTA.Check_Verification_Detail_del;
	   truncate table BUSDTA.Cash_Verification_Detail;
	   truncate table BUSDTA.Cash_Verification_Detail_del;
	   truncate table BUSDTA.Moneyorder_Verification_Detail;
	   truncate table BUSDTA.Moneyorder_Verification_Detail_del;
	   truncate table BUSDTA.MoneyOrderDetails;
	   truncate table BUSDTA.MoneyOrderDetails_del;
	   truncate table BUSDTA.Route_Settlement;
	   truncate table BUSDTA.Route_Settlement_del;
	   truncate table BUSDTA.Route_Settlement_Detail;
	   truncate table BUSDTA.Route_Settlement_Detail_del;
	   truncate table BUSDTA.ExpenseDetails;
	   truncate table BUSDTA.ExpenseDetails_del;
	   truncate table BUSDTA.Inventory_Adjustment;
	   truncate table BUSDTA.Inventory_Adjustment_del;
	   truncate table BUSDTA.Inventory_Ledger;
	   truncate table BUSDTA.Inventory_Ledger_del;
	   truncate table BUSDTA.Invoice_Header;
	   truncate table BUSDTA.Invoice_Header_del;
	   truncate table BUSDTA.Customer_Ledger;
	   truncate table BUSDTA.Customer_Ledger_del;
	   truncate table BUSDTA.Receipt_Header;
	   truncate table BUSDTA.Receipt_Header_del;
	   truncate table BUSDTA.Cycle_Count_Header;
	   truncate table BUSDTA.Cycle_Count_Header_del;
	   truncate table BUSDTA.Cycle_Count_Detail;
	   truncate table BUSDTA.Cycle_Count_Detail_del;
	   truncate table BUSDTA.Pick_Detail;
	   truncate table BUSDTA.Pick_Detail_del;
	   truncate table BUSDTA.Route_Replenishment_Header;
	   truncate table BUSDTA.Route_Replenishment_Header_del;
	   truncate table BUSDTA.Route_Replenishment_Detail;
	   truncate table BUSDTA.Route_Replenishment_Detail_del;
	   truncate table BUSDTA.SalesOrder_ReturnOrder_Mapping;
	   truncate table BUSDTA.SalesOrder_ReturnOrder_Mapping_del;
	   truncate table busdta.notification;
	   truncate table busdta.notification_del;
	   truncate table BUSDTA.Customer_Quote_Header;
	   truncate table BUSDTA.Customer_Quote_Header_del;
	   truncate table BUSDTA.Customer_Quote_Detail;
	   truncate table BUSDTA.Customer_Quote_Detail_del;
	   truncate table busdta.prospect_note;
	   truncate table busdta.prospect_note_del;
	   truncate table busdta.prospect_quote_header;
	   truncate table busdta.prospect_quote_header_del;
	   truncate table busdta.prospect_quote_detail;
	   truncate table busdta.prospect_quote_detail_del;
	   truncate table BUSDTA.ApprovalCodeLog;
	   truncate table BUSDTA.ApprovalCodeLog_del;
	   /* Rebuild Inventory Table - Changed to pull from JDEProdConversionRef*/
	   truncate table busdta.inventory;
	   truncate table busdta.inventory_del; 
	   insert into busdta.inventory (ItemId,ItemNumber,RouteId,OnHandQuantity,CommittedQuantity,HeldQuantity,
		ParLevel,LastReceiptDate,LastConsumeDate,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) 
		select ItemId,IBLITM,RouteMasterID,0,0,0,0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,13,CURRENT_TIMESTAMP,13,CURRENT_TIMESTAMP from busdta.Route_Master A join busdta.F4102 B on LTRIM(BranchNumber)=LTRIM(IBMCU)
		join busdta.ItemConfiguration C on IBITM=ItemId and RouteEnabled=1;
		IF OBJECT_ID('busdta.InventoryTmp', 'U') IS NOT NULL
			DROP TABLE busdta.InventoryTmp; 
		select A.ItemID,A.RouteID,C.LWPQOH*.0001 as OnHand into busdta.InventoryTmp from busdta.inventory A join busdta.Route_Master B on A.RouteId=B.RouteMasterID 
			join  JDEProdConversionRef.dbo.F41021Ref C on A.ItemId=LWITM and LTRIM(B.BranchNumber)=LTRIM(C.LWMCU);
		update A set A.OnHandQuantity=B.OnHand from busdta.inventory A join busdta.InventoryTmp B
			on A.ItemId=B.ItemId and A.RouteId=B.RouteId;  
		update A Set A.ParLevel = ROUND(C.IBROPI*.0001,0) from busdta.inventory A join busdta.Route_Master B on A.RouteId=B.RouteMasterID 
			join  JDEProdConversionRef.dbo.F4102Ref C on A.ItemId=IBITM and LTRIM(B.BranchNumber)=LTRIM(C.IBMCU)
				where IBROPI>0 and ROUND(C.IBROPI*.0001,0)<9999;
   END
 GO
/*UPDATED - VER5*/
CREATE PROCEDURE [dbo].[get_cpu_stats]
(@DBName VARCHAR(100)='MobileDataModel')
AS
BEGIN
	DECLARE @ts_now BIGINT, @cpu DECIMAL(4,2);
	SELECT @ts_now = ms_ticks FROM sys.dm_os_sys_info
	SELECT TOP 1 
	@cpu=(100 - SystemIdle)
	FROM (
	SELECT record.value('(./Record/@id)[1]', 'int') AS record_id, record.value('(./Record/SchedulerMonitorEvent/SystemHealth/SystemIdle)[1]', 'int') AS SystemIdle,
	record.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') AS SQLProcessUtilization,
	TIMESTAMP FROM ( SELECT TIMESTAMP, CONVERT(XML, record) AS record FROM sys.dm_os_ring_buffers WHERE ring_buffer_type = N'RING_BUFFER_SCHEDULER_MONITOR' 
	AND record LIKE '%<SystemHealth>%') AS x ) AS y ORDER BY record_id DESC

	
	DECLARE @db INT, @nondb INT;

	SELECT @db = COUNT(dbid) 
		FROM sys.sysprocesses 
		WHERE dbid > 0 AND DB_NAME(dbid)=@DBName
		
	SELECT @nondb = COUNT(dbid) 
		FROM sys.sysprocesses 
		WHERE dbid > 0 AND DB_NAME(dbid)!=@DBName
		
	SELECT @DBName database_name, @db AS db_connections_count, @nondb AS nondb_connections_count, @cpu AS cpu_usage
END

GO
CREATE PROCEDURE [dbo].[get_currently_running_queries]
(@DBName VARCHAR(100)='MobileDataModel')
AS
BEGIN
	DECLARE @ts_now BIGINT, @cpu DECIMAL(4,2);
	SELECT @ts_now = ms_ticks FROM sys.dm_os_sys_info
	SELECT TOP 1 
	@cpu=(100 - SystemIdle)
	FROM (
	SELECT record.value('(./Record/@id)[1]', 'int') AS record_id, record.value('(./Record/SchedulerMonitorEvent/SystemHealth/SystemIdle)[1]', 'int') AS SystemIdle,
	record.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') AS SQLProcessUtilization,
	TIMESTAMP FROM ( SELECT TIMESTAMP, CONVERT(XML, record) AS record FROM sys.dm_os_ring_buffers WHERE ring_buffer_type = N'RING_BUFFER_SCHEDULER_MONITOR' 
	AND record LIKE '%<SystemHealth>%') AS x ) AS y ORDER BY record_id DESC

	
	DECLARE @db INT, @nondb INT;

	SELECT @db = COUNT(dbid) 
		FROM sys.sysprocesses 
		WHERE dbid > 0 AND DB_NAME(dbid)=@DBName
		
	SELECT @nondb = COUNT(dbid) 
		FROM sys.sysprocesses 
		WHERE dbid > 0 AND DB_NAME(dbid)!=@DBName
		
	SELECT @DBName database_name, @db AS db_connections_count, @nondb AS nondb_connections_count, @cpu AS cpu_usage
END

GO

CREATE PROCEDURE [dbo].[Register_DeviceRoute]
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30)
)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM BUSDTA.Route_Master WHERE RouteName=@RouteID)
	BEGIN
		UPDATE BUSDTA.Route_Master SET RouteStatusID=(SELECT RouteStatusID FROM busdta.RouteStatus WHERE RouteStatus='REGISTERED')
			WHERE RouteName=@RouteID
		UPDATE BUSDTA.Route_Device_Map SET Active=1 WHERE Device_Id=@DeviceId AND Route_Id=@RouteID
	END
END

GO
CREATE PROCEDURE [dbo].[Reserve_DeviceRoute]
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30),
	@DBID INT
)
AS
BEGIN
	--IF NOT EXISTS(SELECT 1 FROM BUSDTA.Route_Device_Map WHERE Route_Id=@RouteID AND Device_Id=@DeviceId)
	--BEGIN
	--	INSERT INTO BUSDTA.Route_Device_Map (Route_Id, Device_Id, Active, Remote_Id)
	--	VALUES (@RouteID, @DeviceId, 0, CONCAT(@DeviceId, '_', @RouteID))
	--END
	IF NOT EXISTS(SELECT 1 FROM ml_user WHERE name=CAST(@DBID AS VARCHAR))
	BEGIN
		INSERT INTO ml_user (name) VALUES (@DBID)
	END
	UPDATE BUSDTA.Route_Master SET RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='PENDING REGISTRATION')
	WHERE RouteName=@RouteID AND RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='UNREGISTERED')
END
GO

CREATE PROCEDURE [dbo].[SaveReserveLog]
(
	@Appuserid INT,
	@RouteId VARCHAR(10)
)
AS
BEGIN
SET NOCOUNT ON;
	INSERT INTO BUSDTA.Reserve_RouteUser(App_user_id, Route_Id) 
	VALUES (@Appuserid,@RouteId)
SET NOCOUNT OFF;
END

GO




CREATE PROCEDURE [dbo].[SaveResponseLog]
(
	@DeviceID VARCHAR(100)='',
	@Request VARCHAR(MAX),
	@Response VARCHAR(MAX)
)
AS
BEGIN
	INSERT INTO [dbo].[tblResponseLog] ([DeviceID], [Request], [Response])
	VALUES(@DeviceID, CAST(@Request AS XML), CAST(@Response AS XML))
END

GO

CREATE PROCEDURE [dbo].[SaveWebServiceLog]
(
	@DeviceID VARCHAR(100)='',
	@Request NVARCHAR(MAX),
	@Response NVARCHAR(MAX),
	@Environment VARCHAR(128)='Development',
	@ServiceName VARCHAR(100)='',
	@ServiceEndPoint VARCHAR(1000)='',
	@ServiceContract VARCHAR(1000)=''
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @DBName VARCHAR(128); 
	SET @DBName = (SELECT DB_NAME());

	EXEC [SystemDB].[dbo].[SaveWebServiceLog] 
			@DeviceID=@DeviceID, 
			@Request =@Request,
			@Response =@Response,
			@DBName = @DBName,
			@Environment = @Environment,
			@ServiceName = @ServiceName,
			@ServiceEndPoint = @ServiceEndPoint,
			@ServiceContract = @ServiceContract
SET NOCOUNT OFF
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetupNumberPoolForAllEntities]
as
Begin
	Declare @EntityBucketMasterId as numeric(4,0)=1
	Declare EntityBucketCursor CURSOR FOR
	Select EntityBucketMasterId from   BUSDTA.Entity_Bucket_Master (Nolock)
	OPEN EntityBucketCursor
		FETCH NEXT FROM EntityBucketCursor INTO @EntityBucketMasterId
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXEC [dbo].[SetupNumberPoolForEntity] @EntityBucketMasterId
					FETCH NEXT FROM EntityBucketCursor INTO @EntityBucketMasterId
				END
		CLOSE EntityBucketCursor
	DEALLOCATE EntityBucketCursor
End
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetupNumberPoolForEntity]
@EntityBucketMasterId numeric(2,0)
as
Begin
	SET NOCOUNT ON;
	Declare @Entity as varchar(500)
	Declare @Name as varchar(30)
	Declare @RouteID as varchar(2)
	Declare @BucketSize as numeric(4,0)
	Declare @NoOfbucket as numeric(4,0)
	Declare @BucketExists as numeric(4,0)
	Declare @BucketName as nvarchar(25)
	Declare @LastAlloted as numeric(8,0)
	Declare RouteCursor CURSOR FOR
	Select RouteMasterID from  BUSDTA.Route_Master (Nolock)
	OPEN RouteCursor
		FETCH NEXT FROM RouteCursor INTO @RouteID
			WHILE @@FETCH_STATUS = 0
				BEGIN
					select @Entity=Entity,@BucketSize=BucketSize,@NoOfbucket=NoOfbuckets from BUSDTA.Entity_Bucket_Master
					where EntityBucketMasterId=@EntityBucketMasterId;
					select @LastAlloted= case  when isnull(LastAlloted,0)=0 then RangeStart else LastAlloted +1 End from BUSDTA.Entity_Range_Master
					where EntityBucketMasterId=@EntityBucketMasterId and Active=1;
					declare @counter numeric(2,0)=1;
					while(@counter<=@NoOfbucket)
						begin
						    set @BucketName = 'Bucket' +convert(varchar,@counter);
						    select @BucketExists=COUNT(*) from BUSDTA.Entity_Number_Status where
						    RouteId = @RouteID 
						    and Bucket = @BucketName
						    and EntityBucketMasterId = @EntityBucketMasterId
						    and IsConsumed=0;
							if(@BucketExists=0)
								Begin
									insert into BUSDTA.Entity_Number_Status 
									(RouteId,Bucket,EntityBucketMasterId,RangeStart,RangeEnd,CurrentAllocated,IsConsumed,CreatedDatetime)
									values
									(@RouteID,'Bucket' +convert(varchar,@counter),@EntityBucketMasterId,@LastAlloted,@LastAlloted + @BucketSize,null,0,GETDATE())
									set @LastAlloted=@LastAlloted + @BucketSize;
									update BUSDTA.Entity_Range_Master set LastAlloted=@LastAlloted
									where EntityBucketMasterId=@EntityBucketMasterId and Active=1
									set @LastAlloted=@LastAlloted + 1;
								End
							set @counter = @counter+1;	
						end
					FETCH NEXT FROM RouteCursor INTO @RouteID
				END
		CLOSE RouteCursor
	DEALLOCATE RouteCursor
	/*
	--truncate table  BUSDTA.Entity_Number_Status
	--update BUSDTA.Entity_Range_Master set LastAlloted=null
	select * from BUSDTA.Entity_Number_Status
	select * from BUSDTA.Entity_Bucket_Master
	select * from BUSDTA.Entity_Range_Master
	select * from BUSDTA.Route_Master
	*/
End
GO



CREATE PROCEDURE [dbo].[USP_GetCursorForTable]
(
	@version NVARCHAR(100),
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* ' + @version + ' ' + @table + ' - begins */'

--DECLARE @version NVARCHAR(100)='VER1'
--DECLARE @table NVARCHAR(100)='tblCountries'

/* Download Cursor */
DECLARE @selectStmt NVARCHAR(MAX)
DECLARE @dc NVARCHAR(MAX)
SET @selectStmt = NULL
--SELECT @selectStmt = COALESCE(@selectStmt + ',','') + CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"', COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
SELECT @selectStmt = COALESCE(@selectStmt + ',','') + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"', COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME=@table AND COLUMN_NAME != 'last_modified'
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION

SELECT @dc = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')+CHAR(13) + CHAR(10)+
	'WHERE ' + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"last_modified">= {ml s.last_table_download}')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''download_cursor'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''

--UNION ALL
PRINT 'SELECT ' + @selectStmt
--UNION ALL
PRINT 'FROM ' +@dc
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'

/* Download Cursor -END */

/* Download Delete Cursor */
DECLARE @ddc NVARCHAR(MAX)
DECLARE @pkCols NVARCHAR(MAX)
SET @pkCols = NULL
--SELECT @pkCols =COALESCE(@pkCols + ',','') + CONCAT('"',A.TABLE_CATALOG,'".','"', A.TABLE_SCHEMA,'".','"', A.TABLE_NAME,'_del".','"', B.COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
SELECT @pkCols =COALESCE(@pkCols + ',','') + CONCAT('"', A.TABLE_SCHEMA,'".','"', A.TABLE_NAME,'_del".','"', B.COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME
SELECT @ddc = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'_del"')+CHAR(13) + CHAR(10)+
	'WHERE ' + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'_del".','"last_modified">= {ml s.last_table_download}')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

PRINT 'EXEC ml_add_lang_table_script'
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''download_delete_cursor'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT 'SELECT ' + @pkCols
--UNION ALL
PRINT 'FROM ' +@ddc
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'
/* Download Delete Cursor -END */

/* Upload Insert Cursor */
DECLARE @ui NVARCHAR(MAX)
SELECT @ui = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

DECLARE @uiCols NVARCHAR(MAX)
SELECT @uiCols =COALESCE(@uiCols + ', ','') + CONCAT('"', A.COLUMN_NAME ,'"')
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME

DECLARE @uiColML NVARCHAR(MAX)
SELECT @uiColML =COALESCE(@uiColML + ', {ml r.','') + CONCAT('"', A.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''upload_insert'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT '/* Insert the row into the consolidated database. */'
--UNION ALL
PRINT 'INSERT INTO ' + @ui
--UNION ALL
PRINT '(' + @uiCols +')'
--UNION ALL
PRINT 'VALUES({ml r.' + @uiColML +')'
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'
/* Upload Insert Cursor -END */


/* Upload Update Cursor */
DECLARE @uu NVARCHAR(MAX)
SELECT @uu = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

DECLARE @uuCols NVARCHAR(MAX)
SELECT @uuCols =COALESCE(@uuCols + ', ','') + CONCAT('"', A.COLUMN_NAME ,'" = {ml r."', A.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified' AND A.COLUMN_NAME NOT IN 
(SELECT B1.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A1, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B1 WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A1.CONSTRAINT_NAME = B1.CONSTRAINT_NAME AND A1.TABLE_NAME=A.TABLE_NAME)
ORDER BY A.TABLE_NAME

DECLARE @uupkCols NVARCHAR(MAX)
SET @uupkCols = NULL
SELECT @uupkCols =COALESCE(@uupkCols + ' AND ','') + CONCAT('"', B.COLUMN_NAME ,'" = {ml r."', B.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'	
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''upload_update'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT '/* Update the row in the consolidated database. */'
--UNION ALL
PRINT 'UPDATE ' + @uu
--UNION ALL
PRINT 'SET ' + @uuCols 
--UNION ALL
PRINT 'WHERE ' +@uupkCols
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'
/* Upload Update Cursor - END */

/* Upload Delete Cursor */
DECLARE @ud NVARCHAR(MAX)
SELECT @ud = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

DECLARE @udCols NVARCHAR(MAX)
SET @udCols = NULL
SELECT @udCols =COALESCE(@udCols + ' AND ','') + CONCAT('"', B.COLUMN_NAME ,'" = {ml r."', B.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'	
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''upload_delete'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT '/* Delete the row in the consolidated database. */'
--UNION ALL
PRINT 'DELETE FROM ' + @ud
--UNION ALL
PRINT 'WHERE ' +@udCols
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'

/* Upload Delete Cursor - END */
Print '/* ' + @version + ' ' + @table + ' - ends */'
Print ''
Print ''

SET NOCOUNT OFF
END



GO



CREATE PROCEDURE [dbo].[USP_GetDownloadCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Download Cursor */
DECLARE @selectStmt NVARCHAR(MAX)
DECLARE @dc NVARCHAR(MAX)
SET @selectStmt = NULL
--SELECT @selectStmt = COALESCE(@selectStmt + ',','') + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"', COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
SELECT @selectStmt = COALESCE(@selectStmt + ',','') + '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '".' + '"' + COLUMN_NAME + '"' + CHAR(13) + CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME=@table AND COLUMN_NAME != 'last_modified'
ORDER BY TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION

SELECT @dc = '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '"'+CHAR(13) + CHAR(10)+
	'WHERE ' + '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '".' + '"last_modified">= {ml s.last_table_download}'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

PRINT 'SELECT ' + @selectStmt
PRINT 'FROM ' +@dc

/* Download Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END



GO

CREATE PROCEDURE [dbo].[USP_GetDownloadDeleteCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Download Delete Cursor */
DECLARE @ddc NVARCHAR(MAX)
DECLARE @pkCols NVARCHAR(MAX)
SET @pkCols = NULL
--SELECT @pkCols =COALESCE(@pkCols + ',','') + CONCAT('"', A.TABLE_SCHEMA,'".','"', A.TABLE_NAME,'_del".','"', B.COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
SELECT @pkCols =COALESCE(@pkCols + ',','') + '"' + A.TABLE_SCHEMA + '".' + '"' + A.TABLE_NAME + '_del".' + '"' + B.COLUMN_NAME + '"' + CHAR(13) + CHAR(10)
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS AS C
WHERE (CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME)
AND(A.TABLE_CATALOG=C.TABLE_CATALOG AND A.TABLE_SCHEMA=C.TABLE_SCHEMA AND A.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME,  C.ORDINAL_POSITION
SELECT @ddc = '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '_del"'+CHAR(13) + CHAR(10)+
	'WHERE ' + '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '_del".' + '"last_modified">= {ml s.last_table_download}'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

PRINT 'SELECT ' + @pkCols
PRINT 'FROM ' +@ddc
/* Download Delete Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END


GO


CREATE PROCEDURE [dbo].[USP_GetTRandShadow]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
--Print '/* ' + @table + ' - begins */
--'
--DECLARE @table NVARCHAR(100)=@table
--PRINT '/* Timestamp Column last_modified FOR USER DEFINED TABLE */'
--PRINT 'ALTER TABLE BUSDTA.['+ @table +'] ADD last_modified DATETIME DEFAULT GETDATE();'
--PRINT 'GO'
--PRINT ' '
DECLARE @uupkCols NVARCHAR(MAX)
SET @uupkCols = NULL
SELECT @uupkCols =COALESCE(@uupkCols + ', ','') + CONCAT( B.COLUMN_NAME ,' ', C.DATA_TYPE ,' ',CASE WHEN C.IS_NULLABLE = 'NO' THEN 'NOT NULL' END)
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS C
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND (B.TABLE_CATALOG=c.TABLE_CATALOG AND B.TABLE_SCHEMA=C.TABLE_SCHEMA AND B.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

DECLARE @uupk NVARCHAR(MAX)
SET @uupk = NULL
SELECT @uupk =COALESCE(@uupk + ', ','') + B.COLUMN_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS C
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND (B.TABLE_CATALOG=c.TABLE_CATALOG AND B.TABLE_SCHEMA=C.TABLE_SCHEMA AND B.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

--/* Create Shadow Table */
--DECLARE @uu NVARCHAR(MAX)
--SELECT @uu = CONCAT('CREATE TABLE ',TABLE_CATALOG,'.', TABLE_SCHEMA,'.',TABLE_NAME,'_del (')
--FROM INFORMATION_SCHEMA.TABLES 
--WHERE TABLE_NAME=@table
--ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

--PRINT '/* SHADOW TABLE FOR USER DEFINED TABLE */'
--PRINT 
--	@uu 
--	+CHAR(13) + CHAR(10)+ 
--	@uupkCols 
--	+CHAR(13) + CHAR(10)+ 
--	', last_modified DATETIME DEFAULT GETDATE(),
--		PRIMARY KEY ('+ @uupk +')
--	);
--GO'
--PRINT ' '
--/* Create Shadow Table - END */
DECLARE @TableName VARCHAR(250), @SchemaName VARCHAR(250)
SELECT @TableName=TABLE_NAME, @SchemaName=TABLE_SCHEMA
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

/* Create Insert Trigger */
DECLARE @pkCols NVARCHAR(MAX)
SET @pkCols = NULL
SELECT @pkCols =COALESCE(@pkCols + ' AND ','') + CONCAT('', A.TABLE_SCHEMA,'.', A.TABLE_NAME,'.', B.COLUMN_NAME ,'= inserted.', B.COLUMN_NAME )
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

DECLARE @pkCols_del NVARCHAR(MAX)
SET @pkCols_del = NULL
SELECT @pkCols_del =COALESCE(@pkCols_del + ' AND ','') + CONCAT('', A.TABLE_SCHEMA,'.', A.TABLE_NAME,'_del.', B.COLUMN_NAME ,'= inserted.', B.COLUMN_NAME )
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

PRINT '/* TRIGGERS FOR '+ @TableName +' - START */'
PRINT '/* Create the shadow insert trigger */'
DECLARE @it NVARCHAR(MAX)

PRINT 'IF OBJECT_ID('''+ @SchemaName +'.'+ @TableName +'_ins'') IS NULL
BEGIN
	EXEC('''
PRINT '	CREATE TRIGGER '+ @SchemaName +'.'+ @TableName +'_ins
	ON '+ @SchemaName +'.'+ @TableName +' AFTER INSERT'
PRINT '	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/'
SELECT @it='	DELETE FROM '+ @SchemaName +'.'+ @TableName +'_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE '+ @pkCols_del +'
		);
	'')
END
GO'
PRINT @it
/* Create Insert Trigger - END */

/* Create Update Trigger */
PRINT ' '
PRINT '/* Create the shadow update trigger. */'

DECLARE @ut NVARCHAR(MAX)
SELECT @ut='IF OBJECT_ID('''+ @SchemaName +'.'+ @TableName +'_upd'') IS NULL
BEGIN
	EXEC('''
PRINT @ut
SELECT @ut='CREATE TRIGGER '+ @SchemaName +'.'+ @TableName +'_upd
	ON '+ @SchemaName +'.'+ @TableName +' AFTER UPDATE'
PRINT @ut
PRINT 'AS
/* Update the column last_modified in modified row. */'
SELECT @ut='UPDATE '+ @SchemaName +'.'+ @TableName +'
	SET last_modified = GETDATE()
	FROM inserted
		WHERE '+ @pkCols +''');
END
GO'
PRINT @ut

/* Create Update Trigger - END */

DECLARE @uupkDeleted NVARCHAR(MAX)
SET @uupkDeleted = NULL
SELECT @uupkDeleted =COALESCE(@uupkDeleted + ', ','') + 'deleted.'+B.COLUMN_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS C
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND (B.TABLE_CATALOG=c.TABLE_CATALOG AND B.TABLE_SCHEMA=C.TABLE_SCHEMA AND B.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

/* Create Delete Trigger */
PRINT ' '
PRINT '/* Create the shadow delete trigger */'
DECLARE @dt NVARCHAR(MAX)
SELECT @dt='IF OBJECT_ID('''+ @SchemaName +'.'+ @TableName +'_dlt'') IS NULL
BEGIN
	EXEC('''
PRINT @dt

SELECT @dt='CREATE TRIGGER '+ @SchemaName +'.'+ @TableName +'_dlt
	ON '+ @SchemaName +'.'+ @TableName +' AFTER DELETE'
PRINT @dt
PRINT 'AS
/* Insert the row into the shadow delete table. */'
SELECT @dt='INSERT INTO '+ @SchemaName +'.'+ @TableName +'_del ('+ @uupk +', last_modified )
	SELECT '+ @uupkDeleted +', GETDATE()
	FROM deleted;
	'');
END
GO'
PRINT @dt

/* Create Delete Trigger - END */

PRINT '/* TRIGGERS FOR '+ @TableName +' - END */'
SET NOCOUNT OFF
END



GO



CREATE PROCEDURE [dbo].[USP_GetUploadDeleteCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/*'
Print ''

/* Upload Delete Cursor */
DECLARE @ud NVARCHAR(MAX)
SELECT @ud = '"'+TABLE_SCHEMA+'".'+'"'+TABLE_NAME+'"'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

DECLARE @udCols NVARCHAR(MAX)
SET @udCols = NULL
SELECT @udCols =COALESCE(@udCols + ' AND ','') + '"'+B.COLUMN_NAME+'" = {ml r."'+B.COLUMN_NAME+'"}'
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS AS C
WHERE (CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME)
AND(A.TABLE_CATALOG=C.TABLE_CATALOG AND A.TABLE_SCHEMA=C.TABLE_SCHEMA AND A.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME, C.ORDINAL_POSITION

PRINT '/* Delete the row in the consolidated database. */'
PRINT 'DELETE FROM ' + @ud
PRINT 'WHERE ' +@udCols

/* Upload Delete Cursor - END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO

CREATE PROCEDURE [dbo].[USP_GetUploadInsertCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Upload Insert Cursor */
DECLARE @ui NVARCHAR(MAX)
SELECT @ui = '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '"'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

DECLARE @uiCols NVARCHAR(MAX)
SELECT @uiCols =COALESCE(@uiCols + ', ','') + '"' + A.COLUMN_NAME + '"'
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME

DECLARE @uiColML NVARCHAR(MAX)
SELECT @uiColML =COALESCE(@uiColML + ', {ml r.','') + '"' + A.COLUMN_NAME + '"}'
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME
PRINT '/* Insert the row into the consolidated database. */'
PRINT 'INSERT INTO ' + @ui
PRINT '(' + @uiCols +')'
PRINT 'VALUES({ml r.' + @uiColML +')'
/* Upload Insert Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO

CREATE PROCEDURE [dbo].[USP_GetUploadUpdateCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/*'
Print ''

/* Upload Update Cursor */
DECLARE @uu NVARCHAR(MAX)
SELECT @uu = '"'+TABLE_SCHEMA+'".'+'"'+TABLE_NAME+'"'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

DECLARE @uuCols NVARCHAR(MAX)
SELECT @uuCols =COALESCE(@uuCols + ', ','') + '"'+A.COLUMN_NAME+'" = {ml r."'+A.COLUMN_NAME+'"}'
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified' AND A.COLUMN_NAME NOT IN 
(SELECT B1.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A1, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B1 WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A1.CONSTRAINT_NAME = B1.CONSTRAINT_NAME AND A1.TABLE_NAME=A.TABLE_NAME)
ORDER BY A.TABLE_NAME

DECLARE @uupkCols NVARCHAR(MAX)
SET @uupkCols = NULL
SELECT @uupkCols =COALESCE(@uupkCols + ' AND ','') + '"'+B.COLUMN_NAME+'" = {ml r."'+B.COLUMN_NAME+'"}'
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS AS C
WHERE (CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME)
AND(A.TABLE_CATALOG=C.TABLE_CATALOG AND A.TABLE_SCHEMA=C.TABLE_SCHEMA AND A.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME, C.ORDINAL_POSITION

PRINT '/* Update the row in the consolidated database. */'
PRINT 'UPDATE ' + @uu
PRINT 'SET ' + @uuCols 
PRINT 'WHERE ' +@uupkCols
/* Upload Update Cursor - END */
Print ''
Print '*/'

SET NOCOUNT OFF
END



GO



CREATE PROCEDURE [dbo].[USP_RollbackERPResponse]
(
	@TransactionID uniqueidentifier = NULL
)
AS
BEGIN
DECLARE @Columns_Updated NVARCHAR(1000)
DECLARE @WhereClause NVARCHAR(1000)
DECLARE @EXQUERY VARCHAR(MAX)
DECLARE @Cnt INT 

SELECT TableName, IDENTITY(INT,1,1) Cnt INTO #TmpTables FROM [BUSDTA].[tblTransactionAudit] 
WHERE TransactionID = @TransactionID
GROUP BY TableName

CREATE TABLE #TmpUpdQry ( Qry VARCHAR(MAX), TableName VARCHAR(500), WhereClause NVARCHAR(1000))

	SET @Cnt = 1
	WHILE @Cnt <= (SELECT COUNT(*) FROM #TmpTables)
	BEGIN 
		SET @Columns_Updated = NULL
		SET @WhereClause = NULL

		SELECT @Columns_Updated = ISNULL(@Columns_Updated + ', ', '') + RTRIM(FieldName)+'='''+ RTRIM(OldValue) +'''' 
		FROM [BUSDTA].[tblTransactionAudit] 
			JOIN #TmpTables ON #TmpTables.TableName = [BUSDTA].[tblTransactionAudit].TableName 
			AND #TmpTables.Cnt = @Cnt AND [BUSDTA].[tblTransactionAudit].TransactionID = @TransactionID
		WHERE IsPkey=0
	
		SELECT @WhereClause = ISNULL(@WhereClause + ' AND ', '') + RTRIM(FieldName)+'='''+ RTRIM(OldValue) +'''' 
		FROM [BUSDTA].[tblTransactionAudit] 
			JOIN #TmpTables ON #TmpTables.TableName = [BUSDTA].[tblTransactionAudit].TableName AND #TmpTables.Cnt = @Cnt 
			AND [BUSDTA].[tblTransactionAudit].TransactionID = @TransactionID
		WHERE IsPkey=1

		INSERT INTO #TmpUpdQry ( Qry, TableName, WhereClause) 
		SELECT @Columns_Updated, #TmpTables.TableName, @WhereClause FROM #TmpTables WHERE #TmpTables.Cnt = @Cnt
		
		SELECT @EXQUERY = 'UPDATE '+A.SchemaName+'.' + A.TableName + ' SET ' + Qry + ' WHERE ' + WhereClause FROM #TmpUpdQry
		JOIN [BUSDTA].[tblTransactionAudit] A ON A.TableName = #TmpUpdQry.TableName 
		--SELECT (@EXQUERY)
	EXECUTE(@EXQUERY)
	SET @Cnt = @Cnt + 1
	END

END



GO



CREATE PROCEDURE [dbo].[uspCDBScriptSchema] 
(
	@table_name SYSNAME,
	@add_timestamp_col bit=0,
	@generate_cdb_script_only bit=0
)
AS
BEGIN
/*CREATE TABLE SCRIPT - START*/
DECLARE @table SYSNAME

DECLARE 
      @object_name SYSNAME
    , @object_id INT

SELECT 
      @object_name = '[' + s.name + '].[' + o.name + ']'
	, @table = o.name
    , @object_id = o.[object_id]
FROM sys.objects o WITH (NOWAIT)
JOIN sys.schemas s WITH (NOWAIT) ON o.[schema_id] = s.[schema_id]
WHERE s.name + '.' + o.name = @table_name
    AND o.[type] = 'U'
    AND o.is_ms_shipped = 0

DECLARE @SQL NVARCHAR(MAX) = ''

PRINT '/*
/* '+ @object_name +' - begins */

/* TableDDL - '+ @object_name +' - Start */
IF OBJECT_ID('''+ @object_name +''') IS NULL'
PRINT 'BEGIN
'

;WITH 
fk_columns AS 
(
     SELECT 
          k.constraint_object_id
        , cname = c.name
        , rcname = rc.name
    FROM sys.foreign_key_columns k WITH (NOWAIT)
    JOIN sys.columns rc WITH (NOWAIT) ON rc.[object_id] = k.referenced_object_id AND rc.column_id = k.referenced_column_id 
    JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = k.parent_object_id AND c.column_id = k.parent_column_id
    WHERE k.parent_object_id = @object_id
)
SELECT @SQL = '	CREATE TABLE ' + @object_name + CHAR(13) + '	(' + CHAR(13) + STUFF((
    SELECT CHAR(9) + ', [' + c.name + '] ' + 
        CASE WHEN c.is_computed = 1
            THEN 'AS ' + cc.[definition] 
			WHEN tp.name IN ('INTEGER') 
                THEN 'INT'
			WHEN tp.name IN ('TEXT', 'NTEXT') 
                THEN tp.name 
            ELSE UPPER(tp.name) + 
                CASE WHEN tp.name IN ('varchar', 'char', 'varbinary', 'binary')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('nvarchar', 'nchar')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length / 2 AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('datetime2', 'time2', 'datetimeoffset') 
                       THEN '(' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'decimal' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'NUMERIC' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                    ELSE ''
                END
			END +
                --CASE WHEN c.collation_name IS NOT NULL THEN ' COLLATE ' + c.collation_name ELSE '' END +
                CASE WHEN c.is_nullable = 1 THEN ' NULL' ELSE ' NOT NULL' END +
                CASE WHEN dc.[definition] IS NOT NULL THEN ' CONSTRAINT DF_' + UPPER(OBJECT_NAME(C.object_id)) +'_' + C.name +' DEFAULT' + dc.[definition] ELSE '' END + 
                CASE WHEN ic.is_identity = 1 THEN ' IDENTITY(' + CAST(ISNULL(ic.seed_value, '0') AS CHAR(1)) + ',' + CAST(ISNULL(ic.increment_value, '1') AS CHAR(1)) + ')' ELSE '' END 
         + CHAR(13)
    FROM sys.columns c WITH (NOWAIT)
    JOIN sys.types tp WITH (NOWAIT) ON c.user_type_id = tp.user_type_id
    LEFT JOIN sys.computed_columns cc WITH (NOWAIT) ON c.[object_id] = cc.[object_id] AND c.column_id = cc.column_id
    LEFT JOIN sys.default_constraints dc WITH (NOWAIT) ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
    LEFT JOIN sys.identity_columns ic WITH (NOWAIT) ON c.is_identity = 1 AND c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
    WHERE c.[object_id] = @object_id
    ORDER BY c.column_id
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, CHAR(9) + ' ')
    + ISNULL((SELECT CHAR(9) + ', CONSTRAINT [PK_' + @table + '] PRIMARY KEY (' + --' + k.name + '
                    (SELECT STUFF((
                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                         FROM sys.index_columns ic WITH (NOWAIT)
                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
                             AND ic.[object_id] = k.parent_object_id 
                             AND ic.index_id = k.unique_index_id     
                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
            + ')' + CHAR(13)
            FROM sys.key_constraints k WITH (NOWAIT)
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '') + '	)'  + CHAR(13)
    + ISNULL((SELECT (
        SELECT CHAR(13) +
             'ALTER TABLE ' + @object_name + ' WITH' 
            + CASE WHEN fk.is_not_trusted = 1 
                THEN ' NOCHECK' 
                ELSE ' CHECK' 
              END + 
              ' ADD CONSTRAINT [' + fk.name  + '] FOREIGN KEY(' 
              + STUFF((
                SELECT ', [' + k.cname + ']'
                FROM fk_columns k
                WHERE k.constraint_object_id = fk.[object_id]
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
               + ')' +
              ' REFERENCES [' + SCHEMA_NAME(ro.[schema_id]) + '].[' + ro.name + '] ('
              + STUFF((
                SELECT ', [' + k.rcname + ']'
                FROM fk_columns k
                WHERE k.constraint_object_id = fk.[object_id]
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
               + ')'
            + CASE 
                WHEN fk.delete_referential_action = 1 THEN ' ON DELETE CASCADE' 
                WHEN fk.delete_referential_action = 2 THEN ' ON DELETE SET NULL'
                WHEN fk.delete_referential_action = 3 THEN ' ON DELETE SET DEFAULT' 
                ELSE '' 
              END
            + CASE 
                WHEN fk.update_referential_action = 1 THEN ' ON UPDATE CASCADE'
                WHEN fk.update_referential_action = 2 THEN ' ON UPDATE SET NULL'
                WHEN fk.update_referential_action = 3 THEN ' ON UPDATE SET DEFAULT'  
                ELSE '' 
              END 
            + CHAR(13) + 'ALTER TABLE ' + @object_name + ' CHECK CONSTRAINT [' + fk.name  + ']' + CHAR(13)
        FROM sys.foreign_keys fk WITH (NOWAIT)
        JOIN sys.objects ro WITH (NOWAIT) ON ro.[object_id] = fk.referenced_object_id
        WHERE fk.parent_object_id = @object_id
        FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)')), '')

PRINT @SQL

PRINT 'END
/* TableDDL - '+ @object_name +' - End */'

/*CREATE TABLE SCRIPT - END*/

IF(@generate_cdb_script_only=0) 
BEGIN
/*CREATE TIMESTAMP COL - START*/
IF(@add_timestamp_col=1)
BEGIN
PRINT '
/* TIMESTAMP COL FOR '+ @object_name +' - Start */
'
	IF NOT EXISTS(
		SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=@table_name AND COLUMN_NAME='last_modified'
		)
	BEGIN
		DECLARE @add_timestamp VARCHAR(512)='ALTER TABLE '+ @table_name +' ADD last_modified DATETIME CONSTRAINT DF_'+REPLACE(REPLACE(@table,'[',''),']','')+'_Last_Modified DEFAULT (GETDATE())'
		PRINT @add_timestamp
	END
PRINT '/* TIMESTAMP COL FOR '+ @object_name +' - End */'
END
/*CREATE TIMESTAMP COL - END*/

/*CREATE SHADOW TABLE SCRIPT - START*/
PRINT '
/* SHADOW TABLE FOR '+ @object_name +' - Start */'
PRINT 'IF OBJECT_ID('''+ substring(@object_name,1,len(@object_name)-1) +'_del]'+''') IS NULL'
PRINT 'BEGIN
'
SELECT @SQL = '	CREATE TABLE ' + substring(@object_name,1,len(@object_name)-1) +'_del]'+ CHAR(13) + '	(' + CHAR(13) + STUFF((
    SELECT CHAR(9) + ', [' + c.name + '] ' +
         UPPER(tp.name) + 
                CASE WHEN tp.name IN ('varchar', 'char', 'varbinary', 'binary', 'text')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('nvarchar', 'nchar', 'ntext')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length / 2 AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('datetime2', 'time2', 'datetimeoffset') 
                       THEN '(' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'decimal' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'NUMERIC' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                    ELSE ''
                END 
        + CHAR(13)
    FROM sys.columns c WITH (NOWAIT)
    JOIN sys.types tp WITH (NOWAIT) ON c.user_type_id = tp.user_type_id
    LEFT JOIN sys.default_constraints dc WITH (NOWAIT) ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
	INNER JOIN SYS.key_constraints AS K ON C.object_id=K.parent_object_id -- WHERE parent_object_id=OBJECT_ID('BUSDTA.F0004')
	INNER JOIN SYS.index_columns AS IC ON IC.index_id=K.unique_index_id AND IC.object_id=K.parent_object_id AND IC.column_id=C.column_id
    WHERE c.[object_id] = @object_id
    ORDER BY c.column_id	
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, CHAR(9) + ' ')
    + ISNULL((SELECT CHAR(9) +', last_modified DATETIME DEFAULT GETDATE()' +CHAR(13)+ '	, PRIMARY KEY (' + 
                    (SELECT STUFF((
                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                         FROM sys.index_columns ic WITH (NOWAIT)
                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
                             AND ic.[object_id] = k.parent_object_id 
                             AND ic.index_id = k.unique_index_id     
                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
            + ')' + CHAR(13)
            FROM sys.key_constraints k WITH (NOWAIT)
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '')+ '	)'  + CHAR(13)

PRINT @SQL
PRINT 'END'
PRINT '/* SHADOW TABLE FOR '+ @object_name +' - End */'
/*CREATE SHADOW TABLE SCRIPT - END*/

	EXEC [dbo].[USP_GetTRandShadow] @table
END
PRINT '*/'
END


GO



CREATE PROCEDURE [dbo].[uspCheckGlobalSync] 
(
	@DeviceID VARCHAR(100)
)
AS
BEGIN
	SELECT Sync=CASE WHEN COUNT(1)>0 THEN 1 ELSE 0 END
	FROM tblGlobalSync AS gs
	WHERE GlobalSyncID NOT IN(SELECT GlobalSyncID FROM tblGlobalSyncLog AS gsl WHERE gsl.DeviceID=@DeviceID)
END

GO


CREATE PROCEDURE [dbo].[uspDeltaRows] 
(
	@remote_id VARCHAR(100),
	@script_version VARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON;
/*BUSDTA.F0101*/
		SELECT ABMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F0101' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0101 AS F0101 
		WHERE F0101.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0101' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY ABMCU
	UNION ALL
/*BUSDTA.F40073*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F40073' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F40073 AS F40073
		WHERE F40073.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F40073' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0004*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0004' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0004 AS F0004
		WHERE F0004.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0004' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0005*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0005' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0005 AS F0005
		WHERE F0005.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0005' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0014*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0014' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0014 AS F0014 
		WHERE F0014.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0014' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0150*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0150' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0150 AS F0150
		WHERE F0150.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0150' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.M0140*/
		SELECT M0140.RMMCU AS RouteID, 'BUSDTA' AS CTSchema, 'M0140' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.M0140 AS M0140
		WHERE M0140.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='M0140' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY M0140.RMMCU
	UNION ALL
/*BUSDTA.F0006*/
		SELECT F0006.MCMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F0006' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0006 AS F0006
		WHERE F0006.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0006' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F0006.MCMCU
	UNION ALL
		--Pricing
		--Product
/*BUSDTA.F4106*/
		SELECT F4106.BPMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F4106' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F4106 AS F4106
		WHERE F4106.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F4106' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F4106.BPMCU
	UNION ALL
/*BUSDTA.F41001*/
		SELECT F41001.CIMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F41001' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F41001 AS F41001
		WHERE F41001.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F41001' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F41001.CIMCU
	UNION ALL 
/*BUSDTA.F4105*/
		SELECT F4105.COMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F4105' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F4105 AS F4105
		WHERE F4105.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F4105' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F4105.COMCU

		--Customer
	UNION ALL
/*BUSDTA.F0116*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0116' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0116 AS F0116 
		WHERE F0116.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0116' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0116*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F03012' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F03012 AS F03012
		WHERE F03012.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F03012' AND remote_id=@remote_id AND script_version=@script_version)
SET NOCOUNT OFF;
END



GO



CREATE PROCEDURE [dbo].[uspGetDeltaRows] 
(
	@DeviceID VARCHAR(100),
	@Script_Version VARCHAR(100)='SLE_RemoteDB'
)
AS
BEGIN
DECLARE @remote_id VARCHAR(100)=(SELECT TOP 1 Remote_Id FROM BUSDTA.Route_Device_Map WHERE Device_Id=@DeviceID);
DECLARE @DeltaStats TABLE (RouteID VARCHAR(100), CTSchema VARCHAR(128), CTTable VARCHAR(128), CTDeltaRows BIGINT NOT NULL, Syncdate DATETIME DEFAULT GETDATE()) 

INSERT INTO @DeltaStats(RouteID, CTSchema, CTTable, CTDeltaRows)
EXEC [dbo].[uspDeltaRows] @remote_id=@remote_id, @script_version=@script_version;
--SELECT * FROM @DeltaStats

	SELECT RouteID,rdm.Route_Id,  /*F56M0001.FFMCU,*/ ISNULL(ag.ApplicationGroup, 'Other Group')AS ApplicationGroup, 
		SUM(dt.CTDeltaRows) AS DeltaRows, 
		IsMandatorySync=CASE WHEN ag.IsMandatorySync=1 THEN 1 ELSE ISNULL(gt.IsMandatorySync,0) END
		FROM @DeltaStats AS dt
		LEFT JOIN tblGroupTables AS gt ON dt.CTSchema=gt.RefDBSchema AND dt.CTTable=gt.ConDBTable
		LEFT JOIN tblApplicationGroup AS ag ON ag.ApplicationGroupID=gt.ApplicationGroupID
		LEFT JOIN BUSDTA.F56M0001 AS F56M0001 ON F56M0001.FFMCU=dt.RouteID
		LEFT JOIN BUSDTA.Route_Device_Map AS rdm ON rdm.Route_Id=F56M0001.FFUSER AND rdm.Device_Id=@DeviceID
	WHERE 
		((RouteID IS NULL AND rdm.Route_Id IS NULL)OR (RouteID IS NOT NULL AND rdm.Route_Id IS NOT NULL))
	GROUP BY RouteID,rdm.Route_Id,  /*F56M0001.FFMCU,*/ ag.ApplicationGroup, ag.SortOrder, CASE WHEN ag.IsMandatorySync=1 THEN 1 ELSE ISNULL(gt.IsMandatorySync,0) END
	HAVING SUM(dt.CTDeltaRows)>0
	ORDER BY ISNULL(ag.SortOrder, 9999)/*and make other group last in the list*/
END
GO


CREATE PROCEDURE [dbo].[uspRDBScriptSchema] 
(
	@table_name SYSNAME
)
AS
BEGIN
/*CREATE TABLE SCRIPT - START*/
DECLARE @table SYSNAME

DECLARE 
      @object_name SYSNAME
    , @object_id INT

SELECT 
      @object_name = '[' + s.name + '].[' + o.name + ']'
	, @table = o.name
    , @object_id = o.[object_id]
FROM sys.objects o WITH (NOWAIT)
JOIN sys.schemas s WITH (NOWAIT) ON o.[schema_id] = s.[schema_id]
WHERE s.name + '.' + o.name = @table_name
    AND o.[type] = 'U'
    AND o.is_ms_shipped = 0

DECLARE @SQL NVARCHAR(MAX) = ''

PRINT '/*
/* '+ @object_name +' - begins */

/* TableDDL - '+ @object_name +' - Start */
IF OBJECT_ID('''+ @object_name +''') IS NULL'
PRINT 'BEGIN
'

;WITH 
fk_columns AS 
(
     SELECT 
          k.constraint_object_id
        , cname = c.name
        , rcname = rc.name
    FROM sys.foreign_key_columns k WITH (NOWAIT)
    JOIN sys.columns rc WITH (NOWAIT) ON rc.[object_id] = k.referenced_object_id AND rc.column_id = k.referenced_column_id 
    JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = k.parent_object_id AND c.column_id = k.parent_column_id
    WHERE k.parent_object_id = @object_id
)
SELECT @SQL = '	CREATE TABLE ' + @object_name + CHAR(13) + '	(' + CHAR(13) + STUFF((
    SELECT CHAR(9) + ', [' + c.name + '] ' + 
        CASE WHEN c.is_computed = 1
				THEN 'AS ' + cc.[definition] 
			WHEN (tp.name IN ('varchar','nvarchar') AND c.max_length = -1) OR (tp.name IN ('ntext','text'))
                THEN 'LONG VARCHAR'
			WHEN tp.name IN ('varbinary') AND c.max_length = -1
                THEN 'LONG BINARY'
			WHEN tp.name IN ('nchar') AND c.max_length != -1
                THEN 'NVARCHAR ('+ CASE WHEN SUBSTRING(tp.name,1,1)='n' THEN CAST(c.max_length/2 AS VARCHAR(5)) ELSE CAST(c.max_length AS VARCHAR(5)) END +')'
			WHEN tp.name IN ('image') 
                THEN 'LONG BINARY'
			WHEN tp.name IN ('datetime2') 
                THEN 'TIMESTAMP'
			WHEN tp.name IN ('int') 
                THEN 'INTEGER'
            ELSE UPPER(tp.name) + 
                CASE
					WHEN tp.name IN ('varchar', 'char', 'varbinary', 'binary', 'text')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN '' ELSE CAST(c.max_length AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('nvarchar', 'nchar', 'ntext')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN '' ELSE CAST(c.max_length / 2 AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('time2', 'datetimeoffset') 
                       THEN '(' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'decimal' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'NUMERIC' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                    ELSE ''
                END
			END +
                --CASE WHEN c.collation_name IS NOT NULL THEN ' COLLATE ' + c.collation_name ELSE '' END +
                CASE WHEN c.is_nullable = 1 THEN ' NULL' ELSE ' NOT NULL' END +
                CASE WHEN dc.[definition] IS NOT NULL THEN ' DEFAULT' + dc.[definition] ELSE '' END + 
                CASE WHEN ic.is_identity = 1 THEN ' autoincrement' ELSE '' END 
         + CHAR(13)
    FROM sys.columns c WITH (NOWAIT)
    JOIN sys.types tp WITH (NOWAIT) ON c.user_type_id = tp.user_type_id
    LEFT JOIN sys.computed_columns cc WITH (NOWAIT) ON c.[object_id] = cc.[object_id] AND c.column_id = cc.column_id
    LEFT JOIN sys.default_constraints dc WITH (NOWAIT) ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
    LEFT JOIN sys.identity_columns ic WITH (NOWAIT) ON c.is_identity = 1 AND c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
    WHERE c.[object_id] = @object_id
    ORDER BY c.column_id
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, CHAR(9) + ' ')
    + ISNULL((SELECT CHAR(9) + ', PRIMARY KEY (' + --' + k.name + '
                    (SELECT STUFF((
                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                         FROM sys.index_columns ic WITH (NOWAIT)
                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
                             AND ic.[object_id] = k.parent_object_id 
                             AND ic.index_id = k.unique_index_id     
                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
            + ')' + CHAR(13)
            FROM sys.key_constraints k WITH (NOWAIT)
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '') + '	)'  

PRINT @SQL

PRINT 'END
/* TableDDL - '+ @object_name +' - End */
*/'

/*CREATE TABLE SCRIPT - END*/

END


GO


