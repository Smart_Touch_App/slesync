 IF (SCHEMA_ID('BUSDTA') IS NULL) 
BEGIN
    EXEC ('CREATE SCHEMA [BUSDTA] AUTHORIZATION [dbo]')
END
/* [BUSDTA].[ApprovalCodeLog] - begins */

/* TableDDL - [BUSDTA].[ApprovalCodeLog] - Start */
IF OBJECT_ID('[BUSDTA].[ApprovalCodeLog]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ApprovalCodeLog]
	(
	  [LogID] NUMERIC(8,0) NOT NULL
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [ApprovalCode] NCHAR(8) NOT NULL
	, [RequestCode] NCHAR(8) NOT NULL
	, [Feature] NCHAR(50) NOT NULL
	, [Amount] NUMERIC(8,2) NOT NULL
	, [RequestedByUser] NUMERIC(8,0) NOT NULL
	, [RequestedForCustomer] NUMERIC(8,0) NOT NULL
	, [Payload] NVARCHAR(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_APPROVALCODELOG_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ApprovalCodeLog] PRIMARY KEY ([LogID] ASC, [RouteID] ASC)
	)

END
/* TableDDL - [BUSDTA].[ApprovalCodeLog] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[ApprovalCodeLog] - Start */
IF OBJECT_ID('[BUSDTA].[ApprovalCodeLog_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ApprovalCodeLog_del]
	(
	  [LogID] NUMERIC(8,0)
	, [RouteID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([LogID] ASC, [RouteID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ApprovalCodeLog] - End */
/* TRIGGERS FOR ApprovalCodeLog - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ApprovalCodeLog_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ApprovalCodeLog_ins
	ON BUSDTA.ApprovalCodeLog AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ApprovalCodeLog_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ApprovalCodeLog_del.LogID= inserted.LogID AND BUSDTA.ApprovalCodeLog_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ApprovalCodeLog_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ApprovalCodeLog_upd
	ON BUSDTA.ApprovalCodeLog AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ApprovalCodeLog
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ApprovalCodeLog.LogID= inserted.LogID AND BUSDTA.ApprovalCodeLog.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ApprovalCodeLog_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ApprovalCodeLog_dlt
	ON BUSDTA.ApprovalCodeLog AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ApprovalCodeLog_del (LogID, RouteID, last_modified )
	SELECT deleted.LogID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ApprovalCodeLog - END */
/* [BUSDTA].[Request_Authorization_Format] - begins */

/* TableDDL - [BUSDTA].[Request_Authorization_Format] - Start */
IF OBJECT_ID('[BUSDTA].[Request_Authorization_Format]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Request_Authorization_Format]
	(
	  [RequestFormatCode] NCHAR(1) NOT NULL
	, [AuthorizationFormatCode] NCHAR(1) NOT NULL
	, [Key1] NCHAR(1) NOT NULL
	, [Key2] NCHAR(1) NOT NULL
	, [Key3] NCHAR(1) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_REQUEST_AUTHORIZATION_FORMAT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Request_Authorization_Format] PRIMARY KEY ([RequestFormatCode] ASC, [AuthorizationFormatCode] ASC, [Key1] ASC, [Key2] ASC, [Key3] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Request_Authorization_Format] - End */

/* SHADOW TABLE FOR [BUSDTA].[Request_Authorization_Format] - Start */
IF OBJECT_ID('[BUSDTA].[Request_Authorization_Format_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Request_Authorization_Format_del]
	(
	  [RequestFormatCode] NCHAR(1)
	, [AuthorizationFormatCode] NCHAR(1)
	, [Key1] NCHAR(1)
	, [Key2] NCHAR(1)
	, [Key3] NCHAR(1)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RequestFormatCode] ASC, [AuthorizationFormatCode] ASC, [Key1] ASC, [Key2] ASC, [Key3] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Request_Authorization_Format] - End */
/* TRIGGERS FOR Request_Authorization_Format - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Request_Authorization_Format_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Request_Authorization_Format_ins
	ON BUSDTA.Request_Authorization_Format AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Request_Authorization_Format_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Request_Authorization_Format_del.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Request_Authorization_Format_del.Key1= inserted.Key1 AND BUSDTA.Request_Authorization_Format_del.Key2= inserted.Key2 AND BUSDTA.Request_Authorization_Format_del.Key3= inserted.Key3 AND BUSDTA.Request_Authorization_Format_del.RequestFormatCode= inserted.RequestFormatCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Request_Authorization_Format_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Request_Authorization_Format_upd
	ON BUSDTA.Request_Authorization_Format AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Request_Authorization_Format
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Request_Authorization_Format.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Request_Authorization_Format.Key1= inserted.Key1 AND BUSDTA.Request_Authorization_Format.Key2= inserted.Key2 AND BUSDTA.Request_Authorization_Format.Key3= inserted.Key3 AND BUSDTA.Request_Authorization_Format.RequestFormatCode= inserted.RequestFormatCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Request_Authorization_Format_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Request_Authorization_Format_dlt
	ON BUSDTA.Request_Authorization_Format AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Request_Authorization_Format_del (AuthorizationFormatCode, Key1, Key2, Key3, RequestFormatCode, last_modified )
	SELECT deleted.AuthorizationFormatCode, deleted.Key1, deleted.Key2, deleted.Key3, deleted.RequestFormatCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Request_Authorization_Format - END */
/* [BUSDTA].[Prospect_Quote_Header] - begins */

/* TableDDL - [BUSDTA].[Prospect_Quote_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Header]
	(
	  [ProspectQuoteId] NUMERIC(8,0) NOT NULL
	, [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(3,0) NOT NULL
	, [IsPrinted] NCHAR(1) NULL
	, [IsSampled] NCHAR(1) NULL
	, [SettlementId] NUMERIC(8,0) NOT NULL
	, [PickStatus] NCHAR(1) NULL
	, [IsMasterSetup] NCHAR(1) NULL
	, [AIAC05District] NCHAR(3) NULL
	, [AIAC05DistrictMst] NCHAR(3) NULL
	, [AIAC06Region] NCHAR(3) NULL
	, [AIAC06RegionMst] NCHAR(3) NULL
	, [AIAC07Reporting] NCHAR(3) NULL
	, [AIAC07ReportingMst] NCHAR(3) NULL
	, [AIAC08Chain] NCHAR(3) NULL
	, [AIAC08ChainMst] NCHAR(3) NULL
	, [AIAC09BrewmaticAgentCode] NCHAR(3) NULL
	, [AIAC09BrewmaticAgentCodeMst] NCHAR(3) NULL
	, [AIAC10NTR] NCHAR(3) NULL
	, [AIAC10NTRMst] NCHAR(3) NULL
	, [AIAC11CustomerTaxGrp] NCHAR(3) NULL
	, [AIAC11CustomerTaxGrpMst] NCHAR(3) NULL
	, [AIAC12CategoryCode12] NCHAR(3) NULL
	, [AIAC12CategoryCode12Mst] NCHAR(3) NULL
	, [AIAC13APCheckCode] NCHAR(3) NULL
	, [AIAC13APCheckCodeMst] NCHAR(3) NULL
	, [AIAC14CategoryCode14] NCHAR(3) NULL
	, [AIAC14CategoryCode14Mst] NCHAR(3) NULL
	, [AIAC15CategoryCode15] NCHAR(3) NULL
	, [AIAC15CategoryCode15Mst] NCHAR(3) NULL
	, [AIAC16CategoryCode16] NCHAR(3) NULL
	, [AIAC16CategoryCode16Mst] NCHAR(3) NULL
	, [AIAC17CategoryCode17] NCHAR(3) NULL
	, [AIAC17CategoryCode17Mst] NCHAR(3) NULL
	, [AIAC18CategoryCode18] NCHAR(3) NULL
	, [AIAC18CategoryCode18Mst] NCHAR(3) NULL
	, [AIAC22POSUpCharge] NCHAR(3) NULL
	, [AIAC22POSUpChargeMst] NCHAR(3) NULL
	, [AIAC23LiquidCoffee] NCHAR(3) NULL
	, [AIAC23LiquidCoffeeMst] NCHAR(3) NULL
	, [AIAC24PriceProtection] NCHAR(3) NULL
	, [AIAC24PriceProtectionMst] NCHAR(3) NULL
	, [AIAC27AlliedDiscount] NCHAR(3) NULL
	, [AIAC27AlliedDiscountMst] NCHAR(3) NULL
	, [AIAC28CoffeeVolume] NCHAR(3) NULL
	, [AIAC28CoffeeVolumeMst] NCHAR(3) NULL
	, [AIAC29EquipmentProgPts] NCHAR(3) NULL
	, [AIAC29EquipmentProgPtsMst] NCHAR(3) NULL
	, [AIAC30SpecialCCP] NCHAR(3) NULL
	, [AIAC30SpecialCCPMst] NCHAR(3) NULL
	, [PriceDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_QUOTE_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Quote_Header] PRIMARY KEY ([ProspectQuoteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Quote_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Header_del]
	(
	  [ProspectQuoteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectQuoteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Header] - End */
/* TRIGGERS FOR Prospect_Quote_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Quote_Header_ins
	ON BUSDTA.Prospect_Quote_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Quote_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Quote_Header_del.ProspectQuoteId= inserted.ProspectQuoteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Header_upd
	ON BUSDTA.Prospect_Quote_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Quote_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Quote_Header.ProspectQuoteId= inserted.ProspectQuoteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Header_dlt
	ON BUSDTA.Prospect_Quote_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Quote_Header_del (ProspectQuoteId, last_modified )
	SELECT deleted.ProspectQuoteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Quote_Header - END */
/* [BUSDTA].[Prospect_Quote_Detail] - begins */

/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Detail]
	(
	  [ProspectQuoteId] NUMERIC(8,0) NOT NULL
	, [ProspectQuoteDetailId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [QuoteQty] NUMERIC(4,0) NOT NULL
	, [SampleQty] NUMERIC(4,0) NOT NULL
	, [SampleUM] NCHAR(5) NULL
	, [PricingAmt] NUMERIC(8,4) NULL
	, [PricingUM] NCHAR(2) NULL
	, [TransactionAmt] NUMERIC(8,4) NULL
	, [TransactionUM] NCHAR(2) NULL
	, [OtherAmt] NUMERIC(8,4) NULL
	, [OtherUM] NCHAR(2) NULL
	, [CompetitorName] NVARCHAR(50) NOT NULL
	, [UnitPriceAmt] NUMERIC(8,4) NULL
	, [UnitPriceUM] NCHAR(2) NULL
	, [AvailableQty] NUMERIC(4,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_QUOTE_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Quote_Detail] PRIMARY KEY ([ProspectQuoteId] ASC, [ProspectQuoteDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Detail_del]
	(
	  [ProspectQuoteId] NUMERIC(8,0)
	, [ProspectQuoteDetailId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectQuoteId] ASC, [ProspectQuoteDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Detail] - End */
/* TRIGGERS FOR Prospect_Quote_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Quote_Detail_ins
	ON BUSDTA.Prospect_Quote_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Quote_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Quote_Detail_del.ProspectQuoteDetailId= inserted.ProspectQuoteDetailId AND BUSDTA.Prospect_Quote_Detail_del.ProspectQuoteId= inserted.ProspectQuoteId AND BUSDTA.Prospect_Quote_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Detail_upd
	ON BUSDTA.Prospect_Quote_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Quote_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Quote_Detail.ProspectQuoteDetailId= inserted.ProspectQuoteDetailId AND BUSDTA.Prospect_Quote_Detail.ProspectQuoteId= inserted.ProspectQuoteId AND BUSDTA.Prospect_Quote_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Detail_dlt
	ON BUSDTA.Prospect_Quote_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Quote_Detail_del (ProspectQuoteDetailId, ProspectQuoteId, RouteId, last_modified )
	SELECT deleted.ProspectQuoteDetailId, deleted.ProspectQuoteId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Quote_Detail - END */
/* [BUSDTA].[Prospect_Note] - begins */

/* TableDDL - [BUSDTA].[Prospect_Note] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Note]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Note]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [ProspectNoteId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ProspectNoteType] NUMERIC(3,0) NOT NULL
	, [ProspectNoteDetail] NCHAR(1000) NULL
	, [IsDefault] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_NOTE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Note] PRIMARY KEY ([ProspectId] ASC, [ProspectNoteId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Note] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Note] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Note_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Note_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, [ProspectNoteId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC, [ProspectNoteId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Note] - End */
/* TRIGGERS FOR Prospect_Note - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Note_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Note_ins
	ON BUSDTA.Prospect_Note AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Note_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Note_del.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Note_del.ProspectNoteId= inserted.ProspectNoteId AND BUSDTA.Prospect_Note_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Note_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Note_upd
	ON BUSDTA.Prospect_Note AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Note
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Note.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Note.ProspectNoteId= inserted.ProspectNoteId AND BUSDTA.Prospect_Note.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Note_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Note_dlt
	ON BUSDTA.Prospect_Note AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Note_del (ProspectId, ProspectNoteId, RouteId, last_modified )
	SELECT deleted.ProspectId, deleted.ProspectNoteId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Note - END */
/* [BUSDTA].[Prospect_Master] - begins */

/* TableDDL - [BUSDTA].[Prospect_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Master]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ProspectName] NCHAR(100) NULL
	, [AcctSegmentType] NUMERIC(3,0) NULL
	, [BuyingGroup] NCHAR(50) NULL
	, [LocationCount] NCHAR(10) NULL
	, [CategoryCode01] NCHAR(3) NULL
	, [OperatingUnit] NCHAR(3) NULL
	, [Branch] NCHAR(3) NULL
	, [District] NCHAR(3) NULL
	, [Region] NCHAR(3) NULL
	, [Reporting] NCHAR(3) NULL
	, [Chain] NCHAR(3) NULL
	, [BrewmaticAgentCode] NCHAR(3) NULL
	, [NTR] NCHAR(3) NULL
	, [ProspectTaxGrp] NCHAR(3) NULL
	, [CategoryCode12] NCHAR(3) NULL
	, [APCheckCode] NCHAR(3) NULL
	, [CategoryCode14] NCHAR(3) NULL
	, [CategoryCode15] NCHAR(3) NULL
	, [CategoryCode16] NCHAR(3) NULL
	, [CategoryCode17] NCHAR(3) NULL
	, [CategoryCode18] NCHAR(3) NULL
	, [CategoryCode19] NCHAR(3) NULL
	, [CategoryCode20] NCHAR(3) NULL
	, [CategoryCode21] NCHAR(3) NULL
	, [CategoryCode22] NCHAR(3) NULL
	, [SpecialEquipment] NCHAR(3) NULL
	, [CAProtection] NCHAR(3) NULL
	, [ProspectGroup] NCHAR(3) NULL
	, [SPCommisFBType] NCHAR(3) NULL
	, [AlliedDiscount] NCHAR(3) NULL
	, [CoffeeVolume] NCHAR(3) NULL
	, [EquipmentProgPts] NCHAR(3) NULL
	, [LiquidCoffee] NCHAR(3) NULL
	, [PriceProtection] NCHAR(3) NULL
	, [POSUpCharge] NCHAR(3) NULL
	, [SpecialCCP] NCHAR(3) NULL
	, [TaxGroup] NCHAR(3) NULL
	, [IsPricesetup] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Master] PRIMARY KEY ([ProspectId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Master_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Master] - End */
/* TRIGGERS FOR Prospect_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Master_ins
	ON BUSDTA.Prospect_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Master_del.ProspectId= inserted.ProspectId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Master_upd
	ON BUSDTA.Prospect_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Master.ProspectId= inserted.ProspectId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Master_dlt
	ON BUSDTA.Prospect_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Master_del (ProspectId, last_modified )
	SELECT deleted.ProspectId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Master - END */

/* [BUSDTA].[Feature_Code_Mapping] - begins */

/* TableDDL - [BUSDTA].[Feature_Code_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[Feature_Code_Mapping]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Feature_Code_Mapping]
	(
	  [RequestCode] NCHAR(1) NOT NULL
	, [AuthorizationFormatCode] NCHAR(1) NOT NULL
	, [Feature] NCHAR(100) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [Description] NCHAR(150) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_FEATURE_CODE_MAPPING_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Feature_Code_Mapping] PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC, [Feature] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Feature_Code_Mapping] - End */

/* SHADOW TABLE FOR [BUSDTA].[Feature_Code_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[Feature_Code_Mapping_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Feature_Code_Mapping_del]
	(
	  [RequestCode] NCHAR(1)
	, [AuthorizationFormatCode] NCHAR(1)
	, [Feature] NCHAR(100)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC, [Feature] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Feature_Code_Mapping] - End */
/* TRIGGERS FOR Feature_Code_Mapping - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Feature_Code_Mapping_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Feature_Code_Mapping_ins
	ON BUSDTA.Feature_Code_Mapping AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Feature_Code_Mapping_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Feature_Code_Mapping_del.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Feature_Code_Mapping_del.Feature= inserted.Feature AND BUSDTA.Feature_Code_Mapping_del.RequestCode= inserted.RequestCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Feature_Code_Mapping_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Feature_Code_Mapping_upd
	ON BUSDTA.Feature_Code_Mapping AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Feature_Code_Mapping
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Feature_Code_Mapping.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Feature_Code_Mapping.Feature= inserted.Feature AND BUSDTA.Feature_Code_Mapping.RequestCode= inserted.RequestCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Feature_Code_Mapping_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Feature_Code_Mapping_dlt
	ON BUSDTA.Feature_Code_Mapping AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Feature_Code_Mapping_del (AuthorizationFormatCode, Feature, RequestCode, last_modified )
	SELECT deleted.AuthorizationFormatCode, deleted.Feature, deleted.RequestCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Feature_Code_Mapping - END */


/* [BUSDTA].[Customer_Quote_Detail] - begins */

/* TableDDL - [BUSDTA].[Customer_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Quote_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Quote_Detail]
	(
	  [CustomerQuoteId] NUMERIC(8,0) NOT NULL
	, [CustomerQuoteDetailId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [QuoteQty] NUMERIC(4,0) NOT NULL
	, [TransactionUM] NCHAR(2) NULL
	, [PricingAmt] NUMERIC(8,4) NULL
	, [PricingUM] NCHAR(2) NULL
	, [TransactionAmt] NUMERIC(8,4) NULL
	, [OtherAmt] NUMERIC(8,4) NULL
	, [OtherUM] NCHAR(2) NULL
	, [CompetitorName] NVARCHAR(50) NOT NULL
	, [UnitPriceAmt] NUMERIC(8,4) NULL
	, [UnitPriceUM] NCHAR(2) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CUSTOMER_QUOTE_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Customer_Quote_Detail] PRIMARY KEY ([CustomerQuoteId] ASC, [CustomerQuoteDetailId] ASC, [RouteId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Customer_Quote_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Customer_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Quote_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Quote_Detail_del]
	(
	  [CustomerQuoteId] NUMERIC(8,0)
	, [CustomerQuoteDetailId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerQuoteId] ASC, [CustomerQuoteDetailId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Customer_Quote_Detail] - End */
/* TRIGGERS FOR Customer_Quote_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Customer_Quote_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Customer_Quote_Detail_ins
	ON BUSDTA.Customer_Quote_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Customer_Quote_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Customer_Quote_Detail_del.CustomerQuoteDetailId= inserted.CustomerQuoteDetailId AND BUSDTA.Customer_Quote_Detail_del.CustomerQuoteId= inserted.CustomerQuoteId AND BUSDTA.Customer_Quote_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Customer_Quote_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Quote_Detail_upd
	ON BUSDTA.Customer_Quote_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Customer_Quote_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Customer_Quote_Detail.CustomerQuoteDetailId= inserted.CustomerQuoteDetailId AND BUSDTA.Customer_Quote_Detail.CustomerQuoteId= inserted.CustomerQuoteId AND BUSDTA.Customer_Quote_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Customer_Quote_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Quote_Detail_dlt
	ON BUSDTA.Customer_Quote_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Customer_Quote_Detail_del (CustomerQuoteDetailId, CustomerQuoteId, RouteId, last_modified )
	SELECT deleted.CustomerQuoteDetailId, deleted.CustomerQuoteId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Customer_Quote_Detail - END */


/* [BUSDTA].[Amount_Range_Index] - begins */

/* TableDDL - [BUSDTA].[Amount_Range_Index] - Start */
IF OBJECT_ID('[BUSDTA].[Amount_Range_Index]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Amount_Range_Index]
	(
	  [RequestCode] NCHAR(1) NOT NULL
	, [AuthorizationFormatCode] NCHAR(1) NOT NULL
	, [RangeStart] NUMERIC(8,2) NOT NULL
	, [RangeEnd] NUMERIC(8,2) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_AMOUNT_RANGE_INDEX_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Amount_Range_Index] PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC)
	)

END
/* TableDDL - [BUSDTA].[Amount_Range_Index] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[Amount_Range_Index] - Start */
IF OBJECT_ID('[BUSDTA].[Amount_Range_Index_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Amount_Range_Index_del]
	(
	  [RequestCode] NCHAR(1)
	, [AuthorizationFormatCode] NCHAR(1)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Amount_Range_Index] - End */
/* TRIGGERS FOR Amount_Range_Index - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Amount_Range_Index_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Amount_Range_Index_ins
	ON BUSDTA.Amount_Range_Index AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Amount_Range_Index_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Amount_Range_Index_del.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Amount_Range_Index_del.RequestCode= inserted.RequestCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Amount_Range_Index_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Amount_Range_Index_upd
	ON BUSDTA.Amount_Range_Index AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Amount_Range_Index
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Amount_Range_Index.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Amount_Range_Index.RequestCode= inserted.RequestCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Amount_Range_Index_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Amount_Range_Index_dlt
	ON BUSDTA.Amount_Range_Index AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Amount_Range_Index_del (AuthorizationFormatCode, RequestCode, last_modified )
	SELECT deleted.AuthorizationFormatCode, deleted.RequestCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Amount_Range_Index - END */

/* [BUSDTA].[Customer_Quote_Header] - begins */

/* TableDDL - [BUSDTA].[Customer_Quote_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Quote_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Quote_Header]
	(
	  [CustomerQuoteId] NUMERIC(8,0) NOT NULL
	, [CustomerId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(3,0) NOT NULL
	, [BillToId] NUMERIC(8,0) NOT NULL
	, [ParentId] NUMERIC(8,0) NOT NULL
	, [IsPrinted] NCHAR(1) NULL
	, [SettlementId] NUMERIC(8,0) NOT NULL
	, [IsSampled] NCHAR(1) NULL
	, [PickStatus] NCHAR(1) NULL
	, [IsMasterSetup] NCHAR(1) NULL
	, [AIAC05District] NCHAR(3) NULL
	, [AIAC05DistrictMst] NCHAR(3) NULL
	, [AIAC06Region] NCHAR(3) NULL
	, [AIAC06RegionMst] NCHAR(3) NULL
	, [AIAC07Reporting] NCHAR(3) NULL
	, [AIAC07ReportingMst] NCHAR(3) NULL
	, [AIAC08Chain] NCHAR(3) NULL
	, [AIAC08ChainMst] NCHAR(3) NULL
	, [AIAC09BrewmaticAgentCode] NCHAR(3) NULL
	, [AIAC09BrewmaticAgentCodeMst] NCHAR(3) NULL
	, [AIAC10NTR] NCHAR(3) NULL
	, [AIAC10NTRMst] NCHAR(3) NULL
	, [AIAC11CustomerTaxGrp] NCHAR(3) NULL
	, [AIAC11CustomerTaxGrpMst] NCHAR(3) NULL
	, [AIAC12CategoryCode12] NCHAR(3) NULL
	, [AIAC12CategoryCode12Mst] NCHAR(3) NULL
	, [AIAC13APCheckCode] NCHAR(3) NULL
	, [AIAC13APCheckCodeMst] NCHAR(3) NULL
	, [AIAC14CategoryCode14] NCHAR(3) NULL
	, [AIAC14CategoryCode14Mst] NCHAR(3) NULL
	, [AIAC15CategoryCode15] NCHAR(3) NULL
	, [AIAC15CategoryCode15Mst] NCHAR(3) NULL
	, [AIAC16CategoryCode16] NCHAR(3) NULL
	, [AIAC16CategoryCode16Mst] NCHAR(3) NULL
	, [AIAC17CategoryCode17] NCHAR(3) NULL
	, [AIAC17CategoryCode17Mst] NCHAR(3) NULL
	, [AIAC18CategoryCode18] NCHAR(3) NULL
	, [AIAC18CategoryCode18Mst] NCHAR(3) NULL
	, [AIAC22POSUpCharge] NCHAR(3) NULL
	, [AIAC22POSUpChargeMst] NCHAR(3) NULL
	, [AIAC23LiquidCoffee] NCHAR(3) NULL
	, [AIAC23LiquidCoffeeMst] NCHAR(3) NULL
	, [AIAC24PriceProtection] NCHAR(3) NULL
	, [AIAC24PriceProtectionMst] NCHAR(3) NULL
	, [AIAC27AlliedDiscount] NCHAR(3) NULL
	, [AIAC27AlliedDiscountMst] NCHAR(3) NULL
	, [AIAC28CoffeeVolume] NCHAR(3) NULL
	, [AIAC28CoffeeVolumeMst] NCHAR(3) NULL
	, [AIAC29EquipmentProgPts] NCHAR(3) NULL
	, [AIAC29EquipmentProgPtsMst] NCHAR(3) NULL
	, [AIAC30SpecialCCP] NCHAR(3) NULL
	, [AIAC30SpecialCCPMst] NCHAR(3) NULL
	, [PriceDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CUSTOMER_QUOTE_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Customer_Quote_Header] PRIMARY KEY ([CustomerQuoteId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Customer_Quote_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Customer_Quote_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Quote_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Quote_Header_del]
	(
	  [CustomerQuoteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerQuoteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Customer_Quote_Header] - End */
/* TRIGGERS FOR Customer_Quote_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Customer_Quote_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Customer_Quote_Header_ins
	ON BUSDTA.Customer_Quote_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Customer_Quote_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Customer_Quote_Header_del.CustomerQuoteId= inserted.CustomerQuoteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Customer_Quote_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Quote_Header_upd
	ON BUSDTA.Customer_Quote_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Customer_Quote_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Customer_Quote_Header.CustomerQuoteId= inserted.CustomerQuoteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Customer_Quote_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Quote_Header_dlt
	ON BUSDTA.Customer_Quote_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Customer_Quote_Header_del (CustomerQuoteId, last_modified )
	SELECT deleted.CustomerQuoteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Customer_Quote_Header - END */


/* [BUSDTA].[F0004] - begins */

/* TableDDL - [BUSDTA].[F0004] - Start */
IF OBJECT_ID('[BUSDTA].[F0004]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0004]
	(
	  [DTSY] NCHAR(4) NOT NULL
	, [DTRT] NCHAR(2) NOT NULL
	, [DTDL01] NCHAR(30) NULL
	, [DTCDL] FLOAT NULL
	, [DTLN2] NCHAR(1) NULL
	, [DTCNUM] NCHAR(1) NULL
	, [DTYN] BIT NULL CONSTRAINT DF_F0004_DTYN DEFAULT((0))
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0004_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0004] PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0004] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0004] - Start */
IF OBJECT_ID('[BUSDTA].[F0004_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0004_del]
	(
	  [DTSY] NCHAR(4)
	, [DTRT] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0004] - End */
/* TRIGGERS FOR F0004 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0004_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0004_ins
	ON BUSDTA.F0004 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0004_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0004_del.DTRT= inserted.DTRT AND BUSDTA.F0004_del.DTSY= inserted.DTSY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0004_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0004_upd
	ON BUSDTA.F0004 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0004
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0004.DTRT= inserted.DTRT AND BUSDTA.F0004.DTSY= inserted.DTSY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0004_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0004_dlt
	ON BUSDTA.F0004 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0004_del (DTRT, DTSY, last_modified )
	SELECT deleted.DTRT, deleted.DTSY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0004 - END */


/* [BUSDTA].[F0005] - begins */

/* TableDDL - [BUSDTA].[F0005] - Start */
IF OBJECT_ID('[BUSDTA].[F0005]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0005]
	(
	  [DRSY] NCHAR(4) NOT NULL
	, [DRRT] NCHAR(2) NOT NULL
	, [DRKY] NCHAR(10) NOT NULL
	, [DRDL01] NCHAR(30) NULL
	, [DRDL02] NCHAR(30) NULL
	, [DRSPHD] NCHAR(10) NULL
	, [DRHRDC] NCHAR(1) NULL
	, [DRYN] BIT NULL CONSTRAINT DF_F0005_DRYN DEFAULT((1))
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0005_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0005] PRIMARY KEY ([DRSY] ASC, [DRRT] ASC, [DRKY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0005] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0005] - Start */
IF OBJECT_ID('[BUSDTA].[F0005_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0005_del]
	(
	  [DRSY] NCHAR(4)
	, [DRRT] NCHAR(2)
	, [DRKY] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DRSY] ASC, [DRRT] ASC, [DRKY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0005] - End */
/* TRIGGERS FOR F0005 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0005_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0005_ins
	ON BUSDTA.F0005 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0005_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0005_del.DRKY= inserted.DRKY AND BUSDTA.F0005_del.DRRT= inserted.DRRT AND BUSDTA.F0005_del.DRSY= inserted.DRSY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0005_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0005_upd
	ON BUSDTA.F0005 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0005
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0005.DRKY= inserted.DRKY AND BUSDTA.F0005.DRRT= inserted.DRRT AND BUSDTA.F0005.DRSY= inserted.DRSY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0005_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0005_dlt
	ON BUSDTA.F0005 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0005_del (DRKY, DRRT, DRSY, last_modified )
	SELECT deleted.DRKY, deleted.DRRT, deleted.DRSY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0005 - END */


/* [BUSDTA].[F0006] - begins */

/* TableDDL - [BUSDTA].[F0006] - Start */
IF OBJECT_ID('[BUSDTA].[F0006]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0006]
	(
	  [MCMCU] NCHAR(12) NOT NULL
	, [MCSTYL] NCHAR(2) NULL
	, [MCLDM] NCHAR(1) NULL
	, [MCCO] NCHAR(5) NULL
	, [MCAN8] FLOAT NULL
	, [MCDL01] NCHAR(30) NULL
	, [MCRP01] NCHAR(3) NULL
	, [MCRP02] NCHAR(3) NULL
	, [MCRP03] NCHAR(3) NULL
	, [MCRP04] NCHAR(3) NULL
	, [MCRP05] NCHAR(3) NULL
	, [MCRP06] NCHAR(3) NULL
	, [MCRP07] NCHAR(3) NULL
	, [MCRP08] NCHAR(3) NULL
	, [MCRP09] NCHAR(3) NULL
	, [MCRP10] NCHAR(3) NULL
	, [MCRP11] NCHAR(3) NULL
	, [MCRP12] NCHAR(3) NULL
	, [MCRP13] NCHAR(3) NULL
	, [MCRP14] NCHAR(3) NULL
	, [MCRP15] NCHAR(3) NULL
	, [MCRP16] NCHAR(3) NULL
	, [MCRP17] NCHAR(3) NULL
	, [MCRP18] NCHAR(3) NULL
	, [MCRP19] NCHAR(3) NULL
	, [MCRP20] NCHAR(3) NULL
	, [MCRP21] NCHAR(10) NULL
	, [MCRP22] NCHAR(10) NULL
	, [MCRP23] NCHAR(10) NULL
	, [MCRP24] NCHAR(10) NULL
	, [MCRP25] NCHAR(10) NULL
	, [MCRP26] NCHAR(10) NULL
	, [MCRP27] NCHAR(10) NULL
	, [MCRP28] NCHAR(10) NULL
	, [MCRP29] NCHAR(10) NULL
	, [MCRP30] NCHAR(10) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0006_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0006] PRIMARY KEY ([MCMCU] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0006] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0006] - Start */
IF OBJECT_ID('[BUSDTA].[F0006_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0006_del]
	(
	  [MCMCU] NCHAR(12)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MCMCU] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0006] - End */
/* TRIGGERS FOR F0006 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0006_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0006_ins
	ON BUSDTA.F0006 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0006_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0006_del.MCMCU= inserted.MCMCU
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0006_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0006_upd
	ON BUSDTA.F0006 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0006
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0006.MCMCU= inserted.MCMCU');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0006_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0006_dlt
	ON BUSDTA.F0006 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0006_del (MCMCU, last_modified )
	SELECT deleted.MCMCU, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0006 - END */


/* [BUSDTA].[Customer_Route_Map] - begins */

/* TableDDL - [BUSDTA].[Customer_Route_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Route_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Route_Map]
	(
	  [BranchNumber] NCHAR(12) NOT NULL
	, [CustomerShipToNumber] NUMERIC(8,0) NOT NULL
	, [RelationshipType] NCHAR(10) NOT NULL
	, [RelatedAddressBookNumber] NUMERIC(8,0) NULL
	, [IsActive] NCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_CUSTOMER_ROUTE_MAP_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_CUSTOMER_ROUTE_MAP_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_CUSTOMER_ROUTE_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Customer_Route_Map] PRIMARY KEY ([BranchNumber] ASC, [CustomerShipToNumber] ASC, [RelationshipType] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Customer_Route_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Customer_Route_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Route_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Route_Map_del]
	(
	  [BranchNumber] NCHAR(12)
	, [CustomerShipToNumber] NUMERIC(8,0)
	, [RelationshipType] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([BranchNumber] ASC, [CustomerShipToNumber] ASC, [RelationshipType] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Customer_Route_Map] - End */
/* TRIGGERS FOR Customer_Route_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Customer_Route_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Customer_Route_Map_ins
	ON BUSDTA.Customer_Route_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Customer_Route_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Customer_Route_Map_del.BranchNumber= inserted.BranchNumber AND BUSDTA.Customer_Route_Map_del.CustomerShipToNumber= inserted.CustomerShipToNumber AND BUSDTA.Customer_Route_Map_del.RelationshipType= inserted.RelationshipType
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Customer_Route_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Route_Map_upd
	ON BUSDTA.Customer_Route_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Customer_Route_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Customer_Route_Map.BranchNumber= inserted.BranchNumber AND BUSDTA.Customer_Route_Map.CustomerShipToNumber= inserted.CustomerShipToNumber AND BUSDTA.Customer_Route_Map.RelationshipType= inserted.RelationshipType');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Customer_Route_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Route_Map_dlt
	ON BUSDTA.Customer_Route_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Customer_Route_Map_del (BranchNumber, CustomerShipToNumber, RelationshipType, last_modified )
	SELECT deleted.BranchNumber, deleted.CustomerShipToNumber, deleted.RelationshipType, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Customer_Route_Map - END */

/* [BUSDTA].[Prospect_Address] - begins */

/* TableDDL - [BUSDTA].[Prospect_Address] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Address]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Address]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [AddressLine1] NCHAR(40) NULL
	, [AddressLine2] NCHAR(40) NULL
	, [AddressLine3] NCHAR(40) NULL
	, [AddressLine4] NCHAR(40) NULL
	, [CityName] NCHAR(25) NULL
	, [StateName] NCHAR(25) NULL
	, [ZipCode] NCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_ADDRESS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Address] PRIMARY KEY ([ProspectId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Address] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Address] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Address_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Address_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Address] - End */
/* TRIGGERS FOR Prospect_Address - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Address_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Address_ins
	ON BUSDTA.Prospect_Address AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Address_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Address_del.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Address_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Address_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Address_upd
	ON BUSDTA.Prospect_Address AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Address
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Address.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Address.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Address_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Address_dlt
	ON BUSDTA.Prospect_Address AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Address_del (ProspectId, RouteId, last_modified )
	SELECT deleted.ProspectId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Address - END */
/* [BUSDTA].[Prospect_Contact] - begins */

/* TableDDL - [BUSDTA].[Prospect_Contact] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Contact]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Contact]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [ProspectContactId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ShowOnDashboard] NCHAR(1) NULL
	, [FirstName] NCHAR(75) NULL
	, [MiddleName] NCHAR(75) NULL
	, [LastName] NCHAR(75) NULL
	, [Title] NVARCHAR(50) NULL
	, [IsActive] NCHAR(1) NOT NULL
	, [AreaCode1] NCHAR(6) NULL
	, [Phone1] NCHAR(20) NULL
	, [Extn1] NCHAR(8) NULL
	, [PhoneType1] NUMERIC(3,0) NULL
	, [IsDefaultPhone1] NCHAR(1) NULL
	, [AreaCode2] NCHAR(6) NULL
	, [Phone2] NCHAR(20) NULL
	, [Extn2] NCHAR(8) NULL
	, [PhoneType2] NUMERIC(3,0) NULL
	, [IsDefaultPhone2] NCHAR(1) NULL
	, [AreaCode3] NCHAR(6) NULL
	, [Phone3] NCHAR(20) NULL
	, [Extn3] NCHAR(8) NULL
	, [PhoneType3] NUMERIC(3,0) NULL
	, [IsDefaultPhone3] NCHAR(1) NULL
	, [AreaCode4] NCHAR(6) NULL
	, [Phone4] NCHAR(20) NULL
	, [Extn4] NCHAR(8) NULL
	, [PhoneType4] NUMERIC(3,0) NULL
	, [IsDefaultPhone4] NCHAR(1) NULL
	, [EmailID1] NCHAR(256) NULL
	, [EmailType1] NUMERIC(3,0) NULL
	, [IsDefaultEmail1] NCHAR(1) NULL
	, [EmailID2] NCHAR(256) NULL
	, [EmailType2] NUMERIC(3,0) NULL
	, [IsDefaultEmail2] NCHAR(1) NULL
	, [EmailID3] NCHAR(256) NULL
	, [EmailType3] NUMERIC(3,0) NULL
	, [IsDefaultEmail3] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_CONTACT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Contact] PRIMARY KEY ([ProspectId] ASC, [ProspectContactId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Contact] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Contact] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Contact_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Contact_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, [ProspectContactId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC, [ProspectContactId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Contact] - End */
/* TRIGGERS FOR Prospect_Contact - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Contact_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Contact_ins
	ON BUSDTA.Prospect_Contact AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Contact_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Contact_del.ProspectContactId= inserted.ProspectContactId AND BUSDTA.Prospect_Contact_del.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Contact_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Contact_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Contact_upd
	ON BUSDTA.Prospect_Contact AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Contact
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Contact.ProspectContactId= inserted.ProspectContactId AND BUSDTA.Prospect_Contact.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Contact.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Contact_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Contact_dlt
	ON BUSDTA.Prospect_Contact AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Contact_del (ProspectContactId, ProspectId, RouteId, last_modified )
	SELECT deleted.ProspectContactId, deleted.ProspectId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Contact - END */

/* [BUSDTA].[Category_Type] - begins */

/* TableDDL - [BUSDTA].[Category_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Category_Type]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Category_Type]
	(
	  [CategoryTypeID] NUMERIC(8,0) NOT NULL
	, [CategoryTypeCD] VARCHAR(5) NOT NULL
	, [CategoryCodeType] NVARCHAR(20) NULL
	, [CategoryTypeDESC] NVARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CATEGORY_TYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Category_Type] PRIMARY KEY ([CategoryTypeCD] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Category_Type] - End */

/* SHADOW TABLE FOR [BUSDTA].[Category_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Category_Type_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Category_Type_del]
	(
	  [CategoryTypeCD] VARCHAR(5)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CategoryTypeCD] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Category_Type] - End */
/* TRIGGERS FOR Category_Type - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Category_Type_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Category_Type_ins
	ON BUSDTA.Category_Type AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Category_Type_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Category_Type_del.CategoryTypeCD= inserted.CategoryTypeCD
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Category_Type_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Category_Type_upd
	ON BUSDTA.Category_Type AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Category_Type
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Category_Type.CategoryTypeCD= inserted.CategoryTypeCD');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Category_Type_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Category_Type_dlt
	ON BUSDTA.Category_Type AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Category_Type_del (CategoryTypeCD, last_modified )
	SELECT deleted.CategoryTypeCD, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Category_Type - END */


/* [BUSDTA].[F0014] - begins */

/* TableDDL - [BUSDTA].[F0014] - Start */
IF OBJECT_ID('[BUSDTA].[F0014]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0014]
	(
	  [PNPTC] NCHAR(3) NOT NULL
	, [PNPTD] NCHAR(30) NULL
	, [PNDCP] FLOAT NULL
	, [PNDCD] FLOAT NULL
	, [PNNDTP] FLOAT NULL
	, [PNNSP] FLOAT NULL
	, [PNDTPA] FLOAT NULL
	, [PNPXDM] FLOAT NULL
	, [PNPXDD] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0014_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0014] PRIMARY KEY ([PNPTC] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0014] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0014] - Start */
IF OBJECT_ID('[BUSDTA].[F0014_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0014_del]
	(
	  [PNPTC] NCHAR(3)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PNPTC] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0014] - End */
/* TRIGGERS FOR F0014 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0014_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0014_ins
	ON BUSDTA.F0014 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0014_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0014_del.PNPTC= inserted.PNPTC
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0014_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0014_upd
	ON BUSDTA.F0014 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0014
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0014.PNPTC= inserted.PNPTC');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0014_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0014_dlt
	ON BUSDTA.F0014 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0014_del (PNPTC, last_modified )
	SELECT deleted.PNPTC, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0014 - END */


/* [BUSDTA].[F0101] - begins */

/* TableDDL - [BUSDTA].[F0101] - Start */
IF OBJECT_ID('[BUSDTA].[F0101]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0101]
	(
	  [ABAN8] NUMERIC(8,0) NOT NULL
	, [ABALKY] NCHAR(20) NULL
	, [ABTAX] NCHAR(20) NULL
	, [ABALPH] NCHAR(40) NULL
	, [ABMCU] NCHAR(12) NULL
	, [ABSIC] NCHAR(10) NULL
	, [ABLNGP] NCHAR(2) NULL
	, [ABAT1] NCHAR(3) NULL
	, [ABCM] NCHAR(2) NULL
	, [ABTAXC] NCHAR(1) NULL
	, [ABAT2] NCHAR(1) NULL
	, [ABAN81] FLOAT NULL
	, [ABAN82] FLOAT NULL
	, [ABAN83] FLOAT NULL
	, [ABAN84] FLOAT NULL
	, [ABAN86] FLOAT NULL
	, [ABAN85] FLOAT NULL
	, [ABAC01] NCHAR(3) NULL
	, [ABAC02] NCHAR(3) NULL
	, [ABAC03] NCHAR(3) NULL
	, [ABAC04] NCHAR(3) NULL
	, [ABAC05] NCHAR(3) NULL
	, [ABAC06] NCHAR(3) NULL
	, [ABAC07] NCHAR(3) NULL
	, [ABAC08] NCHAR(3) NULL
	, [ABAC09] NCHAR(3) NULL
	, [ABAC10] NCHAR(3) NULL
	, [ABAC11] NCHAR(3) NULL
	, [ABAC12] NCHAR(3) NULL
	, [ABAC13] NCHAR(3) NULL
	, [ABAC14] NCHAR(3) NULL
	, [ABAC15] NCHAR(3) NULL
	, [ABAC16] NCHAR(3) NULL
	, [ABAC17] NCHAR(3) NULL
	, [ABAC18] NCHAR(3) NULL
	, [ABAC19] NCHAR(3) NULL
	, [ABAC20] NCHAR(3) NULL
	, [ABAC21] NCHAR(3) NULL
	, [ABAC22] NCHAR(3) NULL
	, [ABAC23] NCHAR(3) NULL
	, [ABAC24] NCHAR(3) NULL
	, [ABAC25] NCHAR(3) NULL
	, [ABAC26] NCHAR(3) NULL
	, [ABAC27] NCHAR(3) NULL
	, [ABAC28] NCHAR(3) NULL
	, [ABAC29] NCHAR(3) NULL
	, [ABAC30] NCHAR(3) NULL
	, [ABRMK] NCHAR(30) NULL
	, [ABTXCT] NCHAR(20) NULL
	, [ABTX2] NCHAR(20) NULL
	, [ABALP1] NCHAR(40) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0101_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0101] PRIMARY KEY ([ABAN8] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0101] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0101] - Start */
IF OBJECT_ID('[BUSDTA].[F0101_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0101_del]
	(
	  [ABAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ABAN8] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0101] - End */
/* TRIGGERS FOR F0101 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0101_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0101_ins
	ON BUSDTA.F0101 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0101_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0101_del.ABAN8= inserted.ABAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0101_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0101_upd
	ON BUSDTA.F0101 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0101
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0101.ABAN8= inserted.ABAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0101_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0101_dlt
	ON BUSDTA.F0101 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0101_del (ABAN8, last_modified )
	SELECT deleted.ABAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0101 - END */


/* [BUSDTA].[F0111] - begins */

/* TableDDL - [BUSDTA].[F0111] - Start */
IF OBJECT_ID('[BUSDTA].[F0111]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0111]
	(
	  [WWAN8] FLOAT NOT NULL
	, [WWIDLN] FLOAT NOT NULL
	, [WWDSS5] FLOAT NULL
	, [WWMLNM] NCHAR(40) NULL
	, [WWATTL] NCHAR(40) NULL
	, [WWREM1] NCHAR(40) NULL
	, [WWSLNM] NCHAR(40) NULL
	, [WWALPH] NCHAR(40) NULL
	, [WWDC] NCHAR(40) NULL
	, [WWGNNM] NCHAR(25) NULL
	, [WWMDNM] NCHAR(25) NULL
	, [WWSRNM] NCHAR(25) NULL
	, [WWTYC] NCHAR(1) NULL
	, [WWW001] NCHAR(3) NULL
	, [WWW002] NCHAR(3) NULL
	, [WWW003] NCHAR(3) NULL
	, [WWW004] NCHAR(3) NULL
	, [WWW005] NCHAR(3) NULL
	, [WWW006] NCHAR(3) NULL
	, [WWW007] NCHAR(3) NULL
	, [WWW008] NCHAR(3) NULL
	, [WWW009] NCHAR(3) NULL
	, [WWW010] NCHAR(3) NULL
	, [WWMLN1] NCHAR(40) NULL
	, [WWALP1] NCHAR(40) NULL
	, [WWUSER] NCHAR(10) NULL
	, [WWPID] NCHAR(10) NULL
	, [WWUPMJ] NUMERIC(18,0) NULL
	, [WWJOBN] NCHAR(10) NULL
	, [WWUPMT] FLOAT NULL
	, [WWNTYP] NCHAR(3) NULL
	, [WWNICK] NCHAR(40) NULL
	, [WWGEND] NCHAR(1) NULL
	, [WWDDATE] FLOAT NULL
	, [WWDMON] FLOAT NULL
	, [WWDYR] FLOAT NULL
	, [WWWN001] NCHAR(3) NULL
	, [WWWN002] NCHAR(3) NULL
	, [WWWN003] NCHAR(3) NULL
	, [WWWN004] NCHAR(3) NULL
	, [WWWN005] NCHAR(3) NULL
	, [WWWN006] NCHAR(3) NULL
	, [WWWN007] NCHAR(3) NULL
	, [WWWN008] NCHAR(3) NULL
	, [WWWN009] NCHAR(3) NULL
	, [WWWN010] NCHAR(3) NULL
	, [WWFUCO] NCHAR(10) NULL
	, [WWPCM] NCHAR(10) NULL
	, [WWPCF] NCHAR(3) NULL
	, [WWACTIN] NCHAR(1) NULL
	, [WWCFRGUID] NCHAR(36) NULL
	, [WWSYNCS] FLOAT NULL
	, [WWCAAD] FLOAT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, CONSTRAINT [PK_F0111] PRIMARY KEY ([WWAN8] ASC, [WWIDLN] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0111] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0111] - Start */
IF OBJECT_ID('[BUSDTA].[F0111_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0111_del]
	(
	  [WWAN8] FLOAT
	, [WWIDLN] FLOAT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([WWAN8] ASC, [WWIDLN] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0111] - End */
/* TRIGGERS FOR F0111 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0111_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0111_ins
	ON BUSDTA.F0111 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0111_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0111_del.WWAN8= inserted.WWAN8 AND BUSDTA.F0111_del.WWIDLN= inserted.WWIDLN
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0111_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0111_upd
	ON BUSDTA.F0111 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0111
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0111.WWAN8= inserted.WWAN8 AND BUSDTA.F0111.WWIDLN= inserted.WWIDLN');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0111_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0111_dlt
	ON BUSDTA.F0111 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0111_del (WWAN8, WWIDLN, last_modified )
	SELECT deleted.WWAN8, deleted.WWIDLN, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0111 - END */


/* [BUSDTA].[F0116] - begins */

/* TableDDL - [BUSDTA].[F0116] - Start */
IF OBJECT_ID('[BUSDTA].[F0116]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0116]
	(
	  [ALAN8] NUMERIC(8,0) NOT NULL
	, [ALEFTB] NUMERIC(18,0) NOT NULL
	, [ALEFTF] NCHAR(1) NULL
	, [ALADD1] NCHAR(40) NULL
	, [ALADD2] NCHAR(40) NULL
	, [ALADD3] NCHAR(40) NULL
	, [ALADD4] NCHAR(40) NULL
	, [ALADDZ] NCHAR(12) NULL
	, [ALCTY1] NCHAR(25) NULL
	, [ALCOUN] NCHAR(25) NULL
	, [ALADDS] NCHAR(3) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0116_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0116] PRIMARY KEY ([ALAN8] ASC, [ALEFTB] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0116] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0116] - Start */
IF OBJECT_ID('[BUSDTA].[F0116_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0116_del]
	(
	  [ALAN8] NUMERIC(8,0)
	, [ALEFTB] NUMERIC(18,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ALAN8] ASC, [ALEFTB] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0116] - End */
/* TRIGGERS FOR F0116 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0116_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0116_ins
	ON BUSDTA.F0116 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0116_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0116_del.ALAN8= inserted.ALAN8 AND BUSDTA.F0116_del.ALEFTB= inserted.ALEFTB
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0116_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_upd
	ON BUSDTA.F0116 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0116
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0116.ALAN8= inserted.ALAN8 AND BUSDTA.F0116.ALEFTB= inserted.ALEFTB');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0116_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_dlt
	ON BUSDTA.F0116 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0116_del (ALAN8, ALEFTB, last_modified )
	SELECT deleted.ALAN8, deleted.ALEFTB, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0116 - END */


/* [BUSDTA].[F0150] - begins */

/* TableDDL - [BUSDTA].[F0150] - Start */
IF OBJECT_ID('[BUSDTA].[F0150]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0150]
	(
	  [MAOSTP] NCHAR(3) NOT NULL
	, [MAPA8] NUMERIC(8,0) NOT NULL
	, [MAAN8] NUMERIC(8,0) NOT NULL
	, [MABEFD] NUMERIC(18,0) NULL
	, [MAEEFD] NUMERIC(18,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0150_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0150] PRIMARY KEY ([MAOSTP] ASC, [MAPA8] ASC, [MAAN8] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0150] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0150] - Start */
IF OBJECT_ID('[BUSDTA].[F0150_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0150_del]
	(
	  [MAOSTP] NCHAR(3)
	, [MAPA8] NUMERIC(8,0)
	, [MAAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MAOSTP] ASC, [MAPA8] ASC, [MAAN8] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0150] - End */
/* TRIGGERS FOR F0150 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0150_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0150_ins
	ON BUSDTA.F0150 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0150_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0150_del.MAAN8= inserted.MAAN8 AND BUSDTA.F0150_del.MAOSTP= inserted.MAOSTP AND BUSDTA.F0150_del.MAPA8= inserted.MAPA8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0150_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0150_upd
	ON BUSDTA.F0150 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0150
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0150.MAAN8= inserted.MAAN8 AND BUSDTA.F0150.MAOSTP= inserted.MAOSTP AND BUSDTA.F0150.MAPA8= inserted.MAPA8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0150_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0150_dlt
	ON BUSDTA.F0150 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0150_del (MAAN8, MAOSTP, MAPA8, last_modified )
	SELECT deleted.MAAN8, deleted.MAOSTP, deleted.MAPA8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0150 - END */


/* [BUSDTA].[F03012] - begins */

/* TableDDL - [BUSDTA].[F03012] - Start */
IF OBJECT_ID('[BUSDTA].[F03012]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F03012]
	(
	  [AIAN8] NUMERIC(8,0) NOT NULL
	, [AICO] NCHAR(5) NOT NULL
	, [AIMCUR] NCHAR(12) NULL
	, [AITXA1] NCHAR(10) NULL
	, [AIEXR1] NCHAR(2) NULL
	, [AIACL] FLOAT NULL
	, [AIHDAR] NCHAR(1) NULL
	, [AITRAR] NCHAR(3) NULL
	, [AISTTO] NCHAR(1) NULL
	, [AIRYIN] NCHAR(1) NULL
	, [AISTMT] NCHAR(1) NULL
	, [AIARPY] FLOAT NULL
	, [AISITO] NCHAR(1) NULL
	, [AICYCN] NCHAR(2) NULL
	, [AIBO] NCHAR(1) NULL
	, [AITSTA] NCHAR(2) NULL
	, [AICKHC] NCHAR(1) NULL
	, [AIDLC] NUMERIC(18,0) NULL
	, [AIDNLT] NCHAR(1) NULL
	, [AIPLCR] NCHAR(10) NULL
	, [AIRVDJ] NUMERIC(18,0) NULL
	, [AIDSO] FLOAT NULL
	, [AICMGR] NCHAR(10) NULL
	, [AICLMG] NCHAR(10) NULL
	, [AIAB2] NCHAR(1) NULL
	, [AIDT1J] NUMERIC(18,0) NULL
	, [AIDFIJ] NUMERIC(18,0) NULL
	, [AIDLIJ] NUMERIC(18,0) NULL
	, [AIDLP] NUMERIC(18,0) NULL
	, [AIASTY] FLOAT NULL
	, [AISPYE] FLOAT NULL
	, [AIAHB] FLOAT NULL
	, [AIALP] FLOAT NULL
	, [AIABAM] FLOAT NULL
	, [AIABA1] FLOAT NULL
	, [AIAPRC] FLOAT NULL
	, [AIMAXO] FLOAT NULL
	, [AIMINO] FLOAT NULL
	, [AIOYTD] FLOAT NULL
	, [AIOPY] FLOAT NULL
	, [AIPOPN] NCHAR(10) NULL
	, [AIDAOJ] NUMERIC(18,0) NULL
	, [AIAN8R] FLOAT NULL
	, [AIBADT] NCHAR(1) NULL
	, [AICPGP] NCHAR(8) NULL
	, [AIORTP] NCHAR(8) NULL
	, [AITRDC] FLOAT NULL
	, [AIINMG] NCHAR(10) NULL
	, [AIEXHD] NCHAR(1) NULL
	, [AIHOLD] NCHAR(2) NULL
	, [AIROUT] NCHAR(3) NULL
	, [AISTOP] NCHAR(3) NULL
	, [AIZON] NCHAR(3) NULL
	, [AICARS] FLOAT NULL
	, [AIDEL1] NCHAR(30) NULL
	, [AIDEL2] NCHAR(30) NULL
	, [AILTDT] FLOAT NULL
	, [AIFRTH] NCHAR(3) NULL
	, [AIAFT] NCHAR(1) NULL
	, [AIAPTS] NCHAR(1) NULL
	, [AISBAL] NCHAR(1) NULL
	, [AIBACK] NCHAR(1) NULL
	, [AIPORQ] NCHAR(1) NULL
	, [AIPRIO] NCHAR(1) NULL
	, [AIARTO] NCHAR(1) NULL
	, [AIINVC] FLOAT NULL
	, [AIICON] NCHAR(1) NULL
	, [AIBLFR] NCHAR(1) NULL
	, [AINIVD] NUMERIC(18,0) NULL
	, [AILEDJ] NUMERIC(18,0) NULL
	, [AIPLST] NCHAR(1) NULL
	, [AIEDF1] NCHAR(1) NULL
	, [AIEDF2] NCHAR(1) NULL
	, [AIASN] NCHAR(8) NULL
	, [AIDSPA] NCHAR(1) NULL
	, [AICRMD] NCHAR(1) NULL
	, [AIAMCR] FLOAT NULL
	, [AIAC01] NCHAR(3) NULL
	, [AIAC02] NCHAR(3) NULL
	, [AIAC03] NCHAR(3) NULL
	, [AIAC04] NCHAR(3) NULL
	, [AIAC05] NCHAR(3) NULL
	, [AIAC06] NCHAR(3) NULL
	, [AIAC07] NCHAR(3) NULL
	, [AIAC08] NCHAR(3) NULL
	, [AIAC09] NCHAR(3) NULL
	, [AIAC10] NCHAR(3) NULL
	, [AIAC11] NCHAR(3) NULL
	, [AIAC12] NCHAR(3) NULL
	, [AIAC13] NCHAR(3) NULL
	, [AIAC14] NCHAR(3) NULL
	, [AIAC15] NCHAR(3) NULL
	, [AIAC16] NCHAR(3) NULL
	, [AIAC17] NCHAR(3) NULL
	, [AIAC18] NCHAR(3) NULL
	, [AIAC19] NCHAR(3) NULL
	, [AIAC20] NCHAR(3) NULL
	, [AIAC21] NCHAR(3) NULL
	, [AIAC22] NCHAR(3) NULL
	, [AIAC23] NCHAR(3) NULL
	, [AIAC24] NCHAR(3) NULL
	, [AIAC25] NCHAR(3) NULL
	, [AIAC26] NCHAR(3) NULL
	, [AIAC27] NCHAR(3) NULL
	, [AIAC28] NCHAR(3) NULL
	, [AIAC29] NCHAR(3) NULL
	, [AIAC30] NCHAR(3) NULL
	, [AIPRSN] NCHAR(8) NULL
	, [AIOPBO] NCHAR(30) NULL
	, [AITIER1] NCHAR(5) NULL
	, [AIPWPCP] FLOAT NULL
	, [AICUSTS] NCHAR(1) NULL
	, [AISTOF] NCHAR(1) NULL
	, [AITERRID] FLOAT NULL
	, [AICIG] FLOAT NULL
	, [AITORG] NCHAR(10) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F03012_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F03012] PRIMARY KEY ([AIAN8] ASC, [AICO] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F03012] - End */

/* SHADOW TABLE FOR [BUSDTA].[F03012] - Start */
IF OBJECT_ID('[BUSDTA].[F03012_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F03012_del]
	(
	  [AIAN8] NUMERIC(8,0)
	, [AICO] NCHAR(5)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([AIAN8] ASC, [AICO] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F03012] - End */
/* TRIGGERS FOR F03012 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F03012_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F03012_ins
	ON BUSDTA.F03012 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F03012_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F03012_del.AIAN8= inserted.AIAN8 AND BUSDTA.F03012_del.AICO= inserted.AICO
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F03012_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F03012_upd
	ON BUSDTA.F03012 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F03012
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F03012.AIAN8= inserted.AIAN8 AND BUSDTA.F03012.AICO= inserted.AICO');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F03012_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F03012_dlt
	ON BUSDTA.F03012 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F03012_del (AIAN8, AICO, last_modified )
	SELECT deleted.AIAN8, deleted.AICO, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F03012 - END */

/* [BUSDTA].[F40073] - begins */

/* TableDDL - [BUSDTA].[F40073] - Start */
IF OBJECT_ID('[BUSDTA].[F40073]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40073]
	(
	  [HYPRFR] NCHAR(2) NOT NULL
	, [HYHYID] NCHAR(10) NOT NULL
	, [HYHY01] FLOAT NULL
	, [HYHY02] FLOAT NULL
	, [HYHY03] FLOAT NULL
	, [HYHY04] FLOAT NULL
	, [HYHY05] FLOAT NULL
	, [HYHY06] FLOAT NULL
	, [HYHY07] FLOAT NULL
	, [HYHY08] FLOAT NULL
	, [HYHY09] FLOAT NULL
	, [HYHY10] FLOAT NULL
	, [HYHY11] FLOAT NULL
	, [HYHY12] FLOAT NULL
	, [HYHY13] FLOAT NULL
	, [HYHY14] FLOAT NULL
	, [HYHY15] FLOAT NULL
	, [HYHY16] FLOAT NULL
	, [HYHY17] FLOAT NULL
	, [HYHY18] FLOAT NULL
	, [HYHY19] FLOAT NULL
	, [HYHY20] FLOAT NULL
	, [HYHY21] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F40073_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F40073] PRIMARY KEY ([HYPRFR] ASC, [HYHYID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F40073] - End */

/* SHADOW TABLE FOR [BUSDTA].[F40073] - Start */
IF OBJECT_ID('[BUSDTA].[F40073_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40073_del]
	(
	  [HYPRFR] NCHAR(2)
	, [HYHYID] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([HYPRFR] ASC, [HYHYID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F40073] - End */
/* TRIGGERS FOR F40073 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F40073_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F40073_ins
	ON BUSDTA.F40073 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F40073_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F40073_del.HYHYID= inserted.HYHYID AND BUSDTA.F40073_del.HYPRFR= inserted.HYPRFR
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F40073_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40073_upd
	ON BUSDTA.F40073 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F40073
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F40073.HYHYID= inserted.HYHYID AND BUSDTA.F40073.HYPRFR= inserted.HYPRFR');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F40073_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40073_dlt
	ON BUSDTA.F40073 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F40073_del (HYHYID, HYPRFR, last_modified )
	SELECT deleted.HYHYID, deleted.HYPRFR, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F40073 - END */

/* [BUSDTA].[F4013] - begins */

/* TableDDL - [BUSDTA].[F4013] - Start */
IF OBJECT_ID('[BUSDTA].[F4013]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4013]
	(
	  [SXXRTC] NCHAR(2) NOT NULL
	, [SXXRVF] NCHAR(30) NOT NULL
	, [SXXRVT] NCHAR(30) NOT NULL
	, [SXEDF1] NCHAR(1) NOT NULL
	, [SXDESC] NCHAR(30) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4013_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4013] PRIMARY KEY ([SXXRTC] ASC, [SXXRVF] ASC, [SXXRVT] ASC, [SXEDF1] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4013] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4013] - Start */
IF OBJECT_ID('[BUSDTA].[F4013_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4013_del]
	(
	  [SXXRTC] NCHAR(2)
	, [SXXRVF] NCHAR(30)
	, [SXXRVT] NCHAR(30)
	, [SXEDF1] NCHAR(1)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SXXRTC] ASC, [SXXRVF] ASC, [SXXRVT] ASC, [SXEDF1] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4013] - End */
/* TRIGGERS FOR F4013 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4013_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4013_ins
	ON BUSDTA.F4013 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4013_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4013_del.SXEDF1= inserted.SXEDF1 AND BUSDTA.F4013_del.SXXRTC= inserted.SXXRTC AND BUSDTA.F4013_del.SXXRVF= inserted.SXXRVF AND BUSDTA.F4013_del.SXXRVT= inserted.SXXRVT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4013_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4013_upd
	ON BUSDTA.F4013 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4013
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4013.SXEDF1= inserted.SXEDF1 AND BUSDTA.F4013.SXXRTC= inserted.SXXRTC AND BUSDTA.F4013.SXXRVF= inserted.SXXRVF AND BUSDTA.F4013.SXXRVT= inserted.SXXRVT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4013_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4013_dlt
	ON BUSDTA.F4013 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4013_del (SXEDF1, SXXRTC, SXXRVF, SXXRVT, last_modified )
	SELECT deleted.SXEDF1, deleted.SXXRTC, deleted.SXXRVF, deleted.SXXRVT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4013 - END */


/* [BUSDTA].[F4015] - begins */

/* TableDDL - [BUSDTA].[F4015] - Start */
IF OBJECT_ID('[BUSDTA].[F4015]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4015]
	(
	  [OTORTP] NCHAR(8) NOT NULL
	, [OTAN8] NUMERIC(8,0) NOT NULL
	, [OTOSEQ] NUMERIC(4,0) NULL
	, [OTITM] [numeric](8, 0) NOT NULL
	, [OTLITM] NCHAR(25) NULL
	, [OTQTYU] FLOAT NULL
	, [OTUOM] NCHAR(2) NULL
	, [OTLNTY] NCHAR(2) NULL
	, [OTEFTJ] NUMERIC(18,0) NULL
	, [OTEXDJ] NUMERIC(18,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4015_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4015] PRIMARY KEY ([OTORTP] ASC, [OTAN8] ASC, [OTITM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4015] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4015] - Start */
IF OBJECT_ID('[BUSDTA].[F4015_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4015_del]
	(
	  [OTORTP] NCHAR(8)
	, [OTAN8] NUMERIC(8,0)
	, [OTITM] [numeric](8, 0) NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([OTORTP] ASC, [OTAN8] ASC, [OTITM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4015] - End */
/* TRIGGERS FOR F4015 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4015_ins') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER [BUSDTA].[F4015_ins]
	ON [BUSDTA].[F4015] AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4015_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4015_del.OTAN8= inserted.OTAN8 AND BUSDTA.F4015_del.OTITM= inserted.OTITM AND BUSDTA.F4015_del.OTORTP= inserted.OTORTP
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4015_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER [BUSDTA].[F4015_upd]
	ON [BUSDTA].[F4015] AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4015
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4015.OTAN8= inserted.OTAN8 AND BUSDTA.F4015.OTITM= inserted.OTITM AND BUSDTA.F4015.OTORTP= inserted.OTORTP');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4015_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER [BUSDTA].[F4015_dlt]
	ON [BUSDTA].[F4015] AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4015_del (OTORTP,OTAN8, OTITM,  last_modified )
	SELECT deleted.OTORTP, deleted.OTAN8, deleted.OTITM,  GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4015 - END */


/* [BUSDTA].[F4070] - begins */

/* TableDDL - [BUSDTA].[F4070] - Start */
IF OBJECT_ID('[BUSDTA].[F4070]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4070]
	(
	  [SNASN] NCHAR(8) NOT NULL
	, [SNOSEQ] NUMERIC(4,0) NOT NULL
	, [SNANPS] NUMERIC(8,0) NOT NULL
	, [SNAST] NCHAR(8) NULL
	, [SNEFTJ] NUMERIC(18,0) NULL
	, [SNEXDJ] NUMERIC(18,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4070_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4070] PRIMARY KEY ([SNASN] ASC, [SNOSEQ] ASC, [SNANPS] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4070] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4070] - Start */
IF OBJECT_ID('[BUSDTA].[F4070_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4070_del]
	(
	  [SNASN] NCHAR(8)
	, [SNOSEQ] NUMERIC(4,0)
	, [SNANPS] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SNASN] ASC, [SNOSEQ] ASC, [SNANPS] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4070] - End */
/* TRIGGERS FOR F4070 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4070_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4070_ins
	ON BUSDTA.F4070 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4070_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4070_del.SNANPS= inserted.SNANPS AND BUSDTA.F4070_del.SNASN= inserted.SNASN AND BUSDTA.F4070_del.SNOSEQ= inserted.SNOSEQ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4070_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4070_upd
	ON BUSDTA.F4070 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4070
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4070.SNANPS= inserted.SNANPS AND BUSDTA.F4070.SNASN= inserted.SNASN AND BUSDTA.F4070.SNOSEQ= inserted.SNOSEQ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4070_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4070_dlt
	ON BUSDTA.F4070 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4070_del (SNANPS, SNASN, SNOSEQ, last_modified )
	SELECT deleted.SNANPS, deleted.SNASN, deleted.SNOSEQ, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4070 - END */

/* [BUSDTA].[F4071] - begins */

/* TableDDL - [BUSDTA].[F4071] - Start */
IF OBJECT_ID('[BUSDTA].[F4071]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4071]
	(
	  [ATAST] NCHAR(8) NOT NULL
	, [ATPRGR] NCHAR(8) NULL
	, [ATCPGP] NCHAR(8) NULL
	, [ATSDGR] NCHAR(8) NULL
	, [ATPRFR] NCHAR(2) NULL
	, [ATLBT] NCHAR(1) NULL
	, [ATGLC] NCHAR(4) NULL
	, [ATSBIF] NCHAR(1) NULL
	, [ATACNT] NCHAR(1) NULL
	, [ATLNTY] NCHAR(2) NULL
	, [ATMDED] NCHAR(1) NULL
	, [ATABAS] NCHAR(1) NULL
	, [ATOLVL] NCHAR(1) NULL
	, [ATTXB] NCHAR(1) NULL
	, [ATPA01] NCHAR(1) NULL
	, [ATPA02] NCHAR(1) NULL
	, [ATPA03] NCHAR(1) NULL
	, [ATPA04] NCHAR(1) NULL
	, [ATPA05] NCHAR(1) NULL
	, [ATENBM] NCHAR(1) NULL
	, [ATSRFLAG] NCHAR(1) NULL
	, [ATUSADJ] NCHAR(1) NULL
	, [ATATIER] FLOAT NULL
	, [ATBTIER] FLOAT NULL
	, [ATBNAD] FLOAT NULL
	, [ATAPRP1] NCHAR(3) NULL
	, [ATAPRP2] NCHAR(3) NULL
	, [ATAPRP3] NCHAR(3) NULL
	, [ATAPRP4] NCHAR(6) NULL
	, [ATAPRP5] NCHAR(6) NULL
	, [ATAPRP6] NCHAR(6) NULL
	, [ATADJGRP] NCHAR(10) NULL
	, [ATMEADJ] NCHAR(1) NULL
	, [ATPDCL] NCHAR(1) NULL
	, [ATUSER] NCHAR(10) NULL
	, [ATPID] NCHAR(10) NULL
	, [ATJOBN] NCHAR(10) NULL
	, [ATUPMJ] NUMERIC(18,0) NULL
	, [ATTDAY] FLOAT NULL
	, [ATDIDP] NCHAR(12) NULL
	, [ATPMTN] NCHAR(12) NULL
	, [ATPHST] NCHAR(1) NULL
	, [ATPA06] NCHAR(1) NULL
	, [ATPA07] NCHAR(1) NULL
	, [ATPA08] NCHAR(1) NULL
	, [ATPA09] NCHAR(1) NULL
	, [ATPA10] NCHAR(1) NULL
	, [ATEFCN] NCHAR(1) NULL
	, [ATAPTYPE] NCHAR(2) NULL
	, [ATMOADJ] NCHAR(1) NULL
	, [ATPLGRP] NCHAR(3) NULL
	, [ATEXCPL] NCHAR(1) NULL
	, [ATUPMX] NUMERIC(18,0) NULL
	, [ATMNMXAJ] NCHAR(1) NULL
	, [ATMNMXRL] NCHAR(1) NULL
	, [ATTSTRSNM] NCHAR(30) NULL
	, [ATADJQTY] NCHAR(1) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4071_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4071] PRIMARY KEY ([ATAST] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4071] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4071] - Start */
IF OBJECT_ID('[BUSDTA].[F4071_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4071_del]
	(
	  [ATAST] NCHAR(8)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ATAST] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4071] - End */
/* TRIGGERS FOR F4071 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4071_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4071_ins
	ON BUSDTA.F4071 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4071_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4071_del.ATAST= inserted.ATAST
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4071_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4071_upd
	ON BUSDTA.F4071 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4071
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4071.ATAST= inserted.ATAST');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4071_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4071_dlt
	ON BUSDTA.F4071 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4071_del (ATAST, last_modified )
	SELECT deleted.ATAST, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4071 - END */

/* [BUSDTA].[F4072] - begins */

/* TableDDL - [BUSDTA].[F4072] - Start */
IF OBJECT_ID('[BUSDTA].[F4072]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4072]
	(
	  [ADAST] NCHAR(8) NOT NULL
	, [ADITM] NUMERIC(8,0) NOT NULL
	, [ADLITM] NCHAR(25) NULL
	, [ADAITM] NCHAR(25) NULL
	, [ADAN8] NUMERIC(8,0) NOT NULL
	, [ADIGID] NUMERIC(8,0) NOT NULL
	, [ADCGID] NUMERIC(8,0) NOT NULL
	, [ADOGID] NUMERIC(8,0) NOT NULL
	, [ADCRCD] NCHAR(3) NOT NULL
	, [ADUOM] NCHAR(2) NOT NULL
	, [ADMNQ] NUMERIC(15,0) NOT NULL
	, [ADEFTJ] NUMERIC(18,0) NULL
	, [ADEXDJ] NUMERIC(18,0) NOT NULL
	, [ADBSCD] NCHAR(1) NULL
	, [ADLEDG] NCHAR(2) NULL
	, [ADFRMN] NCHAR(10) NULL
	, [ADFVTR] FLOAT NULL
	, [ADFGY] NCHAR(1) NULL
	, [ADATID] FLOAT NULL
	, [ADNBRORD] FLOAT NULL
	, [ADUOMVID] NCHAR(2) NULL
	, [ADFVUM] NCHAR(2) NULL
	, [ADPARTFG] NCHAR(1) NULL
	, [ADAPRS] NCHAR(1) NULL
	, [ADUPMJ] NUMERIC(18,0) NOT NULL
	, [ADTDAY] NUMERIC(6,0) NOT NULL
	, [ADBKTPID] FLOAT NULL
	, [ADCRCDVID] NCHAR(3) NULL
	, [ADRULENAME] NCHAR(10) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4072_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4072] PRIMARY KEY ([ADAST] ASC, [ADITM] ASC, [ADAN8] ASC, [ADIGID] ASC, [ADCGID] ASC, [ADOGID] ASC, [ADCRCD] ASC, [ADUOM] ASC, [ADMNQ] ASC, [ADEXDJ] ASC, [ADUPMJ] ASC, [ADTDAY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4072] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4072] - Start */
IF OBJECT_ID('[BUSDTA].[F4072_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4072_del]
	(
	  [ADAST] NCHAR(8)
	, [ADITM] NUMERIC(8,0)
	, [ADAN8] NUMERIC(8,0)
	, [ADIGID] NUMERIC(8,0)
	, [ADCGID] NUMERIC(8,0)
	, [ADOGID] NUMERIC(8,0)
	, [ADCRCD] NCHAR(3)
	, [ADUOM] NCHAR(2)
	, [ADMNQ] NUMERIC(15,0)
	, [ADEXDJ] NUMERIC(18,0)
	, [ADUPMJ] NUMERIC(18,0)
	, [ADTDAY] NUMERIC(6,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ADAST] ASC, [ADITM] ASC, [ADAN8] ASC, [ADIGID] ASC, [ADCGID] ASC, [ADOGID] ASC, [ADCRCD] ASC, [ADUOM] ASC, [ADMNQ] ASC, [ADEXDJ] ASC, [ADUPMJ] ASC, [ADTDAY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4072] - End */
/* TRIGGERS FOR F4072 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4072_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4072_ins
	ON BUSDTA.F4072 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4072_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4072_del.ADAN8= inserted.ADAN8 AND BUSDTA.F4072_del.ADAST= inserted.ADAST AND BUSDTA.F4072_del.ADCGID= inserted.ADCGID AND BUSDTA.F4072_del.ADCRCD= inserted.ADCRCD AND BUSDTA.F4072_del.ADEXDJ= inserted.ADEXDJ AND BUSDTA.F4072_del.ADIGID= inserted.ADIGID AND BUSDTA.F4072_del.ADITM= inserted.ADITM AND BUSDTA.F4072_del.ADMNQ= inserted.ADMNQ AND BUSDTA.F4072_del.ADOGID= inserted.ADOGID AND BUSDTA.F4072_del.ADTDAY= inserted.ADTDAY AND BUSDTA.F4072_del.ADUOM= inserted.ADUOM AND BUSDTA.F4072_del.ADUPMJ= inserted.ADUPMJ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4072_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4072_upd
	ON BUSDTA.F4072 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4072
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4072.ADAN8= inserted.ADAN8 AND BUSDTA.F4072.ADAST= inserted.ADAST AND BUSDTA.F4072.ADCGID= inserted.ADCGID AND BUSDTA.F4072.ADCRCD= inserted.ADCRCD AND BUSDTA.F4072.ADEXDJ= inserted.ADEXDJ AND BUSDTA.F4072.ADIGID= inserted.ADIGID AND BUSDTA.F4072.ADITM= inserted.ADITM AND BUSDTA.F4072.ADMNQ= inserted.ADMNQ AND BUSDTA.F4072.ADOGID= inserted.ADOGID AND BUSDTA.F4072.ADTDAY= inserted.ADTDAY AND BUSDTA.F4072.ADUOM= inserted.ADUOM AND BUSDTA.F4072.ADUPMJ= inserted.ADUPMJ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4072_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4072_dlt
	ON BUSDTA.F4072 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4072_del (ADAN8, ADAST, ADCGID, ADCRCD, ADEXDJ, ADIGID, ADITM, ADMNQ, ADOGID, ADTDAY, ADUOM, ADUPMJ, last_modified )
	SELECT deleted.ADAN8, deleted.ADAST, deleted.ADCGID, deleted.ADCRCD, deleted.ADEXDJ, deleted.ADIGID, deleted.ADITM, deleted.ADMNQ, deleted.ADOGID, deleted.ADTDAY, deleted.ADUOM, deleted.ADUPMJ, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4072 - END */


/* [BUSDTA].[F4075] - begins */

/* TableDDL - [BUSDTA].[F4075] - Start */
IF OBJECT_ID('[BUSDTA].[F4075]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4075]
	(
	  [VBVBT] NCHAR(10) NOT NULL
	, [VBCRCD] NCHAR(3) NULL
	, [VBUOM] NCHAR(2) NULL
	, [VBUPRC] FLOAT NULL
	, [VBEFTJ] NUMERIC(18,0) NOT NULL
	, [VBEXDJ] NUMERIC(18,0) NULL
	, [VBAPRS] NCHAR(1) NULL
	, [VBUPMJ] NUMERIC(18,0) NOT NULL
	, [VBTDAY] NUMERIC(6,0) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4075_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4075] PRIMARY KEY ([VBVBT] ASC, [VBEFTJ] ASC, [VBUPMJ] ASC, [VBTDAY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4075] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4075] - Start */
IF OBJECT_ID('[BUSDTA].[F4075_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4075_del]
	(
	  [VBVBT] NCHAR(10)
	, [VBEFTJ] NUMERIC(18,0)
	, [VBUPMJ] NUMERIC(18,0)
	, [VBTDAY] NUMERIC(6,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([VBVBT] ASC, [VBEFTJ] ASC, [VBUPMJ] ASC, [VBTDAY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4075] - End */
/* TRIGGERS FOR F4075 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4075_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4075_ins
	ON BUSDTA.F4075 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4075_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4075_del.VBEFTJ= inserted.VBEFTJ AND BUSDTA.F4075_del.VBTDAY= inserted.VBTDAY AND BUSDTA.F4075_del.VBUPMJ= inserted.VBUPMJ AND BUSDTA.F4075_del.VBVBT= inserted.VBVBT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4075_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4075_upd
	ON BUSDTA.F4075 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4075
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4075.VBEFTJ= inserted.VBEFTJ AND BUSDTA.F4075.VBTDAY= inserted.VBTDAY AND BUSDTA.F4075.VBUPMJ= inserted.VBUPMJ AND BUSDTA.F4075.VBVBT= inserted.VBVBT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4075_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4075_dlt
	ON BUSDTA.F4075 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4075_del (VBEFTJ, VBTDAY, VBUPMJ, VBVBT, last_modified )
	SELECT deleted.VBEFTJ, deleted.VBTDAY, deleted.VBUPMJ, deleted.VBVBT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4075 - END */


/* [BUSDTA].[F4076] - begins */

/* TableDDL - [BUSDTA].[F4076] - Start */
IF OBJECT_ID('[BUSDTA].[F4076]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4076]
	(
	  [FMFRMN] NCHAR(10) NOT NULL
	, [FMFML] NCHAR(160) NULL
	, [FMAPRS] NCHAR(1) NULL
	, [FMUPMJ] NUMERIC(18,0) NOT NULL
	, [FMTDAY] NUMERIC(6,0) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4076_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4076] PRIMARY KEY ([FMFRMN] ASC, [FMUPMJ] ASC, [FMTDAY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4076] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4076] - Start */
IF OBJECT_ID('[BUSDTA].[F4076_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4076_del]
	(
	  [FMFRMN] NCHAR(10)
	, [FMUPMJ] NUMERIC(18,0)
	, [FMTDAY] NUMERIC(6,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([FMFRMN] ASC, [FMUPMJ] ASC, [FMTDAY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4076] - End */
/* TRIGGERS FOR F4076 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4076_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4076_ins
	ON BUSDTA.F4076 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4076_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4076_del.FMFRMN= inserted.FMFRMN AND BUSDTA.F4076_del.FMTDAY= inserted.FMTDAY AND BUSDTA.F4076_del.FMUPMJ= inserted.FMUPMJ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4076_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4076_upd
	ON BUSDTA.F4076 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4076
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4076.FMFRMN= inserted.FMFRMN AND BUSDTA.F4076.FMTDAY= inserted.FMTDAY AND BUSDTA.F4076.FMUPMJ= inserted.FMUPMJ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4076_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4076_dlt
	ON BUSDTA.F4076 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4076_del (FMFRMN, FMTDAY, FMUPMJ, last_modified )
	SELECT deleted.FMFRMN, deleted.FMTDAY, deleted.FMUPMJ, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4076 - END */


/* [BUSDTA].[F4092] - begins */

/* TableDDL - [BUSDTA].[F4092] - Start */
IF OBJECT_ID('[BUSDTA].[F4092]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4092]
	(
	  [GPGPTY] NCHAR(1) NOT NULL
	, [GPGPC] NCHAR(8) NOT NULL
	, [GPDL01] NCHAR(30) NULL
	, [GPGPK1] NCHAR(10) NULL
	, [GPGPK2] NCHAR(10) NULL
	, [GPGPK3] NCHAR(10) NULL
	, [GPGPK4] NCHAR(10) NULL
	, [GPGPK5] NCHAR(10) NULL
	, [GPGPK6] NCHAR(10) NULL
	, [GPGPK7] NCHAR(10) NULL
	, [GPGPK8] NCHAR(10) NULL
	, [GPGPK9] NCHAR(10) NULL
	, [GPGPK10] NCHAR(10) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4092_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4092] PRIMARY KEY ([GPGPTY] ASC, [GPGPC] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4092] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4092] - Start */
IF OBJECT_ID('[BUSDTA].[F4092_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4092_del]
	(
	  [GPGPTY] NCHAR(1)
	, [GPGPC] NCHAR(8)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([GPGPTY] ASC, [GPGPC] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4092] - End */
/* TRIGGERS FOR F4092 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4092_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4092_ins
	ON BUSDTA.F4092 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4092_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4092_del.GPGPC= inserted.GPGPC AND BUSDTA.F4092_del.GPGPTY= inserted.GPGPTY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4092_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4092_upd
	ON BUSDTA.F4092 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4092
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4092.GPGPC= inserted.GPGPC AND BUSDTA.F4092.GPGPTY= inserted.GPGPTY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4092_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4092_dlt
	ON BUSDTA.F4092 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4092_del (GPGPC, GPGPTY, last_modified )
	SELECT deleted.GPGPC, deleted.GPGPTY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4092 - END */


/* [BUSDTA].[F40941] - begins */

/* TableDDL - [BUSDTA].[F40941] - Start */
IF OBJECT_ID('[BUSDTA].[F40941]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40941]
	(
	  [IKPRGR] NCHAR(8) NOT NULL
	, [IKIGP1] NCHAR(6) NOT NULL
	, [IKIGP2] NCHAR(6) NOT NULL
	, [IKIGP3] NCHAR(6) NOT NULL
	, [IKIGP4] NCHAR(6) NOT NULL
	, [IKIGP5] NCHAR(6) NOT NULL
	, [IKIGP6] NCHAR(6) NOT NULL
	, [IKIGP7] NCHAR(6) NOT NULL
	, [IKIGP8] NCHAR(6) NOT NULL
	, [IKIGP9] NCHAR(6) NOT NULL
	, [IKIGP10] NCHAR(6) NOT NULL
	, [IKIGID] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F40941_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F40941] PRIMARY KEY ([IKPRGR] ASC, [IKIGP1] ASC, [IKIGP2] ASC, [IKIGP3] ASC, [IKIGP4] ASC, [IKIGP5] ASC, [IKIGP6] ASC, [IKIGP7] ASC, [IKIGP8] ASC, [IKIGP9] ASC, [IKIGP10] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F40941] - End */

/* SHADOW TABLE FOR [BUSDTA].[F40941] - Start */
IF OBJECT_ID('[BUSDTA].[F40941_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40941_del]
	(
	  [IKPRGR] NCHAR(8)
	, [IKIGP1] NCHAR(6)
	, [IKIGP2] NCHAR(6)
	, [IKIGP3] NCHAR(6)
	, [IKIGP4] NCHAR(6)
	, [IKIGP5] NCHAR(6)
	, [IKIGP6] NCHAR(6)
	, [IKIGP7] NCHAR(6)
	, [IKIGP8] NCHAR(6)
	, [IKIGP9] NCHAR(6)
	, [IKIGP10] NCHAR(6)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([IKPRGR] ASC, [IKIGP1] ASC, [IKIGP2] ASC, [IKIGP3] ASC, [IKIGP4] ASC, [IKIGP5] ASC, [IKIGP6] ASC, [IKIGP7] ASC, [IKIGP8] ASC, [IKIGP9] ASC, [IKIGP10] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F40941] - End */
/* TRIGGERS FOR F40941 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F40941_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F40941_ins
	ON BUSDTA.F40941 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F40941_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F40941_del.IKIGP1= inserted.IKIGP1 AND BUSDTA.F40941_del.IKIGP10= inserted.IKIGP10 AND BUSDTA.F40941_del.IKIGP2= inserted.IKIGP2 AND BUSDTA.F40941_del.IKIGP3= inserted.IKIGP3 AND BUSDTA.F40941_del.IKIGP4= inserted.IKIGP4 AND BUSDTA.F40941_del.IKIGP5= inserted.IKIGP5 AND BUSDTA.F40941_del.IKIGP6= inserted.IKIGP6 AND BUSDTA.F40941_del.IKIGP7= inserted.IKIGP7 AND BUSDTA.F40941_del.IKIGP8= inserted.IKIGP8 AND BUSDTA.F40941_del.IKIGP9= inserted.IKIGP9 AND BUSDTA.F40941_del.IKPRGR= inserted.IKPRGR
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F40941_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40941_upd
	ON BUSDTA.F40941 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F40941
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F40941.IKIGP1= inserted.IKIGP1 AND BUSDTA.F40941.IKIGP10= inserted.IKIGP10 AND BUSDTA.F40941.IKIGP2= inserted.IKIGP2 AND BUSDTA.F40941.IKIGP3= inserted.IKIGP3 AND BUSDTA.F40941.IKIGP4= inserted.IKIGP4 AND BUSDTA.F40941.IKIGP5= inserted.IKIGP5 AND BUSDTA.F40941.IKIGP6= inserted.IKIGP6 AND BUSDTA.F40941.IKIGP7= inserted.IKIGP7 AND BUSDTA.F40941.IKIGP8= inserted.IKIGP8 AND BUSDTA.F40941.IKIGP9= inserted.IKIGP9 AND BUSDTA.F40941.IKPRGR= inserted.IKPRGR');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F40941_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40941_dlt
	ON BUSDTA.F40941 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F40941_del (IKIGP1, IKIGP10, IKIGP2, IKIGP3, IKIGP4, IKIGP5, IKIGP6, IKIGP7, IKIGP8, IKIGP9, IKPRGR, last_modified )
	SELECT deleted.IKIGP1, deleted.IKIGP10, deleted.IKIGP2, deleted.IKIGP3, deleted.IKIGP4, deleted.IKIGP5, deleted.IKIGP6, deleted.IKIGP7, deleted.IKIGP8, deleted.IKIGP9, deleted.IKPRGR, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F40941 - END */


/* [BUSDTA].[F40942] - begins */

/* TableDDL - [BUSDTA].[F40942] - Start */
IF OBJECT_ID('[BUSDTA].[F40942]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40942]
	(
	  [CKCPGP] NCHAR(8) NOT NULL
	, [CKCGP1] NCHAR(3) NOT NULL
	, [CKCGP2] NCHAR(3) NOT NULL
	, [CKCGP3] NCHAR(3) NOT NULL
	, [CKCGP4] NCHAR(3) NOT NULL
	, [CKCGP5] NCHAR(3) NOT NULL
	, [CKCGP6] NCHAR(3) NOT NULL
	, [CKCGP7] NCHAR(3) NOT NULL
	, [CKCGP8] NCHAR(3) NOT NULL
	, [CKCGP9] NCHAR(3) NOT NULL
	, [CKCGP10] NCHAR(3) NOT NULL
	, [CKCGID] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F40942_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F40942] PRIMARY KEY ([CKCPGP] ASC, [CKCGP1] ASC, [CKCGP2] ASC, [CKCGP3] ASC, [CKCGP4] ASC, [CKCGP5] ASC, [CKCGP6] ASC, [CKCGP7] ASC, [CKCGP8] ASC, [CKCGP9] ASC, [CKCGP10] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F40942] - End */

/* SHADOW TABLE FOR [BUSDTA].[F40942] - Start */
IF OBJECT_ID('[BUSDTA].[F40942_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40942_del]
	(
	  [CKCPGP] NCHAR(8)
	, [CKCGP1] NCHAR(3)
	, [CKCGP2] NCHAR(3)
	, [CKCGP3] NCHAR(3)
	, [CKCGP4] NCHAR(3)
	, [CKCGP5] NCHAR(3)
	, [CKCGP6] NCHAR(3)
	, [CKCGP7] NCHAR(3)
	, [CKCGP8] NCHAR(3)
	, [CKCGP9] NCHAR(3)
	, [CKCGP10] NCHAR(3)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CKCPGP] ASC, [CKCGP1] ASC, [CKCGP2] ASC, [CKCGP3] ASC, [CKCGP4] ASC, [CKCGP5] ASC, [CKCGP6] ASC, [CKCGP7] ASC, [CKCGP8] ASC, [CKCGP9] ASC, [CKCGP10] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F40942] - End */
/* TRIGGERS FOR F40942 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F40942_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F40942_ins
	ON BUSDTA.F40942 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F40942_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F40942_del.CKCGP1= inserted.CKCGP1 AND BUSDTA.F40942_del.CKCGP10= inserted.CKCGP10 AND BUSDTA.F40942_del.CKCGP2= inserted.CKCGP2 AND BUSDTA.F40942_del.CKCGP3= inserted.CKCGP3 AND BUSDTA.F40942_del.CKCGP4= inserted.CKCGP4 AND BUSDTA.F40942_del.CKCGP5= inserted.CKCGP5 AND BUSDTA.F40942_del.CKCGP6= inserted.CKCGP6 AND BUSDTA.F40942_del.CKCGP7= inserted.CKCGP7 AND BUSDTA.F40942_del.CKCGP8= inserted.CKCGP8 AND BUSDTA.F40942_del.CKCGP9= inserted.CKCGP9 AND BUSDTA.F40942_del.CKCPGP= inserted.CKCPGP
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F40942_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40942_upd
	ON BUSDTA.F40942 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F40942
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F40942.CKCGP1= inserted.CKCGP1 AND BUSDTA.F40942.CKCGP10= inserted.CKCGP10 AND BUSDTA.F40942.CKCGP2= inserted.CKCGP2 AND BUSDTA.F40942.CKCGP3= inserted.CKCGP3 AND BUSDTA.F40942.CKCGP4= inserted.CKCGP4 AND BUSDTA.F40942.CKCGP5= inserted.CKCGP5 AND BUSDTA.F40942.CKCGP6= inserted.CKCGP6 AND BUSDTA.F40942.CKCGP7= inserted.CKCGP7 AND BUSDTA.F40942.CKCGP8= inserted.CKCGP8 AND BUSDTA.F40942.CKCGP9= inserted.CKCGP9 AND BUSDTA.F40942.CKCPGP= inserted.CKCPGP');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F40942_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40942_dlt
	ON BUSDTA.F40942 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F40942_del (CKCGP1, CKCGP10, CKCGP2, CKCGP3, CKCGP4, CKCGP5, CKCGP6, CKCGP7, CKCGP8, CKCGP9, CKCPGP, last_modified )
	SELECT deleted.CKCGP1, deleted.CKCGP10, deleted.CKCGP2, deleted.CKCGP3, deleted.CKCGP4, deleted.CKCGP5, deleted.CKCGP6, deleted.CKCGP7, deleted.CKCGP8, deleted.CKCGP9, deleted.CKCPGP, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F40942 - END */


/* [BUSDTA].[F41002] - begins */

/* TableDDL - [BUSDTA].[F41002] - Start */
IF OBJECT_ID('[BUSDTA].[F41002]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F41002]
	(
	  [UMMCU] NCHAR(12) NOT NULL
	, [UMITM] NUMERIC(8,0) NOT NULL
	, [UMUM] NCHAR(2) NOT NULL
	, [UMRUM] NCHAR(2) NOT NULL
	, [UMUSTR] NCHAR(1) NULL
	, [UMCONV] FLOAT NULL
	, [UMCNV1] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F41002_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F41002] PRIMARY KEY ([UMMCU] ASC, [UMITM] ASC, [UMUM] ASC, [UMRUM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F41002] - End */

/* SHADOW TABLE FOR [BUSDTA].[F41002] - Start */
IF OBJECT_ID('[BUSDTA].[F41002_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F41002_del]
	(
	  [UMMCU] NCHAR(12)
	, [UMITM] NUMERIC(8,0)
	, [UMUM] NCHAR(2)
	, [UMRUM] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UMMCU] ASC, [UMITM] ASC, [UMUM] ASC, [UMRUM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F41002] - End */
/* TRIGGERS FOR F41002 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F41002_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F41002_ins
	ON BUSDTA.F41002 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F41002_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F41002_del.UMITM= inserted.UMITM AND BUSDTA.F41002_del.UMMCU= inserted.UMMCU AND BUSDTA.F41002_del.UMRUM= inserted.UMRUM AND BUSDTA.F41002_del.UMUM= inserted.UMUM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F41002_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F41002_upd
	ON BUSDTA.F41002 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F41002
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F41002.UMITM= inserted.UMITM AND BUSDTA.F41002.UMMCU= inserted.UMMCU AND BUSDTA.F41002.UMRUM= inserted.UMRUM AND BUSDTA.F41002.UMUM= inserted.UMUM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F41002_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F41002_dlt
	ON BUSDTA.F41002 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F41002_del (UMITM, UMMCU, UMRUM, UMUM, last_modified )
	SELECT deleted.UMITM, deleted.UMMCU, deleted.UMRUM, deleted.UMUM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F41002 - END */


/* [BUSDTA].[F4101] - begins */

/* TableDDL - [BUSDTA].[F4101] - Start */
IF OBJECT_ID('[BUSDTA].[F4101]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4101]
	(
	  [IMITM] NUMERIC(8,0) NOT NULL
	, [IMLITM] NCHAR(25) NULL
	, [IMAITM] NCHAR(25) NULL
	, [IMDSC1] NCHAR(30) NULL
	, [IMDSC2] NCHAR(30) NULL
	, [IMSRP1] NCHAR(3) NULL
	, [IMSRP2] NCHAR(3) NULL
	, [IMSRP3] NCHAR(3) NULL
	, [IMSRP4] NCHAR(3) NULL
	, [IMSRP5] NCHAR(3) NULL
	, [IMSRP6] NCHAR(6) NULL
	, [IMSRP7] NCHAR(6) NULL
	, [IMSRP8] NCHAR(6) NULL
	, [IMSRP9] NCHAR(6) NULL
	, [IMSRP0] NCHAR(6) NULL
	, [IMPRP1] NCHAR(3) NULL
	, [IMPRP2] NCHAR(3) NULL
	, [IMPRP3] NCHAR(3) NULL
	, [IMPRP4] NCHAR(3) NULL
	, [IMPRP5] NCHAR(3) NULL
	, [IMPRP6] NCHAR(6) NULL
	, [IMPRP7] NCHAR(6) NULL
	, [IMPRP8] NCHAR(6) NULL
	, [IMPRP9] NCHAR(6) NULL
	, [IMPRP0] NCHAR(6) NULL
	, [IMCDCD] NCHAR(15) NULL
	, [IMPDGR] NCHAR(3) NULL
	, [IMDSGP] NCHAR(3) NULL
	, [IMPRGR] NCHAR(8) NULL
	, [IMRPRC] NCHAR(8) NULL
	, [IMORPR] NCHAR(8) NULL
	, [IMVCUD] FLOAT NULL
	, [IMUOM1] NCHAR(2) NULL
	, [IMUOM2] NCHAR(2) NULL
	, [IMUOM4] NCHAR(2) NULL
	, [IMUOM6] NCHAR(2) NULL
	, [IMUWUM] NCHAR(2) NULL
	, [IMUVM1] NCHAR(2) NULL
	, [IMCYCL] NCHAR(3) NULL
	, [IMGLPT] NCHAR(4) NULL
	, [IMPLEV] NCHAR(1) NULL
	, [IMPPLV] NCHAR(1) NULL
	, [IMCLEV] NCHAR(1) NULL
	, [IMCKAV] NCHAR(1) NULL
	, [IMSRCE] NCHAR(1) NULL
	, [IMSTKT] NCHAR(1) NULL
	, [IMLNTY] NCHAR(2) NULL
	, [IMBACK] NCHAR(1) NULL
	, [IMIFLA] NCHAR(2) NULL
	, [IMTFLA] NCHAR(2) NULL
	, [IMINMG] NCHAR(10) NULL
	, [IMABCS] NCHAR(1) NULL
	, [IMABCM] NCHAR(1) NULL
	, [IMABCI] NCHAR(1) NULL
	, [IMOVR] NCHAR(1) NULL
	, [IMCMCG] NCHAR(8) NULL
	, [IMSRNR] NCHAR(1) NULL
	, [IMFIFO] NCHAR(1) NULL
	, [IMLOTS] NCHAR(1) NULL
	, [IMSLD] FLOAT NULL
	, [IMPCTM] FLOAT NULL
	, [IMMMPC] FLOAT NULL
	, [IMCMGL] NCHAR(1) NULL
	, [IMUPCN] NCHAR(13) NULL
	, [IMUMUP] NCHAR(2) NULL
	, [IMUMDF] NCHAR(2) NULL
	, [IMBBDD] FLOAT NULL
	, [IMCMDM] NCHAR(1) NULL
	, [IMLECM] NCHAR(1) NULL
	, [IMLEDD] FLOAT NULL
	, [IMPEFD] FLOAT NULL
	, [IMSBDD] FLOAT NULL
	, [IMU1DD] FLOAT NULL
	, [IMU2DD] FLOAT NULL
	, [IMU3DD] FLOAT NULL
	, [IMU4DD] FLOAT NULL
	, [IMU5DD] FLOAT NULL
	, [IMLNPA] NCHAR(1) NULL
	, [IMLOTC] NCHAR(3) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4101_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4101] PRIMARY KEY ([IMITM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4101] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4101] - Start */
IF OBJECT_ID('[BUSDTA].[F4101_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4101_del]
	(
	  [IMITM] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([IMITM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4101] - End */
/* TRIGGERS FOR F4101 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4101_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4101_ins
	ON BUSDTA.F4101 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4101_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4101_del.IMITM= inserted.IMITM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4101_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4101_upd
	ON BUSDTA.F4101 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4101
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4101.IMITM= inserted.IMITM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4101_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4101_dlt
	ON BUSDTA.F4101 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4101_del (IMITM, last_modified )
	SELECT deleted.IMITM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4101 - END */


/* [BUSDTA].[F4102] - begins */

/* TableDDL - [BUSDTA].[F4102] - Start */
IF OBJECT_ID('[BUSDTA].[F4102]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4102]
	(
	  [IBITM] NUMERIC(8,0) NOT NULL
	, [IBLITM] NCHAR(25) NULL
	, [IBAITM] NCHAR(25) NULL
	, [IBMCU] NCHAR(12) NOT NULL
	, [IBSRP1] NCHAR(3) NULL
	, [IBSRP2] NCHAR(3) NULL
	, [IBSRP3] NCHAR(3) NULL
	, [IBSRP4] NCHAR(3) NULL
	, [IBSRP5] NCHAR(3) NULL
	, [IBSRP6] NCHAR(6) NULL
	, [IBSRP7] NCHAR(6) NULL
	, [IBSRP8] NCHAR(6) NULL
	, [IBSRP9] NCHAR(6) NULL
	, [IBSRP0] NCHAR(6) NULL
	, [IBPRP1] NCHAR(3) NULL
	, [IBPRP2] NCHAR(3) NULL
	, [IBPRP3] NCHAR(3) NULL
	, [IBPRP4] NCHAR(3) NULL
	, [IBPRP5] NCHAR(3) NULL
	, [IBPRP6] NCHAR(6) NULL
	, [IBPRP7] NCHAR(6) NULL
	, [IBPRP8] NCHAR(6) NULL
	, [IBPRP9] NCHAR(6) NULL
	, [IBPRP0] NCHAR(6) NULL
	, [IBCDCD] NCHAR(15) NULL
	, [IBPDGR] NCHAR(3) NULL
	, [IBDSGP] NCHAR(3) NULL
	, [IBGLPT] NCHAR(4) NULL
	, [IBORIG] NCHAR(3) NULL
	, [IBSAFE] FLOAT NULL
	, [IBSLD] FLOAT NULL
	, [IBCKAV] NCHAR(1) NULL
	, [IBSRCE] NCHAR(1) NULL
	, [IBLOTS] NCHAR(1) NULL
	, [IBMMPC] FLOAT NULL
	, [IBPRGR] NCHAR(8) NULL
	, [IBRPRC] NCHAR(8) NULL
	, [IBORPR] NCHAR(8) NULL
	, [IBBACK] NCHAR(1) NULL
	, [IBIFLA] NCHAR(2) NULL
	, [IBABCS] NCHAR(1) NULL
	, [IBABCM] NCHAR(1) NULL
	, [IBABCI] NCHAR(1) NULL
	, [IBOVR] NCHAR(1) NULL
	, [IBSTKT] NCHAR(1) NULL
	, [IBLNTY] NCHAR(2) NULL
	, [IBFIFO] NCHAR(1) NULL
	, [IBCYCL] NCHAR(3) NULL
	, [IBINMG] NCHAR(10) NULL
	, [IBSRNR] NCHAR(1) NULL
	, [IBPCTM] FLOAT NULL
	, [IBCMCG] NCHAR(8) NULL
	, [IBTAX1] NCHAR(1) NULL
	, [IBBBDD] FLOAT NULL
	, [IBCMDM] NCHAR(1) NULL
	, [IBLECM] NCHAR(1) NULL
	, [IBLEDD] FLOAT NULL
	, [IBMLOT] NCHAR(1) NULL
	, [IBSBDD] FLOAT NULL
	, [IBU1DD] FLOAT NULL
	, [IBU2DD] FLOAT NULL
	, [IBU3DD] FLOAT NULL
	, [IBU4DD] FLOAT NULL
	, [IBU5DD] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4102_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4102] PRIMARY KEY ([IBITM] ASC, [IBMCU] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4102] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4102] - Start */
IF OBJECT_ID('[BUSDTA].[F4102_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4102_del]
	(
	  [IBITM] NUMERIC(8,0)
	, [IBMCU] NCHAR(12)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([IBITM] ASC, [IBMCU] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4102] - End */
/* TRIGGERS FOR F4102 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4102_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4102_ins
	ON BUSDTA.F4102 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4102_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4102_del.IBITM= inserted.IBITM AND BUSDTA.F4102_del.IBMCU= inserted.IBMCU
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4102_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4102_upd
	ON BUSDTA.F4102 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4102
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4102.IBITM= inserted.IBITM AND BUSDTA.F4102.IBMCU= inserted.IBMCU');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4102_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4102_dlt
	ON BUSDTA.F4102 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4102_del (IBITM, IBMCU, last_modified )
	SELECT deleted.IBITM, deleted.IBMCU, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4102 - END */


/* [BUSDTA].[F4106] - begins */

/* TableDDL - [BUSDTA].[F4106] - Start */
IF OBJECT_ID('[BUSDTA].[F4106]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4106]
	(
	  [BPITM] NUMERIC(8,0) NOT NULL
	, [BPLITM] NCHAR(25) NULL
	, [BPMCU] NCHAR(12) NOT NULL
	, [BPLOCN] NCHAR(20) NOT NULL
	, [BPLOTN] NCHAR(30) NOT NULL
	, [BPAN8] NUMERIC(8,0) NOT NULL
	, [BPIGID] NUMERIC(8,0) NOT NULL
	, [BPCGID] NUMERIC(8,0) NOT NULL
	, [BPLOTG] NCHAR(3) NOT NULL
	, [BPFRMP] NUMERIC(7,0) NOT NULL
	, [BPCRCD] NCHAR(3) NOT NULL
	, [BPUOM] NCHAR(2) NOT NULL
	, [BPEFTJ] NUMERIC(18,0) NULL
	, [BPEXDJ] NUMERIC(18,0) NOT NULL
	, [BPUPRC] FLOAT NULL
	, [BPUPMJ] NUMERIC(18,0) NOT NULL
	, [BPTDAY] FLOAT NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4106_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4106] PRIMARY KEY ([BPITM] ASC, [BPMCU] ASC, [BPLOCN] ASC, [BPLOTN] ASC, [BPAN8] ASC, [BPIGID] ASC, [BPCGID] ASC, [BPLOTG] ASC, [BPFRMP] ASC, [BPCRCD] ASC, [BPUOM] ASC, [BPEXDJ] ASC, [BPUPMJ] ASC, [BPTDAY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4106] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4106] - Start */
IF OBJECT_ID('[BUSDTA].[F4106_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4106_del]
	(
	  [BPITM] NUMERIC(8,0)
	, [BPMCU] NCHAR(12)
	, [BPLOCN] NCHAR(20)
	, [BPLOTN] NCHAR(30)
	, [BPAN8] NUMERIC(8,0)
	, [BPIGID] NUMERIC(8,0)
	, [BPCGID] NUMERIC(8,0)
	, [BPLOTG] NCHAR(3)
	, [BPFRMP] NUMERIC(7,0)
	, [BPCRCD] NCHAR(3)
	, [BPUOM] NCHAR(2)
	, [BPEXDJ] NUMERIC(18,0)
	, [BPUPMJ] NUMERIC(18,0)
	, [BPTDAY] FLOAT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([BPITM] ASC, [BPMCU] ASC, [BPLOCN] ASC, [BPLOTN] ASC, [BPAN8] ASC, [BPIGID] ASC, [BPCGID] ASC, [BPLOTG] ASC, [BPFRMP] ASC, [BPCRCD] ASC, [BPUOM] ASC, [BPEXDJ] ASC, [BPUPMJ] ASC, [BPTDAY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4106] - End */
/* TRIGGERS FOR F4106 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4106_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4106_ins
	ON BUSDTA.F4106 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4106_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4106_del.BPAN8= inserted.BPAN8 AND BUSDTA.F4106_del.BPCGID= inserted.BPCGID AND BUSDTA.F4106_del.BPCRCD= inserted.BPCRCD AND BUSDTA.F4106_del.BPEXDJ= inserted.BPEXDJ AND BUSDTA.F4106_del.BPFRMP= inserted.BPFRMP AND BUSDTA.F4106_del.BPIGID= inserted.BPIGID AND BUSDTA.F4106_del.BPITM= inserted.BPITM AND BUSDTA.F4106_del.BPLOCN= inserted.BPLOCN AND BUSDTA.F4106_del.BPLOTG= inserted.BPLOTG AND BUSDTA.F4106_del.BPLOTN= inserted.BPLOTN AND BUSDTA.F4106_del.BPMCU= inserted.BPMCU AND BUSDTA.F4106_del.BPTDAY= inserted.BPTDAY AND BUSDTA.F4106_del.BPUOM= inserted.BPUOM AND BUSDTA.F4106_del.BPUPMJ= inserted.BPUPMJ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4106_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4106_upd
	ON BUSDTA.F4106 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4106
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4106.BPAN8= inserted.BPAN8 AND BUSDTA.F4106.BPCGID= inserted.BPCGID AND BUSDTA.F4106.BPCRCD= inserted.BPCRCD AND BUSDTA.F4106.BPEXDJ= inserted.BPEXDJ AND BUSDTA.F4106.BPFRMP= inserted.BPFRMP AND BUSDTA.F4106.BPIGID= inserted.BPIGID AND BUSDTA.F4106.BPITM= inserted.BPITM AND BUSDTA.F4106.BPLOCN= inserted.BPLOCN AND BUSDTA.F4106.BPLOTG= inserted.BPLOTG AND BUSDTA.F4106.BPLOTN= inserted.BPLOTN AND BUSDTA.F4106.BPMCU= inserted.BPMCU AND BUSDTA.F4106.BPTDAY= inserted.BPTDAY AND BUSDTA.F4106.BPUOM= inserted.BPUOM AND BUSDTA.F4106.BPUPMJ= inserted.BPUPMJ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4106_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4106_dlt
	ON BUSDTA.F4106 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4106_del (BPAN8, BPCGID, BPCRCD, BPEXDJ, BPFRMP, BPIGID, BPUOM, BPUPMJ, BPITM, BPLOCN, BPLOTG, BPLOTN, BPMCU, BPTDAY, last_modified )
	SELECT deleted.BPAN8, deleted.BPCGID, deleted.BPCRCD, deleted.BPEXDJ, deleted.BPFRMP, deleted.BPIGID, deleted.BPUOM, deleted.BPUPMJ, deleted.BPITM, deleted.BPLOCN, deleted.BPLOTG, deleted.BPLOTN, deleted.BPMCU, deleted.BPTDAY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4106 - END */


/* [BUSDTA].[F42019] - begins */

/* TableDDL - [BUSDTA].[F42019] - Start */
IF OBJECT_ID('[BUSDTA].[F42019]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F42019]
	(
	  [SHKCOO] NCHAR(5) NOT NULL
	, [SHDOCO] NUMERIC(8,0) NOT NULL
	, [SHDCTO] NCHAR(2) NOT NULL
	, [SHMCU] NCHAR(12) NULL
	, [SHRKCO] NCHAR(5) NULL
	, [SHRORN] NCHAR(8) NULL
	, [SHRCTO] NCHAR(2) NULL
	, [SHAN8] FLOAT NULL
	, [SHSHAN] FLOAT NULL
	, [SHPA8] FLOAT NULL
	, [SHDRQJ] NUMERIC(18,0) NULL
	, [SHTRDJ] NUMERIC(18,0) NULL
	, [SHPDDJ] NUMERIC(18,0) NULL
	, [SHADDJ] NUMERIC(18,0) NULL
	, [SHCNDJ] NUMERIC(18,0) NULL
	, [SHPEFJ] NUMERIC(18,0) NULL
	, [SHVR01] NCHAR(25) NULL
	, [SHVR02] NCHAR(25) NULL
	, [SHDEL1] NCHAR(30) NULL
	, [SHDEL2] NCHAR(30) NULL
	, [SHINMG] NCHAR(10) NULL
	, [SHPTC] NCHAR(3) NULL
	, [SHRYIN] NCHAR(1) NULL
	, [SHASN] NCHAR(8) NULL
	, [SHPRGP] NCHAR(8) NULL
	, [SHTXA1] NCHAR(10) NULL
	, [SHEXR1] NCHAR(2) NULL
	, [SHTXCT] NCHAR(20) NULL
	, [SHATXT] NCHAR(1) NULL
	, [SHHOLD] NCHAR(2) NULL
	, [SHROUT] NCHAR(3) NULL
	, [SHSTOP] NCHAR(3) NULL
	, [SHZON] NCHAR(3) NULL
	, [SHFRTH] NCHAR(3) NULL
	, [SHRCD] NCHAR(3) NULL
	, [SHFUF2] NCHAR(1) NULL
	, [SHOTOT] FLOAT NULL
	, [SHAUTN] NCHAR(10) NULL
	, [SHCACT] NCHAR(25) NULL
	, [SHCEXP] NUMERIC(18,0) NULL
	, [SHORBY] NCHAR(10) NULL
	, [SHTKBY] NCHAR(10) NULL
	, [SHDOC1] FLOAT NULL
	, [SHDCT4] NCHAR(2) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F42019_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F42019] PRIMARY KEY ([SHKCOO] ASC, [SHDOCO] ASC, [SHDCTO] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F42019] - End */

/* SHADOW TABLE FOR [BUSDTA].[F42019] - Start */
IF OBJECT_ID('[BUSDTA].[F42019_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F42019_del]
	(
	  [SHKCOO] NCHAR(5)
	, [SHDOCO] NUMERIC(8,0)
	, [SHDCTO] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SHKCOO] ASC, [SHDOCO] ASC, [SHDCTO] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F42019] - End */
/* TRIGGERS FOR F42019 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F42019_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F42019_ins
	ON BUSDTA.F42019 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F42019_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F42019_del.SHDCTO= inserted.SHDCTO AND BUSDTA.F42019_del.SHDOCO= inserted.SHDOCO AND BUSDTA.F42019_del.SHKCOO= inserted.SHKCOO
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F42019_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F42019_upd
	ON BUSDTA.F42019 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F42019
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F42019.SHDCTO= inserted.SHDCTO AND BUSDTA.F42019.SHDOCO= inserted.SHDOCO AND BUSDTA.F42019.SHKCOO= inserted.SHKCOO');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F42019_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F42019_dlt
	ON BUSDTA.F42019 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F42019_del (SHDCTO, SHDOCO, SHKCOO, last_modified )
	SELECT deleted.SHDCTO, deleted.SHDOCO, deleted.SHKCOO, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F42019 - END */


/* [BUSDTA].[F42119] - begins */

/* TableDDL - [BUSDTA].[F42119] - Start */
IF OBJECT_ID('[BUSDTA].[F42119]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F42119]
	(
	  [SDKCOO] NCHAR(5) NOT NULL
	, [SDDOCO] NUMERIC(8,0) NOT NULL
	, [SDDCTO] NCHAR(2) NOT NULL
	, [SDLNID] NUMERIC(7,0) NOT NULL
	, [SDMCU] NCHAR(12) NULL
	, [SDRKCO] NCHAR(5) NULL
	, [SDRORN] NCHAR(8) NULL
	, [SDRCTO] NCHAR(2) NULL
	, [SDRLLN] FLOAT NULL
	, [SDAN8] FLOAT NULL
	, [SDSHAN] FLOAT NULL
	, [SDPA8] FLOAT NULL
	, [SDDRQJ] NUMERIC(18,0) NULL
	, [SDTRDJ] NUMERIC(18,0) NULL
	, [SDPDDJ] NUMERIC(18,0) NULL
	, [SDADDJ] NUMERIC(18,0) NULL
	, [SDIVD] NUMERIC(18,0) NULL
	, [SDCNDJ] NUMERIC(18,0) NULL
	, [SDDGL] NUMERIC(18,0) NULL
	, [SDPEFJ] NUMERIC(18,0) NULL
	, [SDVR01] NCHAR(25) NULL
	, [SDVR02] NCHAR(25) NULL
	, [SDITM] FLOAT NULL
	, [SDLITM] NCHAR(25) NULL
	, [SDAITM] NCHAR(25) NULL
	, [SDLOCN] NCHAR(20) NULL
	, [SDLOTN] NCHAR(30) NULL
	, [SDDSC1] NCHAR(30) NULL
	, [SDDSC2] NCHAR(30) NULL
	, [SDLNTY] NCHAR(2) NULL
	, [SDNXTR] NCHAR(3) NULL
	, [SDLTTR] NCHAR(3) NULL
	, [SDEMCU] NCHAR(12) NULL
	, [SDSRP1] NCHAR(3) NULL
	, [SDSRP2] NCHAR(3) NULL
	, [SDSRP3] NCHAR(3) NULL
	, [SDSRP4] NCHAR(3) NULL
	, [SDSRP5] NCHAR(3) NULL
	, [SDPRP1] NCHAR(3) NULL
	, [SDPRP2] NCHAR(3) NULL
	, [SDPRP3] NCHAR(3) NULL
	, [SDPRP4] NCHAR(3) NULL
	, [SDPRP5] NCHAR(3) NULL
	, [SDUOM] NCHAR(2) NULL
	, [SDUORG] FLOAT NULL
	, [SDSOQS] FLOAT NULL
	, [SDSOBK] FLOAT NULL
	, [SDSOCN] FLOAT NULL
	, [SDUPRC] FLOAT NULL
	, [SDAEXP] FLOAT NULL
	, [SDPROV] NCHAR(1) NULL
	, [SDINMG] NCHAR(10) NULL
	, [SDPTC] NCHAR(3) NULL
	, [SDASN] NCHAR(8) NULL
	, [SDPRGR] NCHAR(8) NULL
	, [SDCLVL] NCHAR(3) NULL
	, [SDKCO] NCHAR(5) NULL
	, [SDDOC] FLOAT NULL
	, [SDDCT] NCHAR(2) NULL
	, [SDTAX1] NCHAR(1) NULL
	, [SDTXA1] NCHAR(10) NULL
	, [SDEXR1] NCHAR(2) NULL
	, [SDATXT] NCHAR(1) NULL
	, [SDROUT] NCHAR(3) NULL
	, [SDSTOP] NCHAR(3) NULL
	, [SDZON] NCHAR(3) NULL
	, [SDFRTH] NCHAR(3) NULL
	, [SDUOM1] NCHAR(2) NULL
	, [SDPQOR] FLOAT NULL
	, [SDUOM2] NCHAR(2) NULL
	, [SDSQOR] FLOAT NULL
	, [SDUOM4] NCHAR(2) NULL
	, [SDRPRC] NCHAR(8) NULL
	, [SDORPR] NCHAR(8) NULL
	, [SDORP] NCHAR(1) NULL
	, [SDGLC] NCHAR(4) NULL
	, [SDCTRY] FLOAT NULL
	, [SDFY] FLOAT NULL
	, [SDACOM] NCHAR(1) NULL
	, [SDCMCG] NCHAR(8) NULL
	, [SDRCD] NCHAR(3) NULL
	, [SDUPC1] NCHAR(2) NULL
	, [SDUPC2] NCHAR(2) NULL
	, [SDUPC3] NCHAR(2) NULL
	, [SDTORG] NCHAR(10) NULL
	, [SDVR03] NCHAR(25) NULL
	, [SDNUMB] FLOAT NULL
	, [SDAAID] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F42119_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F42119] PRIMARY KEY ([SDKCOO] ASC, [SDDOCO] ASC, [SDDCTO] ASC, [SDLNID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F42119] - End */

/* SHADOW TABLE FOR [BUSDTA].[F42119] - Start */
IF OBJECT_ID('[BUSDTA].[F42119_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F42119_del]
	(
	  [SDKCOO] NCHAR(5)
	, [SDDOCO] NUMERIC(8,0)
	, [SDDCTO] NCHAR(2)
	, [SDLNID] NUMERIC(7,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SDKCOO] ASC, [SDDOCO] ASC, [SDDCTO] ASC, [SDLNID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F42119] - End */
/* TRIGGERS FOR F42119 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F42119_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F42119_ins
	ON BUSDTA.F42119 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F42119_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F42119_del.SDDCTO= inserted.SDDCTO AND BUSDTA.F42119_del.SDDOCO= inserted.SDDOCO AND BUSDTA.F42119_del.SDKCOO= inserted.SDKCOO AND BUSDTA.F42119_del.SDLNID= inserted.SDLNID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F42119_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F42119_upd
	ON BUSDTA.F42119 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F42119
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F42119.SDDCTO= inserted.SDDCTO AND BUSDTA.F42119.SDDOCO= inserted.SDDOCO AND BUSDTA.F42119.SDKCOO= inserted.SDKCOO AND BUSDTA.F42119.SDLNID= inserted.SDLNID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F42119_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F42119_dlt
	ON BUSDTA.F42119 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F42119_del (SDDCTO, SDDOCO, SDKCOO, SDLNID, last_modified )
	SELECT deleted.SDDCTO, deleted.SDDOCO, deleted.SDKCOO, deleted.SDLNID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F42119 - END */


/* [BUSDTA].[F56M0000] - begins */

/* TableDDL - [BUSDTA].[F56M0000] - Start */
IF OBJECT_ID('[BUSDTA].[F56M0000]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F56M0000]
	(
	  [GFSY] NCHAR(4) NOT NULL
	, [GFCXPJ] NUMERIC(18,0) NULL
	, [GFDTEN] NUMERIC(18,0) NULL
	, [GFLAVJ] NUMERIC(18,0) NULL
	, [GFOBJ] NCHAR(6) NULL
	, [GFMCU] NCHAR(12) NULL
	, [GFSUB] NCHAR(8) NULL
	, [GFPST] NCHAR(1) NULL
	, [GFEV01] NCHAR(1) NULL
	, [GFEV02] NCHAR(1) NULL
	, [GFEV03] NCHAR(1) NULL
	, [GFMATH01] FLOAT NULL
	, [GFMATH02] FLOAT NULL
	, [GFMATH03] FLOAT NULL
	, [GFCFSTR1] NCHAR(3) NULL
	, [GFCFSTR2] NCHAR(8) NULL
	, [GFGS1A] NCHAR(10) NULL
	, [GFGS1B] NCHAR(10) NULL
	, [GFGS2A] NCHAR(20) NULL
	, [GFGS2B] NCHAR(20) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F56M0000_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F56M0000] PRIMARY KEY ([GFSY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F56M0000] - End */

/* SHADOW TABLE FOR [BUSDTA].[F56M0000] - Start */
IF OBJECT_ID('[BUSDTA].[F56M0000_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F56M0000_del]
	(
	  [GFSY] NCHAR(4)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([GFSY] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[F56M0000] - End */
/* TRIGGERS FOR F56M0000 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F56M0000_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F56M0000_ins
	ON BUSDTA.F56M0000 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F56M0000_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F56M0000_del.GFSY= inserted.GFSY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F56M0000_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F56M0000_upd
	ON BUSDTA.F56M0000 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F56M0000
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F56M0000.GFSY= inserted.GFSY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F56M0000_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F56M0000_dlt
	ON BUSDTA.F56M0000 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F56M0000_del (GFSY, last_modified )
	SELECT deleted.GFSY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F56M0000 - END */


/* [BUSDTA].[F56M0001] - begins */

/* TableDDL - [BUSDTA].[F56M0001] - Start */
IF OBJECT_ID('[BUSDTA].[F56M0001]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F56M0001]
	(
	  [FFUSER] NCHAR(10) NOT NULL
	, [FFROUT] NCHAR(3) NOT NULL
	, [FFMCU] NCHAR(12) NOT NULL
	, [FFHMCU] NCHAR(12) NULL
	, [FFBUVAL] NCHAR(12) NULL
	, [FFAN8] FLOAT NULL
	, [FFPA8] FLOAT NULL
	, [FFSTOP] NCHAR(3) NULL
	, [FFZON] NCHAR(3) NULL
	, [FFLOCN] NCHAR(20) NULL
	, [FFLOCF] NCHAR(20) NULL
	, [FFEV01] NCHAR(1) NULL
	, [FFEV02] NCHAR(1) NULL
	, [FFEV03] NCHAR(1) NULL
	, [FFMATH01] FLOAT NULL
	, [FFMATH02] FLOAT NULL
	, [FFMATH03] FLOAT NULL
	, [FFCXPJ] NUMERIC(18,0) NULL
	, [FFCLRJ] NUMERIC(18,0) NULL
	, [FFDTE] NUMERIC(18,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F56M0001_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F56M0001] PRIMARY KEY ([FFUSER] ASC, [FFROUT] ASC, [FFMCU] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F56M0001] - End */

/* SHADOW TABLE FOR [BUSDTA].[F56M0001] - Start */
IF OBJECT_ID('[BUSDTA].[F56M0001_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F56M0001_del]
	(
	  [FFUSER] NCHAR(10)
	, [FFROUT] NCHAR(3)
	, [FFMCU] NCHAR(12)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([FFUSER] ASC, [FFROUT] ASC, [FFMCU] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F56M0001] - End */
/* TRIGGERS FOR F56M0001 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F56M0001_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F56M0001_ins
	ON BUSDTA.F56M0001 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F56M0001_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F56M0001_del.FFMCU= inserted.FFMCU AND BUSDTA.F56M0001_del.FFROUT= inserted.FFROUT AND BUSDTA.F56M0001_del.FFUSER= inserted.FFUSER
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F56M0001_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F56M0001_upd
	ON BUSDTA.F56M0001 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F56M0001
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F56M0001.FFMCU= inserted.FFMCU AND BUSDTA.F56M0001.FFROUT= inserted.FFROUT AND BUSDTA.F56M0001.FFUSER= inserted.FFUSER');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F56M0001_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F56M0001_dlt
	ON BUSDTA.F56M0001 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F56M0001_del (FFMCU, FFROUT, FFUSER, last_modified )
	SELECT deleted.FFMCU, deleted.FFROUT, deleted.FFUSER, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F56M0001 - END */

/* [BUSDTA].[F90CA003] - begins */

/* TableDDL - [BUSDTA].[F90CA003] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA003]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA003]
	(
	  [SMAN8] FLOAT NOT NULL
	, [SMSLSM] FLOAT NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F90CA003_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F90CA003] PRIMARY KEY ([SMAN8] ASC, [SMSLSM] ASC)
	)

END
/* TableDDL - [BUSDTA].[F90CA003] - End */

/* SHADOW TABLE FOR [BUSDTA].[F90CA003] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA003_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA003_del]
	(
	  [SMAN8] FLOAT
	, [SMSLSM] FLOAT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SMAN8] ASC, [SMSLSM] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[F90CA003] - End */
/* TRIGGERS FOR F90CA003 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F90CA003_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F90CA003_ins
	ON BUSDTA.F90CA003 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F90CA003_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F90CA003_del.SMAN8= inserted.SMAN8 AND BUSDTA.F90CA003_del.SMSLSM= inserted.SMSLSM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F90CA003_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA003_upd
	ON BUSDTA.F90CA003 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F90CA003
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F90CA003.SMAN8= inserted.SMAN8 AND BUSDTA.F90CA003.SMSLSM= inserted.SMSLSM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F90CA003_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA003_dlt
	ON BUSDTA.F90CA003 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F90CA003_del (SMAN8, SMSLSM, last_modified )
	SELECT deleted.SMAN8, deleted.SMSLSM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F90CA003 - END */
/* [BUSDTA].[F90CA042] - begins */

/* TableDDL - [BUSDTA].[F90CA042] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA042]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA042]
	(
	  [EMAN8] NUMERIC(8,0) NOT NULL
	, [EMPA8] NUMERIC(8,0) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F90CA042_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F90CA042] PRIMARY KEY ([EMAN8] ASC, [EMPA8] ASC)
	)

END
/* TableDDL - [BUSDTA].[F90CA042] - End */

/* SHADOW TABLE FOR [BUSDTA].[F90CA042] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA042_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA042_del]
	(
	  [EMAN8] NUMERIC(8,0)
	, [EMPA8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EMAN8] ASC, [EMPA8] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[F90CA042] - End */
/* TRIGGERS FOR F90CA042 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F90CA042_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F90CA042_ins
	ON BUSDTA.F90CA042 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F90CA042_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F90CA042_del.EMAN8= inserted.EMAN8 AND BUSDTA.F90CA042_del.EMPA8= inserted.EMPA8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F90CA042_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA042_upd
	ON BUSDTA.F90CA042 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F90CA042
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F90CA042.EMAN8= inserted.EMAN8 AND BUSDTA.F90CA042.EMPA8= inserted.EMPA8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F90CA042_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA042_dlt
	ON BUSDTA.F90CA042 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F90CA042_del (EMAN8, EMPA8, last_modified )
	SELECT deleted.EMAN8, deleted.EMPA8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F90CA042 - END */
/* [BUSDTA].[F90CA086] - begins */

/* TableDDL - [BUSDTA].[F90CA086] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA086]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA086]
	(
	  [CRCUAN8] NUMERIC(8,0) NOT NULL
	, [CRCRAN8] NUMERIC(8,0) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F90CA086_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F90CA086] PRIMARY KEY ([CRCUAN8] ASC, [CRCRAN8] ASC)
	)

END
/* TableDDL - [BUSDTA].[F90CA086] - End */

/* SHADOW TABLE FOR [BUSDTA].[F90CA086] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA086_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA086_del]
	(
	  [CRCUAN8] NUMERIC(8,0)
	, [CRCRAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CRCUAN8] ASC, [CRCRAN8] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[F90CA086] - End */
/* TRIGGERS FOR F90CA086 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F90CA086_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F90CA086_ins
	ON BUSDTA.F90CA086 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F90CA086_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F90CA086_del.CRCRAN8= inserted.CRCRAN8 AND BUSDTA.F90CA086_del.CRCUAN8= inserted.CRCUAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F90CA086_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA086_upd
	ON BUSDTA.F90CA086 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F90CA086
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F90CA086.CRCRAN8= inserted.CRCRAN8 AND BUSDTA.F90CA086.CRCUAN8= inserted.CRCUAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F90CA086_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA086_dlt
	ON BUSDTA.F90CA086 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F90CA086_del (CRCRAN8, CRCUAN8, last_modified )
	SELECT deleted.CRCRAN8, deleted.CRCUAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F90CA086 - END */

/* [BUSDTA].[M0111] - begins */

/* TableDDL - [BUSDTA].[M0111] - Start */
IF OBJECT_ID('[BUSDTA].[M0111]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0111]
	(
	  [CDAN8] NUMERIC(8,0) NOT NULL
	, [CDID] NUMERIC(8,0) NOT NULL
	, [CDIDLN] NUMERIC(5,0) NOT NULL
	, [CDRCK7] NUMERIC(5,0) NOT NULL
	, [CDCNLN] NUMERIC(5,0) NOT NULL
	, [CDAR1] NCHAR(6) NULL
	, [CDPH1] NCHAR(20) NULL
	, [CDEXTN1] NCHAR(8) NULL
	, [CDPHTP1] NUMERIC(8,0) NULL
	, [CDDFLTPH1] BIT NULL
	, [CDREFPH1] INT NULL
	, [CDAR2] NCHAR(6) NULL
	, [CDPH2] NCHAR(20) NULL
	, [CDEXTN2] NCHAR(8) NULL
	, [CDPHTP2] NUMERIC(8,0) NULL
	, [CDDFLTPH2] BIT NULL
	, [CDREFPH2] INT NULL
	, [CDAR3] NCHAR(6) NULL
	, [CDPH3] NCHAR(20) NULL
	, [CDEXTN3] NCHAR(8) NULL
	, [CDPHTP3] NUMERIC(8,0) NULL
	, [CDDFLTPH3] BIT NULL
	, [CDREFPH3] INT NULL
	, [CDAR4] NCHAR(6) NULL
	, [CDPH4] NCHAR(20) NULL
	, [CDEXTN4] NCHAR(8) NULL
	, [CDPHTP4] NUMERIC(8,0) NULL
	, [CDDFLTPH4] BIT NULL
	, [CDREFPH4] INT NULL
	, [CDEMAL1] NVARCHAR(256) NULL
	, [CDETP1] NUMERIC(3,0) NULL
	, [CDDFLTEM1] BIT NULL
	, [CDREFEM1] INT NULL
	, [CDEMAL2] NVARCHAR(256) NULL
	, [CDETP2] NUMERIC(3,0) NULL
	, [CDDFLTEM2] BIT NULL
	, [CDREFEM2] INT NULL
	, [CDEMAL3] NVARCHAR(256) NULL
	, [CDETP3] NUMERIC(3,0) NULL
	, [CDDFLTEM3] BIT NULL
	, [CDREFEM3] INT NULL
	, [CDGNNM] NCHAR(75) NULL
	, [CDMDNM] NCHAR(25) NULL
	, [CDSRNM] NCHAR(25) NULL
	, [CDTITL] NUMERIC(3,0) NULL
	, [CDACTV] BIT NULL
	, [CDDFLT] BIT NULL
	, [CDSET] INT NULL
	, [CDCRBY] NCHAR(25) NULL
	, [CDCRDT] DATETIME NULL
	, [CDUPBY] NCHAR(25) NULL
	, [CDUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M0111_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M0111] PRIMARY KEY ([CDAN8] ASC, [CDID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M0111] - End */

/* SHADOW TABLE FOR [BUSDTA].[M0111] - Start */
IF OBJECT_ID('[BUSDTA].[M0111_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0111_del]
	(
	  [CDAN8] NUMERIC(8,0)
	, [CDID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CDAN8] ASC, [CDID] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M0111] - End */
/* TRIGGERS FOR M0111 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M0111_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M0111_ins
	ON BUSDTA.M0111 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M0111_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M0111_del.CDAN8= inserted.CDAN8 AND BUSDTA.M0111_del.CDID= inserted.CDID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M0111_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0111_upd
	ON BUSDTA.M0111 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M0111
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M0111.CDAN8= inserted.CDAN8 AND BUSDTA.M0111.CDID= inserted.CDID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M0111_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0111_dlt
	ON BUSDTA.M0111 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M0111_del (CDAN8, CDID, last_modified )
	SELECT deleted.CDAN8, deleted.CDID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M0111 - END */


/* [BUSDTA].[M0112] - begins */

/* TableDDL - [BUSDTA].[M0112] - Start */
IF OBJECT_ID('[BUSDTA].[M0112]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0112]
	(
	  [NDAN8] NUMERIC(8,0) NOT NULL
	, [NDID] NUMERIC(8,0) NOT NULL
	, [NDDTTM] DATETIME NULL
	, [NDDTLS] NCHAR(500) NULL
	, [NDTYP] NCHAR(15) NULL
	, [NDDFLT] BIT NULL
	, [NDCRBY] NCHAR(50) NULL
	, [NDCRDT] DATE NULL
	, [NDUPBY] NCHAR(50) NULL
	, [NDUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M0112_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M0112] PRIMARY KEY ([NDAN8] ASC, [NDID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M0112] - End */

/* SHADOW TABLE FOR [BUSDTA].[M0112] - Start */
IF OBJECT_ID('[BUSDTA].[M0112_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0112_del]
	(
	  [NDAN8] NUMERIC(8,0)
	, [NDID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([NDAN8] ASC, [NDID] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M0112] - End */
/* TRIGGERS FOR M0112 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M0112_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M0112_ins
	ON BUSDTA.M0112 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M0112_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M0112_del.NDAN8= inserted.NDAN8 AND BUSDTA.M0112_del.NDID= inserted.NDID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M0112_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0112_upd
	ON BUSDTA.M0112 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M0112
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M0112.NDAN8= inserted.NDAN8 AND BUSDTA.M0112.NDID= inserted.NDID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M0112_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0112_dlt
	ON BUSDTA.M0112 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M0112_del (NDAN8, NDID, last_modified )
	SELECT deleted.NDAN8, deleted.NDID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M0112 - END */


/* [BUSDTA].[M0140] - begins */

/* TableDDL - [BUSDTA].[M0140] - Start */
IF OBJECT_ID('[BUSDTA].[M0140]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0140]
	(
	  [RMMCU] NCHAR(12) NOT NULL
	, [RMAN8] NUMERIC(8,0) NOT NULL
	, [RMDTAI] NCHAR(10) NOT NULL
	, [RMCAN8] NUMERIC(8,0) NULL
	, [RMYN] NCHAR(2) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M0140_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M0140] PRIMARY KEY ([RMMCU] ASC, [RMAN8] ASC, [RMDTAI] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M0140] - End */

/* SHADOW TABLE FOR [BUSDTA].[M0140] - Start */
IF OBJECT_ID('[BUSDTA].[M0140_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0140_del]
	(
	  [RMMCU] NCHAR(12)
	, [RMAN8] NUMERIC(8,0)
	, [RMDTAI] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RMMCU] ASC, [RMAN8] ASC, [RMDTAI] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M0140] - End */
/* TRIGGERS FOR M0140 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M0140_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M0140_ins
	ON BUSDTA.M0140 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M0140_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M0140_del.RMAN8= inserted.RMAN8 AND BUSDTA.M0140_del.RMDTAI= inserted.RMDTAI AND BUSDTA.M0140_del.RMMCU= inserted.RMMCU
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M0140_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0140_upd
	ON BUSDTA.M0140 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M0140
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M0140.RMAN8= inserted.RMAN8 AND BUSDTA.M0140.RMDTAI= inserted.RMDTAI AND BUSDTA.M0140.RMMCU= inserted.RMMCU');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M0140_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0140_dlt
	ON BUSDTA.M0140 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M0140_del (RMAN8, RMDTAI, RMMCU, last_modified )
	SELECT deleted.RMAN8, deleted.RMDTAI, deleted.RMMCU, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M0140 - END */


/* [BUSDTA].[M03011] - begins */

/* TableDDL - [BUSDTA].[M03011] - Start */
IF OBJECT_ID('[BUSDTA].[M03011]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03011]
	(
	  [CSAN8] NUMERIC(8,0) NOT NULL
	, [CSCO] VARCHAR(5) NOT NULL
	, [CSUAMT] NUMERIC(8,4) NULL
	, [CSOBAL] NUMERIC(8,4) NULL
	, [CSCRBY] NCHAR(10) NULL
	, [CSCRDT] DATE NULL
	, [CSUPBY] NCHAR(10) NULL
	, [CSUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M03011_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M03011] PRIMARY KEY ([CSAN8] ASC, [CSCO] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M03011] - End */

/* SHADOW TABLE FOR [BUSDTA].[M03011] - Start */
IF OBJECT_ID('[BUSDTA].[M03011_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03011_del]
	(
	  [CSAN8] NUMERIC(8,0)
	, [CSCO] VARCHAR(5)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CSAN8] ASC, [CSCO] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M03011] - End */
/* TRIGGERS FOR M03011 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M03011_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M03011_ins
	ON BUSDTA.M03011 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M03011_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M03011_del.CSAN8= inserted.CSAN8 AND BUSDTA.M03011_del.CSCO= inserted.CSCO
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M03011_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03011_upd
	ON BUSDTA.M03011 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M03011
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M03011.CSAN8= inserted.CSAN8 AND BUSDTA.M03011.CSCO= inserted.CSCO');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M03011_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03011_dlt
	ON BUSDTA.M03011 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M03011_del (CSAN8, CSCO, last_modified )
	SELECT deleted.CSAN8, deleted.CSCO, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M03011 - END */


/* [BUSDTA].[M03042] - begins */

/* TableDDL - [BUSDTA].[M03042] - Start */
IF OBJECT_ID('[BUSDTA].[M03042]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03042]
	(
	  [PDAN8] NUMERIC(8,0) NOT NULL
	, [PDCO] VARCHAR(5) NOT NULL
	, [PDID] NUMERIC(8,0) NOT NULL
	, [PDPAMT] NUMERIC(8,4) NULL
	, [PDPMODE] BIT NULL
	, [PDCHQNO] NCHAR(10) NULL
	, [PDCHQDT] DATE NULL
	, [PDCRBY] NCHAR(10) NULL
	, [PDCRDT] DATE NULL
	, [PDUPBY] NCHAR(10) NULL
	, [PDUPDT] DATE NULL
	, [PDRCID] INT NULL
	, [PDTRMD] BIT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M03042_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M03042] PRIMARY KEY ([PDAN8] ASC, [PDCO] ASC, [PDID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M03042] - End */

/* SHADOW TABLE FOR [BUSDTA].[M03042] - Start */
IF OBJECT_ID('[BUSDTA].[M03042_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03042_del]
	(
	  [PDAN8] NUMERIC(8,0)
	, [PDCO] VARCHAR(5)
	, [PDID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PDAN8] ASC, [PDCO] ASC, [PDID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[M03042] - End */
/* TRIGGERS FOR M03042 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M03042_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M03042_ins
	ON BUSDTA.M03042 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M03042_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M03042_del.PDAN8= inserted.PDAN8 AND BUSDTA.M03042_del.PDCO= inserted.PDCO AND BUSDTA.M03042_del.PDID= inserted.PDID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M03042_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03042_upd
	ON BUSDTA.M03042 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M03042
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M03042.PDAN8= inserted.PDAN8 AND BUSDTA.M03042.PDCO= inserted.PDCO AND BUSDTA.M03042.PDID= inserted.PDID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M03042_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03042_dlt
	ON BUSDTA.M03042 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M03042_del (PDAN8, PDCO, PDID, last_modified )
	SELECT deleted.PDAN8, deleted.PDCO, deleted.PDID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M03042 - END */

/* [BUSDTA].[M04012] - begins */

/* TableDDL - [BUSDTA].[M04012] - Start */
IF OBJECT_ID('[BUSDTA].[M04012]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M04012]
	(
	  [PMAN8] NUMERIC(8,0) NOT NULL
	, [PMALPH] NCHAR(40) NULL
	, [PMROUT] NCHAR(10) NOT NULL
	, [PMSRS] INT NULL
	, [PMCRBY] NCHAR(25) NULL
	, [PMCRDT] DATE NULL
	, [PMUPBY] NCHAR(25) NULL
	, [PMUPDT] DATE NULL
	, [PMIBN] VARCHAR(50) NULL
	, [PMISTP] VARCHAR(50) NULL
	, [PMIBGP] VARCHAR(50) NULL
	, [PMILCT] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M04012_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M04012] PRIMARY KEY ([PMAN8] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[M04012] - End */

/* SHADOW TABLE FOR [BUSDTA].[M04012] - Start */
IF OBJECT_ID('[BUSDTA].[M04012_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M04012_del]
	(
	  [PMAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PMAN8] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M04012] - End */
/* TRIGGERS FOR M04012 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M04012_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M04012_ins
	ON BUSDTA.M04012 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M04012_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M04012_del.PMAN8= inserted.PMAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M04012_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M04012_upd
	ON BUSDTA.M04012 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M04012
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M04012.PMAN8= inserted.PMAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M04012_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M04012_dlt
	ON BUSDTA.M04012 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M04012_del (PMAN8, last_modified )
	SELECT deleted.PMAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M04012 - END */


/* [BUSDTA].[M080111] - begins */

/* TableDDL - [BUSDTA].[M080111] - Start */
IF OBJECT_ID('[BUSDTA].[M080111]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M080111]
	(
	  [CTID] NUMERIC(8,0) NOT NULL
	, [CTTYP] NCHAR(10) NULL
	, [CTCD] NCHAR(5) NOT NULL
	, [CTDSC1] NCHAR(50) NULL
	, [CTTXA1] NCHAR(10) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M080111_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M080111] PRIMARY KEY ([CTID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M080111] - End */

/* SHADOW TABLE FOR [BUSDTA].[M080111] - Start */
IF OBJECT_ID('[BUSDTA].[M080111_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M080111_del]
	(
	  [CTID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CTID] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M080111] - End */
/* TRIGGERS FOR M080111 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M080111_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M080111_ins
	ON BUSDTA.M080111 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M080111_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M080111_del.CTID= inserted.CTID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M080111_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M080111_upd
	ON BUSDTA.M080111 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M080111
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M080111.CTID= inserted.CTID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M080111_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M080111_dlt
	ON BUSDTA.M080111 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M080111_del (CTID, last_modified )
	SELECT deleted.CTID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M080111 - END */


/* [BUSDTA].[M40111] - begins */

/* TableDDL - [BUSDTA].[M40111] - Start */
IF OBJECT_ID('[BUSDTA].[M40111]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M40111]
	(
	  [PCAN8] NUMERIC(8,0) NOT NULL
	, [PCIDLN] NUMERIC(5,0) NOT NULL
	, [PCAR1] NVARCHAR(6) NULL
	, [PCPH1] NVARCHAR(20) NULL
	, [PCEXTN1] NVARCHAR(8) NULL
	, [PCPHTP1] NUMERIC(8,0) NULL
	, [PCDFLTPH1] BIT NULL CONSTRAINT DF_M40111_PCDFLTPH1 DEFAULT((1))
	, [PCREFPH1] INT NULL
	, [PCAR2] NVARCHAR(6) NULL
	, [PCPH2] NVARCHAR(20) NULL
	, [PCEXTN2] NVARCHAR(8) NULL
	, [PCPHTP2] NUMERIC(8,0) NULL
	, [PCDFLTPH2] BIT NULL CONSTRAINT DF_M40111_PCDFLTPH2 DEFAULT((0))
	, [PCREFPH2] INT NULL
	, [PCAR3] NVARCHAR(6) NULL
	, [PCPH3] NVARCHAR(20) NULL
	, [PCEXTN3] NVARCHAR(8) NULL
	, [PCPHTP3] NUMERIC(8,0) NULL
	, [PCDFLTPH3] BIT NULL CONSTRAINT DF_M40111_PCDFLTPH3 DEFAULT((0))
	, [PCREFPH3] INT NULL
	, [PCAR4] NVARCHAR(6) NULL
	, [PCPH4] NVARCHAR(20) NULL
	, [PCEXTN4] NVARCHAR(8) NULL
	, [PCPHTP4] NUMERIC(8,0) NULL
	, [PCDFLTPH4] BIT NULL CONSTRAINT DF_M40111_PCDFLTPH4 DEFAULT((0))
	, [PCREFPH4] INT NULL
	, [PCEMAL1] NVARCHAR(256) NULL
	, [PCETP1] NUMERIC(3,0) NULL
	, [PCDFLTEM1] BIT NULL CONSTRAINT DF_M40111_PCDFLTEM1 DEFAULT((1))
	, [PCREFEM1] INT NULL
	, [PCEMAL2] NVARCHAR(256) NULL
	, [PCETP2] NUMERIC(3,0) NULL
	, [PCDFLTEM2] BIT NULL CONSTRAINT DF_M40111_PCDFLTEM2 DEFAULT((0))
	, [PCREFEM2] INT NULL
	, [PCEMAL3] NVARCHAR(256) NULL
	, [PCETP3] NUMERIC(3,0) NULL
	, [PCDFLTEM3] BIT NULL CONSTRAINT DF_M40111_PCDFLTEM3 DEFAULT((0))
	, [PCREFEM3] INT NULL
	, [PCGNNM] NVARCHAR(75) NULL
	, [PCMDNM] NVARCHAR(25) NULL
	, [PCSRNM] NVARCHAR(25) NULL
	, [PCTITL] NVARCHAR(50) NULL
	, [PCACTV] BIT NULL
	, [PCDFLT] BIT NULL CONSTRAINT DF_M40111_PCDFLT DEFAULT((0))
	, [PCSET] INT NULL
	, [PCCRBY] NCHAR(25) NULL
	, [PCCRDT] DATE NULL
	, [PCUPBY] NCHAR(25) NULL
	, [PCUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M40111_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M40111] PRIMARY KEY ([PCAN8] ASC, [PCIDLN] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M40111] - End */

/* SHADOW TABLE FOR [BUSDTA].[M40111] - Start */
IF OBJECT_ID('[BUSDTA].[M40111_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M40111_del]
	(
	  [PCAN8] NUMERIC(8,0)
	, [PCIDLN] NUMERIC(5,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PCAN8] ASC, [PCIDLN] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[M40111] - End */
/* TRIGGERS FOR M40111 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M40111_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M40111_ins
	ON BUSDTA.M40111 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M40111_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M40111_del.PCAN8= inserted.PCAN8 AND BUSDTA.M40111_del.PCIDLN= inserted.PCIDLN
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M40111_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M40111_upd
	ON BUSDTA.M40111 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M40111
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M40111.PCAN8= inserted.PCAN8 AND BUSDTA.M40111.PCIDLN= inserted.PCIDLN');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M40111_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M40111_dlt
	ON BUSDTA.M40111 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M40111_del (PCAN8, PCIDLN, last_modified )
	SELECT deleted.PCAN8, deleted.PCIDLN, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M40111 - END */


/* [BUSDTA].[M40116] - begins */

/* TableDDL - [BUSDTA].[M40116] - Start */
IF OBJECT_ID('[BUSDTA].[M40116]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M40116]
	(
	  [PAAN8] NUMERIC(8,0) NOT NULL
	, [PAADD1] NCHAR(40) NULL
	, [PAADD2] NCHAR(40) NULL
	, [PAADD3] NCHAR(40) NULL
	, [PAADD4] NCHAR(40) NULL
	, [PAADDZ] NCHAR(12) NULL
	, [PACTY1] NCHAR(25) NULL
	, [PACOUN] NCHAR(25) NULL
	, [PAADDS] NCHAR(3) NULL
	, [PACRBY] NCHAR(50) NULL
	, [PACRDT] DATE NULL
	, [PAUPBY] NCHAR(50) NULL
	, [PAUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M40116_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M40116] PRIMARY KEY ([PAAN8] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M40116] - End */

/* SHADOW TABLE FOR [BUSDTA].[M40116] - Start */
IF OBJECT_ID('[BUSDTA].[M40116_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M40116_del]
	(
	  [PAAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PAAN8] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M40116] - End */
/* TRIGGERS FOR M40116 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M40116_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M40116_ins
	ON BUSDTA.M40116 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M40116_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M40116_del.PAAN8= inserted.PAAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M40116_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M40116_upd
	ON BUSDTA.M40116 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M40116
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M40116.PAAN8= inserted.PAAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M40116_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M40116_dlt
	ON BUSDTA.M40116 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M40116_del (PAAN8, last_modified )
	SELECT deleted.PAAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M40116 - END */


/* [BUSDTA].[M4016] - begins */

/* TableDDL - [BUSDTA].[M4016] - Start */
IF OBJECT_ID('[BUSDTA].[M4016]') IS NULL
BEGIN

CREATE TABLE [BUSDTA].[M4016](
	[POORTP] [varchar](8) NOT NULL,
	[POAN8] [numeric](8, 0) NOT NULL,
	[POITM] [varchar](25) NOT NULL,
	[POSTDT] [date] NOT NULL,
	[POOSEQ] [numeric](4, 0) NULL,
	[POLITM] [nchar](25) NULL,
	[POQTYU] [int] NULL,
	[POUOM] [nchar](2) NULL,
	[POLNTY] [nchar](2) NULL,
	[POSRP1] [nchar](3) NULL,
	[POSRP5] [nchar](3) NULL,
	[POSTFG] [bit] NULL,
	[POCRBY] [nchar](50) NULL,
	[POCRDT] [datetime] NULL,
	[POUPBY] [nchar](50) NULL,
	[POUPDT] [datetime] NULL,
	[last_modified] [datetime] DEFAULT (getdate()),
 CONSTRAINT [PK_M4016] PRIMARY KEY CLUSTERED ([POORTP] ASC,	[POAN8] ASC,[POITM] ASC,[POSTDT] ASC)
) ON [PRIMARY]


END
GO
/* TableDDL - [BUSDTA].[M4016] - End */

/* SHADOW TABLE FOR [BUSDTA].[M4016] - Start */
IF OBJECT_ID('[BUSDTA].[M4016_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M4016_del]
	(
	  [POORTP] VARCHAR(8)
	, [POAN8] NUMERIC(8,0)
	, [POITM] VARCHAR(25)
	, [POSTDT] DATE
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([POORTP] ASC, [POAN8] ASC, [POITM] ASC, [POSTDT] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M4016] - End */
/* TRIGGERS FOR M4016 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M4016_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M4016_ins
	ON BUSDTA.M4016 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M4016_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M4016_del.POAN8= inserted.POAN8 AND BUSDTA.M4016_del.POITM= inserted.POITM AND BUSDTA.M4016_del.POORTP= inserted.POORTP AND BUSDTA.M4016_del.POSTDT= inserted.POSTDT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M4016_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M4016_upd
	ON BUSDTA.M4016 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M4016
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M4016.POAN8= inserted.POAN8 AND BUSDTA.M4016.POITM= inserted.POITM AND BUSDTA.M4016.POORTP= inserted.POORTP AND BUSDTA.M4016.POSTDT= inserted.POSTDT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M4016_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M4016_dlt
	ON BUSDTA.M4016 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M4016_del (POAN8, POITM, POORTP, POSTDT, last_modified )
	SELECT deleted.POAN8, deleted.POITM, deleted.POORTP, deleted.POSTDT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M4016 - END */

/* [BUSDTA].[M5001] - begins */

/* TableDDL - [BUSDTA].[M5001] - Start */
IF OBJECT_ID('[BUSDTA].[M5001]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5001]
	(
	  [TTID] NUMERIC(8,0) NOT NULL
	, [TTKEY] NCHAR(30) NOT NULL
	, [TTTYP] NCHAR(25) NOT NULL
	, [TTACTN] NCHAR(100) NULL
	, [TTADSC] NCHAR(50) NULL
	, [TTACTR] NCHAR(15) NULL
	, [TTSTAT] NCHAR(15) NULL
	, [TTISTRT] BIT NULL
	, [TTIEND] BIT NULL
	, [TTCRBY] NCHAR(50) NULL
	, [TTCRDT] DATETIME NULL
	, [TTUPBY] NCHAR(50) NULL
	, [TTUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M5001_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M5001] PRIMARY KEY ([TTKEY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M5001] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5001] - Start */
IF OBJECT_ID('[BUSDTA].[M5001_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5001_del]
	(
	  [TTKEY] NCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TTKEY] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M5001] - End */
/* TRIGGERS FOR M5001 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5001_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5001_ins
	ON BUSDTA.M5001 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5001_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5001_del.TTKEY= inserted.TTKEY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5001_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5001_upd
	ON BUSDTA.M5001 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5001
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5001.TTKEY= inserted.TTKEY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5001_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5001_dlt
	ON BUSDTA.M5001 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5001_del (TTKEY, last_modified )
	SELECT deleted.TTKEY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5001 - END */


/* [BUSDTA].[M50012] - begins */

/* TableDDL - [BUSDTA].[M50012] - Start */
IF OBJECT_ID('[BUSDTA].[M50012]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50012]
	(
	  [TDID] NUMERIC(8,0) NOT NULL
	, [TDROUT] VARCHAR(10) NOT NULL
	, [TDTYP] NCHAR(50) NOT NULL
	, [TDCLASS] NCHAR(100) NULL
	, [TDDTLS] NVARCHAR(MAX) NULL
	, [TDSTRTTM] DATETIME2(7) NULL
	, [TDENDTM] DATETIME2(7) NULL
	, [TDSTID] NCHAR(15) NULL
	, [TDAN8] NCHAR(15) NULL
	, [TDSTTLID] NCHAR(15) NULL
	, [TDPNTID] NUMERIC(8,0) NULL
	, [TDSTAT] NCHAR(100) NULL
	, [TDREFHDRID] NUMERIC(8,0) NULL
	, [TDREFHDRTYP] NCHAR(30) NULL
	, [TDCRBY] NCHAR(50) NULL
	, [TDCRDT] DATETIME NULL
	, [TDUPBY] NCHAR(50) NULL
	, [TDUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M50012_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M50012] PRIMARY KEY ([TDID] ASC, [TDROUT] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M50012] - End */

/* SHADOW TABLE FOR [BUSDTA].[M50012] - Start */
IF OBJECT_ID('[BUSDTA].[M50012_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50012_del]
	(
	  [TDID] NUMERIC(8,0)
	, [TDROUT] VARCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TDID] ASC, [TDROUT] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M50012] - End */
/* TRIGGERS FOR M50012 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M50012_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M50012_ins
	ON BUSDTA.M50012 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M50012_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M50012_del.TDID= inserted.TDID AND BUSDTA.M50012_del.TDROUT= inserted.TDROUT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M50012_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50012_upd
	ON BUSDTA.M50012 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M50012
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M50012.TDID= inserted.TDID AND BUSDTA.M50012.TDROUT= inserted.TDROUT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M50012_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50012_dlt
	ON BUSDTA.M50012 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M50012_del (TDID, TDROUT, last_modified )
	SELECT deleted.TDID, deleted.TDROUT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M50012 - END */


/* [BUSDTA].[M5002] - begins */

/* TableDDL - [BUSDTA].[M5002] - Start */
IF OBJECT_ID('[BUSDTA].[M5002]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5002]
	(
	  [TNID] NUMERIC(8,0) NOT NULL
	, [TNKEY] NCHAR(30) NOT NULL
	, [TNTYP] NCHAR(25) NOT NULL
	, [TNACTN] NCHAR(30) NULL
	, [TNADSC] NCHAR(50) NULL
	, [TNACTR] NCHAR(15) NULL
	, [TNILDGRD] BIT NULL
	, [TNCRBY] NCHAR(50) NULL
	, [TNCRDT] DATETIME NULL
	, [TNUPBY] NCHAR(50) NULL
	, [TNUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M5002_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M5002] PRIMARY KEY ([TNKEY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M5002] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5002] - Start */
IF OBJECT_ID('[BUSDTA].[M5002_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5002_del]
	(
	  [TNKEY] NCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TNKEY] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M5002] - End */
/* TRIGGERS FOR M5002 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5002_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5002_ins
	ON BUSDTA.M5002 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5002_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5002_del.TNKEY= inserted.TNKEY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5002_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5002_upd
	ON BUSDTA.M5002 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5002
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5002.TNKEY= inserted.TNKEY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5002_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5002_dlt
	ON BUSDTA.M5002 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5002_del (TNKEY, last_modified )
	SELECT deleted.TNKEY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5002 - END */


/* [BUSDTA].[M5003] - begins */

/* TableDDL - [BUSDTA].[M5003] - Start */
IF OBJECT_ID('[BUSDTA].[M5003]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5003]
	(
	  [ALID] NUMERIC(8,0) NOT NULL
	, [ALHDID] NUMERIC(8,0) NULL
	, [ALTMSTMP] DATETIME NOT NULL
	, [ALDSC] NCHAR(250) NULL
	, [ALTYP] NCHAR(50) NOT NULL
	, [ALITX] BIT NULL
	, [ALCRBY] NCHAR(50) NULL
	, [ALCRDT] DATETIME NULL
	, [ALUPBY] NCHAR(50) NULL
	, [ALUPDT] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_M5003_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M5003] PRIMARY KEY ([ALID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M5003] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5003] - Start */
IF OBJECT_ID('[BUSDTA].[M5003_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5003_del]
	(
	  [ALID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ALID] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M5003] - End */
/* TRIGGERS FOR M5003 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5003_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5003_ins
	ON BUSDTA.M5003 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5003_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5003_del.ALID= inserted.ALID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5003_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5003_upd
	ON BUSDTA.M5003 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5003
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5003.ALID= inserted.ALID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5003_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5003_dlt
	ON BUSDTA.M5003 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5003_del (ALID, last_modified )
	SELECT deleted.ALID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5003 - END */


/* [BUSDTA].[M5005] - begins */

/* TableDDL - [BUSDTA].[M5005] - Start */
IF OBJECT_ID('[BUSDTA].[M5005]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5005]
	(
	  [SMKEY] VARCHAR(30) NOT NULL
	, [SMDIR] NCHAR(10) NULL
	, [SMPRTY] NCHAR(10) NULL
	, [SMCRBY] NCHAR(50) NULL
	, [SMCRDT] DATETIME NULL
	, [SMUPBY] NCHAR(50) NULL
	, [SMUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M5005_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M5005] PRIMARY KEY ([SMKEY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M5005] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5005] - Start */
IF OBJECT_ID('[BUSDTA].[M5005_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5005_del]
	(
	  [SMKEY] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SMKEY] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M5005] - End */
/* TRIGGERS FOR M5005 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5005_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5005_ins
	ON BUSDTA.M5005 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5005_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5005_del.SMKEY= inserted.SMKEY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5005_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5005_upd
	ON BUSDTA.M5005 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5005
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5005.SMKEY= inserted.SMKEY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5005_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5005_dlt
	ON BUSDTA.M5005 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5005_del (SMKEY, last_modified )
	SELECT deleted.SMKEY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5005 - END */


/* [BUSDTA].[M50052] - begins */

/* TableDDL - [BUSDTA].[M50052] - Start */
IF OBJECT_ID('[BUSDTA].[M50052]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50052]
	(
	  [SDID] NUMERIC(8,0) NOT NULL
	, [SDTXID] NUMERIC(8,0) NOT NULL
	, [SDKEY] NCHAR(15) NULL
	, [SDISYNCD] BIT NOT NULL
	, [SDTMSTMP] DATETIME NULL
	, [SDCRBY] NCHAR(50) NULL
	, [SDCRDT] DATETIME NULL
	, [SDUPBY] NCHAR(50) NULL
	, [SDUPDT] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_M50052_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M50052] PRIMARY KEY ([SDID] ASC, [SDTXID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M50052] - End */

/* SHADOW TABLE FOR [BUSDTA].[M50052] - Start */
IF OBJECT_ID('[BUSDTA].[M50052_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50052_del]
	(
	  [SDID] NUMERIC(8,0)
	, [SDTXID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SDID] ASC, [SDTXID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[M50052] - End */
/* TRIGGERS FOR M50052 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M50052_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M50052_ins
	ON BUSDTA.M50052 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M50052_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M50052_del.SDID= inserted.SDID AND BUSDTA.M50052_del.SDTXID= inserted.SDTXID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M50052_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50052_upd
	ON BUSDTA.M50052 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M50052
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M50052.SDID= inserted.SDID AND BUSDTA.M50052.SDTXID= inserted.SDTXID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M50052_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50052_dlt
	ON BUSDTA.M50052 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M50052_del (SDID, SDTXID, last_modified )
	SELECT deleted.SDID, deleted.SDTXID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M50052 - END */


/* [BUSDTA].[M56M0001] - begins */

/* TableDDL - [BUSDTA].[M56M0001] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0001]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0001]
	(
	  [DMDDC] VARCHAR(3) NOT NULL
	, [DMDDCD] NCHAR(50) NOT NULL
	, [DMSTAT] BIT NOT NULL
	, [DMWPC] INT NOT NULL
	, [DMCRBY] NCHAR(50) NULL
	, [DMCRDT] DATETIME NULL
	, [DMUPBY] NCHAR(50) NULL
	, [DMUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M56M0001_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M56M0001] PRIMARY KEY ([DMDDC] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M56M0001] - End */

/* SHADOW TABLE FOR [BUSDTA].[M56M0001] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0001_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0001_del]
	(
	  [DMDDC] VARCHAR(3)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DMDDC] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M56M0001] - End */
/* TRIGGERS FOR M56M0001 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M56M0001_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M56M0001_ins
	ON BUSDTA.M56M0001 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M56M0001_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M56M0001_del.DMDDC= inserted.DMDDC
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M56M0001_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0001_upd
	ON BUSDTA.M56M0001 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M56M0001
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M56M0001.DMDDC= inserted.DMDDC');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M56M0001_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0001_dlt
	ON BUSDTA.M56M0001 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M56M0001_del (DMDDC, last_modified )
	SELECT deleted.DMDDC, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M56M0001 - END */


/* [BUSDTA].[M56M0002] - begins */

/* TableDDL - [BUSDTA].[M56M0002] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0002]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0002]
	(
	  [DCDDC] VARCHAR(3) NOT NULL
	, [DCWN] NUMERIC(4,0) NOT NULL
	, [DCDN] NUMERIC(4,0) NOT NULL
	, [DCISST] NUMERIC(4,0) NULL
	, [DCCRBY] NCHAR(50) NULL
	, [DCCRDT] DATETIME NULL
	, [DCUPBY] NCHAR(50) NULL
	, [DCUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M56M0002_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M56M0002] PRIMARY KEY ([DCDDC] ASC, [DCWN] ASC, [DCDN] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M56M0002] - End */

/* SHADOW TABLE FOR [BUSDTA].[M56M0002] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0002_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0002_del]
	(
	  [DCDDC] VARCHAR(3)
	, [DCWN] NUMERIC(4,0)
	, [DCDN] NUMERIC(4,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DCDDC] ASC, [DCWN] ASC, [DCDN] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M56M0002] - End */
/* TRIGGERS FOR M56M0002 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M56M0002_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M56M0002_ins
	ON BUSDTA.M56M0002 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M56M0002_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M56M0002_del.DCDDC= inserted.DCDDC AND BUSDTA.M56M0002_del.DCDN= inserted.DCDN AND BUSDTA.M56M0002_del.DCWN= inserted.DCWN
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M56M0002_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0002_upd
	ON BUSDTA.M56M0002 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M56M0002
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M56M0002.DCDDC= inserted.DCDDC AND BUSDTA.M56M0002.DCDN= inserted.DCDN AND BUSDTA.M56M0002.DCWN= inserted.DCWN');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M56M0002_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0002_dlt
	ON BUSDTA.M56M0002 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M56M0002_del (DCDDC, DCDN, DCWN, last_modified )
	SELECT deleted.DCDDC, deleted.DCDN, deleted.DCWN, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M56M0002 - END */


/* [BUSDTA].[M56M0003] - begins */

/* TableDDL - [BUSDTA].[M56M0003] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0003]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0003]
	(
	  [RSROUT] VARCHAR(10) NOT NULL
	, [RSAN8] NUMERIC(8,0) NOT NULL
	, [RSWN] NUMERIC(4,0) NOT NULL
	, [RSDN] NUMERIC(4,0) NOT NULL
	, [RSSN] NUMERIC(4,0) NULL
	, [RSDDC] NCHAR(3) NULL
	, [RSCRBY] NCHAR(50) NULL
	, [RSCRDT] DATETIME NULL
	, [RSUPBY] NCHAR(50) NULL
	, [RSUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M56M0003_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M56M0003] PRIMARY KEY ([RSROUT] ASC, [RSAN8] ASC, [RSWN] ASC, [RSDN] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M56M0003] - End */

/* SHADOW TABLE FOR [BUSDTA].[M56M0003] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0003_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0003_del]
	(
	  [RSROUT] VARCHAR(10)
	, [RSAN8] NUMERIC(8,0)
	, [RSWN] NUMERIC(4,0)
	, [RSDN] NUMERIC(4,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RSROUT] ASC, [RSAN8] ASC, [RSWN] ASC, [RSDN] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M56M0003] - End */
/* TRIGGERS FOR M56M0003 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M56M0003_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M56M0003_ins
	ON BUSDTA.M56M0003 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M56M0003_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M56M0003_del.RSAN8= inserted.RSAN8 AND BUSDTA.M56M0003_del.RSDN= inserted.RSDN AND BUSDTA.M56M0003_del.RSROUT= inserted.RSROUT AND BUSDTA.M56M0003_del.RSWN= inserted.RSWN
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M56M0003_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0003_upd
	ON BUSDTA.M56M0003 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M56M0003
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M56M0003.RSAN8= inserted.RSAN8 AND BUSDTA.M56M0003.RSDN= inserted.RSDN AND BUSDTA.M56M0003.RSROUT= inserted.RSROUT AND BUSDTA.M56M0003.RSWN= inserted.RSWN');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M56M0003_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0003_dlt
	ON BUSDTA.M56M0003 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M56M0003_del (RSAN8, RSDN, RSROUT, RSWN, last_modified )
	SELECT deleted.RSAN8, deleted.RSDN, deleted.RSROUT, deleted.RSWN, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M56M0003 - END */

/* Requested by Nimesh to add column(RPROUT) in shadow table */

/* [BUSDTA].[M56M0004] - begins */

/* TableDDL - [BUSDTA].[M56M0004] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0004]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0004]
	(
	  [RPSTID] NUMERIC(8,0) NOT NULL
	, [RPROUT] NCHAR(10) NOT NULL
	, [RPAN8] NUMERIC(8,0) NOT NULL
	, [RPSTDT] DATE NOT NULL
	, [RPOGDT] DATE NULL
	, [RPSN] NUMERIC(4,0) NULL
	, [RPVTTP] NCHAR(10) NULL
	, [RPSTTP] NCHAR(15) NULL
	, [RPRSTID] NUMERIC(8,0) NULL
	, [RPISRSN] BIT NULL CONSTRAINT DF_M56M0004_RPISRSN DEFAULT((0))
	, [RPACTID] NCHAR(6) NULL
	, [RPRCID] NUMERIC(3,0) NULL
	, [RPCACT] INT NULL CONSTRAINT DF_M56M0004_RPCACT DEFAULT((0))
	, [RPPACT] INT NULL CONSTRAINT DF_M56M0004_RPPACT DEFAULT((0))
	, [RPCHOVRD] NCHAR(10) NULL
	, [RPCRBY] NCHAR(50) NULL
	, [RPCRDT] DATETIME NULL
	, [RPUPBY] NCHAR(50) NULL
	, [RPUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M56M0004_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M56M0004] PRIMARY KEY ([RPSTID] ASC, [RPAN8] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M56M0004] - End */

/* SHADOW TABLE FOR [BUSDTA].[M56M0004] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0004_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0004_del]
	(
	  [RPSTID] NUMERIC(8,0)
	, [RPAN8] NUMERIC(8,0)
	, [RPROUT] NCHAR(10) 
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RPSTID] ASC, [RPAN8] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M56M0004] - End */
/* TRIGGERS FOR M56M0004 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M56M0004_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M56M0004_ins
	ON BUSDTA.M56M0004 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M56M0004_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M56M0004_del.RPAN8= inserted.RPAN8 AND BUSDTA.M56M0004_del.RPSTID= inserted.RPSTID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M56M0004_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0004_upd
	ON BUSDTA.M56M0004 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M56M0004
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M56M0004.RPAN8= inserted.RPAN8 AND BUSDTA.M56M0004.RPSTID= inserted.RPSTID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M56M0004_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0004_dlt
	ON BUSDTA.M56M0004 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M56M0004_del (RPSTID,RPAN8, RPROUT, last_modified )
	SELECT  deleted.RPSTID,deleted.RPAN8,deleted.RPROUT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M56M0004 - END */

/* [BUSDTA].[Order_Detail] - begins */

/* TableDDL - [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail]') IS NULL
BEGIN

CREATE TABLE [BUSDTA].[Order_Detail](
	[OrderID] [numeric](8, 0) NOT NULL,
	[LineID] [numeric](7, 4) NOT NULL,
	[ItemId] [numeric](8, 0) NULL,
	[LongItem] [nchar](25) NULL,
	[ItemLocation] [nchar](20) NULL,
	[ItemLotNumber] [nchar](30) NULL,
	[LineType] [nchar](2) NULL,
	[IsNonStock] [nchar](1) NULL,
	[OrderQty] [numeric](8, 0) NOT NULL,
	[OrderUM] [nchar](2) NULL,
	[OrderQtyInPricingUM] [numeric](8, 0) NOT NULL,
	[PricingUM] [nchar](2) NULL,
	[UnitPriceOriginalAmtInPriceUM] [numeric](12, 4) NULL,
	[UnitPriceAmtInPriceUoM] [numeric](12, 4) NULL,
	[UnitPriceAmt] [numeric](12, 4) NULL,
	[ExtnPriceAmt] [numeric](12, 4) NULL,
	[ItemSalesTaxAmt] [numeric](12, 4) NULL,
	[Discounted] [nchar](1) NULL,
	[PriceOverriden] [nchar](1) NULL,
	[PriceOverrideReasonCodeId] [numeric](3, 0) NULL,
	[IsTaxable] [nchar](1) NULL,
	[ReturnHeldQty] [numeric](8, 0) NULL,
	[SalesCat1] [nchar](3) NULL,
	[SalesCat2] [nchar](3) NULL,
	[SalesCat3] [nchar](3) NULL,
	[SalesCat4] [nchar](3) NULL,
	[SalesCat5] [nchar](3) NULL,
	[PurchasingCat1] [nchar](3) NULL,
	[PurchasingCat2] [nchar](3) NULL,
	[PurchasingCat3] [nchar](3) NULL,
	[PurchasingCat4] [nchar](3) NULL,
	[PurchasingCat5] [nchar](3) NULL,
	[JDEOrderCompany] [nchar](5) NULL,
	[JDEOrderNumber] [numeric](8, 0) NULL,
	[JDEOrderType] [nchar](2) NULL,
	[JDEOrderLine] [numeric](7, 4) NULL,
	[JDEInvoiceCompany] [nchar](5) NULL,
	[JDEInvoiceNumber] [numeric](8, 0) NULL,
	[JDEOInvoiceType] [nchar](2) NULL,
	[JDEZBatchNumber] [nchar](15) NULL,
	[JDEProcessID] [numeric](3, 0) NULL,
	[SettlementID] [numeric](8, 0) NULL,
	[ExtendedAmtVariance] [numeric](12, 4) NULL,
	[TaxAmountAmtVariance] [numeric](12, 4) NULL,
	[HoldCode] [nchar](2) NULL,
	[CreatedBy] [numeric](8, 0) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedBy] [numeric](8, 0) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[last_modified] [datetime] DEFAULT (getdate()),
 CONSTRAINT [PK_Order_Details] PRIMARY KEY CLUSTERED ([OrderID] ASC,[LineID] ASC)
) ON [PRIMARY]


GO

END
GO
/* TableDDL - [BUSDTA].[Order_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail_del]
(
	[OrderID] [numeric](8, 0) NOT NULL,
	[LineID] [numeric](7, 4) NOT NULL,
	[last_modified] [datetime] DEFAULT (getdate()) ,
 CONSTRAINT [PK_Order_Details_del] PRIMARY KEY CLUSTERED ([OrderID] ASC,[LineID] ASC)
) ON [PRIMARY]

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Order_Detail] - End */
/* TRIGGERS FOR Order_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Detail_ins
	ON BUSDTA.Order_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Detail_del.OrderID= inserted.OrderID AND BUSDTA.Order_Detail_del.LineID= inserted.LineID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Detail_upd
	ON BUSDTA.Order_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Detail.OrderID= inserted.OrderID AND BUSDTA.Order_Detail.LineID= inserted.LineID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Detail_dlt
	ON BUSDTA.Order_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Detail_del (OrderID,LineID,last_modified )
	SELECT deleted.OrderID,deleted.LineID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Detail - END */

/* [BUSDTA].[Order_Header] - begins */

/* TableDDL - [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header](
	[OrderID] [numeric](8, 0) NOT NULL,
	[OrderTypeId] [numeric](3, 0) NULL,
	[OriginatingRouteID] [numeric](8, 0) NOT NULL,
	[Branch] [nchar](12) NULL,
	[CustShipToId] [numeric](8, 0) NOT NULL,
	[CustBillToId] [numeric](8, 0) NOT NULL,
	[CustParentId] [numeric](8, 0) NOT NULL,
	[OrderDate] [date] NULL,
	[TotalCoffeeAmt] [numeric](12, 4) NULL,
	[TotalAlliedAmt] [numeric](12, 4) NULL,
	[OrderTotalAmt] [numeric](12, 4) NULL,
	[SalesTaxAmt] [numeric](12, 4) NULL,
	[InvoiceTotalAmt] [numeric](12, 4) NULL,
	[EnergySurchargeAmt] [numeric](12, 4) NULL,
	[VarianceAmt] [numeric](12, 4) NULL,
	[SurchargeReasonCodeId] [numeric](3, 0) NULL,
	[OrderStateId] [numeric](3, 0) NULL,
	[OrderSignature] [varbinary](max) NULL,
	[ChargeOnAccountSignature] [varbinary](max) NULL,
	[JDEOrderCompany] [nchar](5) NULL,
	[JDEOrderNumber] [numeric](8, 0) NULL,
	[JDEOrderType] [nchar](2) NULL,
	[JDEInvoiceCompany] [nchar](5) NULL,
	[JDEInvoiceNumber] [numeric](8, 0) NULL,
	[JDEOInvoiceType] [nchar](2) NULL,
	[JDEZBatchNumber] [nchar](15) NULL,
	[JDEProcessID] [numeric](3, 0) NULL,
	[SettlementID] [numeric](8, 0) NULL,
	[PaymentTerms] [nchar](3) NULL,
	[CustomerReference1] [nchar](25) NULL,
	[CustomerReference2] [nchar](25) NULL,
	[AdjustmentSchedule] [nchar](8) NULL,
	[TaxArea] [nchar](10) NULL,
	[TaxExplanationCode] [nchar](1) NULL,
	[VoidReasonCodeId] [numeric](3, 0) NULL,
	[ChargeOnAccount] [nchar](1) NULL,
	[HoldCommitted] [nchar](1) NULL,
	[CreatedBy] [numeric](8, 0) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedBy] [numeric](8, 0) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[last_modified] [datetime] DEFAULT (getdate()),
 CONSTRAINT [PK_Order_Headers] PRIMARY KEY CLUSTERED ([OrderID] ASC))

END
GO
/* TableDDL - [BUSDTA].[Order_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header_del](
	[OrderID] [numeric](8, 0) NOT NULL,
	[last_modified] [datetime] DEFAULT (getdate()),
 CONSTRAINT [PK_Order_Header_del] PRIMARY KEY CLUSTERED ([OrderID] ASC)
) ON [PRIMARY]

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - End */
/* TRIGGERS FOR Order_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Header_ins
	ON BUSDTA.Order_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Header_del.OrderID= inserted.OrderID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_upd
	ON BUSDTA.Order_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Header.OrderID= inserted.OrderID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_dlt
	ON BUSDTA.Order_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Header_del (OrderID, last_modified )
	SELECT deleted.OrderID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Header - END */
/* [BUSDTA].[Payment_Ref_Map] - begins */

/* TableDDL - [BUSDTA].[Payment_Ref_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Payment_Ref_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Payment_Ref_Map]
	(
	  [Payment_Ref_Map_Id] INT NOT NULL
	, [Payment_Id] INT NOT NULL
	, [Ref_Id] INT NOT NULL
	, [Ref_Type] NCHAR(3) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PAYMENT_REF_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Payment_Ref_Map] PRIMARY KEY ([Payment_Ref_Map_Id] ASC, [Payment_Id] ASC, [Ref_Id] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Payment_Ref_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Payment_Ref_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Payment_Ref_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Payment_Ref_Map_del]
	(
	  [Payment_Ref_Map_Id] INT
	, [Payment_Id] INT
	, [Ref_Id] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Payment_Ref_Map_Id] ASC, [Payment_Id] ASC, [Ref_Id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Payment_Ref_Map] - End */
/* TRIGGERS FOR Payment_Ref_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Payment_Ref_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Payment_Ref_Map_ins
	ON BUSDTA.Payment_Ref_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Payment_Ref_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Payment_Ref_Map_del.Payment_Id= inserted.Payment_Id AND BUSDTA.Payment_Ref_Map_del.Payment_Ref_Map_Id= inserted.Payment_Ref_Map_Id AND BUSDTA.Payment_Ref_Map_del.Ref_Id= inserted.Ref_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Payment_Ref_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Payment_Ref_Map_upd
	ON BUSDTA.Payment_Ref_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Payment_Ref_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Payment_Ref_Map.Payment_Id= inserted.Payment_Id AND BUSDTA.Payment_Ref_Map.Payment_Ref_Map_Id= inserted.Payment_Ref_Map_Id AND BUSDTA.Payment_Ref_Map.Ref_Id= inserted.Ref_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Payment_Ref_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Payment_Ref_Map_dlt
	ON BUSDTA.Payment_Ref_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Payment_Ref_Map_del (Payment_Id, Payment_Ref_Map_Id, Ref_Id, last_modified )
	SELECT deleted.Payment_Id, deleted.Payment_Ref_Map_Id, deleted.Ref_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Payment_Ref_Map - END */
/* [BUSDTA].[PickOrder] - begins */

/* TableDDL - [BUSDTA].[PickOrder] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder]
	(
	  [PickOrder_Id] INT NOT NULL
	, [Order_ID] INT NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Item_Number] NCHAR(25) NULL
	, [Order_Qty] INT NULL
	, [Order_UOM] NCHAR(2) NULL
	, [Picked_Qty_Primary_UOM] INT NULL
	, [Primary_UOM] NCHAR(2) NULL
	, [Order_Qty_Primary_UOM] INT NULL
	, [On_Hand_Qty_Primary] INT NULL
	, [Last_Scan_Mode] BIT NULL
	, [Item_Scan_Sequence] INT NULL
	, [Picked_By] INT NULL
	, [IsOnHold] INT NULL
	, [Reason_Code_Id] INT NULL
	, [ManuallyPickCount] INT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PICKORDER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_PickOrder] PRIMARY KEY ([PickOrder_Id] ASC, [Order_ID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[PickOrder] - End */

/* SHADOW TABLE FOR [BUSDTA].[PickOrder] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder_del]
	(
	  [PickOrder_Id] INT
	, [Order_ID] INT
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PickOrder_Id] ASC, [Order_ID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[PickOrder] - End */
/* TRIGGERS FOR PickOrder - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PickOrder_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PickOrder_ins
	ON BUSDTA.PickOrder AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PickOrder_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PickOrder_del.Order_ID= inserted.Order_ID AND BUSDTA.PickOrder_del.PickOrder_Id= inserted.PickOrder_Id AND BUSDTA.PickOrder_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PickOrder_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_upd
	ON BUSDTA.PickOrder AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PickOrder
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PickOrder.Order_ID= inserted.Order_ID AND BUSDTA.PickOrder.PickOrder_Id= inserted.PickOrder_Id AND BUSDTA.PickOrder.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PickOrder_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_dlt
	ON BUSDTA.PickOrder AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PickOrder_del (Order_ID, PickOrder_Id, RouteId, last_modified )
	SELECT deleted.Order_ID, deleted.PickOrder_Id, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PickOrder - END */
/* [BUSDTA].[PickOrder_Exception] - begins */

/* TableDDL - [BUSDTA].[PickOrder_Exception] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder_Exception]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder_Exception]
	(
	  [PickOrder_Exception_Id] INT NOT NULL
	, [Order_Id] INT NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Item_Number] NCHAR(25) NULL
	, [Exception_Qty] INT NULL
	, [UOM] NCHAR(2) NULL
	, [Exception_Reason] VARCHAR(25) NULL
	, [ManualPickReasonCode] INT NULL
	, [ManuallyPickCount] INT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PICKORDER_EXCEPTION_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_PickOrder_Exception] PRIMARY KEY ([PickOrder_Exception_Id] ASC, [Order_Id] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[PickOrder_Exception] - End */

/* SHADOW TABLE FOR [BUSDTA].[PickOrder_Exception] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder_Exception_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder_Exception_del]
	(
	  [PickOrder_Exception_Id] INT
	, [Order_Id] INT
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PickOrder_Exception_Id] ASC, [Order_Id] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[PickOrder_Exception] - End */
/* TRIGGERS FOR PickOrder_Exception - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PickOrder_Exception_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PickOrder_Exception_ins
	ON BUSDTA.PickOrder_Exception AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PickOrder_Exception_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PickOrder_Exception_del.Order_Id= inserted.Order_Id AND BUSDTA.PickOrder_Exception_del.PickOrder_Exception_Id= inserted.PickOrder_Exception_Id AND BUSDTA.PickOrder_Exception_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PickOrder_Exception_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_Exception_upd
	ON BUSDTA.PickOrder_Exception AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PickOrder_Exception
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PickOrder_Exception.Order_Id= inserted.Order_Id AND BUSDTA.PickOrder_Exception.PickOrder_Exception_Id= inserted.PickOrder_Exception_Id AND BUSDTA.PickOrder_Exception.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PickOrder_Exception_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_Exception_dlt
	ON BUSDTA.PickOrder_Exception AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PickOrder_Exception_del (Order_Id, PickOrder_Exception_Id, RouteId, last_modified )
	SELECT deleted.Order_Id, deleted.PickOrder_Exception_Id, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PickOrder_Exception - END */
/* [BUSDTA].[ReasonCodeMaster] - begins */

/* TableDDL - [BUSDTA].[ReasonCodeMaster] - Start */
IF OBJECT_ID('[BUSDTA].[ReasonCodeMaster]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ReasonCodeMaster]
	(
	  [ReasonCodeId] INT NOT NULL
	, [ReasonCode] VARCHAR(5) NOT NULL
	, [ReasonCodeDescription] NCHAR(100) NULL
	, [ReasonCodeType] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_REASONCODEMASTER_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_REASONCODEMASTER_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_REASONCODEMASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ReasonCodeMaster] PRIMARY KEY ([ReasonCode] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ReasonCodeMaster] - End */

/* SHADOW TABLE FOR [BUSDTA].[ReasonCodeMaster] - Start */
IF OBJECT_ID('[BUSDTA].[ReasonCodeMaster_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ReasonCodeMaster_del]
	(
	  [ReasonCode] VARCHAR(5)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ReasonCode] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ReasonCodeMaster] - End */
/* TRIGGERS FOR ReasonCodeMaster - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ReasonCodeMaster_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ReasonCodeMaster_ins
	ON BUSDTA.ReasonCodeMaster AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ReasonCodeMaster_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ReasonCodeMaster_del.ReasonCode= inserted.ReasonCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ReasonCodeMaster_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ReasonCodeMaster_upd
	ON BUSDTA.ReasonCodeMaster AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ReasonCodeMaster
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ReasonCodeMaster.ReasonCode= inserted.ReasonCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ReasonCodeMaster_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ReasonCodeMaster_dlt
	ON BUSDTA.ReasonCodeMaster AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ReasonCodeMaster_del (ReasonCode, last_modified )
	SELECT deleted.ReasonCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ReasonCodeMaster - END */
/* [BUSDTA].[Route_Device_Map] - begins */

/* TableDDL - [BUSDTA].[Route_Device_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Device_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Device_Map]
	(
	  [Route_Id] VARCHAR(8) NOT NULL
	, [Device_Id] VARCHAR(30) NOT NULL
	, [Active] INT NULL
	, [Remote_Id] VARCHAR(30) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_DEVICE_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Device_Map] PRIMARY KEY ([Route_Id] ASC, [Device_Id] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Device_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Device_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Device_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Device_Map_del]
	(
	  [Route_Id] VARCHAR(8)
	, [Device_Id] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Route_Id] ASC, [Device_Id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Device_Map] - End */
/* TRIGGERS FOR Route_Device_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Device_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Device_Map_ins
	ON BUSDTA.Route_Device_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Device_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Device_Map_del.Device_Id= inserted.Device_Id AND BUSDTA.Route_Device_Map_del.Route_Id= inserted.Route_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Device_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Device_Map_upd
	ON BUSDTA.Route_Device_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Device_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Device_Map.Device_Id= inserted.Device_Id AND BUSDTA.Route_Device_Map.Route_Id= inserted.Route_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Device_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Device_Map_dlt
	ON BUSDTA.Route_Device_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Device_Map_del (Device_Id, Route_Id, last_modified )
	SELECT deleted.Device_Id, deleted.Route_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Device_Map - END */

/* [BUSDTA].[Route_User_Map] - begins */

/* TableDDL - [BUSDTA].[Route_User_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_User_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_User_Map]
	(
	  [App_user_id] INT NOT NULL
	, [Route_Id] VARCHAR(8) NOT NULL
	, [Active] INT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_USER_MAP_last_modified DEFAULT(getdate())
	, [Default_Route] INT NULL
	, CONSTRAINT [PK_Route_User_Map] PRIMARY KEY ([App_user_id] ASC, [Route_Id] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_User_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_User_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_User_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_User_Map_del]
	(
	  [App_user_id] INT
	, [Route_Id] VARCHAR(8)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([App_user_id] ASC, [Route_Id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_User_Map] - End */
/* TRIGGERS FOR Route_User_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_User_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_User_Map_ins
	ON BUSDTA.Route_User_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_User_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_User_Map_del.App_user_id= inserted.App_user_id AND BUSDTA.Route_User_Map_del.Route_Id= inserted.Route_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_User_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_User_Map_upd
	ON BUSDTA.Route_User_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_User_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_User_Map.App_user_id= inserted.App_user_id AND BUSDTA.Route_User_Map.Route_Id= inserted.Route_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_User_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_User_Map_dlt
	ON BUSDTA.Route_User_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_User_Map_del (App_user_id, Route_Id, last_modified )
	SELECT deleted.App_user_id, deleted.Route_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_User_Map - END */
/* [BUSDTA].[UDCKEYLIST] - begins */

/* TableDDL - [BUSDTA].[UDCKEYLIST] - Start */
IF OBJECT_ID('[BUSDTA].[UDCKEYLIST]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UDCKEYLIST]
	(
	  [DTSY] NCHAR(4) NOT NULL
	, [DTRT] NCHAR(2) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_UDCKEYLIST_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_UDCKEYLIST] PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
/* TableDDL - [BUSDTA].[UDCKEYLIST] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[UDCKEYLIST] - Start */
IF OBJECT_ID('[BUSDTA].[UDCKEYLIST_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UDCKEYLIST_del]
	(
	  [DTSY] NCHAR(4)
	, [DTRT] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[UDCKEYLIST] - End */
/* TRIGGERS FOR UDCKEYLIST - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.UDCKEYLIST_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.UDCKEYLIST_ins
	ON BUSDTA.UDCKEYLIST AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.UDCKEYLIST_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.UDCKEYLIST_del.DTRT= inserted.DTRT AND BUSDTA.UDCKEYLIST_del.DTSY= inserted.DTSY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.UDCKEYLIST_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UDCKEYLIST_upd
	ON BUSDTA.UDCKEYLIST AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.UDCKEYLIST
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.UDCKEYLIST.DTRT= inserted.DTRT AND BUSDTA.UDCKEYLIST.DTSY= inserted.DTSY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.UDCKEYLIST_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UDCKEYLIST_dlt
	ON BUSDTA.UDCKEYLIST AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.UDCKEYLIST_del (DTRT, DTSY, last_modified )
	SELECT deleted.DTRT, deleted.DTSY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR UDCKEYLIST - END */
/* [BUSDTA].[User_Role_Map] - begins */

/* TableDDL - [BUSDTA].[User_Role_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Role_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Role_Map]
	(
	  [App_user_id] INT NOT NULL
	, [Role] VARCHAR(8) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_USER_ROLE_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_User_Role_Map] PRIMARY KEY ([App_user_id] ASC)
	)

END
/* TableDDL - [BUSDTA].[User_Role_Map] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[User_Role_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Role_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Role_Map_del]
	(
	  [App_user_id] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([App_user_id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[User_Role_Map] - End */
/* TRIGGERS FOR User_Role_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.User_Role_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.User_Role_Map_ins
	ON BUSDTA.User_Role_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.User_Role_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.User_Role_Map_del.App_user_id= inserted.App_user_id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.User_Role_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Role_Map_upd
	ON BUSDTA.User_Role_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.User_Role_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.User_Role_Map.App_user_id= inserted.App_user_id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.User_Role_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Role_Map_dlt
	ON BUSDTA.User_Role_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.User_Role_Map_del (App_user_id, last_modified )
	SELECT deleted.App_user_id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR User_Role_Map - END */

/* [BUSDTA].[Inventory] - begins */

/* TableDDL - [BUSDTA].[Inventory] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory]
	(
	  [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OnHandQuantity] NUMERIC(4,0) NULL
	, [CommittedQuantity] NUMERIC(4,0) NULL
	, [HeldQuantity] NUMERIC(4,0) NULL
	, [ParLevel] NUMERIC(4,0) NULL
	, [LastReceiptDate] DATETIME NULL
	, [LastConsumeDate] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_INVENTORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Inventory] PRIMARY KEY ([ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Inventory] - End */

/* SHADOW TABLE FOR [BUSDTA].[Inventory] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_del]
	(
	  [ItemId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Inventory] - End */
/* TRIGGERS FOR Inventory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Inventory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Inventory_ins
	ON BUSDTA.Inventory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Inventory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Inventory_del.ItemId= inserted.ItemId AND BUSDTA.Inventory_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Inventory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_upd
	ON BUSDTA.Inventory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Inventory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Inventory.ItemId= inserted.ItemId AND BUSDTA.Inventory.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Inventory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_dlt
	ON BUSDTA.Inventory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Inventory_del (ItemId, RouteId, last_modified )
	SELECT deleted.ItemId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Inventory - END */

/* [BUSDTA].[Inventory_Ledger] - begins */

/* TableDDL - [BUSDTA].[Inventory_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Ledger]
	(
	  [InventoryLedgerID] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionQty] NUMERIC(4,0) NULL
	, [TransactionQtyUM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUM] NCHAR(2) NULL
	, [TransactionType] NUMERIC(8,0) NULL
	, [TransactionId] NUMERIC(8,0) NULL
	, [SettlementID] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_INVENTORY_LEDGER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Inventory_Ledger] PRIMARY KEY ([InventoryLedgerID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Inventory_Ledger] - End */

/* SHADOW TABLE FOR [BUSDTA].[Inventory_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Ledger_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Ledger_del]
	(
	  [InventoryLedgerID] NUMERIC(8,0)
	, [ItemId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([InventoryLedgerID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Inventory_Ledger] - End */
/* TRIGGERS FOR Inventory_Ledger - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Inventory_Ledger_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Inventory_Ledger_ins
	ON BUSDTA.Inventory_Ledger AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Inventory_Ledger_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Inventory_Ledger_del.InventoryLedgerID= inserted.InventoryLedgerID AND BUSDTA.Inventory_Ledger_del.ItemId= inserted.ItemId AND BUSDTA.Inventory_Ledger_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Inventory_Ledger_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Ledger_upd
	ON BUSDTA.Inventory_Ledger AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Inventory_Ledger
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Inventory_Ledger.InventoryLedgerID= inserted.InventoryLedgerID AND BUSDTA.Inventory_Ledger.ItemId= inserted.ItemId AND BUSDTA.Inventory_Ledger.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Inventory_Ledger_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Ledger_dlt
	ON BUSDTA.Inventory_Ledger AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Inventory_Ledger_del (InventoryLedgerID, ItemId, RouteId, last_modified )
	SELECT deleted.InventoryLedgerID, deleted.ItemId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Inventory_Ledger - END */


/* [BUSDTA].[Inventory_Adjustment] - begins */

/* TableDDL - [BUSDTA].[Inventory_Adjustment] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Adjustment]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Adjustment]
	(
	  [InventoryAdjustmentId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionQty] NUMERIC(4,0) NULL
	, [TransactionQtyUM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUM] NCHAR(2) NULL
	, [ReasonCode] NUMERIC(3,0) NOT NULL
	, [IsApproved] BIT NULL CONSTRAINT DF_INVENTORY_ADJUSTMENT_IsApproved DEFAULT((0))
	, [IsApplied] BIT NULL CONSTRAINT DF_INVENTORY_ADJUSTMENT_IsApplied DEFAULT((0))
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [AdjustmentStatus] NUMERIC(3,0) NULL
	, [Source] NUMERIC(3,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_INVENTORY_ADJUSTMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Inventory_Adjustment] PRIMARY KEY ([InventoryAdjustmentId] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Inventory_Adjustment] - End */

/* SHADOW TABLE FOR [BUSDTA].[Inventory_Adjustment] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Adjustment_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Adjustment_del]
	(
	  [InventoryAdjustmentId] NUMERIC(8,0)
	, [ItemId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([InventoryAdjustmentId] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Inventory_Adjustment] - End */
/* TRIGGERS FOR Inventory_Adjustment - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Inventory_Adjustment_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Inventory_Adjustment_ins
	ON BUSDTA.Inventory_Adjustment AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Inventory_Adjustment_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Inventory_Adjustment_del.InventoryAdjustmentId= inserted.InventoryAdjustmentId AND BUSDTA.Inventory_Adjustment_del.ItemId= inserted.ItemId AND BUSDTA.Inventory_Adjustment_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Inventory_Adjustment_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Adjustment_upd
	ON BUSDTA.Inventory_Adjustment AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Inventory_Adjustment
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Inventory_Adjustment.InventoryAdjustmentId= inserted.InventoryAdjustmentId AND BUSDTA.Inventory_Adjustment.ItemId= inserted.ItemId AND BUSDTA.Inventory_Adjustment.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Inventory_Adjustment_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Adjustment_dlt
	ON BUSDTA.Inventory_Adjustment AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Inventory_Adjustment_del (InventoryAdjustmentId, ItemId, RouteId, last_modified )
	SELECT deleted.InventoryAdjustmentId, deleted.ItemId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Inventory_Adjustment - END */


/* [BUSDTA].[ItemConfiguration] - begins */

/* TableDDL - [BUSDTA].[ItemConfiguration] - Start */
IF OBJECT_ID('[BUSDTA].[ItemConfiguration]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemConfiguration]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [RouteEnabled] BIT NULL
	, [Sellable] BIT NULL
	, [AllowSearch] BIT NULL
	, [PrimaryUM] NCHAR(2) NULL
	, [PricingUM] NCHAR(2) NULL
	, [TransactionUM] NCHAR(2) NULL
	, [OtherUM1] NCHAR(2) NULL
	, [OtherUM2] NCHAR(2) NULL
	, [AllowLooseSample] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ITEMCONFIGURATION_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ItemConfiguration] PRIMARY KEY ([ItemID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ItemConfiguration] - End */

/* SHADOW TABLE FOR [BUSDTA].[ItemConfiguration] - Start */
IF OBJECT_ID('[BUSDTA].[ItemConfiguration_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemConfiguration_del]
	(
	  [ItemID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ItemConfiguration] - End */
/* TRIGGERS FOR ItemConfiguration - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ItemConfiguration_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ItemConfiguration_ins
	ON BUSDTA.ItemConfiguration AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ItemConfiguration_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ItemConfiguration_del.ItemID= inserted.ItemID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ItemConfiguration_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemConfiguration_upd
	ON BUSDTA.ItemConfiguration AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ItemConfiguration
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ItemConfiguration.ItemID= inserted.ItemID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ItemConfiguration_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemConfiguration_dlt
	ON BUSDTA.ItemConfiguration AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ItemConfiguration_del (ItemID, last_modified )
	SELECT deleted.ItemID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ItemConfiguration - END */


/* [BUSDTA].[ItemCrossReference] - begins */

/* TableDDL - [BUSDTA].[ItemCrossReference] - Start */
IF OBJECT_ID('[BUSDTA].[ItemCrossReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemCrossReference]
	(
	  [ItemReferenceID] NUMERIC(8,0) NOT NULL
	, [CrossReferenceID] NUMERIC(8,0) NOT NULL
	, [MobileType] NCHAR(10) NULL
	, [EffectiveFrom] DATE NULL
	, [EffectiveThru] DATE NULL
	, [CrossData] NCHAR(50) NULL
	, [AddressNumber] NUMERIC(8,0) NULL
	, [CrossReferenceType] NCHAR(50) NULL
	, [ItemNumber] NUMERIC(8,0) NULL
	, [ItemRevisionLevel] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ITEMCROSSREFERENCE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ItemCrossReference] PRIMARY KEY ([ItemReferenceID] ASC, [CrossReferenceID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ItemCrossReference] - End */

/* SHADOW TABLE FOR [BUSDTA].[ItemCrossReference] - Start */
IF OBJECT_ID('[BUSDTA].[ItemCrossReference_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemCrossReference_del]
	(
	  [ItemReferenceID] NUMERIC(8,0)
	, [CrossReferenceID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemReferenceID] ASC, [CrossReferenceID] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[ItemCrossReference] - End */
/* TRIGGERS FOR ItemCrossReference - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ItemCrossReference_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ItemCrossReference_ins
	ON BUSDTA.ItemCrossReference AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ItemCrossReference_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ItemCrossReference_del.CrossReferenceID= inserted.CrossReferenceID AND BUSDTA.ItemCrossReference_del.ItemReferenceID= inserted.ItemReferenceID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ItemCrossReference_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemCrossReference_upd
	ON BUSDTA.ItemCrossReference AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ItemCrossReference
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ItemCrossReference.CrossReferenceID= inserted.CrossReferenceID AND BUSDTA.ItemCrossReference.ItemReferenceID= inserted.ItemReferenceID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ItemCrossReference_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemCrossReference_dlt
	ON BUSDTA.ItemCrossReference AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ItemCrossReference_del (CrossReferenceID, ItemReferenceID, last_modified )
	SELECT deleted.CrossReferenceID, deleted.ItemReferenceID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ItemCrossReference - END */


/* [BUSDTA].[ItemUoMs] - begins */

/* TableDDL - [BUSDTA].[ItemUoMs] - Start */
IF OBJECT_ID('[BUSDTA].[ItemUoMs]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemUoMs]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [UOM] NCHAR(2) NOT NULL
	, [CrossReferenceID] NUMERIC(8,0) NULL
	, [CanSell] BIT NULL
	, [CanSample] BIT NULL
	, [CanRestock] BIT NULL
	, [DisplaySeq] NUMERIC(2,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ITEMUOMS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ItemUoMs] PRIMARY KEY ([ItemID] ASC, [UOM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ItemUoMs] - End */

/* SHADOW TABLE FOR [BUSDTA].[ItemUoMs] - Start */
IF OBJECT_ID('[BUSDTA].[ItemUoMs_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemUoMs_del]
	(
	  [ItemID] NUMERIC(8,0)
	, [UOM] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemID] ASC, [UOM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ItemUoMs] - End */
/* TRIGGERS FOR ItemUoMs - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ItemUoMs_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ItemUoMs_ins
	ON BUSDTA.ItemUoMs AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ItemUoMs_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ItemUoMs_del.ItemID= inserted.ItemID AND BUSDTA.ItemUoMs_del.UOM= inserted.UOM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ItemUoMs_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemUoMs_upd
	ON BUSDTA.ItemUoMs AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ItemUoMs
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ItemUoMs.ItemID= inserted.ItemID AND BUSDTA.ItemUoMs.UOM= inserted.UOM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ItemUoMs_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemUoMs_dlt
	ON BUSDTA.ItemUoMs AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ItemUoMs_del (ItemID, UOM, last_modified )
	SELECT deleted.ItemID, deleted.UOM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ItemUoMs - END */

/* [BUSDTA].[UoMFactorReference] - begins */

/* TableDDL - [BUSDTA].[UoMFactorReference] - Start */
IF OBJECT_ID('[BUSDTA].[UoMFactorReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UoMFactorReference]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [FromUOM] NCHAR(2) NOT NULL
	, [ToUOM] NCHAR(2) NOT NULL
	, [ConversionFactor] NUMERIC(8,4) NULL
	, [GenerationDate] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_UOMFACTORREFERENCE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_UoMFactorReference] PRIMARY KEY ([ItemID] ASC, [FromUOM] ASC, [ToUOM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[UoMFactorReference] - End */

/* SHADOW TABLE FOR [BUSDTA].[UoMFactorReference] - Start */
IF OBJECT_ID('[BUSDTA].[UoMFactorReference_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UoMFactorReference_del]
	(
	  [ItemID] NUMERIC(8,0)
	, [FromUOM] NCHAR(2)
	, [ToUOM] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemID] ASC, [FromUOM] ASC, [ToUOM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[UoMFactorReference] - End */
/* TRIGGERS FOR UoMFactorReference - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.UoMFactorReference_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.UoMFactorReference_ins
	ON BUSDTA.UoMFactorReference AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.UoMFactorReference_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.UoMFactorReference_del.FromUOM= inserted.FromUOM AND BUSDTA.UoMFactorReference_del.ItemID= inserted.ItemID AND BUSDTA.UoMFactorReference_del.ToUOM= inserted.ToUOM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.UoMFactorReference_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UoMFactorReference_upd
	ON BUSDTA.UoMFactorReference AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.UoMFactorReference
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.UoMFactorReference.FromUOM= inserted.FromUOM AND BUSDTA.UoMFactorReference.ItemID= inserted.ItemID AND BUSDTA.UoMFactorReference.ToUOM= inserted.ToUOM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.UoMFactorReference_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UoMFactorReference_dlt
	ON BUSDTA.UoMFactorReference AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.UoMFactorReference_del (FromUOM, ItemID, ToUOM, last_modified )
	SELECT deleted.FromUOM, deleted.ItemID, deleted.ToUOM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR UoMFactorReference - END */
/* [BUSDTA].[Question_Master] - begins */

/* TableDDL - [BUSDTA].[Question_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Master]
	(
	  [QuestionId] NUMERIC(8,0) NOT NULL
	, [QuestionTitle] NCHAR(100) NULL
	, [QuestionDescription] NCHAR(100) NULL
	, [IsMandatory] BIT NULL
	, [IsMultivalue] BIT NULL
	, [IsDescriptive] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_QUESTION_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Question_Master] PRIMARY KEY ([QuestionId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Question_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Question_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Master_del]
	(
	  [QuestionId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([QuestionId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Question_Master] - End */
/* TRIGGERS FOR Question_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Question_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Question_Master_ins
	ON BUSDTA.Question_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Question_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Question_Master_del.QuestionId= inserted.QuestionId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Question_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Master_upd
	ON BUSDTA.Question_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Question_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Question_Master.QuestionId= inserted.QuestionId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Question_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Master_dlt
	ON BUSDTA.Question_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Question_Master_del (QuestionId, last_modified )
	SELECT deleted.QuestionId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Question_Master - END */
/* [BUSDTA].[Response_Master] - begins */

/* TableDDL - [BUSDTA].[Response_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Response_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Response_Master]
	(
	  [ResponseId] NUMERIC(8,0) NOT NULL
	, [Response] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_RESPONSE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Response_Master] PRIMARY KEY ([ResponseId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Response_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Response_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Response_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Response_Master_del]
	(
	  [ResponseId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ResponseId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Response_Master] - End */
/* TRIGGERS FOR Response_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Response_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Response_Master_ins
	ON BUSDTA.Response_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Response_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Response_Master_del.ResponseId= inserted.ResponseId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Response_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Response_Master_upd
	ON BUSDTA.Response_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Response_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Response_Master.ResponseId= inserted.ResponseId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Response_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Response_Master_dlt
	ON BUSDTA.Response_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Response_Master_del (ResponseId, last_modified )
	SELECT deleted.ResponseId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Response_Master - END */
/* [BUSDTA].[Question_Template] - begins */

/* TableDDL - [BUSDTA].[Question_Template] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Template]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Template]
	(
	  [TemplateId] NUMERIC(8,0) NOT NULL
	, [TemplateName] NCHAR(100) NULL
	, [QuestionId] NUMERIC(8,0) NOT NULL
	, [ResponseID] NUMERIC(8,0) NOT NULL
	, [RevisionId] NUMERIC(2,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_QUESTION_TEMPLATE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Question_Template] PRIMARY KEY ([TemplateId] ASC, [QuestionId] ASC, [ResponseID] ASC, [RevisionId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Question_Template] - End */

/* SHADOW TABLE FOR [BUSDTA].[Question_Template] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Template_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Template_del]
	(
	  [TemplateId] NUMERIC(8,0)
	, [QuestionId] NUMERIC(8,0)
	, [ResponseID] NUMERIC(8,0)
	, [RevisionId] NUMERIC(2,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TemplateId] ASC, [QuestionId] ASC, [ResponseID] ASC, [RevisionId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Question_Template] - End */
/* TRIGGERS FOR Question_Template - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Question_Template_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Question_Template_ins
	ON BUSDTA.Question_Template AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Question_Template_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Question_Template_del.QuestionId= inserted.QuestionId AND BUSDTA.Question_Template_del.ResponseID= inserted.ResponseID AND BUSDTA.Question_Template_del.RevisionId= inserted.RevisionId AND BUSDTA.Question_Template_del.TemplateId= inserted.TemplateId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Question_Template_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Template_upd
	ON BUSDTA.Question_Template AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Question_Template
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Question_Template.QuestionId= inserted.QuestionId AND BUSDTA.Question_Template.ResponseID= inserted.ResponseID AND BUSDTA.Question_Template.RevisionId= inserted.RevisionId AND BUSDTA.Question_Template.TemplateId= inserted.TemplateId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Question_Template_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Template_dlt
	ON BUSDTA.Question_Template AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Question_Template_del (QuestionId, ResponseID, RevisionId, TemplateId, last_modified )
	SELECT deleted.QuestionId, deleted.ResponseID, deleted.RevisionId, deleted.TemplateId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Question_Template - END */
/* [BUSDTA].[PreTrip_Inspection_Header] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Header]
	(
	  [PreTripInspectionHeaderId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TemplateId] NUMERIC(8,0) NULL
	, [PreTripDateTime] DATETIME NULL
	, [UserName] NCHAR(50) NULL
	, [StatusId] NUMERIC(3,0) NULL
	, [VehicleMake] NCHAR(25) NULL
	, [VehicleNumber] NUMERIC(10,0) NULL
	, [OdoMmeterReading] NUMERIC(10,2) NULL
	, [Comment] NCHAR(100) NULL
	, [VerSignature] VARBINARY(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PRETRIP_INSPECTION_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_PreTrip_Inspection_Header] PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Header] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Header_del]
	(
	  [PreTripInspectionHeaderId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Header] - End */
/* TRIGGERS FOR PreTrip_Inspection_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PreTrip_Inspection_Header_ins
	ON BUSDTA.PreTrip_Inspection_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PreTrip_Inspection_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PreTrip_Inspection_Header_del.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Header_upd
	ON BUSDTA.PreTrip_Inspection_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PreTrip_Inspection_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PreTrip_Inspection_Header.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Header_dlt
	ON BUSDTA.PreTrip_Inspection_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PreTrip_Inspection_Header_del (PreTripInspectionHeaderId, RouteId, last_modified )
	SELECT deleted.PreTripInspectionHeaderId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PreTrip_Inspection_Header - END */
/* [BUSDTA].[PreTrip_Inspection_Detail] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Detail]
	(
	  [PreTripInspectionHeaderId] NUMERIC(8,0) NOT NULL
	, [PreTripInspectionDetailId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [QuestionId] NUMERIC(8,0) NOT NULL
	, [ResponseID] NUMERIC(8,0) NULL
	, [ResponseReason] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PRETRIP_INSPECTION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_PreTrip_Inspection_Detail] PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [PreTripInspectionDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Detail_del]
	(
	  [PreTripInspectionHeaderId] NUMERIC(8,0)
	, [PreTripInspectionDetailId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [PreTripInspectionDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Detail] - End */
/* TRIGGERS FOR PreTrip_Inspection_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PreTrip_Inspection_Detail_ins
	ON BUSDTA.PreTrip_Inspection_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PreTrip_Inspection_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PreTrip_Inspection_Detail_del.PreTripInspectionDetailId= inserted.PreTripInspectionDetailId AND BUSDTA.PreTrip_Inspection_Detail_del.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Detail_upd
	ON BUSDTA.PreTrip_Inspection_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PreTrip_Inspection_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PreTrip_Inspection_Detail.PreTripInspectionDetailId= inserted.PreTripInspectionDetailId AND BUSDTA.PreTrip_Inspection_Detail.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Detail_dlt
	ON BUSDTA.PreTrip_Inspection_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PreTrip_Inspection_Detail_del (PreTripInspectionDetailId, PreTripInspectionHeaderId, RouteId, last_modified )
	SELECT deleted.PreTripInspectionDetailId, deleted.PreTripInspectionHeaderId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PreTrip_Inspection_Detail - END */
/* [BUSDTA].[Cash_Master] - begins */

/* TableDDL - [BUSDTA].[Cash_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Master]
	(
	  [CashId] NUMERIC(8,0) NOT NULL
	, [CashType] NCHAR(10) NULL
	, [CashCode] NCHAR(10) NULL
	, [CashDescription] NCHAR(100) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CASH_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cash_Master] PRIMARY KEY ([CashId] ASC)
	)

END
/* TableDDL - [BUSDTA].[Cash_Master] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Master_del]
	(
	  [CashId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CashId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Master] - End */
/* TRIGGERS FOR Cash_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cash_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cash_Master_ins
	ON BUSDTA.Cash_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cash_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cash_Master_del.CashId= inserted.CashId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cash_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Master_upd
	ON BUSDTA.Cash_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cash_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cash_Master.CashId= inserted.CashId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cash_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Master_dlt
	ON BUSDTA.Cash_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cash_Master_del (CashId, last_modified )
	SELECT deleted.CashId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cash_Master - END */

/* [BUSDTA].[Route_Settlement] - begins */

/* TableDDL - [BUSDTA].[Route_Settlement] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement]
	(
	  [SettlementID] NUMERIC(8,0) NOT NULL
	, [SettlementNO] NCHAR(10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Status] NUMERIC(3,0) NULL
	, [SettlementDateTime] DATETIME NULL
	, [UserId] NUMERIC(8,0) NULL
	, [Originator] NUMERIC(8,0) NULL
	, [Verifier] NUMERIC(8,0) NULL
	, [SettlementAmount] NUMERIC(8,4) NULL
	, [ExceptionAmount] NUMERIC(8,4) NULL
	, [Comment] NVARCHAR(100) NULL
	, [OriginatingRoute] NUMERIC(8,0) NOT NULL
	, [partitioningRoute] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_SETTLEMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Settlement] PRIMARY KEY ([SettlementID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Settlement] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement_del]
	(
	  [SettlementID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SettlementID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement] - End */
/* TRIGGERS FOR Route_Settlement - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Settlement_ins
	ON BUSDTA.Route_Settlement AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Settlement_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Settlement_del.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement_del.SettlementID= inserted.SettlementID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Settlement_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_upd
	ON BUSDTA.Route_Settlement AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Settlement
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Settlement.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement.SettlementID= inserted.SettlementID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_dlt
	ON BUSDTA.Route_Settlement AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Settlement_del (RouteId, SettlementID, last_modified )
	SELECT deleted.RouteId, deleted.SettlementID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Settlement - END */
/* [BUSDTA].[Route_Settlement_Detail] - begins */

/* TableDDL - [BUSDTA].[Route_Settlement_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement_Detail]
	(
	  [SettlementId] NVARCHAR(8) NOT NULL
	, [SettlementDetailId] NUMERIC(8,0) NOT NULL
	, [UserId] NUMERIC(8,0) NULL
	, [VerificationNum] NUMERIC(2,0) NOT NULL
	, [CashAmount] NUMERIC(8,4) NOT NULL
	, [CheckAmount] NUMERIC(8,4) NOT NULL
	, [MoneyOrderAmount] NUMERIC(8,4) NOT NULL
	, [TotalVerified] NUMERIC(8,4) NOT NULL
	, [Expenses] NUMERIC(8,4) NOT NULL
	, [Payments] NUMERIC(8,4) NOT NULL
	, [OverShortAmount] NUMERIC(8,4) NOT NULL
	, [VerSignature] VARBINARY(MAX) NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OriginatingRoute] NUMERIC(8,0) NOT NULL
	, [partitioningRoute] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_SETTLEMENT_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Settlement_Detail] PRIMARY KEY ([SettlementId] ASC, [SettlementDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Settlement_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement_Detail_del]
	(
	  [SettlementId] NVARCHAR(8)
	, [SettlementDetailId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SettlementId] ASC, [SettlementDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement_Detail] - End */
/* TRIGGERS FOR Route_Settlement_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Settlement_Detail_ins
	ON BUSDTA.Route_Settlement_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Settlement_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Settlement_Detail_del.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement_Detail_del.SettlementDetailId= inserted.SettlementDetailId AND BUSDTA.Route_Settlement_Detail_del.SettlementId= inserted.SettlementId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Settlement_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_Detail_upd
	ON BUSDTA.Route_Settlement_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Settlement_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Settlement_Detail.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement_Detail.SettlementDetailId= inserted.SettlementDetailId AND BUSDTA.Route_Settlement_Detail.SettlementId= inserted.SettlementId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_Detail_dlt
	ON BUSDTA.Route_Settlement_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Settlement_Detail_del (RouteId, SettlementDetailId, SettlementId, last_modified )
	SELECT deleted.RouteId, deleted.SettlementDetailId, deleted.SettlementId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Settlement_Detail - END */
/* [BUSDTA].[Cash_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Verification_Detail]
	(
	  [CashVerificationDetailId] NUMERIC(18,0) NOT NULL
	, [SettlementDetailId] NUMERIC(18,0) NOT NULL
	, [CashTypeId] NUMERIC(18,0) NOT NULL
	, [Quantity] NUMERIC(18,0) NULL
	, [Amount] NUMERIC(18,0) NULL
	, [RouteId] NUMERIC(18,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CASH_VERIFICATION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cash_Verification_Detail] PRIMARY KEY ([CashVerificationDetailId] ASC, [RouteId] ASC)
	)

END
/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Verification_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Verification_Detail_del]
	(
	  [CashVerificationDetailId] NUMERIC(18,0)
	, [RouteId] NUMERIC(18,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CashVerificationDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Verification_Detail] - End */
/* TRIGGERS FOR Cash_Verification_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cash_Verification_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cash_Verification_Detail_ins
	ON BUSDTA.Cash_Verification_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cash_Verification_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cash_Verification_Detail_del.CashVerificationDetailId= inserted.CashVerificationDetailId AND BUSDTA.Cash_Verification_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cash_Verification_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Verification_Detail_upd
	ON BUSDTA.Cash_Verification_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cash_Verification_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cash_Verification_Detail.CashVerificationDetailId= inserted.CashVerificationDetailId AND BUSDTA.Cash_Verification_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cash_Verification_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Verification_Detail_dlt
	ON BUSDTA.Cash_Verification_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cash_Verification_Detail_del (CashVerificationDetailId, RouteId, last_modified )
	SELECT deleted.CashVerificationDetailId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cash_Verification_Detail - END */
/* [BUSDTA].[MoneyOrder_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrder_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrder_Verification_Detail]
	(
	  [MoneyOrderVerificationDetailId] NUMERIC(18,0) NOT NULL
	, [SettlementDetailId] NUMERIC(18,0) NOT NULL
	, [MoneyOrderId] NUMERIC(18,0) NOT NULL
	, [RouteId] NUMERIC(18,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_MONEYORDER_VERIFICATION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_MoneyOrder_Verification_Detail] PRIMARY KEY ([MoneyOrderVerificationDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[MoneyOrder_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrder_Verification_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrder_Verification_Detail_del]
	(
	  [MoneyOrderVerificationDetailId] NUMERIC(18,0)
	, [RouteId] NUMERIC(18,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MoneyOrderVerificationDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[MoneyOrder_Verification_Detail] - End */
/* TRIGGERS FOR MoneyOrder_Verification_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.MoneyOrder_Verification_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.MoneyOrder_Verification_Detail_ins
	ON BUSDTA.MoneyOrder_Verification_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.MoneyOrder_Verification_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.MoneyOrder_Verification_Detail_del.MoneyOrderVerificationDetailId= inserted.MoneyOrderVerificationDetailId AND BUSDTA.MoneyOrder_Verification_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.MoneyOrder_Verification_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrder_Verification_Detail_upd
	ON BUSDTA.MoneyOrder_Verification_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.MoneyOrder_Verification_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.MoneyOrder_Verification_Detail.MoneyOrderVerificationDetailId= inserted.MoneyOrderVerificationDetailId AND BUSDTA.MoneyOrder_Verification_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.MoneyOrder_Verification_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrder_Verification_Detail_dlt
	ON BUSDTA.MoneyOrder_Verification_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.MoneyOrder_Verification_Detail_del (MoneyOrderVerificationDetailId, RouteId, last_modified )
	SELECT deleted.MoneyOrderVerificationDetailId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR MoneyOrder_Verification_Detail - END */


/* [BUSDTA].[Check_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Check_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Check_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Check_Verification_Detail]
	(
	  [CheckVerificationDetailId] NUMERIC(18,0) NOT NULL
	, [SettlementDetailId] NUMERIC(18,0) NOT NULL
	, [CheckDetailsId] NUMERIC(18,0) NOT NULL
	, [RouteId] NUMERIC(18,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CHECK_VERIFICATION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Check_Verification_Detail] PRIMARY KEY ([CheckVerificationDetailId] ASC, [RouteId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Check_Verification_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Check_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Check_Verification_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Check_Verification_Detail_del]
	(
	  [CheckVerificationDetailId] NUMERIC(18,0)
	, [RouteId] NUMERIC(18,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CheckVerificationDetailId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Check_Verification_Detail] - End */
/* TRIGGERS FOR Check_Verification_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Check_Verification_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Check_Verification_Detail_ins
	ON BUSDTA.Check_Verification_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Check_Verification_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Check_Verification_Detail_del.CheckVerificationDetailId= inserted.CheckVerificationDetailId AND BUSDTA.Check_Verification_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Check_Verification_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Check_Verification_Detail_upd
	ON BUSDTA.Check_Verification_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Check_Verification_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Check_Verification_Detail.CheckVerificationDetailId= inserted.CheckVerificationDetailId AND BUSDTA.Check_Verification_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Check_Verification_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Check_Verification_Detail_dlt
	ON BUSDTA.Check_Verification_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Check_Verification_Detail_del (CheckVerificationDetailId, RouteId, last_modified )
	SELECT deleted.CheckVerificationDetailId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Check_Verification_Detail - END */


/* [BUSDTA].[ExpenseDetails] - begins */

/* TableDDL - [BUSDTA].[ExpenseDetails] - Start */
IF OBJECT_ID('[BUSDTA].[ExpenseDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ExpenseDetails]
	(
	  [ExpenseId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionId] NUMERIC(8,0) NULL
	, [CategoryId] NUMERIC(8,0) NOT NULL
	, [ExpensesExplanation] NCHAR(100) NULL
	, [ExpenseAmount] NUMERIC(8,4) NULL
	, [StatusId] NUMERIC(8,0) NULL
	, [ExpensesDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC(8,0) NULL
	, [RouteSettlementId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_EXPENSEDETAILS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ExpenseDetails] PRIMARY KEY ([ExpenseId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ExpenseDetails] - End */

/* SHADOW TABLE FOR [BUSDTA].[ExpenseDetails] - Start */
IF OBJECT_ID('[BUSDTA].[ExpenseDetails_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ExpenseDetails_del]
	(
	  [ExpenseId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ExpenseId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ExpenseDetails] - End */
/* TRIGGERS FOR ExpenseDetails - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ExpenseDetails_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ExpenseDetails_ins
	ON BUSDTA.ExpenseDetails AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ExpenseDetails_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ExpenseDetails_del.ExpenseId= inserted.ExpenseId AND BUSDTA.ExpenseDetails_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ExpenseDetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ExpenseDetails_upd
	ON BUSDTA.ExpenseDetails AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ExpenseDetails
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ExpenseDetails.ExpenseId= inserted.ExpenseId AND BUSDTA.ExpenseDetails.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ExpenseDetails_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ExpenseDetails_dlt
	ON BUSDTA.ExpenseDetails AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ExpenseDetails_del (ExpenseId, RouteId, last_modified )
	SELECT deleted.ExpenseId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ExpenseDetails - END */

/* [BUSDTA].[MoneyOrderDetails] - begins */

/* TableDDL - [BUSDTA].[MoneyOrderDetails] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrderDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrderDetails]
	(
	  [MoneyOrderId] NUMERIC(8,0) NOT NULL
	, [MoneyOrderNumber] NCHAR(10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [MoneyOrderAmount] NUMERIC(8,4) NULL
	, [MoneyOrderFeeAmount] NUMERIC(8,4) NULL
	, [StatusId] NUMERIC(8,0) NULL
	, [MoneyOrderDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC(8,0) NULL
	, [RouteSettlementId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_MONEYORDERDETAILS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_MoneyOrderDetails] PRIMARY KEY ([MoneyOrderId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[MoneyOrderDetails] - End */

/* SHADOW TABLE FOR [BUSDTA].[MoneyOrderDetails] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrderDetails_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrderDetails_del]
	(
	  [MoneyOrderId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MoneyOrderId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[MoneyOrderDetails] - End */
/* TRIGGERS FOR MoneyOrderDetails - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.MoneyOrderDetails_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.MoneyOrderDetails_ins
	ON BUSDTA.MoneyOrderDetails AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.MoneyOrderDetails_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.MoneyOrderDetails_del.MoneyOrderId= inserted.MoneyOrderId AND BUSDTA.MoneyOrderDetails_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.MoneyOrderDetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrderDetails_upd
	ON BUSDTA.MoneyOrderDetails AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.MoneyOrderDetails
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.MoneyOrderDetails.MoneyOrderId= inserted.MoneyOrderId AND BUSDTA.MoneyOrderDetails.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.MoneyOrderDetails_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrderDetails_dlt
	ON BUSDTA.MoneyOrderDetails AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.MoneyOrderDetails_del (MoneyOrderId, RouteId, last_modified )
	SELECT deleted.MoneyOrderId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR MoneyOrderDetails - END */

/* [BUSDTA].[CheckDetails] - begins */

/* TableDDL - [BUSDTA].[CheckDetails] - Start */
IF OBJECT_ID('[BUSDTA].[CheckDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[CheckDetails]
	(
	  [CheckDetailsId] NUMERIC(8,0) NOT NULL
	, [CheckDetailsNumber] VARCHAR(10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CheckNumber] NCHAR(10) NULL
	, [CustomerId] NCHAR(10) NULL
	, [CustomerName] NCHAR(10) NULL
	, [CheckAmount] NUMERIC(8,2) NULL
	, [CheckDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CHECKDETAILS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_CheckDetails] PRIMARY KEY ([CheckDetailsId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[CheckDetails] - End */

/* SHADOW TABLE FOR [BUSDTA].[CheckDetails] - Start */
IF OBJECT_ID('[BUSDTA].[CheckDetails_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[CheckDetails_del]
	(
	  [CheckDetailsId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CheckDetailsId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[CheckDetails] - End */
/* TRIGGERS FOR CheckDetails - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.CheckDetails_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.CheckDetails_ins
	ON BUSDTA.CheckDetails AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.CheckDetails_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.CheckDetails_del.CheckDetailsId= inserted.CheckDetailsId AND BUSDTA.CheckDetails_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.CheckDetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CheckDetails_upd
	ON BUSDTA.CheckDetails AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.CheckDetails
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.CheckDetails.CheckDetailsId= inserted.CheckDetailsId AND BUSDTA.CheckDetails.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.CheckDetails_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CheckDetails_dlt
	ON BUSDTA.CheckDetails AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.CheckDetails_del (CheckDetailsId, RouteId, last_modified )
	SELECT deleted.CheckDetailsId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR CheckDetails - END */

/* [BUSDTA].[Vehicle_Master] - begins */

/* TableDDL - [BUSDTA].[Vehicle_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Vehicle_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Vehicle_Master]
	(
	  [VehicleID] NUMERIC(8,0) NOT NULL
	, [VINNumber] NVARCHAR(20) NOT NULL
	, [VehicleNumber] NUMERIC(8,0) NULL
	, [VehicleMake] NCHAR(30) NULL
	, [VehicleColour] NCHAR(20) NULL
	, [VehicleAxle] NUMERIC(1,0) NULL
	, [VehicleFuel] NCHAR(10) NULL
	, [VehicleMilage] NUMERIC(3,0) NULL
	, [Isactive] BIT NULL
	, [VehicleManufacturingDt] DATETIME NULL
	, [VehicleExpiryDt] DATE NULL
	, [VehicleOwner] NCHAR(25) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_VEHICLE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Vehicle_Master] PRIMARY KEY ([VehicleID] ASC, [VINNumber] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Vehicle_Master] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[Vehicle_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Vehicle_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Vehicle_Master_del]
	(
	  [VehicleID] NUMERIC(8,0)
	, [VINNumber] NVARCHAR(20)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([VehicleID] ASC, [VINNumber] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Vehicle_Master] - End */
/* TRIGGERS FOR Vehicle_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Vehicle_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Vehicle_Master_ins
	ON BUSDTA.Vehicle_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Vehicle_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Vehicle_Master_del.VehicleID= inserted.VehicleID AND BUSDTA.Vehicle_Master_del.VINNumber= inserted.VINNumber
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Vehicle_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Vehicle_Master_upd
	ON BUSDTA.Vehicle_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Vehicle_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Vehicle_Master.VehicleID= inserted.VehicleID AND BUSDTA.Vehicle_Master.VINNumber= inserted.VINNumber');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Vehicle_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Vehicle_Master_dlt
	ON BUSDTA.Vehicle_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Vehicle_Master_del (VehicleID, VINNumber, last_modified )
	SELECT deleted.VehicleID, deleted.VINNumber, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Vehicle_Master - END */
/* [BUSDTA].[Status_Type] - begins */

/* TableDDL - [BUSDTA].[Status_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Status_Type]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Status_Type]
	(
	  [StatusTypeID] NUMERIC(8,0) NOT NULL
	, [StatusTypeCD] VARCHAR(10) NOT NULL
	, [StatusTypeDESC] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_STATUS_TYPE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_STATUS_TYPE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_STATUS_TYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Status_Type] PRIMARY KEY ([StatusTypeCD] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Status_Type] - End */

/* SHADOW TABLE FOR [BUSDTA].[Status_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Status_Type_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Status_Type_del]
	(
	  [StatusTypeCD] VARCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([StatusTypeCD] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Status_Type] - End */
/* TRIGGERS FOR Status_Type - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Status_Type_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Status_Type_ins
	ON BUSDTA.Status_Type AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Status_Type_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Status_Type_del.StatusTypeCD= inserted.StatusTypeCD
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Status_Type_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Status_Type_upd
	ON BUSDTA.Status_Type AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Status_Type
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Status_Type.StatusTypeCD= inserted.StatusTypeCD');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Status_Type_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Status_Type_dlt
	ON BUSDTA.Status_Type AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Status_Type_del (StatusTypeCD, last_modified )
	SELECT deleted.StatusTypeCD, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Status_Type - END */
/* [BUSDTA].[Route_Master] - begins */

/* TableDDL - [BUSDTA].[Route_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Master]
	(
	  [RouteMasterID] NUMERIC(8,0) NOT NULL
	, [RouteName] VARCHAR(10) NOT NULL
	, [RouteDescription] NCHAR(50) NULL
	, [RouteAdressBookNumber] NUMERIC(8,0) NOT NULL
	, [BranchNumber] NCHAR(12) NULL
	, [BranchAdressBookNumber] NUMERIC(8,0) NULL
	, [DefaultUser] NUMERIC(8,0) NULL
	, [VehicleID] NUMERIC(8,0) NULL
	, [RepnlBranch] NUMERIC(8,0) NULL CONSTRAINT DF_ROUTE_MASTER_RepnlBranch DEFAULT((101))
	, [RepnlBranchType] NVARCHAR(50) NULL CONSTRAINT DF_ROUTE_MASTER_RepnlBranchType DEFAULT(N'Sales Branch')
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [RouteStatusID] INT NULL CONSTRAINT DF_ROUTE_MASTER_RouteStatusID DEFAULT((1))
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Master] PRIMARY KEY ([RouteMasterID] ASC, [RouteName] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Master_del]
	(
	  [RouteMasterID] NUMERIC(8,0)
	, [RouteName] VARCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RouteMasterID] ASC, [RouteName] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Master] - End */
/* TRIGGERS FOR Route_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Master_ins
	ON BUSDTA.Route_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Master_del.RouteMasterID= inserted.RouteMasterID AND BUSDTA.Route_Master_del.RouteName= inserted.RouteName
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Master_upd
	ON BUSDTA.Route_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Master.RouteMasterID= inserted.RouteMasterID AND BUSDTA.Route_Master.RouteName= inserted.RouteName');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Master_dlt
	ON BUSDTA.Route_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Master_del (RouteMasterID, RouteName, last_modified )
	SELECT deleted.RouteMasterID, deleted.RouteName, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Master - END */
/* [BUSDTA].[RouteStatus] - begins */

/* TableDDL - [BUSDTA].[RouteStatus] - Start */
IF OBJECT_ID('[BUSDTA].[RouteStatus]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RouteStatus]
	(
	  [RouteStatusID] INT NOT NULL
	, [RouteStatus] VARCHAR(100) NULL
	, [StatusDescription] VARCHAR(500) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_ROUTESTATUS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_RouteStatus] PRIMARY KEY ([RouteStatusID] ASC)
	)

END
/* TableDDL - [BUSDTA].[RouteStatus] - End */

/* [BUSDTA].[Entity_Range_Master] - begins */

/* TableDDL - [BUSDTA].[Entity_Range_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Range_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Range_Master]
	(
	  [EntityRangeMasterID] DECIMAL(8,0) NOT NULL
	, [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [RangeStart] DECIMAL(18,0) NULL
	, [RangeEnd] DECIMAL(18,0) NULL
	, [LastAlloted] DECIMAL(18,0) NULL
	, [Size] DECIMAL(8,0) NULL
	, [Source] VARCHAR(100) NULL
	, [Active] BIT NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ENTITY_RANGE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Entity_Range_Master] PRIMARY KEY ([EntityRangeMasterID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Entity_Range_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Entity_Range_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Range_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Range_Master_del]
	(
	  [EntityRangeMasterID] DECIMAL(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EntityRangeMasterID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Entity_Range_Master] - End */
/* TRIGGERS FOR Entity_Range_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Entity_Range_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Entity_Range_Master_ins
	ON BUSDTA.Entity_Range_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Entity_Range_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Entity_Range_Master_del.EntityRangeMasterID= inserted.EntityRangeMasterID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Entity_Range_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Range_Master_upd
	ON BUSDTA.Entity_Range_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Entity_Range_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Entity_Range_Master.EntityRangeMasterID= inserted.EntityRangeMasterID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Entity_Range_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Range_Master_dlt
	ON BUSDTA.Entity_Range_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Entity_Range_Master_del (EntityRangeMasterID, last_modified )
	SELECT deleted.EntityRangeMasterID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Entity_Range_Master - END */
/* [BUSDTA].[Entity_Number_Status] - begins */

/* TableDDL - [BUSDTA].[Entity_Number_Status] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Number_Status]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Number_Status]
	(
	  [EntityNumberStatusID] DECIMAL(8,0) NOT NULL IDENTITY(1,1)
	, [RouteId] DECIMAL(8,0) NULL
	, [Bucket] VARCHAR(30) NULL
	, [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [RangeStart] DECIMAL(8,0) NULL
	, [RangeEnd] DECIMAL(8,0) NULL
	, [CurrentAllocated] DECIMAL(8,0) NULL
	, [IsConsumed] BIT NULL
	, [ConsumedOnDate] SMALLDATETIME NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ENTITY_NUMBER_STATUS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Entity_Number_Status] PRIMARY KEY ([EntityNumberStatusID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Entity_Number_Status] - End */

/* SHADOW TABLE FOR [BUSDTA].[Entity_Number_Status] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Number_Status_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Number_Status_del]
	(
	  [EntityNumberStatusID] DECIMAL(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EntityNumberStatusID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Entity_Number_Status] - End */
/* TRIGGERS FOR Entity_Number_Status - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Entity_Number_Status_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Entity_Number_Status_ins
	ON BUSDTA.Entity_Number_Status AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Entity_Number_Status_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Entity_Number_Status_del.EntityNumberStatusID= inserted.EntityNumberStatusID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Entity_Number_Status_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Number_Status_upd
	ON BUSDTA.Entity_Number_Status AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Entity_Number_Status
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Entity_Number_Status.EntityNumberStatusID= inserted.EntityNumberStatusID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Entity_Number_Status_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Number_Status_dlt
	ON BUSDTA.Entity_Number_Status AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Entity_Number_Status_del (EntityNumberStatusID, last_modified )
	SELECT deleted.EntityNumberStatusID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Entity_Number_Status - END */

/* [BUSDTA].[Entity_Bucket_Master] - begins */

/* TableDDL - [BUSDTA].[Entity_Bucket_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Bucket_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Bucket_Master]
	(
	  [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [Entity] NCHAR(50) NOT NULL
	, [BucketSize] DECIMAL(4,0) NULL
	, [NoOfBuckets] DECIMAL(4,0) NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ENTITY_BUCKET_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Entity_Bucket_Master] PRIMARY KEY ([EntityBucketMasterId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Entity_Bucket_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Entity_Bucket_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Bucket_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Bucket_Master_del]
	(
	  [EntityBucketMasterId] DECIMAL(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EntityBucketMasterId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Entity_Bucket_Master] - End */
/* TRIGGERS FOR Entity_Bucket_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Entity_Bucket_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Entity_Bucket_Master_ins
	ON BUSDTA.Entity_Bucket_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Entity_Bucket_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Entity_Bucket_Master_del.EntityBucketMasterId= inserted.EntityBucketMasterId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Entity_Bucket_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Bucket_Master_upd
	ON BUSDTA.Entity_Bucket_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Entity_Bucket_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Entity_Bucket_Master.EntityBucketMasterId= inserted.EntityBucketMasterId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Entity_Bucket_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Bucket_Master_dlt
	ON BUSDTA.Entity_Bucket_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Entity_Bucket_Master_del (EntityBucketMasterId, last_modified )
	SELECT deleted.EntityBucketMasterId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Entity_Bucket_Master - END */


/* [BUSDTA].[Invoice_Header] - begins */

/* TableDDL - [BUSDTA].[Invoice_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Invoice_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Invoice_Header]
	(
	  [InvoiceID] NUMERIC(8,0) NOT NULL
	, [CustShiptoId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OrderId] NUMERIC(8,0) NOT NULL
	, [ARInvoiceNumber] NCHAR(20) NULL
	, [DeviceInvoiceNumber] NCHAR(20) NULL
	, [ARInvoiceDate] DATE NULL
	, [DeviceInvoiceDate] DATE NULL
	, [DeviceStatusID] NUMERIC(3,0) NOT NULL
	, [ARStatusID] NUMERIC(3,0) NOT NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [DueDate] DATETIME NULL
	, [InvoicePaymentType] NCHAR(8) NULL
	, [GrossAmt] NUMERIC(8,4) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_INVOICE_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Invoice_Header] PRIMARY KEY ([InvoiceID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Invoice_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Invoice_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Invoice_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Invoice_Header_del]
	(
	  [InvoiceID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([InvoiceID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Invoice_Header] - End */
/* TRIGGERS FOR Invoice_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Invoice_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Invoice_Header_ins
	ON BUSDTA.Invoice_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Invoice_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Invoice_Header_del.InvoiceID= inserted.InvoiceID AND BUSDTA.Invoice_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Invoice_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Invoice_Header_upd
	ON BUSDTA.Invoice_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Invoice_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Invoice_Header.InvoiceID= inserted.InvoiceID AND BUSDTA.Invoice_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Invoice_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Invoice_Header_dlt
	ON BUSDTA.Invoice_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Invoice_Header_del (InvoiceID, RouteId, last_modified )
	SELECT deleted.InvoiceID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Invoice_Header - END */

/* [BUSDTA].[Receipt_Header] - begins */

/* TableDDL - [BUSDTA].[Receipt_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Receipt_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Receipt_Header]
	(
	  [CustShipToId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ReceiptID] NUMERIC(8,0) NOT NULL
	, [ReceiptNumber] NCHAR(20) NULL
	, [ReceiptDate] DATE NULL
	, [TransactionAmount] NUMERIC(10,4) NULL
	, [PaymentMode] NCHAR(1) NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [CreditReasonCodeId] NUMERIC(3,0) NULL
	, [CreditMemoNote] NCHAR(200) NULL
	, [TransactionMode] NCHAR(1) NULL
	, [ChequeNum] NCHAR(10) NULL
	, [ChequeDate] DATE NULL
	, [StatusId] NUMERIC(3,0) NULL
	, [JDEStatusId] NUMERIC(3,0) NULL
	, [JDEReferenceId] NUMERIC(8,0) NULL
	, [SettelmentId] NUMERIC(8,0) NULL
	, [AuthoritySignature] VARBINARY(MAX) NULL
	, [CustomerSignature] VARBINARY(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_RECEIPT_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Receipt_Header] PRIMARY KEY ([CustShipToId] ASC, [RouteId] ASC, [ReceiptID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Receipt_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Receipt_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Receipt_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Receipt_Header_del]
	(
	  [CustShipToId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, [ReceiptID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustShipToId] ASC, [RouteId] ASC, [ReceiptID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Receipt_Header] - End */
/* TRIGGERS FOR Receipt_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Receipt_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Receipt_Header_ins
	ON BUSDTA.Receipt_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Receipt_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Receipt_Header_del.CustShipToId= inserted.CustShipToId AND BUSDTA.Receipt_Header_del.ReceiptID= inserted.ReceiptID AND BUSDTA.Receipt_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Receipt_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Receipt_Header_upd
	ON BUSDTA.Receipt_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Receipt_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Receipt_Header.CustShipToId= inserted.CustShipToId AND BUSDTA.Receipt_Header.ReceiptID= inserted.ReceiptID AND BUSDTA.Receipt_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Receipt_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Receipt_Header_dlt
	ON BUSDTA.Receipt_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Receipt_Header_del (CustShipToId, ReceiptID, RouteId, last_modified )
	SELECT deleted.CustShipToId, deleted.ReceiptID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Receipt_Header - END */

/* [BUSDTA].[Customer_Ledger] - begins */

/* TableDDL - [BUSDTA].[Customer_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Ledger]
	(
	  [CustomerLedgerId] NUMERIC(8,0) NOT NULL
	, [ReceiptID] NUMERIC(8,0) NULL
	, [InvoiceId] NUMERIC(8,0) NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CustShipToId] NUMERIC(8,0) NOT NULL
	, [IsActive] NCHAR(1) NULL
	, [InvoiceGrossAmt] NUMERIC(10,4) NULL
	, [InvoiceOpenAmt] NUMERIC(10,4) NULL
	, [ReceiptUnAppliedAmt] NUMERIC(10,4) NULL
	, [ReceiptAppliedAmt] NUMERIC(10,4) NULL
	, [ConcessionAmt] NUMERIC(10,4) NULL
	, [ConcessionCodeId] NUMERIC(3,0) NULL
	, [DateApplied] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CUSTOMER_LEDGER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Customer_Ledger] PRIMARY KEY ([CustomerLedgerId] ASC, [RouteId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Customer_Ledger] - End */

/* SHADOW TABLE FOR [BUSDTA].[Customer_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Ledger_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Ledger_del]
	(
	  [CustomerLedgerId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerLedgerId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Customer_Ledger] - End */
/* TRIGGERS FOR Customer_Ledger - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Customer_Ledger_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Customer_Ledger_ins
	ON BUSDTA.Customer_Ledger AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Customer_Ledger_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Customer_Ledger_del.CustomerLedgerId= inserted.CustomerLedgerId AND BUSDTA.Customer_Ledger_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Customer_Ledger_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Ledger_upd
	ON BUSDTA.Customer_Ledger AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Customer_Ledger
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Customer_Ledger.CustomerLedgerId= inserted.CustomerLedgerId AND BUSDTA.Customer_Ledger.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Customer_Ledger_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Ledger_dlt
	ON BUSDTA.Customer_Ledger AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Customer_Ledger_del (CustomerLedgerId, RouteId, last_modified )
	SELECT deleted.CustomerLedgerId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Customer_Ledger - END */


/* [BUSDTA].[Cycle_Count_Header] - begins */

/* TableDDL - [BUSDTA].[Cycle_Count_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Header]
	(
	  [CycleCountID] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(3,0) NOT NULL
	, [IsCountInitiated] NCHAR(1) NULL
	, [CycleCountTypeId] NUMERIC(3,0) NOT NULL
	, [InitiatorId] NUMERIC(8,0) NULL
	, [CycleCountDatetime] DATETIME NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [JDEQty] NUMERIC(3,0) NULL
	, [JDEUM] NCHAR(2) NULL
	, [JDECycleCountNum] NUMERIC(16,0) NULL
	, [JDECycleCountDocType] NUMERIC(16,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CYCLE_COUNT_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cycle_Count_Header] PRIMARY KEY ([CycleCountID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Cycle_Count_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Header_del]
	(
	  [CycleCountID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CycleCountID] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Header] - End */
/* TRIGGERS FOR Cycle_Count_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cycle_Count_Header_ins
	ON BUSDTA.Cycle_Count_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cycle_Count_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cycle_Count_Header_del.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cycle_Count_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Header_upd
	ON BUSDTA.Cycle_Count_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cycle_Count_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cycle_Count_Header.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Header_dlt
	ON BUSDTA.Cycle_Count_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cycle_Count_Header_del (CycleCountID, RouteId, last_modified )
	SELECT deleted.CycleCountID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cycle_Count_Header - END */


/* [BUSDTA].[Cycle_Count_Detail] - begins */

/* TableDDL - [BUSDTA].[Cycle_Count_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Detail]
	(
	  [CycleCountID] NUMERIC(8,0) NOT NULL
	, [CycleCountDetailID] NUMERIC(8,0) NOT NULL
	, [ReCountNumber] NUMERIC(2,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [CountedQty] NUMERIC(4,0) NOT NULL
	, [ItemStatus] NCHAR(1) NULL
	, [OnHandQty] NUMERIC(4,0) NULL
	, [HeldQty] NUMERIC(4,0) NULL
	, [CountAccepted] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CYCLE_COUNT_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cycle_Count_Detail] PRIMARY KEY ([CycleCountID] ASC, [CycleCountDetailID] ASC, [ReCountNumber] ASC, [ItemId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Cycle_Count_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Detail_del]
	(
	  [CycleCountID] NUMERIC(8,0)
	, [CycleCountDetailID] NUMERIC(8,0)
	, [ReCountNumber] NUMERIC(2,0)
	, [ItemId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CycleCountID] ASC, [CycleCountDetailID] ASC, [ReCountNumber] ASC, [ItemId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Detail] - End */
/* TRIGGERS FOR Cycle_Count_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cycle_Count_Detail_ins
	ON BUSDTA.Cycle_Count_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cycle_Count_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cycle_Count_Detail_del.CycleCountDetailID= inserted.CycleCountDetailID AND BUSDTA.Cycle_Count_Detail_del.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Detail_del.ItemId= inserted.ItemId AND BUSDTA.Cycle_Count_Detail_del.ReCountNumber= inserted.ReCountNumber
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cycle_Count_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Detail_upd
	ON BUSDTA.Cycle_Count_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cycle_Count_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cycle_Count_Detail.CycleCountDetailID= inserted.CycleCountDetailID AND BUSDTA.Cycle_Count_Detail.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Detail.ItemId= inserted.ItemId AND BUSDTA.Cycle_Count_Detail.ReCountNumber= inserted.ReCountNumber');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Detail_dlt
	ON BUSDTA.Cycle_Count_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cycle_Count_Detail_del (CycleCountDetailID, CycleCountID, ItemId, ReCountNumber, last_modified )
	SELECT deleted.CycleCountDetailID, deleted.CycleCountID, deleted.ItemId, deleted.ReCountNumber, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cycle_Count_Detail - END */

/* [BUSDTA].[Pick_Detail] - begins */

/* TableDDL - [BUSDTA].[Pick_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Pick_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Pick_Detail]
	(
	  [PickDetailID] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionID] NUMERIC(8,0) NOT NULL
	, [TransactionDetailID] NUMERIC(8,0) NOT NULL
	, [TransactionTypeId] NUMERIC(8,0) NOT NULL
	, [TransactionStatusId] NUMERIC(8,0) NULL
	, [ItemID] NCHAR(25) NULL
	, [TransactionQty] NUMERIC(8,0) NULL
	, [TransactionUOM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUOM] NUMERIC(8,0) NULL
	, [PrimaryUOM] NCHAR(2) NULL
	, [PickedQty] NUMERIC(8,0) NULL
	, [PickQtyPrimaryUOM] NUMERIC(8,0) NULL
	, [LastScanMode] NUMERIC(3,0) NULL
	, [ItemScanSeq] NUMERIC(3,0) NULL
	, [IsOnHold] NCHAR(1) NULL
	, [PickAdjusted] NUMERIC(1,0) NOT NULL
	, [AdjustedQty] NUMERIC(4,0) NOT NULL
	, [ReasonCodeId] NUMERIC(8,0) NULL
	, [ManuallyPickCount] NUMERIC(8,0) NULL
	, [IsInException] NCHAR(1) NULL
	, [ExceptionReasonCodeId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PICK_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Pick_Detail] PRIMARY KEY ([PickDetailID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Pick_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Pick_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Pick_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Pick_Detail_del]
	(
	  [PickDetailID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PickDetailID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Pick_Detail] - End */
/* TRIGGERS FOR Pick_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Pick_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Pick_Detail_ins
	ON BUSDTA.Pick_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Pick_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Pick_Detail_del.PickDetailID= inserted.PickDetailID AND BUSDTA.Pick_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Pick_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Pick_Detail_upd
	ON BUSDTA.Pick_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Pick_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Pick_Detail.PickDetailID= inserted.PickDetailID AND BUSDTA.Pick_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Pick_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Pick_Detail_dlt
	ON BUSDTA.Pick_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Pick_Detail_del (PickDetailID, RouteId, last_modified )
	SELECT deleted.PickDetailID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Pick_Detail - END */
/* [BUSDTA].[Route_Replenishment_Header] - begins */

/* TableDDL - [BUSDTA].[Route_Replenishment_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Header]
	(
	  [ReplenishmentID] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(8,0) NOT NULL
	, [ReplenishmentTypeId] NUMERIC(8,0) NOT NULL
	, [TransDateFrom] DATETIME NULL
	, [TransDateTo] DATETIME NULL
	, [RequestedBy] NUMERIC(8,0) NULL
	, [FromBranchId] NUMERIC(8,0) NULL
	, [ToBranchId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_REPLENISHMENT_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Replenishment_Header] PRIMARY KEY ([ReplenishmentID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Replenishment_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Header_del]
	(
	  [ReplenishmentID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ReplenishmentID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Header] - End */
/* TRIGGERS FOR Route_Replenishment_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Replenishment_Header_ins
	ON BUSDTA.Route_Replenishment_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Replenishment_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Replenishment_Header_del.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Header_upd
	ON BUSDTA.Route_Replenishment_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Replenishment_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Replenishment_Header.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Header_dlt
	ON BUSDTA.Route_Replenishment_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Replenishment_Header_del (ReplenishmentID, RouteId, last_modified )
	SELECT deleted.ReplenishmentID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Replenishment_Header - END */
/* [BUSDTA].[Route_Replenishment_Detail] - begins */

/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Detail]
	(
	  [ReplenishmentID] NUMERIC(8,0) NOT NULL
	, [ReplenishmentDetailID] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ReplenishmentQtyUM] NCHAR(2) NOT NULL
	, [ReplenishmentQty] NUMERIC(8,0) NOT NULL
	, [AdjustmentQty] NUMERIC(8,0) NOT NULL
	, [PickedQty] NUMERIC(8,0) NULL
	, [PickedStatusTypeID] NUMERIC(3,0) NULL
	, [AvailaibilityAtTime] NUMERIC(8,0) NULL
	, [CurrentAvailability] NUMERIC(8,0) NULL
	, [DemandQty] NUMERIC(8,0) NULL
	, [ParLevelQty] NUMERIC(8,0) NULL
	, [OpenReplnQty] NUMERIC(8,0) NULL
	, [ShippedQty] NUMERIC(8,0) NULL
	, [OrderID] NUMERIC(8,0) NULL
	, [LastSaved] DATETIME NULL
	, [SequenceNumber] NUMERIC(3,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_REPLENISHMENT_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Replenishment_Detail] PRIMARY KEY ([ReplenishmentID] ASC, [ReplenishmentDetailID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Detail_del]
	(
	  [ReplenishmentID] NUMERIC(8,0)
	, [ReplenishmentDetailID] NUMERIC(8,0)
	, [ItemId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ReplenishmentID] ASC, [ReplenishmentDetailID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Detail] - End */
/* TRIGGERS FOR Route_Replenishment_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Replenishment_Detail_ins
	ON BUSDTA.Route_Replenishment_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Replenishment_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Replenishment_Detail_del.ItemId= inserted.ItemId AND BUSDTA.Route_Replenishment_Detail_del.ReplenishmentDetailID= inserted.ReplenishmentDetailID AND BUSDTA.Route_Replenishment_Detail_del.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Detail_upd
	ON BUSDTA.Route_Replenishment_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Replenishment_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Replenishment_Detail.ItemId= inserted.ItemId AND BUSDTA.Route_Replenishment_Detail.ReplenishmentDetailID= inserted.ReplenishmentDetailID AND BUSDTA.Route_Replenishment_Detail.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Detail_dlt
	ON BUSDTA.Route_Replenishment_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Replenishment_Detail_del (ItemId, ReplenishmentDetailID, ReplenishmentID, RouteId, last_modified )
	SELECT deleted.ItemId, deleted.ReplenishmentDetailID, deleted.ReplenishmentID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Replenishment_Detail - END */
/* [BUSDTA].[Metric_CustHistDemand] - begins */

/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - Start */
IF OBJECT_ID('[BUSDTA].[Metric_CustHistDemand]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Metric_CustHistDemand]
	(
	  [CustomerId] NUMERIC(8,0) NOT NULL
	, [ItemID] NUMERIC(8,0) NOT NULL
	, [DemandFromDate] DATE NULL
	, [DemandThroughDate] DATE NULL
	, [PlannedStops] NUMERIC(4,0) NULL
	, [ActualStops] NUMERIC(4,0) NULL
	, [TotalQtySold] NUMERIC(12,0) NULL
	, [TotalQtyReturned] NUMERIC(12,0) NULL
	, [QtySoldPerStop] NUMERIC(12,4) NULL
	, [QtyReturnedPerStop] NUMERIC(12,4) NULL
	, [NetQtySoldPerStop] NUMERIC(12,4) NULL
	, [QtySoldDeltaPerStop] NUMERIC(12,4) NULL
	, [QtyReturnedDeltaPerStop] NUMERIC(12,4) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_METRIC_CUSTHISTDEMAND_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Metric_CustHistDemand] PRIMARY KEY ([CustomerId] ASC, [ItemID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - End */

/* SHADOW TABLE FOR [BUSDTA].[Metric_CustHistDemand] - Start */
IF OBJECT_ID('[BUSDTA].[Metric_CustHistDemand_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Metric_CustHistDemand_del]
	(
	  [CustomerId] NUMERIC(8,0)
	, [ItemID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerId] ASC, [ItemID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Metric_CustHistDemand] - End */
/* TRIGGERS FOR Metric_CustHistDemand - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Metric_CustHistDemand_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Metric_CustHistDemand_ins
	ON BUSDTA.Metric_CustHistDemand AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Metric_CustHistDemand_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Metric_CustHistDemand_del.CustomerId= inserted.CustomerId AND BUSDTA.Metric_CustHistDemand_del.ItemID= inserted.ItemID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Metric_CustHistDemand_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Metric_CustHistDemand_upd
	ON BUSDTA.Metric_CustHistDemand AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Metric_CustHistDemand
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Metric_CustHistDemand.CustomerId= inserted.CustomerId AND BUSDTA.Metric_CustHistDemand.ItemID= inserted.ItemID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Metric_CustHistDemand_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Metric_CustHistDemand_dlt
	ON BUSDTA.Metric_CustHistDemand AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Metric_CustHistDemand_del (CustomerId, ItemID, last_modified )
	SELECT deleted.CustomerId, deleted.ItemID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Metric_CustHistDemand - END */
/* [BUSDTA].[Notification] - begins */

/* TableDDL - [BUSDTA].[Notification] - Start */
IF OBJECT_ID('[BUSDTA].[Notification]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Notification]
	(
	  [NotificationID] NUMERIC(8,0) NOT NULL IDENTITY(1,1)
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [Title] NCHAR(200) NOT NULL
	, [Description] NCHAR(300) NOT NULL
	, [TypeID] NUMERIC(4,0) NULL
	, [Category] NUMERIC(4,0) NULL
	, [SubCategory] NUMERIC(4,0) NULL
	, [Read] NCHAR(1) NULL
	, [ValidTill] DATE NULL
	, [Acknowledged] NCHAR(1) NULL
	, [Initiator] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_NOTIFICATION_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Notification] PRIMARY KEY ([NotificationID] ASC, [RouteID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Notification] - End */

/* SHADOW TABLE FOR [BUSDTA].[Notification] - Start */
IF OBJECT_ID('[BUSDTA].[Notification_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Notification_del]
	(
	  [NotificationID] NUMERIC(8,0)
	, [RouteID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([NotificationID] ASC, [RouteID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Notification] - End */
/* TRIGGERS FOR Notification - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Notification_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Notification_ins
	ON BUSDTA.Notification AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Notification_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Notification_del.NotificationID= inserted.NotificationID AND BUSDTA.Notification_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Notification_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Notification_upd
	ON BUSDTA.Notification AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Notification
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Notification.NotificationID= inserted.NotificationID AND BUSDTA.Notification.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Notification_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Notification_dlt
	ON BUSDTA.Notification AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Notification_del (NotificationID, RouteID, last_modified )
	SELECT deleted.NotificationID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Notification - END */
/* [BUSDTA].[tblSystemSetup] - begins */

/* TableDDL - [BUSDTA].[tblSystemSetup] - Start */
IF OBJECT_ID('[BUSDTA].[tblSystemSetup]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblSystemSetup]
	(
	  [SystemSetupID] INT NOT NULL IDENTITY(1,1)
	, [KeyName] VARCHAR(128) NOT NULL
	, [KeyValue] VARCHAR(MAX) NOT NULL
	, [Description] VARCHAR(MAX) NULL
	, [ActiveYN] BIT NULL CONSTRAINT DF_TBLSYSTEMSETUP_ActiveYN DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLSYSTEMSETUP_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYSTEMSETUP_EditedOn DEFAULT(getdate())
	, CONSTRAINT [PK_tblSystemSetup] PRIMARY KEY ([SystemSetupID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblSystemSetup] - End */
GO

IF NOT EXISTS(SELECT 1 FROM tblSystemSetup WHERE KeyName='SystemDBConnection')
BEGIN
	INSERT [BUSDTA].[tblSystemSetup] ([KeyName], [KeyValue], [Description], [CreatedBy], [EditedBy]) 
	VALUES (N'SystemDBConnection', N'Data Source=HP-1\SQLEXPRESS2012;Initial Catalog=EnvironmentDB;User ID=dba;Password=sql', N'Conneciton for SystemDB', N'sjanardh', N'sjanardh')
END
GO

IF NOT EXISTS(SELECT 1 FROM tblSystemSetup WHERE KeyName='LogFileLocaiton')
BEGIN
	INSERT [BUSDTA].[tblSystemSetup] ([KeyName], [KeyValue], [Description], [CreatedBy], [EditedBy]) 
	VALUES (N'LogFileLocaiton', N'E:\bitbucket\slesync\Releases\Development\D.3.2.1', N'Location for Log File', N'sjanardh', N'sjanardh')
END
GO
/* [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - begins */

/* TableDDL - [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[SalesOrder_ReturnOrder_Mapping]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[SalesOrder_ReturnOrder_Mapping]
	(
	  [SalesOrderId] NUMERIC(8,0) NOT NULL
	, [ReturnOrderID] NUMERIC(8,0) NOT NULL
	, [OrderDetailID] NUMERIC(8,0) NOT NULL
	, [ItemID] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_SALESORDER_RETURNORDER_MAPPING_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_SalesOrder_ReturnOrder_Mapping] PRIMARY KEY ([SalesOrderId] ASC, [ReturnOrderID] ASC, [OrderDetailID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - End */

/* SHADOW TABLE FOR [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[SalesOrder_ReturnOrder_Mapping_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[SalesOrder_ReturnOrder_Mapping_del]
	(
	  [SalesOrderId] NUMERIC(8,0)
	, [ReturnOrderID] NUMERIC(8,0)
	, [OrderDetailID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SalesOrderId] ASC, [ReturnOrderID] ASC, [OrderDetailID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - End */
/* TRIGGERS FOR SalesOrder_ReturnOrder_Mapping - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.SalesOrder_ReturnOrder_Mapping_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.SalesOrder_ReturnOrder_Mapping_ins
	ON BUSDTA.SalesOrder_ReturnOrder_Mapping AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.SalesOrder_ReturnOrder_Mapping_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.SalesOrder_ReturnOrder_Mapping_del.OrderDetailID= inserted.OrderDetailID AND BUSDTA.SalesOrder_ReturnOrder_Mapping_del.ReturnOrderID= inserted.ReturnOrderID AND BUSDTA.SalesOrder_ReturnOrder_Mapping_del.SalesOrderId= inserted.SalesOrderId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.SalesOrder_ReturnOrder_Mapping_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.SalesOrder_ReturnOrder_Mapping_upd
	ON BUSDTA.SalesOrder_ReturnOrder_Mapping AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.SalesOrder_ReturnOrder_Mapping
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.SalesOrder_ReturnOrder_Mapping.OrderDetailID= inserted.OrderDetailID AND BUSDTA.SalesOrder_ReturnOrder_Mapping.ReturnOrderID= inserted.ReturnOrderID AND BUSDTA.SalesOrder_ReturnOrder_Mapping.SalesOrderId= inserted.SalesOrderId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.SalesOrder_ReturnOrder_Mapping_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.SalesOrder_ReturnOrder_Mapping_dlt
	ON BUSDTA.SalesOrder_ReturnOrder_Mapping AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.SalesOrder_ReturnOrder_Mapping_del (OrderDetailID, ReturnOrderID, SalesOrderId, last_modified )
	SELECT deleted.OrderDetailID, deleted.ReturnOrderID, deleted.SalesOrderId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR SalesOrder_ReturnOrder_Mapping - END */

IF OBJECT_ID('[BUSDTA].[F0115]') IS NULL
BEGIN

CREATE TABLE [BUSDTA].[F0115](
	[WPAN8] [numeric](8, 0) NOT NULL,
	[WPIDLN] [numeric](5, 0) NOT NULL,
	[WPRCK7] [numeric](5, 0) NOT NULL,
	[WPCNLN] [numeric](5, 0) NOT NULL,
	[WPPHTP] [nchar](4) NULL,
	[WPAR1] [nchar](6) NULL,
	[WPPH1] [nchar](20) NULL,
	[last_modified] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_F0115] PRIMARY KEY CLUSTERED 
(
	[WPAN8] ASC,
	[WPIDLN] ASC,
	[WPRCK7] ASC,
	[WPCNLN] ASC
)) ON [PRIMARY]

END


IF OBJECT_ID('[BUSDTA].[F01151]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[F01151](
	[EAAN8] [numeric](8, 0) NOT NULL,
	[EAIDLN] [numeric](5, 0) NOT NULL,
	[EARCK7] [numeric](5, 0) NOT NULL,
	[EAETP] [nchar](4) NULL,
	[EAEMAL] [nvarchar](256) NULL,
	[last_modified] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_F01151] PRIMARY KEY CLUSTERED 
(
	[EAAN8] ASC,
	[EAIDLN] ASC,
	[EARCK7] ASC
)) ON [PRIMARY]

END
/* [BUSDTA].[F41001] - begins */

/* TableDDL - [BUSDTA].[F41001] - Start */
IF OBJECT_ID('[BUSDTA].[F41001]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F41001]
	(
	  [CIPRIM] NCHAR(1) NULL
	, [CISYM1] NCHAR(1) NULL
	, [CISYM2] NCHAR(1) NULL
	, [CISYM3] NCHAR(1) NULL
	, [CISYM5] NCHAR(1) NULL
	, [CISECD] NCHAR(1) NULL
	, [CISEPL] NCHAR(1) NULL
	, [CISE01] FLOAT NULL
	, [CISE02] FLOAT NULL
	, [CISE03] FLOAT NULL
	, [CISE04] FLOAT NULL
	, [CISE05] FLOAT NULL
	, [CISE06] FLOAT NULL
	, [CISE07] FLOAT NULL
	, [CISE08] FLOAT NULL
	, [CISE09] FLOAT NULL
	, [CISE10] FLOAT NULL
	, [CILR01] NCHAR(1) NULL
	, [CILR02] NCHAR(1) NULL
	, [CILR03] NCHAR(1) NULL
	, [CILR04] NCHAR(1) NULL
	, [CILR05] NCHAR(1) NULL
	, [CILR06] NCHAR(1) NULL
	, [CILR07] NCHAR(1) NULL
	, [CILR08] NCHAR(1) NULL
	, [CILR09] NCHAR(1) NULL
	, [CILR10] NCHAR(1) NULL
	, [CIAN8] FLOAT NULL
	, [CIPOCS] FLOAT NULL
	, [CIINCS] FLOAT NULL
	, [CIIALP] FLOAT NULL
	, [CIBSDY] FLOAT NULL
	, [CIIA1] FLOAT NULL
	, [CIIB1] FLOAT NULL
	, [CIIC1] FLOAT NULL
	, [CIIA2] FLOAT NULL
	, [CIIB2] FLOAT NULL
	, [CIIC2] FLOAT NULL
	, [CIIA3] FLOAT NULL
	, [CIIB3] FLOAT NULL
	, [CIIC3] FLOAT NULL
	, [CIPNC] FLOAT NULL
	, [CIGLPO] NCHAR(1) NULL
	, [CIUNGL] NCHAR(1) NULL
	, [CICMGL] NCHAR(1) NULL
	, [CICSMT] NCHAR(2) NULL
	, [CIPCSM] NCHAR(2) NULL
	, [CIBACK] NCHAR(1) NULL
	, [CICOMH] FLOAT NULL
	, [CIICRI] NCHAR(1) NULL
	, [CIIARI] NCHAR(1) NULL
	, [CIMATY] NCHAR(1) NULL
	, [CIXRT] NCHAR(2) NULL
	, [CIXRT2] NCHAR(2) NULL
	, [CIARTG] NCHAR(12) NULL
	, [CIDA01] NCHAR(1) NULL
	, [CIDA02] NCHAR(1) NULL
	, [CIDA03] NCHAR(1) NULL
	, [CIDA04] NCHAR(1) NULL
	, [CIDA05] NCHAR(1) NULL
	, [CIDA06] NCHAR(1) NULL
	, [CIDA07] NCHAR(1) NULL
	, [CIDA08] NCHAR(1) NULL
	, [CIDA09] NCHAR(1) NULL
	, [CIDA10] NCHAR(1) NULL
	, [CIDA11] NCHAR(1) NULL
	, [CIDA12] NCHAR(1) NULL
	, [CIDA13] NCHAR(1) NULL
	, [CIDA14] NCHAR(1) NULL
	, [CIDA15] NCHAR(1) NULL
	, [CIDA16] NCHAR(1) NULL
	, [CISYM4] NCHAR(1) NULL
	, [CIWIUM] NCHAR(2) NULL
	, [CIVUMD] NCHAR(2) NULL
	, [CIUWUM] NCHAR(2) NULL
	, [CIULOT] NCHAR(1) NULL
	, [CILCTL] NCHAR(1) NULL
	, [CIWCTL] NCHAR(1) NULL
	, [CIMVER] NCHAR(3) NULL
	, [CICLOC] NCHAR(20) NULL
	, [CIXLOC] NCHAR(20) NULL
	, [CISTPU] NCHAR(1) NULL
	, [CIDNTP] NCHAR(1) NULL
	, [CITMPS] FLOAT NULL
	, [CIUSMT] NCHAR(1) NULL
	, [CIOT1Y] NCHAR(1) NULL
	, [CIOT2Y] NCHAR(1) NULL
	, [CIOT3Y] NCHAR(1) NULL
	, [CIOT4Y] NCHAR(1) NULL
	, [CIOT5Y] NCHAR(1) NULL
	, [CIUSID] NCHAR(1) NULL
	, [CIUSER] NCHAR(10) NULL
	, [CIPID] NCHAR(10) NULL
	, [CIJOBN] NCHAR(10) NULL
	, [CIUPMJ] NUMERIC(18,0) NULL
	, [CITDAY] FLOAT NULL
	, [CISY] NCHAR(4) NOT NULL
	, [CIMCU] NCHAR(12) NOT NULL
	, [CIOT6Y] NCHAR(1) NULL
	, [CISCTL] NCHAR(1) NULL
	, [CISEPS] NCHAR(1) NULL
	, [CISYM6] NCHAR(1) NULL
	, [CILAF] NCHAR(1) NULL
	, [CILTFM] NCHAR(1) NULL
	, [CIRWLA] NCHAR(1) NULL
	, [CILNPA] NCHAR(1) NULL
	, [CINCOOC] NCHAR(3) NULL
	, [CICSNBF] NCHAR(1) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F41001_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F41001] PRIMARY KEY([CISY]ASC,  [CIMCU] ASC)
	)
END
/* TableDDL - [BUSDTA].[F41001] - End */
/* [BUSDTA].[F4105] - begins */

/* TableDDL - [BUSDTA].[F4105] - Start */
IF OBJECT_ID('[BUSDTA].[F4105]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4105]
	(
	  [COITM] FLOAT NOT NULL
	, [COLITM] NCHAR(25) NULL
	, [COAITM] NCHAR(25) NULL
	, [COMCU] NCHAR(12) NOT NULL
	, [COLOCN] NCHAR(20) NOT NULL
	, [COLOTN] NCHAR(30) NOT NULL
	, [COLOTG] NCHAR(3) NULL
	, [COLEDG] NCHAR(2) NOT NULL
	, [COUNCS] FLOAT NULL
	, [COCSPO] NCHAR(1) NULL
	, [COCSIN] NCHAR(1) NULL
	, [COURCD] NCHAR(2) NULL
	, [COURDT] NUMERIC(18,0) NULL
	, [COURAT] FLOAT NULL
	, [COURAB] FLOAT NULL
	, [COURRF] NCHAR(15) NULL
	, [COUSER] NCHAR(10) NULL
	, [COPID] NCHAR(10) NULL
	, [COJOBN] NCHAR(10) NULL
	, [COUPMJ] NUMERIC(18,0) NULL
	, [COTDAY] FLOAT NULL
	, [COCCFL] NCHAR(1) NULL
	, [COCRCS] FLOAT NULL
	, [COOSTC] FLOAT NULL
	, [COSTOC] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4105_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4105] PRIMARY KEY ([COITM] ASC, [COMCU] ASC, [COLOCN] ASC, [COLOTN] ASC, [COLEDG] ASC)
	)

END


IF OBJECT_ID('[BUSDTA].[F42140]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[F42140](
	[CMAN8] [float] NOT NULL,
	[CMCO] [nchar](5) NOT NULL,
	[CMCMLN] [float] NOT NULL,
	[CMSLSM] [float] NOT NULL,
	[CMSLCM] [float] NULL,
	[CMFCA] [float] NULL,
	[CMAPUN] [float] NULL,
	[CMCCTY] [nchar](1) NULL,
	[CMOWNFLG] [nchar](1) NULL,
	[CMRTYPE] [nvarchar](5) NULL,
	[CMSTMBDT] [datetime] NULL,
	[CMSTMEDT] [datetime] NULL,
	[CMCOMMYN] [nchar](1) NULL,
	[CMUSER] [nchar](10) NULL,
	[CMJOBN] [nchar](10) NULL,
	[CMMKEY] [nchar](15) NULL,
	[CMENTDBY] [float] NULL,
	[CMEDATE] [datetime] NULL,
	[CMUDTTM] [datetime] NULL,
	[CMPID] [nchar](10) NULL,
	[CMVID] [nchar](10) NULL,
	[CMEFFS] [nchar](1) NULL,
	[last_modified] DATETIME NOT NULL CONSTRAINT DF_F42140_last_modified DEFAULT(getdate()),
 CONSTRAINT [PK_F42140] PRIMARY KEY CLUSTERED (	[CMAN8] ASC,[CMCO] ASC,	[CMCMLN] ASC,[CMSLSM] ASC)) ON [PRIMARY]

END

/* [BUSDTA].[M0100] - begins */

/* TableDDL - [BUSDTA].[M0100] - Start */
IF OBJECT_ID('[BUSDTA].[M0100]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0100]
	(
	  [MRID] NCHAR(12) NOT NULL
	, [MRMCU] NCHAR(12) NULL
	, [MRAN8] NUMERIC(8,0) NULL
	, [MRCAN8] NUMERIC(8,0) NULL
	, [MRYN] NCHAR(2) NULL CONSTRAINT DF_M0100_MRYN DEFAULT('Y')
	, [last_modified] DATETIME NULL CONSTRAINT DF_M0100_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M0100] PRIMARY KEY ([MRID] ASC)
	)

ALTER TABLE [BUSDTA].[M0100] WITH CHECK ADD CONSTRAINT [FK_M0100_F0101_MRAN8] FOREIGN KEY([MRAN8]) REFERENCES [BUSDTA].[F0101] ([ABAN8])
ALTER TABLE [BUSDTA].[M0100] CHECK CONSTRAINT [FK_M0100_F0101_MRAN8]

ALTER TABLE [BUSDTA].[M0100] WITH CHECK ADD CONSTRAINT [FK_M0100_F0101_MRCAN8] FOREIGN KEY([MRCAN8]) REFERENCES [BUSDTA].[F0101] ([ABAN8])
ALTER TABLE [BUSDTA].[M0100] CHECK CONSTRAINT [FK_M0100_F0101_MRCAN8]
END
/* [BUSDTA].[M550001] - begins */

/* TableDDL - [BUSDTA].[M550001] - Start */
IF OBJECT_ID('[BUSDTA].[M550001]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M550001]
	(
	  [CSTOP] NCHAR(3) NOT NULL
	, [CPERIOD] NUMERIC(2,0) NOT NULL
	, [CDAY1] NUMERIC(1,0) NULL
	, [CDAY2] NUMERIC(1,0) NULL
	, [CDAY3] NUMERIC(1,0) NULL
	, [CDAY4] NUMERIC(1,0) NULL
	, [CDAY5] NUMERIC(1,0) NULL
	, [CDAY6] NUMERIC(1,0) NULL
	, [CDAY7] NUMERIC(1,0) NULL
	, [CDAY8] NUMERIC(1,0) NULL
	, [CDAY9] NUMERIC(1,0) NULL
	, [CDAY10] NUMERIC(1,0) NULL
	, [CDAY11] NUMERIC(1,0) NULL
	, [CDAY12] NUMERIC(1,0) NULL
	, [CDAY13] NUMERIC(1,0) NULL
	, [CDAY14] NUMERIC(1,0) NULL
	, [CDAY15] NUMERIC(1,0) NULL
	, [CDAY16] NUMERIC(1,0) NULL
	, [CDAY17] NUMERIC(1,0) NULL
	, [CDAY18] NUMERIC(1,0) NULL
	, [CDAY19] NUMERIC(1,0) NULL
	, [CDAY20] NUMERIC(1,0) NULL
	, [CDAY21] NUMERIC(1,0) NULL
	, [CDAY22] NUMERIC(1,0) NULL
	, [CDAY23] NUMERIC(1,0) NULL
	, [CDAY24] NUMERIC(1,0) NULL
	, [CDAY25] NUMERIC(1,0) NULL
	, [CDAY26] NUMERIC(1,0) NULL
	, [CDAY27] NUMERIC(1,0) NULL
	, [CDAY28] NUMERIC(1,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M550001_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M550001] PRIMARY KEY ([CSTOP] ASC, [CPERIOD] ASC)
	)

END
/* TableDDL - [BUSDTA].[M550001] - End */
IF OBJECT_ID('[BUSDTA].[M550002]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[M550002](
	[BYEAR] [numeric](4, 0) NOT NULL,
	[BDSTRT] [numeric](18, 0) NULL,
	[last_modified] [datetime] NOT NULL  DEFAULT (getdate()) ,
 CONSTRAINT [PK_M550002] PRIMARY KEY CLUSTERED 
(
	[BYEAR] ASC
)) ON [PRIMARY]

END

/* [BUSDTA].[M550003] - begins */

/* TableDDL - [BUSDTA].[M550003] - Start */
IF OBJECT_ID('[BUSDTA].[M550003]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M550003]
	(
	  [CDDATE] NUMERIC(18,0) NOT NULL
	, [CDSTOP] NCHAR(3) NOT NULL
	, [CDYEAR] NUMERIC(4,0) NULL
	, [CDYDAY] NUMERIC(4,0) NULL
	, [CDDAYNM] NUMERIC(4,0) NULL
	, [CDSTOPNM] NUMERIC(4,0) NULL
	, [CDORGDATE] NUMERIC(18,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M550003_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M550003] PRIMARY KEY ([CDDATE] ASC, [CDSTOP] ASC)
	)

END
/* TableDDL - [BUSDTA].[M550003] - End */
/* [BUSDTA].[M56M0002B] - begins */

/* TableDDL - [BUSDTA].[M56M0002B] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0002B]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0002B]
	(
	  [DCDDC] NCHAR(3) NOT NULL
	, [DCWN] NUMERIC(4,0) NOT NULL
	, [DCDN] NUMERIC(4,0) NOT NULL
	, [DCISST] NUMERIC(4,0) NULL CONSTRAINT DF_M56M0002B_DCISST DEFAULT((1))
	, [DCCRBY] NCHAR(50) NULL
	, [DCCRDT] DATE NULL
	, [DCUPBY] NCHAR(50) NULL
	, [DCUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M56M0002B_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M56M0002B] PRIMARY KEY ([DCDDC] ASC, [DCWN] ASC, [DCDN] ASC)
	)

END
/* TableDDL - [BUSDTA].[M56M0002B] - End */


/* TableDDL - [BUSDTA].[tblGlobalSync] - Start */
IF OBJECT_ID('[BUSDTA].[tblGlobalSync]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblGlobalSync]
	(
	  [GlobalSyncID] INT NOT NULL IDENTITY(1,1)
	, [VersionNum] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLGLOBALSYNC_CreatedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLGLOBALSYNC_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGlobalSync] PRIMARY KEY ([GlobalSyncID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblGlobalSync] - End */


/* TableDDL - [BUSDTA].[tblGlobalSyncLog] - Start */
IF OBJECT_ID('[BUSDTA].[tblGlobalSyncLog]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblGlobalSyncLog]
	(
	  [GlobalSyncLogID] INT NOT NULL IDENTITY(1,1)
	, [GlobalSyncID] INT NULL
	, [DeviceID] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLGLOBALSYNCLOG_CreatedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLGLOBALSYNCLOG_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGlobalSyncLog] PRIMARY KEY ([GlobalSyncLogID] ASC)
	)

ALTER TABLE [BUSDTA].[tblGlobalSyncLog] WITH CHECK ADD CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync] FOREIGN KEY([GlobalSyncID]) REFERENCES [BUSDTA].[tblGlobalSync] ([GlobalSyncID])
ALTER TABLE [BUSDTA].[tblGlobalSyncLog] CHECK CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync]

END
/* TableDDL - [BUSDTA].[tblGlobalSyncLog] - End */


/* TableDDL - [BUSDTA].[tblApplicationGroup] - Start */
IF OBJECT_ID('[BUSDTA].[tblApplicationGroup]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblApplicationGroup]
	(
	  [ApplicationGroupID] INT NOT NULL IDENTITY(1,1)
	, [ApplicationGroup] VARCHAR(128) NULL
	, [SortOrder] INT NULL
	, [IsMandatorySync] BIT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLAPPLICATIONGROUP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblApplicationGroup] PRIMARY KEY ([ApplicationGroupID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblApplicationGroup] - End */
GO




/* TableDDL - [BUSDTA].[tblGroupTables] - Start */
IF OBJECT_ID('[BUSDTA].[tblGroupTables]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblGroupTables]
	(
	  [GroupTableID] INT NOT NULL IDENTITY(1,1)
	, [RefDBSchema] VARCHAR(128) NULL
	, [RefDBTable] VARCHAR(128) NULL
	, [ApplicationGroupID] INT NULL
	, [ConDBTable] VARCHAR(128) NULL
	, [IsMandatorySync] BIT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLGROUPTABLES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGroupTables] PRIMARY KEY ([GroupTableID] ASC)
	)

ALTER TABLE [BUSDTA].[tblGroupTables] WITH CHECK ADD CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID] FOREIGN KEY([ApplicationGroupID]) REFERENCES [BUSDTA].[tblApplicationGroup] ([ApplicationGroupID])
ALTER TABLE [BUSDTA].[tblGroupTables] CHECK CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID]

END
/* TableDDL - [BUSDTA].[tblGroupTables] - End */

/* TableDDL - [BUSDTA].[tblStatsGroups] - Start */
IF OBJECT_ID('[BUSDTA].[tblStatsGroups]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblStatsGroups]
	(
	  [StatsGroupID] INT NOT NULL IDENTITY(1,1)
	, [GroupName] VARCHAR(128) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLSTATSGROUPS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblStatsGroups] PRIMARY KEY ([StatsGroupID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblStatsGroups] - End */


/* TableDDL - [BUSDTA].[tblSyncArticles] - Start */
IF OBJECT_ID('[BUSDTA].[tblSyncArticles]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblSyncArticles]
	(
	  [SyncArticleID] INT NOT NULL IDENTITY(1,1)
	, [PublicationName] VARCHAR(100) NULL
	, [SchemaName] VARCHAR(128) NULL
	, [TableName] VARCHAR(128) NULL
	, [ActiveYN] BIT NULL CONSTRAINT DF_TBLSYNCARTICLES_ActiveYN DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_EditedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLSYNCARTICLES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblSyncArticles] PRIMARY KEY ([SyncArticleID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblSyncArticles] - End */

/* *** [BUSDTA].[tblSyncDetails] - Start ***/
IF OBJECT_ID('[BUSDTA].[tblSyncDetails]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[tblSyncDetails]
(
	[ObjectID] [int] NOT NULL IDENTITY(1, 1),
	[Objectname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastSyncVersion] [bigint] NULL,
	[TruncTimeAtCDB] [datetime] NULL,
	[UpdatedDateTime] [datetime] NULL DEFAULT (getdate())
)
END
/* TableDDL - [BUSDTA].[tblSyncDetails] - End */
GO
IF OBJECT_ID('BUSDTA.tblsyncdetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER [BUSDTA].[tblsyncdetails_upd]
	ON [BUSDTA].[tblSyncDetails] AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblsyncdetails
	SET UpdatedDateTime = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblsyncdetails.ObjectID= inserted.ObjectID)';
END
GO
/* [BUSDTA].[tblTransactionAudit] - begins */

/* TableDDL - [BUSDTA].[tblTransactionAudit] - Start */
IF OBJECT_ID('[BUSDTA].[tblTransactionAudit]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblTransactionAudit]
	(
	  [AuditID] UNIQUEIDENTIFIER NOT NULL
	, [Action] NVARCHAR(50) NULL
	, [SchemaName] NVARCHAR(255) NOT NULL
	, [TableName] VARCHAR(200) NULL
	, [FieldName] VARCHAR(200) NULL
	, [OldValue] NVARCHAR(4000) NULL
	, [NewValue] NVARCHAR(4000) NULL
	, [IsPkey] BIT NULL
	, [TransactionID] UNIQUEIDENTIFIER NULL
	, [CreatedOn] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONAUDIT_CreatedOn DEFAULT(getdate())
	, [Createdby] VARCHAR(50) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONAUDIT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblTransactionAudit] PRIMARY KEY ([AuditID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblTransactionAudit] - End */
/* [BUSDTA].[tblTransactionControl] - begins */

/* TableDDL - [BUSDTA].[tblTransactionControl] - Start */
IF OBJECT_ID('[BUSDTA].[tblTransactionControl]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblTransactionControl]
	(
	  [TransactionID] UNIQUEIDENTIFIER NOT NULL
	, [PageName] VARCHAR(200) NULL
	, [NoofTxns] BIGINT NULL
	, [RouteID] VARCHAR(50) NOT NULL
	, [DeviceID] VARCHAR(50) NULL
	, [IsValidated] INT NULL
	, [CreatedOn] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONCONTROL_CreatedOn DEFAULT(getdate())
	, [Createdby] VARCHAR(50) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONCONTROL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblTransactionControl] PRIMARY KEY ([TransactionID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblTransactionControl] - End */
/* [Busdta].[tblAlliedProspCategory] - begins */

/* TableDDL - [Busdta].[tblAlliedProspCategory] - Start */
IF OBJECT_ID('[Busdta].[tblAlliedProspCategory]') IS NULL
BEGIN

	CREATE TABLE [Busdta].[tblAlliedProspCategory]
	(
	  [CategoryID] INT NOT NULL IDENTITY(1,1)
	, [Category] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblAlliedProspCategory] PRIMARY KEY ([CategoryID] ASC)
	)

END
GO
/* TableDDL - [Busdta].[tblAlliedProspCategory] - End */

/* SHADOW TABLE FOR [Busdta].[tblAlliedProspCategory] - Start */
IF OBJECT_ID('[Busdta].[tblAlliedProspCategory_del]') IS NULL
BEGIN

	CREATE TABLE [Busdta].[tblAlliedProspCategory_del]
	(
	  [CategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [Busdta].[tblAlliedProspCategory] - End */
/* TRIGGERS FOR tblAlliedProspCategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('Busdta.tblAlliedProspCategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER Busdta.tblAlliedProspCategory_ins
	ON Busdta.tblAlliedProspCategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM Busdta.tblAlliedProspCategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE Busdta.tblAlliedProspCategory_del.CategoryID= inserted.CategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('Busdta.tblAlliedProspCategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER Busdta.tblAlliedProspCategory_upd
	ON Busdta.tblAlliedProspCategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE Busdta.tblAlliedProspCategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE Busdta.tblAlliedProspCategory.CategoryID= inserted.CategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('Busdta.tblAlliedProspCategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER Busdta.tblAlliedProspCategory_dlt
	ON Busdta.tblAlliedProspCategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO Busdta.tblAlliedProspCategory_del (CategoryID, last_modified )
	SELECT deleted.CategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblAlliedProspCategory - END */
/* [BUSDTA].[tblAlliedProspect] - begins */

/* TableDDL - [BUSDTA].[tblAlliedProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspect]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspect]
	(
	  [AlliedProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, [CompetitorID] INT NULL
	, [CategoryID] INT NULL
	, [SubCategoryID] INT NULL
	, [BrandID] INT NULL
	, [UOMID] INT NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] NUMERIC(8,0) NULL
	, [UsageMeasurementID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblAlliedProspect] PRIMARY KEY ([AlliedProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblAlliedProspect] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspect_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspect_del]
	(
	  [AlliedProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([AlliedProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspect] - End */
/* TRIGGERS FOR tblAlliedProspect - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspect_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblAlliedProspect_ins
	ON BUSDTA.tblAlliedProspect AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblAlliedProspect_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblAlliedProspect_del.AlliedProspectID= inserted.AlliedProspectID AND BUSDTA.tblAlliedProspect_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblAlliedProspect_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspect_upd
	ON BUSDTA.tblAlliedProspect AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblAlliedProspect
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblAlliedProspect.AlliedProspectID= inserted.AlliedProspectID AND BUSDTA.tblAlliedProspect.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspect_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspect_dlt
	ON BUSDTA.tblAlliedProspect AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblAlliedProspect_del (AlliedProspectID,ProspectID, last_modified )
	SELECT deleted.AlliedProspectID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblAlliedProspect - END */
/* [BUSDTA].[tblAlliedProspSubcategory] - begins */

/* TableDDL - [BUSDTA].[tblAlliedProspSubcategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspSubcategory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspSubcategory]
	(
	  [SubCategoryID] INT NOT NULL IDENTITY(1,1)
	, [SubCategory] VARCHAR(500) NULL
	, [CategoryID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPSUBCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPSUBCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPSUBCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblAlliedProspSubcategory] PRIMARY KEY ([SubCategoryID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblAlliedProspSubcategory] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspSubcategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspSubcategory_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspSubcategory_del]
	(
	  [SubCategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SubCategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspSubcategory] - End */
/* TRIGGERS FOR tblAlliedProspSubcategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspSubcategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblAlliedProspSubcategory_ins
	ON BUSDTA.tblAlliedProspSubcategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblAlliedProspSubcategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblAlliedProspSubcategory_del.SubCategoryID= inserted.SubCategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblAlliedProspSubcategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspSubcategory_upd
	ON BUSDTA.tblAlliedProspSubcategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblAlliedProspSubcategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblAlliedProspSubcategory.SubCategoryID= inserted.SubCategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspSubcategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspSubcategory_dlt
	ON BUSDTA.tblAlliedProspSubcategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblAlliedProspSubcategory_del (SubCategoryID, last_modified )
	SELECT deleted.SubCategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblAlliedProspSubcategory - END */
/* [BUSDTA].[tblBrandLable] - begins */

/* TableDDL - [BUSDTA].[tblBrandLable] - Start */
IF OBJECT_ID('[BUSDTA].[tblBrandLable]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblBrandLable]
	(
	  [BrandID] INT NOT NULL IDENTITY(1,1)
	, [BrandLable] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLBRANDLABLE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLBRANDLABLE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLBRANDLABLE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblBrandLable] PRIMARY KEY ([BrandID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblBrandLable] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblBrandLable] - Start */
IF OBJECT_ID('[BUSDTA].[tblBrandLable_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblBrandLable_del]
	(
	  [BrandID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([BrandID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblBrandLable] - End */
/* TRIGGERS FOR tblBrandLable - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblBrandLable_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblBrandLable_ins
	ON BUSDTA.tblBrandLable AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblBrandLable_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblBrandLable_del.BrandID= inserted.BrandID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblBrandLable_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblBrandLable_upd
	ON BUSDTA.tblBrandLable AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblBrandLable
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblBrandLable.BrandID= inserted.BrandID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblBrandLable_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblBrandLable_dlt
	ON BUSDTA.tblBrandLable AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblBrandLable_del (BrandID, last_modified )
	SELECT deleted.BrandID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblBrandLable - END */
/* [BUSDTA].[tblCoffeeBlends] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeBlends]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeBlends]
	(
	  [CoffeeBlendID] INT NOT NULL IDENTITY(1,1)
	, [CoffeeBlend] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeBlends] PRIMARY KEY ([CoffeeBlendID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblCoffeeBlends] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeBlends_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeBlends_del]
	(
	  [CoffeeBlendID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeBlendID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeBlends] - End */
/* TRIGGERS FOR tblCoffeeBlends - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeBlends_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCoffeeBlends_ins
	ON BUSDTA.tblCoffeeBlends AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCoffeeBlends_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCoffeeBlends_del.CoffeeBlendID= inserted.CoffeeBlendID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCoffeeBlends_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeBlends_upd
	ON BUSDTA.tblCoffeeBlends AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCoffeeBlends
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCoffeeBlends.CoffeeBlendID= inserted.CoffeeBlendID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeBlends_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeBlends_dlt
	ON BUSDTA.tblCoffeeBlends AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCoffeeBlends_del (CoffeeBlendID, last_modified )
	SELECT deleted.CoffeeBlendID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeBlends - END */
/* [BUSDTA].[tblCoffeeProspect] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeProspect]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeProspect]
	(
	  [CoffeeProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, [CoffeeBlendID] INT NULL
	, [CompetitorID] INT NULL
	, [UOMID] INT NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] NUMERIC(8,0) NULL
	, [UsageMeasurementID] INT NULL
	, [LiqCoffeeTypeID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEPROSPECT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeProspect] PRIMARY KEY ([CoffeeProspectID] ASC,[ProspectID] ASC)
	)



END
GO
/* TableDDL - [BUSDTA].[tblCoffeeProspect] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeProspect_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeProspect_del]
	(
	  [CoffeeProspectID] INT NOT NULL
	,[ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeProspect] - End */
/* TRIGGERS FOR tblCoffeeProspect - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeProspect_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCoffeeProspect_ins
	ON BUSDTA.tblCoffeeProspect AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCoffeeProspect_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCoffeeProspect_del.CoffeeProspectID= inserted.CoffeeProspectID AND  BUSDTA.tblCoffeeProspect_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCoffeeProspect_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeProspect_upd
	ON BUSDTA.tblCoffeeProspect AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCoffeeProspect
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCoffeeProspect.CoffeeProspectID= inserted.CoffeeProspectID AND BUSDTA.tblCoffeeProspect.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeProspect_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeProspect_dlt
	ON BUSDTA.tblCoffeeProspect AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCoffeeProspect_del (CoffeeProspectID,ProspectID, last_modified )
	SELECT deleted.CoffeeProspectID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeProspect - END */
/* [BUSDTA].[tblCoffeeVolume] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeVolume] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeVolume]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeVolume]
	(
	  [CoffeeVolumeID] INT NOT NULL IDENTITY(1,1)
	, [Volume] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEVOLUME_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEVOLUME_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEVOLUME_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeVolume] PRIMARY KEY ([CoffeeVolumeID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblCoffeeVolume] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeVolume] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeVolume_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeVolume_del]
	(
	  [CoffeeVolumeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeVolumeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeVolume] - End */
/* TRIGGERS FOR tblCoffeeVolume - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeVolume_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCoffeeVolume_ins
	ON BUSDTA.tblCoffeeVolume AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCoffeeVolume_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCoffeeVolume_del.CoffeeVolumeID= inserted.CoffeeVolumeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCoffeeVolume_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeVolume_upd
	ON BUSDTA.tblCoffeeVolume AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCoffeeVolume
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCoffeeVolume.CoffeeVolumeID= inserted.CoffeeVolumeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeVolume_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeVolume_dlt
	ON BUSDTA.tblCoffeeVolume AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCoffeeVolume_del (CoffeeVolumeID, last_modified )
	SELECT deleted.CoffeeVolumeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeVolume - END */
/* [BUSDTA].[tblCompetitor] - begins */

/* TableDDL - [BUSDTA].[tblCompetitor] - Start */
IF OBJECT_ID('[BUSDTA].[tblCompetitor]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCompetitor]
	(
	  [CompetitorID] INT NOT NULL IDENTITY(1,1)
	, [Competitor] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOMPETITOR_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOMPETITOR_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOMPETITOR_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCompetitor] PRIMARY KEY ([CompetitorID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblCompetitor] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCompetitor] - Start */
IF OBJECT_ID('[BUSDTA].[tblCompetitor_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCompetitor_del]
	(
	  [CompetitorID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CompetitorID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCompetitor] - End */
/* TRIGGERS FOR tblCompetitor - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCompetitor_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCompetitor_ins
	ON BUSDTA.tblCompetitor AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCompetitor_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCompetitor_del.CompetitorID= inserted.CompetitorID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCompetitor_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCompetitor_upd
	ON BUSDTA.tblCompetitor AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCompetitor
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCompetitor.CompetitorID= inserted.CompetitorID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCompetitor_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCompetitor_dlt
	ON BUSDTA.tblCompetitor AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCompetitor_del (CompetitorID, last_modified )
	SELECT deleted.CompetitorID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCompetitor - END */
/* [BUSDTA].[tblCSPKLB] - begins */

/* TableDDL - [BUSDTA].[tblCSPKLB] - Start */
IF OBJECT_ID('[BUSDTA].[tblCSPKLB]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCSPKLB]
	(
	  [CSPKLBID] INT NOT NULL IDENTITY(1,1)
	, [CSPKLBType] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCSPKLB_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCSPKLB_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCSPKLB_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCSPKLB] PRIMARY KEY ([CSPKLBID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblCSPKLB] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCSPKLB] - Start */
IF OBJECT_ID('[BUSDTA].[tblCSPKLB_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCSPKLB_del]
	(
	  [CSPKLBID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CSPKLBID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCSPKLB] - End */
/* TRIGGERS FOR tblCSPKLB - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCSPKLB_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCSPKLB_ins
	ON BUSDTA.tblCSPKLB AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCSPKLB_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCSPKLB_del.CSPKLBID= inserted.CSPKLBID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCSPKLB_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCSPKLB_upd
	ON BUSDTA.tblCSPKLB AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCSPKLB
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCSPKLB.CSPKLBID= inserted.CSPKLBID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCSPKLB_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCSPKLB_dlt
	ON BUSDTA.tblCSPKLB AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCSPKLB_del (CSPKLBID, last_modified )
	SELECT deleted.CSPKLBID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCSPKLB - END */
/* [BUSDTA].[tblEquipment] - begins */

/* TableDDL - [BUSDTA].[tblEquipment] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipment]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipment]
	(
	  [EquipmentID] INT NOT NULL
	, [ProspectID] NUMERIC(10,0) NOT NULL
	, [EquipmentType] VARCHAR(200) NULL
	, [EquipmentCategoryID] INT NULL
	, [EquipmentSubCategory] VARCHAR(MAX) NULL
	, [EquipmentQuantity] INT NULL
	, [EquipmentOwned] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENT_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENT_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipment] PRIMARY KEY ([EquipmentID] ASC,[ProspectID] ASC)
	)


END
GO
/* TableDDL - [BUSDTA].[tblEquipment] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblEquipment] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipment_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipment_del]
	(
	  [EquipmentID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblEquipment] - End */
/* TRIGGERS FOR tblEquipment - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblEquipment_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblEquipment_ins
	ON BUSDTA.tblEquipment AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblEquipment_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblEquipment_del.EquipmentID= inserted.EquipmentID AND BUSDTA.tblEquipment_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblEquipment_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipment_upd
	ON BUSDTA.tblEquipment AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblEquipment
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblEquipment.EquipmentID= inserted.EquipmentID AND BUSDTA.tblEquipment.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblEquipment_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipment_dlt
	ON BUSDTA.tblEquipment AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblEquipment_del (EquipmentID,ProspectID, last_modified )
	SELECT deleted.EquipmentID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipment - END */
/* [BUSDTA].[tblEquipmentCategory] - begins */

/* TableDDL - [BUSDTA].[tblEquipmentCategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentCategory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentCategory]
	(
	  [EquipmentCategoryID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentCategory] VARCHAR(500) NULL
	, [EquipmentTypeID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentCategory] PRIMARY KEY ([EquipmentCategoryID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblEquipmentCategory] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentCategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentCategory_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentCategory_del]
	(
	  [EquipmentCategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentCategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentCategory] - End */
/* TRIGGERS FOR tblEquipmentCategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentCategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblEquipmentCategory_ins
	ON BUSDTA.tblEquipmentCategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblEquipmentCategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblEquipmentCategory_del.EquipmentCategoryID= inserted.EquipmentCategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblEquipmentCategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentCategory_upd
	ON BUSDTA.tblEquipmentCategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblEquipmentCategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblEquipmentCategory.EquipmentCategoryID= inserted.EquipmentCategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentCategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentCategory_dlt
	ON BUSDTA.tblEquipmentCategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblEquipmentCategory_del (EquipmentCategoryID, last_modified )
	SELECT deleted.EquipmentCategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentCategory - END */
/* [BUSDTA].[tblEquipmentSubcategory] - begins */

/* TableDDL - [BUSDTA].[tblEquipmentSubcategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentSubcategory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentSubcategory]
	(
	  [EquipmentSubcategoryID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentCategoryID] INT NULL
	, [EquipmentSubCategory] VARCHAR(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTSUBCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTSUBCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTSUBCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentSubcategory] PRIMARY KEY ([EquipmentSubcategoryID] ASC)
	)


END
GO
/* TableDDL - [BUSDTA].[tblEquipmentSubcategory] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentSubcategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentSubcategory_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentSubcategory_del]
	(
	  [EquipmentSubcategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentSubcategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentSubcategory] - End */
/* TRIGGERS FOR tblEquipmentSubcategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentSubcategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblEquipmentSubcategory_ins
	ON BUSDTA.tblEquipmentSubcategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblEquipmentSubcategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblEquipmentSubcategory_del.EquipmentSubcategoryID= inserted.EquipmentSubcategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblEquipmentSubcategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentSubcategory_upd
	ON BUSDTA.tblEquipmentSubcategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblEquipmentSubcategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblEquipmentSubcategory.EquipmentSubcategoryID= inserted.EquipmentSubcategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentSubcategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentSubcategory_dlt
	ON BUSDTA.tblEquipmentSubcategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblEquipmentSubcategory_del (EquipmentSubcategoryID, last_modified )
	SELECT deleted.EquipmentSubcategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentSubcategory - END */
/* [BUSDTA].[tblEquipmentType] - begins */

/* TableDDL - [BUSDTA].[tblEquipmentType] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentType]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentType]
	(
	  [EquipmentTypeID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentType] VARCHAR(200) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentType] PRIMARY KEY ([EquipmentTypeID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblEquipmentType] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentType] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentType_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentType_del]
	(
	  [EquipmentTypeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentTypeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentType] - End */
/* TRIGGERS FOR tblEquipmentType - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentType_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblEquipmentType_ins
	ON BUSDTA.tblEquipmentType AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblEquipmentType_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblEquipmentType_del.EquipmentTypeID= inserted.EquipmentTypeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblEquipmentType_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentType_upd
	ON BUSDTA.tblEquipmentType AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblEquipmentType
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblEquipmentType.EquipmentTypeID= inserted.EquipmentTypeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentType_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentType_dlt
	ON BUSDTA.tblEquipmentType AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblEquipmentType_del (EquipmentTypeID, last_modified )
	SELECT deleted.EquipmentTypeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentType - END */
/* [BUSDTA].[tblUsageMeasurement] - begins */

/* TableDDL - [BUSDTA].[tblUsageMeasurement] - Start */
IF OBJECT_ID('[BUSDTA].[tblUsageMeasurement]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblUsageMeasurement]
	(
	  [UsageMeasurementID] INT NOT NULL IDENTITY(1,1)
	, [UsageMeasurement] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUSAGEMEASUREMENT_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUSAGEMEASUREMENT_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLUSAGEMEASUREMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblUsageMeasurement] PRIMARY KEY ([UsageMeasurementID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblUsageMeasurement] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[tblUsageMeasurement] - Start */
IF OBJECT_ID('[BUSDTA].[tblUsageMeasurement_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblUsageMeasurement_del]
	(
	  [UsageMeasurementID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UsageMeasurementID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblUsageMeasurement] - End */
/* TRIGGERS FOR tblUsageMeasurement - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblUsageMeasurement_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblUsageMeasurement_ins
	ON BUSDTA.tblUsageMeasurement AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblUsageMeasurement_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblUsageMeasurement_del.UsageMeasurementID= inserted.UsageMeasurementID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblUsageMeasurement_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblUsageMeasurement_upd
	ON BUSDTA.tblUsageMeasurement AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblUsageMeasurement
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblUsageMeasurement.UsageMeasurementID= inserted.UsageMeasurementID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblUsageMeasurement_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblUsageMeasurement_dlt
	ON BUSDTA.tblUsageMeasurement AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblUsageMeasurement_del (UsageMeasurementID, last_modified )
	SELECT deleted.UsageMeasurementID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblUsageMeasurement - END */
/* [BUSDTA].[tblUOM] - begins */

/* TableDDL - [BUSDTA].[tblUOM] - Start */
IF OBJECT_ID('[BUSDTA].[tblUOM]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblUOM]
	(
	  [UOMID] INT NOT NULL IDENTITY(1,1)
	, [UOMLable] VARCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUOM_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUOM_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLUOM_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblUOM] PRIMARY KEY ([UOMID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblUOM] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblUOM] - Start */
IF OBJECT_ID('[BUSDTA].[tblUOM_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblUOM_del]
	(
	  [UOMID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UOMID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblUOM] - End */
/* TRIGGERS FOR tblUOM - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblUOM_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblUOM_ins
	ON BUSDTA.tblUOM AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblUOM_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblUOM_del.UOMID= inserted.UOMID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblUOM_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblUOM_upd
	ON BUSDTA.tblUOM AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblUOM
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblUOM.UOMID= inserted.UOMID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblUOM_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblUOM_dlt
	ON BUSDTA.tblUOM AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblUOM_del (UOMID, last_modified )
	SELECT deleted.UOMID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblUOM - END */
/* [BUSDTA].[tblLiqCoffeeType] - begins */

/* TableDDL - [BUSDTA].[tblLiqCoffeeType] - Start */
IF OBJECT_ID('[BUSDTA].[tblLiqCoffeeType]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblLiqCoffeeType]
	(
	  [LiqCoffeeTypeID] INT NOT NULL IDENTITY(1,1)
	, [LiqCoffeeType] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLLIQCOFFEETYPE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLLIQCOFFEETYPE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLLIQCOFFEETYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblLiqCoffeeType] PRIMARY KEY ([LiqCoffeeTypeID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblLiqCoffeeType] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblLiqCoffeeType] - Start */
IF OBJECT_ID('[BUSDTA].[tblLiqCoffeeType_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblLiqCoffeeType_del]
	(
	  [LiqCoffeeTypeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([LiqCoffeeTypeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblLiqCoffeeType] - End */
/* TRIGGERS FOR tblLiqCoffeeType - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblLiqCoffeeType_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblLiqCoffeeType_ins
	ON BUSDTA.tblLiqCoffeeType AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblLiqCoffeeType_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblLiqCoffeeType_del.LiqCoffeeTypeID= inserted.LiqCoffeeTypeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblLiqCoffeeType_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblLiqCoffeeType_upd
	ON BUSDTA.tblLiqCoffeeType AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblLiqCoffeeType
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblLiqCoffeeType.LiqCoffeeTypeID= inserted.LiqCoffeeTypeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblLiqCoffeeType_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblLiqCoffeeType_dlt
	ON BUSDTA.tblLiqCoffeeType AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblLiqCoffeeType_del (LiqCoffeeTypeID, last_modified )
	SELECT deleted.LiqCoffeeTypeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblLiqCoffeeType - END */

/* [BUSDTA].[tblPacketSize] - begins */

/* TableDDL - [BUSDTA].[tblPacketSize] - Start */
IF OBJECT_ID('[BUSDTA].[tblPacketSize]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblPacketSize]
	(
	  [PacketSizeID] INT NOT NULL IDENTITY(1,1)
	, [Size] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblPacketSize] PRIMARY KEY ([PacketSizeID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblPacketSize] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[tblPacketSize] - Start */
IF OBJECT_ID('[BUSDTA].[tblPacketSize_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblPacketSize_del]
	(
	  [PacketSizeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PacketSizeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblPacketSize] - End */
/* TRIGGERS FOR tblPacketSize - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblPacketSize_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblPacketSize_ins
	ON BUSDTA.tblPacketSize AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblPacketSize_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblPacketSize_del.PacketSizeID= inserted.PacketSizeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblPacketSize_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblPacketSize_upd
	ON BUSDTA.tblPacketSize AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblPacketSize
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblPacketSize.PacketSizeID= inserted.PacketSizeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblPacketSize_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblPacketSize_dlt
	ON BUSDTA.tblPacketSize AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblPacketSize_del (PacketSizeID, last_modified )
	SELECT deleted.PacketSizeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblPacketSize - END */
/* [BUSDTA].[F40205] - begins */

/* TableDDL - [BUSDTA].[F40205] - Start */
IF OBJECT_ID('[BUSDTA].[F40205]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40205]
	(
	  [LFLNTY] NVARCHAR(2) NOT NULL
	, [LFLNDS] NVARCHAR(30) NULL
	, [LFGLI] NVARCHAR(1) NULL
	, [LFIVI] NVARCHAR(1) NULL
	, [LFARI] NVARCHAR(1) NULL
	, [LFAPI] NVARCHAR(1) NULL
	, [LFRSGN] NVARCHAR(1) NULL
	, [LFTXYN] NVARCHAR(1) NULL
	, [LFPRFT] NVARCHAR(1) NULL
	, [LFCDSC] NVARCHAR(1) NULL
	, [LFTX01] NVARCHAR(1) NULL
	, [LFTX02] NVARCHAR(1) NULL
	, [LFGLC] NVARCHAR(4) NULL
	, [LFPDC1] NVARCHAR(1) NULL
	, [LFPDC2] NVARCHAR(1) NULL
	, [LFPDC3] NVARCHAR(1) NULL
	, [LFCSJ] NVARCHAR(1) NULL
	, [LFDCTO] NVARCHAR(2) NULL
	, [LFART] NVARCHAR(1) NULL
	, [LFAFT] NVARCHAR(1) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_F40205_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F40205] PRIMARY KEY ([LFLNTY] ASC)
	)

END
/* TableDDL - [BUSDTA].[F40205] - End */

/* SHADOW TABLE FOR [BUSDTA].[F40205] - Start */
IF OBJECT_ID('[BUSDTA].[F40205_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40205_del]
	(
	  [LFLNTY] NVARCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([LFLNTY] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[F40205] - End */
/* TRIGGERS FOR F40205 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F40205_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F40205_ins
	ON BUSDTA.F40205 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F40205_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F40205_del.LFLNTY= inserted.LFLNTY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F40205_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40205_upd
	ON BUSDTA.F40205 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F40205
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F40205.LFLNTY= inserted.LFLNTY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F40205_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40205_dlt
	ON BUSDTA.F40205 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F40205_del (LFLNTY, last_modified )
	SELECT deleted.LFLNTY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F40205 - END */

/* [BUSDTA].[Order_PriceAdj] - begins */

/* TableDDL - [BUSDTA].[Order_PriceAdj] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_PriceAdj]
	(
	  [OrderID] NUMERIC(8,0) NOT NULL
	, [LineID] NUMERIC(7,4) NOT NULL
	, [AdjustmentSeq] NUMERIC(4,0) NOT NULL
	, [AdjName] NVARCHAR(20) NULL
	, [AdjDesc] NVARCHAR(60) NULL
	, [FactorValue] NUMERIC(12,4) NULL
	, [UnitPrice] NUMERIC(12,4) NULL
	, [OP] NVARCHAR(2) NULL
	, [BC] NVARCHAR(2) NULL
	, [BasicDesc] NVARCHAR(60) NULL
	, [PriceAdjustmentKeyID] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, CONSTRAINT [PK_Order_PriceAdj] PRIMARY KEY ([OrderID] ASC, [LineID] ASC, [AdjustmentSeq] ASC)
	)END
/* TableDDL - [BUSDTA].[Order_PriceAdj] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_PriceAdj] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_PriceAdj_del]
	(
	  [OrderID] NUMERIC(8,0)
	, [LineID] NUMERIC(7,4)
	, [AdjustmentSeq] NUMERIC(4,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([OrderID] ASC, [LineID] ASC, [AdjustmentSeq] ASC)
	)END
/* SHADOW TABLE FOR [BUSDTA].[Order_PriceAdj] - End */
/* TRIGGERS FOR Order_PriceAdj - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_PriceAdj_ins
	ON BUSDTA.Order_PriceAdj AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_PriceAdj_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_PriceAdj_del.AdjustmentSeq= inserted.AdjustmentSeq AND BUSDTA.Order_PriceAdj_del.LineID= inserted.LineID AND BUSDTA.Order_PriceAdj_del.OrderID= inserted.OrderID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_PriceAdj_upd
	ON BUSDTA.Order_PriceAdj AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_PriceAdj
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_PriceAdj.AdjustmentSeq= inserted.AdjustmentSeq AND BUSDTA.Order_PriceAdj.LineID= inserted.LineID AND BUSDTA.Order_PriceAdj.OrderID= inserted.OrderID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_PriceAdj_dlt
	ON BUSDTA.Order_PriceAdj AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_PriceAdj_del (AdjustmentSeq, LineID, OrderID, last_modified )
	SELECT deleted.AdjustmentSeq, deleted.LineID, deleted.OrderID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_PriceAdj - END */
/* [BUSDTA].[CustomerConfig] - begins */

/* TableDDL - [BUSDTA].[CustomerConfig] - Start */
IF OBJECT_ID('[BUSDTA].[CustomerConfig]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[CustomerConfig]
	(
	  [CustomerID] NUMERIC(8,0) NOT NULL
	, [CustomerReference1] NVARCHAR(25) NULL
	, [CustomerReference2] NVARCHAR(25) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, CONSTRAINT [PK_CustomerConfig] PRIMARY KEY ([CustomerID] ASC)
	)END
/* TableDDL - [BUSDTA].[CustomerConfig] - End */

/* SHADOW TABLE FOR [BUSDTA].[CustomerConfig] - Start */
IF OBJECT_ID('[BUSDTA].[CustomerConfig_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[CustomerConfig_del]
	(
	  [CustomerID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerID] ASC)
	)END
/* SHADOW TABLE FOR [BUSDTA].[CustomerConfig] - End */
/* TRIGGERS FOR CustomerConfig - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.CustomerConfig_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.CustomerConfig_ins
	ON BUSDTA.CustomerConfig AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.CustomerConfig_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.CustomerConfig_del.CustomerID= inserted.CustomerID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.CustomerConfig_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CustomerConfig_upd
	ON BUSDTA.CustomerConfig AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.CustomerConfig
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.CustomerConfig.CustomerID= inserted.CustomerID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.CustomerConfig_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CustomerConfig_dlt
	ON BUSDTA.CustomerConfig AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.CustomerConfig_del (CustomerID, last_modified )
	SELECT deleted.CustomerID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR CustomerConfig - END */

/* [BUSDTA].[Order_Header_BETA] - begins */

/* TableDDL - [BUSDTA].[Order_Header_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_BETA]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_Header_BETA]
	(
	  [OrderID] NUMERIC(8,0) NOT NULL
	, [OrderTypeId] NUMERIC(3,0) NULL
	, [OriginatingRouteID] NUMERIC(8,0) NOT NULL
	, [Branch] NVARCHAR(12) NULL
	, [CustShipToId] NUMERIC(8,0) NOT NULL
	, [CustBillToId] NUMERIC(8,0) NOT NULL
	, [CustParentId] NUMERIC(8,0) NOT NULL
	, [OrderDate] DATE NULL
	, [TotalCoffeeAmt] NUMERIC(12,4) NULL
	, [TotalAlliedAmt] NUMERIC(12,4) NULL
	, [OrderTotalAmt] NUMERIC(12,4) NULL
	, [SalesTaxAmt] NUMERIC(12,4) NULL
	, [InvoiceTotalAmt] NUMERIC(12,4) NULL
	, [EnergySurchargeAmt] NUMERIC(12,4) NULL
	, [VarianceAmt] NUMERIC(12,4) NULL
	, [SurchargeReasonCodeId] NUMERIC(3,0) NULL
	, [OrderStateId] NUMERIC(3,0) NULL
	, [OrderSignature] VARBINARY(MAX) NULL
	, [ChargeOnAccountSignature] VARBINARY(MAX) NULL
	, [JDEOrderCompany] NVARCHAR(5) NULL
	, [JDEOrderNumber] NUMERIC(8,0) NULL
	, [JDEOrderType] NVARCHAR(2) NULL
	, [JDEInvoiceCompany] NVARCHAR(5) NULL
	, [JDEInvoiceNumber] NUMERIC(8,0) NULL
	, [JDEOInvoiceType] NVARCHAR(2) NULL
	, [JDEZBatchNumber] NVARCHAR(15) NULL
	, [JDEProcessID] NUMERIC(3,0) NULL
	, [SettlementID] NUMERIC(8,0) NULL
	, [PaymentTerms] NVARCHAR(3) NULL
	, [CustomerReference1] NVARCHAR(25) NULL
	, [CustomerReference2] NVARCHAR(25) NULL
	, [AdjustmentSchedule] NVARCHAR(8) NULL
	, [TaxArea] NVARCHAR(10) NULL
	, [TaxExplanationCode] NVARCHAR(1) NULL
	, [VoidReasonCodeId] NUMERIC(3,0) NULL
	, [ChargeOnAccount] NVARCHAR(1) NULL
	, [HoldCommitted] NVARCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [Last_Modified] DATETIME null default getdate()
	, CONSTRAINT [PK_Order_Header_BETA] PRIMARY KEY ([OrderID] ASC)
	)END
/* TableDDL - [BUSDTA].[Order_Header_BETA] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Header_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_BETA_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_Header_BETA_del]
	(
	  [OrderID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([OrderID] ASC)
	)END
/* SHADOW TABLE FOR [BUSDTA].[Order_Header_BETA] - End */
/* TRIGGERS FOR Order_Header_BETA - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Header_BETA_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Header_BETA_ins
	ON BUSDTA.Order_Header_BETA AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Header_BETA_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Header_BETA_del.OrderID= inserted.OrderID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Header_BETA_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_BETA_upd
	ON BUSDTA.Order_Header_BETA AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Header_BETA
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Header_BETA.OrderID= inserted.OrderID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Header_BETA_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_BETA_dlt
	ON BUSDTA.Order_Header_BETA AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Header_BETA_del (OrderID, last_modified )
	SELECT deleted.OrderID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Header_BETA - END */
/* [BUSDTA].[Order_PriceAdj_BETA] - begins */

/* TableDDL - [BUSDTA].[Order_PriceAdj_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj_BETA]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_PriceAdj_BETA]
	(
	  [OrderID] NUMERIC(8,0) NOT NULL
	, [LineID] NUMERIC(7,4) NOT NULL
	, [AdjustmentSeq] NUMERIC(4,0) NOT NULL
	, [AdjName] NVARCHAR(20) NULL
	, [AdjDesc] NVARCHAR(60) NULL
	, [FactorValue] NUMERIC(12,4) NULL
	, [UnitPrice] NUMERIC(12,4) NULL
	, [OP] NVARCHAR(2) NULL
	, [BC] NVARCHAR(2) NULL
	, [BasicDesc] NVARCHAR(60) NULL
	, [PriceAdjustmentKeyID] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, CONSTRAINT [PK_Order_PriceAdj_BETA] PRIMARY KEY ([OrderID] ASC, [LineID] ASC, [AdjustmentSeq] ASC)
	)END
/* TableDDL - [BUSDTA].[Order_PriceAdj_BETA] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_PriceAdj_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj_BETA_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_PriceAdj_BETA_del]
	(
	  [OrderID] NUMERIC(8,0)
	, [LineID] NUMERIC(7,4)
	, [AdjustmentSeq] NUMERIC(4,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([OrderID] ASC, [LineID] ASC, [AdjustmentSeq] ASC)
	)END
/* SHADOW TABLE FOR [BUSDTA].[Order_PriceAdj_BETA] - End */
/* TRIGGERS FOR Order_PriceAdj_BETA - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_BETA_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_PriceAdj_BETA_ins
	ON BUSDTA.Order_PriceAdj_BETA AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_PriceAdj_BETA_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_PriceAdj_BETA_del.AdjustmentSeq= inserted.AdjustmentSeq AND BUSDTA.Order_PriceAdj_BETA_del.LineID= inserted.LineID AND BUSDTA.Order_PriceAdj_BETA_del.OrderID= inserted.OrderID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_BETA_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_PriceAdj_BETA_upd
	ON BUSDTA.Order_PriceAdj_BETA AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_PriceAdj_BETA
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_PriceAdj_BETA.AdjustmentSeq= inserted.AdjustmentSeq AND BUSDTA.Order_PriceAdj_BETA.LineID= inserted.LineID AND BUSDTA.Order_PriceAdj_BETA.OrderID= inserted.OrderID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_BETA_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_PriceAdj_BETA_dlt
	ON BUSDTA.Order_PriceAdj_BETA AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_PriceAdj_BETA_del (AdjustmentSeq, LineID, OrderID, last_modified )
	SELECT deleted.AdjustmentSeq, deleted.LineID, deleted.OrderID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_PriceAdj_BETA - END */

IF OBJECT_ID('[BUSDTA].[Order_Detail_BETA]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[Order_Detail_BETA]
(
	[OrderID] [numeric](8, 0) NOT NULL,
	[LineID] [numeric](7, 4) NOT NULL,
	[ItemId] [numeric](8, 0) NULL,
	[LongItem] [nchar](25) NULL,
	[ItemLocation] [nchar](20) NULL,
	[ItemLotNumber] [nchar](30) NULL,
	[LineType] [nchar](2) NULL,
	[IsNonStock] [nchar](1) NULL,
	[OrderQty] [numeric](8, 0) NOT NULL,
	[OrderUM] [nchar](2) NULL,
	[OrderQtyInPricingUM] [numeric](8, 0) NOT NULL,
	[PricingUM] [nchar](2) NULL,
	[UnitPriceOriginalAmtInPriceUM] [numeric](12, 4) NULL,
	[UnitPriceAmtInPriceUoM] [numeric](12, 4) NULL,
	[UnitPriceAmt] [numeric](12, 4) NULL,
	[ExtnPriceAmt] [numeric](12, 4) NULL,
	[ItemSalesTaxAmt] [numeric](12, 4) NULL,
	[Discounted] [nchar](1) NULL,
	[PriceOverriden] [nchar](1) NULL,
	[PriceOverrideReasonCodeId] [numeric](3, 0) NULL,
	[IsTaxable] [nchar](1) NULL,
	[ReturnHeldQty] [numeric](8, 0) NULL,
	[SalesCat1] [nchar](3) NULL,
	[SalesCat2] [nchar](3) NULL,
	[SalesCat3] [nchar](3) NULL,
	[SalesCat4] [nchar](3) NULL,
	[SalesCat5] [nchar](3) NULL,
	[PurchasingCat1] [nchar](3) NULL,
	[PurchasingCat2] [nchar](3) NULL,
	[PurchasingCat3] [nchar](3) NULL,
	[PurchasingCat4] [nchar](3) NULL,
	[PurchasingCat5] [nchar](3) NULL,
	[JDEOrderCompany] [nchar](5) NULL,
	[JDEOrderNumber] [numeric](8, 0) NULL,
	[JDEOrderType] [nchar](2) NULL,
	[JDEOrderLine] [numeric](7, 4) NULL,
	[JDEInvoiceCompany] [nchar](5) NULL,
	[JDEInvoiceNumber] [numeric](8, 0) NULL,
	[JDEOInvoiceType] [nchar](2) NULL,
	[JDEZBatchNumber] [nchar](15) NULL,
	[JDEProcessID] [numeric](3, 0) NULL,
	[SettlementID] [numeric](8, 0) NULL,
	[ExtendedAmtVariance] [numeric](12, 4) NULL,
	[TaxAmountAmtVariance] [numeric](12, 4) NULL,
	[HoldCode] [nchar](2) NULL,
	[CreatedBy] [numeric](8, 0) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedBy] [numeric](8, 0) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[last_modified] [datetime] NULL DEFAULT (getdate()),
 PRIMARY KEY ([OrderID] ASC,[LineID] ASC)) 
END

IF OBJECT_ID('[BUSDTA].[Order_Detail_BETA_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail_BETA_del]
	(
	[OrderID] [numeric](8, 0) NOT NULL,
	[LineID] [numeric](7, 4) NOT NULL,
	[last_modified] [datetime] NULL,
	PRIMARY KEY ([OrderID] ASC,	[LineID] ASC)
	)
	
END

IF OBJECT_ID('BUSDTA.Order_Detail_BETA_ins') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER [BUSDTA].[Order_Detail_BETA_ins]
		ON [BUSDTA].[Order_Detail_BETA] AFTER INSERT
		AS

		DELETE FROM BUSDTA.Order_Detail_BETA_del
		WHERE EXISTS ( SELECT 1
		FROM inserted
		WHERE BUSDTA.Order_Detail_BETA_del.OrderId= inserted.OrderId AND BUSDTA.Order_Detail_BETA_del.LineID= inserted.LineID 
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Detail_BETA_upd') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER [BUSDTA].[Order_Detail_BETA_upd]
			ON [BUSDTA].[Order_Detail_BETA] AFTER UPDATE
		AS
		UPDATE BUSDTA.Order_Detail_BETA
			SET last_modified = GETDATE()
			FROM inserted
				WHERE BUSDTA.Order_Detail_BETA.OrderId= inserted.OrderId AND BUSDTA.Order_Detail_BETA.LineID= inserted.LineID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_BETA_dlt') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER [BUSDTA].[Order_Detail_BETA_dlt]
			ON [BUSDTA].[Order_Detail_BETA] AFTER DELETE
		AS
		/* Insert the row into the shadow delete table. */
		INSERT INTO BUSDTA.Order_Detail_BETA_del (OrderID, LineID, last_modified )
			SELECT deleted.OrderID, deleted.LineID, GETDATE()
			FROM deleted;
	');
END
GO


/* [BUSDTA].[MasterNotification] - begins */

/* TableDDL - [BUSDTA].[MasterNotification] - Start */

IF OBJECT_ID('[BUSDTA].[MasterNotification]') IS NULL
BEGIN

CREATE TABLE [BUSDTA].[MasterNotification]
	
(
	  
[MasterNotificationID] NUMERIC(8,0) NOT NULL
	
, [RouteID] NUMERIC(8,0) NOT NULL
	
, [Title] NVARCHAR(200) NOT NULL
	
, [Description] NVARCHAR(300) NOT NULL
	
, [TypeID] NUMERIC(4,0) NULL
	
, [TransactionID] NUMERIC(4,0) NULL
, [Status] NUMERIC(4,0) NULL
	
, [Category] NUMERIC(4,0) NULL
	
, [Read] NVARCHAR(1) NULL CONSTRAINT DF_MASTERNOTIFICATION_Read DEFAULT('N')
	
, [ValidTill] DATE NULL
	, [Acknowledged] NVARCHAR(1) NULL CONSTRAINT DF_MASTERNOTIFICATION_Acknowledged DEFAULT('N')
	
, [Initiator] NUMERIC(8,0) NULL
	, [ResponseType] NUMERIC(3,0) NULL
	
, [ResponseValue] CHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	
, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_MASTERNOTIFICATION_CreatedDatetime DEFAULT(getdate())
	
, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_MASTERNOTIFICATION_UpdatedDatetime DEFAULT(getdate())
	
, last_modified DATETIME DEFAULT GETDATE()
	, CONSTRAINT [PK_MasterNotification] PRIMARY KEY ([MasterNotificationID] ASC, [RouteID] ASC)
	
)

END
/* TableDDL - [BUSDTA].[MasterNotification] - End */


/* SHADOW TABLE FOR [BUSDTA].[MasterNotification] - Start */
IF OBJECT_ID('[BUSDTA].[MasterNotification_del]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[MasterNotification_del]
	
(
	  
[MasterNotificationID] NUMERIC(8,0)
	
, [RouteID] NUMERIC(8,0)
	
, last_modified DATETIME DEFAULT GETDATE()
	
, PRIMARY KEY ([MasterNotificationID] ASC, [RouteID] ASC)
	
)

END


/* SHADOW TABLE FOR [BUSDTA].[MasterNotification] - End */
/* TRIGGERS FOR MasterNotification - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.MasterNotification_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.MasterNotification_ins
	ON BUSDTA.MasterNotification AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.MasterNotification_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.MasterNotification_del.MasterNotificationID= inserted.MasterNotificationID AND BUSDTA.MasterNotification_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.MasterNotification_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MasterNotification_upd
	ON BUSDTA.MasterNotification AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.MasterNotification
	SET last_modified = GETDATE()
	FROM inserted
	WHERE BUSDTA.MasterNotification.MasterNotificationID= inserted.MasterNotificationID AND BUSDTA.MasterNotification.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.MasterNotification_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MasterNotification_dlt
	ON BUSDTA.MasterNotification AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.MasterNotification_del (MasterNotificationID, RouteID, last_modified )
	SELECT deleted.MasterNotificationID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO
/* [BUSDTA].[StatusMaster] - begins */

/* TableDDL - [BUSDTA].[StatusMaster] - Start */
IF OBJECT_ID('[BUSDTA].[StatusMaster]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[StatusMaster]
	
(

	  
[StatusId] INT NOT NULL IDENTITY(1,1)
	
, [StatusCode] VARCHAR(5) NOT NULL
	
, [StatusDescription] NVARCHAR(100) NULL
	
, [StatusForType] NVARCHAR(50) NULL
	
, [CreatedBy] NUMERIC(8,0) NULL
	
, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_STATUSMASTER_CreatedDatetime DEFAULT(getdate())
	
, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_STATUSMASTER_UpdatedDatetime DEFAULT(getdate())
	
, last_modified DATETIME DEFAULT GETDATE()
	
, CONSTRAINT [PK_StatusMaster] PRIMARY KEY ([StatusId] ASC)
	
)

END
/* TableDDL - [BUSDTA].[StatusMaster] - End */

/* SHADOW TABLE FOR [BUSDTA].[StatusMaster] - Start */
IF OBJECT_ID('[BUSDTA].[StatusMaster_del]') IS NULL
BEGIN
	
CREATE TABLE [BUSDTA].[StatusMaster_del]
	
(

	  
[StatusId] INT
	
,last_modified DATETIME DEFAULT GETDATE()
	
,PRIMARY KEY ([StatusId] ASC)
	)

END

/* SHADOW TABLE FOR [BUSDTA].[StatusMaster] - End */
/* TRIGGERS FOR StatusMaster - START */

/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.StatusMaster_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.StatusMaster_ins
	ON BUSDTA.StatusMaster AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.StatusMaster_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.StatusMaster_del.StatusId= inserted.StatusId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.StatusMaster_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.StatusMaster_upd
	ON BUSDTA.StatusMaster AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.StatusMaster
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.StatusMaster.StatusId= inserted.StatusId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.StatusMaster_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.StatusMaster_dlt
	ON BUSDTA.StatusMaster AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.StatusMaster_del (StatusId, last_modified )
	SELECT deleted.StatusId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR StatusMaster - END */

/* [BUSDTA].[Order_Templates] - begins */

/* TableDDL - [BUSDTA].[Order_Templates] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Templates]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_Templates]
	(
	  [TemplateType] NCHAR(10) NOT NULL
	, [ShipToID] NUMERIC(8,0) NOT NULL
	, [TemplateDetailID] NUMERIC(12,0) NOT NULL
	, [DisplaySeq] NUMERIC(5,0) NULL
	, [ItemID] NUMERIC(8,0) NULL
	, [LongItem] NCHAR(25) NULL
	, [TemplateQuantity] NUMERIC(8,0) NULL
	, [TemplateUoM] NCHAR(2) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ORDER_TEMPLATES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Order_Templates] PRIMARY KEY ([TemplateType] ASC, [ShipToID] ASC, [TemplateDetailID] ASC)
	)END
/* TableDDL - [BUSDTA].[Order_Templates] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Templates] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Templates_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_Templates_del]
	(
	  [TemplateType] NCHAR(10)
	, [ShipToID] NUMERIC(8,0)
	, [TemplateDetailID] NUMERIC(12,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TemplateType] ASC, [ShipToID] ASC, [TemplateDetailID] ASC)
	)END
/* SHADOW TABLE FOR [BUSDTA].[Order_Templates] - End */
/* TRIGGERS FOR Order_Templates - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Templates_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Templates_ins
	ON BUSDTA.Order_Templates AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Templates_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Templates_del.ShipToID= inserted.ShipToID AND BUSDTA.Order_Templates_del.TemplateDetailID= inserted.TemplateDetailID AND BUSDTA.Order_Templates_del.TemplateType= inserted.TemplateType
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Templates_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Templates_upd
	ON BUSDTA.Order_Templates AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Templates
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Templates.ShipToID= inserted.ShipToID AND BUSDTA.Order_Templates.TemplateDetailID= inserted.TemplateDetailID AND BUSDTA.Order_Templates.TemplateType= inserted.TemplateType');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Templates_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Templates_dlt
	ON BUSDTA.Order_Templates AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Templates_del (ShipToID, TemplateDetailID, TemplateType, last_modified )
	SELECT deleted.ShipToID, deleted.TemplateDetailID, deleted.TemplateType, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Templates - END */


/* [BUSDTA].[F0101_Audit] - begins */

/* TableDDL - [BUSDTA].[F0101_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0101_Audit]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0101_Audit]
	(
	  [TransID] NUMERIC(8,0) NULL
	, [TransType] VARCHAR(50) NULL
	, [ABAN8] NUMERIC(8,0) NOT NULL
	, [ABALKY] NCHAR(20) NULL
	, [ABTAX] NCHAR(20) NULL
	, [ABALPH] NCHAR(40) NULL
	, [ABMCU] NCHAR(12) NULL
	, [ABSIC] NCHAR(10) NULL
	, [ABLNGP] NCHAR(2) NULL
	, [ABAT1] NCHAR(3) NULL
	, [ABCM] NCHAR(2) NULL
	, [ABTAXC] NCHAR(1) NULL
	, [ABAT2] NCHAR(1) NULL
	, [ABAN81] FLOAT NULL
	, [ABAN82] FLOAT NULL
	, [ABAN83] FLOAT NULL
	, [ABAN84] FLOAT NULL
	, [ABAN86] FLOAT NULL
	, [ABAN85] FLOAT NULL
	, [ABAC01] NCHAR(3) NULL
	, [ABAC02] NCHAR(3) NULL
	, [ABAC03] NCHAR(3) NULL
	, [ABAC04] NCHAR(3) NULL
	, [ABAC05] NCHAR(3) NULL
	, [ABAC06] NCHAR(3) NULL
	, [ABAC07] NCHAR(3) NULL
	, [ABAC08] NCHAR(3) NULL
	, [ABAC09] NCHAR(3) NULL
	, [ABAC10] NCHAR(3) NULL
	, [ABAC11] NCHAR(3) NULL
	, [ABAC12] NCHAR(3) NULL
	, [ABAC13] NCHAR(3) NULL
	, [ABAC14] NCHAR(3) NULL
	, [ABAC15] NCHAR(3) NULL
	, [ABAC16] NCHAR(3) NULL
	, [ABAC17] NCHAR(3) NULL
	, [ABAC18] NCHAR(3) NULL
	, [ABAC19] NCHAR(3) NULL
	, [ABAC20] NCHAR(3) NULL
	, [ABAC21] NCHAR(3) NULL
	, [ABAC22] NCHAR(3) NULL
	, [ABAC23] NCHAR(3) NULL
	, [ABAC24] NCHAR(3) NULL
	, [ABAC25] NCHAR(3) NULL
	, [ABAC26] NCHAR(3) NULL
	, [ABAC27] NCHAR(3) NULL
	, [ABAC28] NCHAR(3) NULL
	, [ABAC29] NCHAR(3) NULL
	, [ABAC30] NCHAR(3) NULL
	, [ABRMK] NCHAR(30) NULL
	, [ABTXCT] NCHAR(20) NULL
	, [ABTX2] NCHAR(20) NULL
	, [ABALP1] NCHAR(40) NULL
	, [WSValidation] VARCHAR(50) NULL
	, [Status] VARCHAR(50) NULL
	, [RejReason] VARCHAR(50) NULL
	, [SyncID] INT NULL
	, [UpdatedDateTime] DATETIME NOT NULL CONSTRAINT DF_F0101_AUDIT_UpdatedDateTime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0101_AUDIT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0101_Audit] PRIMARY KEY ([ABAN8] ASC)
	)END
/* TableDDL - [BUSDTA].[F0101_Audit] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0101_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0101_Audit_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0101_Audit_del]
	(
	  [TransID] NUMERIC(8,0) 
	, [ABAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TransID] asc, [ABAN8] ASC)
	)END
/* SHADOW TABLE FOR [BUSDTA].[F0101_Audit] - End */
/* TRIGGERS FOR F0101_Audit - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0101_Audit_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0101_Audit_ins
	ON BUSDTA.F0101_Audit AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0101_Audit_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0101_Audit_del.TransID= inserted.TransID AND BUSDTA.F0101_Audit_del.ABAN8= inserted.ABAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0101_Audit_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0101_Audit_upd
	ON BUSDTA.F0101_Audit AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0101_Audit
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0101_Audit.TransID= inserted.TransID AND BUSDTA.F0101_Audit.ABAN8= inserted.ABAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0101_Audit_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0101_Audit_dlt
	ON BUSDTA.F0101_Audit AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0101_Audit_del (TransID,ABAN8, last_modified )
	SELECT deleted.TransID,deleted.ABAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0101_Audit - END */

/* TableDDL - [BUSDTA].[F0116_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0116_Audit]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0116_Audit]
	(
	  [TransID] NUMERIC(8,0) 
	, [TransType] VARCHAR(50) NULL
	, [ALAN8] NUMERIC(8,0) NOT NULL
	, [ALEFTB] NUMERIC(18,0) NOT NULL
	, [ALEFTF] NCHAR(1) NULL
	, [ALADD1] NCHAR(40) NULL
	, [ALADD2] NCHAR(40) NULL
	, [ALADD3] NCHAR(40) NULL
	, [ALADD4] NCHAR(40) NULL
	, [ALADDZ] NCHAR(12) NULL
	, [ALCTY1] NCHAR(25) NULL
	, [ALCOUN] NCHAR(25) NULL
	, [ALADDS] NCHAR(3) NULL
	, [WSValidation] VARCHAR(50) NULL
	, [Status] VARCHAR(50) NULL
	, [RejReason] VARCHAR(50) NULL
	, [SyncID] INT NULL
	, [UpdatedDateTime] DATETIME NOT NULL CONSTRAINT DF_F0116_AUDIT_UpdatedDateTime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0116_AUDIT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0116_Audit] PRIMARY KEY ([ALAN8] ASC, [ALEFTB] ASC,[TransID] ASC )
	)END
/* TableDDL - [BUSDTA].[F0116_Audit] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0116_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0116_Audit_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0116_Audit_del]
	(
	  [TransID] NUMERIC(8,0) 	
	, [ALAN8] NUMERIC(8,0)
	, [ALEFTB] NUMERIC(18,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY (TransID ASC,[ALAN8] ASC, [ALEFTB] ASC)
	)END
/* SHADOW TABLE FOR [BUSDTA].[F0116_Audit] - End */
/* TRIGGERS FOR F0116_Audit - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0116_Audit_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0116_Audit_ins
	ON BUSDTA.F0116_Audit AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0116_Audit_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0116_Audit_del.TransID= inserted.TransID AND BUSDTA.F0116_Audit_del.ALAN8= inserted.ALAN8 AND BUSDTA.F0116_Audit_del.ALEFTB= inserted.ALEFTB
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0116_Audit_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_Audit_upd
	ON BUSDTA.F0116_Audit AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0116_Audit
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0116_Audit.TransID= inserted.TransID AND BUSDTA.F0116_Audit.ALAN8= inserted.ALAN8 AND BUSDTA.F0116_Audit.ALEFTB= inserted.ALEFTB');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0116_Audit_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_Audit_dlt
	ON BUSDTA.F0116_Audit AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0116_Audit_del (TransID,ALAN8, ALEFTB, last_modified )
	SELECT deleted.TransID, deleted.ALAN8, deleted.ALEFTB, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0116_Audit - END */
