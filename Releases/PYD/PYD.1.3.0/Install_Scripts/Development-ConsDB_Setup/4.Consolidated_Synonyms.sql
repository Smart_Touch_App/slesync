
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ReferenceRefreshDetails')
DROP SYNONYM [dbo].[ReferenceRefreshDetails]
GO
CREATE SYNONYM [dbo].[ReferenceRefreshDetails] FOR [CIS54\SQLEXPRESS2012].[JDEData_CRP].[dbo].[vw_GetRefreshdetails]
GO



CREATE SYNONYM [BUSDTA].[synDeviceMaster] FOR [HP-1\SQLEXPRESS2012].[SystemDB_FPS5_Release].[dbo].[vw_DeviceMaster]
GO

CREATE SYNONYM [BUSDTA].[synUserMaster] FOR [HP-1\SQLEXPRESS2012].[SystemDB_FPS5_Release].[dbo].[vw_UserMaster]
GO
