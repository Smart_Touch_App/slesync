

EXEC ml_add_lang_connection_script
	'SLE_SyncScript_PYD.1.3.0',
	'authenticate_user',
	'sql',
	null
GO

EXEC ml_add_lang_connection_script
	'SLE_SyncScript_PYD.1.3.0',
	'begin_connection',
	'sql',
    'SET NOCOUNT ON'
GO


/* Table :ApprovalCodeLog */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ApprovalCodeLog',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."ApprovalCodeLog"."LogID"  
,"BUSDTA"."ApprovalCodeLog"."RouteID"  
,"BUSDTA"."ApprovalCodeLog"."ApprovalCode"  
,"BUSDTA"."ApprovalCodeLog"."RequestCode"  
,"BUSDTA"."ApprovalCodeLog"."Feature"  
,"BUSDTA"."ApprovalCodeLog"."Amount"  
,"BUSDTA"."ApprovalCodeLog"."RequestedByUser"  
,"BUSDTA"."ApprovalCodeLog"."RequestedForCustomer"  
,"BUSDTA"."ApprovalCodeLog"."Payload" 
,"BUSDTA"."ApprovalCodeLog"."CreatedBy"  
,"BUSDTA"."ApprovalCodeLog"."CreatedDatetime"  
,"BUSDTA"."ApprovalCodeLog"."UpdatedBy"  
,"BUSDTA"."ApprovalCodeLog"."UpdatedDatetime"   

FROM "BUSDTA"."ApprovalCodeLog"  
WHERE "BUSDTA"."ApprovalCodeLog"."last_modified">= {ml s.last_table_download}  
AND "BUSDTA"."ApprovalCodeLog"."RouteId" = 
(select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})           
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ApprovalCodeLog',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."ApprovalCodeLog_del"."LogID"
,"BUSDTA"."ApprovalCodeLog_del"."RouteID"

FROM "BUSDTA"."ApprovalCodeLog_del"
WHERE "BUSDTA"."ApprovalCodeLog_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ApprovalCodeLog',
	'upload_insert',
	'sql',

'

/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."ApprovalCodeLog"
("LogID", "RouteID", "ApprovalCode", "RequestCode", "Feature", "Amount", "RequestedByUser", "RequestedForCustomer", "Payload", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."LogID"}, {ml r."RouteID"}, {ml r."ApprovalCode"}, {ml r."RequestCode"}, {ml r."Feature"}, {ml r."Amount"}, {ml r."RequestedByUser"}, {ml r."RequestedForCustomer"}, {ml r."Payload"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ApprovalCodeLog',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ApprovalCodeLog"
SET "ApprovalCode" = {ml r."ApprovalCode"}, "RequestCode" = {ml r."RequestCode"}, "Feature" = {ml r."Feature"}, "Amount" = {ml r."Amount"}, "RequestedByUser" = {ml r."RequestedByUser"}, "RequestedForCustomer" = {ml r."RequestedForCustomer"}, "Payload" = {ml r."Payload"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "LogID" = {ml r."LogID"} AND "RouteID" = {ml r."RouteID"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ApprovalCodeLog',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."ApprovalCodeLog"
WHERE "LogID" = {ml r."LogID"} AND "RouteID" = {ml r."RouteID"}

'

/* upload_delete - End */

/* Table :Request_Authorization_Format */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Request_Authorization_Format',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Request_Authorization_Format"."RequestFormatCode"
,"BUSDTA"."Request_Authorization_Format"."AuthorizationFormatCode"
,"BUSDTA"."Request_Authorization_Format"."Key1"
,"BUSDTA"."Request_Authorization_Format"."Key2"
,"BUSDTA"."Request_Authorization_Format"."Key3"
,"BUSDTA"."Request_Authorization_Format"."RouteId"
,"BUSDTA"."Request_Authorization_Format"."CreatedBy"
,"BUSDTA"."Request_Authorization_Format"."CreatedDatetime"
,"BUSDTA"."Request_Authorization_Format"."UpdatedBy"
,"BUSDTA"."Request_Authorization_Format"."UpdatedDatetime"

FROM "BUSDTA"."Request_Authorization_Format"
WHERE "BUSDTA"."Request_Authorization_Format"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Request_Authorization_Format',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Request_Authorization_Format_del"."RequestFormatCode"
,"BUSDTA"."Request_Authorization_Format_del"."AuthorizationFormatCode"
,"BUSDTA"."Request_Authorization_Format_del"."Key1"
,"BUSDTA"."Request_Authorization_Format_del"."Key2"
,"BUSDTA"."Request_Authorization_Format_del"."Key3"

FROM "BUSDTA"."Request_Authorization_Format_del"
WHERE "BUSDTA"."Request_Authorization_Format_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :Prospect_Quote_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Header',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Prospect_Quote_Header"."ProspectQuoteId"
,"BUSDTA"."Prospect_Quote_Header"."ProspectId"
,"BUSDTA"."Prospect_Quote_Header"."RouteId"
,"BUSDTA"."Prospect_Quote_Header"."StatusId"
,"BUSDTA"."Prospect_Quote_Header"."IsPrinted"
,"BUSDTA"."Prospect_Quote_Header"."IsSampled"
,"BUSDTA"."Prospect_Quote_Header"."SettlementId"
,"BUSDTA"."Prospect_Quote_Header"."PickStatus"
,"BUSDTA"."Prospect_Quote_Header"."IsMasterSetup"
,"BUSDTA"."Prospect_Quote_Header"."AIAC05District"
,"BUSDTA"."Prospect_Quote_Header"."AIAC05DistrictMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC06Region"
,"BUSDTA"."Prospect_Quote_Header"."AIAC06RegionMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC07Reporting"
,"BUSDTA"."Prospect_Quote_Header"."AIAC07ReportingMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC08Chain"
,"BUSDTA"."Prospect_Quote_Header"."AIAC08ChainMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC09BrewmaticAgentCode"
,"BUSDTA"."Prospect_Quote_Header"."AIAC09BrewmaticAgentCodeMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC10NTR"
,"BUSDTA"."Prospect_Quote_Header"."AIAC10NTRMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC11CustomerTaxGrp"
,"BUSDTA"."Prospect_Quote_Header"."AIAC11CustomerTaxGrpMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC12CategoryCode12"
,"BUSDTA"."Prospect_Quote_Header"."AIAC12CategoryCode12Mst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC13APCheckCode"
,"BUSDTA"."Prospect_Quote_Header"."AIAC13APCheckCodeMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC14CategoryCode14"
,"BUSDTA"."Prospect_Quote_Header"."AIAC14CategoryCode14Mst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC15CategoryCode15"
,"BUSDTA"."Prospect_Quote_Header"."AIAC15CategoryCode15Mst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC16CategoryCode16"
,"BUSDTA"."Prospect_Quote_Header"."AIAC16CategoryCode16Mst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC17CategoryCode17"
,"BUSDTA"."Prospect_Quote_Header"."AIAC17CategoryCode17Mst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC18CategoryCode18"
,"BUSDTA"."Prospect_Quote_Header"."AIAC18CategoryCode18Mst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC22POSUpCharge"
,"BUSDTA"."Prospect_Quote_Header"."AIAC22POSUpChargeMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC23LiquidCoffee"
,"BUSDTA"."Prospect_Quote_Header"."AIAC23LiquidCoffeeMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC24PriceProtection"
,"BUSDTA"."Prospect_Quote_Header"."AIAC24PriceProtectionMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC27AlliedDiscount"
,"BUSDTA"."Prospect_Quote_Header"."AIAC27AlliedDiscountMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC28CoffeeVolume"
,"BUSDTA"."Prospect_Quote_Header"."AIAC28CoffeeVolumeMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC29EquipmentProgPts"
,"BUSDTA"."Prospect_Quote_Header"."AIAC29EquipmentProgPtsMst"
,"BUSDTA"."Prospect_Quote_Header"."AIAC30SpecialCCP"
,"BUSDTA"."Prospect_Quote_Header"."AIAC30SpecialCCPMst"
,"BUSDTA"."Prospect_Quote_Header"."PriceDate"
,"BUSDTA"."Prospect_Quote_Header"."CreatedBy"
,"BUSDTA"."Prospect_Quote_Header"."CreatedDatetime"
,"BUSDTA"."Prospect_Quote_Header"."UpdatedBy"
,"BUSDTA"."Prospect_Quote_Header"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Quote_Header"
WHERE "BUSDTA"."Prospect_Quote_Header"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Quote_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username}) 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Header',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Prospect_Quote_Header_del"."ProspectQuoteId"

FROM "BUSDTA"."Prospect_Quote_Header_del"
WHERE "BUSDTA"."Prospect_Quote_Header_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Header',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Prospect_Quote_Header"
("ProspectQuoteId", "ProspectId", "RouteId", "StatusId", "IsPrinted", "IsSampled", "SettlementId", "PickStatus", "IsMasterSetup", "AIAC05District", "AIAC05DistrictMst", "AIAC06Region", "AIAC06RegionMst", "AIAC07Reporting", "AIAC07ReportingMst", "AIAC08Chain", "AIAC08ChainMst", "AIAC09BrewmaticAgentCode", "AIAC09BrewmaticAgentCodeMst", "AIAC10NTR", "AIAC10NTRMst", "AIAC11CustomerTaxGrp", "AIAC11CustomerTaxGrpMst", "AIAC12CategoryCode12", "AIAC12CategoryCode12Mst", "AIAC13APCheckCode", "AIAC13APCheckCodeMst", "AIAC14CategoryCode14", "AIAC14CategoryCode14Mst", "AIAC15CategoryCode15", "AIAC15CategoryCode15Mst", "AIAC16CategoryCode16", "AIAC16CategoryCode16Mst", "AIAC17CategoryCode17", "AIAC17CategoryCode17Mst", "AIAC18CategoryCode18", "AIAC18CategoryCode18Mst", "AIAC22POSUpCharge", "AIAC22POSUpChargeMst", "AIAC23LiquidCoffee", "AIAC23LiquidCoffeeMst", "AIAC24PriceProtection", "AIAC24PriceProtectionMst", "AIAC27AlliedDiscount", "AIAC27AlliedDiscountMst", "AIAC28CoffeeVolume", "AIAC28CoffeeVolumeMst", "AIAC29EquipmentProgPts", "AIAC29EquipmentProgPtsMst", "AIAC30SpecialCCP", "AIAC30SpecialCCPMst", "PriceDate", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ProspectQuoteId"}, {ml r."ProspectId"}, {ml r."RouteId"}, {ml r."StatusId"}, {ml r."IsPrinted"}, {ml r."IsSampled"}, {ml r."SettlementId"}, {ml r."PickStatus"}, {ml r."IsMasterSetup"}, {ml r."AIAC05District"}, {ml r."AIAC05DistrictMst"}, {ml r."AIAC06Region"}, {ml r."AIAC06RegionMst"}, {ml r."AIAC07Reporting"}, {ml r."AIAC07ReportingMst"}, {ml r."AIAC08Chain"}, {ml r."AIAC08ChainMst"}, {ml r."AIAC09BrewmaticAgentCode"}, {ml r."AIAC09BrewmaticAgentCodeMst"}, {ml r."AIAC10NTR"}, {ml r."AIAC10NTRMst"}, {ml r."AIAC11CustomerTaxGrp"}, {ml r."AIAC11CustomerTaxGrpMst"}, {ml r."AIAC12CategoryCode12"}, {ml r."AIAC12CategoryCode12Mst"}, {ml r."AIAC13APCheckCode"}, {ml r."AIAC13APCheckCodeMst"}, {ml r."AIAC14CategoryCode14"}, {ml r."AIAC14CategoryCode14Mst"}, {ml r."AIAC15CategoryCode15"}, {ml r."AIAC15CategoryCode15Mst"}, {ml r."AIAC16CategoryCode16"}, {ml r."AIAC16CategoryCode16Mst"}, {ml r."AIAC17CategoryCode17"}, {ml r."AIAC17CategoryCode17Mst"}, {ml r."AIAC18CategoryCode18"}, {ml r."AIAC18CategoryCode18Mst"}, {ml r."AIAC22POSUpCharge"}, {ml r."AIAC22POSUpChargeMst"}, {ml r."AIAC23LiquidCoffee"}, {ml r."AIAC23LiquidCoffeeMst"}, {ml r."AIAC24PriceProtection"}, {ml r."AIAC24PriceProtectionMst"}, {ml r."AIAC27AlliedDiscount"}, {ml r."AIAC27AlliedDiscountMst"}, {ml r."AIAC28CoffeeVolume"}, {ml r."AIAC28CoffeeVolumeMst"}, {ml r."AIAC29EquipmentProgPts"}, {ml r."AIAC29EquipmentProgPtsMst"}, {ml r."AIAC30SpecialCCP"}, {ml r."AIAC30SpecialCCPMst"}, {ml r."PriceDate"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Header',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Quote_Header"
SET "ProspectId" = {ml r."ProspectId"}, "RouteId" = {ml r."RouteId"}, "StatusId" = {ml r."StatusId"}, "IsPrinted" = {ml r."IsPrinted"}, "IsSampled" = {ml r."IsSampled"}, "SettlementId" = {ml r."SettlementId"}, "PickStatus" = {ml r."PickStatus"}, "IsMasterSetup" = {ml r."IsMasterSetup"}, "AIAC05District" = {ml r."AIAC05District"}, "AIAC05DistrictMst" = {ml r."AIAC05DistrictMst"}, "AIAC06Region" = {ml r."AIAC06Region"}, "AIAC06RegionMst" = {ml r."AIAC06RegionMst"}, "AIAC07Reporting" = {ml r."AIAC07Reporting"}, "AIAC07ReportingMst" = {ml r."AIAC07ReportingMst"}, "AIAC08Chain" = {ml r."AIAC08Chain"}, "AIAC08ChainMst" = {ml r."AIAC08ChainMst"}, "AIAC09BrewmaticAgentCode" = {ml r."AIAC09BrewmaticAgentCode"}, "AIAC09BrewmaticAgentCodeMst" = {ml r."AIAC09BrewmaticAgentCodeMst"}, "AIAC10NTR" = {ml r."AIAC10NTR"}, "AIAC10NTRMst" = {ml r."AIAC10NTRMst"}, "AIAC11CustomerTaxGrp" = {ml r."AIAC11CustomerTaxGrp"}, "AIAC11CustomerTaxGrpMst" = {ml r."AIAC11CustomerTaxGrpMst"}, "AIAC12CategoryCode12" = {ml r."AIAC12CategoryCode12"}, "AIAC12CategoryCode12Mst" = {ml r."AIAC12CategoryCode12Mst"}, "AIAC13APCheckCode" = {ml r."AIAC13APCheckCode"}, "AIAC13APCheckCodeMst" = {ml r."AIAC13APCheckCodeMst"}, "AIAC14CategoryCode14" = {ml r."AIAC14CategoryCode14"}, "AIAC14CategoryCode14Mst" = {ml r."AIAC14CategoryCode14Mst"}, "AIAC15CategoryCode15" = {ml r."AIAC15CategoryCode15"}, "AIAC15CategoryCode15Mst" = {ml r."AIAC15CategoryCode15Mst"}, "AIAC16CategoryCode16" = {ml r."AIAC16CategoryCode16"}, "AIAC16CategoryCode16Mst" = {ml r."AIAC16CategoryCode16Mst"}, "AIAC17CategoryCode17" = {ml r."AIAC17CategoryCode17"}, "AIAC17CategoryCode17Mst" = {ml r."AIAC17CategoryCode17Mst"}, "AIAC18CategoryCode18" = {ml r."AIAC18CategoryCode18"}, "AIAC18CategoryCode18Mst" = {ml r."AIAC18CategoryCode18Mst"}, "AIAC22POSUpCharge" = {ml r."AIAC22POSUpCharge"}, "AIAC22POSUpChargeMst" = {ml r."AIAC22POSUpChargeMst"}, "AIAC23LiquidCoffee" = {ml r."AIAC23LiquidCoffee"}, "AIAC23LiquidCoffeeMst" = {ml r."AIAC23LiquidCoffeeMst"}, "AIAC24PriceProtection" = {ml r."AIAC24PriceProtection"}, "AIAC24PriceProtectionMst" = {ml r."AIAC24PriceProtectionMst"}, "AIAC27AlliedDiscount" = {ml r."AIAC27AlliedDiscount"}, "AIAC27AlliedDiscountMst" = {ml r."AIAC27AlliedDiscountMst"}, "AIAC28CoffeeVolume" = {ml r."AIAC28CoffeeVolume"}, "AIAC28CoffeeVolumeMst" = {ml r."AIAC28CoffeeVolumeMst"}, "AIAC29EquipmentProgPts" = {ml r."AIAC29EquipmentProgPts"}, "AIAC29EquipmentProgPtsMst" = {ml r."AIAC29EquipmentProgPtsMst"}, "AIAC30SpecialCCP" = {ml r."AIAC30SpecialCCP"}, "AIAC30SpecialCCPMst" = {ml r."AIAC30SpecialCCPMst"}, "PriceDate" = {ml r."PriceDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectQuoteId" = {ml r."ProspectQuoteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Header',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Prospect_Quote_Header"
WHERE "ProspectQuoteId" = {ml r."ProspectQuoteId"}
'

/* upload_delete - End */

/* Table :Prospect_Quote_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Detail',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."Prospect_Quote_Detail"."ProspectQuoteId"
,"BUSDTA"."Prospect_Quote_Detail"."ProspectQuoteDetailId"
,"BUSDTA"."Prospect_Quote_Detail"."RouteId"
,"BUSDTA"."Prospect_Quote_Detail"."ItemId"
,"BUSDTA"."Prospect_Quote_Detail"."QuoteQty"
,"BUSDTA"."Prospect_Quote_Detail"."SampleQty"
,"BUSDTA"."Prospect_Quote_Detail"."SampleUM"
,"BUSDTA"."Prospect_Quote_Detail"."PricingAmt"
,"BUSDTA"."Prospect_Quote_Detail"."PricingUM"
,"BUSDTA"."Prospect_Quote_Detail"."TransactionAmt"
,"BUSDTA"."Prospect_Quote_Detail"."TransactionUM"
,"BUSDTA"."Prospect_Quote_Detail"."OtherAmt"
,"BUSDTA"."Prospect_Quote_Detail"."OtherUM"
,"BUSDTA"."Prospect_Quote_Detail"."CompetitorName"
,"BUSDTA"."Prospect_Quote_Detail"."UnitPriceAmt"
,"BUSDTA"."Prospect_Quote_Detail"."UnitPriceUM"
,"BUSDTA"."Prospect_Quote_Detail"."AvailableQty"
,"BUSDTA"."Prospect_Quote_Detail"."CreatedBy"
,"BUSDTA"."Prospect_Quote_Detail"."CreatedDatetime"
,"BUSDTA"."Prospect_Quote_Detail"."UpdatedBy"
,"BUSDTA"."Prospect_Quote_Detail"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Quote_Detail"
WHERE "BUSDTA"."Prospect_Quote_Detail"."last_modified">= {ml s.last_table_download}
 AND "BUSDTA"."Prospect_Quote_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Detail',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Prospect_Quote_Detail_del"."ProspectQuoteId"
,"BUSDTA"."Prospect_Quote_Detail_del"."ProspectQuoteDetailId"
,"BUSDTA"."Prospect_Quote_Detail_del"."RouteId"

FROM "BUSDTA"."Prospect_Quote_Detail_del"
WHERE "BUSDTA"."Prospect_Quote_Detail_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Detail',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Prospect_Quote_Detail"
("ProspectQuoteId", "ProspectQuoteDetailId", "RouteId", "ItemId", "QuoteQty", "SampleQty", "SampleUM", "PricingAmt", "PricingUM", "TransactionAmt", "TransactionUM", "OtherAmt", "OtherUM", "CompetitorName", "UnitPriceAmt", "UnitPriceUM", "AvailableQty", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ProspectQuoteId"}, {ml r."ProspectQuoteDetailId"}, {ml r."RouteId"}, {ml r."ItemId"}, {ml r."QuoteQty"}, {ml r."SampleQty"}, {ml r."SampleUM"}, {ml r."PricingAmt"}, {ml r."PricingUM"}, {ml r."TransactionAmt"}, {ml r."TransactionUM"}, {ml r."OtherAmt"}, {ml r."OtherUM"}, {ml r."CompetitorName"}, {ml r."UnitPriceAmt"}, {ml r."UnitPriceUM"}, {ml r."AvailableQty"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Detail',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Quote_Detail"
SET "ItemId" = {ml r."ItemId"}, "QuoteQty" = {ml r."QuoteQty"}, "SampleQty" = {ml r."SampleQty"}, "SampleUM" = {ml r."SampleUM"}, "PricingAmt" = {ml r."PricingAmt"}, "PricingUM" = {ml r."PricingUM"}, "TransactionAmt" = {ml r."TransactionAmt"}, "TransactionUM" = {ml r."TransactionUM"}, "OtherAmt" = {ml r."OtherAmt"}, "OtherUM" = {ml r."OtherUM"}, "CompetitorName" = {ml r."CompetitorName"}, "UnitPriceAmt" = {ml r."UnitPriceAmt"}, "UnitPriceUM" = {ml r."UnitPriceUM"}, "AvailableQty" = {ml r."AvailableQty"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectQuoteId" = {ml r."ProspectQuoteId"} AND "ProspectQuoteDetailId" = {ml r."ProspectQuoteDetailId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Quote_Detail',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Prospect_Quote_Detail"
WHERE "ProspectQuoteId" = {ml r."ProspectQuoteId"} AND "ProspectQuoteDetailId" = {ml r."ProspectQuoteDetailId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :Prospect_Note */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Note',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."Prospect_Note"."ProspectId"
,"BUSDTA"."Prospect_Note"."ProspectNoteId"
,"BUSDTA"."Prospect_Note"."RouteId"
,"BUSDTA"."Prospect_Note"."ProspectNoteType"
,"BUSDTA"."Prospect_Note"."ProspectNoteDetail"
,"BUSDTA"."Prospect_Note"."IsDefault"
,"BUSDTA"."Prospect_Note"."CreatedBy"
,"BUSDTA"."Prospect_Note"."CreatedDatetime"
,"BUSDTA"."Prospect_Note"."UpdatedBy"
,"BUSDTA"."Prospect_Note"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Note"
WHERE "BUSDTA"."Prospect_Note"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Prospect_Note"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username}) 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Note',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Prospect_Note_del"."ProspectId"
,"BUSDTA"."Prospect_Note_del"."ProspectNoteId"
,"BUSDTA"."Prospect_Note_del"."RouteId"

FROM "BUSDTA"."Prospect_Note_del"
WHERE "BUSDTA"."Prospect_Note_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Note',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Prospect_Note"
("ProspectId", "ProspectNoteId", "RouteId", "ProspectNoteType", "ProspectNoteDetail", "IsDefault", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ProspectId"}, {ml r."ProspectNoteId"}, {ml r."RouteId"}, {ml r."ProspectNoteType"}, {ml r."ProspectNoteDetail"}, {ml r."IsDefault"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Note',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Note"
SET "ProspectNoteType" = {ml r."ProspectNoteType"}, "ProspectNoteDetail" = {ml r."ProspectNoteDetail"}, "IsDefault" = {ml r."IsDefault"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectId" = {ml r."ProspectId"} AND "ProspectNoteId" = {ml r."ProspectNoteId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Note',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Prospect_Note"
WHERE "ProspectId" = {ml r."ProspectId"} AND "ProspectNoteId" = {ml r."ProspectNoteId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :Prospect_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Master',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Prospect_Master"."ProspectId"
,"BUSDTA"."Prospect_Master"."RouteId"
,"BUSDTA"."Prospect_Master"."ProspectName"
,"BUSDTA"."Prospect_Master"."AcctSegmentType"
,"BUSDTA"."Prospect_Master"."BuyingGroup"
,"BUSDTA"."Prospect_Master"."LocationCount"
,"BUSDTA"."Prospect_Master"."CategoryCode01"
,"BUSDTA"."Prospect_Master"."OperatingUnit"
,"BUSDTA"."Prospect_Master"."Branch"
,"BUSDTA"."Prospect_Master"."District"
,"BUSDTA"."Prospect_Master"."Region"
,"BUSDTA"."Prospect_Master"."Reporting"
,"BUSDTA"."Prospect_Master"."Chain"
,"BUSDTA"."Prospect_Master"."BrewmaticAgentCode"
,"BUSDTA"."Prospect_Master"."NTR"
,"BUSDTA"."Prospect_Master"."ProspectTaxGrp"
,"BUSDTA"."Prospect_Master"."CategoryCode12"
,"BUSDTA"."Prospect_Master"."APCheckCode"
,"BUSDTA"."Prospect_Master"."CategoryCode14"
,"BUSDTA"."Prospect_Master"."CategoryCode15"
,"BUSDTA"."Prospect_Master"."CategoryCode16"
,"BUSDTA"."Prospect_Master"."CategoryCode17"
,"BUSDTA"."Prospect_Master"."CategoryCode18"
,"BUSDTA"."Prospect_Master"."CategoryCode19"
,"BUSDTA"."Prospect_Master"."CategoryCode20"
,"BUSDTA"."Prospect_Master"."CategoryCode21"
,"BUSDTA"."Prospect_Master"."CategoryCode22"
,"BUSDTA"."Prospect_Master"."SpecialEquipment"
,"BUSDTA"."Prospect_Master"."CAProtection"
,"BUSDTA"."Prospect_Master"."ProspectGroup"
,"BUSDTA"."Prospect_Master"."SPCommisFBType"
,"BUSDTA"."Prospect_Master"."AlliedDiscount"
,"BUSDTA"."Prospect_Master"."CoffeeVolume"
,"BUSDTA"."Prospect_Master"."EquipmentProgPts"
,"BUSDTA"."Prospect_Master"."LiquidCoffee"
,"BUSDTA"."Prospect_Master"."PriceProtection"
,"BUSDTA"."Prospect_Master"."POSUpCharge"
,"BUSDTA"."Prospect_Master"."SpecialCCP"
,"BUSDTA"."Prospect_Master"."TaxGroup"
,"BUSDTA"."Prospect_Master"."IsPricesetup"
,"BUSDTA"."Prospect_Master"."CreatedBy"
,"BUSDTA"."Prospect_Master"."CreatedDatetime"
,"BUSDTA"."Prospect_Master"."UpdatedBy"
,"BUSDTA"."Prospect_Master"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Master"
WHERE "BUSDTA"."Prospect_Master"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Master"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username}) 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Master',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Prospect_Master_del"."ProspectId"

FROM "BUSDTA"."Prospect_Master_del"
WHERE "BUSDTA"."Prospect_Master_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Master',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Prospect_Master"
("ProspectId", "RouteId", "ProspectName", "AcctSegmentType", "BuyingGroup", "LocationCount", "CategoryCode01", "OperatingUnit", "Branch", "District", "Region", "Reporting", "Chain", "BrewmaticAgentCode", "NTR", "ProspectTaxGrp", "CategoryCode12", "APCheckCode", "CategoryCode14", "CategoryCode15", "CategoryCode16", "CategoryCode17", "CategoryCode18", "CategoryCode19", "CategoryCode20", "CategoryCode21", "CategoryCode22", "SpecialEquipment", "CAProtection", "ProspectGroup", "SPCommisFBType", "AlliedDiscount", "CoffeeVolume", "EquipmentProgPts", "LiquidCoffee", "PriceProtection", "POSUpCharge", "SpecialCCP", "TaxGroup", "IsPricesetup", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ProspectId"}, {ml r."RouteId"}, {ml r."ProspectName"}, {ml r."AcctSegmentType"}, {ml r."BuyingGroup"}, {ml r."LocationCount"}, {ml r."CategoryCode01"}, {ml r."OperatingUnit"}, {ml r."Branch"}, {ml r."District"}, {ml r."Region"}, {ml r."Reporting"}, {ml r."Chain"}, {ml r."BrewmaticAgentCode"}, {ml r."NTR"}, {ml r."ProspectTaxGrp"}, {ml r."CategoryCode12"}, {ml r."APCheckCode"}, {ml r."CategoryCode14"}, {ml r."CategoryCode15"}, {ml r."CategoryCode16"}, {ml r."CategoryCode17"}, {ml r."CategoryCode18"}, {ml r."CategoryCode19"}, {ml r."CategoryCode20"}, {ml r."CategoryCode21"}, {ml r."CategoryCode22"}, {ml r."SpecialEquipment"}, {ml r."CAProtection"}, {ml r."ProspectGroup"}, {ml r."SPCommisFBType"}, {ml r."AlliedDiscount"}, {ml r."CoffeeVolume"}, {ml r."EquipmentProgPts"}, {ml r."LiquidCoffee"}, {ml r."PriceProtection"}, {ml r."POSUpCharge"}, {ml r."SpecialCCP"}, {ml r."TaxGroup"}, {ml r."IsPricesetup"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Master',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Master"
SET "RouteId" = {ml r."RouteId"}, "ProspectName" = {ml r."ProspectName"}, "AcctSegmentType" = {ml r."AcctSegmentType"}, 
"BuyingGroup" = {ml r."BuyingGroup"}, "LocationCount" = {ml r."LocationCount"}, 
"CategoryCode01" = {ml r."CategoryCode01"}, "OperatingUnit" = {ml r."OperatingUnit"}, "Branch" = {ml r."Branch"}, 
"District" = {ml r."District"}, "Region" = {ml r."Region"}, "Reporting" = {ml r."Reporting"}, "Chain" = {ml r."Chain"}, "BrewmaticAgentCode" = {ml r."BrewmaticAgentCode"}, "NTR" = {ml r."NTR"}, "ProspectTaxGrp" = {ml r."ProspectTaxGrp"}, "CategoryCode12" = {ml r."CategoryCode12"}, "APCheckCode" = {ml r."APCheckCode"}, "CategoryCode14" = {ml r."CategoryCode14"}, "CategoryCode15" = {ml r."CategoryCode15"}, "CategoryCode16" = {ml r."CategoryCode16"}, "CategoryCode17" = {ml r."CategoryCode17"}, "CategoryCode18" = {ml r."CategoryCode18"}, "CategoryCode19" = {ml r."CategoryCode19"}, "CategoryCode20" = {ml r."CategoryCode20"}, "CategoryCode21" = {ml r."CategoryCode21"}, "CategoryCode22" = {ml r."CategoryCode22"}, "SpecialEquipment" = {ml r."SpecialEquipment"}, "CAProtection" = {ml r."CAProtection"}, "ProspectGroup" = {ml r."ProspectGroup"}, "SPCommisFBType" = {ml r."SPCommisFBType"}, "AlliedDiscount" = {ml r."AlliedDiscount"}, "CoffeeVolume" = {ml r."CoffeeVolume"}, "EquipmentProgPts" = {ml r."EquipmentProgPts"}, "LiquidCoffee" = {ml r."LiquidCoffee"}, "PriceProtection" = {ml r."PriceProtection"}, "POSUpCharge" = {ml r."POSUpCharge"}, "SpecialCCP" = {ml r."SpecialCCP"}, "TaxGroup" = {ml r."TaxGroup"}, "IsPricesetup" = {ml r."IsPricesetup"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectId" = {ml r."ProspectId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Master',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Prospect_Master"
WHERE "ProspectId" = {ml r."ProspectId"}
 
'

/* upload_delete - End */

/* Table :Feature_Code_Mapping */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Feature_Code_Mapping',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."Feature_Code_Mapping"."RequestCode"
,"BUSDTA"."Feature_Code_Mapping"."AuthorizationFormatCode"
,"BUSDTA"."Feature_Code_Mapping"."Feature"
,"BUSDTA"."Feature_Code_Mapping"."RouteId"
,"BUSDTA"."Feature_Code_Mapping"."Description"
,"BUSDTA"."Feature_Code_Mapping"."CreatedBy"
,"BUSDTA"."Feature_Code_Mapping"."CreatedDatetime"
,"BUSDTA"."Feature_Code_Mapping"."UpdatedBy"
,"BUSDTA"."Feature_Code_Mapping"."UpdatedDatetime"

FROM "BUSDTA"."Feature_Code_Mapping"
WHERE "BUSDTA"."Feature_Code_Mapping"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Feature_Code_Mapping',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Feature_Code_Mapping_del"."RequestCode"
,"BUSDTA"."Feature_Code_Mapping_del"."AuthorizationFormatCode"
,"BUSDTA"."Feature_Code_Mapping_del"."Feature"

FROM "BUSDTA"."Feature_Code_Mapping_del"
WHERE "BUSDTA"."Feature_Code_Mapping_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :Customer_Quote_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Detail',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."Customer_Quote_Detail"."CustomerQuoteId"
,"BUSDTA"."Customer_Quote_Detail"."CustomerQuoteDetailId"
,"BUSDTA"."Customer_Quote_Detail"."RouteId"
,"BUSDTA"."Customer_Quote_Detail"."ItemId"
,"BUSDTA"."Customer_Quote_Detail"."QuoteQty"
,"BUSDTA"."Customer_Quote_Detail"."TransactionUM"
,"BUSDTA"."Customer_Quote_Detail"."PricingAmt"
,"BUSDTA"."Customer_Quote_Detail"."PricingUM"
,"BUSDTA"."Customer_Quote_Detail"."TransactionAmt"
,"BUSDTA"."Customer_Quote_Detail"."OtherAmt"
,"BUSDTA"."Customer_Quote_Detail"."OtherUM"
,"BUSDTA"."Customer_Quote_Detail"."CompetitorName"
,"BUSDTA"."Customer_Quote_Detail"."UnitPriceAmt"
,"BUSDTA"."Customer_Quote_Detail"."UnitPriceUM"
,"BUSDTA"."Customer_Quote_Detail"."CreatedBy"
,"BUSDTA"."Customer_Quote_Detail"."CreatedDatetime"
,"BUSDTA"."Customer_Quote_Detail"."UpdatedBy"
,"BUSDTA"."Customer_Quote_Detail"."UpdatedDatetime"

FROM "BUSDTA"."Customer_Quote_Detail"
WHERE "BUSDTA"."Customer_Quote_Detail"."last_modified">= {ml s.last_table_download}
 AND "BUSDTA"."Customer_Quote_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Detail',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Customer_Quote_Detail_del"."CustomerQuoteId"
,"BUSDTA"."Customer_Quote_Detail_del"."CustomerQuoteDetailId"
,"BUSDTA"."Customer_Quote_Detail_del"."RouteId"

FROM "BUSDTA"."Customer_Quote_Detail_del"
WHERE "BUSDTA"."Customer_Quote_Detail_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Detail',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Customer_Quote_Detail"
("CustomerQuoteId", "CustomerQuoteDetailId", "RouteId", "ItemId", "QuoteQty", "TransactionUM", "PricingAmt", "PricingUM", "TransactionAmt", "OtherAmt", "OtherUM", "CompetitorName", "UnitPriceAmt", "UnitPriceUM", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CustomerQuoteId"}, {ml r."CustomerQuoteDetailId"}, {ml r."RouteId"}, {ml r."ItemId"}, {ml r."QuoteQty"}, {ml r."TransactionUM"}, {ml r."PricingAmt"}, {ml r."PricingUM"}, {ml r."TransactionAmt"}, {ml r."OtherAmt"}, {ml r."OtherUM"}, {ml r."CompetitorName"}, {ml r."UnitPriceAmt"}, {ml r."UnitPriceUM"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Detail',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Customer_Quote_Detail"
SET "ItemId" = {ml r."ItemId"}, "QuoteQty" = {ml r."QuoteQty"}, "TransactionUM" = {ml r."TransactionUM"}, "PricingAmt" = {ml r."PricingAmt"}, "PricingUM" = {ml r."PricingUM"}, "TransactionAmt" = {ml r."TransactionAmt"}, "OtherAmt" = {ml r."OtherAmt"}, "OtherUM" = {ml r."OtherUM"}, "CompetitorName" = {ml r."CompetitorName"}, "UnitPriceAmt" = {ml r."UnitPriceAmt"}, "UnitPriceUM" = {ml r."UnitPriceUM"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CustomerQuoteId" = {ml r."CustomerQuoteId"} AND "CustomerQuoteDetailId" = {ml r."CustomerQuoteDetailId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Detail',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Customer_Quote_Detail"
WHERE "CustomerQuoteId" = {ml r."CustomerQuoteId"} AND "CustomerQuoteDetailId" = {ml r."CustomerQuoteDetailId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :Amount_Range_Index */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Amount_Range_Index',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Amount_Range_Index"."RequestCode"
,"BUSDTA"."Amount_Range_Index"."AuthorizationFormatCode"
,"BUSDTA"."Amount_Range_Index"."RangeStart"
,"BUSDTA"."Amount_Range_Index"."RangeEnd"
,"BUSDTA"."Amount_Range_Index"."RouteId"
,"BUSDTA"."Amount_Range_Index"."CreatedBy"
,"BUSDTA"."Amount_Range_Index"."CreatedDatetime"
,"BUSDTA"."Amount_Range_Index"."UpdatedBy"
,"BUSDTA"."Amount_Range_Index"."UpdatedDatetime"

FROM "BUSDTA"."Amount_Range_Index"
WHERE "BUSDTA"."Amount_Range_Index"."last_modified">= {ml s.last_table_download}
 

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Amount_Range_Index',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."Amount_Range_Index_del"."RequestCode"
,"BUSDTA"."Amount_Range_Index_del"."AuthorizationFormatCode"

FROM "BUSDTA"."Amount_Range_Index_del"
WHERE "BUSDTA"."Amount_Range_Index_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */

/* Table :Customer_Quote_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Header',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."Customer_Quote_Header"."CustomerQuoteId"
,"BUSDTA"."Customer_Quote_Header"."CustomerId"
,"BUSDTA"."Customer_Quote_Header"."RouteId"
,"BUSDTA"."Customer_Quote_Header"."StatusId"
,"BUSDTA"."Customer_Quote_Header"."BillToId"
,"BUSDTA"."Customer_Quote_Header"."ParentId"
,"BUSDTA"."Customer_Quote_Header"."IsPrinted"
,"BUSDTA"."Customer_Quote_Header"."SettlementId"
,"BUSDTA"."Customer_Quote_Header"."IsSampled"
,"BUSDTA"."Customer_Quote_Header"."PickStatus"
,"BUSDTA"."Customer_Quote_Header"."IsMasterSetup"
,"BUSDTA"."Customer_Quote_Header"."AIAC05District"
,"BUSDTA"."Customer_Quote_Header"."AIAC05DistrictMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC06Region"
,"BUSDTA"."Customer_Quote_Header"."AIAC06RegionMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC07Reporting"
,"BUSDTA"."Customer_Quote_Header"."AIAC07ReportingMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC08Chain"
,"BUSDTA"."Customer_Quote_Header"."AIAC08ChainMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC09BrewmaticAgentCode"
,"BUSDTA"."Customer_Quote_Header"."AIAC09BrewmaticAgentCodeMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC10NTR"
,"BUSDTA"."Customer_Quote_Header"."AIAC10NTRMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC11CustomerTaxGrp"
,"BUSDTA"."Customer_Quote_Header"."AIAC11CustomerTaxGrpMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC12CategoryCode12"
,"BUSDTA"."Customer_Quote_Header"."AIAC12CategoryCode12Mst"
,"BUSDTA"."Customer_Quote_Header"."AIAC13APCheckCode"
,"BUSDTA"."Customer_Quote_Header"."AIAC13APCheckCodeMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC14CategoryCode14"
,"BUSDTA"."Customer_Quote_Header"."AIAC14CategoryCode14Mst"
,"BUSDTA"."Customer_Quote_Header"."AIAC15CategoryCode15"
,"BUSDTA"."Customer_Quote_Header"."AIAC15CategoryCode15Mst"
,"BUSDTA"."Customer_Quote_Header"."AIAC16CategoryCode16"
,"BUSDTA"."Customer_Quote_Header"."AIAC16CategoryCode16Mst"
,"BUSDTA"."Customer_Quote_Header"."AIAC17CategoryCode17"
,"BUSDTA"."Customer_Quote_Header"."AIAC17CategoryCode17Mst"
,"BUSDTA"."Customer_Quote_Header"."AIAC18CategoryCode18"
,"BUSDTA"."Customer_Quote_Header"."AIAC18CategoryCode18Mst"
,"BUSDTA"."Customer_Quote_Header"."AIAC22POSUpCharge"
,"BUSDTA"."Customer_Quote_Header"."AIAC22POSUpChargeMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC23LiquidCoffee"
,"BUSDTA"."Customer_Quote_Header"."AIAC23LiquidCoffeeMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC24PriceProtection"
,"BUSDTA"."Customer_Quote_Header"."AIAC24PriceProtectionMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC27AlliedDiscount"
,"BUSDTA"."Customer_Quote_Header"."AIAC27AlliedDiscountMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC28CoffeeVolume"
,"BUSDTA"."Customer_Quote_Header"."AIAC28CoffeeVolumeMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC29EquipmentProgPts"
,"BUSDTA"."Customer_Quote_Header"."AIAC29EquipmentProgPtsMst"
,"BUSDTA"."Customer_Quote_Header"."AIAC30SpecialCCP"
,"BUSDTA"."Customer_Quote_Header"."AIAC30SpecialCCPMst"
,"BUSDTA"."Customer_Quote_Header"."PriceDate"
,"BUSDTA"."Customer_Quote_Header"."CreatedBy"
,"BUSDTA"."Customer_Quote_Header"."CreatedDatetime"
,"BUSDTA"."Customer_Quote_Header"."UpdatedBy"
,"BUSDTA"."Customer_Quote_Header"."UpdatedDatetime"

FROM "BUSDTA"."Customer_Quote_Header"
WHERE "BUSDTA"."Customer_Quote_Header"."last_modified">= {ml s.last_table_download}
  and "BUSDTA"."Customer_Quote_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Header',
	'download_delete_cursor',
	'sql',

'
 
 
SELECT "BUSDTA"."Customer_Quote_Header_del"."CustomerQuoteId"

FROM "BUSDTA"."Customer_Quote_Header_del"
WHERE "BUSDTA"."Customer_Quote_Header_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Header',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Customer_Quote_Header"
("CustomerQuoteId", "CustomerId", "RouteId", "StatusId", "BillToId", "ParentId", "IsPrinted", "SettlementId", "IsSampled", "PickStatus", "IsMasterSetup", "AIAC05District", "AIAC05DistrictMst", "AIAC06Region", "AIAC06RegionMst", "AIAC07Reporting", "AIAC07ReportingMst", "AIAC08Chain", "AIAC08ChainMst", "AIAC09BrewmaticAgentCode", "AIAC09BrewmaticAgentCodeMst", "AIAC10NTR", "AIAC10NTRMst", "AIAC11CustomerTaxGrp", "AIAC11CustomerTaxGrpMst", "AIAC12CategoryCode12", "AIAC12CategoryCode12Mst", "AIAC13APCheckCode", "AIAC13APCheckCodeMst", "AIAC14CategoryCode14", "AIAC14CategoryCode14Mst", "AIAC15CategoryCode15", "AIAC15CategoryCode15Mst", "AIAC16CategoryCode16", "AIAC16CategoryCode16Mst", "AIAC17CategoryCode17", "AIAC17CategoryCode17Mst", "AIAC18CategoryCode18", "AIAC18CategoryCode18Mst", "AIAC22POSUpCharge", "AIAC22POSUpChargeMst", "AIAC23LiquidCoffee", "AIAC23LiquidCoffeeMst", "AIAC24PriceProtection", "AIAC24PriceProtectionMst", "AIAC27AlliedDiscount", "AIAC27AlliedDiscountMst", "AIAC28CoffeeVolume", "AIAC28CoffeeVolumeMst", "AIAC29EquipmentProgPts", "AIAC29EquipmentProgPtsMst", "AIAC30SpecialCCP", "AIAC30SpecialCCPMst", "PriceDate", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CustomerQuoteId"}, {ml r."CustomerId"}, {ml r."RouteId"}, {ml r."StatusId"}, {ml r."BillToId"}, {ml r."ParentId"}, {ml r."IsPrinted"}, {ml r."SettlementId"}, {ml r."IsSampled"}, {ml r."PickStatus"}, {ml r."IsMasterSetup"}, {ml r."AIAC05District"}, {ml r."AIAC05DistrictMst"}, {ml r."AIAC06Region"}, {ml r."AIAC06RegionMst"}, {ml r."AIAC07Reporting"}, {ml r."AIAC07ReportingMst"}, {ml r."AIAC08Chain"}, {ml r."AIAC08ChainMst"}, {ml r."AIAC09BrewmaticAgentCode"}, {ml r."AIAC09BrewmaticAgentCodeMst"}, {ml r."AIAC10NTR"}, {ml r."AIAC10NTRMst"}, {ml r."AIAC11CustomerTaxGrp"}, {ml r."AIAC11CustomerTaxGrpMst"}, {ml r."AIAC12CategoryCode12"}, {ml r."AIAC12CategoryCode12Mst"}, {ml r."AIAC13APCheckCode"}, {ml r."AIAC13APCheckCodeMst"}, {ml r."AIAC14CategoryCode14"}, {ml r."AIAC14CategoryCode14Mst"}, {ml r."AIAC15CategoryCode15"}, {ml r."AIAC15CategoryCode15Mst"}, {ml r."AIAC16CategoryCode16"}, {ml r."AIAC16CategoryCode16Mst"}, {ml r."AIAC17CategoryCode17"}, {ml r."AIAC17CategoryCode17Mst"}, {ml r."AIAC18CategoryCode18"}, {ml r."AIAC18CategoryCode18Mst"}, {ml r."AIAC22POSUpCharge"}, {ml r."AIAC22POSUpChargeMst"}, {ml r."AIAC23LiquidCoffee"}, {ml r."AIAC23LiquidCoffeeMst"}, {ml r."AIAC24PriceProtection"}, {ml r."AIAC24PriceProtectionMst"}, {ml r."AIAC27AlliedDiscount"}, {ml r."AIAC27AlliedDiscountMst"}, {ml r."AIAC28CoffeeVolume"}, {ml r."AIAC28CoffeeVolumeMst"}, {ml r."AIAC29EquipmentProgPts"}, {ml r."AIAC29EquipmentProgPtsMst"}, {ml r."AIAC30SpecialCCP"}, {ml r."AIAC30SpecialCCPMst"}, {ml r."PriceDate"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Header',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Customer_Quote_Header"
SET "CustomerId" = {ml r."CustomerId"}, "RouteId" = {ml r."RouteId"}, "StatusId" = {ml r."StatusId"}, "BillToId" = {ml r."BillToId"}, "ParentId" = {ml r."ParentId"}, "IsPrinted" = {ml r."IsPrinted"}, "SettlementId" = {ml r."SettlementId"}, "IsSampled" = {ml r."IsSampled"}, "PickStatus" = {ml r."PickStatus"}, "IsMasterSetup" = {ml r."IsMasterSetup"}, "AIAC05District" = {ml r."AIAC05District"}, "AIAC05DistrictMst" = {ml r."AIAC05DistrictMst"}, "AIAC06Region" = {ml r."AIAC06Region"}, "AIAC06RegionMst" = {ml r."AIAC06RegionMst"}, "AIAC07Reporting" = {ml r."AIAC07Reporting"}, "AIAC07ReportingMst" = {ml r."AIAC07ReportingMst"}, "AIAC08Chain" = {ml r."AIAC08Chain"}, "AIAC08ChainMst" = {ml r."AIAC08ChainMst"}, "AIAC09BrewmaticAgentCode" = {ml r."AIAC09BrewmaticAgentCode"}, "AIAC09BrewmaticAgentCodeMst" = {ml r."AIAC09BrewmaticAgentCodeMst"}, "AIAC10NTR" = {ml r."AIAC10NTR"}, "AIAC10NTRMst" = {ml r."AIAC10NTRMst"}, "AIAC11CustomerTaxGrp" = {ml r."AIAC11CustomerTaxGrp"}, "AIAC11CustomerTaxGrpMst" = {ml r."AIAC11CustomerTaxGrpMst"}, "AIAC12CategoryCode12" = {ml r."AIAC12CategoryCode12"}, "AIAC12CategoryCode12Mst" = {ml r."AIAC12CategoryCode12Mst"}, "AIAC13APCheckCode" = {ml r."AIAC13APCheckCode"}, "AIAC13APCheckCodeMst" = {ml r."AIAC13APCheckCodeMst"}, "AIAC14CategoryCode14" = {ml r."AIAC14CategoryCode14"}, "AIAC14CategoryCode14Mst" = {ml r."AIAC14CategoryCode14Mst"}, "AIAC15CategoryCode15" = {ml r."AIAC15CategoryCode15"}, "AIAC15CategoryCode15Mst" = {ml r."AIAC15CategoryCode15Mst"}, "AIAC16CategoryCode16" = {ml r."AIAC16CategoryCode16"}, "AIAC16CategoryCode16Mst" = {ml r."AIAC16CategoryCode16Mst"}, "AIAC17CategoryCode17" = {ml r."AIAC17CategoryCode17"}, "AIAC17CategoryCode17Mst" = {ml r."AIAC17CategoryCode17Mst"}, "AIAC18CategoryCode18" = {ml r."AIAC18CategoryCode18"}, "AIAC18CategoryCode18Mst" = {ml r."AIAC18CategoryCode18Mst"}, "AIAC22POSUpCharge" = {ml r."AIAC22POSUpCharge"}, "AIAC22POSUpChargeMst" = {ml r."AIAC22POSUpChargeMst"}, "AIAC23LiquidCoffee" = {ml r."AIAC23LiquidCoffee"}, "AIAC23LiquidCoffeeMst" = {ml r."AIAC23LiquidCoffeeMst"}, "AIAC24PriceProtection" = {ml r."AIAC24PriceProtection"}, "AIAC24PriceProtectionMst" = {ml r."AIAC24PriceProtectionMst"}, "AIAC27AlliedDiscount" = {ml r."AIAC27AlliedDiscount"}, "AIAC27AlliedDiscountMst" = {ml r."AIAC27AlliedDiscountMst"}, "AIAC28CoffeeVolume" = {ml r."AIAC28CoffeeVolume"}, "AIAC28CoffeeVolumeMst" = {ml r."AIAC28CoffeeVolumeMst"}, "AIAC29EquipmentProgPts" = {ml r."AIAC29EquipmentProgPts"}, "AIAC29EquipmentProgPtsMst" = {ml r."AIAC29EquipmentProgPtsMst"}, "AIAC30SpecialCCP" = {ml r."AIAC30SpecialCCP"}, "AIAC30SpecialCCPMst" = {ml r."AIAC30SpecialCCPMst"}, "PriceDate" = {ml r."PriceDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CustomerQuoteId" = {ml r."CustomerQuoteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Quote_Header',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Customer_Quote_Header"
WHERE "CustomerQuoteId" = {ml r."CustomerQuoteId"}
 

'

/* upload_delete - End */

/* Table :F0004 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0004',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."F0004"."DTSY"
,"BUSDTA"."F0004"."DTRT"
,"BUSDTA"."F0004"."DTDL01"
,"BUSDTA"."F0004"."DTCDL"
,"BUSDTA"."F0004"."DTLN2"
,"BUSDTA"."F0004"."DTCNUM"
,"BUSDTA"."F0004"."DTYN"

FROM "BUSDTA"."F0004"
WHERE "BUSDTA"."F0004"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0004',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0004_del"."DTSY"
,"BUSDTA"."F0004_del"."DTRT"

FROM "BUSDTA"."F0004_del"
WHERE "BUSDTA"."F0004_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :F0005 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0005',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F0005"."DRSY"  ,"BUSDTA"."F0005"."DRRT"  ,"BUSDTA"."F0005"."DRKY"  ,"BUSDTA"."F0005"."DRDL01"  ,
     "BUSDTA"."F0005"."DRDL02"  ,"BUSDTA"."F0005"."DRSPHD"  ,"BUSDTA"."F0005"."DRHRDC"  ,"BUSDTA"."F0005"."DRYN"    
     FROM "BUSDTA"."F0005"  WHERE "BUSDTA"."F0005"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0005',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0005_del"."DRSY"
,"BUSDTA"."F0005_del"."DRRT"
,"BUSDTA"."F0005_del"."DRKY"

FROM "BUSDTA"."F0005_del"
WHERE "BUSDTA"."F0005_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :F0006 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0006',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F0006"."MCMCU"  ,"BUSDTA"."F0006"."MCSTYL"  ,"BUSDTA"."F0006"."MCLDM"  ,"BUSDTA"."F0006"."MCCO"  ,"BUSDTA"."F0006"."MCAN8"  ,
     "BUSDTA"."F0006"."MCDL01"  ,"BUSDTA"."F0006"."MCRP01"  ,"BUSDTA"."F0006"."MCRP02"  ,"BUSDTA"."F0006"."MCRP03"  ,"BUSDTA"."F0006"."MCRP04"  ,
     "BUSDTA"."F0006"."MCRP05"  ,"BUSDTA"."F0006"."MCRP06"  ,"BUSDTA"."F0006"."MCRP07"  ,"BUSDTA"."F0006"."MCRP08"  ,"BUSDTA"."F0006"."MCRP09"  ,
     "BUSDTA"."F0006"."MCRP10"  ,"BUSDTA"."F0006"."MCRP11"  ,"BUSDTA"."F0006"."MCRP12"  ,"BUSDTA"."F0006"."MCRP13"  ,"BUSDTA"."F0006"."MCRP14"  ,
     "BUSDTA"."F0006"."MCRP15"  ,"BUSDTA"."F0006"."MCRP16"  ,"BUSDTA"."F0006"."MCRP17"  ,"BUSDTA"."F0006"."MCRP18"  ,"BUSDTA"."F0006"."MCRP19"  ,
     "BUSDTA"."F0006"."MCRP20"  ,"BUSDTA"."F0006"."MCRP21"  ,"BUSDTA"."F0006"."MCRP22"  ,"BUSDTA"."F0006"."MCRP23"  ,"BUSDTA"."F0006"."MCRP24"  ,
     "BUSDTA"."F0006"."MCRP25"  ,"BUSDTA"."F0006"."MCRP26"  ,"BUSDTA"."F0006"."MCRP27"  ,"BUSDTA"."F0006"."MCRP28"  ,"BUSDTA"."F0006"."MCRP29"  ,
     "BUSDTA"."F0006"."MCRP30"    
     FROM "BUSDTA"."F0006"  
     WHERE "BUSDTA"."F0006"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0006',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0006_del"."MCMCU"

FROM "BUSDTA"."F0006_del"
WHERE "BUSDTA"."F0006_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0006',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0006"
("MCMCU", "MCSTYL", "MCLDM", "MCCO", "MCAN8", "MCDL01", "MCRP01", "MCRP02", "MCRP03", "MCRP04", "MCRP05", "MCRP06", "MCRP07", "MCRP08", "MCRP09", "MCRP10", "MCRP11", "MCRP12", "MCRP13", "MCRP14", "MCRP15", "MCRP16", "MCRP17", "MCRP18", "MCRP19", "MCRP20", "MCRP21", "MCRP22", "MCRP23", "MCRP24", "MCRP25", "MCRP26", "MCRP27", "MCRP28", "MCRP29", "MCRP30")
VALUES({ml r."MCMCU"}, {ml r."MCSTYL"}, {ml r."MCLDM"}, {ml r."MCCO"}, {ml r."MCAN8"}, {ml r."MCDL01"}, {ml r."MCRP01"}, {ml r."MCRP02"}, {ml r."MCRP03"}, {ml r."MCRP04"}, {ml r."MCRP05"}, {ml r."MCRP06"}, {ml r."MCRP07"}, {ml r."MCRP08"}, {ml r."MCRP09"}, {ml r."MCRP10"}, {ml r."MCRP11"}, {ml r."MCRP12"}, {ml r."MCRP13"}, {ml r."MCRP14"}, {ml r."MCRP15"}, {ml r."MCRP16"}, {ml r."MCRP17"}, {ml r."MCRP18"}, {ml r."MCRP19"}, {ml r."MCRP20"}, {ml r."MCRP21"}, {ml r."MCRP22"}, {ml r."MCRP23"}, {ml r."MCRP24"}, {ml r."MCRP25"}, {ml r."MCRP26"}, {ml r."MCRP27"}, {ml r."MCRP28"}, {ml r."MCRP29"}, {ml r."MCRP30"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0006',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0006"
SET "MCSTYL" = {ml r."MCSTYL"}, "MCLDM" = {ml r."MCLDM"}, "MCCO" = {ml r."MCCO"}, "MCAN8" = {ml r."MCAN8"}, "MCDL01" = {ml r."MCDL01"}, "MCRP01" = {ml r."MCRP01"}, "MCRP02" = {ml r."MCRP02"}, "MCRP03" = {ml r."MCRP03"}, "MCRP04" = {ml r."MCRP04"}, "MCRP05" = {ml r."MCRP05"}, "MCRP06" = {ml r."MCRP06"}, "MCRP07" = {ml r."MCRP07"}, "MCRP08" = {ml r."MCRP08"}, "MCRP09" = {ml r."MCRP09"}, "MCRP10" = {ml r."MCRP10"}, "MCRP11" = {ml r."MCRP11"}, "MCRP12" = {ml r."MCRP12"}, "MCRP13" = {ml r."MCRP13"}, "MCRP14" = {ml r."MCRP14"}, "MCRP15" = {ml r."MCRP15"}, "MCRP16" = {ml r."MCRP16"}, "MCRP17" = {ml r."MCRP17"}, "MCRP18" = {ml r."MCRP18"}, "MCRP19" = {ml r."MCRP19"}, "MCRP20" = {ml r."MCRP20"}, "MCRP21" = {ml r."MCRP21"}, "MCRP22" = {ml r."MCRP22"}, "MCRP23" = {ml r."MCRP23"}, "MCRP24" = {ml r."MCRP24"}, "MCRP25" = {ml r."MCRP25"}, "MCRP26" = {ml r."MCRP26"}, "MCRP27" = {ml r."MCRP27"}, "MCRP28" = {ml r."MCRP28"}, "MCRP29" = {ml r."MCRP29"}, "MCRP30" = {ml r."MCRP30"}
WHERE "MCMCU" = {ml r."MCMCU"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0006',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0006"
WHERE "MCMCU" = {ml r."MCMCU"}
 

'

/* upload_delete - End */

/* Table :Customer_Route_Map */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Route_Map',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Customer_Route_Map"."BranchNumber"  ,"BUSDTA"."Customer_Route_Map"."CustomerShipToNumber"  ,
"BUSDTA"."Customer_Route_Map"."RelationshipType"  ,"BUSDTA"."Customer_Route_Map"."RelatedAddressBookNumber"  ,
"BUSDTA"."Customer_Route_Map"."IsActive"  ,"BUSDTA"."Customer_Route_Map"."CreatedBy"  ,
"BUSDTA"."Customer_Route_Map"."CreatedDatetime"  ,"BUSDTA"."Customer_Route_Map"."UpdatedBy"  ,
"BUSDTA"."Customer_Route_Map"."UpdatedDatetime"    
FROM "BUSDTA"."Customer_Route_Map", BUSDTA.Route_Master rm  
WHERE "BUSDTA"."Customer_Route_Map"."last_modified">= {ml s.last_table_download}   
AND "BUSDTA"."Customer_Route_Map"."BranchNumber" = rm.BranchNumber   
AND rm.RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )    
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Route_Map',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Customer_Route_Map_del"."BranchNumber"
,"BUSDTA"."Customer_Route_Map_del"."CustomerShipToNumber"
,"BUSDTA"."Customer_Route_Map_del"."RelationshipType"

FROM "BUSDTA"."Customer_Route_Map_del"
WHERE "BUSDTA"."Customer_Route_Map_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Route_Map',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Customer_Route_Map"
("BranchNumber", "CustomerShipToNumber", "RelationshipType", "RelatedAddressBookNumber", "IsActive", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."BranchNumber"}, {ml r."CustomerShipToNumber"}, {ml r."RelationshipType"}, {ml r."RelatedAddressBookNumber"}, {ml r."IsActive"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Route_Map',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Customer_Route_Map"
SET "RelatedAddressBookNumber" = {ml r."RelatedAddressBookNumber"}, "IsActive" = {ml r."IsActive"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "BranchNumber" = {ml r."BranchNumber"} AND "CustomerShipToNumber" = {ml r."CustomerShipToNumber"} AND "RelationshipType" = {ml r."RelationshipType"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Route_Map',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Customer_Route_Map"
WHERE "BranchNumber" = {ml r."BranchNumber"} AND "CustomerShipToNumber" = {ml r."CustomerShipToNumber"} AND "RelationshipType" = {ml r."RelationshipType"}
 

'

/* upload_delete - End */

/* Table :Prospect_Address */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Address',
	'download_cursor',
	'sql',

'


 
SELECT "BUSDTA"."Prospect_Address"."ProspectId"
,"BUSDTA"."Prospect_Address"."RouteId"
,"BUSDTA"."Prospect_Address"."AddressLine1"
,"BUSDTA"."Prospect_Address"."AddressLine2"
,"BUSDTA"."Prospect_Address"."AddressLine3"
,"BUSDTA"."Prospect_Address"."AddressLine4"
,"BUSDTA"."Prospect_Address"."CityName"
,"BUSDTA"."Prospect_Address"."StateName"
,"BUSDTA"."Prospect_Address"."ZipCode"
,"BUSDTA"."Prospect_Address"."CreatedBy"
,"BUSDTA"."Prospect_Address"."CreatedDatetime"
,"BUSDTA"."Prospect_Address"."UpdatedBy"
,"BUSDTA"."Prospect_Address"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Address"
WHERE "BUSDTA"."Prospect_Address"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Prospect_Address"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Address',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Prospect_Address_del"."ProspectId"
,"BUSDTA"."Prospect_Address_del"."RouteId"

FROM "BUSDTA"."Prospect_Address_del"
WHERE "BUSDTA"."Prospect_Address_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Address',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Prospect_Address"
("ProspectId", "RouteId", "AddressLine1", "AddressLine2", "AddressLine3", "AddressLine4", "CityName", "StateName", "ZipCode", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ProspectId"}, {ml r."RouteId"}, {ml r."AddressLine1"}, {ml r."AddressLine2"}, {ml r."AddressLine3"}, {ml r."AddressLine4"}, {ml r."CityName"}, {ml r."StateName"}, {ml r."ZipCode"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Address',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Address"
SET "AddressLine1" = {ml r."AddressLine1"}, "AddressLine2" = {ml r."AddressLine2"}, "AddressLine3" = {ml r."AddressLine3"}, "AddressLine4" = {ml r."AddressLine4"}, "CityName" = {ml r."CityName"}, "StateName" = {ml r."StateName"}, "ZipCode" = {ml r."ZipCode"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectId" = {ml r."ProspectId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Address',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Prospect_Address"
WHERE "ProspectId" = {ml r."ProspectId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :WebServicesRequestDetail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'WebServicesRequestDetail',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :LocalLastItems */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'LocalLastItems',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :WebServicesResponseDetail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'WebServicesResponseDetail',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :Prospect_Contact */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Contact',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Prospect_Contact"."ProspectId"
,"BUSDTA"."Prospect_Contact"."ProspectContactId"
,"BUSDTA"."Prospect_Contact"."RouteId"
,"BUSDTA"."Prospect_Contact"."ShowOnDashboard"
,"BUSDTA"."Prospect_Contact"."FirstName"
,"BUSDTA"."Prospect_Contact"."MiddleName"
,"BUSDTA"."Prospect_Contact"."LastName"
,"BUSDTA"."Prospect_Contact"."Title"
,"BUSDTA"."Prospect_Contact"."IsActive"
,"BUSDTA"."Prospect_Contact"."AreaCode1"
,"BUSDTA"."Prospect_Contact"."Phone1"
,"BUSDTA"."Prospect_Contact"."Extn1"
,"BUSDTA"."Prospect_Contact"."PhoneType1"
,"BUSDTA"."Prospect_Contact"."IsDefaultPhone1"
,"BUSDTA"."Prospect_Contact"."AreaCode2"
,"BUSDTA"."Prospect_Contact"."Phone2"
,"BUSDTA"."Prospect_Contact"."Extn2"
,"BUSDTA"."Prospect_Contact"."PhoneType2"
,"BUSDTA"."Prospect_Contact"."IsDefaultPhone2"
,"BUSDTA"."Prospect_Contact"."AreaCode3"
,"BUSDTA"."Prospect_Contact"."Phone3"
,"BUSDTA"."Prospect_Contact"."Extn3"
,"BUSDTA"."Prospect_Contact"."PhoneType3"
,"BUSDTA"."Prospect_Contact"."IsDefaultPhone3"
,"BUSDTA"."Prospect_Contact"."AreaCode4"
,"BUSDTA"."Prospect_Contact"."Phone4"
,"BUSDTA"."Prospect_Contact"."Extn4"
,"BUSDTA"."Prospect_Contact"."PhoneType4"
,"BUSDTA"."Prospect_Contact"."IsDefaultPhone4"
,"BUSDTA"."Prospect_Contact"."EmailID1"
,"BUSDTA"."Prospect_Contact"."EmailType1"
,"BUSDTA"."Prospect_Contact"."IsDefaultEmail1"
,"BUSDTA"."Prospect_Contact"."EmailID2"
,"BUSDTA"."Prospect_Contact"."EmailType2"
,"BUSDTA"."Prospect_Contact"."IsDefaultEmail2"
,"BUSDTA"."Prospect_Contact"."EmailID3"
,"BUSDTA"."Prospect_Contact"."EmailType3"
,"BUSDTA"."Prospect_Contact"."IsDefaultEmail3"
,"BUSDTA"."Prospect_Contact"."CreatedBy"
,"BUSDTA"."Prospect_Contact"."CreatedDatetime"
,"BUSDTA"."Prospect_Contact"."UpdatedBy"
,"BUSDTA"."Prospect_Contact"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Contact"
WHERE "BUSDTA"."Prospect_Contact"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Prospect_Contact"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Contact',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Prospect_Contact_del"."ProspectId"
,"BUSDTA"."Prospect_Contact_del"."ProspectContactId"
,"BUSDTA"."Prospect_Contact_del"."RouteId"

FROM "BUSDTA"."Prospect_Contact_del"
WHERE "BUSDTA"."Prospect_Contact_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Contact',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Prospect_Contact"
("ProspectId", "ProspectContactId", "RouteId", "ShowOnDashboard", "FirstName", "MiddleName", "LastName", "Title", "IsActive", "AreaCode1", "Phone1", "Extn1", "PhoneType1", "IsDefaultPhone1", "AreaCode2", "Phone2", "Extn2", "PhoneType2", "IsDefaultPhone2", "AreaCode3", "Phone3", "Extn3", "PhoneType3", "IsDefaultPhone3", "AreaCode4", "Phone4", "Extn4", "PhoneType4", "IsDefaultPhone4", "EmailID1", "EmailType1", "IsDefaultEmail1", "EmailID2", "EmailType2", "IsDefaultEmail2", "EmailID3", "EmailType3", "IsDefaultEmail3", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ProspectId"}, {ml r."ProspectContactId"}, {ml r."RouteId"}, {ml r."ShowOnDashboard"}, {ml r."FirstName"}, {ml r."MiddleName"}, {ml r."LastName"}, {ml r."Title"}, {ml r."IsActive"}, {ml r."AreaCode1"}, {ml r."Phone1"}, {ml r."Extn1"}, {ml r."PhoneType1"}, {ml r."IsDefaultPhone1"}, {ml r."AreaCode2"}, {ml r."Phone2"}, {ml r."Extn2"}, {ml r."PhoneType2"}, {ml r."IsDefaultPhone2"}, {ml r."AreaCode3"}, {ml r."Phone3"}, {ml r."Extn3"}, {ml r."PhoneType3"}, {ml r."IsDefaultPhone3"}, {ml r."AreaCode4"}, {ml r."Phone4"}, {ml r."Extn4"}, {ml r."PhoneType4"}, {ml r."IsDefaultPhone4"}, {ml r."EmailID1"}, {ml r."EmailType1"}, {ml r."IsDefaultEmail1"}, {ml r."EmailID2"}, {ml r."EmailType2"}, {ml r."IsDefaultEmail2"}, {ml r."EmailID3"}, {ml r."EmailType3"}, {ml r."IsDefaultEmail3"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Contact',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Contact"
SET "ShowOnDashboard" = {ml r."ShowOnDashboard"}, "FirstName" = {ml r."FirstName"}, "MiddleName" = {ml r."MiddleName"}, "LastName" = {ml r."LastName"}, "Title" = {ml r."Title"}, "IsActive" = {ml r."IsActive"}, "AreaCode1" = {ml r."AreaCode1"}, "Phone1" = {ml r."Phone1"}, "Extn1" = {ml r."Extn1"}, "PhoneType1" = {ml r."PhoneType1"}, "IsDefaultPhone1" = {ml r."IsDefaultPhone1"}, "AreaCode2" = {ml r."AreaCode2"}, "Phone2" = {ml r."Phone2"}, "Extn2" = {ml r."Extn2"}, "PhoneType2" = {ml r."PhoneType2"}, "IsDefaultPhone2" = {ml r."IsDefaultPhone2"}, "AreaCode3" = {ml r."AreaCode3"}, "Phone3" = {ml r."Phone3"}, "Extn3" = {ml r."Extn3"}, "PhoneType3" = {ml r."PhoneType3"}, "IsDefaultPhone3" = {ml r."IsDefaultPhone3"}, "AreaCode4" = {ml r."AreaCode4"}, "Phone4" = {ml r."Phone4"}, "Extn4" = {ml r."Extn4"}, "PhoneType4" = {ml r."PhoneType4"}, "IsDefaultPhone4" = {ml r."IsDefaultPhone4"}, "EmailID1" = {ml r."EmailID1"}, "EmailType1" = {ml r."EmailType1"}, "IsDefaultEmail1" = {ml r."IsDefaultEmail1"}, "EmailID2" = {ml r."EmailID2"}, "EmailType2" = {ml r."EmailType2"}, "IsDefaultEmail2" = {ml r."IsDefaultEmail2"}, "EmailID3" = {ml r."EmailID3"}, "EmailType3" = {ml r."EmailType3"}, "IsDefaultEmail3" = {ml r."IsDefaultEmail3"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectId" = {ml r."ProspectId"} AND "ProspectContactId" = {ml r."ProspectContactId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Prospect_Contact',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Prospect_Contact"
WHERE "ProspectId" = {ml r."ProspectId"} AND "ProspectContactId" = {ml r."ProspectContactId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_delete - End */

/* Table :Category_Type */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Category_Type',
	'download_cursor',
	'sql',

'
  
  SELECT "BUSDTA"."Category_Type"."CategoryTypeID"  ,"BUSDTA"."Category_Type"."CategoryTypeCD"  ,
  "BUSDTA"."Category_Type"."CategoryCodeType"  ,"BUSDTA"."Category_Type"."CategoryTypeDESC"  ,
  "BUSDTA"."Category_Type"."CreatedBy"  ,"BUSDTA"."Category_Type"."CreatedDatetime"  ,
  "BUSDTA"."Category_Type"."UpdatedBy"  ,"BUSDTA"."Category_Type"."UpdatedDatetime"    
  FROM "BUSDTA"."Category_Type"  WHERE "BUSDTA"."Category_Type"."last_modified">= {ml s.last_table_download}   
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Category_Type',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Category_Type_del"."CategoryTypeCD"

FROM "BUSDTA"."Category_Type_del"
WHERE "BUSDTA"."Category_Type_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Category_Type',
	'upload_insert',
	'sql',

'
--{ml_ignore}
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Category_Type',
	'upload_update',
	'sql',

'
--{ml_ignore}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Category_Type',
	'upload_delete',
	'sql',

'
--{ml_ignore}
'

/* upload_delete - End */

/* Table :F0014 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0014',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F0014"."PNPTC"  ,"BUSDTA"."F0014"."PNPTD"  ,"BUSDTA"."F0014"."PNDCP"  ,"BUSDTA"."F0014"."PNDCD"  ,
     "BUSDTA"."F0014"."PNNDTP"  ,"BUSDTA"."F0014"."PNNSP"  ,"BUSDTA"."F0014"."PNDTPA"  ,"BUSDTA"."F0014"."PNPXDM"  ,
     "BUSDTA"."F0014"."PNPXDD"    FROM "BUSDTA"."F0014"  WHERE "BUSDTA"."F0014"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0014',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0014_del"."PNPTC"

FROM "BUSDTA"."F0014_del"
WHERE "BUSDTA"."F0014_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0014',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0014"
("PNPTC", "PNPTD", "PNDCP", "PNDCD", "PNNDTP", "PNNSP", "PNDTPA", "PNPXDM", "PNPXDD")
VALUES({ml r."PNPTC"}, {ml r."PNPTD"}, {ml r."PNDCP"}, {ml r."PNDCD"}, {ml r."PNNDTP"}, {ml r."PNNSP"}, {ml r."PNDTPA"}, {ml r."PNPXDM"}, {ml r."PNPXDD"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0014',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0014"
SET "PNPTD" = {ml r."PNPTD"}, "PNDCP" = {ml r."PNDCP"}, "PNDCD" = {ml r."PNDCD"}, "PNNDTP" = {ml r."PNNDTP"}, "PNNSP" = {ml r."PNNSP"}, "PNDTPA" = {ml r."PNDTPA"}, "PNPXDM" = {ml r."PNPXDM"}, "PNPXDD" = {ml r."PNPXDD"}
WHERE "PNPTC" = {ml r."PNPTC"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0014',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0014"
WHERE "PNPTC" = {ml r."PNPTC"}
 

'

/* upload_delete - End */

/* Table :F0101 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F0101"."ABAN8"  ,"BUSDTA"."F0101"."ABALKY"  ,"BUSDTA"."F0101"."ABTAX"  ,"BUSDTA"."F0101"."ABALPH"  ,"BUSDTA"."F0101"."ABMCU"  ,
     "BUSDTA"."F0101"."ABSIC"  ,"BUSDTA"."F0101"."ABLNGP"  ,"BUSDTA"."F0101"."ABAT1"  ,"BUSDTA"."F0101"."ABCM"  ,"BUSDTA"."F0101"."ABTAXC"  ,
     "BUSDTA"."F0101"."ABAT2"  ,"BUSDTA"."F0101"."ABAN81"  ,"BUSDTA"."F0101"."ABAN82"  ,"BUSDTA"."F0101"."ABAN83"  ,"BUSDTA"."F0101"."ABAN84"  ,
     "BUSDTA"."F0101"."ABAN86"  ,"BUSDTA"."F0101"."ABAN85"  ,"BUSDTA"."F0101"."ABAC01"  ,"BUSDTA"."F0101"."ABAC02"  ,"BUSDTA"."F0101"."ABAC03"  ,
     "BUSDTA"."F0101"."ABAC04"  ,"BUSDTA"."F0101"."ABAC05"  ,"BUSDTA"."F0101"."ABAC06"  ,"BUSDTA"."F0101"."ABAC07"  ,"BUSDTA"."F0101"."ABAC08"  ,
     "BUSDTA"."F0101"."ABAC09"  ,"BUSDTA"."F0101"."ABAC10"  ,"BUSDTA"."F0101"."ABAC11"  ,"BUSDTA"."F0101"."ABAC12"  ,"BUSDTA"."F0101"."ABAC13"  ,
     "BUSDTA"."F0101"."ABAC14"  ,"BUSDTA"."F0101"."ABAC15"  ,"BUSDTA"."F0101"."ABAC16"  ,"BUSDTA"."F0101"."ABAC17"  ,"BUSDTA"."F0101"."ABAC18"  ,
     "BUSDTA"."F0101"."ABAC19"  ,"BUSDTA"."F0101"."ABAC20"  ,"BUSDTA"."F0101"."ABAC21"  ,"BUSDTA"."F0101"."ABAC22"  ,"BUSDTA"."F0101"."ABAC23"  ,
     "BUSDTA"."F0101"."ABAC24"  ,"BUSDTA"."F0101"."ABAC25"  ,"BUSDTA"."F0101"."ABAC26"  ,"BUSDTA"."F0101"."ABAC27"  ,"BUSDTA"."F0101"."ABAC28"  ,
     "BUSDTA"."F0101"."ABAC29"  ,"BUSDTA"."F0101"."ABAC30"  ,"BUSDTA"."F0101"."ABRMK"  ,"BUSDTA"."F0101"."ABTXCT"  ,"BUSDTA"."F0101"."ABTX2"  ,
     "BUSDTA"."F0101"."ABALP1"   
      FROM "BUSDTA"."F0101", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
      WHERE ("BUSDTA"."F0101"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
      or crm.last_modified >= {ml s.last_table_download}) and   ABAN8=crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber  
      AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )    
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0101_del"."ABAN8"

FROM "BUSDTA"."F0101_del"
WHERE "BUSDTA"."F0101_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0101"
("ABAN8", "ABALKY", "ABTAX", "ABALPH", "ABMCU", "ABSIC", "ABLNGP", "ABAT1", "ABCM", "ABTAXC", "ABAT2", "ABAN81", "ABAN82", "ABAN83", "ABAN84", "ABAN86", "ABAN85", "ABAC01", "ABAC02", "ABAC03", "ABAC04", "ABAC05", "ABAC06", "ABAC07", "ABAC08", "ABAC09", "ABAC10", "ABAC11", "ABAC12", "ABAC13", "ABAC14", "ABAC15", "ABAC16", "ABAC17", "ABAC18", "ABAC19", "ABAC20", "ABAC21", "ABAC22", "ABAC23", "ABAC24", "ABAC25", "ABAC26", "ABAC27", "ABAC28", "ABAC29", "ABAC30", "ABRMK", "ABTXCT", "ABTX2", "ABALP1")
VALUES({ml r."ABAN8"}, {ml r."ABALKY"}, {ml r."ABTAX"}, {ml r."ABALPH"}, {ml r."ABMCU"}, {ml r."ABSIC"}, {ml r."ABLNGP"}, {ml r."ABAT1"}, {ml r."ABCM"}, {ml r."ABTAXC"}, {ml r."ABAT2"}, {ml r."ABAN81"}, {ml r."ABAN82"}, {ml r."ABAN83"}, {ml r."ABAN84"}, {ml r."ABAN86"}, {ml r."ABAN85"}, {ml r."ABAC01"}, {ml r."ABAC02"}, {ml r."ABAC03"}, {ml r."ABAC04"}, {ml r."ABAC05"}, {ml r."ABAC06"}, {ml r."ABAC07"}, {ml r."ABAC08"}, {ml r."ABAC09"}, {ml r."ABAC10"}, {ml r."ABAC11"}, {ml r."ABAC12"}, {ml r."ABAC13"}, {ml r."ABAC14"}, {ml r."ABAC15"}, {ml r."ABAC16"}, {ml r."ABAC17"}, {ml r."ABAC18"}, {ml r."ABAC19"}, {ml r."ABAC20"}, {ml r."ABAC21"}, {ml r."ABAC22"}, {ml r."ABAC23"}, {ml r."ABAC24"}, {ml r."ABAC25"}, {ml r."ABAC26"}, {ml r."ABAC27"}, {ml r."ABAC28"}, {ml r."ABAC29"}, {ml r."ABAC30"}, {ml r."ABRMK"}, {ml r."ABTXCT"}, {ml r."ABTX2"}, {ml r."ABALP1"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0101"
SET "ABALKY" = {ml r."ABALKY"}, "ABTAX" = {ml r."ABTAX"}, "ABALPH" = {ml r."ABALPH"}, "ABMCU" = {ml r."ABMCU"}, "ABSIC" = {ml r."ABSIC"}, "ABLNGP" = {ml r."ABLNGP"}, "ABAT1" = {ml r."ABAT1"}, "ABCM" = {ml r."ABCM"}, "ABTAXC" = {ml r."ABTAXC"}, "ABAT2" = {ml r."ABAT2"}, "ABAN81" = {ml r."ABAN81"}, "ABAN82" = {ml r."ABAN82"}, "ABAN83" = {ml r."ABAN83"}, "ABAN84" = {ml r."ABAN84"}, "ABAN86" = {ml r."ABAN86"}, "ABAN85" = {ml r."ABAN85"}, "ABAC01" = {ml r."ABAC01"}, "ABAC02" = {ml r."ABAC02"}, "ABAC03" = {ml r."ABAC03"}, "ABAC04" = {ml r."ABAC04"}, "ABAC05" = {ml r."ABAC05"}, "ABAC06" = {ml r."ABAC06"}, "ABAC07" = {ml r."ABAC07"}, "ABAC08" = {ml r."ABAC08"}, "ABAC09" = {ml r."ABAC09"}, "ABAC10" = {ml r."ABAC10"}, "ABAC11" = {ml r."ABAC11"}, "ABAC12" = {ml r."ABAC12"}, "ABAC13" = {ml r."ABAC13"}, "ABAC14" = {ml r."ABAC14"}, "ABAC15" = {ml r."ABAC15"}, "ABAC16" = {ml r."ABAC16"}, "ABAC17" = {ml r."ABAC17"}, "ABAC18" = {ml r."ABAC18"}, "ABAC19" = {ml r."ABAC19"}, "ABAC20" = {ml r."ABAC20"}, "ABAC21" = {ml r."ABAC21"}, "ABAC22" = {ml r."ABAC22"}, "ABAC23" = {ml r."ABAC23"}, "ABAC24" = {ml r."ABAC24"}, "ABAC25" = {ml r."ABAC25"}, "ABAC26" = {ml r."ABAC26"}, "ABAC27" = {ml r."ABAC27"}, "ABAC28" = {ml r."ABAC28"}, "ABAC29" = {ml r."ABAC29"}, "ABAC30" = {ml r."ABAC30"}, "ABRMK" = {ml r."ABRMK"}, "ABTXCT" = {ml r."ABTXCT"}, "ABTX2" = {ml r."ABTX2"}, "ABALP1" = {ml r."ABALP1"}
WHERE "ABAN8" = {ml r."ABAN8"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0101"
WHERE "ABAN8" = {ml r."ABAN8"}
 

'

/* upload_delete - End */

/* Table :F0111 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0111',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F0111"."WWAN8"  ,"BUSDTA"."F0111"."WWIDLN"  ,"BUSDTA"."F0111"."WWDSS5"  ,"BUSDTA"."F0111"."WWMLNM"  ,
     "BUSDTA"."F0111"."WWATTL"  ,"BUSDTA"."F0111"."WWREM1"  ,"BUSDTA"."F0111"."WWSLNM"  ,"BUSDTA"."F0111"."WWALPH"  ,"BUSDTA"."F0111"."WWDC"  ,
     "BUSDTA"."F0111"."WWGNNM"  ,"BUSDTA"."F0111"."WWMDNM"  ,"BUSDTA"."F0111"."WWSRNM"  ,"BUSDTA"."F0111"."WWTYC"  ,"BUSDTA"."F0111"."WWW001"  ,
     "BUSDTA"."F0111"."WWW002"  ,"BUSDTA"."F0111"."WWW003"  ,"BUSDTA"."F0111"."WWW004"  ,"BUSDTA"."F0111"."WWW005"  ,"BUSDTA"."F0111"."WWW006"  ,
     "BUSDTA"."F0111"."WWW007"  ,"BUSDTA"."F0111"."WWW008"  ,"BUSDTA"."F0111"."WWW009"  ,"BUSDTA"."F0111"."WWW010"  ,"BUSDTA"."F0111"."WWMLN1"  ,
     "BUSDTA"."F0111"."WWALP1"  ,"BUSDTA"."F0111"."WWUSER"  ,"BUSDTA"."F0111"."WWPID"  ,"BUSDTA"."F0111"."WWUPMJ"  ,"BUSDTA"."F0111"."WWJOBN"  ,
     "BUSDTA"."F0111"."WWUPMT"  ,"BUSDTA"."F0111"."WWNTYP"  ,"BUSDTA"."F0111"."WWNICK"  ,"BUSDTA"."F0111"."WWGEND"  ,"BUSDTA"."F0111"."WWDDATE"  ,
     "BUSDTA"."F0111"."WWDMON"  ,"BUSDTA"."F0111"."WWDYR"  ,"BUSDTA"."F0111"."WWWN001"  ,"BUSDTA"."F0111"."WWWN002"  ,"BUSDTA"."F0111"."WWWN003"  ,
     "BUSDTA"."F0111"."WWWN004"  ,"BUSDTA"."F0111"."WWWN005"  ,"BUSDTA"."F0111"."WWWN006"  ,"BUSDTA"."F0111"."WWWN007"  ,"BUSDTA"."F0111"."WWWN008"  ,
     "BUSDTA"."F0111"."WWWN009"  ,"BUSDTA"."F0111"."WWWN010"  ,"BUSDTA"."F0111"."WWFUCO"  ,"BUSDTA"."F0111"."WWPCM"  ,"BUSDTA"."F0111"."WWPCF"  ,
     "BUSDTA"."F0111"."WWACTIN"  ,"BUSDTA"."F0111"."WWCFRGUID"  ,"BUSDTA"."F0111"."WWSYNCS"  ,"BUSDTA"."F0111"."WWCAAD"    
     FROM "BUSDTA"."F0111"  WHERE "BUSDTA"."F0111"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0111',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0111_del"."WWAN8"
,"BUSDTA"."F0111_del"."WWIDLN"

FROM "BUSDTA"."F0111_del"
WHERE "BUSDTA"."F0111_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0111',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0111"
("WWAN8", "WWIDLN", "WWDSS5", "WWMLNM", "WWATTL", "WWREM1", "WWSLNM", "WWALPH", "WWDC", "WWGNNM", "WWMDNM", "WWSRNM", "WWTYC", "WWW001", "WWW002", "WWW003", "WWW004", "WWW005", "WWW006", "WWW007", "WWW008", "WWW009", "WWW010", "WWMLN1", "WWALP1", "WWUSER", "WWPID", "WWUPMJ", "WWJOBN", "WWUPMT", "WWNTYP", "WWNICK", "WWGEND", "WWDDATE", "WWDMON", "WWDYR", "WWWN001", "WWWN002", "WWWN003", "WWWN004", "WWWN005", "WWWN006", "WWWN007", "WWWN008", "WWWN009", "WWWN010", "WWFUCO", "WWPCM", "WWPCF", "WWACTIN", "WWCFRGUID", "WWSYNCS", "WWCAAD")
VALUES({ml r."WWAN8"}, {ml r."WWIDLN"}, {ml r."WWDSS5"}, {ml r."WWMLNM"}, {ml r."WWATTL"}, {ml r."WWREM1"}, {ml r."WWSLNM"}, {ml r."WWALPH"}, {ml r."WWDC"}, {ml r."WWGNNM"}, {ml r."WWMDNM"}, {ml r."WWSRNM"}, {ml r."WWTYC"}, {ml r."WWW001"}, {ml r."WWW002"}, {ml r."WWW003"}, {ml r."WWW004"}, {ml r."WWW005"}, {ml r."WWW006"}, {ml r."WWW007"}, {ml r."WWW008"}, {ml r."WWW009"}, {ml r."WWW010"}, {ml r."WWMLN1"}, {ml r."WWALP1"}, {ml r."WWUSER"}, {ml r."WWPID"}, {ml r."WWUPMJ"}, {ml r."WWJOBN"}, {ml r."WWUPMT"}, {ml r."WWNTYP"}, {ml r."WWNICK"}, {ml r."WWGEND"}, {ml r."WWDDATE"}, {ml r."WWDMON"}, {ml r."WWDYR"}, {ml r."WWWN001"}, {ml r."WWWN002"}, {ml r."WWWN003"}, {ml r."WWWN004"}, {ml r."WWWN005"}, {ml r."WWWN006"}, {ml r."WWWN007"}, {ml r."WWWN008"}, {ml r."WWWN009"}, {ml r."WWWN010"}, {ml r."WWFUCO"}, {ml r."WWPCM"}, {ml r."WWPCF"}, {ml r."WWACTIN"}, {ml r."WWCFRGUID"}, {ml r."WWSYNCS"}, {ml r."WWCAAD"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0111',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0111"
SET "WWDSS5" = {ml r."WWDSS5"}, "WWMLNM" = {ml r."WWMLNM"}, "WWATTL" = {ml r."WWATTL"}, "WWREM1" = {ml r."WWREM1"}, "WWSLNM" = {ml r."WWSLNM"}, "WWALPH" = {ml r."WWALPH"}, "WWDC" = {ml r."WWDC"}, "WWGNNM" = {ml r."WWGNNM"}, "WWMDNM" = {ml r."WWMDNM"}, "WWSRNM" = {ml r."WWSRNM"}, "WWTYC" = {ml r."WWTYC"}, "WWW001" = {ml r."WWW001"}, "WWW002" = {ml r."WWW002"}, "WWW003" = {ml r."WWW003"}, "WWW004" = {ml r."WWW004"}, "WWW005" = {ml r."WWW005"}, "WWW006" = {ml r."WWW006"}, "WWW007" = {ml r."WWW007"}, "WWW008" = {ml r."WWW008"}, "WWW009" = {ml r."WWW009"}, "WWW010" = {ml r."WWW010"}, "WWMLN1" = {ml r."WWMLN1"}, "WWALP1" = {ml r."WWALP1"}, "WWUSER" = {ml r."WWUSER"}, "WWPID" = {ml r."WWPID"}, "WWUPMJ" = {ml r."WWUPMJ"}, "WWJOBN" = {ml r."WWJOBN"}, "WWUPMT" = {ml r."WWUPMT"}, "WWNTYP" = {ml r."WWNTYP"}, "WWNICK" = {ml r."WWNICK"}, "WWGEND" = {ml r."WWGEND"}, "WWDDATE" = {ml r."WWDDATE"}, "WWDMON" = {ml r."WWDMON"}, "WWDYR" = {ml r."WWDYR"}, "WWWN001" = {ml r."WWWN001"}, "WWWN002" = {ml r."WWWN002"}, "WWWN003" = {ml r."WWWN003"}, "WWWN004" = {ml r."WWWN004"}, "WWWN005" = {ml r."WWWN005"}, "WWWN006" = {ml r."WWWN006"}, "WWWN007" = {ml r."WWWN007"}, "WWWN008" = {ml r."WWWN008"}, "WWWN009" = {ml r."WWWN009"}, "WWWN010" = {ml r."WWWN010"}, "WWFUCO" = {ml r."WWFUCO"}, "WWPCM" = {ml r."WWPCM"}, "WWPCF" = {ml r."WWPCF"}, "WWACTIN" = {ml r."WWACTIN"}, "WWCFRGUID" = {ml r."WWCFRGUID"}, "WWSYNCS" = {ml r."WWSYNCS"}, "WWCAAD" = {ml r."WWCAAD"}
WHERE "WWAN8" = {ml r."WWAN8"} AND "WWIDLN" = {ml r."WWIDLN"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0111',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0111"
WHERE "WWAN8" = {ml r."WWAN8"} AND "WWIDLN" = {ml r."WWIDLN"}
 

'

/* upload_delete - End */

/* Table :F0116 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F0116"."ALAN8"  ,"BUSDTA"."F0116"."ALEFTB"  ,"BUSDTA"."F0116"."ALEFTF"  ,"BUSDTA"."F0116"."ALADD1"  ,"BUSDTA"."F0116"."ALADD2"  ,
     "BUSDTA"."F0116"."ALADD3"  ,"BUSDTA"."F0116"."ALADD4"  ,"BUSDTA"."F0116"."ALADDZ"  ,"BUSDTA"."F0116"."ALCTY1"  ,"BUSDTA"."F0116"."ALCOUN"  ,
     "BUSDTA"."F0116"."ALADDS"    
     FROM "BUSDTA"."F0116", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."F0116"."last_modified" >= {ml s.last_table_download} or rm."last_modified" >= {ml s.last_table_download}  
      or crm."last_modified" >= {ml s.last_table_download})   AND ALAN8=crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber  
      AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0116_del"."ALAN8"
,"BUSDTA"."F0116_del"."ALEFTB"

FROM "BUSDTA"."F0116_del"
WHERE "BUSDTA"."F0116_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0116"
("ALAN8", "ALEFTB", "ALEFTF", "ALADD1", "ALADD2", "ALADD3", "ALADD4", "ALADDZ", "ALCTY1", "ALCOUN", "ALADDS")
VALUES({ml r."ALAN8"}, {ml r."ALEFTB"}, {ml r."ALEFTF"}, {ml r."ALADD1"}, {ml r."ALADD2"}, {ml r."ALADD3"}, {ml r."ALADD4"}, {ml r."ALADDZ"}, {ml r."ALCTY1"}, {ml r."ALCOUN"}, {ml r."ALADDS"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0116"
SET "ALEFTF" = {ml r."ALEFTF"}, "ALADD1" = {ml r."ALADD1"}, "ALADD2" = {ml r."ALADD2"}, "ALADD3" = {ml r."ALADD3"}, "ALADD4" = {ml r."ALADD4"}, "ALADDZ" = {ml r."ALADDZ"}, "ALCTY1" = {ml r."ALCTY1"}, "ALCOUN" = {ml r."ALCOUN"}, "ALADDS" = {ml r."ALADDS"}
WHERE "ALAN8" = {ml r."ALAN8"} AND "ALEFTB" = {ml r."ALEFTB"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0116"
WHERE "ALAN8" = {ml r."ALAN8"} AND "ALEFTB" = {ml r."ALEFTB"}
 

'

/* upload_delete - End */

/* Table :F0150 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0150',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F0150"."MAOSTP"  ,"BUSDTA"."F0150"."MAPA8"  ,"BUSDTA"."F0150"."MAAN8"  ,"BUSDTA"."F0150"."MABEFD"  ,
 "BUSDTA"."F0150"."MAEEFD"    
 FROM "BUSDTA"."F0150", BUSDTA.F03012, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
 WHERE ("BUSDTA"."F0150"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
 or crm.last_modified >= {ml s.last_table_download})    AND AIAN8=MAAN8 AND AIAN8 = crm.RelatedAddressBookNumber 
 AND crm.BranchNumber = rm.BranchNumber   AND RouteMasterID = (select RouteMasterID from busdta.Route_Master   
 where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0150',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F0150_del"."MAOSTP"
,"BUSDTA"."F0150_del"."MAPA8"
,"BUSDTA"."F0150_del"."MAAN8"

FROM "BUSDTA"."F0150_del"
WHERE "BUSDTA"."F0150_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0150',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0150"
("MAOSTP", "MAPA8", "MAAN8", "MABEFD", "MAEEFD")
VALUES({ml r."MAOSTP"}, {ml r."MAPA8"}, {ml r."MAAN8"}, {ml r."MABEFD"}, {ml r."MAEEFD"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0150',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0150"
SET "MABEFD" = {ml r."MABEFD"}, "MAEEFD" = {ml r."MAEEFD"}
WHERE "MAOSTP" = {ml r."MAOSTP"} AND "MAPA8" = {ml r."MAPA8"} AND "MAAN8" = {ml r."MAAN8"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0150',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0150"
WHERE "MAOSTP" = {ml r."MAOSTP"} AND "MAPA8" = {ml r."MAPA8"} AND "MAAN8" = {ml r."MAAN8"}
 

'

/* upload_delete - End */

/* Table :F03012 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F03012',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F03012"."AIAN8"  ,"BUSDTA"."F03012"."AICO"  ,"BUSDTA"."F03012"."AIMCUR"  ,"BUSDTA"."F03012"."AITXA1"  ,
     "BUSDTA"."F03012"."AIEXR1"  ,"BUSDTA"."F03012"."AIACL"  ,"BUSDTA"."F03012"."AIHDAR"  ,"BUSDTA"."F03012"."AITRAR"  ,
     "BUSDTA"."F03012"."AISTTO"  ,"BUSDTA"."F03012"."AIRYIN"  ,"BUSDTA"."F03012"."AISTMT"  ,"BUSDTA"."F03012"."AIARPY"  ,
     "BUSDTA"."F03012"."AISITO"  ,"BUSDTA"."F03012"."AICYCN"  ,"BUSDTA"."F03012"."AIBO"  ,"BUSDTA"."F03012"."AITSTA"  ,"BUSDTA"."F03012"."AICKHC"  ,
     "BUSDTA"."F03012"."AIDLC"  ,"BUSDTA"."F03012"."AIDNLT"  ,"BUSDTA"."F03012"."AIPLCR"  ,"BUSDTA"."F03012"."AIRVDJ"  ,"BUSDTA"."F03012"."AIDSO"  ,
     "BUSDTA"."F03012"."AICMGR"  ,"BUSDTA"."F03012"."AICLMG"  ,"BUSDTA"."F03012"."AIAB2"  ,"BUSDTA"."F03012"."AIDT1J"  ,"BUSDTA"."F03012"."AIDFIJ"  ,
     "BUSDTA"."F03012"."AIDLIJ"  ,"BUSDTA"."F03012"."AIDLP"  ,"BUSDTA"."F03012"."AIASTY"  ,"BUSDTA"."F03012"."AISPYE"  ,"BUSDTA"."F03012"."AIAHB"  ,
     "BUSDTA"."F03012"."AIALP"  ,"BUSDTA"."F03012"."AIABAM"  ,"BUSDTA"."F03012"."AIABA1"  ,"BUSDTA"."F03012"."AIAPRC"  ,"BUSDTA"."F03012"."AIMAXO"  ,
     "BUSDTA"."F03012"."AIMINO"  ,"BUSDTA"."F03012"."AIOYTD"  ,"BUSDTA"."F03012"."AIOPY"  ,"BUSDTA"."F03012"."AIPOPN"  ,"BUSDTA"."F03012"."AIDAOJ"  ,
     "BUSDTA"."F03012"."AIAN8R"  ,"BUSDTA"."F03012"."AIBADT"  ,"BUSDTA"."F03012"."AICPGP"  ,"BUSDTA"."F03012"."AIORTP"  ,
     "BUSDTA"."F03012"."AITRDC"  ,"BUSDTA"."F03012"."AIINMG"  ,"BUSDTA"."F03012"."AIEXHD"  ,"BUSDTA"."F03012"."AIHOLD"  ,
     "BUSDTA"."F03012"."AIROUT"  ,"BUSDTA"."F03012"."AISTOP"  ,"BUSDTA"."F03012"."AIZON"  ,"BUSDTA"."F03012"."AICARS"  ,"BUSDTA"."F03012"."AIDEL1"  ,
     "BUSDTA"."F03012"."AIDEL2"  ,"BUSDTA"."F03012"."AILTDT"  ,"BUSDTA"."F03012"."AIFRTH"  ,"BUSDTA"."F03012"."AIAFT"  ,"BUSDTA"."F03012"."AIAPTS"  ,
     "BUSDTA"."F03012"."AISBAL"  ,"BUSDTA"."F03012"."AIBACK"  ,"BUSDTA"."F03012"."AIPORQ"  ,"BUSDTA"."F03012"."AIPRIO"  ,
     "BUSDTA"."F03012"."AIARTO"  ,"BUSDTA"."F03012"."AIINVC"  ,"BUSDTA"."F03012"."AIICON"  ,"BUSDTA"."F03012"."AIBLFR"  ,
     "BUSDTA"."F03012"."AINIVD"  ,"BUSDTA"."F03012"."AILEDJ"  ,"BUSDTA"."F03012"."AIPLST"  ,"BUSDTA"."F03012"."AIEDF1"  ,
     "BUSDTA"."F03012"."AIEDF2"  ,"BUSDTA"."F03012"."AIASN"  ,"BUSDTA"."F03012"."AIDSPA"  ,"BUSDTA"."F03012"."AICRMD"  ,"BUSDTA"."F03012"."AIAMCR"  ,
     "BUSDTA"."F03012"."AIAC01"  ,"BUSDTA"."F03012"."AIAC02"  ,"BUSDTA"."F03012"."AIAC03"  ,"BUSDTA"."F03012"."AIAC04"  ,
     "BUSDTA"."F03012"."AIAC05"  ,"BUSDTA"."F03012"."AIAC06"  ,"BUSDTA"."F03012"."AIAC07"  ,"BUSDTA"."F03012"."AIAC08"  ,
     "BUSDTA"."F03012"."AIAC09"  ,"BUSDTA"."F03012"."AIAC10"  ,"BUSDTA"."F03012"."AIAC11"  ,"BUSDTA"."F03012"."AIAC12"  ,
     "BUSDTA"."F03012"."AIAC13"  ,"BUSDTA"."F03012"."AIAC14"  ,"BUSDTA"."F03012"."AIAC15"  ,"BUSDTA"."F03012"."AIAC16"  ,
     "BUSDTA"."F03012"."AIAC17"  ,"BUSDTA"."F03012"."AIAC18"  ,"BUSDTA"."F03012"."AIAC19"  ,"BUSDTA"."F03012"."AIAC20"  ,
     "BUSDTA"."F03012"."AIAC21"  ,"BUSDTA"."F03012"."AIAC22"  ,"BUSDTA"."F03012"."AIAC23"  ,"BUSDTA"."F03012"."AIAC24"  ,
     "BUSDTA"."F03012"."AIAC25"  ,"BUSDTA"."F03012"."AIAC26"  ,"BUSDTA"."F03012"."AIAC27"  ,"BUSDTA"."F03012"."AIAC28"  ,
     "BUSDTA"."F03012"."AIAC29"  ,"BUSDTA"."F03012"."AIAC30"  ,"BUSDTA"."F03012"."AIPRSN"  ,"BUSDTA"."F03012"."AIOPBO"  ,
     "BUSDTA"."F03012"."AITIER1"  ,"BUSDTA"."F03012"."AIPWPCP"  ,"BUSDTA"."F03012"."AICUSTS"  ,"BUSDTA"."F03012"."AISTOF"  ,
     "BUSDTA"."F03012"."AITERRID"  ,"BUSDTA"."F03012"."AICIG"  ,"BUSDTA"."F03012"."AITORG"    
     FROM "BUSDTA"."F03012", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."F03012"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or crm.last_modified >= {ml s.last_table_download})    AND AIAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F03012',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F03012_del"."AIAN8"
,"BUSDTA"."F03012_del"."AICO"

FROM "BUSDTA"."F03012_del"
WHERE "BUSDTA"."F03012_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F03012',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F03012"
("AIAN8", "AICO", "AIMCUR", "AITXA1", "AIEXR1", "AIACL", "AIHDAR", "AITRAR", "AISTTO", "AIRYIN", "AISTMT", "AIARPY", "AISITO", "AICYCN", "AIBO", "AITSTA", "AICKHC", "AIDLC", "AIDNLT", "AIPLCR", "AIRVDJ", "AIDSO", "AICMGR", "AICLMG", "AIAB2", "AIDT1J", "AIDFIJ", "AIDLIJ", "AIDLP", "AIASTY", "AISPYE", "AIAHB", "AIALP", "AIABAM", "AIABA1", "AIAPRC", "AIMAXO", "AIMINO", "AIOYTD", "AIOPY", "AIPOPN", "AIDAOJ", "AIAN8R", "AIBADT", "AICPGP", "AIORTP", "AITRDC", "AIINMG", "AIEXHD", "AIHOLD", "AIROUT", "AISTOP", "AIZON", "AICARS", "AIDEL1", "AIDEL2", "AILTDT", "AIFRTH", "AIAFT", "AIAPTS", "AISBAL", "AIBACK", "AIPORQ", "AIPRIO", "AIARTO", "AIINVC", "AIICON", "AIBLFR", "AINIVD", "AILEDJ", "AIPLST", "AIEDF1", "AIEDF2", "AIASN", "AIDSPA", "AICRMD", "AIAMCR", "AIAC01", "AIAC02", "AIAC03", "AIAC04", "AIAC05", "AIAC06", "AIAC07", "AIAC08", "AIAC09", "AIAC10", "AIAC11", "AIAC12", "AIAC13", "AIAC14", "AIAC15", "AIAC16", "AIAC17", "AIAC18", "AIAC19", "AIAC20", "AIAC21", "AIAC22", "AIAC23", "AIAC24", "AIAC25", "AIAC26", "AIAC27", "AIAC28", "AIAC29", "AIAC30", "AIPRSN", "AIOPBO", "AITIER1", "AIPWPCP", "AICUSTS", "AISTOF", "AITERRID", "AICIG", "AITORG")
VALUES({ml r."AIAN8"}, {ml r."AICO"}, {ml r."AIMCUR"}, {ml r."AITXA1"}, {ml r."AIEXR1"}, {ml r."AIACL"}, {ml r."AIHDAR"}, {ml r."AITRAR"}, {ml r."AISTTO"}, {ml r."AIRYIN"}, {ml r."AISTMT"}, {ml r."AIARPY"}, {ml r."AISITO"}, {ml r."AICYCN"}, {ml r."AIBO"}, {ml r."AITSTA"}, {ml r."AICKHC"}, {ml r."AIDLC"}, {ml r."AIDNLT"}, {ml r."AIPLCR"}, {ml r."AIRVDJ"}, {ml r."AIDSO"}, {ml r."AICMGR"}, {ml r."AICLMG"}, {ml r."AIAB2"}, {ml r."AIDT1J"}, {ml r."AIDFIJ"}, {ml r."AIDLIJ"}, {ml r."AIDLP"}, {ml r."AIASTY"}, {ml r."AISPYE"}, {ml r."AIAHB"}, {ml r."AIALP"}, {ml r."AIABAM"}, {ml r."AIABA1"}, {ml r."AIAPRC"}, {ml r."AIMAXO"}, {ml r."AIMINO"}, {ml r."AIOYTD"}, {ml r."AIOPY"}, {ml r."AIPOPN"}, {ml r."AIDAOJ"}, {ml r."AIAN8R"}, {ml r."AIBADT"}, {ml r."AICPGP"}, {ml r."AIORTP"}, {ml r."AITRDC"}, {ml r."AIINMG"}, {ml r."AIEXHD"}, {ml r."AIHOLD"}, {ml r."AIROUT"}, {ml r."AISTOP"}, {ml r."AIZON"}, {ml r."AICARS"}, {ml r."AIDEL1"}, {ml r."AIDEL2"}, {ml r."AILTDT"}, {ml r."AIFRTH"}, {ml r."AIAFT"}, {ml r."AIAPTS"}, {ml r."AISBAL"}, {ml r."AIBACK"}, {ml r."AIPORQ"}, {ml r."AIPRIO"}, {ml r."AIARTO"}, {ml r."AIINVC"}, {ml r."AIICON"}, {ml r."AIBLFR"}, {ml r."AINIVD"}, {ml r."AILEDJ"}, {ml r."AIPLST"}, {ml r."AIEDF1"}, {ml r."AIEDF2"}, {ml r."AIASN"}, {ml r."AIDSPA"}, {ml r."AICRMD"}, {ml r."AIAMCR"}, {ml r."AIAC01"}, {ml r."AIAC02"}, {ml r."AIAC03"}, {ml r."AIAC04"}, {ml r."AIAC05"}, {ml r."AIAC06"}, {ml r."AIAC07"}, {ml r."AIAC08"}, {ml r."AIAC09"}, {ml r."AIAC10"}, {ml r."AIAC11"}, {ml r."AIAC12"}, {ml r."AIAC13"}, {ml r."AIAC14"}, {ml r."AIAC15"}, {ml r."AIAC16"}, {ml r."AIAC17"}, {ml r."AIAC18"}, {ml r."AIAC19"}, {ml r."AIAC20"}, {ml r."AIAC21"}, {ml r."AIAC22"}, {ml r."AIAC23"}, {ml r."AIAC24"}, {ml r."AIAC25"}, {ml r."AIAC26"}, {ml r."AIAC27"}, {ml r."AIAC28"}, {ml r."AIAC29"}, {ml r."AIAC30"}, {ml r."AIPRSN"}, {ml r."AIOPBO"}, {ml r."AITIER1"}, {ml r."AIPWPCP"}, {ml r."AICUSTS"}, {ml r."AISTOF"}, {ml r."AITERRID"}, {ml r."AICIG"}, {ml r."AITORG"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F03012',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F03012"
SET "AIMCUR" = {ml r."AIMCUR"}, "AITXA1" = {ml r."AITXA1"}, "AIEXR1" = {ml r."AIEXR1"}, "AIACL" = {ml r."AIACL"}, "AIHDAR" = {ml r."AIHDAR"}, "AITRAR" = {ml r."AITRAR"}, "AISTTO" = {ml r."AISTTO"}, "AIRYIN" = {ml r."AIRYIN"}, "AISTMT" = {ml r."AISTMT"}, "AIARPY" = {ml r."AIARPY"}, "AISITO" = {ml r."AISITO"}, "AICYCN" = {ml r."AICYCN"}, "AIBO" = {ml r."AIBO"}, "AITSTA" = {ml r."AITSTA"}, "AICKHC" = {ml r."AICKHC"}, "AIDLC" = {ml r."AIDLC"}, "AIDNLT" = {ml r."AIDNLT"}, "AIPLCR" = {ml r."AIPLCR"}, "AIRVDJ" = {ml r."AIRVDJ"}, "AIDSO" = {ml r."AIDSO"}, "AICMGR" = {ml r."AICMGR"}, "AICLMG" = {ml r."AICLMG"}, "AIAB2" = {ml r."AIAB2"}, "AIDT1J" = {ml r."AIDT1J"}, "AIDFIJ" = {ml r."AIDFIJ"}, "AIDLIJ" = {ml r."AIDLIJ"}, "AIDLP" = {ml r."AIDLP"}, "AIASTY" = {ml r."AIASTY"}, "AISPYE" = {ml r."AISPYE"}, "AIAHB" = {ml r."AIAHB"}, "AIALP" = {ml r."AIALP"}, "AIABAM" = {ml r."AIABAM"}, "AIABA1" = {ml r."AIABA1"}, "AIAPRC" = {ml r."AIAPRC"}, "AIMAXO" = {ml r."AIMAXO"}, "AIMINO" = {ml r."AIMINO"}, "AIOYTD" = {ml r."AIOYTD"}, "AIOPY" = {ml r."AIOPY"}, "AIPOPN" = {ml r."AIPOPN"}, "AIDAOJ" = {ml r."AIDAOJ"}, "AIAN8R" = {ml r."AIAN8R"}, "AIBADT" = {ml r."AIBADT"}, "AICPGP" = {ml r."AICPGP"}, "AIORTP" = {ml r."AIORTP"}, "AITRDC" = {ml r."AITRDC"}, "AIINMG" = {ml r."AIINMG"}, "AIEXHD" = {ml r."AIEXHD"}, "AIHOLD" = {ml r."AIHOLD"}, "AIROUT" = {ml r."AIROUT"}, "AISTOP" = {ml r."AISTOP"}, "AIZON" = {ml r."AIZON"}, "AICARS" = {ml r."AICARS"}, "AIDEL1" = {ml r."AIDEL1"}, "AIDEL2" = {ml r."AIDEL2"}, "AILTDT" = {ml r."AILTDT"}, "AIFRTH" = {ml r."AIFRTH"}, "AIAFT" = {ml r."AIAFT"}, "AIAPTS" = {ml r."AIAPTS"}, "AISBAL" = {ml r."AISBAL"}, "AIBACK" = {ml r."AIBACK"}, "AIPORQ" = {ml r."AIPORQ"}, "AIPRIO" = {ml r."AIPRIO"}, "AIARTO" = {ml r."AIARTO"}, "AIINVC" = {ml r."AIINVC"}, "AIICON" = {ml r."AIICON"}, "AIBLFR" = {ml r."AIBLFR"}, "AINIVD" = {ml r."AINIVD"}, "AILEDJ" = {ml r."AILEDJ"}, "AIPLST" = {ml r."AIPLST"}, "AIEDF1" = {ml r."AIEDF1"}, "AIEDF2" = {ml r."AIEDF2"}, "AIASN" = {ml r."AIASN"}, "AIDSPA" = {ml r."AIDSPA"}, "AICRMD" = {ml r."AICRMD"}, "AIAMCR" = {ml r."AIAMCR"}, "AIAC01" = {ml r."AIAC01"}, "AIAC02" = {ml r."AIAC02"}, "AIAC03" = {ml r."AIAC03"}, "AIAC04" = {ml r."AIAC04"}, "AIAC05" = {ml r."AIAC05"}, "AIAC06" = {ml r."AIAC06"}, "AIAC07" = {ml r."AIAC07"}, "AIAC08" = {ml r."AIAC08"}, "AIAC09" = {ml r."AIAC09"}, "AIAC10" = {ml r."AIAC10"}, "AIAC11" = {ml r."AIAC11"}, "AIAC12" = {ml r."AIAC12"}, "AIAC13" = {ml r."AIAC13"}, "AIAC14" = {ml r."AIAC14"}, "AIAC15" = {ml r."AIAC15"}, "AIAC16" = {ml r."AIAC16"}, "AIAC17" = {ml r."AIAC17"}, "AIAC18" = {ml r."AIAC18"}, "AIAC19" = {ml r."AIAC19"}, "AIAC20" = {ml r."AIAC20"}, "AIAC21" = {ml r."AIAC21"}, "AIAC22" = {ml r."AIAC22"}, "AIAC23" = {ml r."AIAC23"}, "AIAC24" = {ml r."AIAC24"}, "AIAC25" = {ml r."AIAC25"}, "AIAC26" = {ml r."AIAC26"}, "AIAC27" = {ml r."AIAC27"}, "AIAC28" = {ml r."AIAC28"}, "AIAC29" = {ml r."AIAC29"}, "AIAC30" = {ml r."AIAC30"}, "AIPRSN" = {ml r."AIPRSN"}, "AIOPBO" = {ml r."AIOPBO"}, "AITIER1" = {ml r."AITIER1"}, "AIPWPCP" = {ml r."AIPWPCP"}, "AICUSTS" = {ml r."AICUSTS"}, "AISTOF" = {ml r."AISTOF"}, "AITERRID" = {ml r."AITERRID"}, "AICIG" = {ml r."AICIG"}, "AITORG" = {ml r."AITORG"}
WHERE "AIAN8" = {ml r."AIAN8"} AND "AICO" = {ml r."AICO"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F03012',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F03012"
WHERE "AIAN8" = {ml r."AIAN8"} AND "AICO" = {ml r."AICO"}
 

'

/* upload_delete - End */

/* Table :F40073 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40073',
	'download_cursor',
	'sql',

'


 
SELECT "BUSDTA"."F40073"."HYPRFR"
,"BUSDTA"."F40073"."HYHYID"
,"BUSDTA"."F40073"."HYHY01"
,"BUSDTA"."F40073"."HYHY02"
,"BUSDTA"."F40073"."HYHY03"
,"BUSDTA"."F40073"."HYHY04"
,"BUSDTA"."F40073"."HYHY05"
,"BUSDTA"."F40073"."HYHY06"
,"BUSDTA"."F40073"."HYHY07"
,"BUSDTA"."F40073"."HYHY08"
,"BUSDTA"."F40073"."HYHY09"
,"BUSDTA"."F40073"."HYHY10"
,"BUSDTA"."F40073"."HYHY11"
,"BUSDTA"."F40073"."HYHY12"
,"BUSDTA"."F40073"."HYHY13"
,"BUSDTA"."F40073"."HYHY14"
,"BUSDTA"."F40073"."HYHY15"
,"BUSDTA"."F40073"."HYHY16"
,"BUSDTA"."F40073"."HYHY17"
,"BUSDTA"."F40073"."HYHY18"
,"BUSDTA"."F40073"."HYHY19"
,"BUSDTA"."F40073"."HYHY20"
,"BUSDTA"."F40073"."HYHY21"

FROM "BUSDTA"."F40073"
WHERE "BUSDTA"."F40073"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40073',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F40073_del"."HYPRFR"
,"BUSDTA"."F40073_del"."HYHYID"

FROM "BUSDTA"."F40073_del"
WHERE "BUSDTA"."F40073_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40073',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F40073"
("HYPRFR", "HYHYID", "HYHY01", "HYHY02", "HYHY03", "HYHY04", "HYHY05", "HYHY06", "HYHY07", "HYHY08", "HYHY09", "HYHY10", "HYHY11", "HYHY12", "HYHY13", "HYHY14", "HYHY15", "HYHY16", "HYHY17", "HYHY18", "HYHY19", "HYHY20", "HYHY21")
VALUES({ml r."HYPRFR"}, {ml r."HYHYID"}, {ml r."HYHY01"}, {ml r."HYHY02"}, {ml r."HYHY03"}, {ml r."HYHY04"}, {ml r."HYHY05"}, {ml r."HYHY06"}, {ml r."HYHY07"}, {ml r."HYHY08"}, {ml r."HYHY09"}, {ml r."HYHY10"}, {ml r."HYHY11"}, {ml r."HYHY12"}, {ml r."HYHY13"}, {ml r."HYHY14"}, {ml r."HYHY15"}, {ml r."HYHY16"}, {ml r."HYHY17"}, {ml r."HYHY18"}, {ml r."HYHY19"}, {ml r."HYHY20"}, {ml r."HYHY21"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40073',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F40073"
SET "HYHY01" = {ml r."HYHY01"}, "HYHY02" = {ml r."HYHY02"}, "HYHY03" = {ml r."HYHY03"}, "HYHY04" = {ml r."HYHY04"}, "HYHY05" = {ml r."HYHY05"}, "HYHY06" = {ml r."HYHY06"}, "HYHY07" = {ml r."HYHY07"}, "HYHY08" = {ml r."HYHY08"}, "HYHY09" = {ml r."HYHY09"}, "HYHY10" = {ml r."HYHY10"}, "HYHY11" = {ml r."HYHY11"}, "HYHY12" = {ml r."HYHY12"}, "HYHY13" = {ml r."HYHY13"}, "HYHY14" = {ml r."HYHY14"}, "HYHY15" = {ml r."HYHY15"}, "HYHY16" = {ml r."HYHY16"}, "HYHY17" = {ml r."HYHY17"}, "HYHY18" = {ml r."HYHY18"}, "HYHY19" = {ml r."HYHY19"}, "HYHY20" = {ml r."HYHY20"}, "HYHY21" = {ml r."HYHY21"}
WHERE "HYPRFR" = {ml r."HYPRFR"} AND "HYHYID" = {ml r."HYHYID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40073',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F40073"
WHERE "HYPRFR" = {ml r."HYPRFR"} AND "HYHYID" = {ml r."HYHYID"}
'

/* upload_delete - End */

/* Table :F4013 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4013',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F4013"."SXXRTC"  ,"BUSDTA"."F4013"."SXXRVF"  ,"BUSDTA"."F4013"."SXXRVT"  ,"BUSDTA"."F4013"."SXEDF1"  ,
     "BUSDTA"."F4013"."SXDESC"    
     FROM "BUSDTA"."F4013"  
     WHERE "BUSDTA"."F4013"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4013',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F4013_del"."SXXRTC"
,"BUSDTA"."F4013_del"."SXXRVF"
,"BUSDTA"."F4013_del"."SXXRVT"
,"BUSDTA"."F4013_del"."SXEDF1"

FROM "BUSDTA"."F4013_del"
WHERE "BUSDTA"."F4013_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4013',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4013"
("SXXRTC", "SXXRVF", "SXXRVT", "SXEDF1", "SXDESC")
VALUES({ml r."SXXRTC"}, {ml r."SXXRVF"}, {ml r."SXXRVT"}, {ml r."SXEDF1"}, {ml r."SXDESC"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4013',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4013"
SET "SXDESC" = {ml r."SXDESC"}
WHERE "SXXRTC" = {ml r."SXXRTC"} AND "SXXRVF" = {ml r."SXXRVF"} AND "SXXRVT" = {ml r."SXXRVT"} AND "SXEDF1" = {ml r."SXEDF1"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4013',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4013"
WHERE "SXXRTC" = {ml r."SXXRTC"} AND "SXXRVF" = {ml r."SXXRVF"} AND "SXXRVT" = {ml r."SXXRVT"} AND "SXEDF1" = {ml r."SXEDF1"}
 

'

/* upload_delete - End */

/* Table :F4015 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4015',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."F4015"."OTORTP"  ,"BUSDTA"."F4015"."OTAN8"  ,"BUSDTA"."F4015"."OTOSEQ"  ,
"BUSDTA"."F4015"."OTITM"  ,"BUSDTA"."F4015"."OTLITM"  ,"BUSDTA"."F4015"."OTQTYU"  ,
"BUSDTA"."F4015"."OTUOM"  ,"BUSDTA"."F4015"."OTLNTY"  ,"BUSDTA"."F4015"."OTEFTJ"  ,
"BUSDTA"."F4015"."OTEXDJ"    FROM "BUSDTA"."F4015", BUSDTA.F03012, BUSDTA.Route_Master rm, 
BUSDTA.Customer_Route_Map crm  WHERE ("BUSDTA"."F4015"."last_modified" >= {ml s.last_table_download} 
or rm.last_modified >= {ml s.last_table_download}   or crm.last_modified >= {ml s.last_table_download}) 
and   OTORTP=AIORTP and OTAN8=AIAN8 and AIAN8=crm.RelatedAddressBookNumber AND 
crm.BranchNumber = rm.BranchNumber   AND RouteMasterID = (select RouteMasterID from busdta.Route_Master 
where RouteName = {ml s.username}) 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4015',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F4015_del"."OTORTP"  ,"BUSDTA"."F4015_del"."OTAN8"  ,"BUSDTA"."F4015_del"."OTITM"   
FROM "BUSDTA"."F4015_del"  
WHERE "BUSDTA"."F4015_del"."last_modified">= {ml s.last_table_download} 
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4015',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4015"
("OTORTP", "OTAN8", "OTOSEQ", "OTITM", "OTLITM", "OTQTYU", "OTUOM", "OTLNTY", "OTEFTJ", "OTEXDJ")
VALUES({ml r."OTORTP"}, {ml r."OTAN8"}, {ml r."OTOSEQ"}, {ml r."OTITM"}, {ml r."OTLITM"}, {ml r."OTQTYU"}, {ml r."OTUOM"}, {ml r."OTLNTY"}, {ml r."OTEFTJ"}, {ml r."OTEXDJ"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4015',
	'upload_update',
	'sql',

'

/* Update the row in the consolidated database. */  
UPDATE "BUSDTA"."F4015"  SET "OTOSEQ" = {ml r."OTOSEQ"}, 
"OTLITM" = {ml r."OTLITM"}, "OTQTYU" = {ml r."OTQTYU"}, 
"OTUOM" = {ml r."OTUOM"}, "OTLNTY" = {ml r."OTLNTY"}, 
"OTEFTJ" = {ml r."OTEFTJ"}, "OTEXDJ" = {ml r."OTEXDJ"}  
WHERE "OTORTP" = {ml r."OTORTP"} AND 
"OTAN8" = {ml r."OTAN8"} AND "OTITM" = {ml r."OTITM"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4015',
	'upload_delete',
	'sql',

'
/* Delete the row in the consolidated database. */  
DELETE FROM "BUSDTA"."F4015"  
WHERE "OTORTP" = {ml r."OTORTP"} AND "OTAN8" = {ml r."OTAN8"} AND "OTITM" = {ml r."OTITM"} 
'

/* upload_delete - End */

/* Table :F4070 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4070',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F4070"."SNASN"  ,"BUSDTA"."F4070"."SNOSEQ"  ,"BUSDTA"."F4070"."SNANPS"  ,"BUSDTA"."F4070"."SNAST"  ,
 "BUSDTA"."F4070"."SNEFTJ"  ,"BUSDTA"."F4070"."SNEXDJ"    
 FROM "BUSDTA"."F4070"  WHERE "BUSDTA"."F4070"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4070',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F4070_del"."SNASN"
,"BUSDTA"."F4070_del"."SNOSEQ"
,"BUSDTA"."F4070_del"."SNANPS"

FROM "BUSDTA"."F4070_del"
WHERE "BUSDTA"."F4070_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4070',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4070"
("SNASN", "SNOSEQ", "SNANPS", "SNAST", "SNEFTJ", "SNEXDJ")
VALUES({ml r."SNASN"}, {ml r."SNOSEQ"}, {ml r."SNANPS"}, {ml r."SNAST"}, {ml r."SNEFTJ"}, {ml r."SNEXDJ"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4070',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4070"
SET "SNAST" = {ml r."SNAST"}, "SNEFTJ" = {ml r."SNEFTJ"}, "SNEXDJ" = {ml r."SNEXDJ"}
WHERE "SNASN" = {ml r."SNASN"} AND "SNOSEQ" = {ml r."SNOSEQ"} AND "SNANPS" = {ml r."SNANPS"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4070',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4070"
WHERE "SNASN" = {ml r."SNASN"} AND "SNOSEQ" = {ml r."SNOSEQ"} AND "SNANPS" = {ml r."SNANPS"}
 

'

/* upload_delete - End */

/* Table :F4071 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4071',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F4071"."ATAST"
,"BUSDTA"."F4071"."ATPRGR"
,"BUSDTA"."F4071"."ATCPGP"
,"BUSDTA"."F4071"."ATSDGR"
,"BUSDTA"."F4071"."ATPRFR"
,"BUSDTA"."F4071"."ATLBT"
,"BUSDTA"."F4071"."ATGLC"
,"BUSDTA"."F4071"."ATSBIF"
,"BUSDTA"."F4071"."ATACNT"
,"BUSDTA"."F4071"."ATLNTY"
,"BUSDTA"."F4071"."ATMDED"
,"BUSDTA"."F4071"."ATABAS"
,"BUSDTA"."F4071"."ATOLVL"
,"BUSDTA"."F4071"."ATTXB"
,"BUSDTA"."F4071"."ATPA01"
,"BUSDTA"."F4071"."ATPA02"
,"BUSDTA"."F4071"."ATPA03"
,"BUSDTA"."F4071"."ATPA04"
,"BUSDTA"."F4071"."ATPA05"
,"BUSDTA"."F4071"."ATENBM"
,"BUSDTA"."F4071"."ATSRFLAG"
,"BUSDTA"."F4071"."ATUSADJ"
,"BUSDTA"."F4071"."ATATIER"
,"BUSDTA"."F4071"."ATBTIER"
,"BUSDTA"."F4071"."ATBNAD"
,"BUSDTA"."F4071"."ATAPRP1"
,"BUSDTA"."F4071"."ATAPRP2"
,"BUSDTA"."F4071"."ATAPRP3"
,"BUSDTA"."F4071"."ATAPRP4"
,"BUSDTA"."F4071"."ATAPRP5"
,"BUSDTA"."F4071"."ATAPRP6"
,"BUSDTA"."F4071"."ATADJGRP"
,"BUSDTA"."F4071"."ATMEADJ"
,"BUSDTA"."F4071"."ATPDCL"
,"BUSDTA"."F4071"."ATUSER"
,"BUSDTA"."F4071"."ATPID"
,"BUSDTA"."F4071"."ATJOBN"
,"BUSDTA"."F4071"."ATUPMJ"
,"BUSDTA"."F4071"."ATTDAY"
,"BUSDTA"."F4071"."ATDIDP"
,"BUSDTA"."F4071"."ATPMTN"
,"BUSDTA"."F4071"."ATPHST"
,"BUSDTA"."F4071"."ATPA06"
,"BUSDTA"."F4071"."ATPA07"
,"BUSDTA"."F4071"."ATPA08"
,"BUSDTA"."F4071"."ATPA09"
,"BUSDTA"."F4071"."ATPA10"
,"BUSDTA"."F4071"."ATEFCN"
,"BUSDTA"."F4071"."ATAPTYPE"
,"BUSDTA"."F4071"."ATMOADJ"
,"BUSDTA"."F4071"."ATPLGRP"
,"BUSDTA"."F4071"."ATEXCPL"
,"BUSDTA"."F4071"."ATUPMX"
,"BUSDTA"."F4071"."ATMNMXAJ"
,"BUSDTA"."F4071"."ATMNMXRL"
,"BUSDTA"."F4071"."ATTSTRSNM"
,"BUSDTA"."F4071"."ATADJQTY"

FROM "BUSDTA"."F4071"
WHERE "BUSDTA"."F4071"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4071',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."F4071_del"."ATAST"

FROM "BUSDTA"."F4071_del"
WHERE "BUSDTA"."F4071_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4071',
	'upload_insert',
	'sql',

'
 


/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4071"
("ATAST", "ATPRGR", "ATCPGP", "ATSDGR", "ATPRFR", "ATLBT", "ATGLC", "ATSBIF", "ATACNT", "ATLNTY", "ATMDED", "ATABAS", "ATOLVL", "ATTXB", "ATPA01", "ATPA02", "ATPA03", "ATPA04", "ATPA05", "ATENBM", "ATSRFLAG", "ATUSADJ", "ATATIER", "ATBTIER", "ATBNAD", "ATAPRP1", "ATAPRP2", "ATAPRP3", "ATAPRP4", "ATAPRP5", "ATAPRP6", "ATADJGRP", "ATMEADJ", "ATPDCL", "ATUSER", "ATPID", "ATJOBN", "ATUPMJ", "ATTDAY", "ATDIDP", "ATPMTN", "ATPHST", "ATPA06", "ATPA07", "ATPA08", "ATPA09", "ATPA10", "ATEFCN", "ATAPTYPE", "ATMOADJ", "ATPLGRP", "ATEXCPL", "ATUPMX", "ATMNMXAJ", "ATMNMXRL", "ATTSTRSNM", "ATADJQTY")
VALUES({ml r."ATAST"}, {ml r."ATPRGR"}, {ml r."ATCPGP"}, {ml r."ATSDGR"}, {ml r."ATPRFR"}, {ml r."ATLBT"}, {ml r."ATGLC"}, {ml r."ATSBIF"}, {ml r."ATACNT"}, {ml r."ATLNTY"}, {ml r."ATMDED"}, {ml r."ATABAS"}, {ml r."ATOLVL"}, {ml r."ATTXB"}, {ml r."ATPA01"}, {ml r."ATPA02"}, {ml r."ATPA03"}, {ml r."ATPA04"}, {ml r."ATPA05"}, {ml r."ATENBM"}, {ml r."ATSRFLAG"}, {ml r."ATUSADJ"}, {ml r."ATATIER"}, {ml r."ATBTIER"}, {ml r."ATBNAD"}, {ml r."ATAPRP1"}, {ml r."ATAPRP2"}, {ml r."ATAPRP3"}, {ml r."ATAPRP4"}, {ml r."ATAPRP5"}, {ml r."ATAPRP6"}, {ml r."ATADJGRP"}, {ml r."ATMEADJ"}, {ml r."ATPDCL"}, {ml r."ATUSER"}, {ml r."ATPID"}, {ml r."ATJOBN"}, {ml r."ATUPMJ"}, {ml r."ATTDAY"}, {ml r."ATDIDP"}, {ml r."ATPMTN"}, {ml r."ATPHST"}, {ml r."ATPA06"}, {ml r."ATPA07"}, {ml r."ATPA08"}, {ml r."ATPA09"}, {ml r."ATPA10"}, {ml r."ATEFCN"}, {ml r."ATAPTYPE"}, {ml r."ATMOADJ"}, {ml r."ATPLGRP"}, {ml r."ATEXCPL"}, {ml r."ATUPMX"}, {ml r."ATMNMXAJ"}, {ml r."ATMNMXRL"}, {ml r."ATTSTRSNM"}, {ml r."ATADJQTY"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4071',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4071"
SET "ATPRGR" = {ml r."ATPRGR"}, "ATCPGP" = {ml r."ATCPGP"}, "ATSDGR" = {ml r."ATSDGR"}, "ATPRFR" = {ml r."ATPRFR"}, "ATLBT" = {ml r."ATLBT"}, "ATGLC" = {ml r."ATGLC"}, "ATSBIF" = {ml r."ATSBIF"}, "ATACNT" = {ml r."ATACNT"}, "ATLNTY" = {ml r."ATLNTY"}, "ATMDED" = {ml r."ATMDED"}, "ATABAS" = {ml r."ATABAS"}, "ATOLVL" = {ml r."ATOLVL"}, "ATTXB" = {ml r."ATTXB"}, "ATPA01" = {ml r."ATPA01"}, "ATPA02" = {ml r."ATPA02"}, "ATPA03" = {ml r."ATPA03"}, "ATPA04" = {ml r."ATPA04"}, "ATPA05" = {ml r."ATPA05"}, "ATENBM" = {ml r."ATENBM"}, "ATSRFLAG" = {ml r."ATSRFLAG"}, "ATUSADJ" = {ml r."ATUSADJ"}, "ATATIER" = {ml r."ATATIER"}, "ATBTIER" = {ml r."ATBTIER"}, "ATBNAD" = {ml r."ATBNAD"}, "ATAPRP1" = {ml r."ATAPRP1"}, "ATAPRP2" = {ml r."ATAPRP2"}, "ATAPRP3" = {ml r."ATAPRP3"}, "ATAPRP4" = {ml r."ATAPRP4"}, "ATAPRP5" = {ml r."ATAPRP5"}, "ATAPRP6" = {ml r."ATAPRP6"}, "ATADJGRP" = {ml r."ATADJGRP"}, "ATMEADJ" = {ml r."ATMEADJ"}, "ATPDCL" = {ml r."ATPDCL"}, "ATUSER" = {ml r."ATUSER"}, "ATPID" = {ml r."ATPID"}, "ATJOBN" = {ml r."ATJOBN"}, "ATUPMJ" = {ml r."ATUPMJ"}, "ATTDAY" = {ml r."ATTDAY"}, "ATDIDP" = {ml r."ATDIDP"}, "ATPMTN" = {ml r."ATPMTN"}, "ATPHST" = {ml r."ATPHST"}, "ATPA06" = {ml r."ATPA06"}, "ATPA07" = {ml r."ATPA07"}, "ATPA08" = {ml r."ATPA08"}, "ATPA09" = {ml r."ATPA09"}, "ATPA10" = {ml r."ATPA10"}, "ATEFCN" = {ml r."ATEFCN"}, "ATAPTYPE" = {ml r."ATAPTYPE"}, "ATMOADJ" = {ml r."ATMOADJ"}, "ATPLGRP" = {ml r."ATPLGRP"}, "ATEXCPL" = {ml r."ATEXCPL"}, "ATUPMX" = {ml r."ATUPMX"}, "ATMNMXAJ" = {ml r."ATMNMXAJ"}, "ATMNMXRL" = {ml r."ATMNMXRL"}, "ATTSTRSNM" = {ml r."ATTSTRSNM"}, "ATADJQTY" = {ml r."ATADJQTY"}
WHERE "ATAST" = {ml r."ATAST"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4071',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4071"
WHERE "ATAST" = {ml r."ATAST"}
 
'

/* upload_delete - End */

/* Table :F4072 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4072',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F4072"."ADAST"  ,"BUSDTA"."F4072"."ADITM"  ,"BUSDTA"."F4072"."ADLITM"  ,"BUSDTA"."F4072"."ADAITM"  ,
 "BUSDTA"."F4072"."ADAN8"  ,"BUSDTA"."F4072"."ADIGID"  ,"BUSDTA"."F4072"."ADCGID"  ,"BUSDTA"."F4072"."ADOGID"  ,"BUSDTA"."F4072"."ADCRCD"  ,
 "BUSDTA"."F4072"."ADUOM"  ,"BUSDTA"."F4072"."ADMNQ"  ,"BUSDTA"."F4072"."ADEFTJ"  ,"BUSDTA"."F4072"."ADEXDJ"  ,"BUSDTA"."F4072"."ADBSCD"  ,
 "BUSDTA"."F4072"."ADLEDG"  ,"BUSDTA"."F4072"."ADFRMN"  ,"BUSDTA"."F4072"."ADFVTR"  ,"BUSDTA"."F4072"."ADFGY"  ,"BUSDTA"."F4072"."ADATID"  ,
 "BUSDTA"."F4072"."ADNBRORD"  ,"BUSDTA"."F4072"."ADUOMVID"  ,"BUSDTA"."F4072"."ADFVUM"  ,"BUSDTA"."F4072"."ADPARTFG"  ,
 "BUSDTA"."F4072"."ADAPRS"  ,"BUSDTA"."F4072"."ADUPMJ"  ,"BUSDTA"."F4072"."ADTDAY"  ,"BUSDTA"."F4072"."ADBKTPID"  ,
 "BUSDTA"."F4072"."ADCRCDVID"  ,"BUSDTA"."F4072"."ADRULENAME"    
 FROM "BUSDTA"."F4072"  
 WHERE "BUSDTA"."F4072"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4072',
	'download_delete_cursor',
	'sql',

'
 
 
SELECT "BUSDTA"."F4072_del"."ADAST"
,"BUSDTA"."F4072_del"."ADITM"
,"BUSDTA"."F4072_del"."ADAN8"
,"BUSDTA"."F4072_del"."ADIGID"
,"BUSDTA"."F4072_del"."ADCGID"
,"BUSDTA"."F4072_del"."ADOGID"
,"BUSDTA"."F4072_del"."ADCRCD"
,"BUSDTA"."F4072_del"."ADUOM"
,"BUSDTA"."F4072_del"."ADMNQ"
,"BUSDTA"."F4072_del"."ADEXDJ"
,"BUSDTA"."F4072_del"."ADUPMJ"
,"BUSDTA"."F4072_del"."ADTDAY"

FROM "BUSDTA"."F4072_del"
WHERE "BUSDTA"."F4072_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4072',
	'upload_insert',
	'sql',

'
 
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4072"
("ADAST", "ADITM", "ADLITM", "ADAITM", "ADAN8", "ADIGID", "ADCGID", "ADOGID", "ADCRCD", "ADUOM", "ADMNQ", "ADEFTJ", "ADEXDJ", "ADBSCD", "ADLEDG", "ADFRMN", "ADFVTR", "ADFGY", "ADATID", "ADNBRORD", "ADUOMVID", "ADFVUM", "ADPARTFG", "ADAPRS", "ADUPMJ", "ADTDAY", "ADBKTPID", "ADCRCDVID", "ADRULENAME")
VALUES({ml r."ADAST"}, {ml r."ADITM"}, {ml r."ADLITM"}, {ml r."ADAITM"}, {ml r."ADAN8"}, {ml r."ADIGID"}, {ml r."ADCGID"}, {ml r."ADOGID"}, {ml r."ADCRCD"}, {ml r."ADUOM"}, {ml r."ADMNQ"}, {ml r."ADEFTJ"}, {ml r."ADEXDJ"}, {ml r."ADBSCD"}, {ml r."ADLEDG"}, {ml r."ADFRMN"}, {ml r."ADFVTR"}, {ml r."ADFGY"}, {ml r."ADATID"}, {ml r."ADNBRORD"}, {ml r."ADUOMVID"}, {ml r."ADFVUM"}, {ml r."ADPARTFG"}, {ml r."ADAPRS"}, {ml r."ADUPMJ"}, {ml r."ADTDAY"}, {ml r."ADBKTPID"}, {ml r."ADCRCDVID"}, {ml r."ADRULENAME"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4072',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4072"
SET "ADLITM" = {ml r."ADLITM"}, "ADAITM" = {ml r."ADAITM"}, "ADEFTJ" = {ml r."ADEFTJ"}, "ADBSCD" = {ml r."ADBSCD"}, "ADLEDG" = {ml r."ADLEDG"}, "ADFRMN" = {ml r."ADFRMN"}, "ADFVTR" = {ml r."ADFVTR"}, "ADFGY" = {ml r."ADFGY"}, "ADATID" = {ml r."ADATID"}, "ADNBRORD" = {ml r."ADNBRORD"}, "ADUOMVID" = {ml r."ADUOMVID"}, "ADFVUM" = {ml r."ADFVUM"}, "ADPARTFG" = {ml r."ADPARTFG"}, "ADAPRS" = {ml r."ADAPRS"}, "ADBKTPID" = {ml r."ADBKTPID"}, "ADCRCDVID" = {ml r."ADCRCDVID"}, "ADRULENAME" = {ml r."ADRULENAME"}
WHERE "ADAST" = {ml r."ADAST"} AND "ADITM" = {ml r."ADITM"} AND "ADAN8" = {ml r."ADAN8"} AND "ADIGID" = {ml r."ADIGID"} AND "ADCGID" = {ml r."ADCGID"} AND "ADOGID" = {ml r."ADOGID"} AND "ADCRCD" = {ml r."ADCRCD"} AND "ADUOM" = {ml r."ADUOM"} AND "ADMNQ" = {ml r."ADMNQ"} AND "ADEXDJ" = {ml r."ADEXDJ"} AND "ADUPMJ" = {ml r."ADUPMJ"} AND "ADTDAY" = {ml r."ADTDAY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4072',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4072"
WHERE "ADAST" = {ml r."ADAST"} AND "ADITM" = {ml r."ADITM"} AND "ADAN8" = {ml r."ADAN8"} AND "ADIGID" = {ml r."ADIGID"} AND "ADCGID" = {ml r."ADCGID"} AND "ADOGID" = {ml r."ADOGID"} AND "ADCRCD" = {ml r."ADCRCD"} AND "ADUOM" = {ml r."ADUOM"} AND "ADMNQ" = {ml r."ADMNQ"} AND "ADEXDJ" = {ml r."ADEXDJ"} AND "ADUPMJ" = {ml r."ADUPMJ"} AND "ADTDAY" = {ml r."ADTDAY"}
 

'

/* upload_delete - End */

/* Table :F4075 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4075',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F4075"."VBVBT"  ,"BUSDTA"."F4075"."VBCRCD"  ,"BUSDTA"."F4075"."VBUOM"  ,"BUSDTA"."F4075"."VBUPRC"  ,"BUSDTA"."F4075"."VBEFTJ"  ,
     "BUSDTA"."F4075"."VBEXDJ"  ,"BUSDTA"."F4075"."VBAPRS"  ,"BUSDTA"."F4075"."VBUPMJ"  ,"BUSDTA"."F4075"."VBTDAY"    
     FROM "BUSDTA"."F4075"  
     WHERE "BUSDTA"."F4075"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4075',
	'download_delete_cursor',
	'sql',

'
 
 
SELECT "BUSDTA"."F4075_del"."VBVBT"
,"BUSDTA"."F4075_del"."VBEFTJ"
,"BUSDTA"."F4075_del"."VBUPMJ"
,"BUSDTA"."F4075_del"."VBTDAY"

FROM "BUSDTA"."F4075_del"
WHERE "BUSDTA"."F4075_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4075',
	'upload_insert',
	'sql',

'
 
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4075"
("VBVBT", "VBCRCD", "VBUOM", "VBUPRC", "VBEFTJ", "VBEXDJ", "VBAPRS", "VBUPMJ", "VBTDAY")
VALUES({ml r."VBVBT"}, {ml r."VBCRCD"}, {ml r."VBUOM"}, {ml r."VBUPRC"}, {ml r."VBEFTJ"}, {ml r."VBEXDJ"}, {ml r."VBAPRS"}, {ml r."VBUPMJ"}, {ml r."VBTDAY"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4075',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4075"
SET "VBCRCD" = {ml r."VBCRCD"}, "VBUOM" = {ml r."VBUOM"}, "VBUPRC" = {ml r."VBUPRC"}, "VBEXDJ" = {ml r."VBEXDJ"}, "VBAPRS" = {ml r."VBAPRS"}
WHERE "VBVBT" = {ml r."VBVBT"} AND "VBEFTJ" = {ml r."VBEFTJ"} AND "VBUPMJ" = {ml r."VBUPMJ"} AND "VBTDAY" = {ml r."VBTDAY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4075',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4075"
WHERE "VBVBT" = {ml r."VBVBT"} AND "VBEFTJ" = {ml r."VBEFTJ"} AND "VBUPMJ" = {ml r."VBUPMJ"} AND "VBTDAY" = {ml r."VBTDAY"}
 

'

/* upload_delete - End */

/* Table :F4076 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4076',
	'download_cursor',
	'sql',

'
 
 
     SELECT "BUSDTA"."F4076"."FMFRMN"  ,"BUSDTA"."F4076"."FMFML"  ,"BUSDTA"."F4076"."FMAPRS"  ,"BUSDTA"."F4076"."FMUPMJ"  ,"BUSDTA"."F4076"."FMTDAY"   
      FROM "BUSDTA"."F4076"  
      WHERE "BUSDTA"."F4076"."last_modified">= {ml s.last_table_download}     
 

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4076',
	'download_delete_cursor',
	'sql',

'
 
 
SELECT "BUSDTA"."F4076_del"."FMFRMN"
,"BUSDTA"."F4076_del"."FMUPMJ"
,"BUSDTA"."F4076_del"."FMTDAY"

FROM "BUSDTA"."F4076_del"
WHERE "BUSDTA"."F4076_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4076',
	'upload_insert',
	'sql',

'
 
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4076"
("FMFRMN", "FMFML", "FMAPRS", "FMUPMJ", "FMTDAY")
VALUES({ml r."FMFRMN"}, {ml r."FMFML"}, {ml r."FMAPRS"}, {ml r."FMUPMJ"}, {ml r."FMTDAY"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4076',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4076"
SET "FMFML" = {ml r."FMFML"}, "FMAPRS" = {ml r."FMAPRS"}
WHERE "FMFRMN" = {ml r."FMFRMN"} AND "FMUPMJ" = {ml r."FMUPMJ"} AND "FMTDAY" = {ml r."FMTDAY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4076',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4076"
WHERE "FMFRMN" = {ml r."FMFRMN"} AND "FMUPMJ" = {ml r."FMUPMJ"} AND "FMTDAY" = {ml r."FMTDAY"}
 

'

/* upload_delete - End */

/* Table :F4092 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4092',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F4092"."GPGPTY"  ,"BUSDTA"."F4092"."GPGPC"  ,"BUSDTA"."F4092"."GPDL01"  ,"BUSDTA"."F4092"."GPGPK1"  ,
     "BUSDTA"."F4092"."GPGPK2"  ,"BUSDTA"."F4092"."GPGPK3"  ,"BUSDTA"."F4092"."GPGPK4"  ,"BUSDTA"."F4092"."GPGPK5"  ,"BUSDTA"."F4092"."GPGPK6"  ,
     "BUSDTA"."F4092"."GPGPK7"  ,"BUSDTA"."F4092"."GPGPK8"  ,"BUSDTA"."F4092"."GPGPK9"  ,"BUSDTA"."F4092"."GPGPK10"    
     FROM "BUSDTA"."F4092"  
     WHERE "BUSDTA"."F4092"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4092',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F4092_del"."GPGPTY"
,"BUSDTA"."F4092_del"."GPGPC"

FROM "BUSDTA"."F4092_del"
WHERE "BUSDTA"."F4092_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4092',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4092"
("GPGPTY", "GPGPC", "GPDL01", "GPGPK1", "GPGPK2", "GPGPK3", "GPGPK4", "GPGPK5", "GPGPK6", "GPGPK7", "GPGPK8", "GPGPK9", "GPGPK10")
VALUES({ml r."GPGPTY"}, {ml r."GPGPC"}, {ml r."GPDL01"}, {ml r."GPGPK1"}, {ml r."GPGPK2"}, {ml r."GPGPK3"}, {ml r."GPGPK4"}, {ml r."GPGPK5"}, {ml r."GPGPK6"}, {ml r."GPGPK7"}, {ml r."GPGPK8"}, {ml r."GPGPK9"}, {ml r."GPGPK10"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4092',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4092"
SET "GPDL01" = {ml r."GPDL01"}, "GPGPK1" = {ml r."GPGPK1"}, "GPGPK2" = {ml r."GPGPK2"}, "GPGPK3" = {ml r."GPGPK3"}, "GPGPK4" = {ml r."GPGPK4"}, "GPGPK5" = {ml r."GPGPK5"}, "GPGPK6" = {ml r."GPGPK6"}, "GPGPK7" = {ml r."GPGPK7"}, "GPGPK8" = {ml r."GPGPK8"}, "GPGPK9" = {ml r."GPGPK9"}, "GPGPK10" = {ml r."GPGPK10"}
WHERE "GPGPTY" = {ml r."GPGPTY"} AND "GPGPC" = {ml r."GPGPC"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4092',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4092"
WHERE "GPGPTY" = {ml r."GPGPTY"} AND "GPGPC" = {ml r."GPGPC"}
 

'

/* upload_delete - End */

/* Table :F40941 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40941',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F40941"."IKPRGR"  ,"BUSDTA"."F40941"."IKIGP1"  ,"BUSDTA"."F40941"."IKIGP2"  ,"BUSDTA"."F40941"."IKIGP3"  ,
 "BUSDTA"."F40941"."IKIGP4"  ,"BUSDTA"."F40941"."IKIGP5"  ,"BUSDTA"."F40941"."IKIGP6"  ,"BUSDTA"."F40941"."IKIGP7"  ,
 "BUSDTA"."F40941"."IKIGP8"  ,"BUSDTA"."F40941"."IKIGP9"  ,"BUSDTA"."F40941"."IKIGP10"  ,"BUSDTA"."F40941"."IKIGID"    
 FROM "BUSDTA"."F40941"  
 WHERE "BUSDTA"."F40941"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40941',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F40941_del"."IKPRGR"
,"BUSDTA"."F40941_del"."IKIGP1"
,"BUSDTA"."F40941_del"."IKIGP2"
,"BUSDTA"."F40941_del"."IKIGP3"
,"BUSDTA"."F40941_del"."IKIGP4"
,"BUSDTA"."F40941_del"."IKIGP5"
,"BUSDTA"."F40941_del"."IKIGP6"
,"BUSDTA"."F40941_del"."IKIGP7"
,"BUSDTA"."F40941_del"."IKIGP8"
,"BUSDTA"."F40941_del"."IKIGP9"
,"BUSDTA"."F40941_del"."IKIGP10"

FROM "BUSDTA"."F40941_del"
WHERE "BUSDTA"."F40941_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40941',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F40941"
("IKPRGR", "IKIGP1", "IKIGP2", "IKIGP3", "IKIGP4", "IKIGP5", "IKIGP6", "IKIGP7", "IKIGP8", "IKIGP9", "IKIGP10", "IKIGID")
VALUES({ml r."IKPRGR"}, {ml r."IKIGP1"}, {ml r."IKIGP2"}, {ml r."IKIGP3"}, {ml r."IKIGP4"}, {ml r."IKIGP5"}, {ml r."IKIGP6"}, {ml r."IKIGP7"}, {ml r."IKIGP8"}, {ml r."IKIGP9"}, {ml r."IKIGP10"}, {ml r."IKIGID"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40941',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F40941"
SET "IKIGID" = {ml r."IKIGID"}
WHERE "IKPRGR" = {ml r."IKPRGR"} AND "IKIGP1" = {ml r."IKIGP1"} AND "IKIGP2" = {ml r."IKIGP2"} AND "IKIGP3" = {ml r."IKIGP3"} AND "IKIGP4" = {ml r."IKIGP4"} AND "IKIGP5" = {ml r."IKIGP5"} AND "IKIGP6" = {ml r."IKIGP6"} AND "IKIGP7" = {ml r."IKIGP7"} AND "IKIGP8" = {ml r."IKIGP8"} AND "IKIGP9" = {ml r."IKIGP9"} AND "IKIGP10" = {ml r."IKIGP10"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40941',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F40941"
WHERE "IKPRGR" = {ml r."IKPRGR"} AND "IKIGP1" = {ml r."IKIGP1"} AND "IKIGP2" = {ml r."IKIGP2"} AND "IKIGP3" = {ml r."IKIGP3"} AND "IKIGP4" = {ml r."IKIGP4"} AND "IKIGP5" = {ml r."IKIGP5"} AND "IKIGP6" = {ml r."IKIGP6"} AND "IKIGP7" = {ml r."IKIGP7"} AND "IKIGP8" = {ml r."IKIGP8"} AND "IKIGP9" = {ml r."IKIGP9"} AND "IKIGP10" = {ml r."IKIGP10"}
 

'

/* upload_delete - End */

/* Table :F40942 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40942',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F40942"."CKCPGP"  ,"BUSDTA"."F40942"."CKCGP1"  ,"BUSDTA"."F40942"."CKCGP2"  ,"BUSDTA"."F40942"."CKCGP3"  ,
 "BUSDTA"."F40942"."CKCGP4"  ,"BUSDTA"."F40942"."CKCGP5"  ,"BUSDTA"."F40942"."CKCGP6"  ,"BUSDTA"."F40942"."CKCGP7"  ,
 "BUSDTA"."F40942"."CKCGP8"  ,"BUSDTA"."F40942"."CKCGP9"  ,"BUSDTA"."F40942"."CKCGP10"  ,"BUSDTA"."F40942"."CKCGID"    
 FROM "BUSDTA"."F40942"  
 WHERE "BUSDTA"."F40942"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40942',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F40942_del"."CKCPGP"
,"BUSDTA"."F40942_del"."CKCGP1"
,"BUSDTA"."F40942_del"."CKCGP2"
,"BUSDTA"."F40942_del"."CKCGP3"
,"BUSDTA"."F40942_del"."CKCGP4"
,"BUSDTA"."F40942_del"."CKCGP5"
,"BUSDTA"."F40942_del"."CKCGP6"
,"BUSDTA"."F40942_del"."CKCGP7"
,"BUSDTA"."F40942_del"."CKCGP8"
,"BUSDTA"."F40942_del"."CKCGP9"
,"BUSDTA"."F40942_del"."CKCGP10"

FROM "BUSDTA"."F40942_del"
WHERE "BUSDTA"."F40942_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40942',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F40942"
("CKCPGP", "CKCGP1", "CKCGP2", "CKCGP3", "CKCGP4", "CKCGP5", "CKCGP6", "CKCGP7", "CKCGP8", "CKCGP9", "CKCGP10", "CKCGID")
VALUES({ml r."CKCPGP"}, {ml r."CKCGP1"}, {ml r."CKCGP2"}, {ml r."CKCGP3"}, {ml r."CKCGP4"}, {ml r."CKCGP5"}, {ml r."CKCGP6"}, {ml r."CKCGP7"}, {ml r."CKCGP8"}, {ml r."CKCGP9"}, {ml r."CKCGP10"}, {ml r."CKCGID"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40942',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F40942"
SET "CKCGID" = {ml r."CKCGID"}
WHERE "CKCPGP" = {ml r."CKCPGP"} AND "CKCGP1" = {ml r."CKCGP1"} AND "CKCGP2" = {ml r."CKCGP2"} AND "CKCGP3" = {ml r."CKCGP3"} AND "CKCGP4" = {ml r."CKCGP4"} AND "CKCGP5" = {ml r."CKCGP5"} AND "CKCGP6" = {ml r."CKCGP6"} AND "CKCGP7" = {ml r."CKCGP7"} AND "CKCGP8" = {ml r."CKCGP8"} AND "CKCGP9" = {ml r."CKCGP9"} AND "CKCGP10" = {ml r."CKCGP10"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40942',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F40942"
WHERE "CKCPGP" = {ml r."CKCPGP"} AND "CKCGP1" = {ml r."CKCGP1"} AND "CKCGP2" = {ml r."CKCGP2"} AND "CKCGP3" = {ml r."CKCGP3"} AND "CKCGP4" = {ml r."CKCGP4"} AND "CKCGP5" = {ml r."CKCGP5"} AND "CKCGP6" = {ml r."CKCGP6"} AND "CKCGP7" = {ml r."CKCGP7"} AND "CKCGP8" = {ml r."CKCGP8"} AND "CKCGP9" = {ml r."CKCGP9"} AND "CKCGP10" = {ml r."CKCGP10"}
 

'

/* upload_delete - End */

/* Table :F41002 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F41002',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F41002"."UMMCU"  ,"BUSDTA"."F41002"."UMITM"  ,"BUSDTA"."F41002"."UMUM"  ,"BUSDTA"."F41002"."UMRUM"  ,
 "BUSDTA"."F41002"."UMUSTR"  ,"BUSDTA"."F41002"."UMCONV"  ,"BUSDTA"."F41002"."UMCNV1"    
 FROM "BUSDTA"."F41002"  WHERE "BUSDTA"."F41002"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F41002',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F41002_del"."UMMCU"
,"BUSDTA"."F41002_del"."UMITM"
,"BUSDTA"."F41002_del"."UMUM"
,"BUSDTA"."F41002_del"."UMRUM"

FROM "BUSDTA"."F41002_del"
WHERE "BUSDTA"."F41002_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F41002',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F41002"
("UMMCU", "UMITM", "UMUM", "UMRUM", "UMUSTR", "UMCONV", "UMCNV1")
VALUES({ml r."UMMCU"}, {ml r."UMITM"}, {ml r."UMUM"}, {ml r."UMRUM"}, {ml r."UMUSTR"}, {ml r."UMCONV"}, {ml r."UMCNV1"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F41002',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F41002"
SET "UMUSTR" = {ml r."UMUSTR"}, "UMCONV" = {ml r."UMCONV"}, "UMCNV1" = {ml r."UMCNV1"}
WHERE "UMMCU" = {ml r."UMMCU"} AND "UMITM" = {ml r."UMITM"} AND "UMUM" = {ml r."UMUM"} AND "UMRUM" = {ml r."UMRUM"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F41002',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F41002"
WHERE "UMMCU" = {ml r."UMMCU"} AND "UMITM" = {ml r."UMITM"} AND "UMUM" = {ml r."UMUM"} AND "UMRUM" = {ml r."UMRUM"}
 

'

/* upload_delete - End */

/* Table :F4101 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4101',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F4101"."IMITM"  ,"BUSDTA"."F4101"."IMLITM"  ,"BUSDTA"."F4101"."IMAITM"  ,"BUSDTA"."F4101"."IMDSC1"  ,
 "BUSDTA"."F4101"."IMDSC2"  ,"BUSDTA"."F4101"."IMSRP1"  ,"BUSDTA"."F4101"."IMSRP2"  ,"BUSDTA"."F4101"."IMSRP3"  ,
 "BUSDTA"."F4101"."IMSRP4"  ,"BUSDTA"."F4101"."IMSRP5"  ,"BUSDTA"."F4101"."IMSRP6"  ,"BUSDTA"."F4101"."IMSRP7"  ,
 "BUSDTA"."F4101"."IMSRP8"  ,"BUSDTA"."F4101"."IMSRP9"  ,"BUSDTA"."F4101"."IMSRP0"  ,"BUSDTA"."F4101"."IMPRP1"  ,
 "BUSDTA"."F4101"."IMPRP2"  ,"BUSDTA"."F4101"."IMPRP3"  ,"BUSDTA"."F4101"."IMPRP4"  ,"BUSDTA"."F4101"."IMPRP5"  ,
 "BUSDTA"."F4101"."IMPRP6"  ,"BUSDTA"."F4101"."IMPRP7"  ,"BUSDTA"."F4101"."IMPRP8"  ,"BUSDTA"."F4101"."IMPRP9"  ,
 "BUSDTA"."F4101"."IMPRP0"  ,"BUSDTA"."F4101"."IMCDCD"  ,"BUSDTA"."F4101"."IMPDGR"  ,"BUSDTA"."F4101"."IMDSGP"  ,
 "BUSDTA"."F4101"."IMPRGR"  ,"BUSDTA"."F4101"."IMRPRC"  ,"BUSDTA"."F4101"."IMORPR"  ,"BUSDTA"."F4101"."IMVCUD"  ,
 "BUSDTA"."F4101"."IMUOM1"  ,"BUSDTA"."F4101"."IMUOM2"  ,"BUSDTA"."F4101"."IMUOM4"  ,"BUSDTA"."F4101"."IMUOM6"  ,
 "BUSDTA"."F4101"."IMUWUM"  ,"BUSDTA"."F4101"."IMUVM1"  ,"BUSDTA"."F4101"."IMCYCL"  ,"BUSDTA"."F4101"."IMGLPT"  ,
 "BUSDTA"."F4101"."IMPLEV"  ,"BUSDTA"."F4101"."IMPPLV"  ,"BUSDTA"."F4101"."IMCLEV"  ,"BUSDTA"."F4101"."IMCKAV"  ,
 "BUSDTA"."F4101"."IMSRCE"  ,"BUSDTA"."F4101"."IMSTKT"  ,"BUSDTA"."F4101"."IMLNTY"  ,"BUSDTA"."F4101"."IMBACK"  ,
 "BUSDTA"."F4101"."IMIFLA"  ,"BUSDTA"."F4101"."IMTFLA"  ,"BUSDTA"."F4101"."IMINMG"  ,"BUSDTA"."F4101"."IMABCS"  ,
 "BUSDTA"."F4101"."IMABCM"  ,"BUSDTA"."F4101"."IMABCI"  ,"BUSDTA"."F4101"."IMOVR"  ,"BUSDTA"."F4101"."IMCMCG"  ,
 "BUSDTA"."F4101"."IMSRNR"  ,"BUSDTA"."F4101"."IMFIFO"  ,"BUSDTA"."F4101"."IMLOTS"  ,"BUSDTA"."F4101"."IMSLD"  ,
 "BUSDTA"."F4101"."IMPCTM"  ,"BUSDTA"."F4101"."IMMMPC"  ,"BUSDTA"."F4101"."IMCMGL"  ,"BUSDTA"."F4101"."IMUPCN"  ,
 "BUSDTA"."F4101"."IMUMUP"  ,"BUSDTA"."F4101"."IMUMDF"  ,"BUSDTA"."F4101"."IMBBDD"  ,"BUSDTA"."F4101"."IMCMDM"  ,
 "BUSDTA"."F4101"."IMLECM"  ,"BUSDTA"."F4101"."IMLEDD"  ,"BUSDTA"."F4101"."IMPEFD"  ,"BUSDTA"."F4101"."IMSBDD"  ,
 "BUSDTA"."F4101"."IMU1DD"  ,"BUSDTA"."F4101"."IMU2DD"  ,"BUSDTA"."F4101"."IMU3DD"  ,"BUSDTA"."F4101"."IMU4DD"  ,
 "BUSDTA"."F4101"."IMU5DD"  ,"BUSDTA"."F4101"."IMLNPA"  ,"BUSDTA"."F4101"."IMLOTC"    
 FROM "BUSDTA"."F4101", BUSDTA.F4102, BUSDTA.Route_Master rm  
 WHERE ("BUSDTA"."F4101"."last_modified" >= {ml s.last_table_download} or BUSDTA.F4102.last_modified >= {ml s.last_table_download}    
 or rm.last_modified >= {ml s.last_table_download})   AND IBITM = IMITM 
 AND IBMCU = rm.BranchNumber  AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4101',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F4101_del"."IMITM"

FROM "BUSDTA"."F4101_del"
WHERE "BUSDTA"."F4101_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4101',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4101"
("IMITM", "IMLITM", "IMAITM", "IMDSC1", "IMDSC2", "IMSRP1", "IMSRP2", "IMSRP3", "IMSRP4", "IMSRP5", "IMSRP6", "IMSRP7", "IMSRP8", "IMSRP9", "IMSRP0", "IMPRP1", "IMPRP2", "IMPRP3", "IMPRP4", "IMPRP5", "IMPRP6", "IMPRP7", "IMPRP8", "IMPRP9", "IMPRP0", "IMCDCD", "IMPDGR", "IMDSGP", "IMPRGR", "IMRPRC", "IMORPR", "IMVCUD", "IMUOM1", "IMUOM2", "IMUOM4", "IMUOM6", "IMUWUM", "IMUVM1", "IMCYCL", "IMGLPT", "IMPLEV", "IMPPLV", "IMCLEV", "IMCKAV", "IMSRCE", "IMSTKT", "IMLNTY", "IMBACK", "IMIFLA", "IMTFLA", "IMINMG", "IMABCS", "IMABCM", "IMABCI", "IMOVR", "IMCMCG", "IMSRNR", "IMFIFO", "IMLOTS", "IMSLD", "IMPCTM", "IMMMPC", "IMCMGL", "IMUPCN", "IMUMUP", "IMUMDF", "IMBBDD", "IMCMDM", "IMLECM", "IMLEDD", "IMPEFD", "IMSBDD", "IMU1DD", "IMU2DD", "IMU3DD", "IMU4DD", "IMU5DD", "IMLNPA", "IMLOTC")
VALUES({ml r."IMITM"}, {ml r."IMLITM"}, {ml r."IMAITM"}, {ml r."IMDSC1"}, {ml r."IMDSC2"}, {ml r."IMSRP1"}, {ml r."IMSRP2"}, {ml r."IMSRP3"}, {ml r."IMSRP4"}, {ml r."IMSRP5"}, {ml r."IMSRP6"}, {ml r."IMSRP7"}, {ml r."IMSRP8"}, {ml r."IMSRP9"}, {ml r."IMSRP0"}, {ml r."IMPRP1"}, {ml r."IMPRP2"}, {ml r."IMPRP3"}, {ml r."IMPRP4"}, {ml r."IMPRP5"}, {ml r."IMPRP6"}, {ml r."IMPRP7"}, {ml r."IMPRP8"}, {ml r."IMPRP9"}, {ml r."IMPRP0"}, {ml r."IMCDCD"}, {ml r."IMPDGR"}, {ml r."IMDSGP"}, {ml r."IMPRGR"}, {ml r."IMRPRC"}, {ml r."IMORPR"}, {ml r."IMVCUD"}, {ml r."IMUOM1"}, {ml r."IMUOM2"}, {ml r."IMUOM4"}, {ml r."IMUOM6"}, {ml r."IMUWUM"}, {ml r."IMUVM1"}, {ml r."IMCYCL"}, {ml r."IMGLPT"}, {ml r."IMPLEV"}, {ml r."IMPPLV"}, {ml r."IMCLEV"}, {ml r."IMCKAV"}, {ml r."IMSRCE"}, {ml r."IMSTKT"}, {ml r."IMLNTY"}, {ml r."IMBACK"}, {ml r."IMIFLA"}, {ml r."IMTFLA"}, {ml r."IMINMG"}, {ml r."IMABCS"}, {ml r."IMABCM"}, {ml r."IMABCI"}, {ml r."IMOVR"}, {ml r."IMCMCG"}, {ml r."IMSRNR"}, {ml r."IMFIFO"}, {ml r."IMLOTS"}, {ml r."IMSLD"}, {ml r."IMPCTM"}, {ml r."IMMMPC"}, {ml r."IMCMGL"}, {ml r."IMUPCN"}, {ml r."IMUMUP"}, {ml r."IMUMDF"}, {ml r."IMBBDD"}, {ml r."IMCMDM"}, {ml r."IMLECM"}, {ml r."IMLEDD"}, {ml r."IMPEFD"}, {ml r."IMSBDD"}, {ml r."IMU1DD"}, {ml r."IMU2DD"}, {ml r."IMU3DD"}, {ml r."IMU4DD"}, {ml r."IMU5DD"}, {ml r."IMLNPA"}, {ml r."IMLOTC"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4101',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4101"
SET "IMLITM" = {ml r."IMLITM"}, "IMAITM" = {ml r."IMAITM"}, "IMDSC1" = {ml r."IMDSC1"}, "IMDSC2" = {ml r."IMDSC2"}, "IMSRP1" = {ml r."IMSRP1"}, "IMSRP2" = {ml r."IMSRP2"}, "IMSRP3" = {ml r."IMSRP3"}, "IMSRP4" = {ml r."IMSRP4"}, "IMSRP5" = {ml r."IMSRP5"}, "IMSRP6" = {ml r."IMSRP6"}, "IMSRP7" = {ml r."IMSRP7"}, "IMSRP8" = {ml r."IMSRP8"}, "IMSRP9" = {ml r."IMSRP9"}, "IMSRP0" = {ml r."IMSRP0"}, "IMPRP1" = {ml r."IMPRP1"}, "IMPRP2" = {ml r."IMPRP2"}, "IMPRP3" = {ml r."IMPRP3"}, "IMPRP4" = {ml r."IMPRP4"}, "IMPRP5" = {ml r."IMPRP5"}, "IMPRP6" = {ml r."IMPRP6"}, "IMPRP7" = {ml r."IMPRP7"}, "IMPRP8" = {ml r."IMPRP8"}, "IMPRP9" = {ml r."IMPRP9"}, "IMPRP0" = {ml r."IMPRP0"}, "IMCDCD" = {ml r."IMCDCD"}, "IMPDGR" = {ml r."IMPDGR"}, "IMDSGP" = {ml r."IMDSGP"}, "IMPRGR" = {ml r."IMPRGR"}, "IMRPRC" = {ml r."IMRPRC"}, "IMORPR" = {ml r."IMORPR"}, "IMVCUD" = {ml r."IMVCUD"}, "IMUOM1" = {ml r."IMUOM1"}, "IMUOM2" = {ml r."IMUOM2"}, "IMUOM4" = {ml r."IMUOM4"}, "IMUOM6" = {ml r."IMUOM6"}, "IMUWUM" = {ml r."IMUWUM"}, "IMUVM1" = {ml r."IMUVM1"}, "IMCYCL" = {ml r."IMCYCL"}, "IMGLPT" = {ml r."IMGLPT"}, "IMPLEV" = {ml r."IMPLEV"}, "IMPPLV" = {ml r."IMPPLV"}, "IMCLEV" = {ml r."IMCLEV"}, "IMCKAV" = {ml r."IMCKAV"}, "IMSRCE" = {ml r."IMSRCE"}, "IMSTKT" = {ml r."IMSTKT"}, "IMLNTY" = {ml r."IMLNTY"}, "IMBACK" = {ml r."IMBACK"}, "IMIFLA" = {ml r."IMIFLA"}, "IMTFLA" = {ml r."IMTFLA"}, "IMINMG" = {ml r."IMINMG"}, "IMABCS" = {ml r."IMABCS"}, "IMABCM" = {ml r."IMABCM"}, "IMABCI" = {ml r."IMABCI"}, "IMOVR" = {ml r."IMOVR"}, "IMCMCG" = {ml r."IMCMCG"}, "IMSRNR" = {ml r."IMSRNR"}, "IMFIFO" = {ml r."IMFIFO"}, "IMLOTS" = {ml r."IMLOTS"}, "IMSLD" = {ml r."IMSLD"}, "IMPCTM" = {ml r."IMPCTM"}, "IMMMPC" = {ml r."IMMMPC"}, "IMCMGL" = {ml r."IMCMGL"}, "IMUPCN" = {ml r."IMUPCN"}, "IMUMUP" = {ml r."IMUMUP"}, "IMUMDF" = {ml r."IMUMDF"}, "IMBBDD" = {ml r."IMBBDD"}, "IMCMDM" = {ml r."IMCMDM"}, "IMLECM" = {ml r."IMLECM"}, "IMLEDD" = {ml r."IMLEDD"}, "IMPEFD" = {ml r."IMPEFD"}, "IMSBDD" = {ml r."IMSBDD"}, "IMU1DD" = {ml r."IMU1DD"}, "IMU2DD" = {ml r."IMU2DD"}, "IMU3DD" = {ml r."IMU3DD"}, "IMU4DD" = {ml r."IMU4DD"}, "IMU5DD" = {ml r."IMU5DD"}, "IMLNPA" = {ml r."IMLNPA"}, "IMLOTC" = {ml r."IMLOTC"}
WHERE "IMITM" = {ml r."IMITM"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4101',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4101"
WHERE "IMITM" = {ml r."IMITM"}
 

'

/* upload_delete - End */

/* Table :F4102 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4102',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F4102"."IBITM"  ,"BUSDTA"."F4102"."IBLITM"  ,"BUSDTA"."F4102"."IBAITM"  ,"BUSDTA"."F4102"."IBMCU"  ,
 "BUSDTA"."F4102"."IBSRP1"  ,"BUSDTA"."F4102"."IBSRP2"  ,"BUSDTA"."F4102"."IBSRP3"  ,"BUSDTA"."F4102"."IBSRP4"  ,
 "BUSDTA"."F4102"."IBSRP5"  ,"BUSDTA"."F4102"."IBSRP6"  ,"BUSDTA"."F4102"."IBSRP7"  ,"BUSDTA"."F4102"."IBSRP8"  ,
 "BUSDTA"."F4102"."IBSRP9"  ,"BUSDTA"."F4102"."IBSRP0"  ,"BUSDTA"."F4102"."IBPRP1"  ,"BUSDTA"."F4102"."IBPRP2"  ,
 "BUSDTA"."F4102"."IBPRP3"  ,"BUSDTA"."F4102"."IBPRP4"  ,"BUSDTA"."F4102"."IBPRP5"  ,"BUSDTA"."F4102"."IBPRP6"  ,
 "BUSDTA"."F4102"."IBPRP7"  ,"BUSDTA"."F4102"."IBPRP8"  ,"BUSDTA"."F4102"."IBPRP9"  ,"BUSDTA"."F4102"."IBPRP0"  ,
 "BUSDTA"."F4102"."IBCDCD"  ,"BUSDTA"."F4102"."IBPDGR"  ,"BUSDTA"."F4102"."IBDSGP"  ,"BUSDTA"."F4102"."IBGLPT"  ,
 "BUSDTA"."F4102"."IBORIG"  ,"BUSDTA"."F4102"."IBSAFE"  ,"BUSDTA"."F4102"."IBSLD"  ,"BUSDTA"."F4102"."IBCKAV"  ,
 "BUSDTA"."F4102"."IBSRCE"  ,"BUSDTA"."F4102"."IBLOTS"  ,"BUSDTA"."F4102"."IBMMPC"  ,"BUSDTA"."F4102"."IBPRGR"  ,
 "BUSDTA"."F4102"."IBRPRC"  ,"BUSDTA"."F4102"."IBORPR"  ,"BUSDTA"."F4102"."IBBACK"  ,"BUSDTA"."F4102"."IBIFLA"  ,
 "BUSDTA"."F4102"."IBABCS"  ,"BUSDTA"."F4102"."IBABCM"  ,"BUSDTA"."F4102"."IBABCI"  ,"BUSDTA"."F4102"."IBOVR"  ,
 "BUSDTA"."F4102"."IBSTKT"  ,"BUSDTA"."F4102"."IBLNTY"  ,"BUSDTA"."F4102"."IBFIFO"  ,"BUSDTA"."F4102"."IBCYCL"  ,
 "BUSDTA"."F4102"."IBINMG"  ,"BUSDTA"."F4102"."IBSRNR"  ,"BUSDTA"."F4102"."IBPCTM"  ,"BUSDTA"."F4102"."IBCMCG"  ,
 "BUSDTA"."F4102"."IBTAX1"  ,"BUSDTA"."F4102"."IBBBDD"  ,"BUSDTA"."F4102"."IBCMDM"  ,"BUSDTA"."F4102"."IBLECM"  ,
 "BUSDTA"."F4102"."IBLEDD"  ,"BUSDTA"."F4102"."IBMLOT"  ,"BUSDTA"."F4102"."IBSBDD"  ,"BUSDTA"."F4102"."IBU1DD"  ,
 "BUSDTA"."F4102"."IBU2DD"  ,"BUSDTA"."F4102"."IBU3DD"  ,"BUSDTA"."F4102"."IBU4DD"  ,"BUSDTA"."F4102"."IBU5DD"    
 FROM "BUSDTA"."F4102", BUSDTA.Route_Master rm  
 WHERE ("BUSDTA"."F4102"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download})    
 AND IBMCU = rm.BranchNumber   AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4102',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F4102_del"."IBITM"
,"BUSDTA"."F4102_del"."IBMCU"

FROM "BUSDTA"."F4102_del"
WHERE "BUSDTA"."F4102_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4102',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4102"
("IBITM", "IBLITM", "IBAITM", "IBMCU", "IBSRP1", "IBSRP2", "IBSRP3", "IBSRP4", "IBSRP5", "IBSRP6", "IBSRP7", "IBSRP8", "IBSRP9", "IBSRP0", "IBPRP1", "IBPRP2", "IBPRP3", "IBPRP4", "IBPRP5", "IBPRP6", "IBPRP7", "IBPRP8", "IBPRP9", "IBPRP0", "IBCDCD", "IBPDGR", "IBDSGP", "IBGLPT", "IBORIG", "IBSAFE", "IBSLD", "IBCKAV", "IBSRCE", "IBLOTS", "IBMMPC", "IBPRGR", "IBRPRC", "IBORPR", "IBBACK", "IBIFLA", "IBABCS", "IBABCM", "IBABCI", "IBOVR", "IBSTKT", "IBLNTY", "IBFIFO", "IBCYCL", "IBINMG", "IBSRNR", "IBPCTM", "IBCMCG", "IBTAX1", "IBBBDD", "IBCMDM", "IBLECM", "IBLEDD", "IBMLOT", "IBSBDD", "IBU1DD", "IBU2DD", "IBU3DD", "IBU4DD", "IBU5DD")
VALUES({ml r."IBITM"}, {ml r."IBLITM"}, {ml r."IBAITM"}, {ml r."IBMCU"}, {ml r."IBSRP1"}, {ml r."IBSRP2"}, {ml r."IBSRP3"}, {ml r."IBSRP4"}, {ml r."IBSRP5"}, {ml r."IBSRP6"}, {ml r."IBSRP7"}, {ml r."IBSRP8"}, {ml r."IBSRP9"}, {ml r."IBSRP0"}, {ml r."IBPRP1"}, {ml r."IBPRP2"}, {ml r."IBPRP3"}, {ml r."IBPRP4"}, {ml r."IBPRP5"}, {ml r."IBPRP6"}, {ml r."IBPRP7"}, {ml r."IBPRP8"}, {ml r."IBPRP9"}, {ml r."IBPRP0"}, {ml r."IBCDCD"}, {ml r."IBPDGR"}, {ml r."IBDSGP"}, {ml r."IBGLPT"}, {ml r."IBORIG"}, {ml r."IBSAFE"}, {ml r."IBSLD"}, {ml r."IBCKAV"}, {ml r."IBSRCE"}, {ml r."IBLOTS"}, {ml r."IBMMPC"}, {ml r."IBPRGR"}, {ml r."IBRPRC"}, {ml r."IBORPR"}, {ml r."IBBACK"}, {ml r."IBIFLA"}, {ml r."IBABCS"}, {ml r."IBABCM"}, {ml r."IBABCI"}, {ml r."IBOVR"}, {ml r."IBSTKT"}, {ml r."IBLNTY"}, {ml r."IBFIFO"}, {ml r."IBCYCL"}, {ml r."IBINMG"}, {ml r."IBSRNR"}, {ml r."IBPCTM"}, {ml r."IBCMCG"}, {ml r."IBTAX1"}, {ml r."IBBBDD"}, {ml r."IBCMDM"}, {ml r."IBLECM"}, {ml r."IBLEDD"}, {ml r."IBMLOT"}, {ml r."IBSBDD"}, {ml r."IBU1DD"}, {ml r."IBU2DD"}, {ml r."IBU3DD"}, {ml r."IBU4DD"}, {ml r."IBU5DD"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4102',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4102"
SET "IBLITM" = {ml r."IBLITM"}, "IBAITM" = {ml r."IBAITM"}, "IBSRP1" = {ml r."IBSRP1"}, "IBSRP2" = {ml r."IBSRP2"}, "IBSRP3" = {ml r."IBSRP3"}, "IBSRP4" = {ml r."IBSRP4"}, "IBSRP5" = {ml r."IBSRP5"}, "IBSRP6" = {ml r."IBSRP6"}, "IBSRP7" = {ml r."IBSRP7"}, "IBSRP8" = {ml r."IBSRP8"}, "IBSRP9" = {ml r."IBSRP9"}, "IBSRP0" = {ml r."IBSRP0"}, "IBPRP1" = {ml r."IBPRP1"}, "IBPRP2" = {ml r."IBPRP2"}, "IBPRP3" = {ml r."IBPRP3"}, "IBPRP4" = {ml r."IBPRP4"}, "IBPRP5" = {ml r."IBPRP5"}, "IBPRP6" = {ml r."IBPRP6"}, "IBPRP7" = {ml r."IBPRP7"}, "IBPRP8" = {ml r."IBPRP8"}, "IBPRP9" = {ml r."IBPRP9"}, "IBPRP0" = {ml r."IBPRP0"}, "IBCDCD" = {ml r."IBCDCD"}, "IBPDGR" = {ml r."IBPDGR"}, "IBDSGP" = {ml r."IBDSGP"}, "IBGLPT" = {ml r."IBGLPT"}, "IBORIG" = {ml r."IBORIG"}, "IBSAFE" = {ml r."IBSAFE"}, "IBSLD" = {ml r."IBSLD"}, "IBCKAV" = {ml r."IBCKAV"}, "IBSRCE" = {ml r."IBSRCE"}, "IBLOTS" = {ml r."IBLOTS"}, "IBMMPC" = {ml r."IBMMPC"}, "IBPRGR" = {ml r."IBPRGR"}, "IBRPRC" = {ml r."IBRPRC"}, "IBORPR" = {ml r."IBORPR"}, "IBBACK" = {ml r."IBBACK"}, "IBIFLA" = {ml r."IBIFLA"}, "IBABCS" = {ml r."IBABCS"}, "IBABCM" = {ml r."IBABCM"}, "IBABCI" = {ml r."IBABCI"}, "IBOVR" = {ml r."IBOVR"}, "IBSTKT" = {ml r."IBSTKT"}, "IBLNTY" = {ml r."IBLNTY"}, "IBFIFO" = {ml r."IBFIFO"}, "IBCYCL" = {ml r."IBCYCL"}, "IBINMG" = {ml r."IBINMG"}, "IBSRNR" = {ml r."IBSRNR"}, "IBPCTM" = {ml r."IBPCTM"}, "IBCMCG" = {ml r."IBCMCG"}, "IBTAX1" = {ml r."IBTAX1"}, "IBBBDD" = {ml r."IBBBDD"}, "IBCMDM" = {ml r."IBCMDM"}, "IBLECM" = {ml r."IBLECM"}, "IBLEDD" = {ml r."IBLEDD"}, "IBMLOT" = {ml r."IBMLOT"}, "IBSBDD" = {ml r."IBSBDD"}, "IBU1DD" = {ml r."IBU1DD"}, "IBU2DD" = {ml r."IBU2DD"}, "IBU3DD" = {ml r."IBU3DD"}, "IBU4DD" = {ml r."IBU4DD"}, "IBU5DD" = {ml r."IBU5DD"}
WHERE "IBITM" = {ml r."IBITM"} AND "IBMCU" = {ml r."IBMCU"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4102',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4102"
WHERE "IBITM" = {ml r."IBITM"} AND "IBMCU" = {ml r."IBMCU"}
 

'

/* upload_delete - End */

/* Table :F4106 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4106',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."F4106"."BPITM"  ,"BUSDTA"."F4106"."BPLITM"  ,"BUSDTA"."F4106"."BPMCU"  ,"BUSDTA"."F4106"."BPLOCN"  ,
     "BUSDTA"."F4106"."BPLOTN"  ,"BUSDTA"."F4106"."BPAN8"  ,"BUSDTA"."F4106"."BPIGID"  ,"BUSDTA"."F4106"."BPCGID"  ,
     "BUSDTA"."F4106"."BPLOTG"  ,"BUSDTA"."F4106"."BPFRMP"  ,"BUSDTA"."F4106"."BPCRCD"  ,"BUSDTA"."F4106"."BPUOM"  ,
     "BUSDTA"."F4106"."BPEFTJ"  ,"BUSDTA"."F4106"."BPEXDJ"  ,"BUSDTA"."F4106"."BPUPRC"  ,"BUSDTA"."F4106"."BPUPMJ"  ,
     "BUSDTA"."F4106"."BPTDAY"    
     FROM "BUSDTA"."F4106", BUSDTA.F4102, BUSDTA.Route_Master rm  
     WHERE ("BUSDTA"."F4106"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or "BUSDTA"."F4102"."last_modified" >= {ml s.last_table_download})    AND IBITM=BPITM AND IBMCU = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )       
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4106',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F4106_del"."BPITM"
,"BUSDTA"."F4106_del"."BPMCU"
,"BUSDTA"."F4106_del"."BPLOCN"
,"BUSDTA"."F4106_del"."BPLOTN"
,"BUSDTA"."F4106_del"."BPAN8"
,"BUSDTA"."F4106_del"."BPIGID"
,"BUSDTA"."F4106_del"."BPCGID"
,"BUSDTA"."F4106_del"."BPLOTG"
,"BUSDTA"."F4106_del"."BPFRMP"
,"BUSDTA"."F4106_del"."BPCRCD"
,"BUSDTA"."F4106_del"."BPUOM"
,"BUSDTA"."F4106_del"."BPEXDJ"
,"BUSDTA"."F4106_del"."BPUPMJ"
,"BUSDTA"."F4106_del"."BPTDAY"

FROM "BUSDTA"."F4106_del"
WHERE "BUSDTA"."F4106_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4106',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F4106"
("BPITM", "BPLITM", "BPMCU", "BPLOCN", "BPLOTN", "BPAN8", "BPIGID", "BPCGID", "BPLOTG", "BPFRMP", "BPCRCD", "BPUOM", "BPEFTJ", "BPEXDJ", "BPUPRC", "BPUPMJ", "BPTDAY")
VALUES({ml r."BPITM"}, {ml r."BPLITM"}, {ml r."BPMCU"}, {ml r."BPLOCN"}, {ml r."BPLOTN"}, {ml r."BPAN8"}, {ml r."BPIGID"}, {ml r."BPCGID"}, {ml r."BPLOTG"}, {ml r."BPFRMP"}, {ml r."BPCRCD"}, {ml r."BPUOM"}, {ml r."BPEFTJ"}, {ml r."BPEXDJ"}, {ml r."BPUPRC"}, {ml r."BPUPMJ"}, {ml r."BPTDAY"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4106',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4106"
SET "BPLITM" = {ml r."BPLITM"}, "BPEFTJ" = {ml r."BPEFTJ"}, "BPUPRC" = {ml r."BPUPRC"}
WHERE "BPITM" = {ml r."BPITM"} AND "BPMCU" = {ml r."BPMCU"} AND "BPLOCN" = {ml r."BPLOCN"} AND "BPLOTN" = {ml r."BPLOTN"} AND "BPAN8" = {ml r."BPAN8"} AND "BPIGID" = {ml r."BPIGID"} AND "BPCGID" = {ml r."BPCGID"} AND "BPLOTG" = {ml r."BPLOTG"} AND "BPFRMP" = {ml r."BPFRMP"} AND "BPCRCD" = {ml r."BPCRCD"} AND "BPUOM" = {ml r."BPUOM"} AND "BPEXDJ" = {ml r."BPEXDJ"} AND "BPUPMJ" = {ml r."BPUPMJ"} AND "BPTDAY" = {ml r."BPTDAY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4106',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F4106"
WHERE "BPITM" = {ml r."BPITM"} AND "BPMCU" = {ml r."BPMCU"} AND "BPLOCN" = {ml r."BPLOCN"} AND "BPLOTN" = {ml r."BPLOTN"} AND "BPAN8" = {ml r."BPAN8"} AND "BPIGID" = {ml r."BPIGID"} AND "BPCGID" = {ml r."BPCGID"} AND "BPLOTG" = {ml r."BPLOTG"} AND "BPFRMP" = {ml r."BPFRMP"} AND "BPCRCD" = {ml r."BPCRCD"} AND "BPUOM" = {ml r."BPUOM"} AND "BPEXDJ" = {ml r."BPEXDJ"} AND "BPUPMJ" = {ml r."BPUPMJ"} AND "BPTDAY" = {ml r."BPTDAY"}
 

'

/* upload_delete - End */

/* Table :F42019 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42019',
	'download_cursor',
	'sql',

'

SELECT "SHKCOO"  ,"SHDOCO"  ,"SHDCTO"  ,"SHMCU"  ,"SHRKCO"  ,"SHRORN"  ,"SHRCTO"  ,"SHAN8"  ,"SHSHAN"  ,"SHPA8"  ,"SHDRQJ"  ,"SHTRDJ"  ,"SHPDDJ"  ,
"SHADDJ"  ,"SHCNDJ"  ,"SHPEFJ"  ,"SHVR01"  ,"SHVR02"  ,"SHDEL1"  ,"SHDEL2"  ,"SHINMG"  ,"SHPTC"  ,"SHRYIN"  ,"SHASN"  ,"SHPRGP"  ,"SHTXA1"  ,
"SHEXR1"  ,"SHTXCT"  ,"SHATXT"  ,"SHHOLD"  ,"SHROUT"  ,"SHSTOP"  ,"SHZON"  ,"SHFRTH"  ,"SHRCD"  ,"SHFUF2"  ,"SHOTOT"  ,"SHAUTN"  ,"SHCACT"  ,
"SHCEXP"  ,"SHORBY"  ,"SHTKBY"  ,"SHDOC1"  ,"SHDCT4"  
FROM "BUSDTA"."F42019", BUSDTA.Route_Master rm  
WHERE ("BUSDTA"."F42019"."last_modified" >= {ml s.last_table_download} or rm."last_modified" >= {ml s.last_table_download})    
AND SHDCTO=''SO'' AND SHMCU = rm.BranchNumber   AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )  
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42019',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F42019_del"."SHKCOO"
,"BUSDTA"."F42019_del"."SHDOCO"
,"BUSDTA"."F42019_del"."SHDCTO"

FROM "BUSDTA"."F42019_del"
WHERE "BUSDTA"."F42019_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42019',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F42019"
("SHKCOO", "SHDOCO", "SHDCTO", "SHMCU", "SHRKCO", "SHRORN", "SHRCTO", "SHAN8", "SHSHAN", "SHPA8", "SHDRQJ", "SHTRDJ", "SHPDDJ", "SHADDJ", "SHCNDJ", "SHPEFJ", "SHVR01", "SHVR02", "SHDEL1", "SHDEL2", "SHINMG", "SHPTC", "SHRYIN", "SHASN", "SHPRGP", "SHTXA1", "SHEXR1", "SHTXCT", "SHATXT", "SHHOLD", "SHROUT", "SHSTOP", "SHZON", "SHFRTH", "SHRCD", "SHFUF2", "SHOTOT", "SHAUTN", "SHCACT", "SHCEXP", "SHORBY", "SHTKBY", "SHDOC1", "SHDCT4")
VALUES({ml r."SHKCOO"}, {ml r."SHDOCO"}, {ml r."SHDCTO"}, {ml r."SHMCU"}, {ml r."SHRKCO"}, {ml r."SHRORN"}, {ml r."SHRCTO"}, {ml r."SHAN8"}, {ml r."SHSHAN"}, {ml r."SHPA8"}, {ml r."SHDRQJ"}, {ml r."SHTRDJ"}, {ml r."SHPDDJ"}, {ml r."SHADDJ"}, {ml r."SHCNDJ"}, {ml r."SHPEFJ"}, {ml r."SHVR01"}, {ml r."SHVR02"}, {ml r."SHDEL1"}, {ml r."SHDEL2"}, {ml r."SHINMG"}, {ml r."SHPTC"}, {ml r."SHRYIN"}, {ml r."SHASN"}, {ml r."SHPRGP"}, {ml r."SHTXA1"}, {ml r."SHEXR1"}, {ml r."SHTXCT"}, {ml r."SHATXT"}, {ml r."SHHOLD"}, {ml r."SHROUT"}, {ml r."SHSTOP"}, {ml r."SHZON"}, {ml r."SHFRTH"}, {ml r."SHRCD"}, {ml r."SHFUF2"}, {ml r."SHOTOT"}, {ml r."SHAUTN"}, {ml r."SHCACT"}, {ml r."SHCEXP"}, {ml r."SHORBY"}, {ml r."SHTKBY"}, {ml r."SHDOC1"}, {ml r."SHDCT4"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42019',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F42019"
SET "SHMCU" = {ml r."SHMCU"}, "SHRKCO" = {ml r."SHRKCO"}, "SHRORN" = {ml r."SHRORN"}, "SHRCTO" = {ml r."SHRCTO"}, "SHAN8" = {ml r."SHAN8"}, "SHSHAN" = {ml r."SHSHAN"}, "SHPA8" = {ml r."SHPA8"}, "SHDRQJ" = {ml r."SHDRQJ"}, "SHTRDJ" = {ml r."SHTRDJ"}, "SHPDDJ" = {ml r."SHPDDJ"}, "SHADDJ" = {ml r."SHADDJ"}, "SHCNDJ" = {ml r."SHCNDJ"}, "SHPEFJ" = {ml r."SHPEFJ"}, "SHVR01" = {ml r."SHVR01"}, "SHVR02" = {ml r."SHVR02"}, "SHDEL1" = {ml r."SHDEL1"}, "SHDEL2" = {ml r."SHDEL2"}, "SHINMG" = {ml r."SHINMG"}, "SHPTC" = {ml r."SHPTC"}, "SHRYIN" = {ml r."SHRYIN"}, "SHASN" = {ml r."SHASN"}, "SHPRGP" = {ml r."SHPRGP"}, "SHTXA1" = {ml r."SHTXA1"}, "SHEXR1" = {ml r."SHEXR1"}, "SHTXCT" = {ml r."SHTXCT"}, "SHATXT" = {ml r."SHATXT"}, "SHHOLD" = {ml r."SHHOLD"}, "SHROUT" = {ml r."SHROUT"}, "SHSTOP" = {ml r."SHSTOP"}, "SHZON" = {ml r."SHZON"}, "SHFRTH" = {ml r."SHFRTH"}, "SHRCD" = {ml r."SHRCD"}, "SHFUF2" = {ml r."SHFUF2"}, "SHOTOT" = {ml r."SHOTOT"}, "SHAUTN" = {ml r."SHAUTN"}, "SHCACT" = {ml r."SHCACT"}, "SHCEXP" = {ml r."SHCEXP"}, "SHORBY" = {ml r."SHORBY"}, "SHTKBY" = {ml r."SHTKBY"}, "SHDOC1" = {ml r."SHDOC1"}, "SHDCT4" = {ml r."SHDCT4"}
WHERE "SHKCOO" = {ml r."SHKCOO"} AND "SHDOCO" = {ml r."SHDOCO"} AND "SHDCTO" = {ml r."SHDCTO"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42019',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F42019"
WHERE "SHKCOO" = {ml r."SHKCOO"} AND "SHDOCO" = {ml r."SHDOCO"} AND "SHDCTO" = {ml r."SHDCTO"}
 

'

/* upload_delete - End */

/* Table :F42119 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42119',
	'download_cursor',
	'sql',

'

SELECT "SDKCOO"
,"SDDOCO"
,"SDDCTO"
,"SDLNID"
,"SDMCU"
,"SDRKCO"
,"SDRORN"
,"SDRCTO"
,"SDRLLN"
,"SDAN8"
,"SDSHAN"
,"SDPA8"
,"SDDRQJ"
,"SDTRDJ"
,"SDPDDJ"
,"SDADDJ"
,"SDIVD"
,"SDCNDJ"
,"SDDGL"
,"SDPEFJ"
,"SDVR01"
,"SDVR02"
,"SDITM"
,"SDLITM"
,"SDAITM"
,"SDLOCN"
,"SDLOTN"
,"SDDSC1"
,"SDDSC2"
,"SDLNTY"
,"SDNXTR"
,"SDLTTR"
,"SDEMCU"
,"SDSRP1"
,"SDSRP2"
,"SDSRP3"
,"SDSRP4"
,"SDSRP5"
,"SDPRP1"
,"SDPRP2"
,"SDPRP3"
,"SDPRP4"
,"SDPRP5"
,"SDUOM"
,"SDUORG"
,"SDSOQS"
,"SDSOBK"
,"SDSOCN"
,"SDUPRC"
,"SDAEXP"
,"SDPROV"
,"SDINMG"
,"SDPTC"
,"SDASN"
,"SDPRGR"
,"SDCLVL"
,"SDKCO"
,"SDDOC"
,"SDDCT"
,"SDTAX1"
,"SDTXA1"
,"SDEXR1"
,"SDATXT"
,"SDROUT"
,"SDSTOP"
,"SDZON"
,"SDFRTH"
,"SDUOM1"
,"SDPQOR"
,"SDUOM2"
,"SDSQOR"
,"SDUOM4"
,"SDRPRC"
,"SDORPR"
,"SDORP"
,"SDGLC"
,"SDCTRY"
,"SDFY"
,"SDACOM"
,"SDCMCG"
,"SDRCD"
,"SDUPC1"
,"SDUPC2"
,"SDUPC3"
,"SDTORG"
,"SDVR03"
,"SDNUMB"
,"SDAAID"

FROM "BUSDTA"."F42119", BUSDTA.F42019, BUSDTA.Route_Master rm
WHERE ("BUSDTA"."F42119"."last_modified" >= {ml s.last_table_download} or rm."last_modified" >= {ml s.last_table_download} or "BUSDTA"."F42019"."last_modified" >= {ml s.last_table_download}) 
 AND SHDCTO=''SO'' and SHKCOO=SDKCOO and SHDOCO=SDDOCO and SHDCTO=SDDCTO AND SHMCU = rm.BranchNumber
 AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42119',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F42119_del"."SDKCOO"
,"BUSDTA"."F42119_del"."SDDOCO"
,"BUSDTA"."F42119_del"."SDDCTO"
,"BUSDTA"."F42119_del"."SDLNID"

FROM "BUSDTA"."F42119_del"
WHERE "BUSDTA"."F42119_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42119',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F42119"
("SDKCOO", "SDDOCO", "SDDCTO", "SDLNID", "SDMCU", "SDRKCO", "SDRORN", "SDRCTO", "SDRLLN", "SDAN8", "SDSHAN", "SDPA8", "SDDRQJ", "SDTRDJ", "SDPDDJ", "SDADDJ", "SDIVD", "SDCNDJ", "SDDGL", "SDPEFJ", "SDVR01", "SDVR02", "SDITM", "SDLITM", "SDAITM", "SDLOCN", "SDLOTN", "SDDSC1", "SDDSC2", "SDLNTY", "SDNXTR", "SDLTTR", "SDEMCU", "SDSRP1", "SDSRP2", "SDSRP3", "SDSRP4", "SDSRP5", "SDPRP1", "SDPRP2", "SDPRP3", "SDPRP4", "SDPRP5", "SDUOM", "SDUORG", "SDSOQS", "SDSOBK", "SDSOCN", "SDUPRC", "SDAEXP", "SDPROV", "SDINMG", "SDPTC", "SDASN", "SDPRGR", "SDCLVL", "SDKCO", "SDDOC", "SDDCT", "SDTAX1", "SDTXA1", "SDEXR1", "SDATXT", "SDROUT", "SDSTOP", "SDZON", "SDFRTH", "SDUOM1", "SDPQOR", "SDUOM2", "SDSQOR", "SDUOM4", "SDRPRC", "SDORPR", "SDORP", "SDGLC", "SDCTRY", "SDFY", "SDACOM", "SDCMCG", "SDRCD", "SDUPC1", "SDUPC2", "SDUPC3", "SDTORG", "SDVR03", "SDNUMB", "SDAAID")
VALUES({ml r."SDKCOO"}, {ml r."SDDOCO"}, {ml r."SDDCTO"}, {ml r."SDLNID"}, {ml r."SDMCU"}, {ml r."SDRKCO"}, {ml r."SDRORN"}, {ml r."SDRCTO"}, {ml r."SDRLLN"}, {ml r."SDAN8"}, {ml r."SDSHAN"}, {ml r."SDPA8"}, {ml r."SDDRQJ"}, {ml r."SDTRDJ"}, {ml r."SDPDDJ"}, {ml r."SDADDJ"}, {ml r."SDIVD"}, {ml r."SDCNDJ"}, {ml r."SDDGL"}, {ml r."SDPEFJ"}, {ml r."SDVR01"}, {ml r."SDVR02"}, {ml r."SDITM"}, {ml r."SDLITM"}, {ml r."SDAITM"}, {ml r."SDLOCN"}, {ml r."SDLOTN"}, {ml r."SDDSC1"}, {ml r."SDDSC2"}, {ml r."SDLNTY"}, {ml r."SDNXTR"}, {ml r."SDLTTR"}, {ml r."SDEMCU"}, {ml r."SDSRP1"}, {ml r."SDSRP2"}, {ml r."SDSRP3"}, {ml r."SDSRP4"}, {ml r."SDSRP5"}, {ml r."SDPRP1"}, {ml r."SDPRP2"}, {ml r."SDPRP3"}, {ml r."SDPRP4"}, {ml r."SDPRP5"}, {ml r."SDUOM"}, {ml r."SDUORG"}, {ml r."SDSOQS"}, {ml r."SDSOBK"}, {ml r."SDSOCN"}, {ml r."SDUPRC"}, {ml r."SDAEXP"}, {ml r."SDPROV"}, {ml r."SDINMG"}, {ml r."SDPTC"}, {ml r."SDASN"}, {ml r."SDPRGR"}, {ml r."SDCLVL"}, {ml r."SDKCO"}, {ml r."SDDOC"}, {ml r."SDDCT"}, {ml r."SDTAX1"}, {ml r."SDTXA1"}, {ml r."SDEXR1"}, {ml r."SDATXT"}, {ml r."SDROUT"}, {ml r."SDSTOP"}, {ml r."SDZON"}, {ml r."SDFRTH"}, {ml r."SDUOM1"}, {ml r."SDPQOR"}, {ml r."SDUOM2"}, {ml r."SDSQOR"}, {ml r."SDUOM4"}, {ml r."SDRPRC"}, {ml r."SDORPR"}, {ml r."SDORP"}, {ml r."SDGLC"}, {ml r."SDCTRY"}, {ml r."SDFY"}, {ml r."SDACOM"}, {ml r."SDCMCG"}, {ml r."SDRCD"}, {ml r."SDUPC1"}, {ml r."SDUPC2"}, {ml r."SDUPC3"}, {ml r."SDTORG"}, {ml r."SDVR03"}, {ml r."SDNUMB"}, {ml r."SDAAID"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42119',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F42119"
SET "SDMCU" = {ml r."SDMCU"}, "SDRKCO" = {ml r."SDRKCO"}, "SDRORN" = {ml r."SDRORN"}, "SDRCTO" = {ml r."SDRCTO"}, "SDRLLN" = {ml r."SDRLLN"}, "SDAN8" = {ml r."SDAN8"}, "SDSHAN" = {ml r."SDSHAN"}, "SDPA8" = {ml r."SDPA8"}, "SDDRQJ" = {ml r."SDDRQJ"}, "SDTRDJ" = {ml r."SDTRDJ"}, "SDPDDJ" = {ml r."SDPDDJ"}, "SDADDJ" = {ml r."SDADDJ"}, "SDIVD" = {ml r."SDIVD"}, "SDCNDJ" = {ml r."SDCNDJ"}, "SDDGL" = {ml r."SDDGL"}, "SDPEFJ" = {ml r."SDPEFJ"}, "SDVR01" = {ml r."SDVR01"}, "SDVR02" = {ml r."SDVR02"}, "SDITM" = {ml r."SDITM"}, "SDLITM" = {ml r."SDLITM"}, "SDAITM" = {ml r."SDAITM"}, "SDLOCN" = {ml r."SDLOCN"}, "SDLOTN" = {ml r."SDLOTN"}, "SDDSC1" = {ml r."SDDSC1"}, "SDDSC2" = {ml r."SDDSC2"}, "SDLNTY" = {ml r."SDLNTY"}, "SDNXTR" = {ml r."SDNXTR"}, "SDLTTR" = {ml r."SDLTTR"}, "SDEMCU" = {ml r."SDEMCU"}, "SDSRP1" = {ml r."SDSRP1"}, "SDSRP2" = {ml r."SDSRP2"}, "SDSRP3" = {ml r."SDSRP3"}, "SDSRP4" = {ml r."SDSRP4"}, "SDSRP5" = {ml r."SDSRP5"}, "SDPRP1" = {ml r."SDPRP1"}, "SDPRP2" = {ml r."SDPRP2"}, "SDPRP3" = {ml r."SDPRP3"}, "SDPRP4" = {ml r."SDPRP4"}, "SDPRP5" = {ml r."SDPRP5"}, "SDUOM" = {ml r."SDUOM"}, "SDUORG" = {ml r."SDUORG"}, "SDSOQS" = {ml r."SDSOQS"}, "SDSOBK" = {ml r."SDSOBK"}, "SDSOCN" = {ml r."SDSOCN"}, "SDUPRC" = {ml r."SDUPRC"}, "SDAEXP" = {ml r."SDAEXP"}, "SDPROV" = {ml r."SDPROV"}, "SDINMG" = {ml r."SDINMG"}, "SDPTC" = {ml r."SDPTC"}, "SDASN" = {ml r."SDASN"}, "SDPRGR" = {ml r."SDPRGR"}, "SDCLVL" = {ml r."SDCLVL"}, "SDKCO" = {ml r."SDKCO"}, "SDDOC" = {ml r."SDDOC"}, "SDDCT" = {ml r."SDDCT"}, "SDTAX1" = {ml r."SDTAX1"}, "SDTXA1" = {ml r."SDTXA1"}, "SDEXR1" = {ml r."SDEXR1"}, "SDATXT" = {ml r."SDATXT"}, "SDROUT" = {ml r."SDROUT"}, "SDSTOP" = {ml r."SDSTOP"}, "SDZON" = {ml r."SDZON"}, "SDFRTH" = {ml r."SDFRTH"}, "SDUOM1" = {ml r."SDUOM1"}, "SDPQOR" = {ml r."SDPQOR"}, "SDUOM2" = {ml r."SDUOM2"}, "SDSQOR" = {ml r."SDSQOR"}, "SDUOM4" = {ml r."SDUOM4"}, "SDRPRC" = {ml r."SDRPRC"}, "SDORPR" = {ml r."SDORPR"}, "SDORP" = {ml r."SDORP"}, "SDGLC" = {ml r."SDGLC"}, "SDCTRY" = {ml r."SDCTRY"}, "SDFY" = {ml r."SDFY"}, "SDACOM" = {ml r."SDACOM"}, "SDCMCG" = {ml r."SDCMCG"}, "SDRCD" = {ml r."SDRCD"}, "SDUPC1" = {ml r."SDUPC1"}, "SDUPC2" = {ml r."SDUPC2"}, "SDUPC3" = {ml r."SDUPC3"}, "SDTORG" = {ml r."SDTORG"}, "SDVR03" = {ml r."SDVR03"}, "SDNUMB" = {ml r."SDNUMB"}, "SDAAID" = {ml r."SDAAID"}
WHERE "SDKCOO" = {ml r."SDKCOO"} AND "SDDOCO" = {ml r."SDDOCO"} AND "SDDCTO" = {ml r."SDDCTO"} AND "SDLNID" = {ml r."SDLNID"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42119',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F42119"
WHERE "SDKCOO" = {ml r."SDKCOO"} AND "SDDOCO" = {ml r."SDDOCO"} AND "SDDCTO" = {ml r."SDDCTO"} AND "SDLNID" = {ml r."SDLNID"}
 
'

/* upload_delete - End */

/* Table :F56M0000 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0000',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F56M0000"."GFSY"  ,"BUSDTA"."F56M0000"."GFCXPJ"  ,"BUSDTA"."F56M0000"."GFDTEN"  ,"BUSDTA"."F56M0000"."GFLAVJ"  ,
 "BUSDTA"."F56M0000"."GFOBJ"  ,"BUSDTA"."F56M0000"."GFMCU"  ,"BUSDTA"."F56M0000"."GFSUB"  ,"BUSDTA"."F56M0000"."GFPST"  ,
 "BUSDTA"."F56M0000"."GFEV01"  ,"BUSDTA"."F56M0000"."GFEV02"  ,"BUSDTA"."F56M0000"."GFEV03"  ,"BUSDTA"."F56M0000"."GFMATH01"  ,
 "BUSDTA"."F56M0000"."GFMATH02"  ,"BUSDTA"."F56M0000"."GFMATH03"  ,"BUSDTA"."F56M0000"."GFCFSTR1"  ,"BUSDTA"."F56M0000"."GFCFSTR2"  ,
 "BUSDTA"."F56M0000"."GFGS1A"  ,"BUSDTA"."F56M0000"."GFGS1B"  ,"BUSDTA"."F56M0000"."GFGS2A"  ,"BUSDTA"."F56M0000"."GFGS2B"    
 FROM "BUSDTA"."F56M0000"  
 WHERE "BUSDTA"."F56M0000"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0000',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F56M0000_del"."GFSY"

FROM "BUSDTA"."F56M0000_del"
WHERE "BUSDTA"."F56M0000_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0000',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F56M0000"
("GFSY", "GFCXPJ", "GFDTEN", "GFLAVJ", "GFOBJ", "GFMCU", "GFSUB", "GFPST", "GFEV01", "GFEV02", "GFEV03", "GFMATH01", "GFMATH02", "GFMATH03", "GFCFSTR1", "GFCFSTR2", "GFGS1A", "GFGS1B", "GFGS2A", "GFGS2B")
VALUES({ml r."GFSY"}, {ml r."GFCXPJ"}, {ml r."GFDTEN"}, {ml r."GFLAVJ"}, {ml r."GFOBJ"}, {ml r."GFMCU"}, {ml r."GFSUB"}, {ml r."GFPST"}, {ml r."GFEV01"}, {ml r."GFEV02"}, {ml r."GFEV03"}, {ml r."GFMATH01"}, {ml r."GFMATH02"}, {ml r."GFMATH03"}, {ml r."GFCFSTR1"}, {ml r."GFCFSTR2"}, {ml r."GFGS1A"}, {ml r."GFGS1B"}, {ml r."GFGS2A"}, {ml r."GFGS2B"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0000',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F56M0000"
SET "GFCXPJ" = {ml r."GFCXPJ"}, "GFDTEN" = {ml r."GFDTEN"}, "GFLAVJ" = {ml r."GFLAVJ"}, "GFOBJ" = {ml r."GFOBJ"}, "GFMCU" = {ml r."GFMCU"}, "GFSUB" = {ml r."GFSUB"}, "GFPST" = {ml r."GFPST"}, "GFEV01" = {ml r."GFEV01"}, "GFEV02" = {ml r."GFEV02"}, "GFEV03" = {ml r."GFEV03"}, "GFMATH01" = {ml r."GFMATH01"}, "GFMATH02" = {ml r."GFMATH02"}, "GFMATH03" = {ml r."GFMATH03"}, "GFCFSTR1" = {ml r."GFCFSTR1"}, "GFCFSTR2" = {ml r."GFCFSTR2"}, "GFGS1A" = {ml r."GFGS1A"}, "GFGS1B" = {ml r."GFGS1B"}, "GFGS2A" = {ml r."GFGS2A"}, "GFGS2B" = {ml r."GFGS2B"}
WHERE "GFSY" = {ml r."GFSY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0000',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F56M0000"
WHERE "GFSY" = {ml r."GFSY"}
 

'

/* upload_delete - End */

/* Table :F56M0001 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0001',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."F56M0001"."FFUSER"  ,"BUSDTA"."F56M0001"."FFROUT"  ,"BUSDTA"."F56M0001"."FFMCU"  ,"BUSDTA"."F56M0001"."FFHMCU"  ,
 "BUSDTA"."F56M0001"."FFBUVAL"  ,"BUSDTA"."F56M0001"."FFAN8"  ,"BUSDTA"."F56M0001"."FFPA8"  ,"BUSDTA"."F56M0001"."FFSTOP"  ,
 "BUSDTA"."F56M0001"."FFZON"  ,"BUSDTA"."F56M0001"."FFLOCN"  ,"BUSDTA"."F56M0001"."FFLOCF"  ,"BUSDTA"."F56M0001"."FFEV01"  ,
 "BUSDTA"."F56M0001"."FFEV02"  ,"BUSDTA"."F56M0001"."FFEV03"  ,"BUSDTA"."F56M0001"."FFMATH01"  ,"BUSDTA"."F56M0001"."FFMATH02"  ,
 "BUSDTA"."F56M0001"."FFMATH03"  ,"BUSDTA"."F56M0001"."FFCXPJ"  ,"BUSDTA"."F56M0001"."FFCLRJ"  ,"BUSDTA"."F56M0001"."FFDTE"    
 FROM "BUSDTA"."F56M0001"  WHERE "BUSDTA"."F56M0001"."last_modified" >= {ml s.last_table_download}  
 AND "BUSDTA"."F56M0001"."FFUSER" = {ml s.username}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0001',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F56M0001_del"."FFUSER"
,"BUSDTA"."F56M0001_del"."FFROUT"
,"BUSDTA"."F56M0001_del"."FFMCU"

FROM "BUSDTA"."F56M0001_del"
WHERE "BUSDTA"."F56M0001_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0001',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F56M0001"
("FFUSER", "FFROUT", "FFMCU", "FFHMCU", "FFBUVAL", "FFAN8", "FFPA8", "FFSTOP", "FFZON", "FFLOCN", "FFLOCF", "FFEV01", "FFEV02", "FFEV03", "FFMATH01", "FFMATH02", "FFMATH03", "FFCXPJ", "FFCLRJ", "FFDTE")
VALUES({ml r."FFUSER"}, {ml r."FFROUT"}, {ml r."FFMCU"}, {ml r."FFHMCU"}, {ml r."FFBUVAL"}, {ml r."FFAN8"}, {ml r."FFPA8"}, {ml r."FFSTOP"}, {ml r."FFZON"}, {ml r."FFLOCN"}, {ml r."FFLOCF"}, {ml r."FFEV01"}, {ml r."FFEV02"}, {ml r."FFEV03"}, {ml r."FFMATH01"}, {ml r."FFMATH02"}, {ml r."FFMATH03"}, {ml r."FFCXPJ"}, {ml r."FFCLRJ"}, {ml r."FFDTE"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0001',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F56M0001"
SET "FFHMCU" = {ml r."FFHMCU"}, "FFBUVAL" = {ml r."FFBUVAL"}, "FFAN8" = {ml r."FFAN8"}, "FFPA8" = {ml r."FFPA8"}, "FFSTOP" = {ml r."FFSTOP"}, "FFZON" = {ml r."FFZON"}, "FFLOCN" = {ml r."FFLOCN"}, "FFLOCF" = {ml r."FFLOCF"}, "FFEV01" = {ml r."FFEV01"}, "FFEV02" = {ml r."FFEV02"}, "FFEV03" = {ml r."FFEV03"}, "FFMATH01" = {ml r."FFMATH01"}, "FFMATH02" = {ml r."FFMATH02"}, "FFMATH03" = {ml r."FFMATH03"}, "FFCXPJ" = {ml r."FFCXPJ"}, "FFCLRJ" = {ml r."FFCLRJ"}, "FFDTE" = {ml r."FFDTE"}
WHERE "FFUSER" = {ml r."FFUSER"} AND "FFROUT" = {ml r."FFROUT"} AND "FFMCU" = {ml r."FFMCU"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F56M0001',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F56M0001"
WHERE "FFUSER" = {ml r."FFUSER"} AND "FFROUT" = {ml r."FFROUT"} AND "FFMCU" = {ml r."FFMCU"}
 

'

/* upload_delete - End */

/* Table :F90CA003 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA003',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F90CA003"."SMAN8"
,"BUSDTA"."F90CA003"."SMSLSM"

FROM "BUSDTA"."F90CA003", BUSDTA.F56M0001
WHERE "BUSDTA"."F90CA003"."last_modified" >= {ml s.last_table_download}
    AND FFAN8=SMSLSM and FFUSER= {ml s.username}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA003',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F90CA003_del"."SMAN8"
,"BUSDTA"."F90CA003_del"."SMSLSM"

FROM "BUSDTA"."F90CA003_del", BUSDTA.F56M0001
WHERE "BUSDTA"."F90CA003_del"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and FFUSER= {ml s.username}

 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA003',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F90CA003"
("SMAN8", "SMSLSM")
VALUES({ml r."SMAN8"}, {ml r."SMSLSM"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA003',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F90CA003"
 
WHERE "SMAN8" = {ml r."SMAN8"} AND "SMSLSM" = {ml r."SMSLSM"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA003',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F90CA003"
WHERE "SMAN8" = {ml r."SMAN8"} AND "SMSLSM" = {ml r."SMSLSM"}
 
'

/* upload_delete - End */

/* Table :F90CA042 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA042',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F90CA042"."EMAN8"
,"BUSDTA"."F90CA042"."EMPA8"

FROM "BUSDTA"."F90CA042"
WHERE "BUSDTA"."F90CA042"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA042',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F90CA042_del"."EMAN8"
,"BUSDTA"."F90CA042_del"."EMPA8"

FROM "BUSDTA"."F90CA042_del"
WHERE "BUSDTA"."F90CA042_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA042',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F90CA042"
("EMAN8", "EMPA8")
VALUES({ml r."EMAN8"}, {ml r."EMPA8"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA042',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F90CA042"
 
WHERE "EMAN8" = {ml r."EMAN8"} AND "EMPA8" = {ml r."EMPA8"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA042',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F90CA042"
WHERE "EMAN8" = {ml r."EMAN8"} AND "EMPA8" = {ml r."EMPA8"}
 
'

/* upload_delete - End */

/* Table :F90CA086 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA086',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F90CA086"."CRCUAN8"
,"BUSDTA"."F90CA086"."CRCRAN8"

FROM "BUSDTA"."F90CA086", BUSDTA.F56M0001 , BUSDTA.F90CA003
WHERE --"BUSDTA"."F90CA086"."last_modified" >= {ml s.last_table_download} AND
FFAN8=SMSLSM and SMAN8=CRCUAN8 and FFUSER= {ml s.username}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA086',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."F90CA086_del"."CRCRAN8"
,"BUSDTA"."F90CA086_del"."CRCUAN8"

FROM "BUSDTA"."F90CA086_del"
where CRCUAN8 in (select SMAN8 from "BUSDTA"."F90CA003_del")
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA086',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F90CA086"
("CRCUAN8", "CRCRAN8")
VALUES({ml r."CRCUAN8"}, {ml r."CRCRAN8"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA086',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F90CA086"
 
WHERE "CRCRAN8" = {ml r."CRCRAN8"} AND "CRCUAN8" = {ml r."CRCUAN8"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F90CA086',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F90CA086"
WHERE "CRCRAN8" = {ml r."CRCRAN8"} AND "CRCUAN8" = {ml r."CRCUAN8"}
 
'

/* upload_delete - End */

/* Table :M0111 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0111',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M0111"."CDAN8"  ,"BUSDTA"."M0111"."CDID"  ,"BUSDTA"."M0111"."CDIDLN"  ,"BUSDTA"."M0111"."CDRCK7"  ,
     "BUSDTA"."M0111"."CDCNLN"  ,"BUSDTA"."M0111"."CDAR1"  ,"BUSDTA"."M0111"."CDPH1"  ,"BUSDTA"."M0111"."CDEXTN1"  ,
     "BUSDTA"."M0111"."CDPHTP1"  ,"BUSDTA"."M0111"."CDDFLTPH1"  ,"BUSDTA"."M0111"."CDREFPH1"  ,"BUSDTA"."M0111"."CDAR2"  ,
     "BUSDTA"."M0111"."CDPH2"  ,"BUSDTA"."M0111"."CDEXTN2"  ,"BUSDTA"."M0111"."CDPHTP2"  ,"BUSDTA"."M0111"."CDDFLTPH2"  ,
     "BUSDTA"."M0111"."CDREFPH2"  ,"BUSDTA"."M0111"."CDAR3"  ,"BUSDTA"."M0111"."CDPH3"  ,"BUSDTA"."M0111"."CDEXTN3"  ,
     "BUSDTA"."M0111"."CDPHTP3"  ,"BUSDTA"."M0111"."CDDFLTPH3"  ,"BUSDTA"."M0111"."CDREFPH3"  ,"BUSDTA"."M0111"."CDAR4"  ,
     "BUSDTA"."M0111"."CDPH4"  ,"BUSDTA"."M0111"."CDEXTN4"  ,"BUSDTA"."M0111"."CDPHTP4"  ,"BUSDTA"."M0111"."CDDFLTPH4"  ,
     "BUSDTA"."M0111"."CDREFPH4"  ,"BUSDTA"."M0111"."CDEMAL1"  ,"BUSDTA"."M0111"."CDETP1"  ,"BUSDTA"."M0111"."CDDFLTEM1"  ,
     "BUSDTA"."M0111"."CDREFEM1"  ,"BUSDTA"."M0111"."CDEMAL2"  ,"BUSDTA"."M0111"."CDETP2"  ,"BUSDTA"."M0111"."CDDFLTEM2"  ,
     "BUSDTA"."M0111"."CDREFEM2"  ,"BUSDTA"."M0111"."CDEMAL3"  ,"BUSDTA"."M0111"."CDETP3"  ,"BUSDTA"."M0111"."CDDFLTEM3"  ,
     "BUSDTA"."M0111"."CDREFEM3"  ,"BUSDTA"."M0111"."CDGNNM"  ,"BUSDTA"."M0111"."CDMDNM"  ,"BUSDTA"."M0111"."CDSRNM"  ,
     "BUSDTA"."M0111"."CDTITL"  ,"BUSDTA"."M0111"."CDACTV"  ,"BUSDTA"."M0111"."CDDFLT"  ,"BUSDTA"."M0111"."CDSET"  ,
     "BUSDTA"."M0111"."CDCRBY"  ,"BUSDTA"."M0111"."CDCRDT"  ,"BUSDTA"."M0111"."CDUPBY"  ,"BUSDTA"."M0111"."CDUPDT"    
     FROM BUSDTA.M0111, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."M0111"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or crm."last_modified" >= {ml s.last_table_download})    AND CDAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0111',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M0111_del"."CDAN8"
,"BUSDTA"."M0111_del"."CDID"

FROM "BUSDTA"."M0111_del"
WHERE "BUSDTA"."M0111_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0111',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M0111"
("CDAN8", "CDID", "CDIDLN", "CDRCK7", "CDCNLN", "CDAR1", "CDPH1", "CDEXTN1", "CDPHTP1", "CDDFLTPH1", "CDREFPH1", "CDAR2", "CDPH2", "CDEXTN2", "CDPHTP2", "CDDFLTPH2", "CDREFPH2", "CDAR3", "CDPH3", "CDEXTN3", "CDPHTP3", "CDDFLTPH3", "CDREFPH3", "CDAR4", "CDPH4", "CDEXTN4", "CDPHTP4", "CDDFLTPH4", "CDREFPH4", "CDEMAL1", "CDETP1", "CDDFLTEM1", "CDREFEM1", "CDEMAL2", "CDETP2", "CDDFLTEM2", "CDREFEM2", "CDEMAL3", "CDETP3", "CDDFLTEM3", "CDREFEM3", "CDGNNM", "CDMDNM", "CDSRNM", "CDTITL", "CDACTV", "CDDFLT", "CDSET", "CDCRBY", "CDCRDT", "CDUPBY", "CDUPDT")
VALUES({ml r."CDAN8"}, {ml r."CDID"}, {ml r."CDIDLN"}, {ml r."CDRCK7"}, {ml r."CDCNLN"}, {ml r."CDAR1"}, {ml r."CDPH1"}, {ml r."CDEXTN1"}, {ml r."CDPHTP1"}, {ml r."CDDFLTPH1"}, {ml r."CDREFPH1"}, {ml r."CDAR2"}, {ml r."CDPH2"}, {ml r."CDEXTN2"}, {ml r."CDPHTP2"}, {ml r."CDDFLTPH2"}, {ml r."CDREFPH2"}, {ml r."CDAR3"}, {ml r."CDPH3"}, {ml r."CDEXTN3"}, {ml r."CDPHTP3"}, {ml r."CDDFLTPH3"}, {ml r."CDREFPH3"}, {ml r."CDAR4"}, {ml r."CDPH4"}, {ml r."CDEXTN4"}, {ml r."CDPHTP4"}, {ml r."CDDFLTPH4"}, {ml r."CDREFPH4"}, {ml r."CDEMAL1"}, {ml r."CDETP1"}, {ml r."CDDFLTEM1"}, {ml r."CDREFEM1"}, {ml r."CDEMAL2"}, {ml r."CDETP2"}, {ml r."CDDFLTEM2"}, {ml r."CDREFEM2"}, {ml r."CDEMAL3"}, {ml r."CDETP3"}, {ml r."CDDFLTEM3"}, {ml r."CDREFEM3"}, {ml r."CDGNNM"}, {ml r."CDMDNM"}, {ml r."CDSRNM"}, {ml r."CDTITL"}, {ml r."CDACTV"}, {ml r."CDDFLT"}, {ml r."CDSET"}, {ml r."CDCRBY"}, {ml r."CDCRDT"}, {ml r."CDUPBY"}, {ml r."CDUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0111',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M0111"
SET "CDIDLN" = {ml r."CDIDLN"}, "CDRCK7" = {ml r."CDRCK7"}, "CDCNLN" = {ml r."CDCNLN"}, "CDAR1" = {ml r."CDAR1"}, "CDPH1" = {ml r."CDPH1"}, "CDEXTN1" = {ml r."CDEXTN1"}, "CDPHTP1" = {ml r."CDPHTP1"}, "CDDFLTPH1" = {ml r."CDDFLTPH1"}, "CDREFPH1" = {ml r."CDREFPH1"}, "CDAR2" = {ml r."CDAR2"}, "CDPH2" = {ml r."CDPH2"}, "CDEXTN2" = {ml r."CDEXTN2"}, "CDPHTP2" = {ml r."CDPHTP2"}, "CDDFLTPH2" = {ml r."CDDFLTPH2"}, "CDREFPH2" = {ml r."CDREFPH2"}, "CDAR3" = {ml r."CDAR3"}, "CDPH3" = {ml r."CDPH3"}, "CDEXTN3" = {ml r."CDEXTN3"}, "CDPHTP3" = {ml r."CDPHTP3"}, "CDDFLTPH3" = {ml r."CDDFLTPH3"}, "CDREFPH3" = {ml r."CDREFPH3"}, "CDAR4" = {ml r."CDAR4"}, "CDPH4" = {ml r."CDPH4"}, "CDEXTN4" = {ml r."CDEXTN4"}, "CDPHTP4" = {ml r."CDPHTP4"}, "CDDFLTPH4" = {ml r."CDDFLTPH4"}, "CDREFPH4" = {ml r."CDREFPH4"}, "CDEMAL1" = {ml r."CDEMAL1"}, "CDETP1" = {ml r."CDETP1"}, "CDDFLTEM1" = {ml r."CDDFLTEM1"}, "CDREFEM1" = {ml r."CDREFEM1"}, "CDEMAL2" = {ml r."CDEMAL2"}, "CDETP2" = {ml r."CDETP2"}, "CDDFLTEM2" = {ml r."CDDFLTEM2"}, "CDREFEM2" = {ml r."CDREFEM2"}, "CDEMAL3" = {ml r."CDEMAL3"}, "CDETP3" = {ml r."CDETP3"}, "CDDFLTEM3" = {ml r."CDDFLTEM3"}, "CDREFEM3" = {ml r."CDREFEM3"}, "CDGNNM" = {ml r."CDGNNM"}, "CDMDNM" = {ml r."CDMDNM"}, "CDSRNM" = {ml r."CDSRNM"}, "CDTITL" = {ml r."CDTITL"}, "CDACTV" = {ml r."CDACTV"}, "CDDFLT" = {ml r."CDDFLT"}, "CDSET" = {ml r."CDSET"}, "CDCRBY" = {ml r."CDCRBY"}, "CDCRDT" = {ml r."CDCRDT"}, "CDUPBY" = {ml r."CDUPBY"}, "CDUPDT" = {ml r."CDUPDT"}
WHERE "CDAN8" = {ml r."CDAN8"} AND "CDID" = {ml r."CDID"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0111',
	'upload_delete',
	'sql',

'

/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M0111"
WHERE "CDAN8" = {ml r."CDAN8"} AND "CDID" = {ml r."CDID"}
 

'

/* upload_delete - End */

/* Table :M0112 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0112',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M0112"."NDAN8"  ,"BUSDTA"."M0112"."NDID"  ,"BUSDTA"."M0112"."NDDTTM"  ,"BUSDTA"."M0112"."NDDTLS"  ,
     "BUSDTA"."M0112"."NDTYP"  ,"BUSDTA"."M0112"."NDDFLT"  ,"BUSDTA"."M0112"."NDCRBY"  ,"BUSDTA"."M0112"."NDCRDT"  ,
     "BUSDTA"."M0112"."NDUPBY"  ,"BUSDTA"."M0112"."NDUPDT"    
     FROM BUSDTA.M0112, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."M0112"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or crm."last_modified" >= {ml s.last_table_download})   AND NDAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0112',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M0112_del"."NDAN8"
,"BUSDTA"."M0112_del"."NDID"

FROM "BUSDTA"."M0112_del"
WHERE "BUSDTA"."M0112_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0112',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M0112"
("NDAN8", "NDID", "NDDTTM", "NDDTLS", "NDTYP", "NDDFLT", "NDCRBY", "NDCRDT", "NDUPBY", "NDUPDT")
VALUES({ml r."NDAN8"}, {ml r."NDID"}, {ml r."NDDTTM"}, {ml r."NDDTLS"}, {ml r."NDTYP"}, {ml r."NDDFLT"}, {ml r."NDCRBY"}, {ml r."NDCRDT"}, {ml r."NDUPBY"}, {ml r."NDUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0112',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M0112"
SET "NDDTTM" = {ml r."NDDTTM"}, "NDDTLS" = {ml r."NDDTLS"}, "NDTYP" = {ml r."NDTYP"}, "NDDFLT" = {ml r."NDDFLT"}, "NDCRBY" = {ml r."NDCRBY"}, "NDCRDT" = {ml r."NDCRDT"}, "NDUPBY" = {ml r."NDUPBY"}, "NDUPDT" = {ml r."NDUPDT"}
WHERE "NDAN8" = {ml r."NDAN8"} AND "NDID" = {ml r."NDID"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0112',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M0112"
WHERE "NDAN8" = {ml r."NDAN8"} AND "NDID" = {ml r."NDID"}
 

'

/* upload_delete - End */

/* Table :M0140 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0140',
	'download_cursor',
	'sql',

'

  SELECT "BUSDTA"."M0140"."RMMCU"  ,"BUSDTA"."M0140"."RMAN8"  ,"BUSDTA"."M0140"."RMDTAI"  ,"BUSDTA"."M0140"."RMCAN8"  ,"BUSDTA"."M0140"."RMYN"    
  FROM "BUSDTA"."M0140"  WHERE "BUSDTA"."M0140"."last_modified">= {ml s.last_table_download}   
  AND ltrim(RMMCU) = (select ltrim(BranchNumber) from busdta.Route_Master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0140',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M0140_del"."RMMCU"
,"BUSDTA"."M0140_del"."RMAN8"
,"BUSDTA"."M0140_del"."RMDTAI"

FROM "BUSDTA"."M0140_del"
WHERE "BUSDTA"."M0140_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :M03011 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03011',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M03011"."CSAN8"  ,"BUSDTA"."M03011"."CSCO"  ,"BUSDTA"."M03011"."CSUAMT"  ,"BUSDTA"."M03011"."CSOBAL"  ,
     "BUSDTA"."M03011"."CSCRBY"  ,"BUSDTA"."M03011"."CSCRDT"  ,"BUSDTA"."M03011"."CSUPBY"  ,"BUSDTA"."M03011"."CSUPDT"    
     FROM BUSDTA.M03011, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."M03011"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or crm."last_modified" >= {ml s.last_table_download})   AND CSAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03011',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M03011_del"."CSAN8"
,"BUSDTA"."M03011_del"."CSCO"

FROM "BUSDTA"."M03011_del"
WHERE "BUSDTA"."M03011_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03011',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M03011"
("CSAN8", "CSCO", "CSUAMT", "CSOBAL", "CSCRBY", "CSCRDT", "CSUPBY", "CSUPDT")
VALUES({ml r."CSAN8"}, {ml r."CSCO"}, {ml r."CSUAMT"}, {ml r."CSOBAL"}, {ml r."CSCRBY"}, {ml r."CSCRDT"}, {ml r."CSUPBY"}, {ml r."CSUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03011',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M03011"
SET "CSUAMT" = {ml r."CSUAMT"}, "CSOBAL" = {ml r."CSOBAL"}, "CSCRBY" = {ml r."CSCRBY"}, "CSCRDT" = {ml r."CSCRDT"}, "CSUPBY" = {ml r."CSUPBY"}, "CSUPDT" = {ml r."CSUPDT"}
WHERE "CSAN8" = {ml r."CSAN8"} AND "CSCO" = {ml r."CSCO"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03011',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M03011"
WHERE "CSAN8" = {ml r."CSAN8"} AND "CSCO" = {ml r."CSCO"}
 
'

/* upload_delete - End */

/* Table :M03042 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03042',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."M03042"."PDAN8"
,"BUSDTA"."M03042"."PDCO"
,"BUSDTA"."M03042"."PDID"
,"BUSDTA"."M03042"."PDPAMT"
,"BUSDTA"."M03042"."PDPMODE"
,"BUSDTA"."M03042"."PDCHQNO"
,"BUSDTA"."M03042"."PDCHQDT"
,"BUSDTA"."M03042"."PDCRBY"
,"BUSDTA"."M03042"."PDCRDT"
,"BUSDTA"."M03042"."PDUPBY"
,"BUSDTA"."M03042"."PDUPDT"
,"BUSDTA"."M03042"."PDRCID"
,"BUSDTA"."M03042"."PDTRMD"

FROM BUSDTA.M03042, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE ("BUSDTA"."M03042"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}
 or crm.last_modified >= {ml s.last_table_download})
AND PDAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber
 AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03042',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M03042_del"."PDAN8"
,"BUSDTA"."M03042_del"."PDCO"
,"BUSDTA"."M03042_del"."PDID"

FROM "BUSDTA"."M03042_del"
WHERE "BUSDTA"."M03042_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03042',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M03042"
("PDAN8", "PDCO", "PDID", "PDPAMT", "PDPMODE", "PDCHQNO", "PDCHQDT", "PDCRBY", "PDCRDT", "PDUPBY", "PDUPDT", "PDRCID", "PDTRMD")
VALUES({ml r."PDAN8"}, {ml r."PDCO"}, {ml r."PDID"}, {ml r."PDPAMT"}, {ml r."PDPMODE"}, {ml r."PDCHQNO"}, {ml r."PDCHQDT"}, {ml r."PDCRBY"}, {ml r."PDCRDT"}, {ml r."PDUPBY"}, {ml r."PDUPDT"}, {ml r."PDRCID"}, {ml r."PDTRMD"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03042',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M03042"
SET "PDPAMT" = {ml r."PDPAMT"}, "PDPMODE" = {ml r."PDPMODE"}, "PDCHQNO" = {ml r."PDCHQNO"}, "PDCHQDT" = {ml r."PDCHQDT"}, "PDCRBY" = {ml r."PDCRBY"}, "PDCRDT" = {ml r."PDCRDT"}, "PDUPBY" = {ml r."PDUPBY"}, "PDUPDT" = {ml r."PDUPDT"}, "PDRCID" = {ml r."PDRCID"}, "PDTRMD" = {ml r."PDTRMD"}
WHERE "PDAN8" = {ml r."PDAN8"} AND "PDCO" = {ml r."PDCO"} AND "PDID" = {ml r."PDID"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M03042',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M03042"
WHERE "PDAN8" = {ml r."PDAN8"} AND "PDCO" = {ml r."PDCO"} AND "PDID" = {ml r."PDID"}
 

'

/* upload_delete - End */

/* Table :M04012 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M04012',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M04012"."PMAN8"  ,"BUSDTA"."M04012"."PMALPH"  ,"BUSDTA"."M04012"."PMROUT"  ,"BUSDTA"."M04012"."PMSRS"  ,
     "BUSDTA"."M04012"."PMCRBY"  ,"BUSDTA"."M04012"."PMCRDT"  ,"BUSDTA"."M04012"."PMUPBY"  ,"BUSDTA"."M04012"."PMUPDT"  ,
     "BUSDTA"."M04012"."PMIBN"  ,"BUSDTA"."M04012"."PMISTP"  ,"BUSDTA"."M04012"."PMIBGP"  ,"BUSDTA"."M04012"."PMILCT"    
     FROM BUSDTA.F56M0001, BUSDTA.M04012  WHERE "BUSDTA"."M04012"."last_modified" >= {ml s.last_table_download}  AND FFUSER = PMROUT  
     and FFUSER = {ml s.username}   
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M04012',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M04012_del"."PMAN8"

FROM "BUSDTA"."M04012_del"
WHERE "BUSDTA"."M04012_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M04012',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M04012"
("PMAN8", "PMALPH", "PMROUT", "PMSRS", "PMCRBY", "PMCRDT", "PMUPBY", "PMUPDT", "PMIBN", "PMISTP", "PMIBGP", "PMILCT")
VALUES({ml r."PMAN8"}, {ml r."PMALPH"}, {ml r."PMROUT"}, {ml r."PMSRS"}, {ml r."PMCRBY"}, {ml r."PMCRDT"}, {ml r."PMUPBY"}, {ml r."PMUPDT"}, {ml r."PMIBN"}, {ml r."PMISTP"}, {ml r."PMIBGP"}, {ml r."PMILCT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M04012',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M04012"
SET "PMALPH" = {ml r."PMALPH"}, "PMROUT" = {ml r."PMROUT"}, "PMSRS" = {ml r."PMSRS"}, "PMCRBY" = {ml r."PMCRBY"}, "PMCRDT" = {ml r."PMCRDT"}, "PMUPBY" = {ml r."PMUPBY"}, "PMUPDT" = {ml r."PMUPDT"}, "PMIBN" = {ml r."PMIBN"}, "PMISTP" = {ml r."PMISTP"}, "PMIBGP" = {ml r."PMIBGP"}, "PMILCT" = {ml r."PMILCT"}
WHERE "PMAN8" = {ml r."PMAN8"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M04012',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M04012"
WHERE "PMAN8" = {ml r."PMAN8"}
 

'

/* upload_delete - End */

/* Table :M080111 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M080111',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M080111"."CTID"  ,"BUSDTA"."M080111"."CTTYP"  ,"BUSDTA"."M080111"."CTCD"  ,"BUSDTA"."M080111"."CTDSC1"  ,
     "BUSDTA"."M080111"."CTTXA1"    FROM "BUSDTA"."M080111"  WHERE "BUSDTA"."M080111"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M080111',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M080111_del"."CTID"

FROM "BUSDTA"."M080111_del"
WHERE "BUSDTA"."M080111_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M080111',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M080111"
("CTID", "CTTYP", "CTCD", "CTDSC1", "CTTXA1")
VALUES({ml r."CTID"}, {ml r."CTTYP"}, {ml r."CTCD"}, {ml r."CTDSC1"}, {ml r."CTTXA1"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M080111',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M080111"
SET "CTTYP" = {ml r."CTTYP"}, "CTCD" = {ml r."CTCD"}, "CTDSC1" = {ml r."CTDSC1"}, "CTTXA1" = {ml r."CTTXA1"}
WHERE "CTID" = {ml r."CTID"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M080111',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M080111"
WHERE "CTID" = {ml r."CTID"}
 

'

/* upload_delete - End */

/* Table :M40111 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40111',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M40111"."PCAN8"  ,"BUSDTA"."M40111"."PCIDLN"  ,"BUSDTA"."M40111"."PCAR1"  ,"BUSDTA"."M40111"."PCPH1"  ,
     "BUSDTA"."M40111"."PCEXTN1"  ,"BUSDTA"."M40111"."PCPHTP1"  ,"BUSDTA"."M40111"."PCDFLTPH1"  ,"BUSDTA"."M40111"."PCREFPH1"  ,
     "BUSDTA"."M40111"."PCAR2"  ,"BUSDTA"."M40111"."PCPH2"  ,"BUSDTA"."M40111"."PCEXTN2"  ,"BUSDTA"."M40111"."PCPHTP2"  ,
     "BUSDTA"."M40111"."PCDFLTPH2"  ,"BUSDTA"."M40111"."PCREFPH2"  ,"BUSDTA"."M40111"."PCAR3"  ,"BUSDTA"."M40111"."PCPH3"  ,
     "BUSDTA"."M40111"."PCEXTN3"  ,"BUSDTA"."M40111"."PCPHTP3"  ,"BUSDTA"."M40111"."PCDFLTPH3"  ,"BUSDTA"."M40111"."PCREFPH3"  ,
     "BUSDTA"."M40111"."PCAR4"  ,"BUSDTA"."M40111"."PCPH4"  ,"BUSDTA"."M40111"."PCEXTN4"  ,"BUSDTA"."M40111"."PCPHTP4"  ,
     "BUSDTA"."M40111"."PCDFLTPH4"  ,"BUSDTA"."M40111"."PCREFPH4"  ,"BUSDTA"."M40111"."PCEMAL1"  ,"BUSDTA"."M40111"."PCETP1"  ,
     "BUSDTA"."M40111"."PCDFLTEM1"  ,"BUSDTA"."M40111"."PCREFEM1"  ,"BUSDTA"."M40111"."PCEMAL2"  ,"BUSDTA"."M40111"."PCETP2"  ,
     "BUSDTA"."M40111"."PCDFLTEM2"  ,"BUSDTA"."M40111"."PCREFEM2"  ,"BUSDTA"."M40111"."PCEMAL3"  ,"BUSDTA"."M40111"."PCETP3"  ,
     "BUSDTA"."M40111"."PCDFLTEM3"  ,"BUSDTA"."M40111"."PCREFEM3"  ,"BUSDTA"."M40111"."PCGNNM"  ,"BUSDTA"."M40111"."PCMDNM"  ,
     "BUSDTA"."M40111"."PCSRNM"  ,"BUSDTA"."M40111"."PCTITL"  ,"BUSDTA"."M40111"."PCACTV"  ,"BUSDTA"."M40111"."PCDFLT"  ,
     "BUSDTA"."M40111"."PCSET"  ,"BUSDTA"."M40111"."PCCRBY"  ,"BUSDTA"."M40111"."PCCRDT"  ,"BUSDTA"."M40111"."PCUPBY"  ,
     "BUSDTA"."M40111"."PCUPDT"    
     FROM BUSDTA.F56M0001, BUSDTA.M40111, BUSDTA.M04012  
     WHERE "BUSDTA"."M40111"."last_modified" >= {ml s.last_table_download}   AND FFUSER = PMROUT and PMAN8 = PCAN8  and FFUSER= {ml s.username}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40111',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M40111_del"."PCAN8"
,"BUSDTA"."M40111_del"."PCIDLN"

FROM "BUSDTA"."M40111_del"
WHERE "BUSDTA"."M40111_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40111',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M40111"
("PCAN8", "PCIDLN", "PCAR1", "PCPH1", "PCEXTN1", "PCPHTP1", "PCDFLTPH1", "PCREFPH1", "PCAR2", "PCPH2", "PCEXTN2", "PCPHTP2", "PCDFLTPH2", "PCREFPH2", "PCAR3", "PCPH3", "PCEXTN3", "PCPHTP3", "PCDFLTPH3", "PCREFPH3", "PCAR4", "PCPH4", "PCEXTN4", "PCPHTP4", "PCDFLTPH4", "PCREFPH4", "PCEMAL1", "PCETP1", "PCDFLTEM1", "PCREFEM1", "PCEMAL2", "PCETP2", "PCDFLTEM2", "PCREFEM2", "PCEMAL3", "PCETP3", "PCDFLTEM3", "PCREFEM3", "PCGNNM", "PCMDNM", "PCSRNM", "PCTITL", "PCACTV", "PCDFLT", "PCSET", "PCCRBY", "PCCRDT", "PCUPBY", "PCUPDT")
VALUES({ml r."PCAN8"}, {ml r."PCIDLN"}, {ml r."PCAR1"}, {ml r."PCPH1"}, {ml r."PCEXTN1"}, {ml r."PCPHTP1"}, {ml r."PCDFLTPH1"}, {ml r."PCREFPH1"}, {ml r."PCAR2"}, {ml r."PCPH2"}, {ml r."PCEXTN2"}, {ml r."PCPHTP2"}, {ml r."PCDFLTPH2"}, {ml r."PCREFPH2"}, {ml r."PCAR3"}, {ml r."PCPH3"}, {ml r."PCEXTN3"}, {ml r."PCPHTP3"}, {ml r."PCDFLTPH3"}, {ml r."PCREFPH3"}, {ml r."PCAR4"}, {ml r."PCPH4"}, {ml r."PCEXTN4"}, {ml r."PCPHTP4"}, {ml r."PCDFLTPH4"}, {ml r."PCREFPH4"}, {ml r."PCEMAL1"}, {ml r."PCETP1"}, {ml r."PCDFLTEM1"}, {ml r."PCREFEM1"}, {ml r."PCEMAL2"}, {ml r."PCETP2"}, {ml r."PCDFLTEM2"}, {ml r."PCREFEM2"}, {ml r."PCEMAL3"}, {ml r."PCETP3"}, {ml r."PCDFLTEM3"}, {ml r."PCREFEM3"}, {ml r."PCGNNM"}, {ml r."PCMDNM"}, {ml r."PCSRNM"}, {ml r."PCTITL"}, {ml r."PCACTV"}, {ml r."PCDFLT"}, {ml r."PCSET"}, {ml r."PCCRBY"}, {ml r."PCCRDT"}, {ml r."PCUPBY"}, {ml r."PCUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40111',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M40111"
SET "PCAR1" = {ml r."PCAR1"}, "PCPH1" = {ml r."PCPH1"}, "PCEXTN1" = {ml r."PCEXTN1"}, "PCPHTP1" = {ml r."PCPHTP1"}, "PCDFLTPH1" = {ml r."PCDFLTPH1"}, "PCREFPH1" = {ml r."PCREFPH1"}, "PCAR2" = {ml r."PCAR2"}, "PCPH2" = {ml r."PCPH2"}, "PCEXTN2" = {ml r."PCEXTN2"}, "PCPHTP2" = {ml r."PCPHTP2"}, "PCDFLTPH2" = {ml r."PCDFLTPH2"}, "PCREFPH2" = {ml r."PCREFPH2"}, "PCAR3" = {ml r."PCAR3"}, "PCPH3" = {ml r."PCPH3"}, "PCEXTN3" = {ml r."PCEXTN3"}, "PCPHTP3" = {ml r."PCPHTP3"}, "PCDFLTPH3" = {ml r."PCDFLTPH3"}, "PCREFPH3" = {ml r."PCREFPH3"}, "PCAR4" = {ml r."PCAR4"}, "PCPH4" = {ml r."PCPH4"}, "PCEXTN4" = {ml r."PCEXTN4"}, "PCPHTP4" = {ml r."PCPHTP4"}, "PCDFLTPH4" = {ml r."PCDFLTPH4"}, "PCREFPH4" = {ml r."PCREFPH4"}, "PCEMAL1" = {ml r."PCEMAL1"}, "PCETP1" = {ml r."PCETP1"}, "PCDFLTEM1" = {ml r."PCDFLTEM1"}, "PCREFEM1" = {ml r."PCREFEM1"}, "PCEMAL2" = {ml r."PCEMAL2"}, "PCETP2" = {ml r."PCETP2"}, "PCDFLTEM2" = {ml r."PCDFLTEM2"}, "PCREFEM2" = {ml r."PCREFEM2"}, "PCEMAL3" = {ml r."PCEMAL3"}, "PCETP3" = {ml r."PCETP3"}, "PCDFLTEM3" = {ml r."PCDFLTEM3"}, "PCREFEM3" = {ml r."PCREFEM3"}, "PCGNNM" = {ml r."PCGNNM"}, "PCMDNM" = {ml r."PCMDNM"}, "PCSRNM" = {ml r."PCSRNM"}, "PCTITL" = {ml r."PCTITL"}, "PCACTV" = {ml r."PCACTV"}, "PCDFLT" = {ml r."PCDFLT"}, "PCSET" = {ml r."PCSET"}, "PCCRBY" = {ml r."PCCRBY"}, "PCCRDT" = {ml r."PCCRDT"}, "PCUPBY" = {ml r."PCUPBY"}, "PCUPDT" = {ml r."PCUPDT"}
WHERE "PCAN8" = {ml r."PCAN8"} AND "PCIDLN" = {ml r."PCIDLN"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40111',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M40111"
WHERE "PCAN8" = {ml r."PCAN8"} AND "PCIDLN" = {ml r."PCIDLN"}
 

'

/* upload_delete - End */

/* Table :M40116 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40116',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M40116"."PAAN8"  ,"BUSDTA"."M40116"."PAADD1"  ,"BUSDTA"."M40116"."PAADD2"  ,"BUSDTA"."M40116"."PAADD3"  ,
     "BUSDTA"."M40116"."PAADD4"  ,"BUSDTA"."M40116"."PAADDZ"  ,"BUSDTA"."M40116"."PACTY1"  ,"BUSDTA"."M40116"."PACOUN"  ,
     "BUSDTA"."M40116"."PAADDS"  ,"BUSDTA"."M40116"."PACRBY"  ,"BUSDTA"."M40116"."PACRDT"  ,"BUSDTA"."M40116"."PAUPBY"  ,
     "BUSDTA"."M40116"."PAUPDT"    
     FROM BUSDTA.F56M0001, BUSDTA.M40116, BUSDTA.M04012  
     WHERE "BUSDTA"."M40116"."last_modified" >= {ml s.last_table_download}  AND FFUSER = PMROUT and PMAN8 = PAAN8  and FFUSER = {ml s.username}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40116',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M40116_del"."PAAN8"

FROM "BUSDTA"."M40116_del"
WHERE "BUSDTA"."M40116_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40116',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M40116"
("PAAN8", "PAADD1", "PAADD2", "PAADD3", "PAADD4", "PAADDZ", "PACTY1", "PACOUN", "PAADDS", "PACRBY", "PACRDT", "PAUPBY", "PAUPDT")
VALUES({ml r."PAAN8"}, {ml r."PAADD1"}, {ml r."PAADD2"}, {ml r."PAADD3"}, {ml r."PAADD4"}, {ml r."PAADDZ"}, {ml r."PACTY1"}, {ml r."PACOUN"}, {ml r."PAADDS"}, {ml r."PACRBY"}, {ml r."PACRDT"}, {ml r."PAUPBY"}, {ml r."PAUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40116',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M40116"
SET "PAADD1" = {ml r."PAADD1"}, "PAADD2" = {ml r."PAADD2"}, "PAADD3" = {ml r."PAADD3"}, "PAADD4" = {ml r."PAADD4"}, "PAADDZ" = {ml r."PAADDZ"}, "PACTY1" = {ml r."PACTY1"}, "PACOUN" = {ml r."PACOUN"}, "PAADDS" = {ml r."PAADDS"}, "PACRBY" = {ml r."PACRBY"}, "PACRDT" = {ml r."PACRDT"}, "PAUPBY" = {ml r."PAUPBY"}, "PAUPDT" = {ml r."PAUPDT"}
WHERE "PAAN8" = {ml r."PAAN8"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M40116',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M40116"
WHERE "PAAN8" = {ml r."PAAN8"}
 

'

/* upload_delete - End */

/* Table :M4016 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M4016',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M4016"."POORTP"  ,"BUSDTA"."M4016"."POAN8"  ,"BUSDTA"."M4016"."POITM"  ,"BUSDTA"."M4016"."POSTDT"  ,
     "BUSDTA"."M4016"."POOSEQ"  ,"BUSDTA"."M4016"."POLITM"  ,"BUSDTA"."M4016"."POQTYU"  ,"BUSDTA"."M4016"."POUOM"  ,"BUSDTA"."M4016"."POLNTY"  ,
     "BUSDTA"."M4016"."POSRP1"  ,"BUSDTA"."M4016"."POSRP5"  ,"BUSDTA"."M4016"."POSTFG"  ,"BUSDTA"."M4016"."POCRBY"  ,"BUSDTA"."M4016"."POCRDT"  ,
     "BUSDTA"."M4016"."POUPBY"  ,"BUSDTA"."M4016"."POUPDT"    
     FROM BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.M4016  
     WHERE "BUSDTA"."M4016"."last_modified" >= {ml s.last_table_download} AND  FFAN8=SMSLSM and SMAN8=CRCUAN8 and POAN8=CRCRAN8  
     and FFUSER= {ml s.username}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M4016',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M4016_del"."POORTP"
,"BUSDTA"."M4016_del"."POAN8"
,"BUSDTA"."M4016_del"."POITM"
,"BUSDTA"."M4016_del"."POSTDT"

FROM "BUSDTA"."M4016_del"
WHERE "BUSDTA"."M4016_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M4016',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M4016"
("POORTP", "POAN8", "POITM", "POSTDT", "POOSEQ", "POLITM", "POQTYU", "POUOM", "POLNTY", "POSRP1", "POSRP5", "POSTFG", "POCRBY", "POCRDT", "POUPBY", "POUPDT")
VALUES({ml r."POORTP"}, {ml r."POAN8"}, {ml r."POITM"}, {ml r."POSTDT"}, {ml r."POOSEQ"}, {ml r."POLITM"}, {ml r."POQTYU"}, {ml r."POUOM"}, {ml r."POLNTY"}, {ml r."POSRP1"}, {ml r."POSRP5"}, {ml r."POSTFG"}, {ml r."POCRBY"}, {ml r."POCRDT"}, {ml r."POUPBY"}, {ml r."POUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M4016',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M4016"
SET "POOSEQ" = {ml r."POOSEQ"}, "POLITM" = {ml r."POLITM"}, "POQTYU" = {ml r."POQTYU"}, "POUOM" = {ml r."POUOM"}, "POLNTY" = {ml r."POLNTY"}, "POSRP1" = {ml r."POSRP1"}, "POSRP5" = {ml r."POSRP5"}, "POSTFG" = {ml r."POSTFG"}, "POCRBY" = {ml r."POCRBY"}, "POCRDT" = {ml r."POCRDT"}, "POUPBY" = {ml r."POUPBY"}, "POUPDT" = {ml r."POUPDT"}
WHERE "POORTP" = {ml r."POORTP"} AND "POAN8" = {ml r."POAN8"} AND "POITM" = {ml r."POITM"} AND "POSTDT" = {ml r."POSTDT"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M4016',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M4016"
WHERE "POORTP" = {ml r."POORTP"} AND "POAN8" = {ml r."POAN8"} AND "POITM" = {ml r."POITM"} AND "POSTDT" = {ml r."POSTDT"}
 

'

/* upload_delete - End */

/* Table :M5001 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5001',
	'download_cursor',
	'sql',

'

        SELECT "BUSDTA"."M5001"."TTID"  ,"BUSDTA"."M5001"."TTKEY"  ,"BUSDTA"."M5001"."TTTYP"  ,"BUSDTA"."M5001"."TTACTN"  ,
        "BUSDTA"."M5001"."TTADSC"  ,"BUSDTA"."M5001"."TTACTR"  ,"BUSDTA"."M5001"."TTSTAT"  ,"BUSDTA"."M5001"."TTISTRT"  ,
        "BUSDTA"."M5001"."TTIEND"  ,"BUSDTA"."M5001"."TTCRBY"  ,"BUSDTA"."M5001"."TTCRDT"  ,"BUSDTA"."M5001"."TTUPBY"  ,
        "BUSDTA"."M5001"."TTUPDT"    
        FROM "BUSDTA"."M5001"  
        WHERE "BUSDTA"."M5001"."last_modified">= {ml s.last_table_download}       
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5001',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M5001_del"."TTKEY"

FROM "BUSDTA"."M5001_del"
WHERE "BUSDTA"."M5001_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5001',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M5001"
("TTID", "TTKEY", "TTTYP", "TTACTN", "TTADSC", "TTACTR", "TTSTAT", "TTISTRT", "TTIEND", "TTCRBY", "TTCRDT", "TTUPBY", "TTUPDT")
VALUES({ml r."TTID"}, {ml r."TTKEY"}, {ml r."TTTYP"}, {ml r."TTACTN"}, {ml r."TTADSC"}, {ml r."TTACTR"}, {ml r."TTSTAT"}, {ml r."TTISTRT"}, {ml r."TTIEND"}, {ml r."TTCRBY"}, {ml r."TTCRDT"}, {ml r."TTUPBY"}, {ml r."TTUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5001',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M5001"
SET "TTID" = {ml r."TTID"}, "TTTYP" = {ml r."TTTYP"}, "TTACTN" = {ml r."TTACTN"}, "TTADSC" = {ml r."TTADSC"}, "TTACTR" = {ml r."TTACTR"}, "TTSTAT" = {ml r."TTSTAT"}, "TTISTRT" = {ml r."TTISTRT"}, "TTIEND" = {ml r."TTIEND"}, "TTCRBY" = {ml r."TTCRBY"}, "TTCRDT" = {ml r."TTCRDT"}, "TTUPBY" = {ml r."TTUPBY"}, "TTUPDT" = {ml r."TTUPDT"}
WHERE "TTKEY" = {ml r."TTKEY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5001',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M5001"
WHERE "TTKEY" = {ml r."TTKEY"}
 
'

/* upload_delete - End */

/* Table :M50012 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50012',
	'download_cursor',
	'sql',

'

    SELECT "BUSDTA"."M50012"."TDID"  ,"BUSDTA"."M50012"."TDROUT"  ,"BUSDTA"."M50012"."TDTYP"  ,"BUSDTA"."M50012"."TDCLASS"  ,
    "BUSDTA"."M50012"."TDDTLS"  ,"BUSDTA"."M50012"."TDSTRTTM"  ,"BUSDTA"."M50012"."TDENDTM"  ,"BUSDTA"."M50012"."TDSTID"  ,
    "BUSDTA"."M50012"."TDAN8"  ,"BUSDTA"."M50012"."TDSTTLID"  ,"BUSDTA"."M50012"."TDPNTID"  ,"BUSDTA"."M50012"."TDSTAT"  ,
    "BUSDTA"."M50012"."TDREFHDRID"  ,"BUSDTA"."M50012"."TDREFHDRTYP"  ,"BUSDTA"."M50012"."TDCRBY"  ,"BUSDTA"."M50012"."TDCRDT"  ,
    "BUSDTA"."M50012"."TDUPBY"  ,"BUSDTA"."M50012"."TDUPDT"    
    FROM "BUSDTA"."M50012"  
    WHERE "BUSDTA"."M50012"."last_modified">= {ml s.last_table_download}    AND "BUSDTA"."M50012"."TDROUT" = {ml s.username}  
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50012',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M50012_del"."TDID"
,"BUSDTA"."M50012_del"."TDROUT"

FROM "BUSDTA"."M50012_del"
WHERE "BUSDTA"."M50012_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50012',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M50012"
("TDID", "TDROUT", "TDTYP", "TDCLASS", "TDDTLS", "TDSTRTTM", "TDENDTM", "TDSTID", "TDAN8", "TDSTTLID", "TDPNTID", "TDSTAT", "TDREFHDRID", "TDREFHDRTYP", "TDCRBY", "TDCRDT", "TDUPBY", "TDUPDT")
VALUES({ml r."TDID"}, {ml r."TDROUT"}, {ml r."TDTYP"}, {ml r."TDCLASS"}, {ml r."TDDTLS"}, {ml r."TDSTRTTM"}, {ml r."TDENDTM"}, {ml r."TDSTID"}, {ml r."TDAN8"}, {ml r."TDSTTLID"}, {ml r."TDPNTID"}, {ml r."TDSTAT"}, {ml r."TDREFHDRID"}, {ml r."TDREFHDRTYP"}, {ml r."TDCRBY"}, {ml r."TDCRDT"}, {ml r."TDUPBY"}, {ml r."TDUPDT"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50012',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M50012"
SET "TDTYP" = {ml r."TDTYP"}, "TDCLASS" = {ml r."TDCLASS"}, "TDDTLS" = {ml r."TDDTLS"}, "TDSTRTTM" = {ml r."TDSTRTTM"}, "TDENDTM" = {ml r."TDENDTM"}, "TDSTID" = {ml r."TDSTID"}, "TDAN8" = {ml r."TDAN8"}, "TDSTTLID" = {ml r."TDSTTLID"}, "TDPNTID" = {ml r."TDPNTID"}, "TDSTAT" = {ml r."TDSTAT"}, "TDREFHDRID" = {ml r."TDREFHDRID"}, "TDREFHDRTYP" = {ml r."TDREFHDRTYP"}, "TDCRBY" = {ml r."TDCRBY"}, "TDCRDT" = {ml r."TDCRDT"}, "TDUPBY" = {ml r."TDUPBY"}, "TDUPDT" = {ml r."TDUPDT"}
WHERE "TDID" = {ml r."TDID"} AND "TDROUT" = {ml r."TDROUT"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50012',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M50012"
WHERE "TDID" = {ml r."TDID"} AND "TDROUT" = {ml r."TDROUT"}
 

'

/* upload_delete - End */

/* Table :M5002 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5002',
	'download_cursor',
	'sql',

'

        SELECT "BUSDTA"."M5002"."TNID"  ,"BUSDTA"."M5002"."TNKEY"  ,"BUSDTA"."M5002"."TNTYP"  ,"BUSDTA"."M5002"."TNACTN"  ,
        "BUSDTA"."M5002"."TNADSC"  ,"BUSDTA"."M5002"."TNACTR"  ,"BUSDTA"."M5002"."TNILDGRD"  ,"BUSDTA"."M5002"."TNCRBY"  ,
        "BUSDTA"."M5002"."TNCRDT"  ,"BUSDTA"."M5002"."TNUPBY"  ,"BUSDTA"."M5002"."TNUPDT"    
        FROM "BUSDTA"."M5002"  
        WHERE "BUSDTA"."M5002"."last_modified">= {ml s.last_table_download}       
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5002',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M5002_del"."TNKEY"

FROM "BUSDTA"."M5002_del"
WHERE "BUSDTA"."M5002_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5002',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M5002"
("TNID", "TNKEY", "TNTYP", "TNACTN", "TNADSC", "TNACTR", "TNILDGRD", "TNCRBY", "TNCRDT", "TNUPBY", "TNUPDT")
VALUES({ml r."TNID"}, {ml r."TNKEY"}, {ml r."TNTYP"}, {ml r."TNACTN"}, {ml r."TNADSC"}, {ml r."TNACTR"}, {ml r."TNILDGRD"}, {ml r."TNCRBY"}, {ml r."TNCRDT"}, {ml r."TNUPBY"}, {ml r."TNUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5002',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M5002"
SET "TNID" = {ml r."TNID"}, "TNTYP" = {ml r."TNTYP"}, "TNACTN" = {ml r."TNACTN"}, "TNADSC" = {ml r."TNADSC"}, "TNACTR" = {ml r."TNACTR"}, "TNILDGRD" = {ml r."TNILDGRD"}, "TNCRBY" = {ml r."TNCRBY"}, "TNCRDT" = {ml r."TNCRDT"}, "TNUPBY" = {ml r."TNUPBY"}, "TNUPDT" = {ml r."TNUPDT"}
WHERE "TNKEY" = {ml r."TNKEY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5002',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M5002"
WHERE "TNKEY" = {ml r."TNKEY"}
 

'

/* upload_delete - End */

/* Table :M5003 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5003',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M5003"."ALID"  ,"BUSDTA"."M5003"."ALHDID"  ,"BUSDTA"."M5003"."ALTMSTMP"  ,"BUSDTA"."M5003"."ALDSC"  ,
     "BUSDTA"."M5003"."ALTYP"  ,"BUSDTA"."M5003"."ALITX"  ,"BUSDTA"."M5003"."ALCRBY"  ,"BUSDTA"."M5003"."ALCRDT"  ,"BUSDTA"."M5003"."ALUPBY"  ,
     "BUSDTA"."M5003"."ALUPDT"    FROM "BUSDTA"."M5003"  WHERE "BUSDTA"."M5003"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5003',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M5003_del"."ALID"

FROM "BUSDTA"."M5003_del"
WHERE "BUSDTA"."M5003_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5003',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M5003"
("ALID", "ALHDID", "ALTMSTMP", "ALDSC", "ALTYP", "ALITX", "ALCRBY", "ALCRDT", "ALUPBY", "ALUPDT")
VALUES({ml r."ALID"}, {ml r."ALHDID"}, {ml r."ALTMSTMP"}, {ml r."ALDSC"}, {ml r."ALTYP"}, {ml r."ALITX"}, {ml r."ALCRBY"}, {ml r."ALCRDT"}, {ml r."ALUPBY"}, {ml r."ALUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5003',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M5003"
SET "ALHDID" = {ml r."ALHDID"}, "ALTMSTMP" = {ml r."ALTMSTMP"}, "ALDSC" = {ml r."ALDSC"}, "ALTYP" = {ml r."ALTYP"}, "ALITX" = {ml r."ALITX"}, "ALCRBY" = {ml r."ALCRBY"}, "ALCRDT" = {ml r."ALCRDT"}, "ALUPBY" = {ml r."ALUPBY"}, "ALUPDT" = {ml r."ALUPDT"}
WHERE "ALID" = {ml r."ALID"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5003',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M5003"
WHERE "ALID" = {ml r."ALID"}
 

'

/* upload_delete - End */

/* Table :M5005 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5005',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M5005"."SMKEY"  ,"BUSDTA"."M5005"."SMDIR"  ,"BUSDTA"."M5005"."SMPRTY"  ,"BUSDTA"."M5005"."SMCRBY"  ,
     "BUSDTA"."M5005"."SMCRDT"  ,"BUSDTA"."M5005"."SMUPBY"  ,"BUSDTA"."M5005"."SMUPDT"    
     FROM "BUSDTA"."M5005"  
     WHERE "BUSDTA"."M5005"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5005',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M5005_del"."SMKEY"

FROM "BUSDTA"."M5005_del"
WHERE "BUSDTA"."M5005_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5005',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M5005"
("SMKEY", "SMDIR", "SMPRTY", "SMCRBY", "SMCRDT", "SMUPBY", "SMUPDT")
VALUES({ml r."SMKEY"}, {ml r."SMDIR"}, {ml r."SMPRTY"}, {ml r."SMCRBY"}, {ml r."SMCRDT"}, {ml r."SMUPBY"}, {ml r."SMUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5005',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M5005"
SET "SMDIR" = {ml r."SMDIR"}, "SMPRTY" = {ml r."SMPRTY"}, "SMCRBY" = {ml r."SMCRBY"}, "SMCRDT" = {ml r."SMCRDT"}, "SMUPBY" = {ml r."SMUPBY"}, "SMUPDT" = {ml r."SMUPDT"}
WHERE "SMKEY" = {ml r."SMKEY"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M5005',
	'upload_delete',
	'sql',

'

/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M5005"
WHERE "SMKEY" = {ml r."SMKEY"}
 

'

/* upload_delete - End */

/* Table :M50052 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50052',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M50052"."SDID"  ,"BUSDTA"."M50052"."SDTXID"  ,"BUSDTA"."M50052"."SDKEY"  ,"BUSDTA"."M50052"."SDISYNCD"  ,
     "BUSDTA"."M50052"."SDTMSTMP"  ,"BUSDTA"."M50052"."SDCRBY"  ,"BUSDTA"."M50052"."SDCRDT"  ,"BUSDTA"."M50052"."SDUPBY"  ,
     "BUSDTA"."M50052"."SDUPDT"    FROM "BUSDTA"."M50052"  WHERE "BUSDTA"."M50052"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50052',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M50052_del"."SDID"
,"BUSDTA"."M50052_del"."SDTXID"

FROM "BUSDTA"."M50052_del"
WHERE "BUSDTA"."M50052_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50052',
	'upload_insert',
	'sql',

'
 
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M50052"
("SDID", "SDTXID", "SDKEY", "SDISYNCD", "SDTMSTMP", "SDCRBY", "SDCRDT", "SDUPBY", "SDUPDT")
VALUES({ml r."SDID"}, {ml r."SDTXID"}, {ml r."SDKEY"}, {ml r."SDISYNCD"}, {ml r."SDTMSTMP"}, {ml r."SDCRBY"}, {ml r."SDCRDT"}, {ml r."SDUPBY"}, {ml r."SDUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50052',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M50052"
SET "SDKEY" = {ml r."SDKEY"}, "SDISYNCD" = {ml r."SDISYNCD"}, "SDTMSTMP" = {ml r."SDTMSTMP"}, "SDCRBY" = {ml r."SDCRBY"}, "SDCRDT" = {ml r."SDCRDT"}, "SDUPBY" = {ml r."SDUPBY"}, "SDUPDT" = {ml r."SDUPDT"}
WHERE "SDID" = {ml r."SDID"} AND "SDTXID" = {ml r."SDTXID"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M50052',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M50052"
WHERE "SDID" = {ml r."SDID"} AND "SDTXID" = {ml r."SDTXID"}
 

'

/* upload_delete - End */

/* Table :M56M0001 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0001',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M56M0001"."DMDDC"  ,"BUSDTA"."M56M0001"."DMDDCD"  ,"BUSDTA"."M56M0001"."DMSTAT"  ,"BUSDTA"."M56M0001"."DMWPC"  ,
     "BUSDTA"."M56M0001"."DMCRBY"  ,"BUSDTA"."M56M0001"."DMCRDT"  ,"BUSDTA"."M56M0001"."DMUPBY"  ,"BUSDTA"."M56M0001"."DMUPDT"    
     FROM "BUSDTA"."M56M0001"  WHERE "BUSDTA"."M56M0001"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0001',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M56M0001_del"."DMDDC"

FROM "BUSDTA"."M56M0001_del"
WHERE "BUSDTA"."M56M0001_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0001',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M56M0001"
("DMDDC", "DMDDCD", "DMSTAT", "DMWPC", "DMCRBY", "DMCRDT", "DMUPBY", "DMUPDT")
VALUES({ml r."DMDDC"}, {ml r."DMDDCD"}, {ml r."DMSTAT"}, {ml r."DMWPC"}, {ml r."DMCRBY"}, {ml r."DMCRDT"}, {ml r."DMUPBY"}, {ml r."DMUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0001',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0001"
SET "DMDDCD" = {ml r."DMDDCD"}, "DMSTAT" = {ml r."DMSTAT"}, "DMWPC" = {ml r."DMWPC"}, "DMCRBY" = {ml r."DMCRBY"}, "DMCRDT" = {ml r."DMCRDT"}, "DMUPBY" = {ml r."DMUPBY"}, "DMUPDT" = {ml r."DMUPDT"}
WHERE "DMDDC" = {ml r."DMDDC"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0001',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M56M0001"
WHERE "DMDDC" = {ml r."DMDDC"}
 

'

/* upload_delete - End */

/* Table :M56M0002 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0002',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M56M0002"."DCDDC"  ,"BUSDTA"."M56M0002"."DCWN"  ,"BUSDTA"."M56M0002"."DCDN"  ,"BUSDTA"."M56M0002"."DCISST"  ,
     "BUSDTA"."M56M0002"."DCCRBY"  ,"BUSDTA"."M56M0002"."DCCRDT"  ,"BUSDTA"."M56M0002"."DCUPBY"  ,"BUSDTA"."M56M0002"."DCUPDT"    
     FROM "BUSDTA"."M56M0002"  WHERE "BUSDTA"."M56M0002"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0002',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M56M0002_del"."DCDDC"
,"BUSDTA"."M56M0002_del"."DCWN"
,"BUSDTA"."M56M0002_del"."DCDN"

FROM "BUSDTA"."M56M0002_del"
WHERE "BUSDTA"."M56M0002_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0002',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M56M0002"
("DCDDC", "DCWN", "DCDN", "DCISST", "DCCRBY", "DCCRDT", "DCUPBY", "DCUPDT")
VALUES({ml r."DCDDC"}, {ml r."DCWN"}, {ml r."DCDN"}, {ml r."DCISST"}, {ml r."DCCRBY"}, {ml r."DCCRDT"}, {ml r."DCUPBY"}, {ml r."DCUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0002',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0002"
SET "DCISST" = {ml r."DCISST"}, "DCCRBY" = {ml r."DCCRBY"}, "DCCRDT" = {ml r."DCCRDT"}, "DCUPBY" = {ml r."DCUPBY"}, "DCUPDT" = {ml r."DCUPDT"}
WHERE "DCDDC" = {ml r."DCDDC"} AND "DCWN" = {ml r."DCWN"} AND "DCDN" = {ml r."DCDN"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0002',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M56M0002"
WHERE "DCDDC" = {ml r."DCDDC"} AND "DCWN" = {ml r."DCWN"} AND "DCDN" = {ml r."DCDN"}
 

'

/* upload_delete - End */

/* Table :M56M0003 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0003',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."M56M0003"."RSROUT"  ,"BUSDTA"."M56M0003"."RSAN8"  ,"BUSDTA"."M56M0003"."RSWN"  ,"BUSDTA"."M56M0003"."RSDN"  ,
     "BUSDTA"."M56M0003"."RSSN"  ,"BUSDTA"."M56M0003"."RSDDC"  ,"BUSDTA"."M56M0003"."RSCRBY"  ,"BUSDTA"."M56M0003"."RSCRDT"  ,
     "BUSDTA"."M56M0003"."RSUPBY"  ,"BUSDTA"."M56M0003"."RSUPDT"    FROM "BUSDTA"."M56M0003"  
     WHERE "BUSDTA"."M56M0003"."last_modified" >= {ml s.last_table_download}  AND "BUSDTA"."M56M0003"."RSROUT" = {ml s.username}       
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0003',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."M56M0003_del"."RSROUT"
,"BUSDTA"."M56M0003_del"."RSAN8"
,"BUSDTA"."M56M0003_del"."RSWN"
,"BUSDTA"."M56M0003_del"."RSDN"

FROM "BUSDTA"."M56M0003_del"
WHERE "BUSDTA"."M56M0003_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0003',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M56M0003"
("RSROUT", "RSAN8", "RSWN", "RSDN", "RSSN", "RSDDC", "RSCRBY", "RSCRDT", "RSUPBY", "RSUPDT")
VALUES({ml r."RSROUT"}, {ml r."RSAN8"}, {ml r."RSWN"}, {ml r."RSDN"}, {ml r."RSSN"}, {ml r."RSDDC"}, {ml r."RSCRBY"}, {ml r."RSCRDT"}, {ml r."RSUPBY"}, {ml r."RSUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0003',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0003"
SET "RSSN" = {ml r."RSSN"}, "RSDDC" = {ml r."RSDDC"}, "RSCRBY" = {ml r."RSCRBY"}, "RSCRDT" = {ml r."RSCRDT"}, "RSUPBY" = {ml r."RSUPBY"}, "RSUPDT" = {ml r."RSUPDT"}
WHERE "RSROUT" = {ml r."RSROUT"} AND "RSAN8" = {ml r."RSAN8"} AND "RSWN" = {ml r."RSWN"} AND "RSDN" = {ml r."RSDN"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0003',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M56M0003"
WHERE "RSROUT" = {ml r."RSROUT"} AND "RSAN8" = {ml r."RSAN8"} AND "RSWN" = {ml r."RSWN"} AND "RSDN" = {ml r."RSDN"}
 

'

/* upload_delete - End */

/* Table :M56M0004 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0004',
	'download_cursor',
	'sql',

'

/* Requested by Nimesh to change the filter(18-01-2016) */

SELECT "BUSDTA"."M56M0004"."RPSTID"  ,"BUSDTA"."M56M0004"."RPROUT"  ,"BUSDTA"."M56M0004"."RPAN8"  ,"BUSDTA"."M56M0004"."RPSTDT" 
,"BUSDTA"."M56M0004"."RPOGDT"  ,"BUSDTA"."M56M0004"."RPSN"  ,"BUSDTA"."M56M0004"."RPVTTP"  ,"BUSDTA"."M56M0004"."RPSTTP"  
,"BUSDTA"."M56M0004"."RPRSTID"  ,"BUSDTA"."M56M0004"."RPISRSN"  ,"BUSDTA"."M56M0004"."RPACTID"  ,"BUSDTA"."M56M0004"."RPRCID"  
,"BUSDTA"."M56M0004"."RPCACT"  ,"BUSDTA"."M56M0004"."RPPACT"  ,"BUSDTA"."M56M0004"."RPCHOVRD"  ,"BUSDTA"."M56M0004"."RPCRBY"  
,"BUSDTA"."M56M0004"."RPCRDT"  ,"BUSDTA"."M56M0004"."RPUPBY"  ,"BUSDTA"."M56M0004"."RPUPDT"    
FROM BUSDTA.M56M0004 
WHERE "BUSDTA"."M56M0004"."last_modified" >= {ml s.last_table_download} AND 
RTRIM(LTRIM(RPROUT)) = (select RTRIM(LTRIM(RouteName)) from busdta.Route_Master where RouteName = {ml s.username} )
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0004',
	'download_delete_cursor',
	'sql',

'
SELECT "BUSDTA"."M56M0004_del"."RPSTID"
,"BUSDTA"."M56M0004_del"."RPAN8"

FROM "BUSDTA"."M56M0004_del"
WHERE "BUSDTA"."M56M0004_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0004',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."M56M0004"
("RPSTID", "RPROUT", "RPAN8", "RPSTDT", "RPOGDT", "RPSN", "RPVTTP", "RPSTTP", "RPRSTID", "RPISRSN", "RPACTID", "RPRCID", "RPCACT", "RPPACT", "RPCHOVRD", "RPCRBY", "RPCRDT", "RPUPBY", "RPUPDT")
VALUES({ml r."RPSTID"}, {ml r."RPROUT"}, {ml r."RPAN8"}, {ml r."RPSTDT"}, {ml r."RPOGDT"}, {ml r."RPSN"}, {ml r."RPVTTP"}, {ml r."RPSTTP"}, {ml r."RPRSTID"}, {ml r."RPISRSN"}, {ml r."RPACTID"}, {ml r."RPRCID"}, {ml r."RPCACT"}, {ml r."RPPACT"}, {ml r."RPCHOVRD"}, {ml r."RPCRBY"}, {ml r."RPCRDT"}, {ml r."RPUPBY"}, {ml r."RPUPDT"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0004',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0004"
SET "RPROUT" = {ml r."RPROUT"}, "RPSTDT" = {ml r."RPSTDT"}, "RPOGDT" = {ml r."RPOGDT"}, "RPSN" = {ml r."RPSN"}, "RPVTTP" = {ml r."RPVTTP"}, "RPSTTP" = {ml r."RPSTTP"}, "RPRSTID" = {ml r."RPRSTID"}, "RPISRSN" = {ml r."RPISRSN"}, "RPACTID" = {ml r."RPACTID"}, "RPRCID" = {ml r."RPRCID"}, "RPCACT" = {ml r."RPCACT"}, "RPPACT" = {ml r."RPPACT"}, "RPCHOVRD" = {ml r."RPCHOVRD"}, "RPCRBY" = {ml r."RPCRBY"}, "RPCRDT" = {ml r."RPCRDT"}, "RPUPBY" = {ml r."RPUPBY"}, "RPUPDT" = {ml r."RPUPDT"}
WHERE "RPSTID" = {ml r."RPSTID"} AND "RPAN8" = {ml r."RPAN8"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0004',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."M56M0004"
WHERE "RPSTID" = {ml r."RPSTID"} AND "RPAN8" = {ml r."RPAN8"}
 

'

/* upload_delete - End */

/* Table :Order_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail',
	'download_cursor',
	'sql',

'

 
 
SELECT "BUSDTA"."Order_Detail"."OrderID"
,"BUSDTA"."Order_Detail"."LineID"
,"BUSDTA"."Order_Detail"."ItemId"
,"BUSDTA"."Order_Detail"."LongItem"
,"BUSDTA"."Order_Detail"."ItemLocation"
,"BUSDTA"."Order_Detail"."ItemLotNumber"
,"BUSDTA"."Order_Detail"."LineType"
,"BUSDTA"."Order_Detail"."IsNonStock"
,"BUSDTA"."Order_Detail"."OrderQty"
,"BUSDTA"."Order_Detail"."OrderUM"
,"BUSDTA"."Order_Detail"."OrderQtyInPricingUM"
,"BUSDTA"."Order_Detail"."PricingUM"
,"BUSDTA"."Order_Detail"."UnitPriceOriginalAmtInPriceUM"
,"BUSDTA"."Order_Detail"."UnitPriceAmtInPriceUoM"
,"BUSDTA"."Order_Detail"."UnitPriceAmt"
,"BUSDTA"."Order_Detail"."ExtnPriceAmt"
,"BUSDTA"."Order_Detail"."ItemSalesTaxAmt"
,"BUSDTA"."Order_Detail"."Discounted"
,"BUSDTA"."Order_Detail"."PriceOverriden"
,"BUSDTA"."Order_Detail"."PriceOverrideReasonCodeId"
,"BUSDTA"."Order_Detail"."IsTaxable"
,"BUSDTA"."Order_Detail"."ReturnHeldQty"
,"BUSDTA"."Order_Detail"."SalesCat1"
,"BUSDTA"."Order_Detail"."SalesCat2"
,"BUSDTA"."Order_Detail"."SalesCat3"
,"BUSDTA"."Order_Detail"."SalesCat4"
,"BUSDTA"."Order_Detail"."SalesCat5"
,"BUSDTA"."Order_Detail"."PurchasingCat1"
,"BUSDTA"."Order_Detail"."PurchasingCat2"
,"BUSDTA"."Order_Detail"."PurchasingCat3"
,"BUSDTA"."Order_Detail"."PurchasingCat4"
,"BUSDTA"."Order_Detail"."PurchasingCat5"
,"BUSDTA"."Order_Detail"."JDEOrderCompany"
,"BUSDTA"."Order_Detail"."JDEOrderNumber"
,"BUSDTA"."Order_Detail"."JDEOrderType"
,"BUSDTA"."Order_Detail"."JDEOrderLine"
,"BUSDTA"."Order_Detail"."JDEInvoiceCompany"
,"BUSDTA"."Order_Detail"."JDEInvoiceNumber"
,"BUSDTA"."Order_Detail"."JDEOInvoiceType"
,"BUSDTA"."Order_Detail"."JDEZBatchNumber"
,"BUSDTA"."Order_Detail"."JDEProcessID"
,"BUSDTA"."Order_Detail"."SettlementID"
,"BUSDTA"."Order_Detail"."ExtendedAmtVariance"
,"BUSDTA"."Order_Detail"."TaxAmountAmtVariance"
,"BUSDTA"."Order_Detail"."HoldCode"
,"BUSDTA"."Order_Detail"."CreatedBy"
,"BUSDTA"."Order_Detail"."CreatedDatetime"
,"BUSDTA"."Order_Detail"."UpdatedBy"
,"BUSDTA"."Order_Detail"."UpdatedDatetime"
FROM "BUSDTA"."Order_Detail"
WHERE "BUSDTA"."Order_Detail"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail',
	'download_delete_cursor',
	'sql',

'
 


SELECT "BUSDTA"."Order_Detail_del"."OrderID"
,"BUSDTA"."Order_Detail_del"."LineID"
FROM "BUSDTA"."Order_Detail_del"
WHERE "BUSDTA"."Order_Detail_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail',
	'upload_insert',
	'sql',

'


/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Detail"
("OrderID", "LineID", "ItemId", "LongItem", "ItemLocation", "ItemLotNumber", "LineType", "IsNonStock", "OrderQty", "OrderUM", "OrderQtyInPricingUM", "PricingUM", "UnitPriceOriginalAmtInPriceUM", "UnitPriceAmtInPriceUoM", "UnitPriceAmt", "ExtnPriceAmt", "ItemSalesTaxAmt", "Discounted", "PriceOverriden", "PriceOverrideReasonCodeId", "IsTaxable", "ReturnHeldQty", "SalesCat1", "SalesCat2", "SalesCat3", "SalesCat4", "SalesCat5", "PurchasingCat1", "PurchasingCat2", "PurchasingCat3", "PurchasingCat4", "PurchasingCat5", "JDEOrderCompany", "JDEOrderNumber", "JDEOrderType", "JDEOrderLine", "JDEInvoiceCompany", "JDEInvoiceNumber", "JDEOInvoiceType", "JDEZBatchNumber", "JDEProcessID", "SettlementID", "ExtendedAmtVariance", "TaxAmountAmtVariance", "HoldCode", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderID"}, {ml r."LineID"}, {ml r."ItemId"}, {ml r."LongItem"}, {ml r."ItemLocation"}, {ml r."ItemLotNumber"}, {ml r."LineType"}, {ml r."IsNonStock"}, {ml r."OrderQty"}, {ml r."OrderUM"}, {ml r."OrderQtyInPricingUM"}, {ml r."PricingUM"}, {ml r."UnitPriceOriginalAmtInPriceUM"}, {ml r."UnitPriceAmtInPriceUoM"}, {ml r."UnitPriceAmt"}, {ml r."ExtnPriceAmt"}, {ml r."ItemSalesTaxAmt"}, {ml r."Discounted"}, {ml r."PriceOverriden"}, {ml r."PriceOverrideReasonCodeId"}, {ml r."IsTaxable"}, {ml r."ReturnHeldQty"}, {ml r."SalesCat1"}, {ml r."SalesCat2"}, {ml r."SalesCat3"}, {ml r."SalesCat4"}, {ml r."SalesCat5"}, {ml r."PurchasingCat1"}, {ml r."PurchasingCat2"}, {ml r."PurchasingCat3"}, {ml r."PurchasingCat4"}, {ml r."PurchasingCat5"}, {ml r."JDEOrderCompany"}, {ml r."JDEOrderNumber"}, {ml r."JDEOrderType"}, {ml r."JDEOrderLine"}, {ml r."JDEInvoiceCompany"}, {ml r."JDEInvoiceNumber"}, {ml r."JDEOInvoiceType"}, {ml r."JDEZBatchNumber"}, {ml r."JDEProcessID"}, {ml r."SettlementID"}, {ml r."ExtendedAmtVariance"}, {ml r."TaxAmountAmtVariance"}, {ml r."HoldCode"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail',
	'upload_update',
	'sql',

'
 

/* Update the row in the consolidated database. */  
UPDATE "BUSDTA"."Order_Detail"  SET "ItemId" = {ml r."ItemId"}, "LongItem" = {ml r."LongItem"}, "ItemLocation" = {ml r."ItemLocation"}, "ItemLotNumber" = {ml r."ItemLotNumber"}, "LineType" = {ml r."LineType"}, "IsNonStock" = {ml r."IsNonStock"}, "OrderQty" = {ml r."OrderQty"}, "OrderUM" = {ml r."OrderUM"}, "OrderQtyInPricingUM" = {ml r."OrderQtyInPricingUM"}, "PricingUM" = {ml r."PricingUM"}, "UnitPriceOriginalAmtInPriceUM" = {ml r."UnitPriceOriginalAmtInPriceUM"}, "UnitPriceAmtInPriceUoM" = {ml r."UnitPriceAmtInPriceUoM"}, "UnitPriceAmt" = {ml r."UnitPriceAmt"}, "ExtnPriceAmt" = {ml r."ExtnPriceAmt"}, "ItemSalesTaxAmt" = {ml r."ItemSalesTaxAmt"}, "Discounted" = {ml r."Discounted"}, "PriceOverriden" = {ml r."PriceOverriden"}, "PriceOverrideReasonCodeId" = {ml r."PriceOverrideReasonCodeId"}, "IsTaxable" = {ml r."IsTaxable"}, "ReturnHeldQty" = {ml r."ReturnHeldQty"}, "SalesCat1" = {ml r."SalesCat1"}, "SalesCat2" = {ml r."SalesCat2"}, "SalesCat3" = {ml r."SalesCat3"}, "SalesCat4" = {ml r."SalesCat4"}, "SalesCat5" = {ml r."SalesCat5"}, "PurchasingCat1" = {ml r."PurchasingCat1"}, "PurchasingCat2" = {ml r."PurchasingCat2"}, "PurchasingCat3" = {ml r."PurchasingCat3"}, "PurchasingCat4" = {ml r."PurchasingCat4"}, "PurchasingCat5" = {ml r."PurchasingCat5"}, "JDEOrderCompany" = {ml r."JDEOrderCompany"}, "JDEOrderNumber" = {ml r."JDEOrderNumber"}, "JDEOrderType" = {ml r."JDEOrderType"}, "JDEOrderLine" = {ml r."JDEOrderLine"}, "JDEInvoiceCompany" = {ml r."JDEInvoiceCompany"}, "JDEInvoiceNumber" = {ml r."JDEInvoiceNumber"}, "JDEOInvoiceType" = {ml r."JDEOInvoiceType"}, "JDEZBatchNumber" = {ml r."JDEZBatchNumber"}, "JDEProcessID" = {ml r."JDEProcessID"}, "SettlementID" = {ml r."SettlementID"}, "ExtendedAmtVariance" = {ml r."ExtendedAmtVariance"}, "TaxAmountAmtVariance" = {ml r."TaxAmountAmtVariance"}, "HoldCode" = {ml r."HoldCode"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}  WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"}                     


'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail',
	'upload_delete',
	'sql',

'
 

/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Order_Detail"
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"}

'

/* upload_delete - End */

/* Table :Order_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header',
	'download_cursor',
	'sql',

'

 

SELECT "BUSDTA"."Order_Header"."OrderID"
,"BUSDTA"."Order_Header"."OrderTypeId"
,"BUSDTA"."Order_Header"."OriginatingRouteID"
,"BUSDTA"."Order_Header"."Branch"
,"BUSDTA"."Order_Header"."CustShipToId"
,"BUSDTA"."Order_Header"."CustBillToId"
,"BUSDTA"."Order_Header"."CustParentId"
,"BUSDTA"."Order_Header"."OrderDate"
,"BUSDTA"."Order_Header"."TotalCoffeeAmt"
,"BUSDTA"."Order_Header"."TotalAlliedAmt"
,"BUSDTA"."Order_Header"."OrderTotalAmt"
,"BUSDTA"."Order_Header"."SalesTaxAmt"
,"BUSDTA"."Order_Header"."InvoiceTotalAmt"
,"BUSDTA"."Order_Header"."EnergySurchargeAmt"
,"BUSDTA"."Order_Header"."VarianceAmt"
,"BUSDTA"."Order_Header"."SurchargeReasonCodeId"
,"BUSDTA"."Order_Header"."OrderStateId"
,"BUSDTA"."Order_Header"."OrderSignature"
,"BUSDTA"."Order_Header"."ChargeOnAccountSignature"
,"BUSDTA"."Order_Header"."JDEOrderCompany"
,"BUSDTA"."Order_Header"."JDEOrderNumber"
,"BUSDTA"."Order_Header"."JDEOrderType"
,"BUSDTA"."Order_Header"."JDEInvoiceCompany"
,"BUSDTA"."Order_Header"."JDEInvoiceNumber"
,"BUSDTA"."Order_Header"."JDEOInvoiceType"
,"BUSDTA"."Order_Header"."JDEZBatchNumber"
,"BUSDTA"."Order_Header"."JDEProcessID"
,"BUSDTA"."Order_Header"."SettlementID"
,"BUSDTA"."Order_Header"."PaymentTerms"
,"BUSDTA"."Order_Header"."CustomerReference1"
,"BUSDTA"."Order_Header"."CustomerReference2"
,"BUSDTA"."Order_Header"."AdjustmentSchedule"
,"BUSDTA"."Order_Header"."TaxArea"
,"BUSDTA"."Order_Header"."TaxExplanationCode"
,"BUSDTA"."Order_Header"."VoidReasonCodeId"
,"BUSDTA"."Order_Header"."ChargeOnAccount"
,"BUSDTA"."Order_Header"."HoldCommitted"
,"BUSDTA"."Order_Header"."CreatedBy"
,"BUSDTA"."Order_Header"."CreatedDatetime"
,"BUSDTA"."Order_Header"."UpdatedBy"
,"BUSDTA"."Order_Header"."UpdatedDatetime"
FROM "BUSDTA"."Order_Header"
WHERE "BUSDTA"."Order_Header"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Order_Header"."OriginatingRouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username}) 

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header',
	'download_delete_cursor',
	'sql',

'


SELECT "BUSDTA"."Order_Header_del"."OrderID"
FROM "BUSDTA"."Order_Header_del"
WHERE "BUSDTA"."Order_Header_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header',
	'upload_insert',
	'sql',

'


/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Header"
("OrderID", "OrderTypeId", "OriginatingRouteID", "Branch", "CustShipToId", "CustBillToId", "CustParentId", "OrderDate", "TotalCoffeeAmt", "TotalAlliedAmt", "OrderTotalAmt", "SalesTaxAmt", "InvoiceTotalAmt", "EnergySurchargeAmt", "VarianceAmt", "SurchargeReasonCodeId", "OrderStateId", "OrderSignature", "ChargeOnAccountSignature", "JDEOrderCompany", "JDEOrderNumber", "JDEOrderType", "JDEInvoiceCompany", "JDEInvoiceNumber", "JDEOInvoiceType", "JDEZBatchNumber", "JDEProcessID", "SettlementID", "PaymentTerms", "CustomerReference1", "CustomerReference2", "AdjustmentSchedule", "TaxArea", "TaxExplanationCode", "VoidReasonCodeId", "ChargeOnAccount", "HoldCommitted", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderID"}, {ml r."OrderTypeId"}, {ml r."OriginatingRouteID"}, {ml r."Branch"}, {ml r."CustShipToId"}, {ml r."CustBillToId"}, {ml r."CustParentId"}, {ml r."OrderDate"}, {ml r."TotalCoffeeAmt"}, {ml r."TotalAlliedAmt"}, {ml r."OrderTotalAmt"}, {ml r."SalesTaxAmt"}, {ml r."InvoiceTotalAmt"}, {ml r."EnergySurchargeAmt"}, {ml r."VarianceAmt"}, {ml r."SurchargeReasonCodeId"}, {ml r."OrderStateId"}, {ml r."OrderSignature"}, {ml r."ChargeOnAccountSignature"}, {ml r."JDEOrderCompany"}, {ml r."JDEOrderNumber"}, {ml r."JDEOrderType"}, {ml r."JDEInvoiceCompany"}, {ml r."JDEInvoiceNumber"}, {ml r."JDEOInvoiceType"}, {ml r."JDEZBatchNumber"}, {ml r."JDEProcessID"}, {ml r."SettlementID"}, {ml r."PaymentTerms"}, {ml r."CustomerReference1"}, {ml r."CustomerReference2"}, {ml r."AdjustmentSchedule"}, {ml r."TaxArea"}, {ml r."TaxExplanationCode"}, {ml r."VoidReasonCodeId"}, {ml r."ChargeOnAccount"}, {ml r."HoldCommitted"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header',
	'upload_update',
	'sql',

'


/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_Header"
SET "OrderTypeId" = {ml r."OrderTypeId"}, "OriginatingRouteID" = {ml r."OriginatingRouteID"}, "Branch" = {ml r."Branch"}, "CustShipToId" = {ml r."CustShipToId"}, "CustBillToId" = {ml r."CustBillToId"}, "CustParentId" = {ml r."CustParentId"}, "OrderDate" = {ml r."OrderDate"}, "TotalCoffeeAmt" = {ml r."TotalCoffeeAmt"}, "TotalAlliedAmt" = {ml r."TotalAlliedAmt"}, "OrderTotalAmt" = {ml r."OrderTotalAmt"}, "SalesTaxAmt" = {ml r."SalesTaxAmt"}, "InvoiceTotalAmt" = {ml r."InvoiceTotalAmt"}, "EnergySurchargeAmt" = {ml r."EnergySurchargeAmt"}, "VarianceAmt" = {ml r."VarianceAmt"}, "SurchargeReasonCodeId" = {ml r."SurchargeReasonCodeId"}, "OrderStateId" = {ml r."OrderStateId"}, "OrderSignature" = {ml r."OrderSignature"}, "ChargeOnAccountSignature" = {ml r."ChargeOnAccountSignature"}, "JDEOrderCompany" = {ml r."JDEOrderCompany"}, "JDEOrderNumber" = {ml r."JDEOrderNumber"}, "JDEOrderType" = {ml r."JDEOrderType"}, "JDEInvoiceCompany" = {ml r."JDEInvoiceCompany"}, "JDEInvoiceNumber" = {ml r."JDEInvoiceNumber"}, "JDEOInvoiceType" = {ml r."JDEOInvoiceType"}, "JDEZBatchNumber" = {ml r."JDEZBatchNumber"}, "JDEProcessID" = {ml r."JDEProcessID"}, "SettlementID" = {ml r."SettlementID"}, "PaymentTerms" = {ml r."PaymentTerms"}, "CustomerReference1" = {ml r."CustomerReference1"}, "CustomerReference2" = {ml r."CustomerReference2"}, "AdjustmentSchedule" = {ml r."AdjustmentSchedule"}, "TaxArea" = {ml r."TaxArea"}, "TaxExplanationCode" = {ml r."TaxExplanationCode"}, "VoidReasonCodeId" = {ml r."VoidReasonCodeId"}, "ChargeOnAccount" = {ml r."ChargeOnAccount"}, "HoldCommitted" = {ml r."HoldCommitted"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "OrderID" = {ml r."OrderID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header',
	'upload_delete',
	'sql',

'


/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Order_Header"
WHERE "OrderID" = {ml r."OrderID"}
'

/* upload_delete - End */

/* Table :Payment_Ref_Map */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Payment_Ref_Map',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Payment_Ref_Map"."Payment_Ref_Map_Id"
,"BUSDTA"."Payment_Ref_Map"."Payment_Id"
,"BUSDTA"."Payment_Ref_Map"."Ref_Id"
,"BUSDTA"."Payment_Ref_Map"."Ref_Type"

FROM "BUSDTA"."Payment_Ref_Map"
WHERE "BUSDTA"."Payment_Ref_Map"."last_modified">= {ml s.last_table_download}

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Payment_Ref_Map',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Payment_Ref_Map_del"."Payment_Ref_Map_Id"
,"BUSDTA"."Payment_Ref_Map_del"."Payment_Id"
,"BUSDTA"."Payment_Ref_Map_del"."Ref_Id"

FROM "BUSDTA"."Payment_Ref_Map_del"
WHERE "BUSDTA"."Payment_Ref_Map_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Payment_Ref_Map',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Payment_Ref_Map"
("Payment_Ref_Map_Id", "Payment_Id", "Ref_Id", "Ref_Type")
VALUES({ml r."Payment_Ref_Map_Id"}, {ml r."Payment_Id"}, {ml r."Ref_Id"}, {ml r."Ref_Type"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Payment_Ref_Map',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Payment_Ref_Map"
SET "Ref_Type" = {ml r."Ref_Type"}
WHERE "Payment_Ref_Map_Id" = {ml r."Payment_Ref_Map_Id"} AND "Payment_Id" = {ml r."Payment_Id"} AND "Ref_Id" = {ml r."Ref_Id"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Payment_Ref_Map',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Payment_Ref_Map"
WHERE "Payment_Ref_Map_Id" = {ml r."Payment_Ref_Map_Id"} AND "Payment_Id" = {ml r."Payment_Id"} AND "Ref_Id" = {ml r."Ref_Id"}
'

/* upload_delete - End */

/* Table :PickOrder */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder',
	'download_cursor',
	'sql',

'

SELECT "busdta"."PickOrder"."PickOrder_Id"
,"busdta"."PickOrder"."Order_ID"
,"busdta"."PickOrder"."RouteId"
,"busdta"."PickOrder"."Item_Number"
,"busdta"."PickOrder"."Order_Qty"
,"busdta"."PickOrder"."Order_UOM"
,"busdta"."PickOrder"."Picked_Qty_Primary_UOM"
,"busdta"."PickOrder"."Primary_UOM"
,"busdta"."PickOrder"."Order_Qty_Primary_UOM"
,"busdta"."PickOrder"."On_Hand_Qty_Primary"
,"busdta"."PickOrder"."Last_Scan_Mode"
,"busdta"."PickOrder"."Item_Scan_Sequence"
,"busdta"."PickOrder"."Picked_By"
,"busdta"."PickOrder"."IsOnHold"
,"busdta"."PickOrder"."Reason_Code_Id"
,"busdta"."PickOrder"."ManuallyPickCount"

FROM busdta.Order_Header, busdta.PickOrder, busdta.Route_Master rm, busdta.Customer_Route_Map crm
WHERE "busdta"."PickOrder"."last_modified" >= {ml s.last_table_download}
AND CustShipToID = crm.RelatedAddressBookNumber and busdta.Order_Header.OrderID = busdta.PickOrder.Order_ID AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )

 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."PickOrder_del"."PickOrder_Id"
,"BUSDTA"."PickOrder_del"."Order_ID"
,"BUSDTA"."PickOrder_del"."RouteId"

FROM "BUSDTA"."PickOrder_del"
WHERE "BUSDTA"."PickOrder_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."PickOrder"
("PickOrder_Id", "Order_ID", "RouteId", "Item_Number", "Order_Qty", "Order_UOM", "Picked_Qty_Primary_UOM", "Primary_UOM", "Order_Qty_Primary_UOM", "On_Hand_Qty_Primary", "Last_Scan_Mode", "Item_Scan_Sequence", "Picked_By", "IsOnHold", "Reason_Code_Id", "ManuallyPickCount")
VALUES({ml r."PickOrder_Id"}, {ml r."Order_ID"}, {ml r."RouteId"}, {ml r."Item_Number"}, {ml r."Order_Qty"}, {ml r."Order_UOM"}, {ml r."Picked_Qty_Primary_UOM"}, {ml r."Primary_UOM"}, {ml r."Order_Qty_Primary_UOM"}, {ml r."On_Hand_Qty_Primary"}, {ml r."Last_Scan_Mode"}, {ml r."Item_Scan_Sequence"}, {ml r."Picked_By"}, {ml r."IsOnHold"}, {ml r."Reason_Code_Id"}, {ml r."ManuallyPickCount"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PickOrder"
SET "Item_Number" = {ml r."Item_Number"}, "Order_Qty" = {ml r."Order_Qty"}, "Order_UOM" = {ml r."Order_UOM"}, "Picked_Qty_Primary_UOM" = {ml r."Picked_Qty_Primary_UOM"}, "Primary_UOM" = {ml r."Primary_UOM"}, "Order_Qty_Primary_UOM" = {ml r."Order_Qty_Primary_UOM"}, "On_Hand_Qty_Primary" = {ml r."On_Hand_Qty_Primary"}, "Last_Scan_Mode" = {ml r."Last_Scan_Mode"}, "Item_Scan_Sequence" = {ml r."Item_Scan_Sequence"}, "Picked_By" = {ml r."Picked_By"}, "IsOnHold" = {ml r."IsOnHold"}, "Reason_Code_Id" = {ml r."Reason_Code_Id"}, "ManuallyPickCount" = {ml r."ManuallyPickCount"}
WHERE "PickOrder_Id" = {ml r."PickOrder_Id"} AND "Order_ID" = {ml r."Order_ID"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."PickOrder"
WHERE "PickOrder_Id" = {ml r."PickOrder_Id"} AND "Order_ID" = {ml r."Order_ID"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :PickOrder_Exception */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder_Exception',
	'download_cursor',
	'sql',

'

 
/*New column changes CustomerID to CustShipToID */
SELECT "BUSDTA"."PickOrder_Exception"."PickOrder_Exception_Id"
,"BUSDTA"."PickOrder_Exception"."Order_Id"
,"BUSDTA"."PickOrder_Exception"."RouteId"
,"BUSDTA"."PickOrder_Exception"."Item_Number"
,"BUSDTA"."PickOrder_Exception"."Exception_Qty"
,"BUSDTA"."PickOrder_Exception"."UOM"
,"BUSDTA"."PickOrder_Exception"."Exception_Reason"
,"BUSDTA"."PickOrder_Exception"."ManualPickReasonCode"
,"BUSDTA"."PickOrder_Exception"."ManuallyPickCount"

FROM  BUSDTA.Order_Header, BUSDTA.PickOrder_Exception, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE "BUSDTA"."PickOrder_Exception"."last_modified" >= {ml s.last_table_download}
AND CustShipToID = crm.RelatedAddressBookNumber and BUSDTA.Order_Header.OrderID = busdta.PickOrder_Exception.Order_ID AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder_Exception',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."PickOrder_Exception_del"."PickOrder_Exception_Id"
,"BUSDTA"."PickOrder_Exception_del"."Order_Id"
,"BUSDTA"."PickOrder_Exception_del"."RouteId"

FROM "BUSDTA"."PickOrder_Exception_del"
WHERE "BUSDTA"."PickOrder_Exception_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder_Exception',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."PickOrder_Exception"
("PickOrder_Exception_Id", "Order_Id", "RouteId", "Item_Number", "Exception_Qty", "UOM", "Exception_Reason", "ManualPickReasonCode", "ManuallyPickCount")
VALUES({ml r."PickOrder_Exception_Id"}, {ml r."Order_Id"}, {ml r."RouteId"}, {ml r."Item_Number"}, {ml r."Exception_Qty"}, {ml r."UOM"}, {ml r."Exception_Reason"}, {ml r."ManualPickReasonCode"}, {ml r."ManuallyPickCount"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder_Exception',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PickOrder_Exception"
SET "Item_Number" = {ml r."Item_Number"}, "Exception_Qty" = {ml r."Exception_Qty"}, "UOM" = {ml r."UOM"}, "Exception_Reason" = {ml r."Exception_Reason"}, "ManualPickReasonCode" = {ml r."ManualPickReasonCode"}, "ManuallyPickCount" = {ml r."ManuallyPickCount"}
WHERE "PickOrder_Exception_Id" = {ml r."PickOrder_Exception_Id"} AND "Order_Id" = {ml r."Order_Id"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PickOrder_Exception',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."PickOrder_Exception"
WHERE "PickOrder_Exception_Id" = {ml r."PickOrder_Exception_Id"} AND "Order_Id" = {ml r."Order_Id"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_delete - End */

/* Table :ReasonCodeMaster */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ReasonCodeMaster',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."ReasonCodeMaster"."ReasonCodeId"
,"BUSDTA"."ReasonCodeMaster"."ReasonCode"
,"BUSDTA"."ReasonCodeMaster"."ReasonCodeDescription"
,"BUSDTA"."ReasonCodeMaster"."ReasonCodeType"
,"BUSDTA"."ReasonCodeMaster"."CreatedBy"
,"BUSDTA"."ReasonCodeMaster"."CreatedDatetime"
,"BUSDTA"."ReasonCodeMaster"."UpdatedBy"
,"BUSDTA"."ReasonCodeMaster"."UpdatedDatetime"

FROM "BUSDTA"."ReasonCodeMaster"
WHERE "BUSDTA"."ReasonCodeMaster"."last_modified">= {ml s.last_table_download}

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ReasonCodeMaster',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."ReasonCodeMaster_del"."ReasonCode"

FROM "BUSDTA"."ReasonCodeMaster_del"
WHERE "BUSDTA"."ReasonCodeMaster_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :Route_Device_Map */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Device_Map',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Route_Device_Map"."Route_Id"
,"BUSDTA"."Route_Device_Map"."Device_Id"
,"BUSDTA"."Route_Device_Map"."Active"
,"BUSDTA"."Route_Device_Map"."Remote_Id"

FROM "BUSDTA"."Route_Device_Map"
WHERE "BUSDTA"."Route_Device_Map"."last_modified">= {ml s.last_table_download}

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Device_Map',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Route_Device_Map_del"."Route_Id"
,"BUSDTA"."Route_Device_Map_del"."Device_Id"

FROM "BUSDTA"."Route_Device_Map_del"
WHERE "BUSDTA"."Route_Device_Map_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Device_Map',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Device_Map"
("Route_Id", "Device_Id", "Active", "Remote_Id")
VALUES({ml r."Route_Id"}, {ml r."Device_Id"}, {ml r."Active"}, {ml r."Remote_Id"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Device_Map',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Device_Map"
SET "Active" = {ml r."Active"}, "Remote_Id" = {ml r."Remote_Id"}
WHERE "Route_Id" = {ml r."Route_Id"} AND "Device_Id" = {ml r."Device_Id"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Device_Map',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Route_Device_Map"
WHERE "Route_Id" = {ml r."Route_Id"} AND "Device_Id" = {ml r."Device_Id"}
 
'

/* upload_delete - End */

/* Table :Route_User_Map */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_User_Map',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Route_User_Map"."App_user_id"
,"BUSDTA"."Route_User_Map"."Route_Id"
,"BUSDTA"."Route_User_Map"."Active"
,"BUSDTA"."Route_User_Map"."Default_Route"

FROM "BUSDTA"."Route_User_Map"
WHERE "BUSDTA"."Route_User_Map"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_User_Map',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Route_User_Map_del"."App_user_id"
,"BUSDTA"."Route_User_Map_del"."Route_Id"

FROM "BUSDTA"."Route_User_Map_del"
WHERE "BUSDTA"."Route_User_Map_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_User_Map',
	'upload_insert',
	'sql',

'

/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_User_Map"
("App_user_id", "Route_Id", "Active", "Default_Route")
VALUES({ml r."App_user_id"}, {ml r."Route_Id"}, {ml r."Active"}, {ml r."Default_Route"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_User_Map',
	'upload_update',
	'sql',

'

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_User_Map"
SET "Active" = {ml r."Active"}, "Default_Route" = {ml r."Default_Route"}
WHERE "App_user_id" = {ml r."App_user_id"} AND "Route_Id" = {ml r."Route_Id"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_User_Map',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Route_User_Map"
WHERE "App_user_id" = {ml r."App_user_id"} AND "Route_Id" = {ml r."Route_Id"}
'

/* upload_delete - End */

/* Table :UDCKEYLIST */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'UDCKEYLIST',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."UDCKEYLIST"."DTSY"
,"BUSDTA"."UDCKEYLIST"."DTRT"

FROM "BUSDTA"."UDCKEYLIST"
WHERE "BUSDTA"."UDCKEYLIST"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'UDCKEYLIST',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."UDCKEYLIST_del"."DTSY"
,"BUSDTA"."UDCKEYLIST_del"."DTRT"

FROM "BUSDTA"."UDCKEYLIST_del"
WHERE "BUSDTA"."UDCKEYLIST_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'UDCKEYLIST',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."UDCKEYLIST"
("DTSY", "DTRT")
VALUES({ml r."DTSY"}, {ml r."DTRT"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'UDCKEYLIST',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."UDCKEYLIST"
 
WHERE "DTSY" = {ml r."DTSY"} AND "DTRT" = {ml r."DTRT"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'UDCKEYLIST',
	'upload_delete',
	'sql',

'
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."UDCKEYLIST"
WHERE "DTSY" = {ml r."DTSY"} AND "DTRT" = {ml r."DTRT"}
'

/* upload_delete - End */

/* Table :User_Role_Map */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'User_Role_Map',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."User_Role_Map"."App_user_id"
,"BUSDTA"."User_Role_Map"."Role"

FROM "BUSDTA"."User_Role_Map"
WHERE "BUSDTA"."User_Role_Map"."last_modified">= {ml s.last_table_download}
 

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'User_Role_Map',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."User_Role_Map_del"."App_user_id"

FROM "BUSDTA"."User_Role_Map_del"
WHERE "BUSDTA"."User_Role_Map_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'User_Role_Map',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."User_Role_Map"
("App_user_id", "Role")
VALUES({ml r."App_user_id"}, {ml r."Role"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'User_Role_Map',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."User_Role_Map"
SET "Role" = {ml r."Role"}
WHERE "App_user_id" = {ml r."App_user_id"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'User_Role_Map',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."User_Role_Map"
WHERE "App_user_id" = {ml r."App_user_id"}
 

'

/* upload_delete - End */

/* Table :Inventory */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."Inventory"."ItemId"  ,"BUSDTA"."Inventory"."ItemNumber"  ,"BUSDTA"."Inventory"."RouteId"  ,
     "BUSDTA"."Inventory"."OnHandQuantity"  ,"BUSDTA"."Inventory"."CommittedQuantity"  ,"BUSDTA"."Inventory"."HeldQuantity"  ,
     "BUSDTA"."Inventory"."ParLevel"  ,"BUSDTA"."Inventory"."LastReceiptDate"  ,"BUSDTA"."Inventory"."LastConsumeDate"  ,
     "BUSDTA"."Inventory"."CreatedBy"  ,"BUSDTA"."Inventory"."CreatedDatetime"  ,"BUSDTA"."Inventory"."UpdatedBy"  ,
     "BUSDTA"."Inventory"."UpdatedDatetime"    
     FROM "BUSDTA"."Inventory"  
     WHERE "BUSDTA"."Inventory"."last_modified" >= {ml s.last_table_download}  
     AND "BUSDTA"."Inventory"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Inventory_del"."ItemId"
,"BUSDTA"."Inventory_del"."RouteId"

FROM "BUSDTA"."Inventory_del"
WHERE "BUSDTA"."Inventory_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Inventory"
("ItemId", "ItemNumber", "RouteId", "OnHandQuantity", "CommittedQuantity", "HeldQuantity", "ParLevel", "LastReceiptDate", "LastConsumeDate", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ItemId"}, {ml r."ItemNumber"}, {ml r."RouteId"}, {ml r."OnHandQuantity"}, {ml r."CommittedQuantity"}, {ml r."HeldQuantity"}, {ml r."ParLevel"}, {ml r."LastReceiptDate"}, {ml r."LastConsumeDate"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Inventory"
SET "ItemNumber" = {ml r."ItemNumber"}, "OnHandQuantity" = {ml r."OnHandQuantity"}, "CommittedQuantity" = {ml r."CommittedQuantity"}, "HeldQuantity" = {ml r."HeldQuantity"}, "ParLevel" = {ml r."ParLevel"}, "LastReceiptDate" = {ml r."LastReceiptDate"}, "LastConsumeDate" = {ml r."LastConsumeDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Inventory"
WHERE "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :Inventory_Ledger */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Ledger',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."Inventory_Ledger"."InventoryLedgerID"  ,"BUSDTA"."Inventory_Ledger"."ItemId"  ,"BUSDTA"."Inventory_Ledger"."ItemNumber"  ,
     "BUSDTA"."Inventory_Ledger"."RouteId"  ,"BUSDTA"."Inventory_Ledger"."TransactionQty"  ,"BUSDTA"."Inventory_Ledger"."TransactionQtyUM"  ,
     "BUSDTA"."Inventory_Ledger"."TransactionQtyPrimaryUM"  ,"BUSDTA"."Inventory_Ledger"."TransactionType"  ,
     "BUSDTA"."Inventory_Ledger"."TransactionId"  ,"BUSDTA"."Inventory_Ledger"."SettlementID"  ,"BUSDTA"."Inventory_Ledger"."CreatedBy"  ,
     "BUSDTA"."Inventory_Ledger"."CreatedDatetime"  ,"BUSDTA"."Inventory_Ledger"."UpdatedBy"  ,"BUSDTA"."Inventory_Ledger"."UpdatedDatetime"    
     FROM "BUSDTA"."Inventory_Ledger"  WHERE "BUSDTA"."Inventory_Ledger"."last_modified" >= {ml s.last_table_download}  
     AND "BUSDTA"."Inventory_Ledger"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Ledger',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Inventory_Ledger_del"."InventoryLedgerID"
,"BUSDTA"."Inventory_Ledger_del"."ItemId"
,"BUSDTA"."Inventory_Ledger_del"."RouteId"

FROM "BUSDTA"."Inventory_Ledger_del"
WHERE "BUSDTA"."Inventory_Ledger_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Ledger',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Inventory_Ledger"
("InventoryLedgerID", "ItemId", "ItemNumber", "RouteId", "TransactionQty", "TransactionQtyUM", "TransactionQtyPrimaryUM", "TransactionType", "TransactionId", "SettlementID", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."InventoryLedgerID"}, {ml r."ItemId"}, {ml r."ItemNumber"}, {ml r."RouteId"}, {ml r."TransactionQty"}, {ml r."TransactionQtyUM"}, {ml r."TransactionQtyPrimaryUM"}, {ml r."TransactionType"}, {ml r."TransactionId"}, {ml r."SettlementID"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Ledger',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Inventory_Ledger"
SET "ItemNumber" = {ml r."ItemNumber"}, "TransactionQty" = {ml r."TransactionQty"}, "TransactionQtyUM" = {ml r."TransactionQtyUM"}, "TransactionQtyPrimaryUM" = {ml r."TransactionQtyPrimaryUM"}, "TransactionType" = {ml r."TransactionType"}, "TransactionId" = {ml r."TransactionId"}, "SettlementID" = {ml r."SettlementID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "InventoryLedgerID" = {ml r."InventoryLedgerID"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Ledger',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Inventory_Ledger"
WHERE "InventoryLedgerID" = {ml r."InventoryLedgerID"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :Inventory_Adjustment */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Adjustment',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Inventory_Adjustment"."InventoryAdjustmentId"
,"BUSDTA"."Inventory_Adjustment"."ItemId"
,"BUSDTA"."Inventory_Adjustment"."ItemNumber"
,"BUSDTA"."Inventory_Adjustment"."RouteId"
,"BUSDTA"."Inventory_Adjustment"."TransactionQty"
,"BUSDTA"."Inventory_Adjustment"."TransactionQtyUM"
,"BUSDTA"."Inventory_Adjustment"."TransactionQtyPrimaryUM"
,"BUSDTA"."Inventory_Adjustment"."ReasonCode"
,"BUSDTA"."Inventory_Adjustment"."IsApproved"
,"BUSDTA"."Inventory_Adjustment"."IsApplied"
,"BUSDTA"."Inventory_Adjustment"."CreatedBy"
,"BUSDTA"."Inventory_Adjustment"."CreatedDatetime"
,"BUSDTA"."Inventory_Adjustment"."UpdatedBy"
,"BUSDTA"."Inventory_Adjustment"."UpdatedDatetime"
,"BUSDTA"."Inventory_Adjustment"."AdjustmentStatus"
,"BUSDTA"."Inventory_Adjustment"."Source"
FROM "BUSDTA"."Inventory_Adjustment"  
WHERE "BUSDTA"."Inventory_Adjustment"."last_modified" >= {ml s.last_table_download}  
AND "BUSDTA"."Inventory_Adjustment"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})    
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Adjustment',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Inventory_Adjustment_del"."InventoryAdjustmentId"
,"BUSDTA"."Inventory_Adjustment_del"."ItemId"
,"BUSDTA"."Inventory_Adjustment_del"."RouteId"

FROM "BUSDTA"."Inventory_Adjustment_del"
WHERE "BUSDTA"."Inventory_Adjustment_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Adjustment',
	'upload_insert',
	'sql',

'


/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Inventory_Adjustment"
("InventoryAdjustmentId", "ItemId", "ItemNumber", "RouteId", "TransactionQty", "TransactionQtyUM", "TransactionQtyPrimaryUM", "ReasonCode", "IsApproved", "IsApplied", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime", "AdjustmentStatus", "Source")
VALUES({ml r."InventoryAdjustmentId"}, {ml r."ItemId"}, {ml r."ItemNumber"}, {ml r."RouteId"}, {ml r."TransactionQty"}, {ml r."TransactionQtyUM"}, {ml r."TransactionQtyPrimaryUM"}, {ml r."ReasonCode"}, {ml r."IsApproved"}, {ml r."IsApplied"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"}, {ml r."AdjustmentStatus"}, {ml r."Source"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Adjustment',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Inventory_Adjustment"
SET "ItemNumber" = {ml r."ItemNumber"}, "TransactionQty" = {ml r."TransactionQty"}, "TransactionQtyUM" = {ml r."TransactionQtyUM"}, "TransactionQtyPrimaryUM" = {ml r."TransactionQtyPrimaryUM"}, "ReasonCode" = {ml r."ReasonCode"}, "IsApproved" = {ml r."IsApproved"}, "IsApplied" = {ml r."IsApplied"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}, "AdjustmentStatus" = {ml r."AdjustmentStatus"}, "Source" = {ml r."Source"}
WHERE "InventoryAdjustmentId" = {ml r."InventoryAdjustmentId"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Inventory_Adjustment',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Inventory_Adjustment"
WHERE "InventoryAdjustmentId" = {ml r."InventoryAdjustmentId"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :ItemConfiguration */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ItemConfiguration',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."ItemConfiguration"."ItemID"  ,"BUSDTA"."ItemConfiguration"."RouteEnabled"  ,"BUSDTA"."ItemConfiguration"."Sellable"  ,
     "BUSDTA"."ItemConfiguration"."AllowSearch"  ,"BUSDTA"."ItemConfiguration"."PrimaryUM"  ,"BUSDTA"."ItemConfiguration"."PricingUM"  ,
     "BUSDTA"."ItemConfiguration"."TransactionUM"  ,"BUSDTA"."ItemConfiguration"."OtherUM1"  ,"BUSDTA"."ItemConfiguration"."OtherUM2"  ,
     "BUSDTA"."ItemConfiguration"."AllowLooseSample"  ,"BUSDTA"."ItemConfiguration"."CreatedBy"  ,"BUSDTA"."ItemConfiguration"."CreatedDatetime"  ,
     "BUSDTA"."ItemConfiguration"."UpdatedBy"  ,"BUSDTA"."ItemConfiguration"."UpdatedDatetime"    
     FROM "BUSDTA"."ItemConfiguration", "BUSDTA"."F4102", BUSDTA.Route_Master rm  
     WHERE ("BUSDTA"."ItemConfiguration"."last_modified">= {ml s.last_table_download}
      or "BUSDTA"."F4102"."last_modified">= {ml s.last_table_download}   or rm.last_modified>= {ml s.last_table_download})   
      AND ItemID=IBITM AND IBMCU = rm.BranchNumber   
      AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ItemConfiguration',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."ItemConfiguration_del"."ItemID"

FROM "BUSDTA"."ItemConfiguration_del"
WHERE "BUSDTA"."ItemConfiguration_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :ItemCrossReference */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ItemCrossReference',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."ItemCrossReference"."ItemReferenceID"  ,"BUSDTA"."ItemCrossReference"."CrossReferenceID"  ,
     "BUSDTA"."ItemCrossReference"."MobileType"  ,"BUSDTA"."ItemCrossReference"."EffectiveFrom"  ,"BUSDTA"."ItemCrossReference"."EffectiveThru"  ,
     "BUSDTA"."ItemCrossReference"."CrossData"  ,"BUSDTA"."ItemCrossReference"."AddressNumber"  ,
     "BUSDTA"."ItemCrossReference"."CrossReferenceType"  ,"BUSDTA"."ItemCrossReference"."ItemNumber"  ,
     "BUSDTA"."ItemCrossReference"."ItemRevisionLevel"  ,"BUSDTA"."ItemCrossReference"."CreatedBy"  ,
     "BUSDTA"."ItemCrossReference"."CreatedDatetime"  ,"BUSDTA"."ItemCrossReference"."UpdatedBy"  ,
     "BUSDTA"."ItemCrossReference"."UpdatedDatetime"    
     FROM "BUSDTA"."ItemCrossReference"  
     WHERE "BUSDTA"."ItemCrossReference"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ItemCrossReference',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."ItemCrossReference_del"."ItemReferenceID"
,"BUSDTA"."ItemCrossReference_del"."CrossReferenceID"

FROM "BUSDTA"."ItemCrossReference_del"
WHERE "BUSDTA"."ItemCrossReference_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :ItemUoMs */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ItemUoMs',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."ItemUoMs"."ItemID"  ,"BUSDTA"."ItemUoMs"."UOM"  ,"BUSDTA"."ItemUoMs"."CrossReferenceID"  ,"BUSDTA"."ItemUoMs"."CanSell"  ,
     "BUSDTA"."ItemUoMs"."CanSample"  ,"BUSDTA"."ItemUoMs"."CanRestock"  ,"BUSDTA"."ItemUoMs"."DisplaySeq"  ,"BUSDTA"."ItemUoMs"."CreatedBy"  ,
     "BUSDTA"."ItemUoMs"."CreatedDatetime"  ,"BUSDTA"."ItemUoMs"."UpdatedBy"  ,"BUSDTA"."ItemUoMs"."UpdatedDatetime"    
     FROM "BUSDTA"."ItemUoMs", "BUSDTA"."F4102", BUSDTA.Route_Master rm  
     WHERE ("BUSDTA"."ItemUoMs"."last_modified">= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}    
     or "BUSDTA"."F4102"."last_modified" >= {ml s.last_table_download})  AND ItemID=IBITM  AND IBMCU = rm.BranchNumber  
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ItemUoMs',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."ItemUoMs_del"."ItemID"
,"BUSDTA"."ItemUoMs_del"."UOM"

FROM "BUSDTA"."ItemUoMs_del"
WHERE "BUSDTA"."ItemUoMs_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :UoMFactorReference */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'UoMFactorReference',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."UoMFactorReference"."ItemID"
,"BUSDTA"."UoMFactorReference"."FromUOM"
,"BUSDTA"."UoMFactorReference"."ToUOM"
,"BUSDTA"."UoMFactorReference"."ConversionFactor"
,"BUSDTA"."UoMFactorReference"."GenerationDate"
,"BUSDTA"."UoMFactorReference"."CreatedBy"
,"BUSDTA"."UoMFactorReference"."CreatedDatetime"
,"BUSDTA"."UoMFactorReference"."UpdatedBy"
,"BUSDTA"."UoMFactorReference"."UpdatedDatetime"

FROM "BUSDTA"."UoMFactorReference"
WHERE "BUSDTA"."UoMFactorReference"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'UoMFactorReference',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."UoMFactorReference_del"."ItemID"
,"BUSDTA"."UoMFactorReference_del"."FromUOM"
,"BUSDTA"."UoMFactorReference_del"."ToUOM"

FROM "BUSDTA"."UoMFactorReference_del"
WHERE "BUSDTA"."UoMFactorReference_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */

/* Table :Question_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Question_Master',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Question_Master"."QuestionId"
,"BUSDTA"."Question_Master"."QuestionTitle"
,"BUSDTA"."Question_Master"."QuestionDescription"
,"BUSDTA"."Question_Master"."IsMandatory"
,"BUSDTA"."Question_Master"."IsMultivalue"
,"BUSDTA"."Question_Master"."IsDescriptive"
,"BUSDTA"."Question_Master"."CreatedBy"
,"BUSDTA"."Question_Master"."CreatedDatetime"
,"BUSDTA"."Question_Master"."UpdatedBy"
,"BUSDTA"."Question_Master"."UpdatedDatetime"

FROM "BUSDTA"."Question_Master"
WHERE "BUSDTA"."Question_Master"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Question_Master',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Question_Master_del"."QuestionId"

FROM "BUSDTA"."Question_Master_del"
WHERE "BUSDTA"."Question_Master_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :Response_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Response_Master',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Response_Master"."ResponseId"
,"BUSDTA"."Response_Master"."Response"
,"BUSDTA"."Response_Master"."CreatedBy"
,"BUSDTA"."Response_Master"."CreatedDatetime"
,"BUSDTA"."Response_Master"."UpdatedBy"
,"BUSDTA"."Response_Master"."UpdatedDatetime"

FROM "BUSDTA"."Response_Master"
WHERE "BUSDTA"."Response_Master"."last_modified">= {ml s.last_table_download}

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Response_Master',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Response_Master_del"."ResponseId"

FROM "BUSDTA"."Response_Master_del"
WHERE "BUSDTA"."Response_Master_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :Question_Template */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Question_Template',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Question_Template"."TemplateId"
,"BUSDTA"."Question_Template"."TemplateName"
,"BUSDTA"."Question_Template"."QuestionId"
,"BUSDTA"."Question_Template"."ResponseID"
,"BUSDTA"."Question_Template"."RevisionId"
,"BUSDTA"."Question_Template"."CreatedBy"
,"BUSDTA"."Question_Template"."CreatedDatetime"
,"BUSDTA"."Question_Template"."UpdatedBy"
,"BUSDTA"."Question_Template"."UpdatedDatetime"

FROM "BUSDTA"."Question_Template"
WHERE "BUSDTA"."Question_Template"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Question_Template',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Question_Template_del"."TemplateId"
,"BUSDTA"."Question_Template_del"."QuestionId"
,"BUSDTA"."Question_Template_del"."ResponseID"
,"BUSDTA"."Question_Template_del"."RevisionId"

FROM "BUSDTA"."Question_Template_del"
WHERE "BUSDTA"."Question_Template_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :PreTrip_Inspection_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Header',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."PreTrip_Inspection_Header"."PreTripInspectionHeaderId"
,"BUSDTA"."PreTrip_Inspection_Header"."RouteId"
,"BUSDTA"."PreTrip_Inspection_Header"."TemplateId"
,"BUSDTA"."PreTrip_Inspection_Header"."PreTripDateTime"
,"BUSDTA"."PreTrip_Inspection_Header"."UserName"
,"BUSDTA"."PreTrip_Inspection_Header"."StatusId"
,"BUSDTA"."PreTrip_Inspection_Header"."VehicleMake"
,"BUSDTA"."PreTrip_Inspection_Header"."VehicleNumber"
,"BUSDTA"."PreTrip_Inspection_Header"."OdoMmeterReading"
,"BUSDTA"."PreTrip_Inspection_Header"."Comment"
,"BUSDTA"."PreTrip_Inspection_Header"."VerSignature"
,"BUSDTA"."PreTrip_Inspection_Header"."CreatedBy"
,"BUSDTA"."PreTrip_Inspection_Header"."CreatedDatetime"
,"BUSDTA"."PreTrip_Inspection_Header"."UpdatedBy"
,"BUSDTA"."PreTrip_Inspection_Header"."UpdatedDatetime"

FROM "BUSDTA"."PreTrip_Inspection_Header"
WHERE "BUSDTA"."PreTrip_Inspection_Header"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."PreTrip_Inspection_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Header',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."PreTrip_Inspection_Header_del"."PreTripInspectionHeaderId"
,"BUSDTA"."PreTrip_Inspection_Header_del"."RouteId"

FROM "BUSDTA"."PreTrip_Inspection_Header_del"
WHERE "BUSDTA"."PreTrip_Inspection_Header_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Header',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."PreTrip_Inspection_Header"
("PreTripInspectionHeaderId", "RouteId", "TemplateId", "PreTripDateTime", "UserName", "StatusId", "VehicleMake", "VehicleNumber", "OdoMmeterReading", "Comment", "VerSignature", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."PreTripInspectionHeaderId"}, {ml r."RouteId"}, {ml r."TemplateId"}, {ml r."PreTripDateTime"}, {ml r."UserName"}, {ml r."StatusId"}, {ml r."VehicleMake"}, {ml r."VehicleNumber"}, {ml r."OdoMmeterReading"}, {ml r."Comment"}, {ml r."VerSignature"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Header',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PreTrip_Inspection_Header"
SET "TemplateId" = {ml r."TemplateId"}, "PreTripDateTime" = {ml r."PreTripDateTime"}, "UserName" = {ml r."UserName"}, "StatusId" = {ml r."StatusId"}, "VehicleMake" = {ml r."VehicleMake"}, "VehicleNumber" = {ml r."VehicleNumber"}, "OdoMmeterReading" = {ml r."OdoMmeterReading"}, "Comment" = {ml r."Comment"}, "VerSignature" = {ml r."VerSignature"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "PreTripInspectionHeaderId" = {ml r."PreTripInspectionHeaderId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Header',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."PreTrip_Inspection_Header"
WHERE "PreTripInspectionHeaderId" = {ml r."PreTripInspectionHeaderId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_delete - End */

/* Table :PreTrip_Inspection_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Detail',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."PreTrip_Inspection_Detail"."PreTripInspectionHeaderId"
,"BUSDTA"."PreTrip_Inspection_Detail"."PreTripInspectionDetailId"
,"BUSDTA"."PreTrip_Inspection_Detail"."RouteId"
,"BUSDTA"."PreTrip_Inspection_Detail"."QuestionId"
,"BUSDTA"."PreTrip_Inspection_Detail"."ResponseID"
,"BUSDTA"."PreTrip_Inspection_Detail"."ResponseReason"
,"BUSDTA"."PreTrip_Inspection_Detail"."CreatedBy"
,"BUSDTA"."PreTrip_Inspection_Detail"."CreatedDatetime"
,"BUSDTA"."PreTrip_Inspection_Detail"."UpdatedBy"
,"BUSDTA"."PreTrip_Inspection_Detail"."UpdatedDatetime"

FROM "BUSDTA"."PreTrip_Inspection_Detail"
WHERE "BUSDTA"."PreTrip_Inspection_Detail"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."PreTrip_Inspection_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Detail',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."PreTrip_Inspection_Detail_del"."PreTripInspectionHeaderId"
,"BUSDTA"."PreTrip_Inspection_Detail_del"."PreTripInspectionDetailId"
,"BUSDTA"."PreTrip_Inspection_Detail_del"."RouteId"

FROM "BUSDTA"."PreTrip_Inspection_Detail_del"
WHERE "BUSDTA"."PreTrip_Inspection_Detail_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Detail',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."PreTrip_Inspection_Detail"
("PreTripInspectionHeaderId", "PreTripInspectionDetailId", "RouteId", "QuestionId", "ResponseID", "ResponseReason", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."PreTripInspectionHeaderId"}, {ml r."PreTripInspectionDetailId"}, {ml r."RouteId"}, {ml r."QuestionId"}, {ml r."ResponseID"}, {ml r."ResponseReason"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Detail',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PreTrip_Inspection_Detail"
SET "QuestionId" = {ml r."QuestionId"}, "ResponseID" = {ml r."ResponseID"}, "ResponseReason" = {ml r."ResponseReason"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "PreTripInspectionHeaderId" = {ml r."PreTripInspectionHeaderId"} AND "PreTripInspectionDetailId" = {ml r."PreTripInspectionDetailId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'PreTrip_Inspection_Detail',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."PreTrip_Inspection_Detail"
WHERE "PreTripInspectionHeaderId" = {ml r."PreTripInspectionHeaderId"} AND "PreTripInspectionDetailId" = {ml r."PreTripInspectionDetailId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_delete - End */

/* Table :Cash_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cash_Master',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Cash_Master"."CashId"  
,"BUSDTA"."Cash_Master"."CashType"  
,"BUSDTA"."Cash_Master"."CashCode"  
,"BUSDTA"."Cash_Master"."CashDescription"  
,"BUSDTA"."Cash_Master"."CreatedBy"  
,"BUSDTA"."Cash_Master"."CreatedDatetime" 
,"BUSDTA"."Cash_Master"."UpdatedBy"  
,"BUSDTA"."Cash_Master"."UpdatedDatetime"    

FROM "BUSDTA"."Cash_Master"  WHERE "BUSDTA"."Cash_Master"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cash_Master',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Cash_Master_del"."CashId"

FROM "BUSDTA"."Cash_Master_del"
WHERE "BUSDTA"."Cash_Master_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */

/* Table :Route_Settlement */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Route_Settlement"."SettlementID"
,"BUSDTA"."Route_Settlement"."SettlementNO"
,"BUSDTA"."Route_Settlement"."RouteId"
,"BUSDTA"."Route_Settlement"."Status"
,"BUSDTA"."Route_Settlement"."SettlementDateTime"
,"BUSDTA"."Route_Settlement"."UserId"
,"BUSDTA"."Route_Settlement"."Originator"
,"BUSDTA"."Route_Settlement"."Verifier"
,"BUSDTA"."Route_Settlement"."SettlementAmount"
,"BUSDTA"."Route_Settlement"."ExceptionAmount"
,"BUSDTA"."Route_Settlement"."Comment"
,"BUSDTA"."Route_Settlement"."OriginatingRoute"
,"BUSDTA"."Route_Settlement"."partitioningRoute"
,"BUSDTA"."Route_Settlement"."CreatedBy"
,"BUSDTA"."Route_Settlement"."CreatedDatetime"
,"BUSDTA"."Route_Settlement"."UpdatedBy"
,"BUSDTA"."Route_Settlement"."UpdatedDatetime"

FROM "BUSDTA"."Route_Settlement"
WHERE "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Route_Settlement_del"."SettlementID"
,"BUSDTA"."Route_Settlement_del"."RouteId"

FROM "BUSDTA"."Route_Settlement_del"
WHERE "BUSDTA"."Route_Settlement_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Settlement"
("SettlementID", "SettlementNO", "RouteId", "Status", "SettlementDateTime", "UserId", "Originator", "Verifier", "SettlementAmount", "ExceptionAmount", "Comment", "OriginatingRoute", "partitioningRoute", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."SettlementID"}, {ml r."SettlementNO"}, {ml r."RouteId"}, {ml r."Status"}, {ml r."SettlementDateTime"}, {ml r."UserId"}, {ml r."Originator"}, {ml r."Verifier"}, {ml r."SettlementAmount"}, {ml r."ExceptionAmount"}, {ml r."Comment"}, {ml r."OriginatingRoute"}, {ml r."partitioningRoute"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Settlement"
SET "SettlementNO" = {ml r."SettlementNO"}, "Status" = {ml r."Status"}, "SettlementDateTime" = {ml r."SettlementDateTime"}, "UserId" = {ml r."UserId"}, "Originator" = {ml r."Originator"}, "Verifier" = {ml r."Verifier"}, "SettlementAmount" = {ml r."SettlementAmount"}, "ExceptionAmount" = {ml r."ExceptionAmount"}, "Comment" = {ml r."Comment"}, "OriginatingRoute" = {ml r."OriginatingRoute"}, "partitioningRoute" = {ml r."partitioningRoute"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "SettlementID" = {ml r."SettlementID"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Route_Settlement"
WHERE "SettlementID" = {ml r."SettlementID"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_delete - End */

/* Table :Route_Settlement_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement_Detail',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Route_Settlement_Detail"."SettlementId"
,"BUSDTA"."Route_Settlement_Detail"."SettlementDetailId"
,"BUSDTA"."Route_Settlement_Detail"."UserId"
,"BUSDTA"."Route_Settlement_Detail"."VerificationNum"
,"BUSDTA"."Route_Settlement_Detail"."CashAmount"
,"BUSDTA"."Route_Settlement_Detail"."CheckAmount"
,"BUSDTA"."Route_Settlement_Detail"."MoneyOrderAmount"
,"BUSDTA"."Route_Settlement_Detail"."TotalVerified"
,"BUSDTA"."Route_Settlement_Detail"."Expenses"
,"BUSDTA"."Route_Settlement_Detail"."Payments"
,"BUSDTA"."Route_Settlement_Detail"."OverShortAmount"
,"BUSDTA"."Route_Settlement_Detail"."VerSignature"
,"BUSDTA"."Route_Settlement_Detail"."RouteId"
,"BUSDTA"."Route_Settlement_Detail"."OriginatingRoute"
,"BUSDTA"."Route_Settlement_Detail"."partitioningRoute"
,"BUSDTA"."Route_Settlement_Detail"."CreatedBy"
,"BUSDTA"."Route_Settlement_Detail"."CreatedDatetime"
,"BUSDTA"."Route_Settlement_Detail"."UpdatedBy"
,"BUSDTA"."Route_Settlement_Detail"."UpdatedDatetime"

FROM "BUSDTA"."Route_Settlement_Detail", "BUSDTA"."Route_Settlement"
WHERE "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId"
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement_Detail',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."Route_Settlement_Detail_del"."SettlementId"
,"BUSDTA"."Route_Settlement_Detail_del"."SettlementDetailId"
,"BUSDTA"."Route_Settlement_Detail_del"."RouteId"

FROM "BUSDTA"."Route_Settlement_Detail_del"
WHERE "BUSDTA"."Route_Settlement_Detail_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement_Detail',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Settlement_Detail"
("SettlementId", "SettlementDetailId", "UserId", "VerificationNum", "CashAmount", "CheckAmount", "MoneyOrderAmount", "TotalVerified", "Expenses", "Payments", "OverShortAmount", "VerSignature", "RouteId", "OriginatingRoute", "partitioningRoute", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."SettlementId"}, {ml r."SettlementDetailId"}, {ml r."UserId"}, {ml r."VerificationNum"}, {ml r."CashAmount"}, {ml r."CheckAmount"}, {ml r."MoneyOrderAmount"}, {ml r."TotalVerified"}, {ml r."Expenses"}, {ml r."Payments"}, {ml r."OverShortAmount"}, {ml r."VerSignature"}, {ml r."RouteId"}, {ml r."OriginatingRoute"}, {ml r."partitioningRoute"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement_Detail',
	'upload_update',
	'sql',

'

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Settlement_Detail"
SET "UserId" = {ml r."UserId"}, "VerificationNum" = {ml r."VerificationNum"}, "CashAmount" = {ml r."CashAmount"}, "CheckAmount" = {ml r."CheckAmount"}, "MoneyOrderAmount" = {ml r."MoneyOrderAmount"}, "TotalVerified" = {ml r."TotalVerified"}, "Expenses" = {ml r."Expenses"}, "Payments" = {ml r."Payments"}, "OverShortAmount" = {ml r."OverShortAmount"}, "VerSignature" = {ml r."VerSignature"}, "OriginatingRoute" = {ml r."OriginatingRoute"}, "partitioningRoute" = {ml r."partitioningRoute"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "SettlementId" = {ml r."SettlementId"} AND "SettlementDetailId" = {ml r."SettlementDetailId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Settlement_Detail',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Route_Settlement_Detail"
WHERE "SettlementId" = {ml r."SettlementId"} AND "SettlementDetailId" = {ml r."SettlementDetailId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :Cash_Verification_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cash_Verification_Detail',
	'download_cursor',
	'sql',

'



SELECT "BUSDTA"."Cash_Verification_Detail"."CashVerificationDetailId"  
,"BUSDTA"."Cash_Verification_Detail"."SettlementDetailId"  
,"BUSDTA"."Cash_Verification_Detail"."CashTypeId"  
,"BUSDTA"."Cash_Verification_Detail"."Quantity"  
,"BUSDTA"."Cash_Verification_Detail"."Amount"  
,"BUSDTA"."Cash_Verification_Detail"."RouteId"  
,"BUSDTA"."Cash_Verification_Detail"."CreatedBy"  
,"BUSDTA"."Cash_Verification_Detail"."CreatedDatetime"  
,"BUSDTA"."Cash_Verification_Detail"."UpdatedBy"  
,"BUSDTA"."Cash_Verification_Detail"."UpdatedDatetime"    

FROM "BUSDTA"."Cash_Verification_Detail", "BUSDTA"."Route_Settlement_Detail", "BUSDTA"."Route_Settlement"  
WHERE ("BUSDTA"."Cash_Verification_Detail"."last_modified" >= {ml s.last_table_download} 
or "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download} 
or "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download})  
AND "BUSDTA"."Cash_Verification_Detail"."SettlementDetailId" ="BUSDTA"."Route_Settlement_Detail"."SettlementDetailId" 
AND "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId"  
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cash_Verification_Detail',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Cash_Verification_Detail_del"."CashVerificationDetailId"
,"BUSDTA"."Cash_Verification_Detail_del"."RouteId"

FROM "BUSDTA"."Cash_Verification_Detail_del"
WHERE "BUSDTA"."Cash_Verification_Detail_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cash_Verification_Detail',
	'upload_insert',
	'sql',

'

/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Cash_Verification_Detail"
("CashVerificationDetailId", "SettlementDetailId", "CashTypeId", "Quantity", "Amount", "RouteId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CashVerificationDetailId"}, {ml r."SettlementDetailId"}, {ml r."CashTypeId"}, {ml r."Quantity"}, {ml r."Amount"}, {ml r."RouteId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cash_Verification_Detail',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Cash_Verification_Detail"
SET "SettlementDetailId" = {ml r."SettlementDetailId"}, "CashTypeId" = {ml r."CashTypeId"}, "Quantity" = {ml r."Quantity"}, "Amount" = {ml r."Amount"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CashVerificationDetailId" = {ml r."CashVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cash_Verification_Detail',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Cash_Verification_Detail"
WHERE "CashVerificationDetailId" = {ml r."CashVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_delete - End */

/* Table :MoneyOrder_Verification_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrder_Verification_Detail',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."MoneyOrder_Verification_Detail"."MoneyOrderVerificationDetailId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."SettlementDetailId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."MoneyOrderId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."RouteId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."CreatedBy"
,"BUSDTA"."MoneyOrder_Verification_Detail"."CreatedDatetime"
,"BUSDTA"."MoneyOrder_Verification_Detail"."UpdatedBy"
,"BUSDTA"."MoneyOrder_Verification_Detail"."UpdatedDatetime"

FROM "BUSDTA"."MoneyOrder_Verification_Detail", "BUSDTA"."Route_Settlement_Detail",
    "BUSDTA"."Route_Settlement"
WHERE ("BUSDTA"."MoneyOrder_Verification_Detail"."last_modified" >= {ml s.last_table_download} or "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download} or "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download})
AND "BUSDTA"."MoneyOrder_Verification_Detail"."SettlementDetailId" ="BUSDTA"."Route_Settlement_Detail"."SettlementDetailId"
AND "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId"
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrder_Verification_Detail',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."MoneyOrder_Verification_Detail_del"."MoneyOrderVerificationDetailId"
,"BUSDTA"."MoneyOrder_Verification_Detail_del"."RouteId"

FROM "BUSDTA"."MoneyOrder_Verification_Detail_del"
WHERE "BUSDTA"."MoneyOrder_Verification_Detail_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrder_Verification_Detail',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."MoneyOrder_Verification_Detail"
("MoneyOrderVerificationDetailId", "SettlementDetailId", "MoneyOrderId", "RouteId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."MoneyOrderVerificationDetailId"}, {ml r."SettlementDetailId"}, {ml r."MoneyOrderId"}, {ml r."RouteId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrder_Verification_Detail',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."MoneyOrder_Verification_Detail"
SET "SettlementDetailId" = {ml r."SettlementDetailId"}, "MoneyOrderId" = {ml r."MoneyOrderId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "MoneyOrderVerificationDetailId" = {ml r."MoneyOrderVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrder_Verification_Detail',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."MoneyOrder_Verification_Detail"
WHERE "MoneyOrderVerificationDetailId" = {ml r."MoneyOrderVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :Check_Verification_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Check_Verification_Detail',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."Check_Verification_Detail"."CheckVerificationDetailId"  ,
 "BUSDTA"."Check_Verification_Detail"."SettlementDetailId"  ,
 "BUSDTA"."Check_Verification_Detail"."CheckDetailsId"  ,
 "BUSDTA"."Check_Verification_Detail"."RouteId"  ,
 "BUSDTA"."Check_Verification_Detail"."CreatedBy"  ,
 "BUSDTA"."Check_Verification_Detail"."CreatedDatetime"  ,
 "BUSDTA"."Check_Verification_Detail"."UpdatedBy"  ,
 "BUSDTA"."Check_Verification_Detail"."UpdatedDatetime"   
  FROM "BUSDTA"."Check_Verification_Detail"   ,"BUSDTA"."Route_Settlement_Detail",      
  "BUSDTA"."Route_Settlement" 
   WHERE ("BUSDTA"."Check_Verification_Detail"."last_modified" >= {ml s.last_table_download} 
   or "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download} or 
   "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download})  AND 
   "BUSDTA"."Check_Verification_Detail"."SettlementDetailId" ="BUSDTA"."Route_Settlement_Detail"."SettlementDetailId"  AND 
   "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId" 
    AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Check_Verification_Detail',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Check_Verification_Detail_del"."CheckVerificationDetailId"
,"BUSDTA"."Check_Verification_Detail_del"."RouteId"

FROM "BUSDTA"."Check_Verification_Detail_del"
WHERE "BUSDTA"."Check_Verification_Detail_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Check_Verification_Detail',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Check_Verification_Detail"
("CheckVerificationDetailId", "SettlementDetailId", "CheckDetailsId", "RouteId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CheckVerificationDetailId"}, {ml r."SettlementDetailId"}, {ml r."CheckDetailsId"}, {ml r."RouteId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Check_Verification_Detail',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Check_Verification_Detail"
SET "SettlementDetailId" = {ml r."SettlementDetailId"}, "CheckDetailsId" = {ml r."CheckDetailsId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CheckVerificationDetailId" = {ml r."CheckVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Check_Verification_Detail',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Check_Verification_Detail"
WHERE "CheckVerificationDetailId" = {ml r."CheckVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :ExpenseDetails */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ExpenseDetails',
	'download_cursor',
	'sql',

'

    SELECT "BUSDTA"."ExpenseDetails"."ExpenseId"  ,"BUSDTA"."ExpenseDetails"."RouteId"  ,"BUSDTA"."ExpenseDetails"."TransactionId"  ,
    "BUSDTA"."ExpenseDetails"."CategoryId"  ,"BUSDTA"."ExpenseDetails"."ExpensesExplanation"  ,"BUSDTA"."ExpenseDetails"."ExpenseAmount"  ,
    "BUSDTA"."ExpenseDetails"."StatusId"  ,"BUSDTA"."ExpenseDetails"."ExpensesDatetime"  ,"BUSDTA"."ExpenseDetails"."VoidReasonId"  ,
    "BUSDTA"."ExpenseDetails"."RouteSettlementId"  ,"BUSDTA"."ExpenseDetails"."CreatedBy"  ,"BUSDTA"."ExpenseDetails"."CreatedDatetime"  ,
    "BUSDTA"."ExpenseDetails"."UpdatedBy"  ,"BUSDTA"."ExpenseDetails"."UpdatedDatetime"    
    FROM "BUSDTA"."ExpenseDetails"  
    WHERE "BUSDTA"."ExpenseDetails"."last_modified">= {ml s.last_table_download}  
    AND "BUSDTA"."ExpenseDetails"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})  
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ExpenseDetails',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."ExpenseDetails_del"."ExpenseId"
,"BUSDTA"."ExpenseDetails_del"."RouteId"

FROM "BUSDTA"."ExpenseDetails_del"
WHERE "BUSDTA"."ExpenseDetails_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ExpenseDetails',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."ExpenseDetails"
("ExpenseId", "RouteId", "TransactionId", "CategoryId", "ExpensesExplanation", "ExpenseAmount", "StatusId", "ExpensesDatetime", "VoidReasonId", "RouteSettlementId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ExpenseId"}, {ml r."RouteId"}, {ml r."TransactionId"}, {ml r."CategoryId"}, {ml r."ExpensesExplanation"}, {ml r."ExpenseAmount"}, {ml r."StatusId"}, {ml r."ExpensesDatetime"}, {ml r."VoidReasonId"}, {ml r."RouteSettlementId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ExpenseDetails',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ExpenseDetails"
SET "TransactionId" = {ml r."TransactionId"}, "CategoryId" = {ml r."CategoryId"}, "ExpensesExplanation" = {ml r."ExpensesExplanation"}, "ExpenseAmount" = {ml r."ExpenseAmount"}, "StatusId" = {ml r."StatusId"}, "ExpensesDatetime" = {ml r."ExpensesDatetime"}, "VoidReasonId" = {ml r."VoidReasonId"}, "RouteSettlementId" = {ml r."RouteSettlementId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ExpenseId" = {ml r."ExpenseId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'ExpenseDetails',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."ExpenseDetails"
WHERE "ExpenseId" = {ml r."ExpenseId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :MoneyOrderDetails */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrderDetails',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."MoneyOrderDetails"."MoneyOrderId"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderNumber"
,"BUSDTA"."MoneyOrderDetails"."RouteId"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderAmount"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderFeeAmount"
,"BUSDTA"."MoneyOrderDetails"."StatusId"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderDatetime"
,"BUSDTA"."MoneyOrderDetails"."VoidReasonId"
,"BUSDTA"."MoneyOrderDetails"."RouteSettlementId"
,"BUSDTA"."MoneyOrderDetails"."CreatedBy"
,"BUSDTA"."MoneyOrderDetails"."CreatedDatetime"
,"BUSDTA"."MoneyOrderDetails"."UpdatedBy"
,"BUSDTA"."MoneyOrderDetails"."UpdatedDatetime"

FROM "BUSDTA"."MoneyOrderDetails"
WHERE "BUSDTA"."MoneyOrderDetails"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."MoneyOrderDetails"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrderDetails',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."MoneyOrderDetails_del"."MoneyOrderId"
,"BUSDTA"."MoneyOrderDetails_del"."RouteId"

FROM "BUSDTA"."MoneyOrderDetails_del"
WHERE "BUSDTA"."MoneyOrderDetails_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrderDetails',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."MoneyOrderDetails"
("MoneyOrderId", "MoneyOrderNumber", "RouteId", "MoneyOrderAmount", "MoneyOrderFeeAmount", "StatusId", "MoneyOrderDatetime", "VoidReasonId", "RouteSettlementId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."MoneyOrderId"}, {ml r."MoneyOrderNumber"}, {ml r."RouteId"}, {ml r."MoneyOrderAmount"}, {ml r."MoneyOrderFeeAmount"}, {ml r."StatusId"}, {ml r."MoneyOrderDatetime"}, {ml r."VoidReasonId"}, {ml r."RouteSettlementId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrderDetails',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."MoneyOrderDetails"
SET "MoneyOrderNumber" = {ml r."MoneyOrderNumber"}, "MoneyOrderAmount" = {ml r."MoneyOrderAmount"}, "MoneyOrderFeeAmount" = {ml r."MoneyOrderFeeAmount"}, "StatusId" = {ml r."StatusId"}, "MoneyOrderDatetime" = {ml r."MoneyOrderDatetime"}, "VoidReasonId" = {ml r."VoidReasonId"}, "RouteSettlementId" = {ml r."RouteSettlementId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "MoneyOrderId" = {ml r."MoneyOrderId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MoneyOrderDetails',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."MoneyOrderDetails"
WHERE "MoneyOrderId" = {ml r."MoneyOrderId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :CheckDetails */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CheckDetails',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."CheckDetails"."CheckDetailsId"  ,"BUSDTA"."CheckDetails"."CheckDetailsNumber"  ,"BUSDTA"."CheckDetails"."RouteId"  ,
"BUSDTA"."CheckDetails"."CheckNumber"  ,"BUSDTA"."CheckDetails"."CustomerId"  ,"BUSDTA"."CheckDetails"."CustomerName"  ,
"BUSDTA"."CheckDetails"."CheckAmount"  ,"BUSDTA"."CheckDetails"."CheckDate"  ,"BUSDTA"."CheckDetails"."CreatedBy"  ,
"BUSDTA"."CheckDetails"."CreatedDatetime"  ,"BUSDTA"."CheckDetails"."UpdatedBy"  ,"BUSDTA"."CheckDetails"."UpdatedDatetime"    

FROM "BUSDTA"."CheckDetails"  
WHERE "BUSDTA"."CheckDetails"."last_modified" >= {ml s.last_table_download}  
AND "BUSDTA"."CheckDetails"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CheckDetails',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."CheckDetails_del"."CheckDetailsId"
,"BUSDTA"."CheckDetails_del"."RouteId"

FROM "BUSDTA"."CheckDetails_del"
WHERE "BUSDTA"."CheckDetails_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CheckDetails',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."CheckDetails"
("CheckDetailsId", "CheckDetailsNumber", "RouteId", "CheckNumber", "CustomerId", "CustomerName", "CheckAmount", "CheckDate", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CheckDetailsId"}, {ml r."CheckDetailsNumber"}, {ml r."RouteId"}, {ml r."CheckNumber"}, {ml r."CustomerId"}, {ml r."CustomerName"}, {ml r."CheckAmount"}, {ml r."CheckDate"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CheckDetails',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."CheckDetails"
SET "CheckDetailsNumber" = {ml r."CheckDetailsNumber"}, "CheckNumber" = {ml r."CheckNumber"}, "CustomerId" = {ml r."CustomerId"}, "CustomerName" = {ml r."CustomerName"}, "CheckAmount" = {ml r."CheckAmount"}, "CheckDate" = {ml r."CheckDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CheckDetailsId" = {ml r."CheckDetailsId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CheckDetails',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."CheckDetails"
WHERE "CheckDetailsId" = {ml r."CheckDetailsId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :Vehicle_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Vehicle_Master',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Vehicle_Master"."VehicleID"
,"BUSDTA"."Vehicle_Master"."VINNumber"
,"BUSDTA"."Vehicle_Master"."VehicleNumber"
,"BUSDTA"."Vehicle_Master"."VehicleMake"
,"BUSDTA"."Vehicle_Master"."VehicleColour"
,"BUSDTA"."Vehicle_Master"."VehicleAxle"
,"BUSDTA"."Vehicle_Master"."VehicleFuel"
,"BUSDTA"."Vehicle_Master"."VehicleMilage"
,"BUSDTA"."Vehicle_Master"."Isactive"
,"BUSDTA"."Vehicle_Master"."VehicleManufacturingDt"
,"BUSDTA"."Vehicle_Master"."VehicleExpiryDt"
,"BUSDTA"."Vehicle_Master"."VehicleOwner"
,"BUSDTA"."Vehicle_Master"."CreatedBy"
,"BUSDTA"."Vehicle_Master"."CreatedDatetime"
,"BUSDTA"."Vehicle_Master"."UpdatedBy"
,"BUSDTA"."Vehicle_Master"."UpdatedDatetime"

FROM "BUSDTA"."Vehicle_Master"
WHERE "BUSDTA"."Vehicle_Master"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Vehicle_Master',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Vehicle_Master_del"."VehicleID"
,"BUSDTA"."Vehicle_Master_del"."VINNumber"

FROM "BUSDTA"."Vehicle_Master_del"
WHERE "BUSDTA"."Vehicle_Master_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :Status_Type */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Status_Type',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Status_Type"."StatusTypeID"
,"BUSDTA"."Status_Type"."StatusTypeCD"
,"BUSDTA"."Status_Type"."StatusTypeDESC"
,"BUSDTA"."Status_Type"."CreatedBy"
,"BUSDTA"."Status_Type"."CreatedDatetime"
,"BUSDTA"."Status_Type"."UpdatedBy"
,"BUSDTA"."Status_Type"."UpdatedDatetime"

FROM "BUSDTA"."Status_Type"
WHERE "BUSDTA"."Status_Type"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Status_Type',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Status_Type_del"."StatusTypeCD"

FROM "BUSDTA"."Status_Type_del"
WHERE "BUSDTA"."Status_Type_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */

/* Table :Route_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Master',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Route_Master"."RouteMasterID"
,"BUSDTA"."Route_Master"."RouteName"
,"BUSDTA"."Route_Master"."RouteDescription"
,"BUSDTA"."Route_Master"."RouteAdressBookNumber"
,"BUSDTA"."Route_Master"."BranchNumber"
,"BUSDTA"."Route_Master"."BranchAdressBookNumber"
,"BUSDTA"."Route_Master"."DefaultUser"
,"BUSDTA"."Route_Master"."VehicleID"
,"BUSDTA"."Route_Master"."RepnlBranch"
,"BUSDTA"."Route_Master"."RepnlBranchType"
,"BUSDTA"."Route_Master"."CreatedBy"
,"BUSDTA"."Route_Master"."CreatedDatetime"
,"BUSDTA"."Route_Master"."UpdatedBy"
,"BUSDTA"."Route_Master"."UpdatedDatetime"
,"BUSDTA"."Route_Master"."RouteStatusID"

FROM "BUSDTA"."Route_Master"
WHERE "BUSDTA"."Route_Master"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Master',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Route_Master_del"."RouteMasterID"
,"BUSDTA"."Route_Master_del"."RouteName"

FROM "BUSDTA"."Route_Master_del"
WHERE "BUSDTA"."Route_Master_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */

/* Table :RouteStatus */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'RouteStatus',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :Entity_Range_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Range_Master',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."Entity_Range_Master"."EntityRangeMasterID"  ,"BUSDTA"."Entity_Range_Master"."EntityBucketMasterId"  ,
     "BUSDTA"."Entity_Range_Master"."RangeStart"  ,"BUSDTA"."Entity_Range_Master"."RangeEnd"  ,
     "BUSDTA"."Entity_Range_Master"."LastAlloted"  ,"BUSDTA"."Entity_Range_Master"."Size"  ,"BUSDTA"."Entity_Range_Master"."Source"  ,
     "BUSDTA"."Entity_Range_Master"."Active"  ,"BUSDTA"."Entity_Range_Master"."CreatedBy"  ,"BUSDTA"."Entity_Range_Master"."CreatedDatetime"  ,
     "BUSDTA"."Entity_Range_Master"."UpdatedBy"  ,"BUSDTA"."Entity_Range_Master"."UpdatedDatetime"    FROM "BUSDTA"."Entity_Range_Master"  
     WHERE "BUSDTA"."Entity_Range_Master"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Range_Master',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Entity_Range_Master_del"."EntityRangeMasterID"

FROM "BUSDTA"."Entity_Range_Master_del"
WHERE "BUSDTA"."Entity_Range_Master_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :Entity_Number_Status */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Number_Status',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Entity_Number_Status"."EntityNumberStatusID"  
,"BUSDTA"."Entity_Number_Status"."RouteId"  
,"BUSDTA"."Entity_Number_Status"."Bucket"  
,"BUSDTA"."Entity_Number_Status"."EntityBucketMasterId"  
,"BUSDTA"."Entity_Number_Status"."RangeStart"  
,"BUSDTA"."Entity_Number_Status"."RangeEnd"  
,"BUSDTA"."Entity_Number_Status"."CurrentAllocated"  
,"BUSDTA"."Entity_Number_Status"."IsConsumed"  
,"BUSDTA"."Entity_Number_Status"."ConsumedOnDate"  
,"BUSDTA"."Entity_Number_Status"."CreatedBy"  
,"BUSDTA"."Entity_Number_Status"."CreatedDatetime"  
,"BUSDTA"."Entity_Number_Status"."UpdatedBy"  
,"BUSDTA"."Entity_Number_Status"."UpdatedDatetime"    

FROM "BUSDTA"."Entity_Number_Status"  WHERE "BUSDTA"."Entity_Number_Status"."last_modified" >= {ml s.last_table_download}  
AND "BUSDTA"."Entity_Number_Status"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Number_Status',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Entity_Number_Status_del"."EntityNumberStatusID"

FROM "BUSDTA"."Entity_Number_Status_del"
WHERE "BUSDTA"."Entity_Number_Status_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Number_Status',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Entity_Number_Status"
("EntityNumberStatusID", "RouteId", "Bucket", "EntityBucketMasterId", "RangeStart", "RangeEnd", "CurrentAllocated", "IsConsumed", "ConsumedOnDate", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."EntityNumberStatusID"}, {ml r."RouteId"}, {ml r."Bucket"}, {ml r."EntityBucketMasterId"}, {ml r."RangeStart"}, {ml r."RangeEnd"}, {ml r."CurrentAllocated"}, {ml r."IsConsumed"}, {ml r."ConsumedOnDate"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Number_Status',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Entity_Number_Status"
SET "RouteId" = {ml r."RouteId"}, "Bucket" = {ml r."Bucket"}, "EntityBucketMasterId" = {ml r."EntityBucketMasterId"}, "RangeStart" = {ml r."RangeStart"}, "RangeEnd" = {ml r."RangeEnd"}, "CurrentAllocated" = {ml r."CurrentAllocated"}, "IsConsumed" = {ml r."IsConsumed"}, "ConsumedOnDate" = {ml r."ConsumedOnDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "EntityNumberStatusID" = {ml r."EntityNumberStatusID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Number_Status',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Entity_Number_Status"
WHERE "EntityNumberStatusID" = {ml r."EntityNumberStatusID"}
'

/* upload_delete - End */

/* Table :Entity_Bucket_Master */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Bucket_Master',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."Entity_Bucket_Master"."EntityBucketMasterId"  ,"BUSDTA"."Entity_Bucket_Master"."Entity"  ,
     "BUSDTA"."Entity_Bucket_Master"."BucketSize"  ,"BUSDTA"."Entity_Bucket_Master"."NoOfBuckets"  ,
     "BUSDTA"."Entity_Bucket_Master"."CreatedBy"  ,"BUSDTA"."Entity_Bucket_Master"."CreatedDatetime"  ,
     "BUSDTA"."Entity_Bucket_Master"."UpdatedBy"  ,"BUSDTA"."Entity_Bucket_Master"."UpdatedDatetime"    
     FROM "BUSDTA"."Entity_Bucket_Master"  WHERE "BUSDTA"."Entity_Bucket_Master"."last_modified">= {ml s.last_table_download}     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Entity_Bucket_Master',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Entity_Bucket_Master_del"."EntityBucketMasterId"

FROM "BUSDTA"."Entity_Bucket_Master_del"
WHERE "BUSDTA"."Entity_Bucket_Master_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :Invoice_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Invoice_Header',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."Invoice_Header"."InvoiceID"  ,"BUSDTA"."Invoice_Header"."CustShipToId"  ,"BUSDTA"."Invoice_Header"."RouteId"  ,
     "BUSDTA"."Invoice_Header"."OrderId"  ,"BUSDTA"."Invoice_Header"."ARInvoiceNumber"  ,"BUSDTA"."Invoice_Header"."DeviceInvoiceNumber"  ,
     "BUSDTA"."Invoice_Header"."ARInvoiceDate"  ,"BUSDTA"."Invoice_Header"."DeviceInvoiceDate"  ,"BUSDTA"."Invoice_Header"."DeviceStatusID"  ,
     "BUSDTA"."Invoice_Header"."ARStatusID"  ,"BUSDTA"."Invoice_Header"."ReasonCodeId"  ,"BUSDTA"."Invoice_Header"."DueDate"  ,
     "BUSDTA"."Invoice_Header"."InvoicePaymentType"  ,"BUSDTA"."Invoice_Header"."GrossAmt"  ,"BUSDTA"."Invoice_Header"."CreatedBy"  ,
     "BUSDTA"."Invoice_Header"."CreatedDatetime"  ,"BUSDTA"."Invoice_Header"."UpdatedBy"  ,"BUSDTA"."Invoice_Header"."UpdatedDatetime" ,"BUSDTA"."Invoice_Header"."CustBillToId"    
     FROM "BUSDTA"."Invoice_Header"  
     WHERE "BUSDTA"."Invoice_Header"."last_modified">= {ml s.last_table_download}   
     AND "BUSDTA"."Invoice_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})    
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Invoice_Header',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Invoice_Header_del"."InvoiceID"
,"BUSDTA"."Invoice_Header_del"."RouteId"

FROM "BUSDTA"."Invoice_Header_del"
WHERE "BUSDTA"."Invoice_Header_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Invoice_Header',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Invoice_Header"
("InvoiceID", "CustShipToId", "RouteId", "OrderId", "ARInvoiceNumber", "DeviceInvoiceNumber", "ARInvoiceDate", "DeviceInvoiceDate", "DeviceStatusID", "ARStatusID", "ReasonCodeId", "DueDate", "InvoicePaymentType", "GrossAmt", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime", "CustBillToId")
VALUES({ml r."InvoiceID"}, {ml r."CustShipToId"}, {ml r."RouteId"}, {ml r."OrderId"}, {ml r."ARInvoiceNumber"}, {ml r."DeviceInvoiceNumber"}, {ml r."ARInvoiceDate"}, {ml r."DeviceInvoiceDate"}, {ml r."DeviceStatusID"}, {ml r."ARStatusID"}, {ml r."ReasonCodeId"}, {ml r."DueDate"}, {ml r."InvoicePaymentType"}, {ml r."GrossAmt"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"},{ml r."CustBillToId"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Invoice_Header',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Invoice_Header"
SET "CustShipToId" = {ml r."CustShipToId"}, "OrderId" = {ml r."OrderId"}, "ARInvoiceNumber" = {ml r."ARInvoiceNumber"}, "DeviceInvoiceNumber" = {ml r."DeviceInvoiceNumber"}, "ARInvoiceDate" = {ml r."ARInvoiceDate"}, "DeviceInvoiceDate" = {ml r."DeviceInvoiceDate"}, "DeviceStatusID" = {ml r."DeviceStatusID"}, "ARStatusID" = {ml r."ARStatusID"}, "ReasonCodeId" = {ml r."ReasonCodeId"}, "DueDate" = {ml r."DueDate"}, "InvoicePaymentType" = {ml r."InvoicePaymentType"}, "GrossAmt" = {ml r."GrossAmt"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}, "CustBillToId" = {ml r."CustBillToId"}
WHERE "InvoiceID" = {ml r."InvoiceID"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Invoice_Header',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Invoice_Header"
WHERE "InvoiceID" = {ml r."InvoiceID"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :Receipt_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Receipt_Header',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Receipt_Header"."CustShipToId"  
,"BUSDTA"."Receipt_Header"."RouteId"  
,"BUSDTA"."Receipt_Header"."ReceiptID"  
,"BUSDTA"."Receipt_Header"."ReceiptNumber"  
,"BUSDTA"."Receipt_Header"."ReceiptDate"  
,"BUSDTA"."Receipt_Header"."TransactionAmount"  
,"BUSDTA"."Receipt_Header"."PaymentMode"  
,"BUSDTA"."Receipt_Header"."ReasonCodeId"  
,"BUSDTA"."Receipt_Header"."CreditReasonCodeId"  
,"BUSDTA"."Receipt_Header"."CreditMemoNote"  
,"BUSDTA"."Receipt_Header"."TransactionMode"  
,"BUSDTA"."Receipt_Header"."ChequeNum"  
,"BUSDTA"."Receipt_Header"."ChequeDate"  
,"BUSDTA"."Receipt_Header"."StatusId"  
,"BUSDTA"."Receipt_Header"."JDEStatusId"  
,"BUSDTA"."Receipt_Header"."JDEReferenceId"  
,"BUSDTA"."Receipt_Header"."SettelmentId"  
,"BUSDTA"."Receipt_Header"."AuthoritySignature"  
,"BUSDTA"."Receipt_Header"."CustomerSignature"  
,"BUSDTA"."Receipt_Header"."CreatedBy"  
,"BUSDTA"."Receipt_Header"."CreatedDatetime"  
,"BUSDTA"."Receipt_Header"."UpdatedBy"  
,"BUSDTA"."Receipt_Header"."UpdatedDatetime"   
,"BUSDTA"."Receipt_Header"."CustBillToId"  
FROM "BUSDTA"."Receipt_Header"  
WHERE "BUSDTA"."Receipt_Header"."last_modified">= {ml s.last_table_download}   
and "BUSDTA"."Receipt_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Receipt_Header',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Receipt_Header_del"."CustShipToId"
,"BUSDTA"."Receipt_Header_del"."RouteId"
,"BUSDTA"."Receipt_Header_del"."ReceiptID"

FROM "BUSDTA"."Receipt_Header_del"
WHERE "BUSDTA"."Receipt_Header_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Receipt_Header',
	'upload_insert',
	'sql',

'


/* Insert the row into the consolidated database. */  
INSERT INTO "BUSDTA"."Receipt_Header"  ("CustShipToId", "RouteId", "ReceiptID", "ReceiptNumber", "ReceiptDate", "TransactionAmount", "PaymentMode", "ReasonCodeId", "CreditReasonCodeId", "CreditMemoNote", "TransactionMode", "ChequeNum", "ChequeDate", "StatusId", "JDEStatusId", "JDEReferenceId", "SettelmentId", "AuthoritySignature", "CustomerSignature", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime","CustBillToId")  
VALUES({ml r."CustShipToId"}, {ml r."RouteId"}, {ml r."ReceiptID"}, {ml r."ReceiptNumber"}, {ml r."ReceiptDate"}, {ml r."TransactionAmount"}, {ml r."PaymentMode"}, {ml r."ReasonCodeId"}, {ml r."CreditReasonCodeId"}, {ml r."CreditMemoNote"}, {ml r."TransactionMode"}, {ml r."ChequeNum"}, {ml r."ChequeDate"}, {ml r."StatusId"}, {ml r."JDEStatusId"}, {ml r."JDEReferenceId"}, {ml r."SettelmentId"}, {ml r."AuthoritySignature"}, {ml r."CustomerSignature"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"}, {ml r."CustBillToId"})       
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Receipt_Header',
	'upload_update',
	'sql',

'
 

/* Update the row in the consolidated database. */  
UPDATE "BUSDTA"."Receipt_Header"  SET "ReceiptNumber" = {ml r."ReceiptNumber"}, "ReceiptDate" = {ml r."ReceiptDate"}, "TransactionAmount" = {ml r."TransactionAmount"}, "PaymentMode" = {ml r."PaymentMode"}, "ReasonCodeId" = {ml r."ReasonCodeId"}, "CreditReasonCodeId" = {ml r."CreditReasonCodeId"}, "CreditMemoNote" = {ml r."CreditMemoNote"}, "TransactionMode" = {ml r."TransactionMode"}, "ChequeNum" = {ml r."ChequeNum"}, "ChequeDate" = {ml r."ChequeDate"}, "StatusId" = {ml r."StatusId"}, "JDEStatusId" = {ml r."JDEStatusId"}, "JDEReferenceId" = {ml r."JDEReferenceId"}, "SettelmentId" = {ml r."SettelmentId"}, "AuthoritySignature" = {ml r."AuthoritySignature"}, "CustomerSignature" = {ml r."CustomerSignature"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}, "CustBillToId" = {ml r."CustBillToId"}  WHERE "CustShipToId" = {ml r."CustShipToId"} AND "RouteId" = {ml r."RouteId"} AND "ReceiptID" = {ml r."ReceiptID"}       
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Receipt_Header',
	'upload_delete',
	'sql',

'
 

/* Delete the row in the consolidated database. */  
DELETE FROM "BUSDTA"."Receipt_Header"  
WHERE "CustShipToId" = {ml r."CustShipToId"} AND "RouteId" = {ml r."RouteId"} AND "ReceiptID" = {ml r."ReceiptID"}       
'

/* upload_delete - End */

/* Table :Customer_Ledger */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Ledger',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Customer_Ledger"."CustomerLedgerId"  ,
"BUSDTA"."Customer_Ledger"."ReceiptID"  ,"BUSDTA"."Customer_Ledger"."InvoiceId"  ,"BUSDTA"."Customer_Ledger"."RouteId"  ,
"BUSDTA"."Customer_Ledger"."CustShipToId"  ,"BUSDTA"."Customer_Ledger"."IsActive"  ,"BUSDTA"."Customer_Ledger"."InvoiceGrossAmt"  ,
"BUSDTA"."Customer_Ledger"."InvoiceOpenAmt"  ,"BUSDTA"."Customer_Ledger"."ReceiptUnAppliedAmt"  ,
"BUSDTA"."Customer_Ledger"."ReceiptAppliedAmt"  ,"BUSDTA"."Customer_Ledger"."ConcessionAmt"  ,
"BUSDTA"."Customer_Ledger"."ConcessionCodeId"  ,"BUSDTA"."Customer_Ledger"."DateApplied"  ,"BUSDTA"."Customer_Ledger"."CreatedBy"  ,
"BUSDTA"."Customer_Ledger"."CreatedDatetime"  ,"BUSDTA"."Customer_Ledger"."UpdatedBy"  ,"BUSDTA"."Customer_Ledger"."UpdatedDatetime" ,"BUSDTA"."Customer_Ledger"."CustBillToId"
FROM "BUSDTA"."Customer_Ledger"  
WHERE "BUSDTA"."Customer_Ledger"."last_modified">= {ml s.last_table_download}   
AND "BUSDTA"."Customer_Ledger"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Ledger',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Customer_Ledger_del"."CustomerLedgerId"
,"BUSDTA"."Customer_Ledger_del"."RouteId"

FROM "BUSDTA"."Customer_Ledger_del"
WHERE "BUSDTA"."Customer_Ledger_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Ledger',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Customer_Ledger"
("CustomerLedgerId", "ReceiptID", "InvoiceId", "RouteId", "CustShipToId", "IsActive", "InvoiceGrossAmt", "InvoiceOpenAmt", "ReceiptUnAppliedAmt", "ReceiptAppliedAmt", "ConcessionAmt", "ConcessionCodeId", "DateApplied", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime","CustBillToId")
VALUES({ml r."CustomerLedgerId"}, {ml r."ReceiptID"}, {ml r."InvoiceId"}, {ml r."RouteId"}, {ml r."CustShipToId"}, {ml r."IsActive"}, {ml r."InvoiceGrossAmt"}, {ml r."InvoiceOpenAmt"}, {ml r."ReceiptUnAppliedAmt"}, {ml r."ReceiptAppliedAmt"}, {ml r."ConcessionAmt"}, {ml r."ConcessionCodeId"}, {ml r."DateApplied"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"},{ml r."CustBillToId"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Ledger',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Customer_Ledger"
SET "ReceiptID" = {ml r."ReceiptID"}, "InvoiceId" = {ml r."InvoiceId"}, "CustShipToId" = {ml r."CustShipToId"}, "IsActive" = {ml r."IsActive"}, "InvoiceGrossAmt" = {ml r."InvoiceGrossAmt"}, "InvoiceOpenAmt" = {ml r."InvoiceOpenAmt"}, "ReceiptUnAppliedAmt" = {ml r."ReceiptUnAppliedAmt"}, "ReceiptAppliedAmt" = {ml r."ReceiptAppliedAmt"}, "ConcessionAmt" = {ml r."ConcessionAmt"}, "ConcessionCodeId" = {ml r."ConcessionCodeId"}, "DateApplied" = {ml r."DateApplied"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"} ,"CustBillToId" = {ml r."CustBillToId"}
WHERE "CustomerLedgerId" = {ml r."CustomerLedgerId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Customer_Ledger',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Customer_Ledger"
WHERE "CustomerLedgerId" = {ml r."CustomerLedgerId"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :Cycle_Count_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Header',
	'download_cursor',
	'sql',

'

     SELECT "BUSDTA"."Cycle_Count_Header"."CycleCountID"  ,"BUSDTA"."Cycle_Count_Header"."RouteId"  ,
     "BUSDTA"."Cycle_Count_Header"."StatusId"  ,"BUSDTA"."Cycle_Count_Header"."IsCountInitiated"  ,
     "BUSDTA"."Cycle_Count_Header"."CycleCountTypeId"  ,"BUSDTA"."Cycle_Count_Header"."InitiatorId"  ,
     "BUSDTA"."Cycle_Count_Header"."CycleCountDatetime"  ,"BUSDTA"."Cycle_Count_Header"."ReasonCodeId"  ,
     "BUSDTA"."Cycle_Count_Header"."JDEQty"  ,"BUSDTA"."Cycle_Count_Header"."JDEUM"  ,"BUSDTA"."Cycle_Count_Header"."JDECycleCountNum"  ,
     "BUSDTA"."Cycle_Count_Header"."JDECycleCountDocType"  ,"BUSDTA"."Cycle_Count_Header"."CreatedBy"  ,
     "BUSDTA"."Cycle_Count_Header"."CreatedDatetime"  ,"BUSDTA"."Cycle_Count_Header"."UpdatedBy"  ,
     "BUSDTA"."Cycle_Count_Header"."UpdatedDatetime"    
     FROM "BUSDTA"."Cycle_Count_Header"  
     WHERE "BUSDTA"."Cycle_Count_Header"."last_modified">= {ml s.last_table_download}  
      AND "BUSDTA"."Cycle_Count_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})    
      AND "BUSDTA"."Cycle_Count_Header"."StatusId" <> (select StatusTypeID from busdta.status_type where StatusTypeCD = ''NEW'')  
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Header',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Cycle_Count_Header_del"."CycleCountID"
,"BUSDTA"."Cycle_Count_Header_del"."RouteId"

FROM "BUSDTA"."Cycle_Count_Header_del"
WHERE "BUSDTA"."Cycle_Count_Header_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Header',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Cycle_Count_Header"
("CycleCountID", "RouteId", "StatusId", "IsCountInitiated", "CycleCountTypeId", "InitiatorId", "CycleCountDatetime", "ReasonCodeId", "JDEQty", "JDEUM", "JDECycleCountNum", "JDECycleCountDocType", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CycleCountID"}, {ml r."RouteId"}, {ml r."StatusId"}, {ml r."IsCountInitiated"}, {ml r."CycleCountTypeId"}, {ml r."InitiatorId"}, {ml r."CycleCountDatetime"}, {ml r."ReasonCodeId"}, {ml r."JDEQty"}, {ml r."JDEUM"}, {ml r."JDECycleCountNum"}, {ml r."JDECycleCountDocType"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Header',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Cycle_Count_Header"
SET "StatusId" = {ml r."StatusId"}, "IsCountInitiated" = {ml r."IsCountInitiated"}, "CycleCountTypeId" = {ml r."CycleCountTypeId"}, "InitiatorId" = {ml r."InitiatorId"}, "CycleCountDatetime" = {ml r."CycleCountDatetime"}, "ReasonCodeId" = {ml r."ReasonCodeId"}, "JDEQty" = {ml r."JDEQty"}, "JDEUM" = {ml r."JDEUM"}, "JDECycleCountNum" = {ml r."JDECycleCountNum"}, "JDECycleCountDocType" = {ml r."JDECycleCountDocType"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CycleCountID" = {ml r."CycleCountID"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Header',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Cycle_Count_Header"
WHERE "CycleCountID" = {ml r."CycleCountID"} AND "RouteId" = {ml r."RouteId"}
 

'

/* upload_delete - End */

/* Table :Cycle_Count_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Detail',
	'download_cursor',
	'sql',

'

 SELECT "BUSDTA"."Cycle_Count_Detail"."CycleCountID"  ,"BUSDTA"."Cycle_Count_Detail"."CycleCountDetailID"  ,
 "BUSDTA"."Cycle_Count_Detail"."ReCountNumber"  ,"BUSDTA"."Cycle_Count_Detail"."ItemId"  ,
 "BUSDTA"."Cycle_Count_Detail"."CountedQty"  ,"BUSDTA"."Cycle_Count_Detail"."ItemStatus"  ,
 "BUSDTA"."Cycle_Count_Detail"."OnHandQty"  ,"BUSDTA"."Cycle_Count_Detail"."HeldQty"  ,
 "BUSDTA"."Cycle_Count_Detail"."CountAccepted"  ,"BUSDTA"."Cycle_Count_Detail"."CreatedBy"  ,
 "BUSDTA"."Cycle_Count_Detail"."CreatedDatetime"  ,"BUSDTA"."Cycle_Count_Detail"."UpdatedBy"  ,
 "BUSDTA"."Cycle_Count_Detail"."UpdatedDatetime"    FROM "BUSDTA"."Cycle_Count_Detail", "BUSDTA"."Cycle_Count_Header"  
 WHERE ("BUSDTA"."Cycle_Count_Detail"."last_modified">= {ml s.last_table_download} or 
 "BUSDTA"."Cycle_Count_Header"."last_modified">= {ml s.last_table_download})  AND 
  "BUSDTA"."Cycle_Count_Detail"."CycleCountID" = "BUSDTA"."Cycle_Count_Header"."CycleCountID"  AND 
  "BUSDTA"."Cycle_Count_Header"."RouteId"  = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Detail',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Cycle_Count_Detail_del"."CycleCountID"
,"BUSDTA"."Cycle_Count_Detail_del"."CycleCountDetailID"
,"BUSDTA"."Cycle_Count_Detail_del"."ReCountNumber"
,"BUSDTA"."Cycle_Count_Detail_del"."ItemId"

FROM "BUSDTA"."Cycle_Count_Detail_del"
WHERE "BUSDTA"."Cycle_Count_Detail_del"."last_modified">= {ml s.last_table_download}
 

'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Detail',
	'upload_insert',
	'sql',

'

 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Cycle_Count_Detail"
("CycleCountID", "CycleCountDetailID", "ReCountNumber", "ItemId", "CountedQty", "ItemStatus", "OnHandQty", "HeldQty", "CountAccepted", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CycleCountID"}, {ml r."CycleCountDetailID"}, {ml r."ReCountNumber"}, {ml r."ItemId"}, {ml r."CountedQty"}, {ml r."ItemStatus"}, {ml r."OnHandQty"}, {ml r."HeldQty"}, {ml r."CountAccepted"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Detail',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Cycle_Count_Detail"
SET "CountedQty" = {ml r."CountedQty"}, "ItemStatus" = {ml r."ItemStatus"}, "OnHandQty" = {ml r."OnHandQty"}, "HeldQty" = {ml r."HeldQty"}, "CountAccepted" = {ml r."CountAccepted"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CycleCountID" = {ml r."CycleCountID"} AND "CycleCountDetailID" = {ml r."CycleCountDetailID"} AND "ReCountNumber" = {ml r."ReCountNumber"} AND "ItemId" = {ml r."ItemId"}
 

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Cycle_Count_Detail',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Cycle_Count_Detail"
WHERE "CycleCountID" = {ml r."CycleCountID"} AND "CycleCountDetailID" = {ml r."CycleCountDetailID"} AND "ReCountNumber" = {ml r."ReCountNumber"} AND "ItemId" = {ml r."ItemId"}
 

'

/* upload_delete - End */

/* Table :Pick_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Pick_Detail',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Pick_Detail"."PickDetailID"
,"BUSDTA"."Pick_Detail"."RouteId"
,"BUSDTA"."Pick_Detail"."TransactionID"
,"BUSDTA"."Pick_Detail"."TransactionDetailID"
,"BUSDTA"."Pick_Detail"."TransactionTypeId"
,"BUSDTA"."Pick_Detail"."TransactionStatusId"
,"BUSDTA"."Pick_Detail"."ItemID"
,"BUSDTA"."Pick_Detail"."TransactionQty"
,"BUSDTA"."Pick_Detail"."TransactionUOM"
,"BUSDTA"."Pick_Detail"."TransactionQtyPrimaryUOM"
,"BUSDTA"."Pick_Detail"."PrimaryUOM"
,"BUSDTA"."Pick_Detail"."PickedQty"
,"BUSDTA"."Pick_Detail"."PickQtyPrimaryUOM"
,"BUSDTA"."Pick_Detail"."LastScanMode"
,"BUSDTA"."Pick_Detail"."ItemScanSeq"
,"BUSDTA"."Pick_Detail"."IsOnHold"
,"BUSDTA"."Pick_Detail"."PickAdjusted"
,"BUSDTA"."Pick_Detail"."AdjustedQty"
,"BUSDTA"."Pick_Detail"."ReasonCodeId"
,"BUSDTA"."Pick_Detail"."ManuallyPickCount"
,"BUSDTA"."Pick_Detail"."IsInException"
,"BUSDTA"."Pick_Detail"."ExceptionReasonCodeId"
,"BUSDTA"."Pick_Detail"."CreatedBy"
,"BUSDTA"."Pick_Detail"."CreatedDatetime"
,"BUSDTA"."Pick_Detail"."UpdatedBy"
,"BUSDTA"."Pick_Detail"."UpdatedDatetime"

FROM "BUSDTA"."Pick_Detail"
WHERE "BUSDTA"."Pick_Detail"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Pick_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Pick_Detail',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Pick_Detail_del"."PickDetailID"
,"BUSDTA"."Pick_Detail_del"."RouteId"

FROM "BUSDTA"."Pick_Detail_del"
WHERE "BUSDTA"."Pick_Detail_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Pick_Detail',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Pick_Detail"
("PickDetailID", "RouteId", "TransactionID", "TransactionDetailID", "TransactionTypeId", "TransactionStatusId", "ItemID", "TransactionQty", "TransactionUOM", "TransactionQtyPrimaryUOM", "PrimaryUOM", "PickedQty", "PickQtyPrimaryUOM", "LastScanMode", "ItemScanSeq", "IsOnHold", "PickAdjusted", "AdjustedQty", "ReasonCodeId", "ManuallyPickCount", "IsInException", "ExceptionReasonCodeId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."PickDetailID"}, {ml r."RouteId"}, {ml r."TransactionID"}, {ml r."TransactionDetailID"}, {ml r."TransactionTypeId"}, {ml r."TransactionStatusId"}, {ml r."ItemID"}, {ml r."TransactionQty"}, {ml r."TransactionUOM"}, {ml r."TransactionQtyPrimaryUOM"}, {ml r."PrimaryUOM"}, {ml r."PickedQty"}, {ml r."PickQtyPrimaryUOM"}, {ml r."LastScanMode"}, {ml r."ItemScanSeq"}, {ml r."IsOnHold"}, {ml r."PickAdjusted"}, {ml r."AdjustedQty"}, {ml r."ReasonCodeId"}, {ml r."ManuallyPickCount"}, {ml r."IsInException"}, {ml r."ExceptionReasonCodeId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Pick_Detail',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Pick_Detail"
SET "TransactionID" = {ml r."TransactionID"}, "TransactionDetailID" = {ml r."TransactionDetailID"}, "TransactionTypeId" = {ml r."TransactionTypeId"}, "TransactionStatusId" = {ml r."TransactionStatusId"}, "ItemID" = {ml r."ItemID"}, "TransactionQty" = {ml r."TransactionQty"}, "TransactionUOM" = {ml r."TransactionUOM"}, "TransactionQtyPrimaryUOM" = {ml r."TransactionQtyPrimaryUOM"}, "PrimaryUOM" = {ml r."PrimaryUOM"}, "PickedQty" = {ml r."PickedQty"}, "PickQtyPrimaryUOM" = {ml r."PickQtyPrimaryUOM"}, "LastScanMode" = {ml r."LastScanMode"}, "ItemScanSeq" = {ml r."ItemScanSeq"}, "IsOnHold" = {ml r."IsOnHold"}, "PickAdjusted" = {ml r."PickAdjusted"}, "AdjustedQty" = {ml r."AdjustedQty"}, "ReasonCodeId" = {ml r."ReasonCodeId"}, "ManuallyPickCount" = {ml r."ManuallyPickCount"}, "IsInException" = {ml r."IsInException"}, "ExceptionReasonCodeId" = {ml r."ExceptionReasonCodeId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "PickDetailID" = {ml r."PickDetailID"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Pick_Detail',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Pick_Detail"
WHERE "PickDetailID" = {ml r."PickDetailID"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_delete - End */

/* Table :Route_Replenishment_Header */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Header',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Route_Replenishment_Header"."ReplenishmentID"
,"BUSDTA"."Route_Replenishment_Header"."RouteId"
,"BUSDTA"."Route_Replenishment_Header"."StatusId"
,"BUSDTA"."Route_Replenishment_Header"."ReplenishmentTypeId"
,"BUSDTA"."Route_Replenishment_Header"."TransDateFrom"
,"BUSDTA"."Route_Replenishment_Header"."TransDateTo"
,"BUSDTA"."Route_Replenishment_Header"."RequestedBy"
,"BUSDTA"."Route_Replenishment_Header"."FromBranchId"
,"BUSDTA"."Route_Replenishment_Header"."ToBranchId"
,"BUSDTA"."Route_Replenishment_Header"."CreatedBy"
,"BUSDTA"."Route_Replenishment_Header"."CreatedDatetime"
,"BUSDTA"."Route_Replenishment_Header"."UpdatedBy"
,"BUSDTA"."Route_Replenishment_Header"."UpdatedDatetime"

FROM "BUSDTA"."Route_Replenishment_Header"
WHERE "BUSDTA"."Route_Replenishment_Header"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Route_Replenishment_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Header',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Route_Replenishment_Header_del"."ReplenishmentID"
,"BUSDTA"."Route_Replenishment_Header_del"."RouteId"

FROM "BUSDTA"."Route_Replenishment_Header_del"
WHERE "BUSDTA"."Route_Replenishment_Header_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Header',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Replenishment_Header"
("ReplenishmentID", "RouteId", "StatusId", "ReplenishmentTypeId", "TransDateFrom", "TransDateTo", "RequestedBy", "FromBranchId", "ToBranchId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ReplenishmentID"}, {ml r."RouteId"}, {ml r."StatusId"}, {ml r."ReplenishmentTypeId"}, {ml r."TransDateFrom"}, {ml r."TransDateTo"}, {ml r."RequestedBy"}, {ml r."FromBranchId"}, {ml r."ToBranchId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Header',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Replenishment_Header"
SET "StatusId" = {ml r."StatusId"}, "ReplenishmentTypeId" = {ml r."ReplenishmentTypeId"}, "TransDateFrom" = {ml r."TransDateFrom"}, "TransDateTo" = {ml r."TransDateTo"}, "RequestedBy" = {ml r."RequestedBy"}, "FromBranchId" = {ml r."FromBranchId"}, "ToBranchId" = {ml r."ToBranchId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ReplenishmentID" = {ml r."ReplenishmentID"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Header',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Route_Replenishment_Header"
WHERE "ReplenishmentID" = {ml r."ReplenishmentID"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :Route_Replenishment_Detail */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Detail',
	'download_cursor',
	'sql',

'
 SELECT "BUSDTA"."Route_Replenishment_Detail"."ReplenishmentID"
,"BUSDTA"."Route_Replenishment_Detail"."ReplenishmentDetailID"
,"BUSDTA"."Route_Replenishment_Detail"."ItemId"
,"BUSDTA"."Route_Replenishment_Detail"."RouteId"
,"BUSDTA"."Route_Replenishment_Detail"."ReplenishmentQtyUM"
,"BUSDTA"."Route_Replenishment_Detail"."ReplenishmentQty"
,"BUSDTA"."Route_Replenishment_Detail"."AdjustmentQty"
,"BUSDTA"."Route_Replenishment_Detail"."PickedQty"
,"BUSDTA"."Route_Replenishment_Detail"."PickedStatusTypeID"
,"BUSDTA"."Route_Replenishment_Detail"."AvailaibilityAtTime"
,"BUSDTA"."Route_Replenishment_Detail"."CurrentAvailability"
,"BUSDTA"."Route_Replenishment_Detail"."DemandQty"
,"BUSDTA"."Route_Replenishment_Detail"."ParLevelQty"
,"BUSDTA"."Route_Replenishment_Detail"."OpenReplnQty"
,"BUSDTA"."Route_Replenishment_Detail"."ShippedQty"
,"BUSDTA"."Route_Replenishment_Detail"."OrderID"
,"BUSDTA"."Route_Replenishment_Detail"."LastSaved"
,"BUSDTA"."Route_Replenishment_Detail"."SequenceNumber"
,"BUSDTA"."Route_Replenishment_Detail"."CreatedBy"
,"BUSDTA"."Route_Replenishment_Detail"."CreatedDatetime"
,"BUSDTA"."Route_Replenishment_Detail"."UpdatedBy"
,"BUSDTA"."Route_Replenishment_Detail"."UpdatedDatetime"
 
FROM "BUSDTA"."Route_Replenishment_Detail", "BUSDTA"."Route_Replenishment_Header"
WHERE "BUSDTA"."Route_Replenishment_Detail"."UpdatedDatetime">= {ml s.last_table_download}
 AND "BUSDTA"."Route_Replenishment_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 AND "BUSDTA"."Route_Replenishment_Detail"."ReplenishmentID" = "BUSDTA"."Route_Replenishment_Header"."ReplenishmentID"


 

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Detail',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Route_Replenishment_Detail_del"."ReplenishmentID"
,"BUSDTA"."Route_Replenishment_Detail_del"."ReplenishmentDetailID"
,"BUSDTA"."Route_Replenishment_Detail_del"."ItemId"
,"BUSDTA"."Route_Replenishment_Detail_del"."RouteId"

FROM "BUSDTA"."Route_Replenishment_Detail_del"
WHERE "BUSDTA"."Route_Replenishment_Detail_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Detail',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Replenishment_Detail"
("ReplenishmentID", "ReplenishmentDetailID", "ItemId", "RouteId", "ReplenishmentQtyUM", "ReplenishmentQty", "AdjustmentQty", "PickedQty", "PickedStatusTypeID", "AvailaibilityAtTime", "CurrentAvailability", "DemandQty", "ParLevelQty", "OpenReplnQty", "ShippedQty", "OrderID", "LastSaved", "SequenceNumber", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ReplenishmentID"}, {ml r."ReplenishmentDetailID"}, {ml r."ItemId"}, {ml r."RouteId"}, {ml r."ReplenishmentQtyUM"}, {ml r."ReplenishmentQty"}, {ml r."AdjustmentQty"}, {ml r."PickedQty"}, {ml r."PickedStatusTypeID"}, {ml r."AvailaibilityAtTime"}, {ml r."CurrentAvailability"}, {ml r."DemandQty"}, {ml r."ParLevelQty"}, {ml r."OpenReplnQty"}, {ml r."ShippedQty"}, {ml r."OrderID"}, {ml r."LastSaved"}, {ml r."SequenceNumber"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Detail',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Replenishment_Detail"
SET "ReplenishmentQtyUM" = {ml r."ReplenishmentQtyUM"}, "ReplenishmentQty" = {ml r."ReplenishmentQty"}, "AdjustmentQty" = {ml r."AdjustmentQty"}, "PickedQty" = {ml r."PickedQty"}, "PickedStatusTypeID" = {ml r."PickedStatusTypeID"}, "AvailaibilityAtTime" = {ml r."AvailaibilityAtTime"}, "CurrentAvailability" = {ml r."CurrentAvailability"}, "DemandQty" = {ml r."DemandQty"}, "ParLevelQty" = {ml r."ParLevelQty"}, "OpenReplnQty" = {ml r."OpenReplnQty"}, "ShippedQty" = {ml r."ShippedQty"}, "OrderID" = {ml r."OrderID"}, "LastSaved" = {ml r."LastSaved"}, "SequenceNumber" = {ml r."SequenceNumber"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ReplenishmentID" = {ml r."ReplenishmentID"} AND "ReplenishmentDetailID" = {ml r."ReplenishmentDetailID"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Route_Replenishment_Detail',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Route_Replenishment_Detail"
WHERE "ReplenishmentID" = {ml r."ReplenishmentID"} AND "ReplenishmentDetailID" = {ml r."ReplenishmentDetailID"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
'

/* upload_delete - End */

/* Table :Metric_CustHistDemand */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Metric_CustHistDemand',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."Metric_CustHistDemand"."CustomerId"
,"BUSDTA"."Metric_CustHistDemand"."ItemID"
,"BUSDTA"."Metric_CustHistDemand"."DemandFromDate"
,"BUSDTA"."Metric_CustHistDemand"."DemandThroughDate"
,"BUSDTA"."Metric_CustHistDemand"."PlannedStops"
,"BUSDTA"."Metric_CustHistDemand"."ActualStops"
,"BUSDTA"."Metric_CustHistDemand"."TotalQtySold"
,"BUSDTA"."Metric_CustHistDemand"."TotalQtyReturned"
,"BUSDTA"."Metric_CustHistDemand"."QtySoldPerStop"
,"BUSDTA"."Metric_CustHistDemand"."QtyReturnedPerStop"
,"BUSDTA"."Metric_CustHistDemand"."NetQtySoldPerStop"
,"BUSDTA"."Metric_CustHistDemand"."QtySoldDeltaPerStop"
,"BUSDTA"."Metric_CustHistDemand"."QtyReturnedDeltaPerStop"
,"BUSDTA"."Metric_CustHistDemand"."CreatedBy"
,"BUSDTA"."Metric_CustHistDemand"."CreatedDatetime"
,"BUSDTA"."Metric_CustHistDemand"."UpdatedBy"
,"BUSDTA"."Metric_CustHistDemand"."UpdatedDatetime"

FROM "BUSDTA"."Metric_CustHistDemand", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE ("BUSDTA"."Metric_CustHistDemand"."last_modified">= {ml s.last_table_download}  or rm.last_modified >= {ml s.last_table_download}
or crm.last_modified >= {ml s.last_table_download})
AND CustomerId = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Metric_CustHistDemand',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Metric_CustHistDemand_del"."CustomerId"
,"BUSDTA"."Metric_CustHistDemand_del"."ItemID"

FROM "BUSDTA"."Metric_CustHistDemand_del"
WHERE "BUSDTA"."Metric_CustHistDemand_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :Notification */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Notification',
	'download_cursor',
	'sql',

'


 
SELECT "BUSDTA"."Notification"."NotificationID"
,"BUSDTA"."Notification"."RouteID"
,"BUSDTA"."Notification"."Title"
,"BUSDTA"."Notification"."Description"
,"BUSDTA"."Notification"."TypeID"
,"BUSDTA"."Notification"."Category"
,"BUSDTA"."Notification"."SubCategory"
,"BUSDTA"."Notification"."Read"
,"BUSDTA"."Notification"."ValidTill"
,"BUSDTA"."Notification"."Acknowledged"
,"BUSDTA"."Notification"."Initiator"
,"BUSDTA"."Notification"."CreatedBy"
,"BUSDTA"."Notification"."CreatedDatetime"
,"BUSDTA"."Notification"."UpdatedBy"
,"BUSDTA"."Notification"."UpdatedDatetime"

FROM "BUSDTA"."Notification"
WHERE "BUSDTA"."Notification"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Notification"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Notification',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Notification_del"."NotificationID"
,"BUSDTA"."Notification_del"."RouteID"

FROM "BUSDTA"."Notification_del"
WHERE "BUSDTA"."Notification_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Notification',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Notification"
("NotificationID", "RouteID", "Title", "Description", "TypeID", "Category", "SubCategory", "Read", "ValidTill", "Acknowledged", "Initiator", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."NotificationID"}, {ml r."RouteID"}, {ml r."Title"}, {ml r."Description"}, {ml r."TypeID"}, {ml r."Category"}, {ml r."SubCategory"}, {ml r."Read"}, {ml r."ValidTill"}, {ml r."Acknowledged"}, {ml r."Initiator"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Notification',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Notification"
SET "Title" = {ml r."Title"}, "Description" = {ml r."Description"}, "TypeID" = {ml r."TypeID"}, "Category" = {ml r."Category"}, "SubCategory" = {ml r."SubCategory"}, "Read" = {ml r."Read"}, "ValidTill" = {ml r."ValidTill"}, "Acknowledged" = {ml r."Acknowledged"}, "Initiator" = {ml r."Initiator"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "NotificationID" = {ml r."NotificationID"} AND "RouteID" = {ml r."RouteID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Notification',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Notification"
WHERE "NotificationID" = {ml r."NotificationID"} AND "RouteID" = {ml r."RouteID"}
'

/* upload_delete - End */

/* Table :tblSystemSetup */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblSystemSetup',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :SalesOrder_ReturnOrder_Mapping */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'SalesOrder_ReturnOrder_Mapping',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."SalesOrder_ReturnOrder_Mapping"."SalesOrderId"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."ReturnOrderID"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."OrderDetailID"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."ItemID"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."RouteId"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."CreatedBy"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."CreatedDatetime"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."UpdatedBy"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."UpdatedDatetime"

FROM "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
WHERE "BUSDTA"."SalesOrder_ReturnOrder_Mapping"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."SalesOrder_ReturnOrder_Mapping"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'SalesOrder_ReturnOrder_Mapping',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."SalesOrder_ReturnOrder_Mapping_del"."SalesOrderId"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping_del"."ReturnOrderID"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping_del"."OrderDetailID"

FROM "BUSDTA"."SalesOrder_ReturnOrder_Mapping_del"
WHERE "BUSDTA"."SalesOrder_ReturnOrder_Mapping_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'SalesOrder_ReturnOrder_Mapping',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
("SalesOrderId", "ReturnOrderID", "OrderDetailID", "ItemID", "RouteId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."SalesOrderId"}, {ml r."ReturnOrderID"}, {ml r."OrderDetailID"}, {ml r."ItemID"}, {ml r."RouteId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'SalesOrder_ReturnOrder_Mapping',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
SET "ItemID" = {ml r."ItemID"}, "RouteId" = {ml r."RouteId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "SalesOrderId" = {ml r."SalesOrderId"} AND "ReturnOrderID" = {ml r."ReturnOrderID"} AND "OrderDetailID" = {ml r."OrderDetailID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'SalesOrder_ReturnOrder_Mapping',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
WHERE "SalesOrderId" = {ml r."SalesOrderId"} AND "ReturnOrderID" = {ml r."ReturnOrderID"} AND "OrderDetailID" = {ml r."OrderDetailID"}
'

/* upload_delete - End */

/* Table :F0115 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0115',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :F01151 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F01151',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :F41001 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F41001',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :F4105 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F4105',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :F42140 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F42140',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :M0100 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M0100',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :M550001 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M550001',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :M550002 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M550002',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :M550003 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M550003',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :M56M0002B */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'M56M0002B',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :NonMobilinkSource */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'NonMobilinkSource',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :SearchTMP */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'SearchTMP',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblDeltaStats */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblDeltaStats',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblGlobalSync */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblGlobalSync',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblGlobalSyncLog */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblGlobalSyncLog',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblApplicationGroup */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblApplicationGroup',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblGroupTables */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblGroupTables',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblStatsGroups */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblStatsGroups',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblSyncArticles */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblSyncArticles',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblSyncDetails */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblSyncDetails',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblTransactionAudit */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblTransactionAudit',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblTransactionControl */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblTransactionControl',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :tblAlliedProspCategory */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspCategory',
	'download_cursor',
	'sql',

'

SELECT "Busdta"."tblAlliedProspCategory"."CategoryID"
,"Busdta"."tblAlliedProspCategory"."Category"
,"Busdta"."tblAlliedProspCategory"."CreatedBy"
,"Busdta"."tblAlliedProspCategory"."CreatedDatetime"
,"Busdta"."tblAlliedProspCategory"."UpdatedBy"
,"Busdta"."tblAlliedProspCategory"."UpdatedDatetime"

FROM "Busdta"."tblAlliedProspCategory"
WHERE "Busdta"."tblAlliedProspCategory"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspCategory',
	'download_delete_cursor',
	'sql',

'
 
SELECT "Busdta"."tblAlliedProspCategory_del"."CategoryID"

FROM "Busdta"."tblAlliedProspCategory_del"
WHERE "Busdta"."tblAlliedProspCategory_del"."last_modified">= {ml s.last_table_download}


'

/* download_delete_cursor - End */

/* Table :tblAlliedProspect */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspect',
	'download_cursor',
	'sql',

'
 
/* Date Modified: 11-01-2016 */

SELECT "tblAlliedProspect"."AlliedProspectID"
,"tblAlliedProspect"."ProspectID"
,"tblAlliedProspect"."CompetitorID"
,"tblAlliedProspect"."CategoryID"
,"tblAlliedProspect"."SubCategoryID"
,"tblAlliedProspect"."BrandID"
,"tblAlliedProspect"."UOMID"
,"tblAlliedProspect"."PackSize"
,"tblAlliedProspect"."CS_PK_LB"
,"tblAlliedProspect"."Price"
,"tblAlliedProspect"."UsageMeasurementID"
,"tblAlliedProspect"."CreatedBy"
,"tblAlliedProspect"."CreatedDatetime"
,"tblAlliedProspect"."UpdatedBy"
,"tblAlliedProspect"."UpdatedDatetime"

FROM "BUSDTA"."tblAlliedProspect"
INNER JOIN BUSDTA.Prospect_Master ON "tblAlliedProspect".ProspectID=BUSDTA.Prospect_Master.ProspectId 
WHERE "BUSDTA"."tblAlliedProspect"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Master"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspect',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblAlliedProspect_del"."AlliedProspectID",
"BUSDTA"."tblAlliedProspect_del"."ProspectID"

FROM "BUSDTA"."tblAlliedProspect_del"
WHERE "BUSDTA"."tblAlliedProspect_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspect',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."tblAlliedProspect"
("AlliedProspectID", "ProspectID", "CompetitorID", "CategoryID", "SubCategoryID", "BrandID", "UOMID", "PackSize", "CS_PK_LB", "Price", "UsageMeasurementID", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."AlliedProspectID"}, {ml r."ProspectID"}, {ml r."CompetitorID"}, {ml r."CategoryID"}, {ml r."SubCategoryID"}, {ml r."BrandID"}, {ml r."UOMID"}, {ml r."PackSize"}, {ml r."CS_PK_LB"}, {ml r."Price"}, {ml r."UsageMeasurementID"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspect',
	'upload_update',
	'sql',

'

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."tblAlliedProspect"
SET  "CompetitorID" = {ml r."CompetitorID"}, "CategoryID" = {ml r."CategoryID"}, "SubCategoryID" = {ml r."SubCategoryID"}, "BrandID" = {ml r."BrandID"}, "UOMID" = {ml r."UOMID"}, "PackSize" = {ml r."PackSize"}, "CS_PK_LB" = {ml r."CS_PK_LB"}, "Price" = {ml r."Price"}, "UsageMeasurementID" = {ml r."UsageMeasurementID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "AlliedProspectID" = {ml r."AlliedProspectID"} and "ProspectID" = {ml r."ProspectID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspect',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."tblAlliedProspect"
WHERE "AlliedProspectID" = {ml r."AlliedProspectID"} and "ProspectID" = {ml r."ProspectID"}
'

/* upload_delete - End */

/* Table :tblAlliedProspSubcategory */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspSubcategory',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblAlliedProspSubcategory"."SubCategoryID"
,"BUSDTA"."tblAlliedProspSubcategory"."SubCategory"
,"BUSDTA"."tblAlliedProspSubcategory"."CategoryID"
,"BUSDTA"."tblAlliedProspSubcategory"."CreatedBy"
,"BUSDTA"."tblAlliedProspSubcategory"."CreatedDatetime"
,"BUSDTA"."tblAlliedProspSubcategory"."UpdatedBy"
,"BUSDTA"."tblAlliedProspSubcategory"."UpdatedDatetime"

FROM "BUSDTA"."tblAlliedProspSubcategory"
WHERE "BUSDTA"."tblAlliedProspSubcategory"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblAlliedProspSubcategory',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblAlliedProspSubcategory_del"."SubCategoryID"

FROM "BUSDTA"."tblAlliedProspSubcategory_del"
WHERE "BUSDTA"."tblAlliedProspSubcategory_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */

/* Table :tblBrandLable */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblBrandLable',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblBrandLable"."BrandID"
,"BUSDTA"."tblBrandLable"."BrandLable"
,"BUSDTA"."tblBrandLable"."CreatedBy"
,"BUSDTA"."tblBrandLable"."CreatedDatetime"
,"BUSDTA"."tblBrandLable"."UpdatedBy"
,"BUSDTA"."tblBrandLable"."UpdatedDatetime"

FROM "BUSDTA"."tblBrandLable"
WHERE "BUSDTA"."tblBrandLable"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblBrandLable',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblBrandLable_del"."BrandID"

FROM "BUSDTA"."tblBrandLable_del"
WHERE "BUSDTA"."tblBrandLable_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblCoffeeBlends */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeBlends',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCoffeeBlends"."CoffeeBlendID"
,"BUSDTA"."tblCoffeeBlends"."CoffeeBlend"
,"BUSDTA"."tblCoffeeBlends"."CreatedBy"
,"BUSDTA"."tblCoffeeBlends"."CreatedDatetime"
,"BUSDTA"."tblCoffeeBlends"."UpdatedBy"
,"BUSDTA"."tblCoffeeBlends"."UpdatedDatetime"

FROM "BUSDTA"."tblCoffeeBlends"
WHERE "BUSDTA"."tblCoffeeBlends"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeBlends',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCoffeeBlends_del"."CoffeeBlendID"

FROM "BUSDTA"."tblCoffeeBlends_del"
WHERE "BUSDTA"."tblCoffeeBlends_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblCoffeeProspect */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeProspect',
	'download_cursor',
	'sql',

'


/* Date Modified: 11-01-2016 */

SELECT "tblCoffeeProspect"."CoffeeProspectID"
,"tblCoffeeProspect"."ProspectID"
,"tblCoffeeProspect"."CoffeeBlendID"
,"tblCoffeeProspect"."CompetitorID"
,"tblCoffeeProspect"."UOMID"
,"tblCoffeeProspect"."PackSize"
,"tblCoffeeProspect"."CS_PK_LB"
,"tblCoffeeProspect"."Price"
,"tblCoffeeProspect"."UsageMeasurementID"
,"tblCoffeeProspect"."LiqCoffeeTypeID"
,"tblCoffeeProspect"."CreatedBy"
,"tblCoffeeProspect"."CreatedDatetime"
,"tblCoffeeProspect"."UpdatedBy"
,"tblCoffeeProspect"."UpdatedDatetime"
FROM "BUSDTA"."tblCoffeeProspect"
INNER JOIN BUSDTA.Prospect_Master ON "tblCoffeeProspect".ProspectID=BUSDTA.Prospect_Master.ProspectId 
WHERE "BUSDTA"."tblCoffeeProspect"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Master"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeProspect',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCoffeeProspect_del"."CoffeeProspectID"
,"BUSDTA"."tblCoffeeProspect_del"."ProspectID"

FROM "BUSDTA"."tblCoffeeProspect_del"
WHERE "BUSDTA"."tblCoffeeProspect_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeProspect',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."tblCoffeeProspect"
("CoffeeProspectID", "ProspectID", "CoffeeBlendID", "CompetitorID", "UOMID", "PackSize", "CS_PK_LB", "Price", "UsageMeasurementID", "LiqCoffeeTypeID", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CoffeeProspectID"}, {ml r."ProspectID"}, {ml r."CoffeeBlendID"}, {ml r."CompetitorID"}, {ml r."UOMID"}, {ml r."PackSize"}, {ml r."CS_PK_LB"}, {ml r."Price"}, {ml r."UsageMeasurementID"}, {ml r."LiqCoffeeTypeID"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeProspect',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."tblCoffeeProspect"
SET "CoffeeBlendID" = {ml r."CoffeeBlendID"}, "CompetitorID" = {ml r."CompetitorID"}, "UOMID" = {ml r."UOMID"}, "PackSize" = {ml r."PackSize"}, "CS_PK_LB" = {ml r."CS_PK_LB"}, "Price" = {ml r."Price"}, "UsageMeasurementID" = {ml r."UsageMeasurementID"}, "LiqCoffeeTypeID" = {ml r."LiqCoffeeTypeID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CoffeeProspectID" = {ml r."CoffeeProspectID"} AND "ProspectID" = {ml r."ProspectID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeProspect',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."tblCoffeeProspect"
WHERE "CoffeeProspectID" = {ml r."CoffeeProspectID"} AND "ProspectID" = {ml r."ProspectID"}


'

/* upload_delete - End */

/* Table :tblCoffeeVolume */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeVolume',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCoffeeVolume"."CoffeeVolumeID"
,"BUSDTA"."tblCoffeeVolume"."Volume"
,"BUSDTA"."tblCoffeeVolume"."CreatedBy"
,"BUSDTA"."tblCoffeeVolume"."CreatedDatetime"
,"BUSDTA"."tblCoffeeVolume"."UpdatedBy"
,"BUSDTA"."tblCoffeeVolume"."UpdatedDatetime"

FROM "BUSDTA"."tblCoffeeVolume"
WHERE "BUSDTA"."tblCoffeeVolume"."last_modified">= {ml s.last_table_download}

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCoffeeVolume',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCoffeeVolume_del"."CoffeeVolumeID"

FROM "BUSDTA"."tblCoffeeVolume_del"
WHERE "BUSDTA"."tblCoffeeVolume_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblCompetitor */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCompetitor',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCompetitor"."CompetitorID"
,"BUSDTA"."tblCompetitor"."Competitor"
,"BUSDTA"."tblCompetitor"."CreatedBy"
,"BUSDTA"."tblCompetitor"."CreatedDatetime"
,"BUSDTA"."tblCompetitor"."UpdatedBy"
,"BUSDTA"."tblCompetitor"."UpdatedDatetime"

FROM "BUSDTA"."tblCompetitor"
WHERE "BUSDTA"."tblCompetitor"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCompetitor',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCompetitor_del"."CompetitorID"

FROM "BUSDTA"."tblCompetitor_del"
WHERE "BUSDTA"."tblCompetitor_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblCSPKLB */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCSPKLB',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCSPKLB"."CSPKLBID"
,"BUSDTA"."tblCSPKLB"."CSPKLBType"
,"BUSDTA"."tblCSPKLB"."CreatedBy"
,"BUSDTA"."tblCSPKLB"."CreatedDatetime"
,"BUSDTA"."tblCSPKLB"."UpdatedBy"
,"BUSDTA"."tblCSPKLB"."UpdatedDatetime"

FROM "BUSDTA"."tblCSPKLB"
WHERE "BUSDTA"."tblCSPKLB"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblCSPKLB',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblCSPKLB_del"."CSPKLBID"

FROM "BUSDTA"."tblCSPKLB_del"
WHERE "BUSDTA"."tblCSPKLB_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblEquipment */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipment',
	'download_cursor',
	'sql',

'


/* Date Modified: 11-01-2016 */
 
SELECT "tblEquipment"."EquipmentID"
,"tblEquipment"."ProspectID"
,"tblEquipment"."EquipmentType"
,"tblEquipment"."EquipmentCategoryID"
,"tblEquipment"."EquipmentSubCategory"
,"tblEquipment"."EquipmentQuantity"
,"tblEquipment"."EquipmentOwned"
,"tblEquipment"."CreatedBy"
,"tblEquipment"."CreatedDatetime"
,"tblEquipment"."UpdatedBy"
,"tblEquipment"."UpdatedDatetime"

FROM "BUSDTA"."tblEquipment"
INNER JOIN BUSDTA.Prospect_Master ON "tblEquipment".ProspectID=BUSDTA.Prospect_Master.ProspectId 
WHERE "BUSDTA"."tblEquipment"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Master"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipment',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblEquipment_del"."EquipmentID"
,"BUSDTA"."tblEquipment_del"."ProspectID"

FROM "BUSDTA"."tblEquipment_del"
WHERE "BUSDTA"."tblEquipment_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipment',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."tblEquipment"
("EquipmentID", "ProspectID", "EquipmentType", "EquipmentCategoryID", "EquipmentSubCategory", "EquipmentQuantity", "EquipmentOwned", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."EquipmentID"}, {ml r."ProspectID"}, {ml r."EquipmentType"}, {ml r."EquipmentCategoryID"}, {ml r."EquipmentSubCategory"}, {ml r."EquipmentQuantity"}, {ml r."EquipmentOwned"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipment',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."tblEquipment"
SET "EquipmentType" = {ml r."EquipmentType"}, "EquipmentCategoryID" = {ml r."EquipmentCategoryID"}, "EquipmentSubCategory" = {ml r."EquipmentSubCategory"}, "EquipmentQuantity" = {ml r."EquipmentQuantity"}, "EquipmentOwned" = {ml r."EquipmentOwned"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "EquipmentID" = {ml r."EquipmentID"} AND "ProspectID" = {ml r."ProspectID"}

'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipment',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."tblEquipment"
WHERE "EquipmentID" = {ml r."EquipmentID"} AND "ProspectID" = {ml r."ProspectID"}

'

/* upload_delete - End */

/* Table :tblEquipmentCategory */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipmentCategory',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblEquipmentCategory"."EquipmentCategoryID"
,"BUSDTA"."tblEquipmentCategory"."EquipmentCategory"
,"BUSDTA"."tblEquipmentCategory"."EquipmentTypeID"
,"BUSDTA"."tblEquipmentCategory"."CreatedBy"
,"BUSDTA"."tblEquipmentCategory"."CreatedDatetime"
,"BUSDTA"."tblEquipmentCategory"."UpdatedBy"
,"BUSDTA"."tblEquipmentCategory"."UpdatedDatetime"

FROM "BUSDTA"."tblEquipmentCategory"
WHERE "BUSDTA"."tblEquipmentCategory"."last_modified">= {ml s.last_table_download}

'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipmentCategory',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblEquipmentCategory_del"."EquipmentCategoryID"

FROM "BUSDTA"."tblEquipmentCategory_del"
WHERE "BUSDTA"."tblEquipmentCategory_del"."last_modified">= {ml s.last_table_download}


'

/* download_delete_cursor - End */

/* Table :tblEquipmentSubcategory */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipmentSubcategory',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblEquipmentSubcategory"."EquipmentSubcategoryID"
,"BUSDTA"."tblEquipmentSubcategory"."EquipmentCategoryID"
,"BUSDTA"."tblEquipmentSubcategory"."EquipmentSubCategory"
,"BUSDTA"."tblEquipmentSubcategory"."CreatedBy"
,"BUSDTA"."tblEquipmentSubcategory"."CreatedDatetime"
,"BUSDTA"."tblEquipmentSubcategory"."UpdatedBy"
,"BUSDTA"."tblEquipmentSubcategory"."UpdatedDatetime"

FROM "BUSDTA"."tblEquipmentSubcategory"
WHERE "BUSDTA"."tblEquipmentSubcategory"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipmentSubcategory',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblEquipmentSubcategory_del"."EquipmentSubcategoryID"

FROM "BUSDTA"."tblEquipmentSubcategory_del"
WHERE "BUSDTA"."tblEquipmentSubcategory_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */

/* Table :tblEquipmentType */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipmentType',
	'download_cursor',
	'sql',

'

SELECT "EquipmentTypeID"
,"EquipmentType"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."tblEquipmentType"
WHERE "BUSDTA"."tblEquipmentType"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblEquipmentType',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblEquipmentType_del"."EquipmentTypeID"

FROM "BUSDTA"."tblEquipmentType_del"
WHERE "BUSDTA"."tblEquipmentType_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblUsageMeasurement */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblUsageMeasurement',
	'download_cursor',
	'sql',

'


SELECT "UsageMeasurementID"
,"UsageMeasurement"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."tblUsageMeasurement"
WHERE "BUSDTA"."tblUsageMeasurement"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblUsageMeasurement',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."tblUsageMeasurement_del"."UsageMeasurementID"

FROM "BUSDTA"."tblUsageMeasurement_del"
WHERE "BUSDTA"."tblUsageMeasurement_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblUOM */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblUOM',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblUOM"."UOMID"
,"BUSDTA"."tblUOM"."UOMLable"
,"BUSDTA"."tblUOM"."CreatedBy"
,"BUSDTA"."tblUOM"."CreatedDatetime"
,"BUSDTA"."tblUOM"."UpdatedBy"
,"BUSDTA"."tblUOM"."UpdatedDatetime"

FROM "BUSDTA"."tblUOM"
WHERE "BUSDTA"."tblUOM"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblUOM',
	'download_delete_cursor',
	'sql',

'
SELECT "BUSDTA"."tblUOM_del"."UOMID"

FROM "BUSDTA"."tblUOM_del"
WHERE "BUSDTA"."tblUOM_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :tblLiqCoffeeType */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblLiqCoffeeType',
	'download_cursor',
	'sql',

'

SELECT "LiqCoffeeTypeID"
,"LiqCoffeeType"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."tblLiqCoffeeType"
WHERE "BUSDTA"."tblLiqCoffeeType"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblLiqCoffeeType',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."tblLiqCoffeeType_del"."LiqCoffeeTypeID"

FROM "BUSDTA"."tblLiqCoffeeType_del"
WHERE "BUSDTA"."tblLiqCoffeeType_del"."last_modified">= {ml s.last_table_download}

'

/* download_delete_cursor - End */

/* Table :tblPacketSize */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblPacketSize',
	'download_cursor',
	'sql',

'



SELECT "PacketSizeID"
,"Size"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."tblPacketSize"
WHERE "BUSDTA"."tblPacketSize"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'tblPacketSize',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."tblPacketSize_del"."PacketSizeID"

FROM "BUSDTA"."tblPacketSize_del"
WHERE "BUSDTA"."tblPacketSize_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :F40205 */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40205',
	'download_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F40205"."LFLNTY"
,"BUSDTA"."F40205"."LFLNDS"
,"BUSDTA"."F40205"."LFGLI"
,"BUSDTA"."F40205"."LFIVI"
,"BUSDTA"."F40205"."LFARI"
,"BUSDTA"."F40205"."LFAPI"
,"BUSDTA"."F40205"."LFRSGN"
,"BUSDTA"."F40205"."LFTXYN"
,"BUSDTA"."F40205"."LFPRFT"
,"BUSDTA"."F40205"."LFCDSC"
,"BUSDTA"."F40205"."LFTX01"
,"BUSDTA"."F40205"."LFTX02"
,"BUSDTA"."F40205"."LFGLC"
,"BUSDTA"."F40205"."LFPDC1"
,"BUSDTA"."F40205"."LFPDC2"
,"BUSDTA"."F40205"."LFPDC3"
,"BUSDTA"."F40205"."LFCSJ"
,"BUSDTA"."F40205"."LFDCTO"
,"BUSDTA"."F40205"."LFART"
,"BUSDTA"."F40205"."LFAFT"

FROM "BUSDTA"."F40205"
WHERE "BUSDTA"."F40205"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F40205',
	'download_delete_cursor',
	'sql',

'

 
SELECT "BUSDTA"."F40205_del"."LFLNTY"

FROM "BUSDTA"."F40205_del"
WHERE "BUSDTA"."F40205_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */

/* Table :Order_PriceAdj */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Order_PriceAdj"."OrderID"
,"BUSDTA"."Order_PriceAdj"."LineID"
,"BUSDTA"."Order_PriceAdj"."AdjustmentSeq"
,"BUSDTA"."Order_PriceAdj"."AdjName"
,"BUSDTA"."Order_PriceAdj"."AdjDesc"
,"BUSDTA"."Order_PriceAdj"."FactorValue"
,"BUSDTA"."Order_PriceAdj"."UnitPrice"
,"BUSDTA"."Order_PriceAdj"."OP"
,"BUSDTA"."Order_PriceAdj"."BC"
,"BUSDTA"."Order_PriceAdj"."BasicDesc"
,"BUSDTA"."Order_PriceAdj"."PriceAdjustmentKeyID"
,"BUSDTA"."Order_PriceAdj"."CreatedBy"
,"BUSDTA"."Order_PriceAdj"."CreatedDatetime"
,"BUSDTA"."Order_PriceAdj"."UpdatedBy"
,"BUSDTA"."Order_PriceAdj"."UpdatedDatetime"
FROM "BUSDTA"."Order_PriceAdj"
WHERE "BUSDTA"."Order_PriceAdj"."last_modified">= {ml s.last_table_download}        
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj',
	'download_delete_cursor',
	'sql',

'


SELECT "BUSDTA"."Order_PriceAdj_del"."OrderID"
,"BUSDTA"."Order_PriceAdj_del"."LineID"
,"BUSDTA"."Order_PriceAdj_del"."AdjustmentSeq"
FROM "BUSDTA"."Order_PriceAdj_del"
WHERE "BUSDTA"."Order_PriceAdj_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj',
	'upload_insert',
	'sql',

'


/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_PriceAdj"
("OrderID", "LineID", "AdjustmentSeq", "AdjName", "AdjDesc", "FactorValue", "UnitPrice", "OP", "BC", "BasicDesc", "PriceAdjustmentKeyID", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderID"}, {ml r."LineID"}, {ml r."AdjustmentSeq"}, {ml r."AdjName"}, {ml r."AdjDesc"}, {ml r."FactorValue"}, {ml r."UnitPrice"}, {ml r."OP"}, {ml r."BC"}, {ml r."BasicDesc"}, {ml r."PriceAdjustmentKeyID"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})

'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj',
	'upload_update',
	'sql',

'
 

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_PriceAdj"
SET "AdjName" = {ml r."AdjName"}, "AdjDesc" = {ml r."AdjDesc"}, "FactorValue" = {ml r."FactorValue"}, "UnitPrice" = {ml r."UnitPrice"}, "OP" = {ml r."OP"}, "BC" = {ml r."BC"}, "BasicDesc" = {ml r."BasicDesc"}, "PriceAdjustmentKeyID" = {ml r."PriceAdjustmentKeyID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"} AND "AdjustmentSeq" = {ml r."AdjustmentSeq"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj',
	'upload_delete',
	'sql',

'


/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Order_PriceAdj"
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"} AND "AdjustmentSeq" = {ml r."AdjustmentSeq"}
 
'

/* upload_delete - End */

/* Table :CustomerConfig */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CustomerConfig',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."CustomerConfig"."CustomerID"
,"BUSDTA"."CustomerConfig"."CustomerReference1"
,"BUSDTA"."CustomerConfig"."CustomerReference2"
,"BUSDTA"."CustomerConfig"."CreatedBy"
,"BUSDTA"."CustomerConfig"."CreatedDatetime"
,"BUSDTA"."CustomerConfig"."UpdatedBy"
,"BUSDTA"."CustomerConfig"."UpdatedDatetime"
FROM "BUSDTA"."CustomerConfig"
WHERE "BUSDTA"."CustomerConfig"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CustomerConfig',
	'download_delete_cursor',
	'sql',

'
SELECT "BUSDTA"."CustomerConfig_del"."CustomerID"
FROM "BUSDTA"."CustomerConfig_del"
WHERE "BUSDTA"."CustomerConfig_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CustomerConfig',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."CustomerConfig"
("CustomerID", "CustomerReference1", "CustomerReference2", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."CustomerID"}, {ml r."CustomerReference1"}, {ml r."CustomerReference2"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
 
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CustomerConfig',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."CustomerConfig"
SET "CustomerReference1" = {ml r."CustomerReference1"}, "CustomerReference2" = {ml r."CustomerReference2"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CustomerID" = {ml r."CustomerID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'CustomerConfig',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."CustomerConfig"
WHERE "CustomerID" = {ml r."CustomerID"}
 
'

/* upload_delete - End */

/* Table :Order_Header_BETA */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header_BETA',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Order_Header_BETA"."OrderID"
,"BUSDTA"."Order_Header_BETA"."OrderTypeId"
,"BUSDTA"."Order_Header_BETA"."OriginatingRouteID"
,"BUSDTA"."Order_Header_BETA"."Branch"
,"BUSDTA"."Order_Header_BETA"."CustShipToId"
,"BUSDTA"."Order_Header_BETA"."CustBillToId"
,"BUSDTA"."Order_Header_BETA"."CustParentId"
,"BUSDTA"."Order_Header_BETA"."OrderDate"
,"BUSDTA"."Order_Header_BETA"."TotalCoffeeAmt"
,"BUSDTA"."Order_Header_BETA"."TotalAlliedAmt"
,"BUSDTA"."Order_Header_BETA"."OrderTotalAmt"
,"BUSDTA"."Order_Header_BETA"."SalesTaxAmt"
,"BUSDTA"."Order_Header_BETA"."InvoiceTotalAmt"
,"BUSDTA"."Order_Header_BETA"."EnergySurchargeAmt"
,"BUSDTA"."Order_Header_BETA"."VarianceAmt"
,"BUSDTA"."Order_Header_BETA"."SurchargeReasonCodeId"
,"BUSDTA"."Order_Header_BETA"."OrderStateId"
,"BUSDTA"."Order_Header_BETA"."OrderSignature"
,"BUSDTA"."Order_Header_BETA"."ChargeOnAccountSignature"
,"BUSDTA"."Order_Header_BETA"."JDEOrderCompany"
,"BUSDTA"."Order_Header_BETA"."JDEOrderNumber"
,"BUSDTA"."Order_Header_BETA"."JDEOrderType"
,"BUSDTA"."Order_Header_BETA"."JDEInvoiceCompany"
,"BUSDTA"."Order_Header_BETA"."JDEInvoiceNumber"
,"BUSDTA"."Order_Header_BETA"."JDEOInvoiceType"
,"BUSDTA"."Order_Header_BETA"."JDEZBatchNumber"
,"BUSDTA"."Order_Header_BETA"."JDEProcessID"
,"BUSDTA"."Order_Header_BETA"."SettlementID"
,"BUSDTA"."Order_Header_BETA"."PaymentTerms"
,"BUSDTA"."Order_Header_BETA"."CustomerReference1"
,"BUSDTA"."Order_Header_BETA"."CustomerReference2"
,"BUSDTA"."Order_Header_BETA"."AdjustmentSchedule"
,"BUSDTA"."Order_Header_BETA"."TaxArea"
,"BUSDTA"."Order_Header_BETA"."TaxExplanationCode"
,"BUSDTA"."Order_Header_BETA"."VoidReasonCodeId"
,"BUSDTA"."Order_Header_BETA"."ChargeOnAccount"
,"BUSDTA"."Order_Header_BETA"."HoldCommitted"
,"BUSDTA"."Order_Header_BETA"."CreatedBy"
,"BUSDTA"."Order_Header_BETA"."CreatedDatetime"
,"BUSDTA"."Order_Header_BETA"."UpdatedBy"
,"BUSDTA"."Order_Header_BETA"."UpdatedDatetime"
FROM "BUSDTA"."Order_Header_BETA"
WHERE "BUSDTA"."Order_Header_BETA"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header_BETA',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Order_Header_BETA_del"."OrderID"
FROM "BUSDTA"."Order_Header_BETA_del"
WHERE "BUSDTA"."Order_Header_BETA_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header_BETA',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Header_BETA"
("OrderID", "OrderTypeId", "OriginatingRouteID", "Branch", "CustShipToId", "CustBillToId", "CustParentId", "OrderDate", "TotalCoffeeAmt", "TotalAlliedAmt", "OrderTotalAmt", "SalesTaxAmt", "InvoiceTotalAmt", "EnergySurchargeAmt", "VarianceAmt", "SurchargeReasonCodeId", "OrderStateId", "OrderSignature", "ChargeOnAccountSignature", "JDEOrderCompany", "JDEOrderNumber", "JDEOrderType", "JDEInvoiceCompany", "JDEInvoiceNumber", "JDEOInvoiceType", "JDEZBatchNumber", "JDEProcessID", "SettlementID", "PaymentTerms", "CustomerReference1", "CustomerReference2", "AdjustmentSchedule", "TaxArea", "TaxExplanationCode", "VoidReasonCodeId", "ChargeOnAccount", "HoldCommitted", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderID"}, {ml r."OrderTypeId"}, {ml r."OriginatingRouteID"}, {ml r."Branch"}, {ml r."CustShipToId"}, {ml r."CustBillToId"}, {ml r."CustParentId"}, {ml r."OrderDate"}, {ml r."TotalCoffeeAmt"}, {ml r."TotalAlliedAmt"}, {ml r."OrderTotalAmt"}, {ml r."SalesTaxAmt"}, {ml r."InvoiceTotalAmt"}, {ml r."EnergySurchargeAmt"}, {ml r."VarianceAmt"}, {ml r."SurchargeReasonCodeId"}, {ml r."OrderStateId"}, {ml r."OrderSignature"}, {ml r."ChargeOnAccountSignature"}, {ml r."JDEOrderCompany"}, {ml r."JDEOrderNumber"}, {ml r."JDEOrderType"}, {ml r."JDEInvoiceCompany"}, {ml r."JDEInvoiceNumber"}, {ml r."JDEOInvoiceType"}, {ml r."JDEZBatchNumber"}, {ml r."JDEProcessID"}, {ml r."SettlementID"}, {ml r."PaymentTerms"}, {ml r."CustomerReference1"}, {ml r."CustomerReference2"}, {ml r."AdjustmentSchedule"}, {ml r."TaxArea"}, {ml r."TaxExplanationCode"}, {ml r."VoidReasonCodeId"}, {ml r."ChargeOnAccount"}, {ml r."HoldCommitted"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header_BETA',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_Header_BETA"
SET "OrderTypeId" = {ml r."OrderTypeId"}, "OriginatingRouteID" = {ml r."OriginatingRouteID"}, "Branch" = {ml r."Branch"}, "CustShipToId" = {ml r."CustShipToId"}, "CustBillToId" = {ml r."CustBillToId"}, "CustParentId" = {ml r."CustParentId"}, "OrderDate" = {ml r."OrderDate"}, "TotalCoffeeAmt" = {ml r."TotalCoffeeAmt"}, "TotalAlliedAmt" = {ml r."TotalAlliedAmt"}, "OrderTotalAmt" = {ml r."OrderTotalAmt"}, "SalesTaxAmt" = {ml r."SalesTaxAmt"}, "InvoiceTotalAmt" = {ml r."InvoiceTotalAmt"}, "EnergySurchargeAmt" = {ml r."EnergySurchargeAmt"}, "VarianceAmt" = {ml r."VarianceAmt"}, "SurchargeReasonCodeId" = {ml r."SurchargeReasonCodeId"}, "OrderStateId" = {ml r."OrderStateId"}, "OrderSignature" = {ml r."OrderSignature"}, "ChargeOnAccountSignature" = {ml r."ChargeOnAccountSignature"}, "JDEOrderCompany" = {ml r."JDEOrderCompany"}, "JDEOrderNumber" = {ml r."JDEOrderNumber"}, "JDEOrderType" = {ml r."JDEOrderType"}, "JDEInvoiceCompany" = {ml r."JDEInvoiceCompany"}, "JDEInvoiceNumber" = {ml r."JDEInvoiceNumber"}, "JDEOInvoiceType" = {ml r."JDEOInvoiceType"}, "JDEZBatchNumber" = {ml r."JDEZBatchNumber"}, "JDEProcessID" = {ml r."JDEProcessID"}, "SettlementID" = {ml r."SettlementID"}, "PaymentTerms" = {ml r."PaymentTerms"}, "CustomerReference1" = {ml r."CustomerReference1"}, "CustomerReference2" = {ml r."CustomerReference2"}, "AdjustmentSchedule" = {ml r."AdjustmentSchedule"}, "TaxArea" = {ml r."TaxArea"}, "TaxExplanationCode" = {ml r."TaxExplanationCode"}, "VoidReasonCodeId" = {ml r."VoidReasonCodeId"}, "ChargeOnAccount" = {ml r."ChargeOnAccount"}, "HoldCommitted" = {ml r."HoldCommitted"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "OrderID" = {ml r."OrderID"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Header_BETA',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Order_Header_BETA"
WHERE "OrderID" = {ml r."OrderID"}
 
'

/* upload_delete - End */

/* Table :Order_PriceAdj_BETA */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj_BETA',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."Order_PriceAdj_BETA"."OrderID"
,"BUSDTA"."Order_PriceAdj_BETA"."LineID"
,"BUSDTA"."Order_PriceAdj_BETA"."AdjustmentSeq"
,"BUSDTA"."Order_PriceAdj_BETA"."AdjName"
,"BUSDTA"."Order_PriceAdj_BETA"."AdjDesc"
,"BUSDTA"."Order_PriceAdj_BETA"."FactorValue"
,"BUSDTA"."Order_PriceAdj_BETA"."UnitPrice"
,"BUSDTA"."Order_PriceAdj_BETA"."OP"
,"BUSDTA"."Order_PriceAdj_BETA"."BC"
,"BUSDTA"."Order_PriceAdj_BETA"."BasicDesc"
,"BUSDTA"."Order_PriceAdj_BETA"."PriceAdjustmentKeyID"
,"BUSDTA"."Order_PriceAdj_BETA"."CreatedBy"
,"BUSDTA"."Order_PriceAdj_BETA"."CreatedDatetime"
,"BUSDTA"."Order_PriceAdj_BETA"."UpdatedBy"
,"BUSDTA"."Order_PriceAdj_BETA"."UpdatedDatetime"
FROM "BUSDTA"."Order_PriceAdj_BETA"
WHERE "BUSDTA"."Order_PriceAdj_BETA"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj_BETA',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."Order_PriceAdj_BETA_del"."OrderID"
,"BUSDTA"."Order_PriceAdj_BETA_del"."LineID"
,"BUSDTA"."Order_PriceAdj_BETA_del"."AdjustmentSeq"
FROM "BUSDTA"."Order_PriceAdj_BETA_del"
WHERE "BUSDTA"."Order_PriceAdj_BETA_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj_BETA',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_PriceAdj_BETA"
("OrderID", "LineID", "AdjustmentSeq", "AdjName", "AdjDesc", "FactorValue", "UnitPrice", "OP", "BC", "BasicDesc", "PriceAdjustmentKeyID", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderID"}, {ml r."LineID"}, {ml r."AdjustmentSeq"}, {ml r."AdjName"}, {ml r."AdjDesc"}, {ml r."FactorValue"}, {ml r."UnitPrice"}, {ml r."OP"}, {ml r."BC"}, {ml r."BasicDesc"}, {ml r."PriceAdjustmentKeyID"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj_BETA',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_PriceAdj_BETA"
SET "AdjName" = {ml r."AdjName"}, "AdjDesc" = {ml r."AdjDesc"}, "FactorValue" = {ml r."FactorValue"}, "UnitPrice" = {ml r."UnitPrice"}, "OP" = {ml r."OP"}, "BC" = {ml r."BC"}, "BasicDesc" = {ml r."BasicDesc"}, "PriceAdjustmentKeyID" = {ml r."PriceAdjustmentKeyID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"} AND "AdjustmentSeq" = {ml r."AdjustmentSeq"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_PriceAdj_BETA',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Order_PriceAdj_BETA"
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"} AND "AdjustmentSeq" = {ml r."AdjustmentSeq"}
 
'

/* upload_delete - End */

/* Table :Order_Detail_BETA */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail_BETA',
	'download_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Order_Detail_BETA"."OrderID"
,"BUSDTA"."Order_Detail_BETA"."LineID"
,"BUSDTA"."Order_Detail_BETA"."ItemId"
,"BUSDTA"."Order_Detail_BETA"."LongItem"
,"BUSDTA"."Order_Detail_BETA"."ItemLocation"
,"BUSDTA"."Order_Detail_BETA"."ItemLotNumber"
,"BUSDTA"."Order_Detail_BETA"."LineType"
,"BUSDTA"."Order_Detail_BETA"."IsNonStock"
,"BUSDTA"."Order_Detail_BETA"."OrderQty"
,"BUSDTA"."Order_Detail_BETA"."OrderUM"
,"BUSDTA"."Order_Detail_BETA"."OrderQtyInPricingUM"
,"BUSDTA"."Order_Detail_BETA"."PricingUM"
,"BUSDTA"."Order_Detail_BETA"."UnitPriceOriginalAmtInPriceUM"
,"BUSDTA"."Order_Detail_BETA"."UnitPriceAmtInPriceUoM"
,"BUSDTA"."Order_Detail_BETA"."UnitPriceAmt"
,"BUSDTA"."Order_Detail_BETA"."ExtnPriceAmt"
,"BUSDTA"."Order_Detail_BETA"."ItemSalesTaxAmt"
,"BUSDTA"."Order_Detail_BETA"."Discounted"
,"BUSDTA"."Order_Detail_BETA"."PriceOverriden"
,"BUSDTA"."Order_Detail_BETA"."PriceOverrideReasonCodeId"
,"BUSDTA"."Order_Detail_BETA"."IsTaxable"
,"BUSDTA"."Order_Detail_BETA"."ReturnHeldQty"
,"BUSDTA"."Order_Detail_BETA"."SalesCat1"
,"BUSDTA"."Order_Detail_BETA"."SalesCat2"
,"BUSDTA"."Order_Detail_BETA"."SalesCat3"
,"BUSDTA"."Order_Detail_BETA"."SalesCat4"
,"BUSDTA"."Order_Detail_BETA"."SalesCat5"
,"BUSDTA"."Order_Detail_BETA"."PurchasingCat1"
,"BUSDTA"."Order_Detail_BETA"."PurchasingCat2"
,"BUSDTA"."Order_Detail_BETA"."PurchasingCat3"
,"BUSDTA"."Order_Detail_BETA"."PurchasingCat4"
,"BUSDTA"."Order_Detail_BETA"."PurchasingCat5"
,"BUSDTA"."Order_Detail_BETA"."JDEOrderCompany"
,"BUSDTA"."Order_Detail_BETA"."JDEOrderNumber"
,"BUSDTA"."Order_Detail_BETA"."JDEOrderType"
,"BUSDTA"."Order_Detail_BETA"."JDEOrderLine"
,"BUSDTA"."Order_Detail_BETA"."JDEInvoiceCompany"
,"BUSDTA"."Order_Detail_BETA"."JDEInvoiceNumber"
,"BUSDTA"."Order_Detail_BETA"."JDEOInvoiceType"
,"BUSDTA"."Order_Detail_BETA"."JDEZBatchNumber"
,"BUSDTA"."Order_Detail_BETA"."JDEProcessID"
,"BUSDTA"."Order_Detail_BETA"."SettlementID"
,"BUSDTA"."Order_Detail_BETA"."ExtendedAmtVariance"
,"BUSDTA"."Order_Detail_BETA"."TaxAmountAmtVariance"
,"BUSDTA"."Order_Detail_BETA"."HoldCode"
,"BUSDTA"."Order_Detail_BETA"."CreatedBy"
,"BUSDTA"."Order_Detail_BETA"."CreatedDatetime"
,"BUSDTA"."Order_Detail_BETA"."UpdatedBy"
,"BUSDTA"."Order_Detail_BETA"."UpdatedDatetime"
FROM "BUSDTA"."Order_Detail_BETA"
WHERE "BUSDTA"."Order_Detail_BETA"."last_modified">= {ml s.last_table_download}
 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail_BETA',
	'download_delete_cursor',
	'sql',

'
 
SELECT "BUSDTA"."Order_Detail_BETA_del"."OrderID"
,"BUSDTA"."Order_Detail_BETA_del"."LineID"
FROM "BUSDTA"."Order_Detail_BETA_del"
WHERE "BUSDTA"."Order_Detail_BETA_del"."last_modified">= {ml s.last_table_download}
 
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail_BETA',
	'upload_insert',
	'sql',

'
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Detail_BETA"
("OrderID", "LineID", "ItemId", "LongItem", "ItemLocation", "ItemLotNumber", "LineType", "IsNonStock", "OrderQty", "OrderUM", "OrderQtyInPricingUM", "PricingUM", "UnitPriceOriginalAmtInPriceUM", "UnitPriceAmtInPriceUoM", "UnitPriceAmt", "ExtnPriceAmt", "ItemSalesTaxAmt", "Discounted", "PriceOverriden", "PriceOverrideReasonCodeId", "IsTaxable", "ReturnHeldQty", "SalesCat1", "SalesCat2", "SalesCat3", "SalesCat4", "SalesCat5", "PurchasingCat1", "PurchasingCat2", "PurchasingCat3", "PurchasingCat4", "PurchasingCat5", "JDEOrderCompany", "JDEOrderNumber", "JDEOrderType", "JDEOrderLine", "JDEInvoiceCompany", "JDEInvoiceNumber", "JDEOInvoiceType", "JDEZBatchNumber", "JDEProcessID", "SettlementID", "ExtendedAmtVariance", "TaxAmountAmtVariance", "HoldCode", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderID"}, {ml r."LineID"}, {ml r."ItemId"}, {ml r."LongItem"}, {ml r."ItemLocation"}, {ml r."ItemLotNumber"}, {ml r."LineType"}, {ml r."IsNonStock"}, {ml r."OrderQty"}, {ml r."OrderUM"}, {ml r."OrderQtyInPricingUM"}, {ml r."PricingUM"}, {ml r."UnitPriceOriginalAmtInPriceUM"}, {ml r."UnitPriceAmtInPriceUoM"}, {ml r."UnitPriceAmt"}, {ml r."ExtnPriceAmt"}, {ml r."ItemSalesTaxAmt"}, {ml r."Discounted"}, {ml r."PriceOverriden"}, {ml r."PriceOverrideReasonCodeId"}, {ml r."IsTaxable"}, {ml r."ReturnHeldQty"}, {ml r."SalesCat1"}, {ml r."SalesCat2"}, {ml r."SalesCat3"}, {ml r."SalesCat4"}, {ml r."SalesCat5"}, {ml r."PurchasingCat1"}, {ml r."PurchasingCat2"}, {ml r."PurchasingCat3"}, {ml r."PurchasingCat4"}, {ml r."PurchasingCat5"}, {ml r."JDEOrderCompany"}, {ml r."JDEOrderNumber"}, {ml r."JDEOrderType"}, {ml r."JDEOrderLine"}, {ml r."JDEInvoiceCompany"}, {ml r."JDEInvoiceNumber"}, {ml r."JDEOInvoiceType"}, {ml r."JDEZBatchNumber"}, {ml r."JDEProcessID"}, {ml r."SettlementID"}, {ml r."ExtendedAmtVariance"}, {ml r."TaxAmountAmtVariance"}, {ml r."HoldCode"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail_BETA',
	'upload_update',
	'sql',

'
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_Detail_BETA"
SET "ItemId" = {ml r."ItemId"}, "LongItem" = {ml r."LongItem"}, "ItemLocation" = {ml r."ItemLocation"}, "ItemLotNumber" = {ml r."ItemLotNumber"}, "LineType" = {ml r."LineType"}, "IsNonStock" = {ml r."IsNonStock"}, "OrderQty" = {ml r."OrderQty"}, "OrderUM" = {ml r."OrderUM"}, "OrderQtyInPricingUM" = {ml r."OrderQtyInPricingUM"}, "PricingUM" = {ml r."PricingUM"}, "UnitPriceOriginalAmtInPriceUM" = {ml r."UnitPriceOriginalAmtInPriceUM"}, "UnitPriceAmtInPriceUoM" = {ml r."UnitPriceAmtInPriceUoM"}, "UnitPriceAmt" = {ml r."UnitPriceAmt"}, "ExtnPriceAmt" = {ml r."ExtnPriceAmt"}, "ItemSalesTaxAmt" = {ml r."ItemSalesTaxAmt"}, "Discounted" = {ml r."Discounted"}, "PriceOverriden" = {ml r."PriceOverriden"}, "PriceOverrideReasonCodeId" = {ml r."PriceOverrideReasonCodeId"}, "IsTaxable" = {ml r."IsTaxable"}, "ReturnHeldQty" = {ml r."ReturnHeldQty"}, "SalesCat1" = {ml r."SalesCat1"}, "SalesCat2" = {ml r."SalesCat2"}, "SalesCat3" = {ml r."SalesCat3"}, "SalesCat4" = {ml r."SalesCat4"}, "SalesCat5" = {ml r."SalesCat5"}, "PurchasingCat1" = {ml r."PurchasingCat1"}, "PurchasingCat2" = {ml r."PurchasingCat2"}, "PurchasingCat3" = {ml r."PurchasingCat3"}, "PurchasingCat4" = {ml r."PurchasingCat4"}, "PurchasingCat5" = {ml r."PurchasingCat5"}, "JDEOrderCompany" = {ml r."JDEOrderCompany"}, "JDEOrderNumber" = {ml r."JDEOrderNumber"}, "JDEOrderType" = {ml r."JDEOrderType"}, "JDEOrderLine" = {ml r."JDEOrderLine"}, "JDEInvoiceCompany" = {ml r."JDEInvoiceCompany"}, "JDEInvoiceNumber" = {ml r."JDEInvoiceNumber"}, "JDEOInvoiceType" = {ml r."JDEOInvoiceType"}, "JDEZBatchNumber" = {ml r."JDEZBatchNumber"}, "JDEProcessID" = {ml r."JDEProcessID"}, "SettlementID" = {ml r."SettlementID"}, "ExtendedAmtVariance" = {ml r."ExtendedAmtVariance"}, "TaxAmountAmtVariance" = {ml r."TaxAmountAmtVariance"}, "HoldCode" = {ml r."HoldCode"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Detail_BETA',
	'upload_delete',
	'sql',

'
 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Order_Detail_BETA"
WHERE "OrderID" = {ml r."OrderID"} AND "LineID" = {ml r."LineID"}
 
'

/* upload_delete - End */

/* Table :InventoryTmp */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/*  - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'InventoryTmp',
	'',
	'sql',

'
--{ml_ignore}
'

/*  - End */

/* Table :MasterNotification */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MasterNotification',
	'download_cursor',
	'sql',

'

SELECT "BUSDTA"."MasterNotification"."MasterNotificationID"
,"BUSDTA"."MasterNotification"."RouteID"
,"BUSDTA"."MasterNotification"."Title"
,"BUSDTA"."MasterNotification"."Description"
,"BUSDTA"."MasterNotification"."TypeID"
,"BUSDTA"."MasterNotification"."TransactionID"
,"BUSDTA"."MasterNotification"."Status"
,"BUSDTA"."MasterNotification"."Category"
,"BUSDTA"."MasterNotification"."Read"
,"BUSDTA"."MasterNotification"."ValidTill"
,"BUSDTA"."MasterNotification"."Acknowledged"
,"BUSDTA"."MasterNotification"."Initiator"
,"BUSDTA"."MasterNotification"."ResponseType"
,"BUSDTA"."MasterNotification"."ResponseValue"
,"BUSDTA"."MasterNotification"."CreatedBy"
,"BUSDTA"."MasterNotification"."CreatedDatetime"
,"BUSDTA"."MasterNotification"."UpdatedBy"
,"BUSDTA"."MasterNotification"."UpdatedDatetime"
FROM "BUSDTA"."MasterNotification"
WHERE "BUSDTA"."MasterNotification"."last_modified">= {ml s.last_table_download}
AND RouteID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MasterNotification',
	'download_delete_cursor',
	'sql',

'


SELECT "BUSDTA"."MasterNotification_del"."MasterNotificationID"
,"BUSDTA"."MasterNotification_del"."RouteID"
FROM "BUSDTA"."MasterNotification_del"
WHERE "BUSDTA"."MasterNotification_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MasterNotification',
	'upload_insert',
	'sql',

'


/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."MasterNotification"
("MasterNotificationID", "RouteID", "Title", "Description", "TypeID", "TransactionID", "Status", "Category", "Read", "ValidTill", "Acknowledged", "Initiator", "ResponseType", "ResponseValue", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."MasterNotificationID"}, {ml r."RouteID"}, {ml r."Title"}, {ml r."Description"}, {ml r."TypeID"}, {ml r."TransactionID"}, {ml r."Status"}, {ml r."Category"}, {ml r."Read"}, {ml r."ValidTill"}, {ml r."Acknowledged"}, {ml r."Initiator"}, {ml r."ResponseType"}, {ml r."ResponseValue"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MasterNotification',
	'upload_update',
	'sql',

'


/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."MasterNotification"
SET "Title" = {ml r."Title"}, "Description" = {ml r."Description"}, "TypeID" = {ml r."TypeID"}, "TransactionID" = {ml r."TransactionID"}, "Status" = {ml r."Status"}, "Category" = {ml r."Category"}, "Read" = {ml r."Read"}, "ValidTill" = {ml r."ValidTill"}, "Acknowledged" = {ml r."Acknowledged"}, "Initiator" = {ml r."Initiator"}, "ResponseType" = {ml r."ResponseType"}, "ResponseValue" = {ml r."ResponseValue"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "MasterNotificationID" = {ml r."MasterNotificationID"} AND "RouteID" = {ml r."RouteID"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'MasterNotification',
	'upload_delete',
	'sql',

'



/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."MasterNotification"
WHERE "MasterNotificationID" = {ml r."MasterNotificationID"} AND "RouteID" = {ml r."RouteID"}
'

/* upload_delete - End */

/* Table :StatusMaster */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'StatusMaster',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."StatusMaster"."StatusId"
,"BUSDTA"."StatusMaster"."StatusCode"
,"BUSDTA"."StatusMaster"."StatusDescription"
,"BUSDTA"."StatusMaster"."StatusForType"
,"BUSDTA"."StatusMaster"."CreatedBy"
,"BUSDTA"."StatusMaster"."CreatedDatetime"
,"BUSDTA"."StatusMaster"."UpdatedBy"
,"BUSDTA"."StatusMaster"."UpdatedDatetime"
FROM "BUSDTA"."StatusMaster"
WHERE "BUSDTA"."StatusMaster"."last_modified">= {ml s.last_table_download}
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'StatusMaster',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."StatusMaster_del"."StatusId"
FROM "BUSDTA"."StatusMaster_del"
WHERE "BUSDTA"."StatusMaster_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :Order_Templates */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Templates',
	'download_cursor',
	'sql',

'


SELECT "BUSDTA"."Order_Templates"."TemplateType"
,"BUSDTA"."Order_Templates"."ShipToID"
,"BUSDTA"."Order_Templates"."TemplateDetailID"
,"BUSDTA"."Order_Templates"."DisplaySeq"
,"BUSDTA"."Order_Templates"."ItemID"
,"BUSDTA"."Order_Templates"."LongItem"
,"BUSDTA"."Order_Templates"."TemplateQuantity"
,"BUSDTA"."Order_Templates"."TemplateUoM" FROM BUSDTA.Order_Templates 
	join BUSDTA.Customer_Route_Map crm on ShipToID=crm.CustomerShipToNumber and crm.RelationshipType=''SHAN''
	join BUSDTA.Route_Master rm ON crm.BranchNumber=rm.BranchNumber
		WHERE RouteName ={ml s.username} and (BUSDTA.Order_Templates.last_modified>= {ml s.last_table_download} or crm.last_modified >= {ml s.last_table_download})
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Templates',
	'download_delete_cursor',
	'sql',

'



SELECT "BUSDTA"."Order_Templates_del"."TemplateType"
,"BUSDTA"."Order_Templates_del"."ShipToID"
,"BUSDTA"."Order_Templates_del"."TemplateDetailID"
FROM BUSDTA.Order_Templates_del	
	join BUSDTA.Customer_Route_Map crm on ShipToID=crm.CustomerShipToNumber and crm.RelationshipType=''SHAN''
	join BUSDTA.Route_Master rm on crm.BranchNumber=rm.BranchNumber
		where RouteName ={ml s.username} and (BUSDTA.Order_Templates_Del.last_modified>= {ml s.last_table_download} or crm.last_modified >= {ml s.last_table_download})
		
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Templates',
	'upload_insert',
	'sql',

'

/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Templates"
("TemplateType", "ShipToID", "TemplateDetailID", "DisplaySeq", "ItemID", "LongItem", "TemplateQuantity", "TemplateUoM")
VALUES({ml r."TemplateType"}, {ml r."ShipToID"}, {ml r."TemplateDetailID"}, {ml r."DisplaySeq"}, {ml r."ItemID"}, {ml r."LongItem"}, {ml r."TemplateQuantity"}, {ml r."TemplateUoM"})
 
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Templates',
	'upload_update',
	'sql',

'
 

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_Templates"
SET "DisplaySeq" = {ml r."DisplaySeq"}, "ItemID" = {ml r."ItemID"}, "LongItem" = {ml r."LongItem"}, "TemplateQuantity" = {ml r."TemplateQuantity"}, "TemplateUoM" = {ml r."TemplateUoM"}
WHERE "TemplateType" = {ml r."TemplateType"} AND "ShipToID" = {ml r."ShipToID"} AND "TemplateDetailID" = {ml r."TemplateDetailID"}
 
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'Order_Templates',
	'upload_delete',
	'sql',

'


/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."Order_Templates"
WHERE "TemplateType" = {ml r."TemplateType"} AND "ShipToID" = {ml r."ShipToID"} AND "TemplateDetailID" = {ml r."TemplateDetailID"}
 
'

/* upload_delete - End */

/* Table :F0101_Audit */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101_Audit',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."F0101_Audit"."TransID"
,"BUSDTA"."F0101_Audit"."TransType"
,"BUSDTA"."F0101_Audit"."ABAN8"
,"BUSDTA"."F0101_Audit"."ABALKY"
,"BUSDTA"."F0101_Audit"."ABTAX"
,"BUSDTA"."F0101_Audit"."ABALPH"
,"BUSDTA"."F0101_Audit"."ABMCU"
,"BUSDTA"."F0101_Audit"."ABSIC"
,"BUSDTA"."F0101_Audit"."ABLNGP"
,"BUSDTA"."F0101_Audit"."ABAT1"
,"BUSDTA"."F0101_Audit"."ABCM"
,"BUSDTA"."F0101_Audit"."ABTAXC"
,"BUSDTA"."F0101_Audit"."ABAT2"
,"BUSDTA"."F0101_Audit"."ABAN81"
,"BUSDTA"."F0101_Audit"."ABAN82"
,"BUSDTA"."F0101_Audit"."ABAN83"
,"BUSDTA"."F0101_Audit"."ABAN84"
,"BUSDTA"."F0101_Audit"."ABAN86"
,"BUSDTA"."F0101_Audit"."ABAN85"
,"BUSDTA"."F0101_Audit"."ABAC01"
,"BUSDTA"."F0101_Audit"."ABAC02"
,"BUSDTA"."F0101_Audit"."ABAC03"
,"BUSDTA"."F0101_Audit"."ABAC04"
,"BUSDTA"."F0101_Audit"."ABAC05"
,"BUSDTA"."F0101_Audit"."ABAC06"
,"BUSDTA"."F0101_Audit"."ABAC07"
,"BUSDTA"."F0101_Audit"."ABAC08"
,"BUSDTA"."F0101_Audit"."ABAC09"
,"BUSDTA"."F0101_Audit"."ABAC10"
,"BUSDTA"."F0101_Audit"."ABAC11"
,"BUSDTA"."F0101_Audit"."ABAC12"
,"BUSDTA"."F0101_Audit"."ABAC13"
,"BUSDTA"."F0101_Audit"."ABAC14"
,"BUSDTA"."F0101_Audit"."ABAC15"
,"BUSDTA"."F0101_Audit"."ABAC16"
,"BUSDTA"."F0101_Audit"."ABAC17"
,"BUSDTA"."F0101_Audit"."ABAC18"
,"BUSDTA"."F0101_Audit"."ABAC19"
,"BUSDTA"."F0101_Audit"."ABAC20"
,"BUSDTA"."F0101_Audit"."ABAC21"
,"BUSDTA"."F0101_Audit"."ABAC22"
,"BUSDTA"."F0101_Audit"."ABAC23"
,"BUSDTA"."F0101_Audit"."ABAC24"
,"BUSDTA"."F0101_Audit"."ABAC25"
,"BUSDTA"."F0101_Audit"."ABAC26"
,"BUSDTA"."F0101_Audit"."ABAC27"
,"BUSDTA"."F0101_Audit"."ABAC28"
,"BUSDTA"."F0101_Audit"."ABAC29"
,"BUSDTA"."F0101_Audit"."ABAC30"
,"BUSDTA"."F0101_Audit"."ABRMK"
,"BUSDTA"."F0101_Audit"."ABTXCT"
,"BUSDTA"."F0101_Audit"."ABTX2"
,"BUSDTA"."F0101_Audit"."ABALP1"
,"BUSDTA"."F0101_Audit"."WSValidation"
,"BUSDTA"."F0101_Audit"."Status"
,"BUSDTA"."F0101_Audit"."RejReason"
,"BUSDTA"."F0101_Audit"."SyncID"
,"BUSDTA"."F0101_Audit"."UpdatedDateTime"
FROM "BUSDTA"."F0101_Audit", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
WHERE ("BUSDTA"."F0101_Audit"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
or crm.last_modified >= {ml s.last_table_download}) and   ABAN8=crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber  
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username})  
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101_Audit',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."F0101_Audit_del"."TransID", "BUSDTA"."F0101_Audit_del"."ABAN8"
FROM "BUSDTA"."F0101_Audit_del"
WHERE "BUSDTA"."F0101_Audit_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101_Audit',
	'upload_insert',
	'sql',

'

 /* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0101_Audit"
("TransID", "TransType", "ABAN8", "ABALKY", "ABTAX", "ABALPH", "ABMCU", "ABSIC", "ABLNGP", "ABAT1", "ABCM", "ABTAXC", "ABAT2", "ABAN81", "ABAN82", "ABAN83", "ABAN84", "ABAN86", "ABAN85", "ABAC01", "ABAC02", "ABAC03", "ABAC04", "ABAC05", "ABAC06", "ABAC07", "ABAC08", "ABAC09", "ABAC10", "ABAC11", "ABAC12", "ABAC13", "ABAC14", "ABAC15", "ABAC16", "ABAC17", "ABAC18", "ABAC19", "ABAC20", "ABAC21", "ABAC22", "ABAC23", "ABAC24", "ABAC25", "ABAC26", "ABAC27", "ABAC28", "ABAC29", "ABAC30", "ABRMK", "ABTXCT", "ABTX2", "ABALP1", "WSValidation", "Status", "RejReason", "SyncID","UpdatedDateTime")
VALUES({ml r."TransID"}, {ml r."TransType"}, {ml r."ABAN8"}, {ml r."ABALKY"}, {ml r."ABTAX"}, {ml r."ABALPH"}, {ml r."ABMCU"}, {ml r."ABSIC"}, {ml r."ABLNGP"}, {ml r."ABAT1"}, {ml r."ABCM"}, {ml r."ABTAXC"}, {ml r."ABAT2"}, {ml r."ABAN81"}, {ml r."ABAN82"}, {ml r."ABAN83"}, {ml r."ABAN84"}, {ml r."ABAN86"}, {ml r."ABAN85"}, {ml r."ABAC01"}, {ml r."ABAC02"}, {ml r."ABAC03"}, {ml r."ABAC04"}, {ml r."ABAC05"}, {ml r."ABAC06"}, {ml r."ABAC07"}, {ml r."ABAC08"}, {ml r."ABAC09"}, {ml r."ABAC10"}, {ml r."ABAC11"}, {ml r."ABAC12"}, {ml r."ABAC13"}, {ml r."ABAC14"}, {ml r."ABAC15"}, {ml r."ABAC16"}, {ml r."ABAC17"}, {ml r."ABAC18"}, {ml r."ABAC19"}, {ml r."ABAC20"}, {ml r."ABAC21"}, {ml r."ABAC22"}, {ml r."ABAC23"}, {ml r."ABAC24"}, {ml r."ABAC25"}, {ml r."ABAC26"}, {ml r."ABAC27"}, {ml r."ABAC28"}, {ml r."ABAC29"}, {ml r."ABAC30"}, {ml r."ABRMK"}, {ml r."ABTXCT"}, {ml r."ABTX2"}, {ml r."ABALP1"}, {ml r."WSValidation"}, {ml r."Status"}, {ml r."RejReason"}, {ml r."SyncID"},{ml r."UpdatedDateTime"})



'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101_Audit',
	'upload_update',
	'sql',

'

 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0101_Audit"
SET "TransType" = {ml r."TransType"}, "ABALKY" = {ml r."ABALKY"}, "ABTAX" = {ml r."ABTAX"}, "ABALPH" = {ml r."ABALPH"}, "ABMCU" = {ml r."ABMCU"}, "ABSIC" = {ml r."ABSIC"}, "ABLNGP" = {ml r."ABLNGP"}, "ABAT1" = {ml r."ABAT1"}, "ABCM" = {ml r."ABCM"}, "ABTAXC" = {ml r."ABTAXC"}, "ABAT2" = {ml r."ABAT2"}, "ABAN81" = {ml r."ABAN81"}, "ABAN82" = {ml r."ABAN82"}, "ABAN83" = {ml r."ABAN83"}, "ABAN84" = {ml r."ABAN84"}, "ABAN86" = {ml r."ABAN86"}, "ABAN85" = {ml r."ABAN85"}, "ABAC01" = {ml r."ABAC01"}, "ABAC02" = {ml r."ABAC02"}, "ABAC03" = {ml r."ABAC03"}, "ABAC04" = {ml r."ABAC04"}, "ABAC05" = {ml r."ABAC05"}, "ABAC06" = {ml r."ABAC06"}, "ABAC07" = {ml r."ABAC07"}, "ABAC08" = {ml r."ABAC08"}, "ABAC09" = {ml r."ABAC09"}, "ABAC10" = {ml r."ABAC10"}, "ABAC11" = {ml r."ABAC11"}, "ABAC12" = {ml r."ABAC12"}, "ABAC13" = {ml r."ABAC13"}, "ABAC14" = {ml r."ABAC14"}, "ABAC15" = {ml r."ABAC15"}, "ABAC16" = {ml r."ABAC16"}, "ABAC17" = {ml r."ABAC17"}, "ABAC18" = {ml r."ABAC18"}, "ABAC19" = {ml r."ABAC19"}, "ABAC20" = {ml r."ABAC20"}, "ABAC21" = {ml r."ABAC21"}, "ABAC22" = {ml r."ABAC22"}, "ABAC23" = {ml r."ABAC23"}, "ABAC24" = {ml r."ABAC24"}, "ABAC25" = {ml r."ABAC25"}, "ABAC26" = {ml r."ABAC26"}, "ABAC27" = {ml r."ABAC27"}, "ABAC28" = {ml r."ABAC28"}, "ABAC29" = {ml r."ABAC29"}, "ABAC30" = {ml r."ABAC30"}, "ABRMK" = {ml r."ABRMK"}, "ABTXCT" = {ml r."ABTXCT"}, "ABTX2" = {ml r."ABTX2"}, "ABALP1" = {ml r."ABALP1"}, "WSValidation" = {ml r."WSValidation"}, "Status" = {ml r."Status"}, "RejReason" = {ml r."RejReason"}, "SyncID" = {ml r."SyncID"} ,"UpdatedDateTime" = {ml r."UpdatedDateTime"}
WHERE "TransID" = {ml r."TransID"} AND "ABAN8" = {ml r."ABAN8"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0101_Audit',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0101_Audit"
WHERE "TransID" = {ml r."TransID"} AND "ABAN8" = {ml r."ABAN8"}

'

/* upload_delete - End */

/* Table :F0116_Audit */
/* Version : SLE_SyncScript_PYD.1.3.0 */


/* download_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116_Audit',
	'download_cursor',
	'sql',

'
SELECT "BUSDTA"."F0116_Audit"."TransID"
,"BUSDTA"."F0116_Audit"."TransType"
,"BUSDTA"."F0116_Audit"."ALAN8"
,"BUSDTA"."F0116_Audit"."ALEFTB"
,"BUSDTA"."F0116_Audit"."ALEFTF"
,"BUSDTA"."F0116_Audit"."ALADD1"
,"BUSDTA"."F0116_Audit"."ALADD2"
,"BUSDTA"."F0116_Audit"."ALADD3"
,"BUSDTA"."F0116_Audit"."ALADD4"
,"BUSDTA"."F0116_Audit"."ALADDZ"
,"BUSDTA"."F0116_Audit"."ALCTY1"
,"BUSDTA"."F0116_Audit"."ALCOUN"
,"BUSDTA"."F0116_Audit"."ALADDS"
,"BUSDTA"."F0116_Audit"."WSValidation"
,"BUSDTA"."F0116_Audit"."Status"
,"BUSDTA"."F0116_Audit"."RejReason"
,"BUSDTA"."F0116_Audit"."SyncID"
,"BUSDTA"."F0116_Audit"."UpdatedDateTime"
FROM "BUSDTA"."F0116_Audit", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
WHERE ("BUSDTA"."F0116_Audit"."last_modified" >= {ml s.last_table_download} or rm."last_modified" >= {ml s.last_table_download}  
or crm."last_modified" >= {ml s.last_table_download})   AND ALAN8=crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber  
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} ) 
'

/* download_cursor - End */


/* download_delete_cursor - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116_Audit',
	'download_delete_cursor',
	'sql',

'

SELECT "BUSDTA"."F0116_Audit_del"."TransID","BUSDTA"."F0116_Audit_del"."ALAN8"
,"BUSDTA"."F0116_Audit_del"."ALEFTB"
FROM "BUSDTA"."F0116_Audit_del"
WHERE "BUSDTA"."F0116_Audit_del"."last_modified">= {ml s.last_table_download}
'

/* download_delete_cursor - End */


/* upload_insert - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116_Audit',
	'upload_insert',
	'sql',

'


/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."F0116_Audit"
("TransID", "TransType", "ALAN8", "ALEFTB", "ALEFTF", "ALADD1", "ALADD2", "ALADD3", "ALADD4", "ALADDZ", "ALCTY1", "ALCOUN", "ALADDS", "WSValidation", "Status", "RejReason", "SyncID","UpdatedDateTime")
VALUES({ml r."TransID"}, {ml r."TransType"}, {ml r."ALAN8"}, {ml r."ALEFTB"}, {ml r."ALEFTF"}, {ml r."ALADD1"}, {ml r."ALADD2"}, {ml r."ALADD3"}, {ml r."ALADD4"}, {ml r."ALADDZ"}, {ml r."ALCTY1"}, {ml r."ALCOUN"}, {ml r."ALADDS"}, {ml r."WSValidation"}, {ml r."Status"}, {ml r."RejReason"}, {ml r."SyncID"}
,{ml r."UpdatedDateTime"})
'

/* upload_insert - End */


/* upload_update - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116_Audit',
	'upload_update',
	'sql',

'


/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0116_Audit"
SET "TransType" = {ml r."TransType"}, "ALEFTF" = {ml r."ALEFTF"}, "ALADD1" = {ml r."ALADD1"}, "ALADD2" = {ml r."ALADD2"}, "ALADD3" = {ml r."ALADD3"}, "ALADD4" = {ml r."ALADD4"}, "ALADDZ" = {ml r."ALADDZ"}, "ALCTY1" = {ml r."ALCTY1"}, "ALCOUN" = {ml r."ALCOUN"}, "ALADDS" = {ml r."ALADDS"}, "WSValidation" = {ml r."WSValidation"}, "Status" = {ml r."Status"}, "RejReason" = {ml r."RejReason"}, "SyncID" = {ml r."SyncID"},"UpdatedDateTime" = {ml r."UpdatedDateTime"}
WHERE "TransID" = {ml r."TransID"} AND "ALAN8" = {ml r."ALAN8"} AND "ALEFTB" = {ml r."ALEFTB"}
'

/* upload_update - End */


/* upload_delete - Start */
EXEC ml_add_lang_table_script
    'SLE_SyncScript_PYD.1.3.0',
	'F0116_Audit',
	'upload_delete',
	'sql',

'

 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."F0116_Audit"
WHERE "TransID" = {ml r."TransID"} AND "ALAN8" = {ml r."ALAN8"} AND "ALEFTB" = {ml r."ALEFTB"}
 

'

/* upload_delete - End */
