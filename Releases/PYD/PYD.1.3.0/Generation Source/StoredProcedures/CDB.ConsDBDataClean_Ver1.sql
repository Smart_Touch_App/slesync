
CREATE procedure [BUSDTA].[ConsDBDataClean] as 
   BEGIN
	   truncate table BUSDTA.Order_Header;
	   truncate table BUSDTA.Order_Header_del;
	   truncate table BUSDTA.Order_Detail;
	   truncate table BUSDTA.Order_Detail_del;
	   truncate table BUSDTA.F4015_del;
	   truncate table BUSDTA.PickOrder;
	   truncate table BUSDTA.PickOrder_del;
	   truncate table BUSDTA.PickOrder_Exception;
	   truncate table BUSDTA.PickOrder_Exception_del;
	   truncate table BUSDTA.M4016; -- preorder
	   truncate table BUSDTA.M4016_del;
	   truncate table BUSDTA.M0112; -- Notes Details
	   truncate table BUSDTA.M0112_del;
	   truncate table BUSDTA.M03011; -- Customer Transaction Summary
	   truncate table BUSDTA.M03011_del;
	   truncate table BUSDTA.M03042; -- Customer Payment Detail
	   truncate table BUSDTA.M03042_del;
	   truncate table BUSDTA.M50012; -- Transaction Activity Detail
	   truncate table BUSDTA.M50012_del;
	   truncate table BUSDTA.M50052; -- Sync Detail
	   truncate table BUSDTA.M56M0004;
	   truncate table BUSDTA.M56M0004_del;
	   truncate table BUSDTA.Pretrip_Inspection_Header;
	   truncate table BUSDTA.Pretrip_Inspection_Header_del;
	   truncate table BUSDTA.Pretrip_Inspection_Detail;
	   truncate table BUSDTA.Pretrip_Inspection_Detail_del;
	   truncate table BUSDTA.Check_Verification_Detail;
	   truncate table BUSDTA.Check_Verification_Detail_del;
	   truncate table BUSDTA.Cash_Verification_Detail;
	   truncate table BUSDTA.Cash_Verification_Detail_del;
	   truncate table BUSDTA.Moneyorder_Verification_Detail;
	   truncate table BUSDTA.Moneyorder_Verification_Detail_del;
	   truncate table BUSDTA.MoneyOrderDetails;
	   truncate table BUSDTA.MoneyOrderDetails_del;
	   truncate table BUSDTA.Route_Settlement;
	   truncate table BUSDTA.Route_Settlement_del;
	   truncate table BUSDTA.Route_Settlement_Detail;
	   truncate table BUSDTA.Route_Settlement_Detail_del;
	   truncate table BUSDTA.ExpenseDetails;
	   truncate table BUSDTA.ExpenseDetails_del;
	   truncate table BUSDTA.Inventory_Adjustment;
	   truncate table BUSDTA.Inventory_Adjustment_del;
	   truncate table BUSDTA.Inventory_Ledger;
	   truncate table BUSDTA.Inventory_Ledger_del;
	   truncate table BUSDTA.Invoice_Header;
	   truncate table BUSDTA.Invoice_Header_del;
	   truncate table BUSDTA.Customer_Ledger;
	   truncate table BUSDTA.Customer_Ledger_del;
	   truncate table BUSDTA.Receipt_Header;
	   truncate table BUSDTA.Receipt_Header_del;
	   truncate table BUSDTA.Cycle_Count_Header;
	   truncate table BUSDTA.Cycle_Count_Header_del;
	   truncate table BUSDTA.Cycle_Count_Detail;
	   truncate table BUSDTA.Cycle_Count_Detail_del;
	   truncate table BUSDTA.Pick_Detail;
	   truncate table BUSDTA.Pick_Detail_del;
	   truncate table BUSDTA.Route_Replenishment_Header;
	   truncate table BUSDTA.Route_Replenishment_Header_del;
	   truncate table BUSDTA.Route_Replenishment_Detail;
	   truncate table BUSDTA.Route_Replenishment_Detail_del;
	   truncate table BUSDTA.SalesOrder_ReturnOrder_Mapping;
	   truncate table BUSDTA.SalesOrder_ReturnOrder_Mapping_del;
	   truncate table busdta.notification;
	   truncate table busdta.notification_del;
	   truncate table BUSDTA.Customer_Quote_Header;
	   truncate table BUSDTA.Customer_Quote_Header_del;
	   truncate table BUSDTA.Customer_Quote_Detail;
	   truncate table BUSDTA.Customer_Quote_Detail_del;
	   truncate table busdta.prospect_note;
	   truncate table busdta.prospect_note_del;
	   truncate table busdta.prospect_quote_header;
	   truncate table busdta.prospect_quote_header_del;
	   truncate table busdta.prospect_quote_detail;
	   truncate table busdta.prospect_quote_detail_del;
	   truncate table BUSDTA.ApprovalCodeLog;
	   truncate table BUSDTA.ApprovalCodeLog_del;
	   /* Rebuild Inventory Table - Changed to pull from JDEProdConversionRef*/
	   truncate table busdta.inventory;
	   truncate table busdta.inventory_del; 
	   insert into busdta.inventory (ItemId,ItemNumber,RouteId,OnHandQuantity,CommittedQuantity,HeldQuantity,
		ParLevel,LastReceiptDate,LastConsumeDate,CreatedBy,CreatedDatetime,UpdatedBy,UpdatedDatetime) 
		select ItemId,IBLITM,RouteMasterID,0,0,0,0,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP,13,CURRENT_TIMESTAMP,13,CURRENT_TIMESTAMP from busdta.Route_Master A join busdta.F4102 B on LTRIM(BranchNumber)=LTRIM(IBMCU)
		join busdta.ItemConfiguration C on IBITM=ItemId and RouteEnabled=1;
		IF OBJECT_ID('busdta.InventoryTmp', 'U') IS NOT NULL
			DROP TABLE busdta.InventoryTmp; 
		select A.ItemID,A.RouteID,C.LWPQOH*.0001 as OnHand into busdta.InventoryTmp from busdta.inventory A join busdta.Route_Master B on A.RouteId=B.RouteMasterID 
			join  JDEProdConversionRef.dbo.F41021Ref C on A.ItemId=LWITM and LTRIM(B.BranchNumber)=LTRIM(C.LWMCU);
		update A set A.OnHandQuantity=B.OnHand from busdta.inventory A join busdta.InventoryTmp B
			on A.ItemId=B.ItemId and A.RouteId=B.RouteId;  
		update A Set A.ParLevel = ROUND(C.IBROPI*.0001,0) from busdta.inventory A join busdta.Route_Master B on A.RouteId=B.RouteMasterID 
			join  JDEProdConversionRef.dbo.F4102Ref C on A.ItemId=IBITM and LTRIM(B.BranchNumber)=LTRIM(C.IBMCU)
				where IBROPI>0 and ROUND(C.IBROPI*.0001,0)<9999;
   END
 GO