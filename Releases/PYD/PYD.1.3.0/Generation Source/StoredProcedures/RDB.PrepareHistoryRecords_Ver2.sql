
/*------------------------------------------------------------------------------
*             Procedure to pull history records for given customer. 
*	This procedure will show the history for orders created using SLE Application.
*-----------------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE "BUSDTA"."prepareHistoryRecords"( @shipTo varchar(25) )
begin
  declare "i" nvarchar(2);
  declare "j" nvarchar(2);
  declare "countHist" integer;
  declare "strQuery" nvarchar(20000);
  drop table if exists "busdta"."localLastOrders";
 
select "NUMBER"() as "chronology",'00800', OrderID, OrderTypeId, OrderDate into "busdta"."localLastOrders" from BUSDTA.Order_Header
where CustShipToId = @shipTo and
OrderTypeId = (select StatusTypeId from busdta.Status_Type where StatusTypeCD = 'SALORD') and
OrderStateId = (select StatusTypeId from busdta.Status_Type where StatusTypeCd = 'CMTD')
order by OrderDate desc;
   
  /* Order Item Last */
  drop table if exists "busdta"."localLastItems";
   select "NUMBER"() as "ranking","ItemId" into "busdta"."localLastItems"
    from "busdta"."localLastOrders" llo join "busdta"."Order_Detail" od on llo.OrderID = od.OrderID
    where "chronology" <= 10  and ISNULL(od.OrderQty,0)<>0 group by "ItemId" order by "Count"() desc,"ItemId" asc;
  /* Order History Panel Data */
  drop table if exists "busdta"."localOrderHistoryPanel";
  select "count"() into "countHist" from "busdta"."localLastOrders";
  if "countHist" <> null or "countHist" <> '' then
    // To limit the procedure to generate only 10 history records. The value will of countHist will also affect the size of 'strQuery'
    if "countHist" > 10 then
      set "countHist" = 10
    end if;
    set "strQuery" = 'select CAST(im.imlitm AS VARCHAR(20)) as ItemCode,im.imdsc1 AS ItemDesc
 , ';
    set "i" = 1;
    while "i" <= "countHist" loop
      set "strQuery" = "strQuery"+'h'+"i"+'d.OrderQty as H'+"i"+'_Qty';
      if "i" <> "countHist" then
        set "strQuery" = "strQuery"+' , '
      end if;
      set "i" = "i"+1
    end loop;
    set "strQuery" = "strQuery"+' into busdta.localOrderHistoryPanel from busdta.localLastItems li ';
    set "strQuery" = "strQuery"+'join busdta.F4101 im on li.ItemId=im.IMITM  ';
    set "j" = 1;
    while "j" <= "countHist" loop
   set strQuery = strQuery+' join busdta.localLastOrders h'+j+' on h'+j+'.chronology='+j ;
   set strQuery = strQuery+' left join busdta.Order_Detail h'+j+'d on h'+j+'.OrderID=h'+j+'d.OrderID and im.imitm=h'+j+'d.ItemId ';
      set "j" = "j"+1
    end loop;
    set "strQuery" = convert(varchar,"strQuery" || ' Order By ranking;')
  else
     set strQuery = 'drop table if exists "busdta"."localOrderHistoryPanel";create table busdta.localOrderHistoryPanel  (ItemCode varchar(20), ItemDesc nvarchar, H1_Qty numeric(8,0));';
  end if;
  execute immediate "strQuery";
end

GO