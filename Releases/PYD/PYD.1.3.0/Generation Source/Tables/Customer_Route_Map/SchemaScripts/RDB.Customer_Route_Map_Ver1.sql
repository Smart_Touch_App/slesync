
/* [BUSDTA].[Customer_Route_Map] - begins */

/* TableDDL - [BUSDTA].[Customer_Route_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Route_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Route_Map]
	(
	  [BranchNumber] NVARCHAR (12) NOT NULL
	, [CustomerShipToNumber] NUMERIC(8,0) NOT NULL
	, [RelationshipType] NVARCHAR (10) NOT NULL
	, [RelatedAddressBookNumber] NUMERIC(8,0) NULL
	, [IsActive] NVARCHAR (10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())	
	, PRIMARY KEY ([BranchNumber] ASC, [CustomerShipToNumber] ASC, [RelationshipType] ASC)
	)
END
/* TableDDL - [BUSDTA].[Customer_Route_Map] - End */

