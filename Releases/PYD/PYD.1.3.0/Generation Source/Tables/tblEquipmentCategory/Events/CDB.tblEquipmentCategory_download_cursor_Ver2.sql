 
SELECT "BUSDTA"."tblEquipmentCategory"."EquipmentCategoryID"
,"BUSDTA"."tblEquipmentCategory"."EquipmentCategory"
,"BUSDTA"."tblEquipmentCategory"."EquipmentTypeID"
,"BUSDTA"."tblEquipmentCategory"."CreatedBy"
,"BUSDTA"."tblEquipmentCategory"."CreatedDatetime"
,"BUSDTA"."tblEquipmentCategory"."UpdatedBy"
,"BUSDTA"."tblEquipmentCategory"."UpdatedDatetime"

FROM "BUSDTA"."tblEquipmentCategory"
WHERE "BUSDTA"."tblEquipmentCategory"."last_modified">= {ml s.last_table_download}

