 
SELECT "BUSDTA"."Request_Authorization_Format"."RequestFormatCode"
,"BUSDTA"."Request_Authorization_Format"."AuthorizationFormatCode"
,"BUSDTA"."Request_Authorization_Format"."Key1"
,"BUSDTA"."Request_Authorization_Format"."Key2"
,"BUSDTA"."Request_Authorization_Format"."Key3"
,"BUSDTA"."Request_Authorization_Format"."RouteId"
,"BUSDTA"."Request_Authorization_Format"."CreatedBy"
,"BUSDTA"."Request_Authorization_Format"."CreatedDatetime"
,"BUSDTA"."Request_Authorization_Format"."UpdatedBy"
,"BUSDTA"."Request_Authorization_Format"."UpdatedDatetime"

FROM "BUSDTA"."Request_Authorization_Format"
WHERE "BUSDTA"."Request_Authorization_Format"."last_modified">= {ml s.last_table_download}
