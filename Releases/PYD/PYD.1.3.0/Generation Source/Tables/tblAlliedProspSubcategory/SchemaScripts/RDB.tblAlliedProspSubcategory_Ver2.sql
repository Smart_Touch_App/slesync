/* [BUSDTA].[tblAlliedProspSubcategory] - begins */

/* TableDDL - [BUSDTA].[tblAlliedProspSubcategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspSubcategory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspSubcategory]
	(
	  [SubCategoryID] INTEGER NOT NULL 
	, [SubCategory] VARCHAR(500) NULL
	, [CategoryID] INTEGER NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([SubCategoryID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblAlliedProspSubcategory] - End */
