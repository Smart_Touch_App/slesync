/* [BUSDTA].[tblPacketSize] - begins */

/* TableDDL - [BUSDTA].[tblPacketSize] - Start */
IF OBJECT_ID('[BUSDTA].[tblPacketSize]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblPacketSize]
	(
	  [PacketSizeID] INT NOT NULL IDENTITY(1,1)
	, [Size] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLPACKETSIZE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblPacketSize] PRIMARY KEY ([PacketSizeID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblPacketSize] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[tblPacketSize] - Start */
IF OBJECT_ID('[BUSDTA].[tblPacketSize_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblPacketSize_del]
	(
	  [PacketSizeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PacketSizeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblPacketSize] - End */
/* TRIGGERS FOR tblPacketSize - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblPacketSize_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblPacketSize_ins
	ON BUSDTA.tblPacketSize AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblPacketSize_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblPacketSize_del.PacketSizeID= inserted.PacketSizeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblPacketSize_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblPacketSize_upd
	ON BUSDTA.tblPacketSize AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblPacketSize
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblPacketSize.PacketSizeID= inserted.PacketSizeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblPacketSize_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblPacketSize_dlt
	ON BUSDTA.tblPacketSize AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblPacketSize_del (PacketSizeID, last_modified )
	SELECT deleted.PacketSizeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblPacketSize - END */
