

/* [BUSDTA].[F0101_Audit] - begins */

/* TableDDL - [BUSDTA].[F0101_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0101_Audit]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0101_Audit]	(	  [TransID] NUMERIC(8,0) NULL	, [TransType] VARCHAR(50) NULL	, [ABAN8] NUMERIC(8,0) NOT NULL	, [ABALKY] NCHAR(20) NULL	, [ABTAX] NCHAR(20) NULL	, [ABALPH] NCHAR(40) NULL	, [ABMCU] NCHAR(12) NULL	, [ABSIC] NCHAR(10) NULL	, [ABLNGP] NCHAR(2) NULL	, [ABAT1] NCHAR(3) NULL	, [ABCM] NCHAR(2) NULL	, [ABTAXC] NCHAR(1) NULL	, [ABAT2] NCHAR(1) NULL	, [ABAN81] FLOAT NULL	, [ABAN82] FLOAT NULL	, [ABAN83] FLOAT NULL	, [ABAN84] FLOAT NULL	, [ABAN86] FLOAT NULL	, [ABAN85] FLOAT NULL	, [ABAC01] NCHAR(3) NULL	, [ABAC02] NCHAR(3) NULL	, [ABAC03] NCHAR(3) NULL	, [ABAC04] NCHAR(3) NULL	, [ABAC05] NCHAR(3) NULL	, [ABAC06] NCHAR(3) NULL	, [ABAC07] NCHAR(3) NULL	, [ABAC08] NCHAR(3) NULL	, [ABAC09] NCHAR(3) NULL	, [ABAC10] NCHAR(3) NULL	, [ABAC11] NCHAR(3) NULL	, [ABAC12] NCHAR(3) NULL	, [ABAC13] NCHAR(3) NULL	, [ABAC14] NCHAR(3) NULL	, [ABAC15] NCHAR(3) NULL	, [ABAC16] NCHAR(3) NULL	, [ABAC17] NCHAR(3) NULL	, [ABAC18] NCHAR(3) NULL	, [ABAC19] NCHAR(3) NULL	, [ABAC20] NCHAR(3) NULL	, [ABAC21] NCHAR(3) NULL	, [ABAC22] NCHAR(3) NULL	, [ABAC23] NCHAR(3) NULL	, [ABAC24] NCHAR(3) NULL	, [ABAC25] NCHAR(3) NULL	, [ABAC26] NCHAR(3) NULL	, [ABAC27] NCHAR(3) NULL	, [ABAC28] NCHAR(3) NULL	, [ABAC29] NCHAR(3) NULL	, [ABAC30] NCHAR(3) NULL	, [ABRMK] NCHAR(30) NULL	, [ABTXCT] NCHAR(20) NULL	, [ABTX2] NCHAR(20) NULL	, [ABALP1] NCHAR(40) NULL	, [WSValidation] VARCHAR(50) NULL	, [Status] VARCHAR(50) NULL	, [RejReason] VARCHAR(50) NULL	, [SyncID] INT NULL	, [UpdatedDateTime] DATETIME NOT NULL CONSTRAINT DF_F0101_AUDIT_UpdatedDateTime DEFAULT(getdate())	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0101_AUDIT_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F0101_Audit] PRIMARY KEY ([ABAN8] ASC)	)END
/* TableDDL - [BUSDTA].[F0101_Audit] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0101_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0101_Audit_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0101_Audit_del]	(	  [TransID] NUMERIC(8,0) 	, [ABAN8] NUMERIC(8,0)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([TransID] asc, [ABAN8] ASC)	)END
/* SHADOW TABLE FOR [BUSDTA].[F0101_Audit] - End */
/* TRIGGERS FOR F0101_Audit - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0101_Audit_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0101_Audit_ins
	ON BUSDTA.F0101_Audit AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0101_Audit_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0101_Audit_del.TransID= inserted.TransID AND BUSDTA.F0101_Audit_del.ABAN8= inserted.ABAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0101_Audit_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0101_Audit_upd
	ON BUSDTA.F0101_Audit AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0101_Audit
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0101_Audit.TransID= inserted.TransID AND BUSDTA.F0101_Audit.ABAN8= inserted.ABAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0101_Audit_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0101_Audit_dlt
	ON BUSDTA.F0101_Audit AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0101_Audit_del (TransID,ABAN8, last_modified )
	SELECT deleted.TransID,deleted.ABAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0101_Audit - END */
