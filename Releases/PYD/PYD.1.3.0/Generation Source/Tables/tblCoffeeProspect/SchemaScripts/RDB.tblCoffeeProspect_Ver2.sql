/* [BUSDTA].[tblCoffeeProspect] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeProspect]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeProspect]
	(
	  [CoffeeProspectID] INTEGER NOT NULL
	, [ProspectID] INTEGER NOT NULL
	, [CoffeeBlendID] INTEGER NULL
	, [CompetitorID] INTEGER NULL
	, [UOMID] INTEGER NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] NUMERIC(8,0) NULL
	, [UsageMeasurementID] INTEGER NULL
	, [LiqCoffeeTypeID] INTEGER NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([CoffeeProspectID] ASC,"ProspectID" ASC)
	)
END
/* TableDDL - [BUSDTA].[tblCoffeeProspect] - End */
