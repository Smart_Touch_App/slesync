/* [BUSDTA].[tblCSPKLB] - begins */

/* TableDDL - [BUSDTA].[tblCSPKLB] - Start */
IF OBJECT_ID('[BUSDTA].[tblCSPKLB]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCSPKLB]
	(
	  [CSPKLBID] INTEGER NOT NULL 
	, [CSPKLBType] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CSPKLBID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblCSPKLB] - End */
