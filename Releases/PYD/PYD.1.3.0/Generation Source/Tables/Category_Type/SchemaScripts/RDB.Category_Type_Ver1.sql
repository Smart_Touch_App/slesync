
/* [BUSDTA].[Category_Type] - begins */

/* TableDDL - [BUSDTA].[Category_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Category_Type]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Category_Type]
	(
	  [CategoryTypeID] NUMERIC(8,0) NOT NULL
	, [CategoryTypeCD] VARCHAR(5) NOT NULL
	, [CategoryCodeType] NVARCHAR(20) NULL
	, [CategoryTypeDESC] NVARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([CategoryTypeCD] ASC)
	)
END
/* TableDDL - [BUSDTA].[Category_Type] - End */

