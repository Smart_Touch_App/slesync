

/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."MasterNotification"
("MasterNotificationID", "RouteID", "Title", "Description", "TypeID", "TransactionID", "Status", "Category", "Read", "ValidTill", "Acknowledged", "Initiator", "ResponseType", "ResponseValue", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."MasterNotificationID"}, {ml r."RouteID"}, {ml r."Title"}, {ml r."Description"}, {ml r."TypeID"}, {ml r."TransactionID"}, {ml r."Status"}, {ml r."Category"}, {ml r."Read"}, {ml r."ValidTill"}, {ml r."Acknowledged"}, {ml r."Initiator"}, {ml r."ResponseType"}, {ml r."ResponseValue"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
