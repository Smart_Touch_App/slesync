
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M50012"
SET "TDTYP" = {ml r."TDTYP"}, "TDCLASS" = {ml r."TDCLASS"}, "TDDTLS" = {ml r."TDDTLS"}, "TDSTRTTM" = {ml r."TDSTRTTM"}, "TDENDTM" = {ml r."TDENDTM"}, "TDSTID" = {ml r."TDSTID"}, "TDAN8" = {ml r."TDAN8"}, "TDSTTLID" = {ml r."TDSTTLID"}, "TDPNTID" = {ml r."TDPNTID"}, "TDSTAT" = {ml r."TDSTAT"}, "TDREFHDRID" = {ml r."TDREFHDRID"}, "TDREFHDRTYP" = {ml r."TDREFHDRTYP"}, "TDCRBY" = {ml r."TDCRBY"}, "TDCRDT" = {ml r."TDCRDT"}, "TDUPBY" = {ml r."TDUPBY"}, "TDUPDT" = {ml r."TDUPDT"}
WHERE "TDID" = {ml r."TDID"} AND "TDROUT" = {ml r."TDROUT"}
 

