 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."PreTrip_Inspection_Header"
("PreTripInspectionHeaderId", "RouteId", "TemplateId", "PreTripDateTime", "UserName", "StatusId", "VehicleMake", "VehicleNumber", "OdoMmeterReading", "Comment", "VerSignature", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."PreTripInspectionHeaderId"}, {ml r."RouteId"}, {ml r."TemplateId"}, {ml r."PreTripDateTime"}, {ml r."UserName"}, {ml r."StatusId"}, {ml r."VehicleMake"}, {ml r."VehicleNumber"}, {ml r."OdoMmeterReading"}, {ml r."Comment"}, {ml r."VerSignature"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
