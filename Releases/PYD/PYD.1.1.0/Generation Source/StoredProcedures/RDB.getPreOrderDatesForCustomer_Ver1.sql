CREATE OR REPLACE PROCEDURE "BUSDTA"."getPreOrderDatesForCustomer" (In CustomerNum numeric(8,0), In SelectedDate date,In BaseDate date, In Years int default 2) 
BEGIN
	DECLARE  WeekNumber numeric(2,0); -- Week Number for SelectedDate
	DECLARE  DayNumber numeric(2,0); -- Day Number for SelectedDate
    DECLARE DeliveryDayCode nvarchar(3);
    DECLARE i INTEGER = 1;
    DECLARE loopUpTo INTEGER = (Years * 366);
    DECLARE nextDate date;
    /*This cursor gets the pattern, for a Delivery Day Code of the customer.*/
    DECLARE cur_DayCodePattern CURSOR FOR
      SELECT  DCWN, DCDN
      FROM BUSDTA.M56M0002 
      WHERE DCDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerNum);

    DECLARE  Week numeric(4,0); -- Week Number for Delivery Day Code
    DECLARE  Day numeric(4,0); -- Day Number for Delivery Day Code
    DECLARE  WeeksPerCycle integer;

    DECLARE LOCAL TEMPORARY TABLE TempTab ( val as nchar(50) ); -- Temporary table to store the next dates
    
    Select AISTOP into DeliveryDayCode from busdta.F03012 where AIAN8 = CustomerNum;

    select DMWPC into WeeksPerCycle from busdta.M56M0001 where DMDDC = DeliveryDayCode;

    /*Outer loop: Generates the Calendar dates from the Base date. These dates will be compared to the Pattern dates.*/    
    lp: LOOP
        IF i = loopUpTo THEN LEAVE lp END IF;
        select dateadd(day, i, BaseDate) into nextDate from dummy;

        /*Get the Period, Week and Day number for the given date, with respect to BaseDate*/
        select busdta.GetWeekForDate(nextDate, BaseDate, WeeksPerCycle) into WeekNumber from dummy;
        select busdta.GetDayForDate(nextDate, BaseDate) into DayNumber from dummy ;
    
        /*Inner Loop: This loop generates the dates for pattern of Delivery Day Code, to be compared with the calendar dates.*/
        OPEN cur_DayCodePattern;    
        ilp: LOOP
            FETCH next cur_DayCodePattern into Week, Day;
                IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                    /*If Calendar date mathes the Pattern dates, it is a qualified date.*/
                    IF Week = WeekNumber AND Day = DayNumber THEN
                        /*If qualified date is greater than than the selected date, then it is the next valid date.*/
                        IF SelectedDate < nextDate THEN
                            insert into busdta.TempTab values(nextDate);
                        END IF;
                    END IF;
        END LOOP; -- End Inner Loop    
        CLOSE cur_DayCodePattern;

        SET i = i + 1;
    END LOOP; -- End Outer Loop
    select val from TempTab; -- All valid next dates.
END
GO