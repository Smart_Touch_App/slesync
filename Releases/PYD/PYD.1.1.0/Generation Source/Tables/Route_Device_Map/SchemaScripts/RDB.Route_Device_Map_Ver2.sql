/* [BUSDTA].[Route_Device_Map] - begins */

/* TableDDL - [BUSDTA].[Route_Device_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Device_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Device_Map]
	(
	  [Route_Id] VARCHAR(8) NOT NULL
	, [Device_Id] VARCHAR(30) NOT NULL
	, [Active] INTEGER NULL
	, [Remote_Id] VARCHAR(30) NULL
	, PRIMARY KEY ([Route_Id] ASC, [Device_Id] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Device_Map] - End */
