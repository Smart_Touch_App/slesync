
/* [BUSDTA].[M56M0002] - begins */

/* TableDDL - [BUSDTA].[M56M0002] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0002]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0002]
	(
	  [DCDDC] VARCHAR(3) NOT NULL
	, [DCWN] NUMERIC(4,0) NOT NULL
	, [DCDN] NUMERIC(4,0) NOT NULL
	, [DCISST] NUMERIC(4,0) NULL DEFAULT ((1))
	, [DCCRBY] NVARCHAR (50) NULL
	, [DCCRDT] DATETIME NULL
	, [DCUPBY] NVARCHAR (50) NULL
	, [DCUPDT] DATETIME NULL
	, PRIMARY KEY ([DCDDC] ASC, [DCWN] ASC, [DCDN] ASC)
	)
END
/* TableDDL - [BUSDTA].[M56M0002] - End */

