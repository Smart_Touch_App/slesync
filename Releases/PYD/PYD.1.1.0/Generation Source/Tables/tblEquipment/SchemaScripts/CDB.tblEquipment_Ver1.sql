/* [dbo].[tblEquipment] - begins */

/* TableDDL - [dbo].[tblEquipment] - Start */
IF OBJECT_ID('[dbo].[tblEquipment]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipment]
	(
	  [EquipmentID] INT NOT NULL
	, [ProspectID] NUMERIC(10,0) NOT NULL
	, [EquipmentType] VARCHAR(200) NULL
	, [EquipmentCategoryID] INT NULL
	, [EquipmentSubCategory] VARCHAR(MAX) NULL
	, [EquipmentQuantity] INT NULL
	, [EquipmentOwned] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENT_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENT_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipment] PRIMARY KEY ([EquipmentID] ASC,[ProspectID] ASC)
	)


END
GO
/* TableDDL - [dbo].[tblEquipment] - End */

/* SHADOW TABLE FOR [dbo].[tblEquipment] - Start */
IF OBJECT_ID('[dbo].[tblEquipment_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipment_del]
	(
	  [EquipmentID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblEquipment] - End */
/* TRIGGERS FOR tblEquipment - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblEquipment_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblEquipment_ins
	ON dbo.tblEquipment AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblEquipment_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblEquipment_del.EquipmentID= inserted.EquipmentID AND dbo.tblEquipment_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblEquipment_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipment_upd
	ON dbo.tblEquipment AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblEquipment
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblEquipment.EquipmentID= inserted.EquipmentID AND dbo.tblEquipment.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblEquipment_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipment_dlt
	ON dbo.tblEquipment AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblEquipment_del (EquipmentID,ProspectID, last_modified )
	SELECT deleted.EquipmentID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipment - END */
