

/* Date Modified: 11-01-2016 */

SELECT "tblCoffeeProspect"."CoffeeProspectID"
,"tblCoffeeProspect"."ProspectID"
,"tblCoffeeProspect"."CoffeeBlendID"
,"tblCoffeeProspect"."CompetitorID"
,"tblCoffeeProspect"."UOMID"
,"tblCoffeeProspect"."PackSize"
,"tblCoffeeProspect"."CS_PK_LB"
,"tblCoffeeProspect"."Price"
,"tblCoffeeProspect"."UsageMeasurementID"
,"tblCoffeeProspect"."LiqCoffeeTypeID"
,"tblCoffeeProspect"."CreatedBy"
,"tblCoffeeProspect"."CreatedDatetime"
,"tblCoffeeProspect"."UpdatedBy"
,"tblCoffeeProspect"."UpdatedDatetime"
FROM "dbo"."tblCoffeeProspect"
INNER JOIN BUSDTA.Prospect_Master ON "tblCoffeeProspect".ProspectID=BUSDTA.Prospect_Master.ProspectId 
WHERE "dbo"."tblCoffeeProspect"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Master"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
