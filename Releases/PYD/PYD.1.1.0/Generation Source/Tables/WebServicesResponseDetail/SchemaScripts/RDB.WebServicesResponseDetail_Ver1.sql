IF OBJECT_ID('[BUSDTA].[WebServicesResponseDetail]') IS NULL
BEGIN

CREATE TABLE "BUSDTA"."WebServicesResponseDetail" (
    "ResponseID"                     numeric(8,0) NOT NULL DEFAULT autoincrement
   ,"ResponseBLOB"                   long nvarchar NULL
   ,"TimeForResponse"                time NULL
   ,"ResponseType"                   nvarchar(700) NULL
   ,"IsMandatorySync"                bit NULL DEFAULT ((0))
   ,"IsSynced"                       bit NULL DEFAULT ((0))
   ,"IsUpdateAvailable"              bit NULL DEFAULT ((0))
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("ResponseID" ASC) 
)

END