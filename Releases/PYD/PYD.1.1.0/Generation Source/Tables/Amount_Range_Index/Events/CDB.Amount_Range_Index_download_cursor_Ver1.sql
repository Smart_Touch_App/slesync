
 
SELECT "BUSDTA"."Amount_Range_Index"."RequestCode"
,"BUSDTA"."Amount_Range_Index"."AuthorizationFormatCode"
,"BUSDTA"."Amount_Range_Index"."RangeStart"
,"BUSDTA"."Amount_Range_Index"."RangeEnd"
,"BUSDTA"."Amount_Range_Index"."RouteId"
,"BUSDTA"."Amount_Range_Index"."CreatedBy"
,"BUSDTA"."Amount_Range_Index"."CreatedDatetime"
,"BUSDTA"."Amount_Range_Index"."UpdatedBy"
,"BUSDTA"."Amount_Range_Index"."UpdatedDatetime"

FROM "BUSDTA"."Amount_Range_Index"
WHERE "BUSDTA"."Amount_Range_Index"."last_modified">= {ml s.last_table_download}
 

