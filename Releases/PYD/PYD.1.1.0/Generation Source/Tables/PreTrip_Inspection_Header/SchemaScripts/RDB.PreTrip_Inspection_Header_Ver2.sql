/* [BUSDTA].[PreTrip_Inspection_Header] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Header]
	(
	  [PreTripInspectionHeaderId] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TemplateId] NUMERIC(8,0) NULL
	, [PreTripDateTime] DATETIME NULL
	, [UserName] NVARCHAR (50) NULL
	, [StatusId] NUMERIC(3,0) NULL
	, [VehicleMake] NVARCHAR (25) NULL
	, [VehicleNumber] NUMERIC(10,0) NULL
	, [OdoMmeterReading] NUMERIC(10,2) NULL
	, [Comment] NVARCHAR (100) NULL
	, [VerSignature] LONG BINARY NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - End */
