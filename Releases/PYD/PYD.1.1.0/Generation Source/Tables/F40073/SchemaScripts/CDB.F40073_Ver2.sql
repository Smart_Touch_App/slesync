/* [BUSDTA].[F40073] - begins */

/* TableDDL - [BUSDTA].[F40073] - Start */
IF OBJECT_ID('[BUSDTA].[F40073]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40073]
	(
	  [HYPRFR] NCHAR(2) NOT NULL
	, [HYHYID] NCHAR(10) NOT NULL
	, [HYHY01] FLOAT NULL
	, [HYHY02] FLOAT NULL
	, [HYHY03] FLOAT NULL
	, [HYHY04] FLOAT NULL
	, [HYHY05] FLOAT NULL
	, [HYHY06] FLOAT NULL
	, [HYHY07] FLOAT NULL
	, [HYHY08] FLOAT NULL
	, [HYHY09] FLOAT NULL
	, [HYHY10] FLOAT NULL
	, [HYHY11] FLOAT NULL
	, [HYHY12] FLOAT NULL
	, [HYHY13] FLOAT NULL
	, [HYHY14] FLOAT NULL
	, [HYHY15] FLOAT NULL
	, [HYHY16] FLOAT NULL
	, [HYHY17] FLOAT NULL
	, [HYHY18] FLOAT NULL
	, [HYHY19] FLOAT NULL
	, [HYHY20] FLOAT NULL
	, [HYHY21] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F40073_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F40073] PRIMARY KEY ([HYPRFR] ASC, [HYHYID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F40073] - End */

/* SHADOW TABLE FOR [BUSDTA].[F40073] - Start */
IF OBJECT_ID('[BUSDTA].[F40073_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40073_del]
	(
	  [HYPRFR] NCHAR(2)
	, [HYHYID] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([HYPRFR] ASC, [HYHYID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F40073] - End */
/* TRIGGERS FOR F40073 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F40073_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F40073_ins
	ON BUSDTA.F40073 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F40073_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F40073_del.HYHYID= inserted.HYHYID AND BUSDTA.F40073_del.HYPRFR= inserted.HYPRFR
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F40073_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40073_upd
	ON BUSDTA.F40073 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F40073
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F40073.HYHYID= inserted.HYHYID AND BUSDTA.F40073.HYPRFR= inserted.HYPRFR');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F40073_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40073_dlt
	ON BUSDTA.F40073 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F40073_del (HYHYID, HYPRFR, last_modified )
	SELECT deleted.HYHYID, deleted.HYPRFR, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F40073 - END */
