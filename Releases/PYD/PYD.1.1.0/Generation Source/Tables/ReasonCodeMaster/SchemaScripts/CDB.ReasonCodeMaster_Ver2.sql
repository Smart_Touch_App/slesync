/* [BUSDTA].[ReasonCodeMaster] - begins */

/* TableDDL - [BUSDTA].[ReasonCodeMaster] - Start */
IF OBJECT_ID('[BUSDTA].[ReasonCodeMaster]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ReasonCodeMaster]
	(
	  [ReasonCodeId] INT NOT NULL
	, [ReasonCode] VARCHAR(5) NOT NULL
	, [ReasonCodeDescription] NCHAR(100) NULL
	, [ReasonCodeType] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_REASONCODEMASTER_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_REASONCODEMASTER_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_REASONCODEMASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ReasonCodeMaster] PRIMARY KEY ([ReasonCode] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ReasonCodeMaster] - End */

/* SHADOW TABLE FOR [BUSDTA].[ReasonCodeMaster] - Start */
IF OBJECT_ID('[BUSDTA].[ReasonCodeMaster_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ReasonCodeMaster_del]
	(
	  [ReasonCode] VARCHAR(5)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ReasonCode] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ReasonCodeMaster] - End */
/* TRIGGERS FOR ReasonCodeMaster - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ReasonCodeMaster_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ReasonCodeMaster_ins
	ON BUSDTA.ReasonCodeMaster AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ReasonCodeMaster_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ReasonCodeMaster_del.ReasonCode= inserted.ReasonCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ReasonCodeMaster_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ReasonCodeMaster_upd
	ON BUSDTA.ReasonCodeMaster AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ReasonCodeMaster
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ReasonCodeMaster.ReasonCode= inserted.ReasonCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ReasonCodeMaster_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ReasonCodeMaster_dlt
	ON BUSDTA.ReasonCodeMaster AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ReasonCodeMaster_del (ReasonCode, last_modified )
	SELECT deleted.ReasonCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ReasonCodeMaster - END */
