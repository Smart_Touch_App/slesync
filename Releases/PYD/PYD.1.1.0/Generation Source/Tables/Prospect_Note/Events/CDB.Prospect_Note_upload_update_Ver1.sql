 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Note"
SET "ProspectNoteType" = {ml r."ProspectNoteType"}, "ProspectNoteDetail" = {ml r."ProspectNoteDetail"}, "IsDefault" = {ml r."IsDefault"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectId" = {ml r."ProspectId"} AND "ProspectNoteId" = {ml r."ProspectNoteId"} AND "RouteId" = {ml r."RouteId"}
 
