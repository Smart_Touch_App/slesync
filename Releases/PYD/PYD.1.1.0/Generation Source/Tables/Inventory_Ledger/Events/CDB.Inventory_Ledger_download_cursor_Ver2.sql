
     SELECT "BUSDTA"."Inventory_Ledger"."InventoryLedgerID"  ,"BUSDTA"."Inventory_Ledger"."ItemId"  ,"BUSDTA"."Inventory_Ledger"."ItemNumber"  ,
     "BUSDTA"."Inventory_Ledger"."RouteId"  ,"BUSDTA"."Inventory_Ledger"."TransactionQty"  ,"BUSDTA"."Inventory_Ledger"."TransactionQtyUM"  ,
     "BUSDTA"."Inventory_Ledger"."TransactionQtyPrimaryUM"  ,"BUSDTA"."Inventory_Ledger"."TransactionType"  ,
     "BUSDTA"."Inventory_Ledger"."TransactionId"  ,"BUSDTA"."Inventory_Ledger"."SettlementID"  ,"BUSDTA"."Inventory_Ledger"."CreatedBy"  ,
     "BUSDTA"."Inventory_Ledger"."CreatedDatetime"  ,"BUSDTA"."Inventory_Ledger"."UpdatedBy"  ,"BUSDTA"."Inventory_Ledger"."UpdatedDatetime"    
     FROM "BUSDTA"."Inventory_Ledger"  WHERE "BUSDTA"."Inventory_Ledger"."last_modified" >= {ml s.last_table_download}  
     AND "BUSDTA"."Inventory_Ledger"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     