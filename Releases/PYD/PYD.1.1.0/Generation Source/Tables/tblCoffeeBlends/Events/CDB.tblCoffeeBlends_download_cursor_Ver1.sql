 
SELECT "dbo"."tblCoffeeBlends"."CoffeeBlendID"
,"dbo"."tblCoffeeBlends"."CoffeeBlend"
,"dbo"."tblCoffeeBlends"."CreatedBy"
,"dbo"."tblCoffeeBlends"."CreatedDatetime"
,"dbo"."tblCoffeeBlends"."UpdatedBy"
,"dbo"."tblCoffeeBlends"."UpdatedDatetime"

FROM "dbo"."tblCoffeeBlends"
WHERE "dbo"."tblCoffeeBlends"."last_modified">= {ml s.last_table_download}
