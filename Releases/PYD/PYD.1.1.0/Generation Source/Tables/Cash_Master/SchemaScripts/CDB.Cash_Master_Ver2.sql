/* [BUSDTA].[Cash_Master] - begins */

/* TableDDL - [BUSDTA].[Cash_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Master]
	(
	  [CashId] NUMERIC(8,0) NOT NULL
	, [CashType] NCHAR(10) NULL
	, [CashCode] NCHAR(10) NULL
	, [CashDescription] NCHAR(100) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CASH_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cash_Master] PRIMARY KEY ([CashId] ASC)
	)

END
/* TableDDL - [BUSDTA].[Cash_Master] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Master_del]
	(
	  [CashId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CashId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Master] - End */
/* TRIGGERS FOR Cash_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cash_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cash_Master_ins
	ON BUSDTA.Cash_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cash_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cash_Master_del.CashId= inserted.CashId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cash_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Master_upd
	ON BUSDTA.Cash_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cash_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cash_Master.CashId= inserted.CashId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cash_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Master_dlt
	ON BUSDTA.Cash_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cash_Master_del (CashId, last_modified )
	SELECT deleted.CashId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cash_Master - END */
