/* [BUSDTA].[Notification] - begins */

/* TableDDL - [BUSDTA].[Notification] - Start */
IF OBJECT_ID('[BUSDTA].[Notification]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Notification]
	(
	  [NotificationID] NUMERIC(8,0) NOT NULL 
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [Title] NVARCHAR (200) NOT NULL
	, [Description] NVARCHAR (300) NOT NULL
	, [TypeID] NUMERIC(4,0) NULL
	, [Category] NUMERIC(4,0) NULL
	, [SubCategory] NUMERIC(4,0) NULL
	, [Read] NVARCHAR (1) NULL DEFAULT 'N'
	, [ValidTill] DATE NULL
	, [Acknowledged] NVARCHAR (1) NULL DEFAULT 'N'
	, [Initiator] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([NotificationID] ASC, [RouteID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Notification] - End */
