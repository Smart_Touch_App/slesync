/* [dbo].[tblGlobalSyncLog] - begins */

/* TableDDL - [dbo].[tblGlobalSyncLog] - Start */
IF OBJECT_ID('[dbo].[tblGlobalSyncLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGlobalSyncLog]
	(
	  [GlobalSyncLogID] INTEGER NOT NULL default autoincrement
	, [GlobalSyncID] INTEGER NULL
	, [DeviceID] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL DEFAULT(getdate())

	, PRIMARY KEY ([GlobalSyncLogID] ASC)
	)
END
/* TableDDL - [dbo].[tblGlobalSyncLog] - End */
