

/* [BUSDTA].[Order_Header_BETA] - begins */

/* TableDDL - [BUSDTA].[Order_Header_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_BETA]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_Header_BETA]	(	  [OrderID] NUMERIC(8,0) NOT NULL	, [OrderTypeId] NUMERIC(3,0) NULL	, [OriginatingRouteID] NUMERIC(8,0) NOT NULL	, [Branch] NVARCHAR(12) NULL	, [CustShipToId] NUMERIC(8,0) NOT NULL	, [CustBillToId] NUMERIC(8,0) NOT NULL	, [CustParentId] NUMERIC(8,0) NOT NULL	, [OrderDate] DATE NULL	, [TotalCoffeeAmt] NUMERIC(12,4) NULL	, [TotalAlliedAmt] NUMERIC(12,4) NULL	, [OrderTotalAmt] NUMERIC(12,4) NULL	, [SalesTaxAmt] NUMERIC(12,4) NULL	, [InvoiceTotalAmt] NUMERIC(12,4) NULL	, [EnergySurchargeAmt] NUMERIC(12,4) NULL	, [VarianceAmt] NUMERIC(12,4) NULL	, [SurchargeReasonCodeId] NUMERIC(3,0) NULL	, [OrderStateId] NUMERIC(3,0) NULL	, [OrderSignature] LONG BINARY NULL	, [ChargeOnAccountSignature] LONG BINARY NULL	, [JDEOrderCompany] NVARCHAR(5) NULL	, [JDEOrderNumber] NUMERIC(8,0) NULL	, [JDEOrderType] NVARCHAR(2) NULL	, [JDEInvoiceCompany] NVARCHAR(5) NULL	, [JDEInvoiceNumber] NUMERIC(8,0) NULL	, [JDEOInvoiceType] NVARCHAR(2) NULL	, [JDEZBatchNumber] NVARCHAR(15) NULL	, [JDEProcessID] NUMERIC(3,0) NULL	, [SettlementID] NUMERIC(8,0) NULL	, [PaymentTerms] NVARCHAR(3) NULL	, [CustomerReference1] NVARCHAR(25) NULL	, [CustomerReference2] NVARCHAR(25) NULL	, [AdjustmentSchedule] NVARCHAR(8) NULL	, [TaxArea] NVARCHAR(10) NULL	, [TaxExplanationCode] NVARCHAR(1) NULL	, [VoidReasonCodeId] NUMERIC(3,0) NULL	, [ChargeOnAccount] NVARCHAR(1) NULL	, [HoldCommitted] NVARCHAR(1) NULL	, [CreatedBy] NUMERIC(8,0) NULL	, [CreatedDatetime] DATETIME NULL	, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL	, PRIMARY KEY ([OrderID] ASC)	)
END
/* TableDDL - [BUSDTA].[Order_Header_BETA] - End */
