/* [BUSDTA].[tblTransactionControl] - begins */

/* TableDDL - [BUSDTA].[tblTransactionControl] - Start */
IF OBJECT_ID('[BUSDTA].[tblTransactionControl]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblTransactionControl]
	(
	  [TransactionID] UNIQUEIDENTIFIER NOT NULL
	, [PageName] VARCHAR(200) NULL
	, [NoofTxns] BIGINT NULL
	, [RouteID] VARCHAR(50) NOT NULL
	, [DeviceID] VARCHAR(50) NULL
	, [IsValidated] INT NULL
	, [CreatedOn] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONCONTROL_CreatedOn DEFAULT(getdate())
	, [Createdby] VARCHAR(50) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONCONTROL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblTransactionControl] PRIMARY KEY ([TransactionID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblTransactionControl] - End */
