 
SELECT "dbo"."tblCoffeeVolume"."CoffeeVolumeID"
,"dbo"."tblCoffeeVolume"."Volume"
,"dbo"."tblCoffeeVolume"."CreatedBy"
,"dbo"."tblCoffeeVolume"."CreatedDatetime"
,"dbo"."tblCoffeeVolume"."UpdatedBy"
,"dbo"."tblCoffeeVolume"."UpdatedDatetime"

FROM "dbo"."tblCoffeeVolume"
WHERE "dbo"."tblCoffeeVolume"."last_modified">= {ml s.last_table_download}

