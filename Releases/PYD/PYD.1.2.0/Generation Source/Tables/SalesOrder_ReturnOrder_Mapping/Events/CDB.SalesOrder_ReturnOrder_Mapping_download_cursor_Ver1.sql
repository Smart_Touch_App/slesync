

SELECT "BUSDTA"."SalesOrder_ReturnOrder_Mapping"."SalesOrderId"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."ReturnOrderID"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."OrderDetailID"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."ItemID"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."RouteId"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."CreatedBy"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."CreatedDatetime"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."UpdatedBy"
,"BUSDTA"."SalesOrder_ReturnOrder_Mapping"."UpdatedDatetime"

FROM "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
WHERE "BUSDTA"."SalesOrder_ReturnOrder_Mapping"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."SalesOrder_ReturnOrder_Mapping"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
