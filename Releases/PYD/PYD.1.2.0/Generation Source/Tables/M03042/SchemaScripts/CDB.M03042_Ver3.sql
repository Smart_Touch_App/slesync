
/* [BUSDTA].[M03042] - begins */

/* TableDDL - [BUSDTA].[M03042] - Start */
IF OBJECT_ID('[BUSDTA].[M03042]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03042]
	(
	  [PDAN8] NUMERIC(8,0) NOT NULL
	, [PDCO] VARCHAR(5) NOT NULL
	, [PDID] NUMERIC(8,0) NOT NULL
	, [PDPAMT] NUMERIC(8,4) NULL
	, [PDPMODE] BIT NULL
	, [PDCHQNO] NCHAR(10) NULL
	, [PDCHQDT] DATE NULL
	, [PDCRBY] NCHAR(10) NULL
	, [PDCRDT] DATE NULL
	, [PDUPBY] NCHAR(10) NULL
	, [PDUPDT] DATE NULL
	, [PDRCID] INT NULL
	, [PDTRMD] BIT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M03042_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M03042] PRIMARY KEY ([PDAN8] ASC, [PDCO] ASC, [PDID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M03042] - End */

/* SHADOW TABLE FOR [BUSDTA].[M03042] - Start */
IF OBJECT_ID('[BUSDTA].[M03042_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03042_del]
	(
	  [PDAN8] NUMERIC(8,0)
	, [PDCO] VARCHAR(5)
	, [PDID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PDAN8] ASC, [PDCO] ASC, [PDID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[M03042] - End */
/* TRIGGERS FOR M03042 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M03042_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M03042_ins
	ON BUSDTA.M03042 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M03042_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M03042_del.PDAN8= inserted.PDAN8 AND BUSDTA.M03042_del.PDCO= inserted.PDCO AND BUSDTA.M03042_del.PDID= inserted.PDID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M03042_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03042_upd
	ON BUSDTA.M03042 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M03042
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M03042.PDAN8= inserted.PDAN8 AND BUSDTA.M03042.PDCO= inserted.PDCO AND BUSDTA.M03042.PDID= inserted.PDID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M03042_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03042_dlt
	ON BUSDTA.M03042 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M03042_del (PDAN8, PDCO, PDID, last_modified )
	SELECT deleted.PDAN8, deleted.PDCO, deleted.PDID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M03042 - END */
