/* [dbo].[tblSyncArticles] - begins */

/* TableDDL - [dbo].[tblSyncArticles] - Start */
IF OBJECT_ID('[dbo].[tblSyncArticles]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblSyncArticles]
	(
	  [SyncArticleID] INTEGER NOT NULL default autoincrement
	, [PublicationName] VARCHAR(100) NULL
	, [SchemaName] VARCHAR(128) NULL
	, [TableName] VARCHAR(128) NULL
	, [ActiveYN] BIT NULL DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL DEFAULT(getdate())

	, PRIMARY KEY ([SyncArticleID] ASC)
	)
END
/* TableDDL - [dbo].[tblSyncArticles] - End */
