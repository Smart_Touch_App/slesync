 
SELECT "dbo"."tblEquipmentCategory"."EquipmentCategoryID"
,"dbo"."tblEquipmentCategory"."EquipmentCategory"
,"dbo"."tblEquipmentCategory"."EquipmentTypeID"
,"dbo"."tblEquipmentCategory"."CreatedBy"
,"dbo"."tblEquipmentCategory"."CreatedDatetime"
,"dbo"."tblEquipmentCategory"."UpdatedBy"
,"dbo"."tblEquipmentCategory"."UpdatedDatetime"

FROM "dbo"."tblEquipmentCategory"
WHERE "dbo"."tblEquipmentCategory"."last_modified">= {ml s.last_table_download}

