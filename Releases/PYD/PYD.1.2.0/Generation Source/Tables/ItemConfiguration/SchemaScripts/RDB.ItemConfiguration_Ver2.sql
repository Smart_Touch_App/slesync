
/* [BUSDTA].[ItemConfiguration] - begins */

/* TableDDL - [BUSDTA].[ItemConfiguration] - Start */
IF OBJECT_ID('[BUSDTA].[ItemConfiguration]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemConfiguration]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [RouteEnabled] BIT NULL
	, [Sellable] BIT NULL
	, [AllowSearch] BIT NULL
	, [PrimaryUM] NVARCHAR (2) NULL
	, [PricingUM] NVARCHAR (2) NULL
	, [TransactionUM] NVARCHAR (2) NULL
	, [OtherUM1] NVARCHAR (2) NULL
	, [OtherUM2] NVARCHAR (2) NULL
	, [AllowLooseSample] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([ItemID] ASC)
	)
END
/* TableDDL - [BUSDTA].[ItemConfiguration] - End */

