/* [dbo].[tblCompetitor] - begins */

/* TableDDL - [dbo].[tblCompetitor] - Start */
IF OBJECT_ID('[dbo].[tblCompetitor]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCompetitor]
	(
	  [CompetitorID] INTEGER NOT NULL
	, [Competitor] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CompetitorID] ASC)
	)
END
/* TableDDL - [dbo].[tblCompetitor] - End */
