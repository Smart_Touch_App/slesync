
     SELECT "BUSDTA"."F4106"."BPITM"  ,"BUSDTA"."F4106"."BPLITM"  ,"BUSDTA"."F4106"."BPMCU"  ,"BUSDTA"."F4106"."BPLOCN"  ,
     "BUSDTA"."F4106"."BPLOTN"  ,"BUSDTA"."F4106"."BPAN8"  ,"BUSDTA"."F4106"."BPIGID"  ,"BUSDTA"."F4106"."BPCGID"  ,
     "BUSDTA"."F4106"."BPLOTG"  ,"BUSDTA"."F4106"."BPFRMP"  ,"BUSDTA"."F4106"."BPCRCD"  ,"BUSDTA"."F4106"."BPUOM"  ,
     "BUSDTA"."F4106"."BPEFTJ"  ,"BUSDTA"."F4106"."BPEXDJ"  ,"BUSDTA"."F4106"."BPUPRC"  ,"BUSDTA"."F4106"."BPUPMJ"  ,
     "BUSDTA"."F4106"."BPTDAY"    
     FROM "BUSDTA"."F4106", BUSDTA.F4102, BUSDTA.Route_Master rm  
     WHERE ("BUSDTA"."F4106"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or "BUSDTA"."F4102"."last_modified" >= {ml s.last_table_download})    AND IBITM=BPITM AND IBMCU = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )       