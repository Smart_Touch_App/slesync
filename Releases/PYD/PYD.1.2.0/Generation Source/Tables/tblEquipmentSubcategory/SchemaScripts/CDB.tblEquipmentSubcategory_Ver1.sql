/* [dbo].[tblEquipmentSubcategory] - begins */

/* TableDDL - [dbo].[tblEquipmentSubcategory] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentSubcategory]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentSubcategory]
	(
	  [EquipmentSubcategoryID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentCategoryID] INT NULL
	, [EquipmentSubCategory] VARCHAR(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTSUBCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTSUBCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTSUBCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentSubcategory] PRIMARY KEY ([EquipmentSubcategoryID] ASC)
	)


END
GO
/* TableDDL - [dbo].[tblEquipmentSubcategory] - End */

/* SHADOW TABLE FOR [dbo].[tblEquipmentSubcategory] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentSubcategory_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentSubcategory_del]
	(
	  [EquipmentSubcategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentSubcategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblEquipmentSubcategory] - End */
/* TRIGGERS FOR tblEquipmentSubcategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblEquipmentSubcategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblEquipmentSubcategory_ins
	ON dbo.tblEquipmentSubcategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblEquipmentSubcategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblEquipmentSubcategory_del.EquipmentSubcategoryID= inserted.EquipmentSubcategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblEquipmentSubcategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipmentSubcategory_upd
	ON dbo.tblEquipmentSubcategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblEquipmentSubcategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblEquipmentSubcategory.EquipmentSubcategoryID= inserted.EquipmentSubcategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblEquipmentSubcategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipmentSubcategory_dlt
	ON dbo.tblEquipmentSubcategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblEquipmentSubcategory_del (EquipmentSubcategoryID, last_modified )
	SELECT deleted.EquipmentSubcategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentSubcategory - END */
