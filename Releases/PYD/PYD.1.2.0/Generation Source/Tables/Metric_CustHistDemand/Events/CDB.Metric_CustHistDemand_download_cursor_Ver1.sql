
 
SELECT "BUSDTA"."Metric_CustHistDemand"."CustomerId"
,"BUSDTA"."Metric_CustHistDemand"."ItemID"
,"BUSDTA"."Metric_CustHistDemand"."DemandFromDate"
,"BUSDTA"."Metric_CustHistDemand"."DemandThroughDate"
,"BUSDTA"."Metric_CustHistDemand"."PlannedStops"
,"BUSDTA"."Metric_CustHistDemand"."ActualStops"
,"BUSDTA"."Metric_CustHistDemand"."TotalQtySold"
,"BUSDTA"."Metric_CustHistDemand"."TotalQtyReturned"
,"BUSDTA"."Metric_CustHistDemand"."QtySoldPerStop"
,"BUSDTA"."Metric_CustHistDemand"."QtyReturnedPerStop"
,"BUSDTA"."Metric_CustHistDemand"."NetQtySoldPerStop"
,"BUSDTA"."Metric_CustHistDemand"."QtySoldDeltaPerStop"
,"BUSDTA"."Metric_CustHistDemand"."QtyReturnedDeltaPerStop"
,"BUSDTA"."Metric_CustHistDemand"."CreatedBy"
,"BUSDTA"."Metric_CustHistDemand"."CreatedDatetime"
,"BUSDTA"."Metric_CustHistDemand"."UpdatedBy"
,"BUSDTA"."Metric_CustHistDemand"."UpdatedDatetime"

FROM "BUSDTA"."Metric_CustHistDemand", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE ("BUSDTA"."Metric_CustHistDemand"."last_modified">= {ml s.last_table_download}  or rm.last_modified >= {ml s.last_table_download}
or crm.last_modified >= {ml s.last_table_download})
AND CustomerId = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
 
