/* [BUSDTA].[Route_Settlement] - begins */

/* TableDDL - [BUSDTA].[Route_Settlement] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement]
	(
	  [SettlementID] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [SettlementNO] NVARCHAR (10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Status] NUMERIC(3,0) NULL
	, [SettlementDateTime] DATETIME NULL
	, [UserId] NUMERIC(8,0) NULL
	, [Originator] NUMERIC(8,0) NULL
	, [Verifier] NUMERIC(8,0) NULL
	, [SettlementAmount] NUMERIC(8,4) NULL
	, [ExceptionAmount] NUMERIC(8,4) NULL
	, [Comment] NVARCHAR(100) NULL
	, [OriginatingRoute] NUMERIC(8,0) NOT NULL
	, [partitioningRoute] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([SettlementID] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Settlement] - End */
