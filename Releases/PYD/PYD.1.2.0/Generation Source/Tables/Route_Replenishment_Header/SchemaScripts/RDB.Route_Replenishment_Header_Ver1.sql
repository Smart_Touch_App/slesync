/* [BUSDTA].[Route_Replenishment_Header] - begins */

/* TableDDL - [BUSDTA].[Route_Replenishment_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Header]
	(
	  [ReplenishmentID] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(8,0) NOT NULL
	, [ReplenishmentTypeId] NUMERIC(8,0) NOT NULL
	, [TransDateFrom] DATETIME NULL
	, [TransDateTo] DATETIME NULL
	, [RequestedBy] NUMERIC(8,0) NULL
	, [FromBranchId] NUMERIC(8,0) NULL
	, [ToBranchId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ReplenishmentID] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Replenishment_Header] - End */
