
/* [BUSDTA].[Order_PriceAdj] - begins */

/* TableDDL - [BUSDTA].[Order_PriceAdj] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_PriceAdj]	(	  [OrderID] NUMERIC(8,0) NOT NULL	, [LineID] NUMERIC(7,4) NOT NULL	, [AdjustmentSeq] NUMERIC(4,0) NOT NULL	, [AdjName] NVARCHAR(20) NULL	, [AdjDesc] NVARCHAR(60) NULL	, [FactorValue] NUMERIC(12,4) NULL	, [UnitPrice] NUMERIC(12,4) NULL	, [OP] NVARCHAR(2) NULL	, [BC] NVARCHAR(2) NULL	, [BasicDesc] NVARCHAR(60) NULL	, [PriceAdjustmentKeyID] NUMERIC(8,0) NULL	, [CreatedBy] NUMERIC(8,0) NULL	, [CreatedDatetime] DATETIME NULL	, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL	, CONSTRAINT [PK_Order_PriceAdj] PRIMARY KEY ([OrderID] ASC, [LineID] ASC, [AdjustmentSeq] ASC)	)END
/* TableDDL - [BUSDTA].[Order_PriceAdj] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_PriceAdj] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_PriceAdj_del]	(	  [OrderID] NUMERIC(8,0)	, [LineID] NUMERIC(7,4)	, [AdjustmentSeq] NUMERIC(4,0)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([OrderID] ASC, [LineID] ASC, [AdjustmentSeq] ASC)	)END
/* SHADOW TABLE FOR [BUSDTA].[Order_PriceAdj] - End */
/* TRIGGERS FOR Order_PriceAdj - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_PriceAdj_ins
	ON BUSDTA.Order_PriceAdj AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_PriceAdj_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_PriceAdj_del.AdjustmentSeq= inserted.AdjustmentSeq AND BUSDTA.Order_PriceAdj_del.LineID= inserted.LineID AND BUSDTA.Order_PriceAdj_del.OrderID= inserted.OrderID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_PriceAdj_upd
	ON BUSDTA.Order_PriceAdj AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_PriceAdj
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_PriceAdj.AdjustmentSeq= inserted.AdjustmentSeq AND BUSDTA.Order_PriceAdj.LineID= inserted.LineID AND BUSDTA.Order_PriceAdj.OrderID= inserted.OrderID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_PriceAdj_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_PriceAdj_dlt
	ON BUSDTA.Order_PriceAdj AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_PriceAdj_del (AdjustmentSeq, LineID, OrderID, last_modified )
	SELECT deleted.AdjustmentSeq, deleted.LineID, deleted.OrderID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_PriceAdj - END */
