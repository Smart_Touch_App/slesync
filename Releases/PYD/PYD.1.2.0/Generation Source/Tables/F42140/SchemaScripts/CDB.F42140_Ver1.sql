

IF OBJECT_ID('[BUSDTA].[F42140]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[F42140](
	[CMAN8] [float] NOT NULL,
	[CMCO] [nchar](5) NOT NULL,
	[CMCMLN] [float] NOT NULL,
	[CMSLSM] [float] NOT NULL,
	[CMSLCM] [float] NULL,
	[CMFCA] [float] NULL,
	[CMAPUN] [float] NULL,
	[CMCCTY] [nchar](1) NULL,
	[CMOWNFLG] [nchar](1) NULL,
	[CMRTYPE] [nvarchar](5) NULL,
	[CMSTMBDT] [datetime] NULL,
	[CMSTMEDT] [datetime] NULL,
	[CMCOMMYN] [nchar](1) NULL,
	[CMUSER] [nchar](10) NULL,
	[CMJOBN] [nchar](10) NULL,
	[CMMKEY] [nchar](15) NULL,
	[CMENTDBY] [float] NULL,
	[CMEDATE] [datetime] NULL,
	[CMUDTTM] [datetime] NULL,
	[CMPID] [nchar](10) NULL,
	[CMVID] [nchar](10) NULL,
	[CMEFFS] [nchar](1) NULL,
	[last_modified] DATETIME NOT NULL CONSTRAINT DF_F42140_last_modified DEFAULT(getdate()),
 CONSTRAINT [PK_F42140] PRIMARY KEY CLUSTERED (	[CMAN8] ASC,[CMCO] ASC,	[CMCMLN] ASC,[CMSLSM] ASC)) ON [PRIMARY]

END

