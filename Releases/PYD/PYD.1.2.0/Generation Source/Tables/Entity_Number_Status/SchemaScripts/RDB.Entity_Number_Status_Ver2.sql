

/* [BUSDTA].[Entity_Number_Status] - begins */

/* TableDDL - [BUSDTA].[Entity_Number_Status] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Number_Status]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Number_Status]
	(
	  [EntityNumberStatusID] DECIMAL(8,0) NOT NULL 
	, [RouteId] DECIMAL(8,0) NULL
	, [Bucket] VARCHAR(30) NULL
	, [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [RangeStart] DECIMAL(8,0) NULL
	, [RangeEnd] DECIMAL(8,0) NULL
	, [CurrentAllocated] DECIMAL(8,0) NULL
	, [IsConsumed] BIT NULL
	, [ConsumedOnDate] SMALLDATETIME NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, PRIMARY KEY ([EntityNumberStatusID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Entity_Number_Status] - End */
