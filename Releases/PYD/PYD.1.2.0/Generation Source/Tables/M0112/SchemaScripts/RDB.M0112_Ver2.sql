
/* [BUSDTA].[M0112] - begins */

/* TableDDL - [BUSDTA].[M0112] - Start */
IF OBJECT_ID('[BUSDTA].[M0112]') IS NULL
BEGIN

CREATE TABLE "BUSDTA"."M0112" (
    "NDAN8"                          numeric(8,0) NOT NULL
   ,"NDID"                           numeric(8,0) NOT NULL DEFAULT autoincrement
   ,"NDDTTM"                         "datetime" NULL
   ,"NDDTLS"                         nvarchar(500) NULL
   ,"NDTYP"                          nvarchar(15) NULL
   ,"NDDFLT"                         bit NULL
   ,"NDCRBY"                         nvarchar(50) NULL
   ,"NDCRDT"                         date NULL
   ,"NDUPBY"                         nvarchar(50) NULL
   ,"NDUPDT"                         date NULL
   ,PRIMARY KEY ("NDAN8" ASC,"NDID" ASC) 
)
    
END
/* TableDDL - [BUSDTA].[M0112] - End */

