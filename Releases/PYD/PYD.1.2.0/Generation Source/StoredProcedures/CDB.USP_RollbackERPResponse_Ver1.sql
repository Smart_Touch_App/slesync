

CREATE PROCEDURE [dbo].[USP_RollbackERPResponse]
(
	@TransactionID uniqueidentifier = NULL
)
AS
BEGIN
DECLARE @Columns_Updated NVARCHAR(1000)
DECLARE @WhereClause NVARCHAR(1000)
DECLARE @EXQUERY VARCHAR(MAX)
DECLARE @Cnt INT 

SELECT TableName, IDENTITY(INT,1,1) Cnt INTO #TmpTables FROM [BUSDTA].[tblTransactionAudit] 
WHERE TransactionID = @TransactionID
GROUP BY TableName

CREATE TABLE #TmpUpdQry ( Qry VARCHAR(MAX), TableName VARCHAR(500), WhereClause NVARCHAR(1000))

	SET @Cnt = 1
	WHILE @Cnt <= (SELECT COUNT(*) FROM #TmpTables)
	BEGIN 
		SET @Columns_Updated = NULL
		SET @WhereClause = NULL

		SELECT @Columns_Updated = ISNULL(@Columns_Updated + ', ', '') + RTRIM(FieldName)+'='''+ RTRIM(OldValue) +'''' 
		FROM [BUSDTA].[tblTransactionAudit] 
			JOIN #TmpTables ON #TmpTables.TableName = [BUSDTA].[tblTransactionAudit].TableName 
			AND #TmpTables.Cnt = @Cnt AND [BUSDTA].[tblTransactionAudit].TransactionID = @TransactionID
		WHERE IsPkey=0
	
		SELECT @WhereClause = ISNULL(@WhereClause + ' AND ', '') + RTRIM(FieldName)+'='''+ RTRIM(OldValue) +'''' 
		FROM [BUSDTA].[tblTransactionAudit] 
			JOIN #TmpTables ON #TmpTables.TableName = [BUSDTA].[tblTransactionAudit].TableName AND #TmpTables.Cnt = @Cnt 
			AND [BUSDTA].[tblTransactionAudit].TransactionID = @TransactionID
		WHERE IsPkey=1

		INSERT INTO #TmpUpdQry ( Qry, TableName, WhereClause) 
		SELECT @Columns_Updated, #TmpTables.TableName, @WhereClause FROM #TmpTables WHERE #TmpTables.Cnt = @Cnt
		
		SELECT @EXQUERY = 'UPDATE '+A.SchemaName+'.' + A.TableName + ' SET ' + Qry + ' WHERE ' + WhereClause FROM #TmpUpdQry
		JOIN [BUSDTA].[tblTransactionAudit] A ON A.TableName = #TmpUpdQry.TableName 
		--SELECT (@EXQUERY)
	EXECUTE(@EXQUERY)
	SET @Cnt = @Cnt + 1
	END

END



GO


