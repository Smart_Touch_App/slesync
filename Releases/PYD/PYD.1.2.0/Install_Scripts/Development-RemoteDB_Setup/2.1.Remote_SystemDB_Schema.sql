
/* [BUSDTA].[Device_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[Device_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Environment_Map]
	(
	  [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [DeviceId] VARCHAR(30) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, PRIMARY KEY ([EnvironmentMasterId] ASC, [DeviceId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Device_Environment_Map] - End */

/* [BUSDTA].[Device_Master] - begins */

/* TableDDL - [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master]
	(
	  [Device_Id] VARCHAR(30) NOT NULL
	, [DeviceName] VARCHAR(100) NULL
	, [Active] INTEGER NULL
	, [manufacturer] NVARCHAR(50) NULL
	, [model] NVARCHAR(50) NULL
	, PRIMARY KEY ([Device_Id] ASC)
	)
END
/* TableDDL - [BUSDTA].[Device_Master] - End */
/* [BUSDTA].[Environment_Master] - begins */

/* TableDDL - [BUSDTA].[Environment_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Environment_Master]') IS NULL
BEGIN
CREATE TABLE "BUSDTA"."Environment_Master" (
    "EnvironmentMasterId"            integer NOT NULL
   ,"EnvName"                        varchar(100) NULL
   ,"EnvDescription"                 varchar(100) NULL
   ,"IsActive"                       varchar(1) NULL
   ,"ReleaseLevel"                   varchar(30) NULL
   ,"MLServerAddress"                nvarchar(50) NULL
   ,"JDEWSAddress"                   nvarchar(50) NULL
   ,"REFDBAddress"                   nvarchar(50) NULL
   ,"ENVDBAddress"                   nvarchar(50) NULL
   ,"CDBAddress"                     nvarchar(50) NULL
   ,"CDBConKey"                      nvarchar(128) NULL
   ,"IISAddress"                     nvarchar(50) NULL
   ,"AppInstallerLocation"           long varchar NULL
   ,"RDBScriptLocation"              long varchar NULL
   ,"TaxLibLocation"                 nvarchar(50) NULL
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("EnvironmentMasterId" ASC) 
)
END
/* TableDDL - [BUSDTA].[Environment_Master] - End */

/* [BUSDTA].[RemoteDBID_Master] - begins */

/* TableDDL - [BUSDTA].[RemoteDBID_Master] - Start */
IF OBJECT_ID('[BUSDTA].[RemoteDBID_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RemoteDBID_Master]
	(
	  [RemoteDBID] INTEGER NOT NULL
	, [AppUserId] INTEGER NULL
	, [DeviceID] VARCHAR(30) NULL
	, [RouteID] VARCHAR(30) NULL
	, [EnvironmentMasterID] INTEGER NULL
	, [ActiveYN] CHAR(1) NOT NULL DEFAULT('N')
	, [CreatedDateTime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([RemoteDBID] ASC)
	)
END
/* TableDDL - [BUSDTA].[RemoteDBID_Master] - End */


/* [BUSDTA].[Reserve_DeviceEnvironmentRoute] - begins */

/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - Start */
IF OBJECT_ID('[BUSDTA].[Reserve_DeviceEnvironmentRoute]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Reserve_DeviceEnvironmentRoute]
	(
	  [ReservationID] INTEGER NOT NULL DEFAULT AUTOINCREMENT
	, [DeviceId] VARCHAR(30) NOT NULL
	, [EnvironmentMasterId] INTEGER NOT NULL
	, [RouteID] VARCHAR(30) NOT NULL
	, [AppUserId] INTEGER NOT NULL
	, [ReservedDateTime] DATETIME NOT NULL DEFAULT(getdate())
	)
END
/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - End */


/* [BUSDTA].[User_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[User_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Environment_Map]
	(
	  [UserEnvironmentMapId] NUMERIC(8,0) NOT NULL
	, [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [AppUserId] NUMERIC(8,0) NOT NULL
	, [IsEnvAdmin] VARCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, PRIMARY KEY ([UserEnvironmentMapId] ASC)
	)
END
/* TableDDL - [BUSDTA].[User_Environment_Map] - End */

/* [BUSDTA].[user_master] - begins */

/* TableDDL - [BUSDTA].[user_master] - Start */
IF OBJECT_ID('[BUSDTA].[user_master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[user_master]
	(
	  [App_user_id] INTEGER NOT NULL 
	, [App_User] VARCHAR(30) NULL
	, [Name] VARCHAR(30) NULL
	, [DomainUser] VARCHAR(40) NULL
	, [AppPassword] VARCHAR(20) NULL
	, [active] BIT NULL DEFAULT((1))
	, PRIMARY KEY ([App_user_id] ASC)
	)
END
/* TableDDL - [BUSDTA].[user_master] - End */

