DROP PUBLICATION IF EXISTS pub_validate_user;
GO
/** Create publication 'pub_validate_user'. **/
CREATE PUBLICATION "pub_validate_user" (
    
    TABLE "BUSDTA"."Route_Device_Map" ,
    TABLE "BUSDTA"."Route_User_Map" ,
    TABLE "BUSDTA"."User_Role_Map" 
)
GO

/** Create the user 'validate_user'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = 'validate_user') THEN
	CREATE SYNCHRONIZATION USER "validate_user";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = 'subs_validate_user') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "subs_validate_user";
END IF
GO

/** Create subscription 'subs_validate_user' to 'pub_validate_user' for 'validate_user'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION "subs_validate_user" TO "pub_validate_user" FOR "validate_user" 
TYPE tcpip ADDRESS 'host=172.30.6.170;port=2439'
	OPTION lt='OFF'
	SCRIPT VERSION 'SLE_SyncScript_PYD.1.2.0'
GO


