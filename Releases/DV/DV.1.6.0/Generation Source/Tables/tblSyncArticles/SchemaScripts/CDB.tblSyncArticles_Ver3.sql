

/* TableDDL - [BUSDTA].[tblSyncArticles] - Start */
IF OBJECT_ID('[BUSDTA].[tblSyncArticles]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblSyncArticles]
	(
	  [SyncArticleID] INT NOT NULL IDENTITY(1,1)
	, [PublicationName] VARCHAR(100) NULL
	, [SchemaName] VARCHAR(128) NULL
	, [TableName] VARCHAR(128) NULL
	, [ActiveYN] BIT NULL CONSTRAINT DF_TBLSYNCARTICLES_ActiveYN DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_EditedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLSYNCARTICLES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblSyncArticles] PRIMARY KEY ([SyncArticleID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblSyncArticles] - End */
