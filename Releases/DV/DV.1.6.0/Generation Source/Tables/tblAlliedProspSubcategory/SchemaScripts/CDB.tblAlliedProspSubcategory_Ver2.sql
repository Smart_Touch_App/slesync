/* [BUSDTA].[tblAlliedProspSubcategory] - begins */

/* TableDDL - [BUSDTA].[tblAlliedProspSubcategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspSubcategory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspSubcategory]
	(
	  [SubCategoryID] INT NOT NULL IDENTITY(1,1)
	, [SubCategory] VARCHAR(500) NULL
	, [CategoryID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPSUBCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPSUBCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPSUBCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblAlliedProspSubcategory] PRIMARY KEY ([SubCategoryID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblAlliedProspSubcategory] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspSubcategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspSubcategory_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspSubcategory_del]
	(
	  [SubCategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SubCategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspSubcategory] - End */
/* TRIGGERS FOR tblAlliedProspSubcategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspSubcategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblAlliedProspSubcategory_ins
	ON BUSDTA.tblAlliedProspSubcategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblAlliedProspSubcategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblAlliedProspSubcategory_del.SubCategoryID= inserted.SubCategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblAlliedProspSubcategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspSubcategory_upd
	ON BUSDTA.tblAlliedProspSubcategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblAlliedProspSubcategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblAlliedProspSubcategory.SubCategoryID= inserted.SubCategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspSubcategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspSubcategory_dlt
	ON BUSDTA.tblAlliedProspSubcategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblAlliedProspSubcategory_del (SubCategoryID, last_modified )
	SELECT deleted.SubCategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblAlliedProspSubcategory - END */
