
/* [BUSDTA].[CustomerConfig] - begins */

/* TableDDL - [BUSDTA].[CustomerConfig] - Start */
IF OBJECT_ID('[BUSDTA].[CustomerConfig]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[CustomerConfig]	(	  [CustomerID] NUMERIC(8,0) NOT NULL	, [CustomerReference1] NVARCHAR(25) NULL	, [CustomerReference2] NVARCHAR(25) NULL	, [CreatedBy] NUMERIC(8,0) NULL	, [CreatedDatetime] DATETIME NULL	, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL	, PRIMARY KEY ([CustomerID] ASC)	)
END
/* TableDDL - [BUSDTA].[CustomerConfig] - End */
