
/* [BUSDTA].[Feature_Code_Mapping] - begins */

/* TableDDL - [BUSDTA].[Feature_Code_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[Feature_Code_Mapping]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Feature_Code_Mapping]
	(
	  [RequestCode] NCHAR(1) NOT NULL
	, [AuthorizationFormatCode] NCHAR(1) NOT NULL
	, [Feature] NCHAR(100) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [Description] NCHAR(150) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_FEATURE_CODE_MAPPING_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Feature_Code_Mapping] PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC, [Feature] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Feature_Code_Mapping] - End */

/* SHADOW TABLE FOR [BUSDTA].[Feature_Code_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[Feature_Code_Mapping_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Feature_Code_Mapping_del]
	(
	  [RequestCode] NCHAR(1)
	, [AuthorizationFormatCode] NCHAR(1)
	, [Feature] NCHAR(100)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC, [Feature] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Feature_Code_Mapping] - End */
/* TRIGGERS FOR Feature_Code_Mapping - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Feature_Code_Mapping_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Feature_Code_Mapping_ins
	ON BUSDTA.Feature_Code_Mapping AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Feature_Code_Mapping_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Feature_Code_Mapping_del.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Feature_Code_Mapping_del.Feature= inserted.Feature AND BUSDTA.Feature_Code_Mapping_del.RequestCode= inserted.RequestCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Feature_Code_Mapping_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Feature_Code_Mapping_upd
	ON BUSDTA.Feature_Code_Mapping AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Feature_Code_Mapping
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Feature_Code_Mapping.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Feature_Code_Mapping.Feature= inserted.Feature AND BUSDTA.Feature_Code_Mapping.RequestCode= inserted.RequestCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Feature_Code_Mapping_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Feature_Code_Mapping_dlt
	ON BUSDTA.Feature_Code_Mapping AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Feature_Code_Mapping_del (AuthorizationFormatCode, Feature, RequestCode, last_modified )
	SELECT deleted.AuthorizationFormatCode, deleted.Feature, deleted.RequestCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Feature_Code_Mapping - END */

