
/* [BUSDTA].[Feature_Code_Mapping] - begins */

/* TableDDL - [BUSDTA].[Feature_Code_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[Feature_Code_Mapping]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Feature_Code_Mapping]
	(
	  [RequestCode] NVARCHAR (1) NOT NULL
	, [AuthorizationFormatCode] NVARCHAR (1) NOT NULL
	, [Feature] NVARCHAR (100) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [Description] NVARCHAR (150) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC, [Feature] ASC)
	)
END
/* TableDDL - [BUSDTA].[Feature_Code_Mapping] - End */

