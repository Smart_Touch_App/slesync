IF OBJECT_ID('[BUSDTA].[localLastOrders]') IS NULL
BEGIN

CREATE TABLE "BUSDTA"."localLastOrders" 
(
    "chronology"                     integer NOT NULL
   ,"expression"                     char(5) NOT NULL
   ,"OrderID"                        numeric(8,0) NOT NULL
   ,"OrderTypeId"                    numeric(3,0) NOT NULL
   ,"OrderDate"                      date NULL
)
END
 
