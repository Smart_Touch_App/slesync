
/* [Busdta].[tblAlliedProspCategory] - begins */

/* TableDDL - [Busdta].[tblAlliedProspCategory] - Start */
IF OBJECT_ID('[Busdta].[tblAlliedProspCategory]') IS NULL
BEGIN

	CREATE TABLE [Busdta].[tblAlliedProspCategory]
	(
	  [CategoryID] INTEGER NOT NULL 
	, [Category] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CategoryID] ASC)
	)
END
/* TableDDL - [Busdta].[tblAlliedProspCategory] - End */

