
/* [BUSDTA].[Route_Settlement] - begins */

/* TableDDL - [BUSDTA].[Route_Settlement] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement]
	(
	  [SettlementID] decimal (15,0) NOT NULL
	, [SettlementNO] NCHAR(10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Status] NUMERIC(3,0) NULL
	, [SettlementDateTime] DATETIME NULL
	, [UserId] NUMERIC(8,0) NULL
	, [Originator] NUMERIC(8,0) NULL
	, [Verifier] NUMERIC(8,0) NULL
	, [SettlementAmount] decimal (15,4) NULL
	, [ExceptionAmount] decimal (15,4) NULL
	, [Comment] NVARCHAR(100) NULL
	, [OriginatingRoute] NUMERIC(8,0) NOT NULL
	, [partitioningRoute] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_SETTLEMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Settlement] PRIMARY KEY ([SettlementID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Settlement] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement_del]
	(
	  [SettlementID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SettlementID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement] - End */
/* TRIGGERS FOR Route_Settlement - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Settlement_ins
	ON BUSDTA.Route_Settlement AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Settlement_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Settlement_del.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement_del.SettlementID= inserted.SettlementID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Settlement_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_upd
	ON BUSDTA.Route_Settlement AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Settlement
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Settlement.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement.SettlementID= inserted.SettlementID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_dlt
	ON BUSDTA.Route_Settlement AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Settlement_del (RouteId, SettlementID, last_modified )
	SELECT deleted.RouteId, deleted.SettlementID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Settlement - END */
