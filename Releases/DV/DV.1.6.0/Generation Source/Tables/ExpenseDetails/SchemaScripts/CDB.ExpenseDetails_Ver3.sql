
/* [BUSDTA].[ExpenseDetails] - begins */

/* TableDDL - [BUSDTA].[ExpenseDetails] - Start */
IF OBJECT_ID('[BUSDTA].[ExpenseDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ExpenseDetails]
	(
	  [ExpenseId] DECIMAL(15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionId] DECIMAL(15,0) NULL
	, [CategoryId] NUMERIC(8,0) NOT NULL
	, [ExpensesExplanation] NCHAR(100) NULL
	, [ExpenseAmount] DECIMAL(15,0) NULL
	, [StatusId] NUMERIC(8,0) NULL
	, [ExpensesDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC(8,0) NULL
	, [RouteSettlementId] DECIMAL(15,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_EXPENSEDETAILS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ExpenseDetails] PRIMARY KEY ([ExpenseId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ExpenseDetails] - End */

/* SHADOW TABLE FOR [BUSDTA].[ExpenseDetails] - Start */
IF OBJECT_ID('[BUSDTA].[ExpenseDetails_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ExpenseDetails_del]
	(
	  [ExpenseId] DECIMAL(15,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ExpenseId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ExpenseDetails] - End */
/* TRIGGERS FOR ExpenseDetails - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ExpenseDetails_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ExpenseDetails_ins
	ON BUSDTA.ExpenseDetails AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ExpenseDetails_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ExpenseDetails_del.ExpenseId= inserted.ExpenseId AND BUSDTA.ExpenseDetails_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ExpenseDetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ExpenseDetails_upd
	ON BUSDTA.ExpenseDetails AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ExpenseDetails
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ExpenseDetails.ExpenseId= inserted.ExpenseId AND BUSDTA.ExpenseDetails.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ExpenseDetails_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ExpenseDetails_dlt
	ON BUSDTA.ExpenseDetails AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ExpenseDetails_del (ExpenseId, RouteId, last_modified )
	SELECT deleted.ExpenseId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ExpenseDetails - END */

