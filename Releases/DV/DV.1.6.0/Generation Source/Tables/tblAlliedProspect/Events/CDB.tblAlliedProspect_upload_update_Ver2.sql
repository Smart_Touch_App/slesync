
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."tblAlliedProspect"
SET  "CompetitorID" = {ml r."CompetitorID"}, "CategoryID" = {ml r."CategoryID"}, "SubCategoryID" = {ml r."SubCategoryID"}, "BrandID" = {ml r."BrandID"}, "UOMID" = {ml r."UOMID"}, "PackSize" = {ml r."PackSize"}, "CS_PK_LB" = {ml r."CS_PK_LB"}, "Price" = {ml r."Price"}, "UsageMeasurementID" = {ml r."UsageMeasurementID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "AlliedProspectID" = {ml r."AlliedProspectID"} and "ProspectID" = {ml r."ProspectID"}
