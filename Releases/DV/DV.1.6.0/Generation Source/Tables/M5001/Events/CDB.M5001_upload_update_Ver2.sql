
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M5001"
SET "TTID" = {ml r."TTID"}, "TTTYP" = {ml r."TTTYP"}, "TTACTN" = {ml r."TTACTN"}, "TTADSC" = {ml r."TTADSC"}, "TTACTR" = {ml r."TTACTR"}, "TTSTAT" = {ml r."TTSTAT"}, "TTISTRT" = {ml r."TTISTRT"}, "TTIEND" = {ml r."TTIEND"}, "TTCRBY" = {ml r."TTCRBY"}, "TTCRDT" = {ml r."TTCRDT"}, "TTUPBY" = {ml r."TTUPBY"}, "TTUPDT" = {ml r."TTUPDT"}
WHERE "TTKEY" = {ml r."TTKEY"}
 

