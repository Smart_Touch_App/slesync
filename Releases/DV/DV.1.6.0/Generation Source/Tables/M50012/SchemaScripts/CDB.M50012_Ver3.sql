
/* [BUSDTA].[M50012] - begins */

/* TableDDL - [BUSDTA].[M50012] - Start */
IF OBJECT_ID('[BUSDTA].[M50012]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50012]
	(
	  [TDID] DECIMAL(15,0) NOT NULL
	, [TDROUT] VARCHAR(10) NOT NULL
	, [TDTYP] NCHAR(50) NOT NULL
	, [TDCLASS] NCHAR(100) NULL
	, [TDDTLS] NVARCHAR(MAX) NULL
	, [TDSTRTTM] DATETIME2(7) NULL
	, [TDENDTM] DATETIME2(7) NULL
	, [TDSTID] NCHAR(15) NULL
	, [TDAN8] NCHAR(15) NULL
	, [TDSTTLID] NCHAR(15) NULL
	, [TDPNTID] DECIMAL(15,0) NULL
	, [TDSTAT] NCHAR(100) NULL
	, [TDREFHDRID] DECIMAL(15,0) NULL
	, [TDREFHDRTYP] NCHAR(30) NULL
	, [TDCRBY] NCHAR(50) NULL
	, [TDCRDT] DATETIME NULL
	, [TDUPBY] NCHAR(50) NULL
	, [TDUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M50012_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M50012] PRIMARY KEY ([TDID] ASC, [TDROUT] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M50012] - End */

/* SHADOW TABLE FOR [BUSDTA].[M50012] - Start */
IF OBJECT_ID('[BUSDTA].[M50012_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50012_del]
	(
	  [TDID] DECIMAL(15,0)
	, [TDROUT] VARCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([TDID] ASC, [TDROUT] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M50012] - End */
/* TRIGGERS FOR M50012 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M50012_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M50012_ins
	ON BUSDTA.M50012 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M50012_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M50012_del.TDID= inserted.TDID AND BUSDTA.M50012_del.TDROUT= inserted.TDROUT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M50012_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50012_upd
	ON BUSDTA.M50012 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M50012
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M50012.TDID= inserted.TDID AND BUSDTA.M50012.TDROUT= inserted.TDROUT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M50012_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50012_dlt
	ON BUSDTA.M50012 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M50012_del (TDID, TDROUT, last_modified )
	SELECT deleted.TDID, deleted.TDROUT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M50012 - END */

