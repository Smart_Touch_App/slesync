/* [BUSDTA].[tblCoffeeVolume] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeVolume] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeVolume]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeVolume]
	(
	  [CoffeeVolumeID] INTEGER NOT NULL 
	, [Volume] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CoffeeVolumeID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblCoffeeVolume] - End */
