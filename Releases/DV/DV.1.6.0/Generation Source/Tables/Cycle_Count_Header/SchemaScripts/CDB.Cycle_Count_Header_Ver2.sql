
/* [BUSDTA].[Cycle_Count_Header] - begins */

/* TableDDL - [BUSDTA].[Cycle_Count_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Header]
	(
	  [CycleCountID] DECIMAL(15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(3,0) NOT NULL
	, [IsCountInitiated] NCHAR(1) NULL
	, [CycleCountTypeId] NUMERIC(3,0) NOT NULL
	, [InitiatorId] NUMERIC(8,0) NULL
	, [CycleCountDatetime] DATETIME NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [JDEQty] DECIMAL(15,0) NULL
	, [JDEUM] NCHAR(2) NULL
	, [JDECycleCountNum] DECIMAL(15,0) NULL
	, [JDECycleCountDocType] NCHAR(2) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CYCLE_COUNT_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cycle_Count_Header] PRIMARY KEY ([CycleCountID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Cycle_Count_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Header_del]
	(
	  [CycleCountID] DECIMAL(15,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CycleCountID] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Header] - End */
/* TRIGGERS FOR Cycle_Count_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cycle_Count_Header_ins
	ON BUSDTA.Cycle_Count_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cycle_Count_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cycle_Count_Header_del.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cycle_Count_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Header_upd
	ON BUSDTA.Cycle_Count_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cycle_Count_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cycle_Count_Header.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Header_dlt
	ON BUSDTA.Cycle_Count_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cycle_Count_Header_del (CycleCountID, RouteId, last_modified )
	SELECT deleted.CycleCountID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cycle_Count_Header - END */

