
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F56M0001"
SET "FFHMCU" = {ml r."FFHMCU"}, "FFBUVAL" = {ml r."FFBUVAL"}, "FFAN8" = {ml r."FFAN8"}, "FFPA8" = {ml r."FFPA8"}, "FFSTOP" = {ml r."FFSTOP"}, "FFZON" = {ml r."FFZON"}, "FFLOCN" = {ml r."FFLOCN"}, "FFLOCF" = {ml r."FFLOCF"}, "FFEV01" = {ml r."FFEV01"}, "FFEV02" = {ml r."FFEV02"}, "FFEV03" = {ml r."FFEV03"}, "FFMATH01" = {ml r."FFMATH01"}, "FFMATH02" = {ml r."FFMATH02"}, "FFMATH03" = {ml r."FFMATH03"}, "FFCXPJ" = {ml r."FFCXPJ"}, "FFCLRJ" = {ml r."FFCLRJ"}, "FFDTE" = {ml r."FFDTE"}
WHERE "FFUSER" = {ml r."FFUSER"} AND "FFROUT" = {ml r."FFROUT"} AND "FFMCU" = {ml r."FFMCU"}
 

