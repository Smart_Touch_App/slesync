/* [BUSDTA].[Prospect_Contact] - begins */

/* TableDDL - [BUSDTA].[Prospect_Contact] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Contact]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Contact]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [ProspectContactId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ShowOnDashboard] NVARCHAR (1) NULL
	, [FirstName] NVARCHAR (75) NULL
	, [MiddleName] NVARCHAR (75) NULL
	, [LastName] NVARCHAR (75) NULL
	, [Title] NVARCHAR(50) NULL
	, [IsActive] NVARCHAR (1) NOT NULL
	, [AreaCode1] NVARCHAR (6) NULL
	, [Phone1] NVARCHAR (20) NULL
	, [Extn1] NVARCHAR (8) NULL
	, [PhoneType1] NUMERIC(3,0) NULL
	, [IsDefaultPhone1] NVARCHAR (1) NULL
	, [AreaCode2] NVARCHAR (6) NULL
	, [Phone2] NVARCHAR (20) NULL
	, [Extn2] NVARCHAR (8) NULL
	, [PhoneType2] NUMERIC(3,0) NULL
	, [IsDefaultPhone2] NVARCHAR (1) NULL
	, [AreaCode3] NVARCHAR (6) NULL
	, [Phone3] NVARCHAR (20) NULL
	, [Extn3] NVARCHAR (8) NULL
	, [PhoneType3] NUMERIC(3,0) NULL
	, [IsDefaultPhone3] NVARCHAR (1) NULL
	, [AreaCode4] NVARCHAR (6) NULL
	, [Phone4] NVARCHAR (20) NULL
	, [Extn4] NVARCHAR (8) NULL
	, [PhoneType4] NUMERIC(3,0) NULL
	, [IsDefaultPhone4] NVARCHAR (1) NULL
	, [EmailID1] NVARCHAR (256) NULL
	, [EmailType1] NUMERIC(3,0) NULL
	, [IsDefaultEmail1] NVARCHAR (1) NULL
	, [EmailID2] NVARCHAR (256) NULL
	, [EmailType2] NUMERIC(3,0) NULL
	, [IsDefaultEmail2] NVARCHAR (1) NULL
	, [EmailID3] NVARCHAR (256) NULL
	, [EmailType3] NUMERIC(3,0) NULL
	, [IsDefaultEmail3] NVARCHAR (1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ProspectId] ASC, [ProspectContactId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Prospect_Contact] - End */
