
 

SELECT 
"busdta"."PickOrder"."PickOrder_Id"  
,"busdta"."PickOrder"."Order_ID"  
,"busdta"."PickOrder"."RouteId"  
,"busdta"."PickOrder"."Item_Number"  
,"busdta"."PickOrder"."Order_Qty"  
,"busdta"."PickOrder"."Order_UOM"  
,"busdta"."PickOrder"."Picked_Qty_Primary_UOM"  
,"busdta"."PickOrder"."Primary_UOM"  
,"busdta"."PickOrder"."Order_Qty_Primary_UOM"  
,"busdta"."PickOrder"."On_Hand_Qty_Primary"  
,"busdta"."PickOrder"."Last_Scan_Mode"  
,"busdta"."PickOrder"."Item_Scan_Sequence"  
,"busdta"."PickOrder"."Picked_By"  
,"busdta"."PickOrder"."IsOnHold"  
,"busdta"."PickOrder"."Reason_Code_Id"  
,"busdta"."PickOrder"."ManuallyPickCount"    
FROM busdta.Order_Header, busdta.PickOrder, busdta.Route_Master rm, busdta.Customer_Route_Map crm  
WHERE "busdta"."PickOrder"."last_modified" >= {ml s.last_table_download}  
AND CustShipToID = crm.RelatedAddressBookNumber and busdta.Order_Header.OrderID = busdta.PickOrder.Order_ID 
AND crm.BranchNumber = rm.BranchNumber  
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} ) 
 
