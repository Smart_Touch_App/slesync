/* [BUSDTA].[ApprovalCodeLog] - begins */

/* TableDDL - [BUSDTA].[ApprovalCodeLog] - Start */
IF OBJECT_ID('[BUSDTA].[ApprovalCodeLog]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ApprovalCodeLog]
	(
	  [LogID] DECIMAL(15,0) NOT NULL
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [ApprovalCode] NCHAR(8) NOT NULL
	, [RequestCode] NCHAR(8) NOT NULL
	, [Feature] NCHAR(50) NOT NULL
	, [Amount] DECIMAL(15,4) NOT NULL
	, [RequestedByUser] NUMERIC(8,0) NOT NULL
	, [RequestedForCustomer] NUMERIC(8,0) NOT NULL
	, [Payload] NVARCHAR(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_APPROVALCODELOG_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ApprovalCodeLog] PRIMARY KEY ([LogID] ASC, [RouteID] ASC)
	)

END
/* TableDDL - [BUSDTA].[ApprovalCodeLog] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[ApprovalCodeLog] - Start */
IF OBJECT_ID('[BUSDTA].[ApprovalCodeLog_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ApprovalCodeLog_del]
	(
	  [LogID] NUMERIC(15,0)
	, [RouteID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([LogID] ASC, [RouteID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ApprovalCodeLog] - End */
/* TRIGGERS FOR ApprovalCodeLog - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ApprovalCodeLog_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ApprovalCodeLog_ins
	ON BUSDTA.ApprovalCodeLog AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ApprovalCodeLog_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ApprovalCodeLog_del.LogID= inserted.LogID AND BUSDTA.ApprovalCodeLog_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ApprovalCodeLog_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ApprovalCodeLog_upd
	ON BUSDTA.ApprovalCodeLog AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ApprovalCodeLog
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ApprovalCodeLog.LogID= inserted.LogID AND BUSDTA.ApprovalCodeLog.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ApprovalCodeLog_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ApprovalCodeLog_dlt
	ON BUSDTA.ApprovalCodeLog AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ApprovalCodeLog_del (LogID, RouteID, last_modified )
	SELECT deleted.LogID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ApprovalCodeLog - END */
