/* [BUSDTA].[tblCompetitor] - begins */

/* TableDDL - [BUSDTA].[tblCompetitor] - Start */
IF OBJECT_ID('[BUSDTA].[tblCompetitor]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCompetitor]
	(
	  [CompetitorID] INTEGER NOT NULL
	, [Competitor] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CompetitorID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblCompetitor] - End */
