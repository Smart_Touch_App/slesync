/* [BUSDTA].[tblCSPKLB] - begins */

/* TableDDL - [BUSDTA].[tblCSPKLB] - Start */
IF OBJECT_ID('[BUSDTA].[tblCSPKLB]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCSPKLB]
	(
	  [CSPKLBID] INT NOT NULL IDENTITY(1,1)
	, [CSPKLBType] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCSPKLB_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCSPKLB_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCSPKLB_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCSPKLB] PRIMARY KEY ([CSPKLBID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblCSPKLB] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCSPKLB] - Start */
IF OBJECT_ID('[BUSDTA].[tblCSPKLB_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCSPKLB_del]
	(
	  [CSPKLBID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CSPKLBID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCSPKLB] - End */
/* TRIGGERS FOR tblCSPKLB - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCSPKLB_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCSPKLB_ins
	ON BUSDTA.tblCSPKLB AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCSPKLB_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCSPKLB_del.CSPKLBID= inserted.CSPKLBID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCSPKLB_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCSPKLB_upd
	ON BUSDTA.tblCSPKLB AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCSPKLB
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCSPKLB.CSPKLBID= inserted.CSPKLBID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCSPKLB_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCSPKLB_dlt
	ON BUSDTA.tblCSPKLB AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCSPKLB_del (CSPKLBID, last_modified )
	SELECT deleted.CSPKLBID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCSPKLB - END */
