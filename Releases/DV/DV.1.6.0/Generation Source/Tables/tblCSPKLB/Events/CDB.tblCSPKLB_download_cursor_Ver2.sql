 
SELECT "BUSDTA"."tblCSPKLB"."CSPKLBID"
,"BUSDTA"."tblCSPKLB"."CSPKLBType"
,"BUSDTA"."tblCSPKLB"."CreatedBy"
,"BUSDTA"."tblCSPKLB"."CreatedDatetime"
,"BUSDTA"."tblCSPKLB"."UpdatedBy"
,"BUSDTA"."tblCSPKLB"."UpdatedDatetime"

FROM "BUSDTA"."tblCSPKLB"
WHERE "BUSDTA"."tblCSPKLB"."last_modified">= {ml s.last_table_download}
