 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Settlement_Detail"
("SettlementId", "SettlementDetailId", "UserId", "VerificationNum", "CashAmount", "CheckAmount", "MoneyOrderAmount", "TotalVerified", "Expenses", "Payments", "OverShortAmount", "VerSignature", "RouteId", "OriginatingRoute", "partitioningRoute", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."SettlementId"}, {ml r."SettlementDetailId"}, {ml r."UserId"}, {ml r."VerificationNum"}, {ml r."CashAmount"}, {ml r."CheckAmount"}, {ml r."MoneyOrderAmount"}, {ml r."TotalVerified"}, {ml r."Expenses"}, {ml r."Payments"}, {ml r."OverShortAmount"}, {ml r."VerSignature"}, {ml r."RouteId"}, {ml r."OriginatingRoute"}, {ml r."partitioningRoute"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
