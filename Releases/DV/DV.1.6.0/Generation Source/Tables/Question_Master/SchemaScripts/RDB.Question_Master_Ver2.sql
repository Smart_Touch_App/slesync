/* [BUSDTA].[Question_Master] - begins */

/* TableDDL - [BUSDTA].[Question_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Master]
	(
	  [QuestionId] NUMERIC(8,0) NOT NULL
	, [QuestionTitle] NVARCHAR (100) NULL
	, [QuestionDescription] NVARCHAR (100) NULL
	, [IsMandatory] BIT NULL
	, [IsMultivalue] BIT NULL
	, [IsDescriptive] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([QuestionId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Question_Master] - End */
