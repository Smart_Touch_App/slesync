/* [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - begins */

/* TableDDL - [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[SalesOrder_ReturnOrder_Mapping]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[SalesOrder_ReturnOrder_Mapping]
	(
	  [SalesOrderId] decimal (15,0) NOT NULL
	, [ReturnOrderID] decimal (15,0) NOT NULL
	, [OrderDetailID] NUMERIC(8,0) NOT NULL
	, [ItemID] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([SalesOrderId] ASC, [ReturnOrderID] ASC, [OrderDetailID] ASC)
	)
END
/* TableDDL - [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - End */
