/* [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - begins */

/* TableDDL - [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[SalesOrder_ReturnOrder_Mapping]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[SalesOrder_ReturnOrder_Mapping]
	(
	  [SalesOrderId] decimal (15,0) NOT NULL
	, [ReturnOrderID] decimal (15,0) NOT NULL
	, [OrderDetailID] NUMERIC(8,0) NOT NULL
	, [ItemID] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_SALESORDER_RETURNORDER_MAPPING_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_SalesOrder_ReturnOrder_Mapping] PRIMARY KEY ([SalesOrderId] ASC, [ReturnOrderID] ASC, [OrderDetailID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - End */

/* SHADOW TABLE FOR [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - Start */
IF OBJECT_ID('[BUSDTA].[SalesOrder_ReturnOrder_Mapping_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[SalesOrder_ReturnOrder_Mapping_del]
	(
	  [SalesOrderId] decimal (15,0)
	, [ReturnOrderID] decimal (15,0)
	, [OrderDetailID] decimal (7,4)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SalesOrderId] ASC, [ReturnOrderID] ASC, [OrderDetailID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[SalesOrder_ReturnOrder_Mapping] - End */
/* TRIGGERS FOR SalesOrder_ReturnOrder_Mapping - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.SalesOrder_ReturnOrder_Mapping_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.SalesOrder_ReturnOrder_Mapping_ins
	ON BUSDTA.SalesOrder_ReturnOrder_Mapping AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.SalesOrder_ReturnOrder_Mapping_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.SalesOrder_ReturnOrder_Mapping_del.OrderDetailID= inserted.OrderDetailID AND BUSDTA.SalesOrder_ReturnOrder_Mapping_del.ReturnOrderID= inserted.ReturnOrderID AND BUSDTA.SalesOrder_ReturnOrder_Mapping_del.SalesOrderId= inserted.SalesOrderId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.SalesOrder_ReturnOrder_Mapping_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.SalesOrder_ReturnOrder_Mapping_upd
	ON BUSDTA.SalesOrder_ReturnOrder_Mapping AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.SalesOrder_ReturnOrder_Mapping
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.SalesOrder_ReturnOrder_Mapping.OrderDetailID= inserted.OrderDetailID AND BUSDTA.SalesOrder_ReturnOrder_Mapping.ReturnOrderID= inserted.ReturnOrderID AND BUSDTA.SalesOrder_ReturnOrder_Mapping.SalesOrderId= inserted.SalesOrderId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.SalesOrder_ReturnOrder_Mapping_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.SalesOrder_ReturnOrder_Mapping_dlt
	ON BUSDTA.SalesOrder_ReturnOrder_Mapping AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.SalesOrder_ReturnOrder_Mapping_del (OrderDetailID, ReturnOrderID, SalesOrderId, last_modified )
	SELECT deleted.OrderDetailID, deleted.ReturnOrderID, deleted.SalesOrderId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR SalesOrder_ReturnOrder_Mapping - END */
