/* [BUSDTA].[tblEquipmentType] - begins */

/* TableDDL - [BUSDTA].[tblEquipmentType] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentType]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentType]
	(
	  [EquipmentTypeID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentType] VARCHAR(200) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentType] PRIMARY KEY ([EquipmentTypeID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblEquipmentType] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentType] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentType_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentType_del]
	(
	  [EquipmentTypeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentTypeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentType] - End */
/* TRIGGERS FOR tblEquipmentType - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentType_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblEquipmentType_ins
	ON BUSDTA.tblEquipmentType AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblEquipmentType_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblEquipmentType_del.EquipmentTypeID= inserted.EquipmentTypeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblEquipmentType_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentType_upd
	ON BUSDTA.tblEquipmentType AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblEquipmentType
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblEquipmentType.EquipmentTypeID= inserted.EquipmentTypeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentType_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentType_dlt
	ON BUSDTA.tblEquipmentType AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblEquipmentType_del (EquipmentTypeID, last_modified )
	SELECT deleted.EquipmentTypeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentType - END */
