/* [BUSDTA].[Route_User_Map] - begins */

/* TableDDL - [BUSDTA].[Route_User_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_User_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_User_Map]
	(
	  [App_user_id] INTEGER NOT NULL
	, [Route_Id] VARCHAR(8) NOT NULL
	, [Active] INTEGER NULL
	, [Default_Route] INTEGER NULL
	, PRIMARY KEY ([App_user_id] ASC, [Route_Id] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_User_Map] - End */
