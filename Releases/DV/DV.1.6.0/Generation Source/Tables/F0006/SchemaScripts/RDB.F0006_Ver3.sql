
/* [BUSDTA].[F0006] - begins */

/* TableDDL - [BUSDTA].[F0006] - Start */
IF OBJECT_ID('[BUSDTA].[F0006]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0006]
	(
	  [MCMCU] NVARCHAR (12) NOT NULL
	, [MCSTYL] NVARCHAR (2) NULL
	, [MCLDM] NVARCHAR (1) NULL
	, [MCCO] NVARCHAR (5) NULL
	, [MCAN8] FLOAT NULL
	, [MCDL01] NVARCHAR (30) NULL
	, [MCRP01] NVARCHAR (3) NULL
	, [MCRP02] NVARCHAR (3) NULL
	, [MCRP03] NVARCHAR (3) NULL
	, [MCRP04] NVARCHAR (3) NULL
	, [MCRP05] NVARCHAR (3) NULL
	, [MCRP06] NVARCHAR (3) NULL
	, [MCRP07] NVARCHAR (3) NULL
	, [MCRP08] NVARCHAR (3) NULL
	, [MCRP09] NVARCHAR (3) NULL
	, [MCRP10] NVARCHAR (3) NULL
	, [MCRP11] NVARCHAR (3) NULL
	, [MCRP12] NVARCHAR (3) NULL
	, [MCRP13] NVARCHAR (3) NULL
	, [MCRP14] NVARCHAR (3) NULL
	, [MCRP15] NVARCHAR (3) NULL
	, [MCRP16] NVARCHAR (3) NULL
	, [MCRP17] NVARCHAR (3) NULL
	, [MCRP18] NVARCHAR (3) NULL
	, [MCRP19] NVARCHAR (3) NULL
	, [MCRP20] NVARCHAR (3) NULL
	, [MCRP21] NVARCHAR (10) NULL
	, [MCRP22] NVARCHAR (10) NULL
	, [MCRP23] NVARCHAR (10) NULL
	, [MCRP24] NVARCHAR (10) NULL
	, [MCRP25] NVARCHAR (10) NULL
	, [MCRP26] NVARCHAR (10) NULL
	, [MCRP27] NVARCHAR (10) NULL
	, [MCRP28] NVARCHAR (10) NULL
	, [MCRP29] NVARCHAR (10) NULL
	, [MCRP30] NVARCHAR (10) NULL	
	, PRIMARY KEY ([MCMCU] ASC)
	)
END
/* TableDDL - [BUSDTA].[F0006] - End */

