/* [BUSDTA].[Prospect_Master] - begins */

/* TableDDL - [BUSDTA].[Prospect_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Master]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ProspectName] NCHAR(100) NULL
	, [AcctSegmentType] NUMERIC(3,0) NULL
	, [BuyingGroup] NCHAR(50) NULL
	, [LocationCount] NCHAR(10) NULL
	, [CategoryCode01] NCHAR(3) NULL
	, [OperatingUnit] NCHAR(3) NULL
	, [Branch] NCHAR(3) NULL
	, [District] NCHAR(3) NULL
	, [Region] NCHAR(3) NULL
	, [Reporting] NCHAR(3) NULL
	, [Chain] NCHAR(3) NULL
	, [BrewmaticAgentCode] NCHAR(3) NULL
	, [NTR] NCHAR(3) NULL
	, [ProspectTaxGrp] NCHAR(3) NULL
	, [CategoryCode12] NCHAR(3) NULL
	, [APCheckCode] NCHAR(3) NULL
	, [CategoryCode14] NCHAR(3) NULL
	, [CategoryCode15] NCHAR(3) NULL
	, [CategoryCode16] NCHAR(3) NULL
	, [CategoryCode17] NCHAR(3) NULL
	, [CategoryCode18] NCHAR(3) NULL
	, [CategoryCode19] NCHAR(3) NULL
	, [CategoryCode20] NCHAR(3) NULL
	, [CategoryCode21] NCHAR(3) NULL
	, [CategoryCode22] NCHAR(3) NULL
	, [SpecialEquipment] NCHAR(3) NULL
	, [CAProtection] NCHAR(3) NULL
	, [ProspectGroup] NCHAR(3) NULL
	, [SPCommisFBType] NCHAR(3) NULL
	, [AlliedDiscount] NCHAR(3) NULL
	, [CoffeeVolume] NCHAR(3) NULL
	, [EquipmentProgPts] NCHAR(3) NULL
	, [LiquidCoffee] NCHAR(3) NULL
	, [PriceProtection] NCHAR(3) NULL
	, [POSUpCharge] NCHAR(3) NULL
	, [SpecialCCP] NCHAR(3) NULL
	, [TaxGroup] NCHAR(3) NULL
	, [IsPricesetup] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Master] PRIMARY KEY ([ProspectId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Master_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Master] - End */
/* TRIGGERS FOR Prospect_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Master_ins
	ON BUSDTA.Prospect_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Master_del.ProspectId= inserted.ProspectId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Master_upd
	ON BUSDTA.Prospect_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Master.ProspectId= inserted.ProspectId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Master_dlt
	ON BUSDTA.Prospect_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Master_del (ProspectId, last_modified )
	SELECT deleted.ProspectId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Master - END */
