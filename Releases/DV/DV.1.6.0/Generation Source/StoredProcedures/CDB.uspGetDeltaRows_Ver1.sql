﻿
CREATE PROCEDURE [dbo].[uspGetDeltaRows] 
(
	@DeviceID VARCHAR(100),
	@Script_Version VARCHAR(100)='SLE_RemoteDB'
)
AS
BEGIN
DECLARE @remote_id VARCHAR(100)=(SELECT TOP 1 Remote_Id FROM BUSDTA.Route_Device_Map WHERE Device_Id=@DeviceID);
DECLARE @DeltaStats TABLE (RouteID VARCHAR(100), CTSchema VARCHAR(128), CTTable VARCHAR(128), CTDeltaRows BIGINT NOT NULL, Syncdate DATETIME DEFAULT GETDATE()) 

INSERT INTO @DeltaStats(RouteID, CTSchema, CTTable, CTDeltaRows)
EXEC [dbo].[uspDeltaRows] @remote_id=@remote_id, @script_version=@script_version;
--SELECT * FROM @DeltaStats

	SELECT RouteID,rdm.Route_Id,  /*F56M0001.FFMCU,*/ ISNULL(ag.ApplicationGroup, 'Other Group')AS ApplicationGroup, 
		SUM(dt.CTDeltaRows) AS DeltaRows, 
		IsMandatorySync=CASE WHEN ag.IsMandatorySync=1 THEN 1 ELSE ISNULL(gt.IsMandatorySync,0) END
		FROM @DeltaStats AS dt
		LEFT JOIN tblGroupTables AS gt ON dt.CTSchema=gt.RefDBSchema AND dt.CTTable=gt.ConDBTable
		LEFT JOIN tblApplicationGroup AS ag ON ag.ApplicationGroupID=gt.ApplicationGroupID
		LEFT JOIN BUSDTA.F56M0001 AS F56M0001 ON F56M0001.FFMCU=dt.RouteID
		LEFT JOIN BUSDTA.Route_Device_Map AS rdm ON rdm.Route_Id=F56M0001.FFUSER AND rdm.Device_Id=@DeviceID
	WHERE 
		((RouteID IS NULL AND rdm.Route_Id IS NULL)OR (RouteID IS NOT NULL AND rdm.Route_Id IS NOT NULL))
	GROUP BY RouteID,rdm.Route_Id,  /*F56M0001.FFMCU,*/ ag.ApplicationGroup, ag.SortOrder, CASE WHEN ag.IsMandatorySync=1 THEN 1 ELSE ISNULL(gt.IsMandatorySync,0) END
	HAVING SUM(dt.CTDeltaRows)>0
	ORDER BY ISNULL(ag.SortOrder, 9999)/*and make other group last in the list*/
END
GO