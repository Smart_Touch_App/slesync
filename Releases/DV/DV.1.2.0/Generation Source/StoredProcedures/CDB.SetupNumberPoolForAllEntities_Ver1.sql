SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetupNumberPoolForAllEntities]
as
Begin
	Declare @EntityBucketMasterId as numeric(4,0)=1
	Declare EntityBucketCursor CURSOR FOR
	Select EntityBucketMasterId from   BUSDTA.Entity_Bucket_Master (Nolock)
	OPEN EntityBucketCursor
		FETCH NEXT FROM EntityBucketCursor INTO @EntityBucketMasterId
			WHILE @@FETCH_STATUS = 0
				BEGIN
					EXEC [dbo].[SetupNumberPoolForEntity] @EntityBucketMasterId
					FETCH NEXT FROM EntityBucketCursor INTO @EntityBucketMasterId
				END
		CLOSE EntityBucketCursor
	DEALLOCATE EntityBucketCursor
End
GO


