Create type busdta.EntityNumberUpdateType as table
(
    [ID] [int]  NOT NULL,
	[RouteId] [varchar](10) NULL,
	[Bucket] [varchar](30) NULL,
	[EntityBucketMasterId] [varchar](30) NULL,
	[RangeStart] [int] NULL,
	[RangeEnd] [int] NULL,
	[CurrentAllocated] [decimal](8,0) NULL,
	[IsConsumed] [bit] NULL ,
	[ConsumedOnDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] NUMERIC(8,0) NULL,
	[CreatedDatetime] DATETIME NULL,
	[UpdatedBy] NUMERIC(8,0) NULL,
	[UpdatedDatetime] DATETIME NULL
)