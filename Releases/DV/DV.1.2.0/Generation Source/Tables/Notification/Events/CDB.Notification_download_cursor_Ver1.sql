

 
SELECT "BUSDTA"."Notification"."NotificationID"
,"BUSDTA"."Notification"."RouteID"
,"BUSDTA"."Notification"."Title"
,"BUSDTA"."Notification"."Description"
,"BUSDTA"."Notification"."TypeID"
,"BUSDTA"."Notification"."Category"
,"BUSDTA"."Notification"."SubCategory"
,"BUSDTA"."Notification"."Read"
,"BUSDTA"."Notification"."ValidTill"
,"BUSDTA"."Notification"."Acknowledged"
,"BUSDTA"."Notification"."Initiator"
,"BUSDTA"."Notification"."CreatedBy"
,"BUSDTA"."Notification"."CreatedDatetime"
,"BUSDTA"."Notification"."UpdatedBy"
,"BUSDTA"."Notification"."UpdatedDatetime"

FROM "BUSDTA"."Notification"
WHERE "BUSDTA"."Notification"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Notification"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
