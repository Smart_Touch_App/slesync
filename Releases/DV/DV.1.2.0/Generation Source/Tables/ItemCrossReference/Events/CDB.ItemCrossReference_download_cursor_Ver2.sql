
     SELECT "BUSDTA"."ItemCrossReference"."ItemReferenceID"  ,"BUSDTA"."ItemCrossReference"."CrossReferenceID"  ,
     "BUSDTA"."ItemCrossReference"."MobileType"  ,"BUSDTA"."ItemCrossReference"."EffectiveFrom"  ,"BUSDTA"."ItemCrossReference"."EffectiveThru"  ,
     "BUSDTA"."ItemCrossReference"."CrossData"  ,"BUSDTA"."ItemCrossReference"."AddressNumber"  ,
     "BUSDTA"."ItemCrossReference"."CrossReferenceType"  ,"BUSDTA"."ItemCrossReference"."ItemNumber"  ,
     "BUSDTA"."ItemCrossReference"."ItemRevisionLevel"  ,"BUSDTA"."ItemCrossReference"."CreatedBy"  ,
     "BUSDTA"."ItemCrossReference"."CreatedDatetime"  ,"BUSDTA"."ItemCrossReference"."UpdatedBy"  ,
     "BUSDTA"."ItemCrossReference"."UpdatedDatetime"    
     FROM "BUSDTA"."ItemCrossReference"  
     WHERE "BUSDTA"."ItemCrossReference"."last_modified">= {ml s.last_table_download}     