/* [dbo].[tblAlliedProspSubcategory] - begins */

/* TableDDL - [dbo].[tblAlliedProspSubcategory] - Start */
IF OBJECT_ID('[dbo].[tblAlliedProspSubcategory]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblAlliedProspSubcategory]
	(
	  [SubCategoryID] INTEGER NOT NULL 
	, [SubCategory] VARCHAR(500) NULL
	, [CategoryID] INTEGER NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([SubCategoryID] ASC)
	)
END
/* TableDDL - [dbo].[tblAlliedProspSubcategory] - End */
