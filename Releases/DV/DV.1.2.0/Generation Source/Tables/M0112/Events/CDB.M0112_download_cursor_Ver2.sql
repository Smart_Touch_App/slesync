
     SELECT "BUSDTA"."M0112"."NDAN8"  ,"BUSDTA"."M0112"."NDID"  ,"BUSDTA"."M0112"."NDDTTM"  ,"BUSDTA"."M0112"."NDDTLS"  ,
     "BUSDTA"."M0112"."NDTYP"  ,"BUSDTA"."M0112"."NDDFLT"  ,"BUSDTA"."M0112"."NDCRBY"  ,"BUSDTA"."M0112"."NDCRDT"  ,
     "BUSDTA"."M0112"."NDUPBY"  ,"BUSDTA"."M0112"."NDUPDT"    
     FROM BUSDTA.M0112, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."M0112"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or crm."last_modified" >= {ml s.last_table_download})   AND NDAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     