
/* [BUSDTA].[Category_Type] - begins */

/* TableDDL - [BUSDTA].[Category_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Category_Type]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Category_Type]
	(
	  [CategoryTypeID] NUMERIC(8,0) NOT NULL
	, [CategoryTypeCD] VARCHAR(5) NOT NULL
	, [CategoryCodeType] NVARCHAR(20) NULL
	, [CategoryTypeDESC] NVARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CATEGORY_TYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Category_Type] PRIMARY KEY ([CategoryTypeCD] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Category_Type] - End */

/* SHADOW TABLE FOR [BUSDTA].[Category_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Category_Type_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Category_Type_del]
	(
	  [CategoryTypeCD] VARCHAR(5)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CategoryTypeCD] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Category_Type] - End */
/* TRIGGERS FOR Category_Type - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Category_Type_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Category_Type_ins
	ON BUSDTA.Category_Type AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Category_Type_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Category_Type_del.CategoryTypeCD= inserted.CategoryTypeCD
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Category_Type_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Category_Type_upd
	ON BUSDTA.Category_Type AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Category_Type
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Category_Type.CategoryTypeCD= inserted.CategoryTypeCD');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Category_Type_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Category_Type_dlt
	ON BUSDTA.Category_Type AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Category_Type_del (CategoryTypeCD, last_modified )
	SELECT deleted.CategoryTypeCD, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Category_Type - END */

