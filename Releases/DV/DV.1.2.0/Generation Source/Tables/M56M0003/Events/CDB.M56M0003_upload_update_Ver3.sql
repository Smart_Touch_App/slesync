
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0003"
SET "RSSN" = {ml r."RSSN"}, "RSDDC" = {ml r."RSDDC"}, "RSCRBY" = {ml r."RSCRBY"}, "RSCRDT" = {ml r."RSCRDT"}, "RSUPBY" = {ml r."RSUPBY"}, "RSUPDT" = {ml r."RSUPDT"}
WHERE "RSROUT" = {ml r."RSROUT"} AND "RSAN8" = {ml r."RSAN8"} AND "RSWN" = {ml r."RSWN"} AND "RSDN" = {ml r."RSDN"}
 

