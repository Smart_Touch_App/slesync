 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
("SalesOrderId", "ReturnOrderID", "OrderDetailID", "ItemID", "RouteId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."SalesOrderId"}, {ml r."ReturnOrderID"}, {ml r."OrderDetailID"}, {ml r."ItemID"}, {ml r."RouteId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
