/* [BUSDTA].[Question_Template] - begins */

/* TableDDL - [BUSDTA].[Question_Template] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Template]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Template]
	(
	  [TemplateId] NUMERIC(8,0) NOT NULL
	, [TemplateName] NVARCHAR (100) NULL
	, [QuestionId] NUMERIC(8,0) NOT NULL
	, [ResponseID] NUMERIC(8,0) NOT NULL
	, [RevisionId] NUMERIC(2,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([TemplateId] ASC, [QuestionId] ASC, [ResponseID] ASC, [RevisionId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Question_Template] - End */
