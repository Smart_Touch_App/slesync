
/* [BUSDTA].[CheckDetails] - begins */

/* TableDDL - [BUSDTA].[CheckDetails] - Start */
IF OBJECT_ID('[BUSDTA].[CheckDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[CheckDetails]
	(
	  [CheckDetailsId] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [CheckDetailsNumber] VARCHAR(10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CheckNumber] NVARCHAR (10) NULL
	, [CustomerId] NVARCHAR (10) NULL
	, [CustomerName] NVARCHAR (10) NULL
	, [CheckAmount] NUMERIC(8,2) NULL
	, [CheckDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([CheckDetailsId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[CheckDetails] - End */

