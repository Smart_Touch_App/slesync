
/* [BUSDTA].[F4075] - begins */

/* TableDDL - [BUSDTA].[F4075] - Start */
IF OBJECT_ID('[BUSDTA].[F4075]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4075]
	(
	  [VBVBT] NCHAR(10) NOT NULL
	, [VBCRCD] NCHAR(3) NULL
	, [VBUOM] NCHAR(2) NULL
	, [VBUPRC] FLOAT NULL
	, [VBEFTJ] NUMERIC(18,0) NOT NULL
	, [VBEXDJ] NUMERIC(18,0) NULL
	, [VBAPRS] NCHAR(1) NULL
	, [VBUPMJ] NUMERIC(18,0) NOT NULL
	, [VBTDAY] NUMERIC(6,0) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4075_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4075] PRIMARY KEY ([VBVBT] ASC, [VBEFTJ] ASC, [VBUPMJ] ASC, [VBTDAY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4075] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4075] - Start */
IF OBJECT_ID('[BUSDTA].[F4075_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4075_del]
	(
	  [VBVBT] NCHAR(10)
	, [VBEFTJ] NUMERIC(18,0)
	, [VBUPMJ] NUMERIC(18,0)
	, [VBTDAY] NUMERIC(6,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([VBVBT] ASC, [VBEFTJ] ASC, [VBUPMJ] ASC, [VBTDAY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4075] - End */
/* TRIGGERS FOR F4075 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4075_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4075_ins
	ON BUSDTA.F4075 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4075_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4075_del.VBEFTJ= inserted.VBEFTJ AND BUSDTA.F4075_del.VBTDAY= inserted.VBTDAY AND BUSDTA.F4075_del.VBUPMJ= inserted.VBUPMJ AND BUSDTA.F4075_del.VBVBT= inserted.VBVBT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4075_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4075_upd
	ON BUSDTA.F4075 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4075
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4075.VBEFTJ= inserted.VBEFTJ AND BUSDTA.F4075.VBTDAY= inserted.VBTDAY AND BUSDTA.F4075.VBUPMJ= inserted.VBUPMJ AND BUSDTA.F4075.VBVBT= inserted.VBVBT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4075_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4075_dlt
	ON BUSDTA.F4075 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4075_del (VBEFTJ, VBTDAY, VBUPMJ, VBVBT, last_modified )
	SELECT deleted.VBEFTJ, deleted.VBTDAY, deleted.VBUPMJ, deleted.VBVBT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4075 - END */

