
/* [BUSDTA].[M56M0001] - begins */

/* TableDDL - [BUSDTA].[M56M0001] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0001]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0001]
	(
	  [DMDDC] VARCHAR(3) NOT NULL
	, [DMDDCD] NCHAR(50) NOT NULL
	, [DMSTAT] BIT NOT NULL
	, [DMWPC] INT NOT NULL
	, [DMCRBY] NCHAR(50) NULL
	, [DMCRDT] DATETIME NULL
	, [DMUPBY] NCHAR(50) NULL
	, [DMUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M56M0001_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M56M0001] PRIMARY KEY ([DMDDC] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M56M0001] - End */

/* SHADOW TABLE FOR [BUSDTA].[M56M0001] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0001_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0001_del]
	(
	  [DMDDC] VARCHAR(3)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DMDDC] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M56M0001] - End */
/* TRIGGERS FOR M56M0001 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M56M0001_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M56M0001_ins
	ON BUSDTA.M56M0001 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M56M0001_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M56M0001_del.DMDDC= inserted.DMDDC
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M56M0001_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0001_upd
	ON BUSDTA.M56M0001 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M56M0001
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M56M0001.DMDDC= inserted.DMDDC');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M56M0001_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0001_dlt
	ON BUSDTA.M56M0001 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M56M0001_del (DMDDC, last_modified )
	SELECT deleted.DMDDC, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M56M0001 - END */

