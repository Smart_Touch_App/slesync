SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[SetupNumberPoolForEntity]
@EntityBucketMasterId numeric(2,0)
as
Begin
	SET NOCOUNT ON;
	Declare @Entity as varchar(500)
	Declare @Name as varchar(30)
	Declare @RouteID as varchar(2)
	Declare @BucketSize as numeric(4,0)
	Declare @NoOfbucket as numeric(4,0)
	Declare @BucketExists as numeric(4,0)
	Declare @BucketName as nvarchar(25)
	Declare @LastAlloted as numeric(8,0)
	Declare RouteCursor CURSOR FOR
	Select RouteMasterID from  BUSDTA.Route_Master (Nolock)
	OPEN RouteCursor
		FETCH NEXT FROM RouteCursor INTO @RouteID
			WHILE @@FETCH_STATUS = 0
				BEGIN
					select @Entity=Entity,@BucketSize=BucketSize,@NoOfbucket=NoOfbuckets from BUSDTA.Entity_Bucket_Master
					where EntityBucketMasterId=@EntityBucketMasterId;
					select @LastAlloted= case  when isnull(LastAlloted,0)=0 then RangeStart else LastAlloted +1 End from BUSDTA.Entity_Range_Master
					where EntityBucketMasterId=@EntityBucketMasterId and Active=1;
					declare @counter numeric(2,0)=1;
					while(@counter<=@NoOfbucket)
						begin
						    set @BucketName = 'Bucket' +convert(varchar,@counter);
						    select @BucketExists=COUNT(*) from BUSDTA.Entity_Number_Status where
						    RouteId = @RouteID 
						    and Bucket = @BucketName
						    and EntityBucketMasterId = @EntityBucketMasterId
						    and IsConsumed=0;
							if(@BucketExists=0)
								Begin
									insert into BUSDTA.Entity_Number_Status 
									(RouteId,Bucket,EntityBucketMasterId,RangeStart,RangeEnd,CurrentAllocated,IsConsumed,CreatedDatetime)
									values
									(@RouteID,'Bucket' +convert(varchar,@counter),@EntityBucketMasterId,@LastAlloted,@LastAlloted + @BucketSize,null,0,GETDATE())
									set @LastAlloted=@LastAlloted + @BucketSize;
									update BUSDTA.Entity_Range_Master set LastAlloted=@LastAlloted
									where EntityBucketMasterId=@EntityBucketMasterId and Active=1
									set @LastAlloted=@LastAlloted + 1;
								End
							set @counter = @counter+1;	
						end
					FETCH NEXT FROM RouteCursor INTO @RouteID
				END
		CLOSE RouteCursor
	DEALLOCATE RouteCursor
	/*
	--truncate table  BUSDTA.Entity_Number_Status
	--update BUSDTA.Entity_Range_Master set LastAlloted=null
	select * from BUSDTA.Entity_Number_Status
	select * from BUSDTA.Entity_Bucket_Master
	select * from BUSDTA.Entity_Range_Master
	select * from BUSDTA.Route_Master
	*/
End
GO


