CREATE OR REPLACE FUNCTION "BUSDTA"."DateG2J" (@julian varchar(6))
RETURNS datetime AS BEGIN
	DECLARE @datetime datetime

	SET @datetime = CAST(19+CAST(SUBSTRING(@julian, 1, 1) as int) as varchar(4))+SUBSTRING(@julian, 2,2)+'-01-01'
	SET @datetime = DATEADD(day, CAST(SUBSTRING(@julian, 4,3) as int)-1, @datetime)

	RETURN @datetime
END
GO