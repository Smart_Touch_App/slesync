
     SELECT "BUSDTA"."Inventory"."ItemId"  ,"BUSDTA"."Inventory"."ItemNumber"  ,"BUSDTA"."Inventory"."RouteId"  ,
     "BUSDTA"."Inventory"."OnHandQuantity"  ,"BUSDTA"."Inventory"."CommittedQuantity"  ,"BUSDTA"."Inventory"."HeldQuantity"  ,
     "BUSDTA"."Inventory"."ParLevel"  ,"BUSDTA"."Inventory"."LastReceiptDate"  ,"BUSDTA"."Inventory"."LastConsumeDate"  ,
     "BUSDTA"."Inventory"."CreatedBy"  ,"BUSDTA"."Inventory"."CreatedDatetime"  ,"BUSDTA"."Inventory"."UpdatedBy"  ,
     "BUSDTA"."Inventory"."UpdatedDatetime"    
     FROM "BUSDTA"."Inventory"  
     WHERE "BUSDTA"."Inventory"."last_modified" >= {ml s.last_table_download}  
     AND "BUSDTA"."Inventory"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     