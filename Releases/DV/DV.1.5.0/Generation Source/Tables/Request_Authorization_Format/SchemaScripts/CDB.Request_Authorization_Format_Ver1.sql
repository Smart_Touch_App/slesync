/* [BUSDTA].[Request_Authorization_Format] - begins */

/* TableDDL - [BUSDTA].[Request_Authorization_Format] - Start */
IF OBJECT_ID('[BUSDTA].[Request_Authorization_Format]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Request_Authorization_Format]
	(
	  [RequestFormatCode] NCHAR(1) NOT NULL
	, [AuthorizationFormatCode] NCHAR(1) NOT NULL
	, [Key1] NCHAR(1) NOT NULL
	, [Key2] NCHAR(1) NOT NULL
	, [Key3] NCHAR(1) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_REQUEST_AUTHORIZATION_FORMAT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Request_Authorization_Format] PRIMARY KEY ([RequestFormatCode] ASC, [AuthorizationFormatCode] ASC, [Key1] ASC, [Key2] ASC, [Key3] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Request_Authorization_Format] - End */

/* SHADOW TABLE FOR [BUSDTA].[Request_Authorization_Format] - Start */
IF OBJECT_ID('[BUSDTA].[Request_Authorization_Format_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Request_Authorization_Format_del]
	(
	  [RequestFormatCode] NCHAR(1)
	, [AuthorizationFormatCode] NCHAR(1)
	, [Key1] NCHAR(1)
	, [Key2] NCHAR(1)
	, [Key3] NCHAR(1)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RequestFormatCode] ASC, [AuthorizationFormatCode] ASC, [Key1] ASC, [Key2] ASC, [Key3] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Request_Authorization_Format] - End */
/* TRIGGERS FOR Request_Authorization_Format - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Request_Authorization_Format_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Request_Authorization_Format_ins
	ON BUSDTA.Request_Authorization_Format AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Request_Authorization_Format_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Request_Authorization_Format_del.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Request_Authorization_Format_del.Key1= inserted.Key1 AND BUSDTA.Request_Authorization_Format_del.Key2= inserted.Key2 AND BUSDTA.Request_Authorization_Format_del.Key3= inserted.Key3 AND BUSDTA.Request_Authorization_Format_del.RequestFormatCode= inserted.RequestFormatCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Request_Authorization_Format_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Request_Authorization_Format_upd
	ON BUSDTA.Request_Authorization_Format AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Request_Authorization_Format
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Request_Authorization_Format.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Request_Authorization_Format.Key1= inserted.Key1 AND BUSDTA.Request_Authorization_Format.Key2= inserted.Key2 AND BUSDTA.Request_Authorization_Format.Key3= inserted.Key3 AND BUSDTA.Request_Authorization_Format.RequestFormatCode= inserted.RequestFormatCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Request_Authorization_Format_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Request_Authorization_Format_dlt
	ON BUSDTA.Request_Authorization_Format AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Request_Authorization_Format_del (AuthorizationFormatCode, Key1, Key2, Key3, RequestFormatCode, last_modified )
	SELECT deleted.AuthorizationFormatCode, deleted.Key1, deleted.Key2, deleted.Key3, deleted.RequestFormatCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Request_Authorization_Format - END */
