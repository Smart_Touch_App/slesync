/* [BUSDTA].[MoneyOrderDetails] - begins */

/* TableDDL - [BUSDTA].[MoneyOrderDetails] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrderDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrderDetails]
	(
	  [MoneyOrderId] DECIMAL(15,0) NOT NULL
	, [MoneyOrderNumber] NVARCHAR (10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [MoneyOrderAmount] DECIMAL(15,4) NULL
	, [MoneyOrderFeeAmount] DECIMAL(15,4) NULL
	, [StatusId] NUMERIC(8,0) NULL
	, [MoneyOrderDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC(8,0) NULL
	, [RouteSettlementId] DECIMAL(15,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([MoneyOrderId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[MoneyOrderDetails] - End */
