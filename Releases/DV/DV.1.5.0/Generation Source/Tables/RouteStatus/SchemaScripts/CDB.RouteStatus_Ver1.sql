/* [BUSDTA].[RouteStatus] - begins */

/* TableDDL - [BUSDTA].[RouteStatus] - Start */
IF OBJECT_ID('[BUSDTA].[RouteStatus]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RouteStatus]
	(
	  [RouteStatusID] INT NOT NULL
	, [RouteStatus] VARCHAR(100) NULL
	, [StatusDescription] VARCHAR(500) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_ROUTESTATUS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_RouteStatus] PRIMARY KEY ([RouteStatusID] ASC)
	)

END
/* TableDDL - [BUSDTA].[RouteStatus] - End */
