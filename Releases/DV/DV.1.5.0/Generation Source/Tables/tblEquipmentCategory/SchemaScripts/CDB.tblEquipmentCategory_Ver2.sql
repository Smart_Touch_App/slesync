/* [BUSDTA].[tblEquipmentCategory] - begins */

/* TableDDL - [BUSDTA].[tblEquipmentCategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentCategory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentCategory]
	(
	  [EquipmentCategoryID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentCategory] VARCHAR(500) NULL
	, [EquipmentTypeID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentCategory] PRIMARY KEY ([EquipmentCategoryID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblEquipmentCategory] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentCategory] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentCategory_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentCategory_del]
	(
	  [EquipmentCategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentCategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblEquipmentCategory] - End */
/* TRIGGERS FOR tblEquipmentCategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentCategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblEquipmentCategory_ins
	ON BUSDTA.tblEquipmentCategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblEquipmentCategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblEquipmentCategory_del.EquipmentCategoryID= inserted.EquipmentCategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblEquipmentCategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentCategory_upd
	ON BUSDTA.tblEquipmentCategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblEquipmentCategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblEquipmentCategory.EquipmentCategoryID= inserted.EquipmentCategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblEquipmentCategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblEquipmentCategory_dlt
	ON BUSDTA.tblEquipmentCategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblEquipmentCategory_del (EquipmentCategoryID, last_modified )
	SELECT deleted.EquipmentCategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentCategory - END */
