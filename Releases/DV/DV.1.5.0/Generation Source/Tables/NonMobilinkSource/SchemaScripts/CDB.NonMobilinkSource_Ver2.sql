/* [Busdta].[NonMobilinkSource] - begins */

/* TableDDL - [Busdta].[NonMobilinkSource] - Start */
IF OBJECT_ID('[Busdta].[NonMobilinkSource]') IS NULL
BEGIN

	CREATE TABLE [Busdta].[NonMobilinkSource]
	(
	  [ServerName] VARCHAR(128) NOT NULL
	, [DatabaseName] VARCHAR(128) NOT NULL
	, [LoginID] VARCHAR(128) NULL
	, [Password] VARCHAR(128) NULL
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_NONMOBILINKSOURCE_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_NONMOBILINKSOURCE_EditedOn DEFAULT(getdate())
	, CONSTRAINT [PK_NonMobilinkSource] PRIMARY KEY ([ServerName] ASC, [DatabaseName] ASC)
	)

END
/* TableDDL - [Busdta].[NonMobilinkSource] - End */
GO