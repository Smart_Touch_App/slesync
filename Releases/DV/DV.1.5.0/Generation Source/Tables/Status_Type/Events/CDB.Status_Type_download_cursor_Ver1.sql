 
SELECT "BUSDTA"."Status_Type"."StatusTypeID"
,"BUSDTA"."Status_Type"."StatusTypeCD"
,"BUSDTA"."Status_Type"."StatusTypeDESC"
,"BUSDTA"."Status_Type"."CreatedBy"
,"BUSDTA"."Status_Type"."CreatedDatetime"
,"BUSDTA"."Status_Type"."UpdatedBy"
,"BUSDTA"."Status_Type"."UpdatedDatetime"

FROM "BUSDTA"."Status_Type"
WHERE "BUSDTA"."Status_Type"."last_modified">= {ml s.last_table_download}
