
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Cycle_Count_Detail"
SET "CountedQty" = {ml r."CountedQty"}, "ItemStatus" = {ml r."ItemStatus"}, "OnHandQty" = {ml r."OnHandQty"}, "HeldQty" = {ml r."HeldQty"}, "CountAccepted" = {ml r."CountAccepted"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CycleCountID" = {ml r."CycleCountID"} AND "CycleCountDetailID" = {ml r."CycleCountDetailID"} AND "ReCountNumber" = {ml r."ReCountNumber"} AND "ItemId" = {ml r."ItemId"}
 

