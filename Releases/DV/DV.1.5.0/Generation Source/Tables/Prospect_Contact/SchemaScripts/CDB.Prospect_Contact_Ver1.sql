/* [BUSDTA].[Prospect_Contact] - begins */

/* TableDDL - [BUSDTA].[Prospect_Contact] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Contact]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Contact]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [ProspectContactId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ShowOnDashboard] NCHAR(1) NULL
	, [FirstName] NCHAR(75) NULL
	, [MiddleName] NCHAR(75) NULL
	, [LastName] NCHAR(75) NULL
	, [Title] NVARCHAR(50) NULL
	, [IsActive] NCHAR(1) NOT NULL
	, [AreaCode1] NCHAR(6) NULL
	, [Phone1] NCHAR(20) NULL
	, [Extn1] NCHAR(8) NULL
	, [PhoneType1] NUMERIC(3,0) NULL
	, [IsDefaultPhone1] NCHAR(1) NULL
	, [AreaCode2] NCHAR(6) NULL
	, [Phone2] NCHAR(20) NULL
	, [Extn2] NCHAR(8) NULL
	, [PhoneType2] NUMERIC(3,0) NULL
	, [IsDefaultPhone2] NCHAR(1) NULL
	, [AreaCode3] NCHAR(6) NULL
	, [Phone3] NCHAR(20) NULL
	, [Extn3] NCHAR(8) NULL
	, [PhoneType3] NUMERIC(3,0) NULL
	, [IsDefaultPhone3] NCHAR(1) NULL
	, [AreaCode4] NCHAR(6) NULL
	, [Phone4] NCHAR(20) NULL
	, [Extn4] NCHAR(8) NULL
	, [PhoneType4] NUMERIC(3,0) NULL
	, [IsDefaultPhone4] NCHAR(1) NULL
	, [EmailID1] NCHAR(256) NULL
	, [EmailType1] NUMERIC(3,0) NULL
	, [IsDefaultEmail1] NCHAR(1) NULL
	, [EmailID2] NCHAR(256) NULL
	, [EmailType2] NUMERIC(3,0) NULL
	, [IsDefaultEmail2] NCHAR(1) NULL
	, [EmailID3] NCHAR(256) NULL
	, [EmailType3] NUMERIC(3,0) NULL
	, [IsDefaultEmail3] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_CONTACT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Contact] PRIMARY KEY ([ProspectId] ASC, [ProspectContactId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Contact] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Contact] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Contact_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Contact_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, [ProspectContactId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC, [ProspectContactId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Contact] - End */
/* TRIGGERS FOR Prospect_Contact - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Contact_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Contact_ins
	ON BUSDTA.Prospect_Contact AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Contact_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Contact_del.ProspectContactId= inserted.ProspectContactId AND BUSDTA.Prospect_Contact_del.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Contact_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Contact_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Contact_upd
	ON BUSDTA.Prospect_Contact AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Contact
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Contact.ProspectContactId= inserted.ProspectContactId AND BUSDTA.Prospect_Contact.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Contact.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Contact_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Contact_dlt
	ON BUSDTA.Prospect_Contact AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Contact_del (ProspectContactId, ProspectId, RouteId, last_modified )
	SELECT deleted.ProspectContactId, deleted.ProspectId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Contact - END */
