
/* [BUSDTA].[Invoice_Header] - begins */

/* TableDDL - [BUSDTA].[Invoice_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Invoice_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Invoice_Header]
	(
	  [InvoiceID] DECIMAL(15,0) NOT NULL DEFAULT autoincrement
	, [CustShipToId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OrderId] DECIMAL(15,0) NOT NULL
	, [ARInvoiceNumber] NVARCHAR (20) NULL
	, [DeviceInvoiceNumber] NVARCHAR (20) NULL
	, [ARInvoiceDate] DATE NULL
	, [DeviceInvoiceDate] DATE NULL
	, [DeviceStatusID] NUMERIC(3,0) NOT NULL
	, [ARStatusID] NUMERIC(3,0) NOT NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [DueDate] DATETIME NULL
	, [InvoicePaymentType] NVARCHAR (8) NULL
	, [GrossAmt] DECIMAL(15,4) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, PRIMARY KEY ([InvoiceID] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Invoice_Header] - End */

