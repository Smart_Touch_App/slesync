/* [BUSDTA].[Route_Settlement_Detail] - begins */

/* TableDDL - [BUSDTA].[Route_Settlement_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement_Detail]
	(
	  [SettlementId] NVARCHAR(8) NOT NULL
	, [SettlementDetailId] NUMERIC(8,0) NOT NULL
	, [UserId] NUMERIC(8,0) NULL
	, [VerificationNum] NUMERIC(2,0) NOT NULL
	, [CashAmount] decimal (15,4) NOT NULL
	, [CheckAmount] decimal (15,4) NOT NULL
	, [MoneyOrderAmount] decimal (15,4) NOT NULL
	, [TotalVerified] decimal (15,4) NOT NULL
	, [Expenses] decimal (15,4) NOT NULL
	, [Payments] decimal (15,4) NOT NULL
	, [OverShortAmount] decimal (15,4) NOT NULL
	, [VerSignature] VARBINARY(MAX) NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OriginatingRoute] NUMERIC(8,0) NOT NULL
	, [partitioningRoute] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_SETTLEMENT_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Settlement_Detail] PRIMARY KEY ([SettlementId] ASC, [SettlementDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Settlement_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement_Detail_del]
	(
	  [SettlementId] NVARCHAR(8)
	, [SettlementDetailId] decimal (15,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SettlementId] ASC, [SettlementDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Settlement_Detail] - End */
/* TRIGGERS FOR Route_Settlement_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Settlement_Detail_ins
	ON BUSDTA.Route_Settlement_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Settlement_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Settlement_Detail_del.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement_Detail_del.SettlementDetailId= inserted.SettlementDetailId AND BUSDTA.Route_Settlement_Detail_del.SettlementId= inserted.SettlementId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Settlement_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_Detail_upd
	ON BUSDTA.Route_Settlement_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Settlement_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Settlement_Detail.RouteId= inserted.RouteId AND BUSDTA.Route_Settlement_Detail.SettlementDetailId= inserted.SettlementDetailId AND BUSDTA.Route_Settlement_Detail.SettlementId= inserted.SettlementId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Settlement_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Settlement_Detail_dlt
	ON BUSDTA.Route_Settlement_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Settlement_Detail_del (RouteId, SettlementDetailId, SettlementId, last_modified )
	SELECT deleted.RouteId, deleted.SettlementDetailId, deleted.SettlementId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Settlement_Detail - END */
