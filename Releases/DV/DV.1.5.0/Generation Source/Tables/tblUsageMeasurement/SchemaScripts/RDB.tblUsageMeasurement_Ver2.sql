/* [BUSDTA].[tblUsageMeasurement] - begins */

/* TableDDL - [BUSDTA].[tblUsageMeasurement] - Start */
IF OBJECT_ID('[BUSDTA].[tblUsageMeasurement]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblUsageMeasurement]
	(
	  [UsageMeasurementID] INTEGER NOT NULL 
	, [UsageMeasurement] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([UsageMeasurementID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblUsageMeasurement] - End */
