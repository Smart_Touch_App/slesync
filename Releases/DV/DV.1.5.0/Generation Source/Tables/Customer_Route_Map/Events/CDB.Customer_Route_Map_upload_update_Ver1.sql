
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Customer_Route_Map"
SET "RelatedAddressBookNumber" = {ml r."RelatedAddressBookNumber"}, "IsActive" = {ml r."IsActive"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "BranchNumber" = {ml r."BranchNumber"} AND "CustomerShipToNumber" = {ml r."CustomerShipToNumber"} AND "RelationshipType" = {ml r."RelationshipType"}
 

