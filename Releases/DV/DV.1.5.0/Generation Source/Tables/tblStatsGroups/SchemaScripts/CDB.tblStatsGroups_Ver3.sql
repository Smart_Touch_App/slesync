
/* TableDDL - [BUSDTA].[tblStatsGroups] - Start */
IF OBJECT_ID('[BUSDTA].[tblStatsGroups]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblStatsGroups]
	(
	  [StatsGroupID] INT NOT NULL IDENTITY(1,1)
	, [GroupName] VARCHAR(128) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLSTATSGROUPS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblStatsGroups] PRIMARY KEY ([StatsGroupID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblStatsGroups] - End */
