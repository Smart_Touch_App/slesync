/* [BUSDTA].[Prospect_Master] - begins */

/* TableDDL - [BUSDTA].[Prospect_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Master]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ProspectName] NVARCHAR (100) NULL
	, [AcctSegmentType] NUMERIC(3,0) NULL
	, [BuyingGroup] NVARCHAR (50) NULL
	, [LocationCount] NVARCHAR (10) NULL
	, [CategoryCode01] NVARCHAR (3) NULL
	, [OperatingUnit] NVARCHAR (3) NULL
	, [Branch] NVARCHAR (3) NULL
	, [District] NVARCHAR (3) NULL
	, [Region] NVARCHAR (3) NULL
	, [Reporting] NVARCHAR (3) NULL
	, [Chain] NVARCHAR (3) NULL
	, [BrewmaticAgentCode] NVARCHAR (3) NULL
	, [NTR] NVARCHAR (3) NULL
	, [ProspectTaxGrp] NVARCHAR (3) NULL
	, [CategoryCode12] NVARCHAR (3) NULL
	, [APCheckCode] NVARCHAR (3) NULL
	, [CategoryCode14] NVARCHAR (3) NULL
	, [CategoryCode15] NVARCHAR (3) NULL
	, [CategoryCode16] NVARCHAR (3) NULL
	, [CategoryCode17] NVARCHAR (3) NULL
	, [CategoryCode18] NVARCHAR (3) NULL
	, [CategoryCode19] NVARCHAR (3) NULL
	, [CategoryCode20] NVARCHAR (3) NULL
	, [CategoryCode21] NVARCHAR (3) NULL
	, [CategoryCode22] NVARCHAR (3) NULL
	, [SpecialEquipment] NVARCHAR (3) NULL
	, [CAProtection] NVARCHAR (3) NULL
	, [ProspectGroup] NVARCHAR (3) NULL
	, [SPCommisFBType] NVARCHAR (3) NULL
	, [AlliedDiscount] NVARCHAR (3) NULL
	, [CoffeeVolume] NVARCHAR (3) NULL
	, [EquipmentProgPts] NVARCHAR (3) NULL
	, [LiquidCoffee] NVARCHAR (3) NULL
	, [PriceProtection] NVARCHAR (3) NULL
	, [POSUpCharge] NVARCHAR (3) NULL
	, [SpecialCCP] NVARCHAR (3) NULL
	, [TaxGroup] NVARCHAR (3) NULL
	, [IsPricesetup] NVARCHAR (1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ProspectId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Prospect_Master] - End */
