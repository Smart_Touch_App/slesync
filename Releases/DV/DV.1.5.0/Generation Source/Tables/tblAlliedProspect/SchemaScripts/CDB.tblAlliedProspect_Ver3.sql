/* [BUSDTA].[tblAlliedProspect] - begins */

/* TableDDL - [BUSDTA].[tblAlliedProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspect]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspect]
	(
	  [AlliedProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, [CompetitorID] INT NULL
	, [CategoryID] INT NULL
	, [SubCategoryID] INT NULL
	, [BrandID] INT NULL
	, [UOMID] INT NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] decimal (15,4) NULL
	, [UsageMeasurementID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblAlliedProspect] PRIMARY KEY ([AlliedProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblAlliedProspect] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspect_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspect_del]
	(
	  [AlliedProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([AlliedProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblAlliedProspect] - End */
/* TRIGGERS FOR tblAlliedProspect - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspect_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblAlliedProspect_ins
	ON BUSDTA.tblAlliedProspect AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblAlliedProspect_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblAlliedProspect_del.AlliedProspectID= inserted.AlliedProspectID AND BUSDTA.tblAlliedProspect_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblAlliedProspect_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspect_upd
	ON BUSDTA.tblAlliedProspect AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblAlliedProspect
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblAlliedProspect.AlliedProspectID= inserted.AlliedProspectID AND BUSDTA.tblAlliedProspect.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblAlliedProspect_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblAlliedProspect_dlt
	ON BUSDTA.tblAlliedProspect AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblAlliedProspect_del (AlliedProspectID,ProspectID, last_modified )
	SELECT deleted.AlliedProspectID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblAlliedProspect - END */
