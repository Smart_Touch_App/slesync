
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_User_Map"
SET "Active" = {ml r."Active"}, "Default_Route" = {ml r."Default_Route"}
WHERE "App_user_id" = {ml r."App_user_id"} AND "Route_Id" = {ml r."Route_Id"}
