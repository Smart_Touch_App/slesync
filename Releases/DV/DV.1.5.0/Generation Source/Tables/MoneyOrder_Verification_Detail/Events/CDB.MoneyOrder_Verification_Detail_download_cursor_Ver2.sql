
 
SELECT "BUSDTA"."MoneyOrder_Verification_Detail"."MoneyOrderVerificationDetailId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."SettlementDetailId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."MoneyOrderId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."RouteId"
,"BUSDTA"."MoneyOrder_Verification_Detail"."CreatedBy"
,"BUSDTA"."MoneyOrder_Verification_Detail"."CreatedDatetime"
,"BUSDTA"."MoneyOrder_Verification_Detail"."UpdatedBy"
,"BUSDTA"."MoneyOrder_Verification_Detail"."UpdatedDatetime"

FROM "BUSDTA"."MoneyOrder_Verification_Detail", "BUSDTA"."Route_Settlement_Detail",
    "BUSDTA"."Route_Settlement"
WHERE ("BUSDTA"."MoneyOrder_Verification_Detail"."last_modified" >= {ml s.last_table_download} or "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download} or "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download})
AND "BUSDTA"."MoneyOrder_Verification_Detail"."SettlementDetailId" ="BUSDTA"."Route_Settlement_Detail"."SettlementDetailId"
AND "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId"
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
