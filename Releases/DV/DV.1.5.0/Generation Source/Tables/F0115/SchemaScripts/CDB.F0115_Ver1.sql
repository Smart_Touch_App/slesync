
IF OBJECT_ID('[BUSDTA].[F0115]') IS NULL
BEGIN

CREATE TABLE [BUSDTA].[F0115](
	[WPAN8] [numeric](8, 0) NOT NULL,
	[WPIDLN] [numeric](5, 0) NOT NULL,
	[WPRCK7] [numeric](5, 0) NOT NULL,
	[WPCNLN] [numeric](5, 0) NOT NULL,
	[WPPHTP] [nchar](4) NULL,
	[WPAR1] [nchar](6) NULL,
	[WPPH1] [nchar](20) NULL,
	[last_modified] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_F0115] PRIMARY KEY CLUSTERED 
(
	[WPAN8] ASC,
	[WPIDLN] ASC,
	[WPRCK7] ASC,
	[WPCNLN] ASC
)) ON [PRIMARY]

END
