
/* [BUSDTA].[PreTrip_Inspection_Detail] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Detail]
	(
	  [PreTripInspectionHeaderId] decimal (15,0) NOT NULL
	, [PreTripInspectionDetailId] decimal (15,0) NOT NULL DEFAULT autoincrement
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [QuestionId] NUMERIC(8,0) NULL
	, [ResponseID] NUMERIC(8,0) NULL
	, [ResponseReason] NVARCHAR (50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [PreTripInspectionDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - End */
