/* [BUSDTA].[Route_Replenishment_Detail] - begins */

/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Detail]
	(
	  [ReplenishmentID] decimal (15,0) NOT NULL
	, [ReplenishmentDetailID] decimal (15,0) NOT NULL DEFAULT autoincrement
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ReplenishmentQtyUM] NVARCHAR (2) NOT NULL
	, [ReplenishmentQty] decimal (15,0) NOT NULL
	, [AdjustmentQty] decimal (15,0) NOT NULL
	, [PickedQty] decimal (15,0) NULL
	, [PickedStatusTypeID] NUMERIC(3,0) NULL
	, [AvailaibilityAtTime] decimal (15,0) NULL
	, [CurrentAvailability] decimal (15,0) NULL
	, [DemandQty] decimal (15,0) NULL DEFAULT 0
	, [ParLevelQty] decimal (15,0) NULL
	, [OpenReplnQty] decimal (15,0) NULL
	, [ShippedQty] decimal (15,0) NULL
	, [OrderID] decimal (15,0) NULL
	, [LastSaved] DATETIME NULL
	, [SequenceNumber] NUMERIC(3,0) NULL DEFAULT 1
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ReplenishmentID] ASC, [ReplenishmentDetailID] ASC, [ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - End */
