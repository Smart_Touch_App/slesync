/* [BUSDTA].[Route_Replenishment_Detail] - begins */

/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Detail]
	(
	  [ReplenishmentID] decimal (15,0) NOT NULL
	, [ReplenishmentDetailID] decimal (15,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ReplenishmentQtyUM] NCHAR(2) NOT NULL
	, [ReplenishmentQty] decimal (15,0) NOT NULL
	, [AdjustmentQty] decimal (15,0) NOT NULL
	, [PickedQty] decimal (15,0) NULL
	, [PickedStatusTypeID] NUMERIC(3,0) NULL
	, [AvailaibilityAtTime] decimal (15,0) NULL
	, [CurrentAvailability] decimal (15,0) NULL
	, [DemandQty] decimal (15,0) NULL
	, [ParLevelQty] decimal (15,0) NULL
	, [OpenReplnQty] decimal (15,0) NULL
	, [ShippedQty] decimal (15,0) NULL
	, [OrderID] decimal (15,0) NULL
	, [LastSaved] DATETIME NULL
	, [SequenceNumber] NUMERIC(3,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_REPLENISHMENT_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Replenishment_Detail] PRIMARY KEY ([ReplenishmentID] ASC, [ReplenishmentDetailID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Detail_del]
	(
	  [ReplenishmentID] decimal (15,0)
	, [ReplenishmentDetailID] NUMERIC(8,0)
	, [ItemId] decimal (15,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ReplenishmentID] ASC, [ReplenishmentDetailID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Route_Replenishment_Detail] - End */
/* TRIGGERS FOR Route_Replenishment_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Replenishment_Detail_ins
	ON BUSDTA.Route_Replenishment_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Replenishment_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Replenishment_Detail_del.ItemId= inserted.ItemId AND BUSDTA.Route_Replenishment_Detail_del.ReplenishmentDetailID= inserted.ReplenishmentDetailID AND BUSDTA.Route_Replenishment_Detail_del.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Detail_upd
	ON BUSDTA.Route_Replenishment_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Replenishment_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Replenishment_Detail.ItemId= inserted.ItemId AND BUSDTA.Route_Replenishment_Detail.ReplenishmentDetailID= inserted.ReplenishmentDetailID AND BUSDTA.Route_Replenishment_Detail.ReplenishmentID= inserted.ReplenishmentID AND BUSDTA.Route_Replenishment_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Replenishment_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Replenishment_Detail_dlt
	ON BUSDTA.Route_Replenishment_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Replenishment_Detail_del (ItemId, ReplenishmentDetailID, ReplenishmentID, RouteId, last_modified )
	SELECT deleted.ItemId, deleted.ReplenishmentDetailID, deleted.ReplenishmentID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Replenishment_Detail - END */
