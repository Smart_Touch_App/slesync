 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Address"
SET "AddressLine1" = {ml r."AddressLine1"}, "AddressLine2" = {ml r."AddressLine2"}, "AddressLine3" = {ml r."AddressLine3"}, "AddressLine4" = {ml r."AddressLine4"}, "CityName" = {ml r."CityName"}, "StateName" = {ml r."StateName"}, "ZipCode" = {ml r."ZipCode"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectId" = {ml r."ProspectId"} AND "RouteId" = {ml r."RouteId"}
 
