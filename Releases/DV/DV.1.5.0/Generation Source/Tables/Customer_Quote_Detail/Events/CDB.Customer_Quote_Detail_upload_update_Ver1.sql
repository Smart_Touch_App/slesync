
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Customer_Quote_Detail"
SET "ItemId" = {ml r."ItemId"}, "QuoteQty" = {ml r."QuoteQty"}, "TransactionUM" = {ml r."TransactionUM"}, "PricingAmt" = {ml r."PricingAmt"}, "PricingUM" = {ml r."PricingUM"}, "TransactionAmt" = {ml r."TransactionAmt"}, "OtherAmt" = {ml r."OtherAmt"}, "OtherUM" = {ml r."OtherUM"}, "CompetitorName" = {ml r."CompetitorName"}, "UnitPriceAmt" = {ml r."UnitPriceAmt"}, "UnitPriceUM" = {ml r."UnitPriceUM"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CustomerQuoteId" = {ml r."CustomerQuoteId"} AND "CustomerQuoteDetailId" = {ml r."CustomerQuoteDetailId"} AND "RouteId" = {ml r."RouteId"}
 

