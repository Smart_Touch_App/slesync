
SELECT "EquipmentTypeID"
,"EquipmentType"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."tblEquipmentType"
WHERE "BUSDTA"."tblEquipmentType"."last_modified">= {ml s.last_table_download}
