/* [BUSDTA].[tblEquipmentType] - begins */

/* TableDDL - [BUSDTA].[tblEquipmentType] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipmentType]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipmentType]
	(
	  [EquipmentTypeID] INTEGER NOT NULL 
	, [EquipmentType] VARCHAR(200) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([EquipmentTypeID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblEquipmentType] - End */
