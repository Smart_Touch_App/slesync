/* [BUSDTA].[ReasonCodeMaster] - begins */

/* TableDDL - [BUSDTA].[ReasonCodeMaster] - Start */
IF OBJECT_ID('[BUSDTA].[ReasonCodeMaster]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ReasonCodeMaster]
	(
	  [ReasonCodeId] INTEGER NOT NULL
	, [ReasonCode] VARCHAR(5) NOT NULL
	, [ReasonCodeDescription] NVARCHAR (100) NULL
	, [ReasonCodeType] NVARCHAR (50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([ReasonCode] ASC)
	)
END
/* TableDDL - [BUSDTA].[ReasonCodeMaster] - End */
