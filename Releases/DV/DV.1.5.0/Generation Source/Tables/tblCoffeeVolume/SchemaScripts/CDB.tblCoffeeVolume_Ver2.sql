/* [BUSDTA].[tblCoffeeVolume] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeVolume] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeVolume]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeVolume]
	(
	  [CoffeeVolumeID] INT NOT NULL IDENTITY(1,1)
	, [Volume] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEVOLUME_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEVOLUME_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEVOLUME_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeVolume] PRIMARY KEY ([CoffeeVolumeID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblCoffeeVolume] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeVolume] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeVolume_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeVolume_del]
	(
	  [CoffeeVolumeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeVolumeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeVolume] - End */
/* TRIGGERS FOR tblCoffeeVolume - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeVolume_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCoffeeVolume_ins
	ON BUSDTA.tblCoffeeVolume AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCoffeeVolume_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCoffeeVolume_del.CoffeeVolumeID= inserted.CoffeeVolumeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCoffeeVolume_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeVolume_upd
	ON BUSDTA.tblCoffeeVolume AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCoffeeVolume
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCoffeeVolume.CoffeeVolumeID= inserted.CoffeeVolumeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeVolume_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeVolume_dlt
	ON BUSDTA.tblCoffeeVolume AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCoffeeVolume_del (CoffeeVolumeID, last_modified )
	SELECT deleted.CoffeeVolumeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeVolume - END */
