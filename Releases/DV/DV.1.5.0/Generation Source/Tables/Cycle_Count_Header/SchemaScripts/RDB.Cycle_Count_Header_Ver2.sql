
/* [BUSDTA].[Cycle_Count_Header] - begins */

/* TableDDL - [BUSDTA].[Cycle_Count_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Header]
	(
	  [CycleCountID] DECIMAL(15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(3,0) NOT NULL
	, [IsCountInitiated] NVARCHAR (1) NULL DEFAULT (0)
	, [CycleCountTypeId] NUMERIC(3,0) NOT NULL
	, [InitiatorId] NUMERIC(8,0) NULL
	, [CycleCountDatetime] DATETIME NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [JDEQty] DECIMAL(15,0) NULL
	, [JDEUM] NVARCHAR (2) NULL
	, [JDECycleCountNum] DECIMAL(15,0) NULL
	, [JDECycleCountDocType] NCHAR(2) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([CycleCountID] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Cycle_Count_Header] - End */

