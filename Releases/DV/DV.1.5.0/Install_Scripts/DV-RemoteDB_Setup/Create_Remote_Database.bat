@ECHO OFF
setlocal

CALL Remote_Connection_Vars.bat

echo Creating remote database....

dbinit -t "%TxLogFullPath%" "%DBFileFullPath%"

echo Remote database created.

GOTO End

:End
EXIT
