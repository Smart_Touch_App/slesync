@ECHO OFF
setlocal

CALL Remote_Connection_Vars.bat

echo Executing Remote Setup...

@echo off
 for %%f in (*.sql) do (
	IF EXIST "%cd%\%%~nf.sql" (
		echo Executing %%~nf.sql....
		dbisql -c "Server=%server%;DBN=%RemoteDB%;UID=%rdbUname%;PWD=%rdbPass%" "%cd%\%%~nf.sql"
		echo                - Executed Successfully.
	) ELSE (
        	echo WARNING: script " %%~nf.sql " file does NOT exist.
	)
    )

echo All Remote scripts are executed successfully.
pause
