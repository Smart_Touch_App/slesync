DROP PUBLICATION IF EXISTS pub_immediate;
GO
/** Create publication 'pub_immediate'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_immediate" (
    TABLE "MobileDataModel"."BUSDTA"."F0116" ,
    TABLE "MobileDataModel"."BUSDTA"."F03012" ,
    TABLE "MobileDataModel"."BUSDTA"."F4015" ,
    TABLE "MobileDataModel"."BUSDTA"."M0111" ,
    TABLE "MobileDataModel"."BUSDTA"."M0112" ,
    TABLE "MobileDataModel"."BUSDTA"."M03042" ,
    TABLE "MobileDataModel"."BUSDTA"."M4016" ,
    TABLE "MobileDataModel"."BUSDTA"."Order_Detail" ,
    TABLE "MobileDataModel"."BUSDTA"."Order_Header" ,
    TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception" ,
    TABLE "MobileDataModel"."BUSDTA"."PickOrder" ,
    TABLE "MobileDataModel"."BUSDTA"."M04012" ,
    TABLE "MobileDataModel"."BUSDTA"."M40116" ,
    TABLE "MobileDataModel"."BUSDTA"."M40111" 
)
GO

/** Create the user '{ml_userid}'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = '{ml_userid}') THEN
	CREATE SYNCHRONIZATION USER "{ml_userid}";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = 'subs_immediate') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "subs_immediate";
END IF
GO

/** Create subscription 'subs_immediate' to 'pub_immediate' for '{ml_userid}'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION "subs_immediate" TO "pub_immediate" FOR "{ml_userid}" 
TYPE tcpip ADDRESS 'host=192.168.1.93;port=2439'
	OPTION lt='OFF'
	SCRIPT VERSION 'SLE_SyncScript_DV.1.0.0'
GO


