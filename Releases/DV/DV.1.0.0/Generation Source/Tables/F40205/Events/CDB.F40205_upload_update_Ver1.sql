
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F40205"
SET "LFLNDS" = {ml r."LFLNDS"}, "LFGLI" = {ml r."LFGLI"}, "LFIVI" = {ml r."LFIVI"}, "LFARI" = {ml r."LFARI"}, "LFAPI" = {ml r."LFAPI"}, "LFRSGN" = {ml r."LFRSGN"}, "LFTXYN" = {ml r."LFTXYN"}, "LFPRFT" = {ml r."LFPRFT"}, "LFCDSC" = {ml r."LFCDSC"}, "LFTX01" = {ml r."LFTX01"}, "LFTX02" = {ml r."LFTX02"}, "LFGLC" = {ml r."LFGLC"}, "LFPDC1" = {ml r."LFPDC1"}, "LFPDC2" = {ml r."LFPDC2"}, "LFPDC3" = {ml r."LFPDC3"}, "LFCSJ" = {ml r."LFCSJ"}, "LFDCTO" = {ml r."LFDCTO"}, "LFART" = {ml r."LFART"}, "LFAFT" = {ml r."LFAFT"}
WHERE "LFLNTY" = {ml r."LFLNTY"}

