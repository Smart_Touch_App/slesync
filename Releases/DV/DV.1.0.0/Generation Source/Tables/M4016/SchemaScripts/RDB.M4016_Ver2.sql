
/* [BUSDTA].[M4016] - begins */

/* TableDDL - [BUSDTA].[M4016] - Start */
IF OBJECT_ID('[BUSDTA].[M4016]') IS NULL
BEGIN
CREATE TABLE "BUSDTA"."M4016" (
    "POORTP"                         varchar(8) NOT NULL
   ,"POAN8"                          numeric(8,0) NOT NULL
   ,"POITM"                          varchar(25) NOT NULL
   ,"POSTDT"                         date NOT NULL
   ,"POOSEQ"                         numeric(4,0) NULL
   ,"POLITM"                         nvarchar(25) NULL
   ,"POQTYU"                         integer NULL
   ,"POUOM"                          nvarchar(2) NULL
   ,"POLNTY"                         nvarchar(2) NULL
   ,"POSRP1"                         nvarchar(3) NULL
   ,"POSRP5"                         nvarchar(3) NULL
   ,"POSTFG"                         bit NULL DEFAULT ((0))
   ,"POCRBY"                         nvarchar(10) NULL
   ,"POCRDT"                         "datetime" NULL
   ,"POUPBY"                         nvarchar(10) NULL
   ,"POUPDT"                         "datetime" NULL
   ,PRIMARY KEY ("POORTP" ASC,"POAN8" ASC,"POITM" ASC,"POSTDT" ASC) 
)
END
/* TableDDL - [BUSDTA].[M4016] - End */

