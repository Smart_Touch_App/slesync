
/* [BUSDTA].[MoneyOrder_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrder_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrder_Verification_Detail]
	(
	  [MoneyOrderVerificationDetailId] NUMERIC(18,0) NOT NULL DEFAULT autoincrement
	, [SettlementDetailId] NUMERIC(18,0) NOT NULL
	, [MoneyOrderId] NUMERIC(18,0) NOT NULL
	, [RouteId] NUMERIC(18,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([MoneyOrderVerificationDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - End */
