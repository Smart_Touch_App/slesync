
/* [BUSDTA].[Amount_Range_Index] - begins */

/* TableDDL - [BUSDTA].[Amount_Range_Index] - Start */
IF OBJECT_ID('[BUSDTA].[Amount_Range_Index]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Amount_Range_Index]
	(
	  [RequestCode] NCHAR(1) NOT NULL
	, [AuthorizationFormatCode] NCHAR(1) NOT NULL
	, [RangeStart] NUMERIC(8,2) NOT NULL
	, [RangeEnd] NUMERIC(8,2) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_AMOUNT_RANGE_INDEX_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Amount_Range_Index] PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC)
	)

END
/* TableDDL - [BUSDTA].[Amount_Range_Index] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[Amount_Range_Index] - Start */
IF OBJECT_ID('[BUSDTA].[Amount_Range_Index_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Amount_Range_Index_del]
	(
	  [RequestCode] NCHAR(1)
	, [AuthorizationFormatCode] NCHAR(1)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RequestCode] ASC, [AuthorizationFormatCode] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Amount_Range_Index] - End */
/* TRIGGERS FOR Amount_Range_Index - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Amount_Range_Index_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Amount_Range_Index_ins
	ON BUSDTA.Amount_Range_Index AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Amount_Range_Index_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Amount_Range_Index_del.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Amount_Range_Index_del.RequestCode= inserted.RequestCode
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Amount_Range_Index_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Amount_Range_Index_upd
	ON BUSDTA.Amount_Range_Index AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Amount_Range_Index
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Amount_Range_Index.AuthorizationFormatCode= inserted.AuthorizationFormatCode AND BUSDTA.Amount_Range_Index.RequestCode= inserted.RequestCode');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Amount_Range_Index_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Amount_Range_Index_dlt
	ON BUSDTA.Amount_Range_Index AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Amount_Range_Index_del (AuthorizationFormatCode, RequestCode, last_modified )
	SELECT deleted.AuthorizationFormatCode, deleted.RequestCode, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Amount_Range_Index - END */
