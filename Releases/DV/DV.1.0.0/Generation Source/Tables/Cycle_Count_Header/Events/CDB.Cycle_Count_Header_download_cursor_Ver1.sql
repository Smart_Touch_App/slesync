
     SELECT "BUSDTA"."Cycle_Count_Header"."CycleCountID"  ,"BUSDTA"."Cycle_Count_Header"."RouteId"  ,
     "BUSDTA"."Cycle_Count_Header"."StatusId"  ,"BUSDTA"."Cycle_Count_Header"."IsCountInitiated"  ,
     "BUSDTA"."Cycle_Count_Header"."CycleCountTypeId"  ,"BUSDTA"."Cycle_Count_Header"."InitiatorId"  ,
     "BUSDTA"."Cycle_Count_Header"."CycleCountDatetime"  ,"BUSDTA"."Cycle_Count_Header"."ReasonCodeId"  ,
     "BUSDTA"."Cycle_Count_Header"."JDEQty"  ,"BUSDTA"."Cycle_Count_Header"."JDEUM"  ,"BUSDTA"."Cycle_Count_Header"."JDECycleCountNum"  ,
     "BUSDTA"."Cycle_Count_Header"."JDECycleCountDocType"  ,"BUSDTA"."Cycle_Count_Header"."CreatedBy"  ,
     "BUSDTA"."Cycle_Count_Header"."CreatedDatetime"  ,"BUSDTA"."Cycle_Count_Header"."UpdatedBy"  ,
     "BUSDTA"."Cycle_Count_Header"."UpdatedDatetime"    
     FROM "BUSDTA"."Cycle_Count_Header"  
     WHERE "BUSDTA"."Cycle_Count_Header"."last_modified">= {ml s.last_table_download}  
      AND "BUSDTA"."Cycle_Count_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})    
      AND "BUSDTA"."Cycle_Count_Header"."StatusId" <> (select StatusTypeID from busdta.status_type where StatusTypeCD = ''NEW'')  