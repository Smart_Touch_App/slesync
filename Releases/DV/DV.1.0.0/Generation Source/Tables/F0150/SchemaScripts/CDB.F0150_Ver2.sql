
/* [BUSDTA].[F0150] - begins */

/* TableDDL - [BUSDTA].[F0150] - Start */
IF OBJECT_ID('[BUSDTA].[F0150]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0150]
	(
	  [MAOSTP] NCHAR(3) NOT NULL
	, [MAPA8] NUMERIC(8,0) NOT NULL
	, [MAAN8] NUMERIC(8,0) NOT NULL
	, [MABEFD] NUMERIC(18,0) NULL
	, [MAEEFD] NUMERIC(18,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0150_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0150] PRIMARY KEY ([MAOSTP] ASC, [MAPA8] ASC, [MAAN8] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0150] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0150] - Start */
IF OBJECT_ID('[BUSDTA].[F0150_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0150_del]
	(
	  [MAOSTP] NCHAR(3)
	, [MAPA8] NUMERIC(8,0)
	, [MAAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MAOSTP] ASC, [MAPA8] ASC, [MAAN8] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0150] - End */
/* TRIGGERS FOR F0150 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0150_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0150_ins
	ON BUSDTA.F0150 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0150_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0150_del.MAAN8= inserted.MAAN8 AND BUSDTA.F0150_del.MAOSTP= inserted.MAOSTP AND BUSDTA.F0150_del.MAPA8= inserted.MAPA8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0150_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0150_upd
	ON BUSDTA.F0150 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0150
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0150.MAAN8= inserted.MAAN8 AND BUSDTA.F0150.MAOSTP= inserted.MAOSTP AND BUSDTA.F0150.MAPA8= inserted.MAPA8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0150_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0150_dlt
	ON BUSDTA.F0150 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0150_del (MAAN8, MAOSTP, MAPA8, last_modified )
	SELECT deleted.MAAN8, deleted.MAOSTP, deleted.MAPA8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0150 - END */

