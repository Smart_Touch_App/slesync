/* [BUSDTA].[Cash_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Verification_Detail]
	(
	  [CashVerificationDetailId] NUMERIC(18,0) NOT NULL
	, [SettlementDetailId] NUMERIC(18,0) NOT NULL
	, [CashTypeId] NUMERIC(18,0) NOT NULL
	, [Quantity] NUMERIC(18,0) NULL
	, [Amount] NUMERIC(18,0) NULL
	, [RouteId] NUMERIC(18,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CASH_VERIFICATION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cash_Verification_Detail] PRIMARY KEY ([CashVerificationDetailId] ASC, [RouteId] ASC)
	)

END
/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Verification_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Verification_Detail_del]
	(
	  [CashVerificationDetailId] NUMERIC(18,0)
	, [RouteId] NUMERIC(18,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CashVerificationDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Cash_Verification_Detail] - End */
/* TRIGGERS FOR Cash_Verification_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cash_Verification_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cash_Verification_Detail_ins
	ON BUSDTA.Cash_Verification_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cash_Verification_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cash_Verification_Detail_del.CashVerificationDetailId= inserted.CashVerificationDetailId AND BUSDTA.Cash_Verification_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cash_Verification_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Verification_Detail_upd
	ON BUSDTA.Cash_Verification_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cash_Verification_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cash_Verification_Detail.CashVerificationDetailId= inserted.CashVerificationDetailId AND BUSDTA.Cash_Verification_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cash_Verification_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cash_Verification_Detail_dlt
	ON BUSDTA.Cash_Verification_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cash_Verification_Detail_del (CashVerificationDetailId, RouteId, last_modified )
	SELECT deleted.CashVerificationDetailId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cash_Verification_Detail - END */
