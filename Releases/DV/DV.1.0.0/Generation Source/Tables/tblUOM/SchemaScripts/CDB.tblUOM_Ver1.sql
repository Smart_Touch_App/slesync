/* [dbo].[tblUOM] - begins */

/* TableDDL - [dbo].[tblUOM] - Start */
IF OBJECT_ID('[dbo].[tblUOM]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblUOM]
	(
	  [UOMID] INT NOT NULL IDENTITY(1,1)
	, [UOMLable] VARCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUOM_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUOM_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLUOM_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblUOM] PRIMARY KEY ([UOMID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblUOM] - End */

/* SHADOW TABLE FOR [dbo].[tblUOM] - Start */
IF OBJECT_ID('[dbo].[tblUOM_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblUOM_del]
	(
	  [UOMID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UOMID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblUOM] - End */
/* TRIGGERS FOR tblUOM - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblUOM_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblUOM_ins
	ON dbo.tblUOM AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblUOM_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblUOM_del.UOMID= inserted.UOMID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblUOM_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblUOM_upd
	ON dbo.tblUOM AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblUOM
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblUOM.UOMID= inserted.UOMID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblUOM_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblUOM_dlt
	ON dbo.tblUOM AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblUOM_del (UOMID, last_modified )
	SELECT deleted.UOMID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblUOM - END */
