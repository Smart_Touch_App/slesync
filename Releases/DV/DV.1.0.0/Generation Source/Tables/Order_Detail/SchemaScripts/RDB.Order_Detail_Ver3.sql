/* [BUSDTA].[Order_Detail] - begins */

/* TableDDL - [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail]
	(
	  [OrderDetailId] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [OrderID] NUMERIC(8,0) NOT NULL
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [OrderQty] NUMERIC(4,0) NOT NULL
	, [OrderUM] NVARCHAR (2) NULL
	, [UnitPriceAmt] NUMERIC(8,4) NULL
	, [ExtnPriceAmt] NUMERIC(8,4) NULL
	, [ItemSalesTaxAmt] NUMERIC(8,4) NULL
	, [PriceOverrideReasonCodeId] NUMERIC(3,0) NULL
	, [IsTaxable] NVARCHAR (1) NULL
	, [ReturnHeldQty] NUMERIC(4,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([OrderDetailId] ASC, [OrderID] ASC, [RouteID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Order_Detail] - End */
