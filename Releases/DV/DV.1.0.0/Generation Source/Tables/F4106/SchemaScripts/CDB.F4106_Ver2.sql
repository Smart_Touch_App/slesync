
/* [BUSDTA].[F4106] - begins */

/* TableDDL - [BUSDTA].[F4106] - Start */
IF OBJECT_ID('[BUSDTA].[F4106]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4106]
	(
	  [BPITM] NUMERIC(8,0) NOT NULL
	, [BPLITM] NCHAR(25) NULL
	, [BPMCU] NCHAR(12) NOT NULL
	, [BPLOCN] NCHAR(20) NOT NULL
	, [BPLOTN] NCHAR(30) NOT NULL
	, [BPAN8] NUMERIC(8,0) NOT NULL
	, [BPIGID] NUMERIC(8,0) NOT NULL
	, [BPCGID] NUMERIC(8,0) NOT NULL
	, [BPLOTG] NCHAR(3) NOT NULL
	, [BPFRMP] NUMERIC(7,0) NOT NULL
	, [BPCRCD] NCHAR(3) NOT NULL
	, [BPUOM] NCHAR(2) NOT NULL
	, [BPEFTJ] NUMERIC(18,0) NULL
	, [BPEXDJ] NUMERIC(18,0) NOT NULL
	, [BPUPRC] FLOAT NULL
	, [BPUPMJ] NUMERIC(18,0) NOT NULL
	, [BPTDAY] FLOAT NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4106_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4106] PRIMARY KEY ([BPITM] ASC, [BPMCU] ASC, [BPLOCN] ASC, [BPLOTN] ASC, [BPAN8] ASC, [BPIGID] ASC, [BPCGID] ASC, [BPLOTG] ASC, [BPFRMP] ASC, [BPCRCD] ASC, [BPUOM] ASC, [BPEXDJ] ASC, [BPUPMJ] ASC, [BPTDAY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4106] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4106] - Start */
IF OBJECT_ID('[BUSDTA].[F4106_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4106_del]
	(
	  [BPITM] NUMERIC(8,0)
	, [BPMCU] NCHAR(12)
	, [BPLOCN] NCHAR(20)
	, [BPLOTN] NCHAR(30)
	, [BPAN8] NUMERIC(8,0)
	, [BPIGID] NUMERIC(8,0)
	, [BPCGID] NUMERIC(8,0)
	, [BPLOTG] NCHAR(3)
	, [BPFRMP] NUMERIC(7,0)
	, [BPCRCD] NCHAR(3)
	, [BPUOM] NCHAR(2)
	, [BPEXDJ] NUMERIC(18,0)
	, [BPUPMJ] NUMERIC(18,0)
	, [BPTDAY] FLOAT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([BPITM] ASC, [BPMCU] ASC, [BPLOCN] ASC, [BPLOTN] ASC, [BPAN8] ASC, [BPIGID] ASC, [BPCGID] ASC, [BPLOTG] ASC, [BPFRMP] ASC, [BPCRCD] ASC, [BPUOM] ASC, [BPEXDJ] ASC, [BPUPMJ] ASC, [BPTDAY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4106] - End */
/* TRIGGERS FOR F4106 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4106_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4106_ins
	ON BUSDTA.F4106 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4106_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4106_del.BPAN8= inserted.BPAN8 AND BUSDTA.F4106_del.BPCGID= inserted.BPCGID AND BUSDTA.F4106_del.BPCRCD= inserted.BPCRCD AND BUSDTA.F4106_del.BPEXDJ= inserted.BPEXDJ AND BUSDTA.F4106_del.BPFRMP= inserted.BPFRMP AND BUSDTA.F4106_del.BPIGID= inserted.BPIGID AND BUSDTA.F4106_del.BPITM= inserted.BPITM AND BUSDTA.F4106_del.BPLOCN= inserted.BPLOCN AND BUSDTA.F4106_del.BPLOTG= inserted.BPLOTG AND BUSDTA.F4106_del.BPLOTN= inserted.BPLOTN AND BUSDTA.F4106_del.BPMCU= inserted.BPMCU AND BUSDTA.F4106_del.BPTDAY= inserted.BPTDAY AND BUSDTA.F4106_del.BPUOM= inserted.BPUOM AND BUSDTA.F4106_del.BPUPMJ= inserted.BPUPMJ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4106_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4106_upd
	ON BUSDTA.F4106 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4106
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4106.BPAN8= inserted.BPAN8 AND BUSDTA.F4106.BPCGID= inserted.BPCGID AND BUSDTA.F4106.BPCRCD= inserted.BPCRCD AND BUSDTA.F4106.BPEXDJ= inserted.BPEXDJ AND BUSDTA.F4106.BPFRMP= inserted.BPFRMP AND BUSDTA.F4106.BPIGID= inserted.BPIGID AND BUSDTA.F4106.BPITM= inserted.BPITM AND BUSDTA.F4106.BPLOCN= inserted.BPLOCN AND BUSDTA.F4106.BPLOTG= inserted.BPLOTG AND BUSDTA.F4106.BPLOTN= inserted.BPLOTN AND BUSDTA.F4106.BPMCU= inserted.BPMCU AND BUSDTA.F4106.BPTDAY= inserted.BPTDAY AND BUSDTA.F4106.BPUOM= inserted.BPUOM AND BUSDTA.F4106.BPUPMJ= inserted.BPUPMJ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4106_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4106_dlt
	ON BUSDTA.F4106 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4106_del (BPAN8, BPCGID, BPCRCD, BPEXDJ, BPFRMP, BPIGID, BPUOM, BPUPMJ, BPITM, BPLOCN, BPLOTG, BPLOTN, BPMCU, BPTDAY, last_modified )
	SELECT deleted.BPAN8, deleted.BPCGID, deleted.BPCRCD, deleted.BPEXDJ, deleted.BPFRMP, deleted.BPIGID, deleted.BPUOM, deleted.BPUPMJ, deleted.BPITM, deleted.BPLOCN, deleted.BPLOTG, deleted.BPLOTN, deleted.BPMCU, deleted.BPTDAY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4106 - END */

