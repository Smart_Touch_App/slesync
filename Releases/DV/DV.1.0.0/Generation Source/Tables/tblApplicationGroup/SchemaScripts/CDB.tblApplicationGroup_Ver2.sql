/* [dbo].[tblApplicationGroup] - begins */

/* TableDDL - [dbo].[tblApplicationGroup] - Start */
IF OBJECT_ID('[dbo].[tblApplicationGroup]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblApplicationGroup]
	(
	  [ApplicationGroupID] INT NOT NULL IDENTITY(1,1)
	, [ApplicationGroup] VARCHAR(128) NULL
	, [SortOrder] INT NULL
	, [IsMandatorySync] BIT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLAPPLICATIONGROUP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblApplicationGroup] PRIMARY KEY ([ApplicationGroupID] ASC)
	)

END
/* TableDDL - [dbo].[tblApplicationGroup] - End */
GO

/* SHADOW TABLE FOR [dbo].[tblApplicationGroup] - Start */
IF OBJECT_ID('[dbo].[tblApplicationGroup_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblApplicationGroup_del]
	(
	  [ApplicationGroupID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ApplicationGroupID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblApplicationGroup] - End */
/* TRIGGERS FOR tblApplicationGroup - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblApplicationGroup_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblApplicationGroup_ins
	ON dbo.tblApplicationGroup AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblApplicationGroup_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblApplicationGroup_del.ApplicationGroupID= inserted.ApplicationGroupID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblApplicationGroup_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblApplicationGroup_upd
	ON dbo.tblApplicationGroup AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblApplicationGroup
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblApplicationGroup.ApplicationGroupID= inserted.ApplicationGroupID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblApplicationGroup_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblApplicationGroup_dlt
	ON dbo.tblApplicationGroup AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblApplicationGroup_del (ApplicationGroupID, last_modified )
	SELECT deleted.ApplicationGroupID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblApplicationGroup - END */
