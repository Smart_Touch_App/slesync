
/* [BUSDTA].[Order_Header] - begins */

/* TableDDL - [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header]
	(
	  [OrderID] NUMERIC(8,0) NOT NULL
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [OrderTypeId] NUMERIC(3,0) NOT NULL
	, [CustomerId] NUMERIC(8,0) NOT NULL
	, [OrderDate] DATE NULL
	, [TotalCoffeeAmt] NUMERIC(12,4) NULL
	, [TotalAlliedAmt] NUMERIC(12,4) NULL
	, [OrderTotalAmt] NUMERIC(12,4) NULL
	, [SalesTaxAmt] NUMERIC(8,4) NULL
	, [InvoiceTotalAmt] NUMERIC(12,4) NULL
	, [EnergySurchargeAmt] NUMERIC(8,4) NULL
	, [SurchargeReasonCodeId] NUMERIC(3,0) NULL
	, [OrderStateId] NUMERIC(3,0) NULL
	, [OrderSign] VARBINARY(MAX) NULL
	, [ChargeOnAccountSign] VARBINARY(MAX) NULL
	, [VoidReasonCodeId] NUMERIC(3,0) NULL
	, [ChargeOnAccount] NCHAR(1) NULL
	, [HoldCommitted] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_ORDER_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Order_Header] PRIMARY KEY ([OrderID] ASC, [RouteID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Order_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header_del]
	(
	  [OrderID] NUMERIC(8,0)
	, [RouteID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([OrderID] ASC, [RouteID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - End */
/* TRIGGERS FOR Order_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Header_ins
	ON BUSDTA.Order_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Header_del.OrderID= inserted.OrderID AND BUSDTA.Order_Header_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_upd
	ON BUSDTA.Order_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Header.OrderID= inserted.OrderID AND BUSDTA.Order_Header.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_dlt
	ON BUSDTA.Order_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Header_del (OrderID, RouteID, last_modified )
	SELECT deleted.OrderID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Header - END */
