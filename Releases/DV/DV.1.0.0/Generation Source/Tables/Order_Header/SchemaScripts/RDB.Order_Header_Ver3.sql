/* [BUSDTA].[Order_Header] - begins */

/* TableDDL - [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header]
	(
	  [OrderID] NUMERIC(8,0) NOT NULL
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [OrderTypeId] NUMERIC(3,0) NOT NULL
	, [CustomerId] NUMERIC(8,0) NOT NULL
	, [OrderDate] DATE NULL
	, [TotalCoffeeAmt] NUMERIC(12,4) NULL
	, [TotalAlliedAmt] NUMERIC(12,4) NULL
	, [OrderTotalAmt] NUMERIC(12,4) NULL
	, [SalesTaxAmt] NUMERIC(8,4) NULL
	, [InvoiceTotalAmt] NUMERIC(12,4) NULL
	, [EnergySurchargeAmt] NUMERIC(8,4) NULL
	, [SurchargeReasonCodeId] NUMERIC(3,0) NULL
	, [OrderStateId] NUMERIC(3,0) NULL
	, [OrderSign] LONG BINARY NULL
	, [ChargeOnAccountSign] LONG BINARY NULL
	, [VoidReasonCodeId] NUMERIC(3,0) NULL
	, [ChargeOnAccount] NVARCHAR (1) NULL
	, [HoldCommitted] NVARCHAR (1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([OrderID] ASC, [RouteID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Order_Header] - End */
