 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Header"
("OrderID", "RouteID", "OrderTypeId", "CustomerId", "OrderDate", "TotalCoffeeAmt", "TotalAlliedAmt", "OrderTotalAmt", "SalesTaxAmt", "InvoiceTotalAmt", "EnergySurchargeAmt", "SurchargeReasonCodeId", "OrderStateId", "OrderSign", "ChargeOnAccountSign", "VoidReasonCodeId", "ChargeOnAccount", "HoldCommitted", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderID"}, {ml r."RouteID"}, {ml r."OrderTypeId"}, {ml r."CustomerId"}, {ml r."OrderDate"}, {ml r."TotalCoffeeAmt"}, {ml r."TotalAlliedAmt"}, {ml r."OrderTotalAmt"}, {ml r."SalesTaxAmt"}, {ml r."InvoiceTotalAmt"}, {ml r."EnergySurchargeAmt"}, {ml r."SurchargeReasonCodeId"}, {ml r."OrderStateId"}, {ml r."OrderSign"}, {ml r."ChargeOnAccountSign"}, {ml r."VoidReasonCodeId"}, {ml r."ChargeOnAccount"}, {ml r."HoldCommitted"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
