/* [dbo].[tblUsageMeasurement] - begins */

/* TableDDL - [dbo].[tblUsageMeasurement] - Start */
IF OBJECT_ID('[dbo].[tblUsageMeasurement]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblUsageMeasurement]
	(
	  [UsageMeasurementID] INT NOT NULL IDENTITY(1,1)
	, [UsageMeasurement] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUSAGEMEASUREMENT_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLUSAGEMEASUREMENT_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLUSAGEMEASUREMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblUsageMeasurement] PRIMARY KEY ([UsageMeasurementID] ASC)
	)

END
/* TableDDL - [dbo].[tblUsageMeasurement] - End */
GO
/* SHADOW TABLE FOR [dbo].[tblUsageMeasurement] - Start */
IF OBJECT_ID('[dbo].[tblUsageMeasurement_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblUsageMeasurement_del]
	(
	  [UsageMeasurementID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UsageMeasurementID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblUsageMeasurement] - End */
/* TRIGGERS FOR tblUsageMeasurement - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblUsageMeasurement_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblUsageMeasurement_ins
	ON dbo.tblUsageMeasurement AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblUsageMeasurement_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblUsageMeasurement_del.UsageMeasurementID= inserted.UsageMeasurementID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblUsageMeasurement_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblUsageMeasurement_upd
	ON dbo.tblUsageMeasurement AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblUsageMeasurement
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblUsageMeasurement.UsageMeasurementID= inserted.UsageMeasurementID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblUsageMeasurement_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblUsageMeasurement_dlt
	ON dbo.tblUsageMeasurement AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblUsageMeasurement_del (UsageMeasurementID, last_modified )
	SELECT deleted.UsageMeasurementID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblUsageMeasurement - END */
