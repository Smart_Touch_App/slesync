CREATE OR REPLACE FUNCTION "BUSDTA"."DateG2J" (@julian varchar(6))
RETURNS datetime AS BEGIN
	DECLARE @datetime datetime

	SET @datetime = CAST(19+CAST(SUBSTRING(@julian, 1, 1) as int) as varchar(4))+SUBSTRING(@julian, 2,2)+'-01-01'
	SET @datetime = DATEADD(day, CAST(SUBSTRING(@julian, 4,3) as int)-1, @datetime)

	RETURN @datetime
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."GetDayForDate" (CalendarDate date, BaseDate date)
RETURNS int
BEGIN
	DECLARE DateDifference int;

	set DateDifference = DATEPART (DW, CalendarDate)-1;

	RETURN DateDifference;
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."GetWeekForDate" (CalendarDate date, BaseDate date, WeeksPerCycle integer default 4)
RETURNS int
 BEGIN
	DECLARE DateDifference int;
	DECLARE varDiv7 float;
	DECLARE WeekNumber float;
	DECLARE ModNumber float;

    IF (WeeksPerCycle <> 0) THEN
    	select DATEDIFF(DAY,BaseDate,CalendarDate)+1 into DateDifference from dummy;
    	select CEILING (DateDifference / 7.00) into varDiv7 from dummy;
    	select varDiv7%WeeksPerCycle into ModNumber from dummy;
     	IF (ModNumber = 0) THEN
    		set WeekNumber = WeeksPerCycle;
    	ELSE
    		set WeekNumber = ModNumber;
    	END IF;
    END IF;
    RETURN cast(WeekNumber as int)
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."getLastStopDate"(CustomerId numeric(8), SelectedDate date, BaseDate date, WithUnplanned bit)
RETURNS DATE
BEGIN

    DECLARE LastStopDate int;
    DECLARE StopType varchar(15);
    DECLARE RefStopId NUMERIC(8,0);
    DECLARE prevDate date;
    DECLARE  Week numeric(4,0); -- Week Number for Delivery Day Code
    DECLARE  Day numeric(4,0); -- Day Number for Delivery Day Code
    DECLARE  WeeksPerCycle integer;
    DECLARE  WeekNumber numeric(2,0); -- Week Number for SelectedDate
	DECLARE  DayNumber numeric(2,0); -- Day Number for SelectedDate
    DECLARE i INTEGER = 1;
    DECLARE Years INTEGER = 1;
    DECLARE loopUpTo INTEGER = (Years * 100);

    SELECT DCWN, DCDN, DMWPC INTO Week, Day, WeeksPerCycle
      FROM BUSDTA.M56M0002, BUSDTA.M56M0001
      WHERE DCDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerId) and DCDDC = DMDDC;
	/*Consider Unplanned stops also*/
    IF WithUnplanned=1 THEN
        SELECT TOP 1 RPSTDT INTO LastStopDate FROM BUSDTA.M56M0004
        where RPAN8 = CustomerId and RPSTDT < SelectedDate and RPSTTP <> 'Moved'
        order by RPSTDT desc;  
        /*If no last stop exists, traverse the date in reverse order and compare with pattern as per the DDC*/
        if isnull(LastStopDate,0) = 0 then
            ilp: LOOP
            IF i > loopUpTo THEN LEAVE ilp END IF;
                select dateadd(day, -i, SelectedDate) into prevDate from dummy; 
                select busdta.GetWeekForDate(prevDate, BaseDate, WeeksPerCycle) into WeekNumber from dummy;
                select busdta.GetDayForDate(prevDate, BaseDate) into DayNumber from dummy ;
                IF Week = WeekNumber AND Day = DayNumber THEN
                    set LastStopDate =  prevDate;
                    LEAVE ilp;
                 END IF;
                set i = i+1;
            END LOOP;
        end if;
    ELSE
            select top 1 CAST(Cast(val as Date) AS INT) into LastStopDate from 
            "BUSDTA"."getPreOrderDatesForCustomer"("CustomerNum" = CustomerId,"SelectedDate" = BaseDate,"BaseDate" = BaseDate) 
            where val < SelectedDate 
            ORDER BY val desc;

            select RPSTTP, RPRSTID into StopType,RefStopId  from busdta.M56M0004 where RPAN8 =  CustomerId and RPSTDT = LastStopDate and RPSTTP <> 'Unplanned';
			/*Check if Last stop is moved*/
            IF StopType = 'Moved' THEN
                select RPSTDT into LastStopDate from busdta.M56M0004 where RPSTID = RefStopId;
                IF LastStopDate >= SelectedDate THEN
                    select LstDT  into LastStopDate  from (
                    select  CAST(Cast(val as Date) AS INT) as 'LstDT', (row_number() over (order by LstDT desc)) as RowNum from 
                    "BUSDTA"."getPreOrderDatesForCustomer"("CustomerNum" = CustomerId,"SelectedDate" = BaseDate,"BaseDate" = BaseDate) 
                    where val < SelectedDate 
                    ORDER BY val desc) a where RowNum = 2;

                END IF;
            END IF;
    END IF;

RETURN LastStopDate;
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."getNextStopDate"(CustomerId numeric(8), SelectedDate date, BaseDate date, WithUnplanned bit)
RETURNS DATE
BEGIN

	DECLARE  WeekNumber numeric(2,0); -- Week Number for SelectedDate
	DECLARE  DayNumber numeric(2,0); -- Day Number for SelectedDate
    DECLARE nextDate date;
    DECLARE nextStopDate date;
    DECLARE MovedCreateStopDate date;
    DECLARE StopType nvarchar(15);
    DECLARE WeeksPerCycle integer;
    /*This cursor gets the pattern, for a Delivery Day Code of the customer.*/
    DECLARE i INTEGER = 1;
    DECLARE Years INTEGER = 1;
    DECLARE loopUpTo INTEGER = (Years * 366); 
    DECLARE cur_DayCodePattern CURSOR FOR
      SELECT DCWN, DCDN
      FROM BUSDTA.M56M0002 
      WHERE DCDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerId);

    DECLARE  Week numeric(4,0); -- Week Number for Delivery Day Code
    DECLARE  Day numeric(4,0); -- Day Number for Delivery Day Code

    DECLARE LOCAL TEMPORARY TABLE TempTab ( val as nchar(50) ); -- Temporary table to store the next dates
    select DMWPC into WeeksPerCycle from busdta.M56M0001 WHERE DMDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerId);
    IF WeeksPerCycle = 0 THEN return null; END IF;
    lp: Loop
        IF i = loopUpTo THEN LEAVE lp END IF;
        select dateadd(day, i, SelectedDate) into nextDate from dummy;

        /*Get the Period, Week and Day number for the given date, with respect to BaseDate*/
        select busdta.GetWeekForDate(nextDate, BaseDate,WeeksPerCycle) into WeekNumber from dummy;
        select busdta.GetDayForDate(nextDate, BaseDate) into DayNumber from dummy ;
    
        /*Inner Loop: This loop generates the dates for pattern of Delivery Day Code, to be compared with the calendar dates.*/
        OPEN cur_DayCodePattern;    
        ilp: LOOP
            FETCH next cur_DayCodePattern into Week, Day;
                IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                    /*If Calendar date matches the Pattern dates, it is a qualified date.*/
                    IF Week = WeekNumber AND Day = DayNumber THEN
                        /*If qualified date is greater than than the selected date, then it is the next valid date.*/
                        IF SelectedDate < nextDate THEN
                            IF WithUnplanned=1 THEN
                                /*Check whether there is any moved or created stopped before planned next stop date. If there is, it is the next date*/
                                select first RPSTDT, RPSTTP into MovedCreateStopDate, StopType from busdta.M56M0004 
                                where RPSTDT > SelectedDate and RPSTDT <= nextDate and RPAN8 = CustomerId order by 1 asc;
                                IF StopType = 'Moved' THEN 
                                    set SelectedDate = MovedCreateStopDate;
				                    set StopType = '';
                                    set MovedCreateStopDate = null;
                                    continue ilp 
                                END IF;
                                IF MovedCreateStopDate < nextDate THEN
                                    select MovedCreateStopDate into nextStopDate from dummy;
                                ELSE
                                    select nextDate into nextStopDate from dummy;
                                END IF;
                                RETURN nextStopDate;
                             ELSE
                                /*Check whether there is any moved or created stopped before planned next stop date. If there is, it is the next date*/
                                select first RPSTDT, RPSTTP into MovedCreateStopDate, StopType from busdta.M56M0004 
                                where RPSTDT > SelectedDate and RPSTDT <= nextDate and RPAN8 = CustomerId and RPSTTP <> 'Unplanned' order by 1 asc;
                                IF StopType = 'Moved' THEN 
                                    set SelectedDate = MovedCreateStopDate;
				                    set StopType = '';
                                    set MovedCreateStopDate = null;
                                    continue ilp 
                                END IF;
                                IF MovedCreateStopDate < nextDate THEN
                                    select MovedCreateStopDate into nextStopDate from dummy;
                                ELSE
                                    select nextDate into nextStopDate from dummy;
                                END IF;
                                RETURN nextStopDate;
                            END IF;
                        END IF;
                    END IF;
        END LOOP; -- End Inner Loop    

        CLOSE cur_DayCodePattern;
        SET i = i + 1;
    END LOOP; -- End Outer Loop
RETURN nextStopDate;
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."GetPeriodForDate" (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int
	DECLARE @varDiv28 float
	DECLARE @PeriodNumber float
	DECLARE @ModNumber float

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate)+1
	select @varDiv28 = CEILING (@DateDifference / 28.00)
	select @ModNumber = @varDiv28%3
	if(@ModNumber = 0)
		select @PeriodNumber = 3
	else
		select @PeriodNumber = @ModNumber
	RETURN cast(@PeriodNumber as int)
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."GetUDCDescription" (@DRSY varchar(3), @DRRT varchar(3), @DRKY varchar(3))
RETURNS varchar(30)
AS BEGIN
	DECLARE @DRDL01 varchar(30)

	select  @DRDL01 = DRDL01  from BUSDTA.F0005 where  DRSY = @DRSY and DRRT = @DRRT and ltrim(DRKY) = @DRKY

	RETURN @DRDL01
END
/*------------------------------------------------------------------------------*/
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."GetABADescriptionFromABAId" (@ABAId numeric(8,0))
RETURNS nchar(40)
AS BEGIN
	DECLARE @ABALPH nchar(40)

	select @ABALPH =  ABALPH from BUSDTA.F0101 ab where ab.ABAN8 = @ABAId

	RETURN @ABALPH
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."GetNextNumber"(  RouteID nvarchar(50), EntityID  nvarchar(50) )
	RETURNS nvarchar(50)
BEGIN
	declare poolentityID nvarchar(50);
	declare poolroute nvarchar(50);
	declare returnNumber nvarchar(50);
	declare returnNumberFormat nvarchar(50);
	declare poolcount numeric(10);
	declare poolbucket nvarchar(50);
	declare poolstart integer;
	declare poolend integer;
	set poolentityID=EntityID;
	set poolroute=RouteID;
		select FIRST RangeStart,RangeEnd,isnull(CurrentAllocated,RangeStart-1)+1,Bucket into poolstart ,poolend ,poolcount, poolbucket from   BUSDTA.Entity_Number_Status 
			where 
			EntityBucketMasterId=poolentityID
			and isconsumed=0
			and RouteID=poolroute
			order by CreatedDatetime asc,bucket asc;
		UPDATE  BUSDTA.Entity_Number_Status
			SET CurrentAllocated = isnull(poolcount,poolstart)
			where 
			EntityBucketMasterId=poolentityID
			and isconsumed=0
			and RouteID=poolroute
			and Bucket=poolbucket;
		IF poolcount >= poolend THEN
			UPDATE  BUSDTA.Entity_Number_Status 
			SET isconsumed = 1,
			ConsumedOnDate=getdate()
			where 
			EntityBucketMasterId=poolentityID
			and isconsumed=0
			and RouteID=poolroute
			and Bucket=poolbucket
		END IF;
		--IF poolcount > 0 THEN
		--	select ''+ replicate('0',"Padding" - datalength(convert(varchar(10),poolcount))) + convert(varchar(10),poolcount),"Format" into returnNumber,returnNumberFormat 
		--  from BUSDTA.Entity_Bucket_Master 
		--	where EntityBucketMasterId=poolentityID;
		--	set returnNumber = replace(returnNumberFormat,'{0}',returnNumber);
		--END IF;
	COMMIT;
	return poolcount;
END
GO
CREATE OR REPLACE FUNCTION "BUSDTA"."GetDefaultPhone"( in "CustomerId" numeric(8) ) 
RETURNS nchar(25)
BEGIN
  DECLARE "PhoneNumber" nchar(25);
  DECLARE "ABCount" integer;
  DECLARE "PH1" bit;
  DECLARE "PH2" bit;
  DECLARE "PH3" bit;
  DECLARE "PH4" bit;
  DECLARE "whoswho" bit;
  DECLARE "LineNum" bit;
  DECLARE "prosph" bit;
  DECLARE cur_CustomerPhone CURSOR FOR
      SELECT CDDFLTPH1, CDDFLTPH2, CDDFLTPH3, CDDFLTPH4, CDIDLN, CDRCK7
      FROM BUSDTA.M0111 
      WHERE CDAN8 = CustomerId AND CDIDLN=0;
  DECLARE cur_ProspectPhone CURSOR FOR
      SELECT IsDefaultPhone1, IsDefaultPhone2, IsDefaultPhone3, IsDefaultPhone4 ,ProspectContactID 
      FROM BUSDTA.Prospect_Contact
      WHERE ProspectId = CustomerId AND ProspectContactID = 1;
  set PhoneNumber = 'NA';
  set "ABCount" = 0;

  select "count"("ProspectId") into "ABCount" from "busdta"."Prospect_Master" where "ProspectId" = "CustomerId";
  IF "ABCount" = 0 THEN
    OPEN cur_CustomerPhone;    
        ilp: LOOP
            FETCH next cur_CustomerPhone into PH1, PH2, PH3, PH4, whoswho, LineNum;
            IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                IF "PH1" = 1 THEN
                  select STRING(CASE ISNULL(CDAR1,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR1),') ') END,RTRIM(CDPH1)  , CASE ISNULL(CDEXTN1,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN1)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSEIF "PH2" = 1 THEN
                  select STRING(CASE ISNULL(CDAR2,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR2),') ') END,RTRIM(CDPH2)  , CASE ISNULL(CDEXTN2,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN2)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSEIF "PH3" = 1 THEN
                  select STRING(CASE ISNULL(CDAR3,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR3),') ') END,RTRIM(CDPH3)  , CASE ISNULL(CDEXTN3,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN3)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSEIF "PH4" = 1 THEN
                  select STRING(CASE ISNULL(CDAR4,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR4),') ') END,RTRIM(CDPH4)  , CASE ISNULL(CDEXTN4,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN4)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSE
                  select 'NA' into "PhoneNumber" from "dummy";
            END IF;
        END LOOP;     
    CLOSE cur_CustomerPhone;
  ELSE 
       OPEN cur_ProspectPhone;    
        ilp: LOOP
            FETCH next cur_ProspectPhone into PH1, PH2, PH3, PH4,prosph;
            IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                IF "PH1" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode1,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode1),') ') END,RTRIM(Phone1)  , CASE ISNULL(EXTN1,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN1)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId" AND ProspectContactID = 1; --and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSEIF "PH2" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode2,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode2),') ') END,RTRIM(Phone2)  , CASE ISNULL(EXTN2,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN2)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId" AND ProspectContactID = 1;-- and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSEIF "PH3" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode3,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode3),') ') END,RTRIM(Phone3)  , CASE ISNULL(EXTN3,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN3)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId" AND ProspectContactID = 1;-- and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSEIF "PH4" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode4,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode4),') ') END,RTRIM(Phone4)  , CASE ISNULL(EXTN4,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN4)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId" AND ProspectContactID = 1;-- and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSE
                  select 'NA' into "PhoneNumber" from "dummy";
            END IF;
        END LOOP;     
    CLOSE cur_ProspectPhone;
  END IF;
  RETURN "PhoneNumber";
END
