@ECHO OFF
setlocal

echo Stoping remote database....

CALL Remote_Connection_Vars.bat

echo Starting remote database....

dbstop -c "Server=%server%;UID=%rdbUname%;PWD=%rdbPass%"

echo Remote database stoped.

GOTO End

:End
EXIT
