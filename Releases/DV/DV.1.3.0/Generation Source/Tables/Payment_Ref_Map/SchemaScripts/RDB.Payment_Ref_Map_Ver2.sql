/* [BUSDTA].[Payment_Ref_Map] - begins */

/* TableDDL - [BUSDTA].[Payment_Ref_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Payment_Ref_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Payment_Ref_Map]
	(
	  [Payment_Ref_Map_Id] INTEGER NOT NULL DEFAULT autoincrement
	, [Payment_Id] INTEGER NOT NULL
	, [Ref_Id] INTEGER NOT NULL
	, [Ref_Type] NVARCHAR (3) NULL
	, PRIMARY KEY ([Payment_Ref_Map_Id] ASC, [Payment_Id] ASC, [Ref_Id] ASC)
	)
END
/* TableDDL - [BUSDTA].[Payment_Ref_Map] - End */
