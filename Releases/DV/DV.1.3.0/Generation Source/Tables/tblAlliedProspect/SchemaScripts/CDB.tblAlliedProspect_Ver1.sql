/* [dbo].[tblAlliedProspect] - begins */

/* TableDDL - [dbo].[tblAlliedProspect] - Start */
IF OBJECT_ID('[dbo].[tblAlliedProspect]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblAlliedProspect]
	(
	  [AlliedProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, [CompetitorID] INT NULL
	, [CategoryID] INT NULL
	, [SubCategoryID] INT NULL
	, [BrandID] INT NULL
	, [UOMID] INT NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] NUMERIC(8,0) NULL
	, [UsageMeasurementID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPECT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblAlliedProspect] PRIMARY KEY ([AlliedProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblAlliedProspect] - End */

/* SHADOW TABLE FOR [dbo].[tblAlliedProspect] - Start */
IF OBJECT_ID('[dbo].[tblAlliedProspect_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblAlliedProspect_del]
	(
	  [AlliedProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([AlliedProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblAlliedProspect] - End */
/* TRIGGERS FOR tblAlliedProspect - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblAlliedProspect_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblAlliedProspect_ins
	ON dbo.tblAlliedProspect AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblAlliedProspect_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblAlliedProspect_del.AlliedProspectID= inserted.AlliedProspectID AND dbo.tblAlliedProspect_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblAlliedProspect_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblAlliedProspect_upd
	ON dbo.tblAlliedProspect AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblAlliedProspect
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblAlliedProspect.AlliedProspectID= inserted.AlliedProspectID AND dbo.tblAlliedProspect.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblAlliedProspect_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblAlliedProspect_dlt
	ON dbo.tblAlliedProspect AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblAlliedProspect_del (AlliedProspectID,ProspectID, last_modified )
	SELECT deleted.AlliedProspectID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblAlliedProspect - END */
