/* [BUSDTA].[Request_Authorization_Format] - begins */

/* TableDDL - [BUSDTA].[Request_Authorization_Format] - Start */
IF OBJECT_ID('[BUSDTA].[Request_Authorization_Format]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Request_Authorization_Format]
	(
	  [RequestFormatCode] NVARCHAR (1) NOT NULL
	, [AuthorizationFormatCode] NVARCHAR (1) NOT NULL
	, [Key1] NVARCHAR (1) NOT NULL
	, [Key2] NVARCHAR (1) NOT NULL
	, [Key3] NVARCHAR (1) NOT NULL
	, [RouteId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([RequestFormatCode] ASC, [AuthorizationFormatCode] ASC, [Key1] ASC, [Key2] ASC, [Key3] ASC)
	)
END
/* TableDDL - [BUSDTA].[Request_Authorization_Format] - End */
