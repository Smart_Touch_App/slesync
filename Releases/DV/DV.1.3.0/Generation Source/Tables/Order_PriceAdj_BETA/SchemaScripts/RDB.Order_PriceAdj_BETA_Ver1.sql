/* [BUSDTA].[Order_PriceAdj_BETA] - begins */

/* TableDDL - [BUSDTA].[Order_PriceAdj_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj_BETA]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_PriceAdj_BETA]	(	  [OrderID] NUMERIC(8,0) NOT NULL	, [LineID] NUMERIC(7,4) NOT NULL	, [AdjustmentSeq] NUMERIC(4,0) NOT NULL	, [AdjName] NVARCHAR(20) NULL	, [AdjDesc] NVARCHAR(60) NULL	, [FactorValue] NUMERIC(12,4) NULL	, [UnitPrice] NUMERIC(12,4) NULL	, [OP] NVARCHAR(2) NULL	, [BC] NVARCHAR(2) NULL	, [BasicDesc] NVARCHAR(60) NULL	, [PriceAdjustmentKeyID] NUMERIC(8,0) NULL	, [CreatedBy] NUMERIC(8,0) NULL	, [CreatedDatetime] DATETIME NULL	, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL	, PRIMARY KEY ([OrderID] ASC, [LineID] ASC, [AdjustmentSeq] ASC)	)
END
/* TableDDL - [BUSDTA].[Order_PriceAdj_BETA] - End */
