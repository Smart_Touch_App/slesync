/* [dbo].[tblPacketSize] - begins */

/* TableDDL - [dbo].[tblPacketSize] - Start */
IF OBJECT_ID('[dbo].[tblPacketSize]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblPacketSize]
	(
	  [PacketSizeID] INTEGER NOT NULL 
	, [Size] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([PacketSizeID] ASC)
	)
END
/* TableDDL - [dbo].[tblPacketSize] - End */
