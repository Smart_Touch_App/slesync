 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Prospect_Quote_Detail"
SET "ItemId" = {ml r."ItemId"}, "QuoteQty" = {ml r."QuoteQty"}, "SampleQty" = {ml r."SampleQty"}, "SampleUM" = {ml r."SampleUM"}, "PricingAmt" = {ml r."PricingAmt"}, "PricingUM" = {ml r."PricingUM"}, "TransactionAmt" = {ml r."TransactionAmt"}, "TransactionUM" = {ml r."TransactionUM"}, "OtherAmt" = {ml r."OtherAmt"}, "OtherUM" = {ml r."OtherUM"}, "CompetitorName" = {ml r."CompetitorName"}, "UnitPriceAmt" = {ml r."UnitPriceAmt"}, "UnitPriceUM" = {ml r."UnitPriceUM"}, "AvailableQty" = {ml r."AvailableQty"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ProspectQuoteId" = {ml r."ProspectQuoteId"} AND "ProspectQuoteDetailId" = {ml r."ProspectQuoteDetailId"} AND "RouteId" = {ml r."RouteId"}
 
