
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M40116"
SET "PAADD1" = {ml r."PAADD1"}, "PAADD2" = {ml r."PAADD2"}, "PAADD3" = {ml r."PAADD3"}, "PAADD4" = {ml r."PAADD4"}, "PAADDZ" = {ml r."PAADDZ"}, "PACTY1" = {ml r."PACTY1"}, "PACOUN" = {ml r."PACOUN"}, "PAADDS" = {ml r."PAADDS"}, "PACRBY" = {ml r."PACRBY"}, "PACRDT" = {ml r."PACRDT"}, "PAUPBY" = {ml r."PAUPBY"}, "PAUPDT" = {ml r."PAUPDT"}
WHERE "PAAN8" = {ml r."PAAN8"}
 

