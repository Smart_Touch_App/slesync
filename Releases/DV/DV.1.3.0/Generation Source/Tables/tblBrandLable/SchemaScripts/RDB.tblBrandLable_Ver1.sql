/* [dbo].[tblBrandLable] - begins */

/* TableDDL - [dbo].[tblBrandLable] - Start */
IF OBJECT_ID('[dbo].[tblBrandLable]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblBrandLable]
	(
	  [BrandID] INTEGER NOT NULL 
	, [BrandLable] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([BrandID] ASC)
	)
END
/* TableDDL - [dbo].[tblBrandLable] - End */
