
/* *** [dbo].[tblSyncDetails] - Start ***/
IF OBJECT_ID('[dbo].[tblSyncDetails]') IS NULL
BEGIN
CREATE TABLE [dbo].[tblSyncDetails]
(
	[ObjectID] [int] NOT NULL IDENTITY(1, 1),
	[Objectname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastSyncVersion] [bigint] NULL,
	[TruncTimeAtCDB] [datetime] NULL,
	[UpdatedDateTime] [datetime] NULL DEFAULT (getdate())
)
END
/* TableDDL - [dbo].[tblSyncDetails] - End */
GO
IF OBJECT_ID('dbo.tblsyncdetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER [dbo].[tblsyncdetails_upd]
	ON [dbo].[tblSyncDetails] AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE DBO.tblsyncdetails
	SET UpdatedDateTime = GETDATE()
	FROM inserted
		WHERE dbo.tblsyncdetails.ObjectID= inserted.ObjectID);
END
GO