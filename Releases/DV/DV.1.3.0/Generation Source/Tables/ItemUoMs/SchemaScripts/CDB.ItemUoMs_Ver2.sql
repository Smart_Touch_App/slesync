
/* [BUSDTA].[ItemUoMs] - begins */

/* TableDDL - [BUSDTA].[ItemUoMs] - Start */
IF OBJECT_ID('[BUSDTA].[ItemUoMs]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemUoMs]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [UOM] NCHAR(2) NOT NULL
	, [CrossReferenceID] NUMERIC(8,0) NULL
	, [CanSell] BIT NULL
	, [CanSample] BIT NULL
	, [CanRestock] BIT NULL
	, [DisplaySeq] NUMERIC(2,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ITEMUOMS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ItemUoMs] PRIMARY KEY ([ItemID] ASC, [UOM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[ItemUoMs] - End */

/* SHADOW TABLE FOR [BUSDTA].[ItemUoMs] - Start */
IF OBJECT_ID('[BUSDTA].[ItemUoMs_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemUoMs_del]
	(
	  [ItemID] NUMERIC(8,0)
	, [UOM] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemID] ASC, [UOM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[ItemUoMs] - End */
/* TRIGGERS FOR ItemUoMs - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ItemUoMs_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ItemUoMs_ins
	ON BUSDTA.ItemUoMs AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ItemUoMs_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ItemUoMs_del.ItemID= inserted.ItemID AND BUSDTA.ItemUoMs_del.UOM= inserted.UOM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ItemUoMs_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemUoMs_upd
	ON BUSDTA.ItemUoMs AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ItemUoMs
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ItemUoMs.ItemID= inserted.ItemID AND BUSDTA.ItemUoMs.UOM= inserted.UOM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ItemUoMs_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemUoMs_dlt
	ON BUSDTA.ItemUoMs AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ItemUoMs_del (ItemID, UOM, last_modified )
	SELECT deleted.ItemID, deleted.UOM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ItemUoMs - END */

