
/* [BUSDTA].[RemoteDBID_Master] - begins */

/* TableDDL - [BUSDTA].[RemoteDBID_Master] - Start */
IF OBJECT_ID('[BUSDTA].[RemoteDBID_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RemoteDBID_Master]
	(
	  [RemoteDBID] INTEGER NOT NULL
	, [AppUserId] INTEGER NULL
	, [DeviceID] VARCHAR(30) NULL
	, [RouteID] VARCHAR(30) NULL
	, [EnvironmentMasterID] INTEGER NULL
	, [ActiveYN] CHAR(1) NOT NULL DEFAULT('N')
	, [CreatedDateTime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([RemoteDBID] ASC)
	)
END
/* TableDDL - [BUSDTA].[RemoteDBID_Master] - End */

