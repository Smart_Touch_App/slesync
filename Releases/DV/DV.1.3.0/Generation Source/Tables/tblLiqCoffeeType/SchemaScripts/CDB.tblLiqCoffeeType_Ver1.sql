/* [dbo].[tblLiqCoffeeType] - begins */

/* TableDDL - [dbo].[tblLiqCoffeeType] - Start */
IF OBJECT_ID('[dbo].[tblLiqCoffeeType]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblLiqCoffeeType]
	(
	  [LiqCoffeeTypeID] INT NOT NULL IDENTITY(1,1)
	, [LiqCoffeeType] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLLIQCOFFEETYPE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLLIQCOFFEETYPE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLLIQCOFFEETYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblLiqCoffeeType] PRIMARY KEY ([LiqCoffeeTypeID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblLiqCoffeeType] - End */

/* SHADOW TABLE FOR [dbo].[tblLiqCoffeeType] - Start */
IF OBJECT_ID('[dbo].[tblLiqCoffeeType_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblLiqCoffeeType_del]
	(
	  [LiqCoffeeTypeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([LiqCoffeeTypeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblLiqCoffeeType] - End */
/* TRIGGERS FOR tblLiqCoffeeType - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblLiqCoffeeType_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblLiqCoffeeType_ins
	ON dbo.tblLiqCoffeeType AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblLiqCoffeeType_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblLiqCoffeeType_del.LiqCoffeeTypeID= inserted.LiqCoffeeTypeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblLiqCoffeeType_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblLiqCoffeeType_upd
	ON dbo.tblLiqCoffeeType AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblLiqCoffeeType
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblLiqCoffeeType.LiqCoffeeTypeID= inserted.LiqCoffeeTypeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblLiqCoffeeType_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblLiqCoffeeType_dlt
	ON dbo.tblLiqCoffeeType AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblLiqCoffeeType_del (LiqCoffeeTypeID, last_modified )
	SELECT deleted.LiqCoffeeTypeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblLiqCoffeeType - END */

