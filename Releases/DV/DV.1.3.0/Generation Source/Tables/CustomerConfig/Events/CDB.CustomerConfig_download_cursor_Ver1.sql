
SELECT "BUSDTA"."CustomerConfig"."CustomerID"
,"BUSDTA"."CustomerConfig"."CustomerReference1"
,"BUSDTA"."CustomerConfig"."CustomerReference2"
,"BUSDTA"."CustomerConfig"."CreatedBy"
,"BUSDTA"."CustomerConfig"."CreatedDatetime"
,"BUSDTA"."CustomerConfig"."UpdatedBy"
,"BUSDTA"."CustomerConfig"."UpdatedDatetime"
FROM "BUSDTA"."CustomerConfig"
WHERE "BUSDTA"."CustomerConfig"."last_modified">= {ml s.last_table_download}