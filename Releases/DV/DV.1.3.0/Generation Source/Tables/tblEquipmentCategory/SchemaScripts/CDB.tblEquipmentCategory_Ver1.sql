/* [dbo].[tblEquipmentCategory] - begins */

/* TableDDL - [dbo].[tblEquipmentCategory] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentCategory]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentCategory]
	(
	  [EquipmentCategoryID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentCategory] VARCHAR(500) NULL
	, [EquipmentTypeID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentCategory] PRIMARY KEY ([EquipmentCategoryID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblEquipmentCategory] - End */

/* SHADOW TABLE FOR [dbo].[tblEquipmentCategory] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentCategory_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentCategory_del]
	(
	  [EquipmentCategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentCategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblEquipmentCategory] - End */
/* TRIGGERS FOR tblEquipmentCategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblEquipmentCategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblEquipmentCategory_ins
	ON dbo.tblEquipmentCategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblEquipmentCategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblEquipmentCategory_del.EquipmentCategoryID= inserted.EquipmentCategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblEquipmentCategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipmentCategory_upd
	ON dbo.tblEquipmentCategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblEquipmentCategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblEquipmentCategory.EquipmentCategoryID= inserted.EquipmentCategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblEquipmentCategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipmentCategory_dlt
	ON dbo.tblEquipmentCategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblEquipmentCategory_del (EquipmentCategoryID, last_modified )
	SELECT deleted.EquipmentCategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentCategory - END */
