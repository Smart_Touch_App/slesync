 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PreTrip_Inspection_Header"
SET "TemplateId" = {ml r."TemplateId"}, "PreTripDateTime" = {ml r."PreTripDateTime"}, "UserName" = {ml r."UserName"}, "StatusId" = {ml r."StatusId"}, "VehicleMake" = {ml r."VehicleMake"}, "VehicleNumber" = {ml r."VehicleNumber"}, "OdoMmeterReading" = {ml r."OdoMmeterReading"}, "Comment" = {ml r."Comment"}, "VerSignature" = {ml r."VerSignature"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "PreTripInspectionHeaderId" = {ml r."PreTripInspectionHeaderId"} AND "RouteId" = {ml r."RouteId"}
 
