

IF OBJECT_ID('[dbo].[Order_Templates]') IS NULL
BEGIN
	CREATE TABLE [dbo].[Order_Templates]	(	  [TemplateType] NVARCHAR (10) NOT NULL	, [ShipToID] NUMERIC(8,0) NOT NULL	, [TemplateDetailID] NUMERIC(12,0) NOT NULL	, [DisplaySeq] NUMERIC(5,0) NULL	, [ItemID] NUMERIC(8,0) NULL	, [LongItem] NVARCHAR (25) NULL	, [TemplateQuantity] NUMERIC(8,0) NULL	, [TemplateUoM] NVARCHAR (2) NULL	, [last_modified] DATETIME NOT NULL DEFAULT(getdate())	, PRIMARY KEY ([TemplateType] ASC, [ShipToID] ASC, [TemplateDetailID] ASC)	)
END
