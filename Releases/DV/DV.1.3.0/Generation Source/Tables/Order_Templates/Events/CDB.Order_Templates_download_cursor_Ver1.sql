
SELECT "dbo"."Order_Templates"."TemplateType"
,"dbo"."Order_Templates"."ShipToID"
,"dbo"."Order_Templates"."TemplateDetailID"
,"dbo"."Order_Templates"."DisplaySeq"
,"dbo"."Order_Templates"."ItemID"
,"dbo"."Order_Templates"."LongItem"
,"dbo"."Order_Templates"."TemplateQuantity"
,"dbo"."Order_Templates"."TemplateUoM"
FROM "dbo"."Order_Templates", BUSDTA.F03012, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE "dbo"."Order_Templates"."last_modified">= {ml s.last_table_download} 
or rm.last_modified >= {ml s.last_table_download}   or crm.last_modified >= {ml s.last_table_download}) 
and  TemplateType=AIORTP and ShipToID=AIAN8 and AIAN8=crm.RelatedAddressBookNumber AND 
crm.BranchNumber = rm.BranchNumber   AND RouteMasterID = (select RouteMasterID from busdta.Route_Master 
where RouteName = {ml s.username})