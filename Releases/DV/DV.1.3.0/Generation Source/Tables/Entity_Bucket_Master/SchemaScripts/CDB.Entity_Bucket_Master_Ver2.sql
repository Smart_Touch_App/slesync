
/* [BUSDTA].[Entity_Bucket_Master] - begins */

/* TableDDL - [BUSDTA].[Entity_Bucket_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Bucket_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Bucket_Master]
	(
	  [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [Entity] NCHAR(50) NOT NULL
	, [BucketSize] DECIMAL(4,0) NULL
	, [NoOfBuckets] DECIMAL(4,0) NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ENTITY_BUCKET_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Entity_Bucket_Master] PRIMARY KEY ([EntityBucketMasterId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Entity_Bucket_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Entity_Bucket_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Bucket_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Bucket_Master_del]
	(
	  [EntityBucketMasterId] DECIMAL(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EntityBucketMasterId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Entity_Bucket_Master] - End */
/* TRIGGERS FOR Entity_Bucket_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Entity_Bucket_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Entity_Bucket_Master_ins
	ON BUSDTA.Entity_Bucket_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Entity_Bucket_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Entity_Bucket_Master_del.EntityBucketMasterId= inserted.EntityBucketMasterId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Entity_Bucket_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Bucket_Master_upd
	ON BUSDTA.Entity_Bucket_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Entity_Bucket_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Entity_Bucket_Master.EntityBucketMasterId= inserted.EntityBucketMasterId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Entity_Bucket_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Bucket_Master_dlt
	ON BUSDTA.Entity_Bucket_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Entity_Bucket_Master_del (EntityBucketMasterId, last_modified )
	SELECT deleted.EntityBucketMasterId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Entity_Bucket_Master - END */

