
/* *** [BUSDTA].[tblSyncDetails] - Start ***/
IF OBJECT_ID('[BUSDTA].[tblSyncDetails]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[tblSyncDetails]
(
	[ObjectID] [int] NOT NULL IDENTITY(1, 1),
	[Objectname] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
	[LastSyncVersion] [bigint] NULL,
	[TruncTimeAtCDB] [datetime] NULL,
	[UpdatedDateTime] [datetime] NULL DEFAULT (getdate())
)
END
/* TableDDL - [BUSDTA].[tblSyncDetails] - End */
GO
IF OBJECT_ID('BUSDTA.tblsyncdetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER [BUSDTA].[tblsyncdetails_upd]
	ON [BUSDTA].[tblSyncDetails] AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblsyncdetails
	SET UpdatedDateTime = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblsyncdetails.ObjectID= inserted.ObjectID);
END
GO