

/* TableDDL - [BUSDTA].[tblApplicationGroup] - Start */
IF OBJECT_ID('[BUSDTA].[tblApplicationGroup]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblApplicationGroup]
	(
	  [ApplicationGroupID] INT NOT NULL IDENTITY(1,1)
	, [ApplicationGroup] VARCHAR(128) NULL
	, [SortOrder] INT NULL
	, [IsMandatorySync] BIT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLAPPLICATIONGROUP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblApplicationGroup] PRIMARY KEY ([ApplicationGroupID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblApplicationGroup] - End */
GO
