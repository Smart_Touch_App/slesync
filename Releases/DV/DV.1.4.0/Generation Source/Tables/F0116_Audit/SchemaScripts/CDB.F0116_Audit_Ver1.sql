
/* TableDDL - [BUSDTA].[F0116_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0116_Audit]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0116_Audit]	(	  [TransID] NUMERIC(8,0) 	, [TransType] VARCHAR(50) NULL	, [ALAN8] NUMERIC(8,0) NOT NULL	, [ALEFTB] NUMERIC(18,0) NOT NULL	, [ALEFTF] NCHAR(1) NULL	, [ALADD1] NCHAR(40) NULL	, [ALADD2] NCHAR(40) NULL	, [ALADD3] NCHAR(40) NULL	, [ALADD4] NCHAR(40) NULL	, [ALADDZ] NCHAR(12) NULL	, [ALCTY1] NCHAR(25) NULL	, [ALCOUN] NCHAR(25) NULL	, [ALADDS] NCHAR(3) NULL	, [WSValidation] VARCHAR(50) NULL	, [Status] VARCHAR(50) NULL	, [RejReason] VARCHAR(50) NULL	, [SyncID] INT NULL	, [UpdatedDateTime] DATETIME NOT NULL CONSTRAINT DF_F0116_AUDIT_UpdatedDateTime DEFAULT(getdate())	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0116_AUDIT_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F0116_Audit] PRIMARY KEY ([ALAN8] ASC, [ALEFTB] ASC,[TransID] ASC )	)END
/* TableDDL - [BUSDTA].[F0116_Audit] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0116_Audit] - Start */
IF OBJECT_ID('[BUSDTA].[F0116_Audit_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[F0116_Audit_del]	(	  [TransID] NUMERIC(8,0) 		, [ALAN8] NUMERIC(8,0)	, [ALEFTB] NUMERIC(18,0)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY (TransID ASC,[ALAN8] ASC, [ALEFTB] ASC)	)END
/* SHADOW TABLE FOR [BUSDTA].[F0116_Audit] - End */
/* TRIGGERS FOR F0116_Audit - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0116_Audit_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0116_Audit_ins
	ON BUSDTA.F0116_Audit AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0116_Audit_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0116_Audit_del.TransID= inserted.TransID AND BUSDTA.F0116_Audit_del.ALAN8= inserted.ALAN8 AND BUSDTA.F0116_Audit_del.ALEFTB= inserted.ALEFTB
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0116_Audit_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_Audit_upd
	ON BUSDTA.F0116_Audit AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0116_Audit
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0116_Audit.TransID= inserted.TransID AND BUSDTA.F0116_Audit.ALAN8= inserted.ALAN8 AND BUSDTA.F0116_Audit.ALEFTB= inserted.ALEFTB');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0116_Audit_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_Audit_dlt
	ON BUSDTA.F0116_Audit AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0116_Audit_del (TransID,ALAN8, ALEFTB, last_modified )
	SELECT deleted.TransID, deleted.ALAN8, deleted.ALEFTB, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0116_Audit - END */
