

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0116_Audit"
SET "TransType" = {ml r."TransType"}, "ALEFTF" = {ml r."ALEFTF"}, "ALADD1" = {ml r."ALADD1"}, "ALADD2" = {ml r."ALADD2"}, "ALADD3" = {ml r."ALADD3"}, "ALADD4" = {ml r."ALADD4"}, "ALADDZ" = {ml r."ALADDZ"}, "ALCTY1" = {ml r."ALCTY1"}, "ALCOUN" = {ml r."ALCOUN"}, "ALADDS" = {ml r."ALADDS"}, "WSValidation" = {ml r."WSValidation"}, "Status" = {ml r."Status"}, "RejReason" = {ml r."RejReason"}, "SyncID" = {ml r."SyncID"},"UpdatedDateTime" = {ml r."UpdatedDateTime"}
WHERE "TransID" = {ml r."TransID"} AND "ALAN8" = {ml r."ALAN8"} AND "ALEFTB" = {ml r."ALEFTB"}
