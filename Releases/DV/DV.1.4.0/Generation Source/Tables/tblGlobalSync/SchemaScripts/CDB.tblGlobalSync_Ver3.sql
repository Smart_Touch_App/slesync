

/* TableDDL - [BUSDTA].[tblGlobalSync] - Start */
IF OBJECT_ID('[BUSDTA].[tblGlobalSync]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblGlobalSync]
	(
	  [GlobalSyncID] INT NOT NULL IDENTITY(1,1)
	, [VersionNum] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLGLOBALSYNC_CreatedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLGLOBALSYNC_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGlobalSync] PRIMARY KEY ([GlobalSyncID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblGlobalSync] - End */
