
/* [BUSDTA].[Entity_Bucket_Master] - begins */

/* TableDDL - [BUSDTA].[Entity_Bucket_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Bucket_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Bucket_Master]
	(
	  [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [Entity] NVARCHAR (50) NOT NULL
	, [BucketSize] DECIMAL(4,0) NULL
	, [NoOfBuckets] DECIMAL(4,0) NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL	
	, PRIMARY KEY ([EntityBucketMasterId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Entity_Bucket_Master] - End */

