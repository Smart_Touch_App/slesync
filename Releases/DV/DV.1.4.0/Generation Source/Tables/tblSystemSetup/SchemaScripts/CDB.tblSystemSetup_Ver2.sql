/* [BUSDTA].[tblSystemSetup] - begins */

/* TableDDL - [BUSDTA].[tblSystemSetup] - Start */
IF OBJECT_ID('[BUSDTA].[tblSystemSetup]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblSystemSetup]
	(
	  [SystemSetupID] INT NOT NULL IDENTITY(1,1)
	, [KeyName] VARCHAR(128) NOT NULL
	, [KeyValue] VARCHAR(MAX) NOT NULL
	, [Description] VARCHAR(MAX) NULL
	, [ActiveYN] BIT NULL CONSTRAINT DF_TBLSYSTEMSETUP_ActiveYN DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLSYSTEMSETUP_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYSTEMSETUP_EditedOn DEFAULT(getdate())
	, CONSTRAINT [PK_tblSystemSetup] PRIMARY KEY ([SystemSetupID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblSystemSetup] - End */
GO

IF NOT EXISTS(SELECT 1 FROM tblSystemSetup WHERE KeyName='SystemDBConnection')
BEGIN
	INSERT [BUSDTA].[tblSystemSetup] ([KeyName], [KeyValue], [Description], [CreatedBy], [EditedBy]) 
	VALUES (N'SystemDBConnection', N'Data Source=HP-1\SQLEXPRESS2012;Initial Catalog=EnvironmentDB;User ID=dba;Password=sql', N'Conneciton for SystemDB', N'sjanardh', N'sjanardh')
END
GO

IF NOT EXISTS(SELECT 1 FROM tblSystemSetup WHERE KeyName='LogFileLocaiton')
BEGIN
	INSERT [BUSDTA].[tblSystemSetup] ([KeyName], [KeyValue], [Description], [CreatedBy], [EditedBy]) 
	VALUES (N'LogFileLocaiton', N'E:\bitbucket\slesync\Releases\Development\D.3.2.1', N'Location for Log File', N'sjanardh', N'sjanardh')
END
GO