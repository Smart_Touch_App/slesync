 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PickOrder_Exception"
SET "Item_Number" = {ml r."Item_Number"}, "Exception_Qty" = {ml r."Exception_Qty"}, "UOM" = {ml r."UOM"}, "Exception_Reason" = {ml r."Exception_Reason"}, "ManualPickReasonCode" = {ml r."ManualPickReasonCode"}, "ManuallyPickCount" = {ml r."ManuallyPickCount"}
WHERE "PickOrder_Exception_Id" = {ml r."PickOrder_Exception_Id"} AND "Order_Id" = {ml r."Order_Id"} AND "RouteId" = {ml r."RouteId"}
