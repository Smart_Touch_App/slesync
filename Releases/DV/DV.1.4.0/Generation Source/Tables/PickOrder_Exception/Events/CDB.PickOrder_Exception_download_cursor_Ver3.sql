
 
/*New column changes CustomerID to CustShipToID */
SELECT "BUSDTA"."PickOrder_Exception"."PickOrder_Exception_Id"
,"BUSDTA"."PickOrder_Exception"."Order_Id"
,"BUSDTA"."PickOrder_Exception"."RouteId"
,"BUSDTA"."PickOrder_Exception"."Item_Number"
,"BUSDTA"."PickOrder_Exception"."Exception_Qty"
,"BUSDTA"."PickOrder_Exception"."UOM"
,"BUSDTA"."PickOrder_Exception"."Exception_Reason"
,"BUSDTA"."PickOrder_Exception"."ManualPickReasonCode"
,"BUSDTA"."PickOrder_Exception"."ManuallyPickCount"

FROM  BUSDTA.Order_Header, BUSDTA.PickOrder_Exception, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE "BUSDTA"."PickOrder_Exception"."last_modified" >= {ml s.last_table_download}
AND CustShipToID = crm.RelatedAddressBookNumber and BUSDTA.Order_Header.OrderID = busdta.PickOrder_Exception.Order_ID AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
