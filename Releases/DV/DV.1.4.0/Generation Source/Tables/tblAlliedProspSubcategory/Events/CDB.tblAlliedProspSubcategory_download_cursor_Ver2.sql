 
SELECT "BUSDTA"."tblAlliedProspSubcategory"."SubCategoryID"
,"BUSDTA"."tblAlliedProspSubcategory"."SubCategory"
,"BUSDTA"."tblAlliedProspSubcategory"."CategoryID"
,"BUSDTA"."tblAlliedProspSubcategory"."CreatedBy"
,"BUSDTA"."tblAlliedProspSubcategory"."CreatedDatetime"
,"BUSDTA"."tblAlliedProspSubcategory"."UpdatedBy"
,"BUSDTA"."tblAlliedProspSubcategory"."UpdatedDatetime"

FROM "BUSDTA"."tblAlliedProspSubcategory"
WHERE "BUSDTA"."tblAlliedProspSubcategory"."last_modified">= {ml s.last_table_download}
 
