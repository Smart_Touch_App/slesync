
 SELECT "BUSDTA"."Cycle_Count_Detail"."CycleCountID"  ,"BUSDTA"."Cycle_Count_Detail"."CycleCountDetailID"  ,
 "BUSDTA"."Cycle_Count_Detail"."ReCountNumber"  ,"BUSDTA"."Cycle_Count_Detail"."ItemId"  ,
 "BUSDTA"."Cycle_Count_Detail"."CountedQty"  ,"BUSDTA"."Cycle_Count_Detail"."ItemStatus"  ,
 "BUSDTA"."Cycle_Count_Detail"."OnHandQty"  ,"BUSDTA"."Cycle_Count_Detail"."HeldQty"  ,
 "BUSDTA"."Cycle_Count_Detail"."CountAccepted"  ,"BUSDTA"."Cycle_Count_Detail"."CreatedBy"  ,
 "BUSDTA"."Cycle_Count_Detail"."CreatedDatetime"  ,"BUSDTA"."Cycle_Count_Detail"."UpdatedBy"  ,
 "BUSDTA"."Cycle_Count_Detail"."UpdatedDatetime"    FROM "BUSDTA"."Cycle_Count_Detail", "BUSDTA"."Cycle_Count_Header"  
 WHERE ("BUSDTA"."Cycle_Count_Detail"."last_modified">= {ml s.last_table_download} or 
 "BUSDTA"."Cycle_Count_Header"."last_modified">= {ml s.last_table_download})  AND 
  "BUSDTA"."Cycle_Count_Detail"."CycleCountID" = "BUSDTA"."Cycle_Count_Header"."CycleCountID"  AND 
  "BUSDTA"."Cycle_Count_Header"."RouteId"  = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     