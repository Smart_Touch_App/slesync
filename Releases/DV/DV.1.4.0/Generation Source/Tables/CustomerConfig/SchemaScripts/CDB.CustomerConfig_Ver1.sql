/* [BUSDTA].[CustomerConfig] - begins */

/* TableDDL - [BUSDTA].[CustomerConfig] - Start */
IF OBJECT_ID('[BUSDTA].[CustomerConfig]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[CustomerConfig]	(	  [CustomerID] NUMERIC(8,0) NOT NULL	, [CustomerReference1] NVARCHAR(25) NULL	, [CustomerReference2] NVARCHAR(25) NULL	, [CreatedBy] NUMERIC(8,0) NULL	, [CreatedDatetime] DATETIME NULL	, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL	, CONSTRAINT [PK_CustomerConfig] PRIMARY KEY ([CustomerID] ASC)	)END
/* TableDDL - [BUSDTA].[CustomerConfig] - End */

/* SHADOW TABLE FOR [BUSDTA].[CustomerConfig] - Start */
IF OBJECT_ID('[BUSDTA].[CustomerConfig_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[CustomerConfig_del]	(	  [CustomerID] NUMERIC(8,0)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([CustomerID] ASC)	)END
/* SHADOW TABLE FOR [BUSDTA].[CustomerConfig] - End */
/* TRIGGERS FOR CustomerConfig - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.CustomerConfig_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.CustomerConfig_ins
	ON BUSDTA.CustomerConfig AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.CustomerConfig_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.CustomerConfig_del.CustomerID= inserted.CustomerID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.CustomerConfig_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CustomerConfig_upd
	ON BUSDTA.CustomerConfig AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.CustomerConfig
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.CustomerConfig.CustomerID= inserted.CustomerID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.CustomerConfig_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CustomerConfig_dlt
	ON BUSDTA.CustomerConfig AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.CustomerConfig_del (CustomerID, last_modified )
	SELECT deleted.CustomerID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR CustomerConfig - END */
