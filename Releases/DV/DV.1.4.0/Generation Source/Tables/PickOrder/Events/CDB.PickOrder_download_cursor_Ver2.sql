
 

SELECT "DBO"."PickOrder"."PickOrder_Id"
,"DBO"."PickOrder"."Order_ID"
,"DBO"."PickOrder"."RouteId"
,"DBO"."PickOrder"."Item_Number"
,"DBO"."PickOrder"."Order_Qty"
,"DBO"."PickOrder"."Order_UOM"
,"DBO"."PickOrder"."Picked_Qty_Primary_UOM"
,"DBO"."PickOrder"."Primary_UOM"
,"DBO"."PickOrder"."Order_Qty_Primary_UOM"
,"DBO"."PickOrder"."On_Hand_Qty_Primary"
,"DBO"."PickOrder"."Last_Scan_Mode"
,"DBO"."PickOrder"."Item_Scan_Sequence"
,"DBO"."PickOrder"."Picked_By"
,"DBO"."PickOrder"."IsOnHold"
,"DBO"."PickOrder"."Reason_Code_Id"
,"DBO"."PickOrder"."ManuallyPickCount"

FROM DBO.Order_Header, DBO.PickOrder, DBO.Route_Master rm, DBO.Customer_Route_Map crm
WHERE "DBO"."PickOrder"."last_modified" >= {ml s.last_table_download}
AND CustShipToID = crm.RelatedAddressBookNumber and DBO.Order_Header.OrderID = DBO.PickOrder.Order_ID AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from DBO.Route_Master where RouteName = {ml s.username} )

 
