/* [BUSDTA].[Prospect_Note] - begins */

/* TableDDL - [BUSDTA].[Prospect_Note] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Note]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Note]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [ProspectNoteId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ProspectNoteType] NUMERIC(3,0) NOT NULL
	, [ProspectNoteDetail] NCHAR(1000) NULL
	, [IsDefault] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_NOTE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Note] PRIMARY KEY ([ProspectId] ASC, [ProspectNoteId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Note] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Note] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Note_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Note_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, [ProspectNoteId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC, [ProspectNoteId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Note] - End */
/* TRIGGERS FOR Prospect_Note - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Note_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Note_ins
	ON BUSDTA.Prospect_Note AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Note_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Note_del.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Note_del.ProspectNoteId= inserted.ProspectNoteId AND BUSDTA.Prospect_Note_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Note_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Note_upd
	ON BUSDTA.Prospect_Note AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Note
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Note.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Note.ProspectNoteId= inserted.ProspectNoteId AND BUSDTA.Prospect_Note.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Note_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Note_dlt
	ON BUSDTA.Prospect_Note AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Note_del (ProspectId, ProspectNoteId, RouteId, last_modified )
	SELECT deleted.ProspectId, deleted.ProspectNoteId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Note - END */
