
/* [BUSDTA].[M080111] - begins */

/* TableDDL - [BUSDTA].[M080111] - Start */
IF OBJECT_ID('[BUSDTA].[M080111]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M080111]
	(
	  [CTID] NUMERIC(8,0) NOT NULL
	, [CTTYP] NCHAR(10) NULL
	, [CTCD] NCHAR(5) NOT NULL
	, [CTDSC1] NCHAR(50) NULL
	, [CTTXA1] NCHAR(10) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M080111_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M080111] PRIMARY KEY ([CTID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M080111] - End */

/* SHADOW TABLE FOR [BUSDTA].[M080111] - Start */
IF OBJECT_ID('[BUSDTA].[M080111_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M080111_del]
	(
	  [CTID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CTID] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M080111] - End */
/* TRIGGERS FOR M080111 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M080111_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M080111_ins
	ON BUSDTA.M080111 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M080111_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M080111_del.CTID= inserted.CTID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M080111_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M080111_upd
	ON BUSDTA.M080111 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M080111
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M080111.CTID= inserted.CTID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M080111_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M080111_dlt
	ON BUSDTA.M080111 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M080111_del (CTID, last_modified )
	SELECT deleted.CTID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M080111 - END */

