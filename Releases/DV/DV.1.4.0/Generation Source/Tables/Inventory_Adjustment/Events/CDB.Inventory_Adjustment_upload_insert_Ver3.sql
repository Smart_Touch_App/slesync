

/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Inventory_Adjustment"
("InventoryAdjustmentId", "ItemId", "ItemNumber", "RouteId", "TransactionQty", "TransactionQtyUM", "TransactionQtyPrimaryUM", "ReasonCode", "IsApproved", "IsApplied", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime", "AdjustmentStatus", "Source")
VALUES({ml r."InventoryAdjustmentId"}, {ml r."ItemId"}, {ml r."ItemNumber"}, {ml r."RouteId"}, {ml r."TransactionQty"}, {ml r."TransactionQtyUM"}, {ml r."TransactionQtyPrimaryUM"}, {ml r."ReasonCode"}, {ml r."IsApproved"}, {ml r."IsApplied"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"}, {ml r."AdjustmentStatus"}, {ml r."Source"})