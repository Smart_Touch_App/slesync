DROP PUBLICATION IF EXISTS pub_immediate;
GO
/** Create publication 'pub_immediate'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_immediate" 

(
TABLE "DBO"."CustomerConfig" ,
	TABLE "DBO"."F0116" ,
	TABLE "DBO"."F03012" ,
	TABLE "DBO"."F4015" ,
	TABLE "DBO"."M0111" ,
	TABLE "DBO"."M0112" ,
	TABLE "DBO"."M03042" ,
	TABLE "DBO"."M04012" ,
	TABLE "DBO"."M40111" ,
	TABLE "DBO"."M40116" ,
	TABLE "DBO"."M4016" ,
	TABLE "DBO"."Order_Detail" ,
	TABLE "DBO"."Order_Detail_BETA" ,
	TABLE "DBO"."Order_Header" ,
	TABLE "DBO"."Order_Header_BETA" ,
	TABLE "DBO"."Order_PriceAdj" ,
	TABLE "DBO"."Order_PriceAdj_BETA" ,
	TABLE "DBO"."PickOrder" ,
	TABLE "DBO"."PickOrder_Exception"    

)
GO

/** Create the user '{ml_userid}'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = '{ml_userid}') THEN
	CREATE SYNCHRONIZATION USER "{ml_userid}";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = 'subs_immediate') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "subs_immediate";
END IF
GO

/** Create subscription 'subs_immediate' to 'pub_immediate' for '{ml_userid}'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION "subs_immediate" TO "pub_immediate" FOR "{ml_userid}" 
TYPE tcpip ADDRESS 'host=172.30.6.170;port=2439'
	OPTION lt='OFF'
	SCRIPT VERSION 'SLE_SyncScript_DV.1.1.0'
GO


