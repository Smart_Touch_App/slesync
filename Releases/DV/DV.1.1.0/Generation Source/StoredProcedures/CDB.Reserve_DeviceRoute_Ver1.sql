CREATE PROCEDURE [dbo].[Reserve_DeviceRoute]
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30),
	@DBID INT
)
AS
BEGIN
	--IF NOT EXISTS(SELECT 1 FROM BUSDTA.Route_Device_Map WHERE Route_Id=@RouteID AND Device_Id=@DeviceId)
	--BEGIN
	--	INSERT INTO BUSDTA.Route_Device_Map (Route_Id, Device_Id, Active, Remote_Id)
	--	VALUES (@RouteID, @DeviceId, 0, CONCAT(@DeviceId, '_', @RouteID))
	--END
	IF NOT EXISTS(SELECT 1 FROM ml_user WHERE name=CAST(@DBID AS VARCHAR))
	BEGIN
		INSERT INTO ml_user (name) VALUES (@DBID)
	END
	UPDATE BUSDTA.Route_Master SET RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='PENDING REGISTRATION')
	WHERE RouteName=@RouteID AND RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='UNREGISTERED')
END
GO