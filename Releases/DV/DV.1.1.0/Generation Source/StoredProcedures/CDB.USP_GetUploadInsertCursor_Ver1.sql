
CREATE PROCEDURE [dbo].[USP_GetUploadInsertCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Upload Insert Cursor */
DECLARE @ui NVARCHAR(MAX)
SELECT @ui = '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '"'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

DECLARE @uiCols NVARCHAR(MAX)
SELECT @uiCols =COALESCE(@uiCols + ', ','') + '"' + A.COLUMN_NAME + '"'
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME

DECLARE @uiColML NVARCHAR(MAX)
SELECT @uiColML =COALESCE(@uiColML + ', {ml r.','') + '"' + A.COLUMN_NAME + '"}'
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME
PRINT '/* Insert the row into the consolidated database. */'
PRINT 'INSERT INTO ' + @ui
PRINT '(' + @uiCols +')'
PRINT 'VALUES({ml r.' + @uiColML +')'
/* Upload Insert Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO
