DROP PUBLICATION IF EXISTS pub_immediate;
GO
/** Create publication 'pub_immediate'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_immediate" 

(
TABLE "DBO"."CustomerConfig" ,
	TABLE "DBO"."F0116" ,
	TABLE "DBO"."F03012" ,
	TABLE "DBO"."F4015" ,
	TABLE "DBO"."M0111" ,
	TABLE "DBO"."M0112" ,
	TABLE "DBO"."M03042" ,
	TABLE "DBO"."M04012" ,
	TABLE "DBO"."M40111" ,
	TABLE "DBO"."M40116" ,
	TABLE "DBO"."M4016" ,
	TABLE "DBO"."Order_Detail" ,
	TABLE "DBO"."Order_Detail_BETA" ,
	TABLE "DBO"."Order_Header" ,
	TABLE "DBO"."Order_Header_BETA" ,
	TABLE "DBO"."Order_PriceAdj" ,
	TABLE "DBO"."Order_PriceAdj_BETA" ,
	TABLE "DBO"."PickOrder" ,
	TABLE "DBO"."PickOrder_Exception"    

)
GO
