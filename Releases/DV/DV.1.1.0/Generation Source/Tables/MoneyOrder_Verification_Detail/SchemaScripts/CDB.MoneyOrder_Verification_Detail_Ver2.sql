/* [BUSDTA].[MoneyOrder_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrder_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrder_Verification_Detail]
	(
	  [MoneyOrderVerificationDetailId] NUMERIC(18,0) NOT NULL
	, [SettlementDetailId] NUMERIC(18,0) NOT NULL
	, [MoneyOrderId] NUMERIC(18,0) NOT NULL
	, [RouteId] NUMERIC(18,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_MONEYORDER_VERIFICATION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_MoneyOrder_Verification_Detail] PRIMARY KEY ([MoneyOrderVerificationDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[MoneyOrder_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrder_Verification_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrder_Verification_Detail_del]
	(
	  [MoneyOrderVerificationDetailId] NUMERIC(18,0)
	, [RouteId] NUMERIC(18,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MoneyOrderVerificationDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[MoneyOrder_Verification_Detail] - End */
/* TRIGGERS FOR MoneyOrder_Verification_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.MoneyOrder_Verification_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.MoneyOrder_Verification_Detail_ins
	ON BUSDTA.MoneyOrder_Verification_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.MoneyOrder_Verification_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.MoneyOrder_Verification_Detail_del.MoneyOrderVerificationDetailId= inserted.MoneyOrderVerificationDetailId AND BUSDTA.MoneyOrder_Verification_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.MoneyOrder_Verification_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrder_Verification_Detail_upd
	ON BUSDTA.MoneyOrder_Verification_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.MoneyOrder_Verification_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.MoneyOrder_Verification_Detail.MoneyOrderVerificationDetailId= inserted.MoneyOrderVerificationDetailId AND BUSDTA.MoneyOrder_Verification_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.MoneyOrder_Verification_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrder_Verification_Detail_dlt
	ON BUSDTA.MoneyOrder_Verification_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.MoneyOrder_Verification_Detail_del (MoneyOrderVerificationDetailId, RouteId, last_modified )
	SELECT deleted.MoneyOrderVerificationDetailId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR MoneyOrder_Verification_Detail - END */

