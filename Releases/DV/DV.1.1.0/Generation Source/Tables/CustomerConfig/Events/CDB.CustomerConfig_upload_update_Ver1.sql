 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."CustomerConfig"
SET "CustomerReference1" = {ml r."CustomerReference1"}, "CustomerReference2" = {ml r."CustomerReference2"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CustomerID" = {ml r."CustomerID"}
