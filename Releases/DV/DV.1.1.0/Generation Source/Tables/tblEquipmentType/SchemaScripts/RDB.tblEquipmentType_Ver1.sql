/* [dbo].[tblEquipmentType] - begins */

/* TableDDL - [dbo].[tblEquipmentType] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentType]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentType]
	(
	  [EquipmentTypeID] INTEGER NOT NULL 
	, [EquipmentType] VARCHAR(200) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([EquipmentTypeID] ASC)
	)
END
/* TableDDL - [dbo].[tblEquipmentType] - End */
