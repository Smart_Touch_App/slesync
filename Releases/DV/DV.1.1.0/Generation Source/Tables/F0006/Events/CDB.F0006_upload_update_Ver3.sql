
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0006"
SET "MCSTYL" = {ml r."MCSTYL"}, "MCLDM" = {ml r."MCLDM"}, "MCCO" = {ml r."MCCO"}, "MCAN8" = {ml r."MCAN8"}, "MCDL01" = {ml r."MCDL01"}, "MCRP01" = {ml r."MCRP01"}, "MCRP02" = {ml r."MCRP02"}, "MCRP03" = {ml r."MCRP03"}, "MCRP04" = {ml r."MCRP04"}, "MCRP05" = {ml r."MCRP05"}, "MCRP06" = {ml r."MCRP06"}, "MCRP07" = {ml r."MCRP07"}, "MCRP08" = {ml r."MCRP08"}, "MCRP09" = {ml r."MCRP09"}, "MCRP10" = {ml r."MCRP10"}, "MCRP11" = {ml r."MCRP11"}, "MCRP12" = {ml r."MCRP12"}, "MCRP13" = {ml r."MCRP13"}, "MCRP14" = {ml r."MCRP14"}, "MCRP15" = {ml r."MCRP15"}, "MCRP16" = {ml r."MCRP16"}, "MCRP17" = {ml r."MCRP17"}, "MCRP18" = {ml r."MCRP18"}, "MCRP19" = {ml r."MCRP19"}, "MCRP20" = {ml r."MCRP20"}, "MCRP21" = {ml r."MCRP21"}, "MCRP22" = {ml r."MCRP22"}, "MCRP23" = {ml r."MCRP23"}, "MCRP24" = {ml r."MCRP24"}, "MCRP25" = {ml r."MCRP25"}, "MCRP26" = {ml r."MCRP26"}, "MCRP27" = {ml r."MCRP27"}, "MCRP28" = {ml r."MCRP28"}, "MCRP29" = {ml r."MCRP29"}, "MCRP30" = {ml r."MCRP30"}
WHERE "MCMCU" = {ml r."MCMCU"}
 

