
/* [BUSDTA].[Order_Header] - begins */

/* TableDDL - [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header](
	[OrderID] [numeric](8, 0) NOT NULL,
	[OrderTypeId] [numeric](3, 0) NULL,
	[OriginatingRouteID] [numeric](8, 0) NOT NULL,
	[Branch] [nchar](12) NULL,
	[CustShipToId] [numeric](8, 0) NOT NULL,
	[CustBillToId] [numeric](8, 0) NOT NULL,
	[CustParentId] [numeric](8, 0) NOT NULL,
	[OrderDate] [date] NULL,
	[TotalCoffeeAmt] [numeric](12, 4) NULL,
	[TotalAlliedAmt] [numeric](12, 4) NULL,
	[OrderTotalAmt] [numeric](12, 4) NULL,
	[SalesTaxAmt] [numeric](12, 4) NULL,
	[InvoiceTotalAmt] [numeric](12, 4) NULL,
	[EnergySurchargeAmt] [numeric](12, 4) NULL,
	[VarianceAmt] [numeric](12, 4) NULL,
	[SurchargeReasonCodeId] [numeric](3, 0) NULL,
	[OrderStateId] [numeric](3, 0) NULL,
	[OrderSignature] [varbinary](max) NULL,
	[ChargeOnAccountSignature] [varbinary](max) NULL,
	[JDEOrderCompany] [nchar](5) NULL,
	[JDEOrderNumber] [numeric](8, 0) NULL,
	[JDEOrderType] [nchar](2) NULL,
	[JDEInvoiceCompany] [nchar](5) NULL,
	[JDEInvoiceNumber] [numeric](8, 0) NULL,
	[JDEOInvoiceType] [nchar](2) NULL,
	[JDEZBatchNumber] [nchar](15) NULL,
	[JDEProcessID] [numeric](3, 0) NULL,
	[SettlementID] [numeric](8, 0) NULL,
	[PaymentTerms] [nchar](3) NULL,
	[CustomerReference1] [nchar](25) NULL,
	[CustomerReference2] [nchar](25) NULL,
	[AdjustmentSchedule] [nchar](8) NULL,
	[TaxArea] [nchar](10) NULL,
	[TaxExplanationCode] [nchar](1) NULL,
	[VoidReasonCodeId] [numeric](3, 0) NULL,
	[ChargeOnAccount] [nchar](1) NULL,
	[HoldCommitted] [nchar](1) NULL,
	[CreatedBy] [numeric](8, 0) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedBy] [numeric](8, 0) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[last_modified] [datetime] DEFAULT (getdate()),
 CONSTRAINT [PK_Order_Headers] PRIMARY KEY CLUSTERED ([OrderID] ASC))

END
GO
/* TableDDL - [BUSDTA].[Order_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header_del](
	[OrderID] [numeric](8, 0) NOT NULL,
	[last_modified] [datetime] DEFAULT (getdate()),
 CONSTRAINT [PK_Order_Header_del] PRIMARY KEY CLUSTERED ([OrderID] ASC)
) ON [PRIMARY]

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - End */
/* TRIGGERS FOR Order_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Header_ins
	ON BUSDTA.Order_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Header_del.OrderID= inserted.OrderID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_upd
	ON BUSDTA.Order_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Header.OrderID= inserted.OrderID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_dlt
	ON BUSDTA.Order_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Header_del (OrderID, last_modified )
	SELECT deleted.OrderID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Header - END */
