
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F56M0000"
SET "GFCXPJ" = {ml r."GFCXPJ"}, "GFDTEN" = {ml r."GFDTEN"}, "GFLAVJ" = {ml r."GFLAVJ"}, "GFOBJ" = {ml r."GFOBJ"}, "GFMCU" = {ml r."GFMCU"}, "GFSUB" = {ml r."GFSUB"}, "GFPST" = {ml r."GFPST"}, "GFEV01" = {ml r."GFEV01"}, "GFEV02" = {ml r."GFEV02"}, "GFEV03" = {ml r."GFEV03"}, "GFMATH01" = {ml r."GFMATH01"}, "GFMATH02" = {ml r."GFMATH02"}, "GFMATH03" = {ml r."GFMATH03"}, "GFCFSTR1" = {ml r."GFCFSTR1"}, "GFCFSTR2" = {ml r."GFCFSTR2"}, "GFGS1A" = {ml r."GFGS1A"}, "GFGS1B" = {ml r."GFGS1B"}, "GFGS2A" = {ml r."GFGS2A"}, "GFGS2B" = {ml r."GFGS2B"}
WHERE "GFSY" = {ml r."GFSY"}
 

