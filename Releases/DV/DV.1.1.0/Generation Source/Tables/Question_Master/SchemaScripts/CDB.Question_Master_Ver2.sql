/* [BUSDTA].[Question_Master] - begins */

/* TableDDL - [BUSDTA].[Question_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Master]
	(
	  [QuestionId] NUMERIC(8,0) NOT NULL
	, [QuestionTitle] NCHAR(100) NULL
	, [QuestionDescription] NCHAR(100) NULL
	, [IsMandatory] BIT NULL
	, [IsMultivalue] BIT NULL
	, [IsDescriptive] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_QUESTION_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Question_Master] PRIMARY KEY ([QuestionId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Question_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Question_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Master_del]
	(
	  [QuestionId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([QuestionId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Question_Master] - End */
/* TRIGGERS FOR Question_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Question_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Question_Master_ins
	ON BUSDTA.Question_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Question_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Question_Master_del.QuestionId= inserted.QuestionId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Question_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Master_upd
	ON BUSDTA.Question_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Question_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Question_Master.QuestionId= inserted.QuestionId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Question_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Question_Master_dlt
	ON BUSDTA.Question_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Question_Master_del (QuestionId, last_modified )
	SELECT deleted.QuestionId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Question_Master - END */
