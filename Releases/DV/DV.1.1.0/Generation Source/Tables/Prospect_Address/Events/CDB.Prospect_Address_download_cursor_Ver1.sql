

 
SELECT "BUSDTA"."Prospect_Address"."ProspectId"
,"BUSDTA"."Prospect_Address"."RouteId"
,"BUSDTA"."Prospect_Address"."AddressLine1"
,"BUSDTA"."Prospect_Address"."AddressLine2"
,"BUSDTA"."Prospect_Address"."AddressLine3"
,"BUSDTA"."Prospect_Address"."AddressLine4"
,"BUSDTA"."Prospect_Address"."CityName"
,"BUSDTA"."Prospect_Address"."StateName"
,"BUSDTA"."Prospect_Address"."ZipCode"
,"BUSDTA"."Prospect_Address"."CreatedBy"
,"BUSDTA"."Prospect_Address"."CreatedDatetime"
,"BUSDTA"."Prospect_Address"."UpdatedBy"
,"BUSDTA"."Prospect_Address"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Address"
WHERE "BUSDTA"."Prospect_Address"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Prospect_Address"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
