/* [BUSDTA].[Prospect_Quote_Detail] - begins */

/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Detail]
	(
	  [ProspectQuoteId] NUMERIC(8,0) NOT NULL
	, [ProspectQuoteDetailId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [QuoteQty] NUMERIC(4,0) NOT NULL
	, [SampleQty] NUMERIC(4,0) NOT NULL
	, [SampleUM] NVARCHAR (5) NULL
	, [PricingAmt] NUMERIC(8,4) NULL
	, [PricingUM] NVARCHAR (2) NULL
	, [TransactionAmt] NUMERIC(8,4) NULL
	, [TransactionUM] NVARCHAR (2) NULL
	, [OtherAmt] NUMERIC(8,4) NULL
	, [OtherUM] NVARCHAR (2) NULL
	, [CompetitorName] NVARCHAR(50) NOT NULL
	, [UnitPriceAmt] NUMERIC(8,4) NULL
	, [UnitPriceUM] NVARCHAR (2) NULL
	, [AvailableQty] NUMERIC(4,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ProspectQuoteId] ASC, [ProspectQuoteDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - End */
