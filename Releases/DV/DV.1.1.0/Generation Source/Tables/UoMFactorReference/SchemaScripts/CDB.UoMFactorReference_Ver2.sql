/* [BUSDTA].[UoMFactorReference] - begins */

/* TableDDL - [BUSDTA].[UoMFactorReference] - Start */
IF OBJECT_ID('[BUSDTA].[UoMFactorReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UoMFactorReference]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [FromUOM] NCHAR(2) NOT NULL
	, [ToUOM] NCHAR(2) NOT NULL
	, [ConversionFactor] NUMERIC(8,4) NULL
	, [GenerationDate] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_UOMFACTORREFERENCE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_UoMFactorReference] PRIMARY KEY ([ItemID] ASC, [FromUOM] ASC, [ToUOM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[UoMFactorReference] - End */

/* SHADOW TABLE FOR [BUSDTA].[UoMFactorReference] - Start */
IF OBJECT_ID('[BUSDTA].[UoMFactorReference_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UoMFactorReference_del]
	(
	  [ItemID] NUMERIC(8,0)
	, [FromUOM] NCHAR(2)
	, [ToUOM] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemID] ASC, [FromUOM] ASC, [ToUOM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[UoMFactorReference] - End */
/* TRIGGERS FOR UoMFactorReference - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.UoMFactorReference_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.UoMFactorReference_ins
	ON BUSDTA.UoMFactorReference AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.UoMFactorReference_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.UoMFactorReference_del.FromUOM= inserted.FromUOM AND BUSDTA.UoMFactorReference_del.ItemID= inserted.ItemID AND BUSDTA.UoMFactorReference_del.ToUOM= inserted.ToUOM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.UoMFactorReference_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UoMFactorReference_upd
	ON BUSDTA.UoMFactorReference AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.UoMFactorReference
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.UoMFactorReference.FromUOM= inserted.FromUOM AND BUSDTA.UoMFactorReference.ItemID= inserted.ItemID AND BUSDTA.UoMFactorReference.ToUOM= inserted.ToUOM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.UoMFactorReference_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UoMFactorReference_dlt
	ON BUSDTA.UoMFactorReference AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.UoMFactorReference_del (FromUOM, ItemID, ToUOM, last_modified )
	SELECT deleted.FromUOM, deleted.ItemID, deleted.ToUOM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR UoMFactorReference - END */
