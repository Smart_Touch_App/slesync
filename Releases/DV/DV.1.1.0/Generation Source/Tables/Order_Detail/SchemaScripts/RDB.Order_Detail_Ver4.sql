/* [BUSDTA].[Order_Detail] - begins */

/* TableDDL - [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail]') IS NULL
BEGIN

CREATE TABLE "BUSDTA"."Order_Detail" (
    "OrderID"                        numeric(8,0) NOT NULL
   ,"LineID"                         numeric(7,4) NOT NULL
   ,"ItemId"                         numeric(8,0) NULL
   ,"LongItem"                       nvarchar(25) NULL
   ,"ItemLocation"                   nvarchar(20) NULL
   ,"ItemLotNumber"                  nvarchar(30) NULL
   ,"LineType"                       nvarchar(2) NULL
   ,"IsNonStock"                     nvarchar(1) NULL
   ,"OrderQty"                       numeric(8,0) NOT NULL
   ,"OrderUM"                        nvarchar(2) NULL
   ,"OrderQtyInPricingUM"            numeric(8,0) NOT NULL
   ,"PricingUM"                      nvarchar(2) NULL
   ,"UnitPriceOriginalAmtInPriceUM"  numeric(12,4) NULL
   ,"UnitPriceAmtInPriceUoM"         numeric(12,4) NULL
   ,"UnitPriceAmt"                   numeric(12,4) NULL
   ,"ExtnPriceAmt"                   numeric(12,4) NULL
   ,"ItemSalesTaxAmt"                numeric(12,4) NULL
   ,"Discounted"                     nvarchar(1) NULL
   ,"PriceOverriden"                 nvarchar(1) NULL
   ,"PriceOverrideReasonCodeId"      numeric(3,0) NULL
   ,"IsTaxable"                      nvarchar(1) NULL
   ,"ReturnHeldQty"                  numeric(8,0) NULL
   ,"SalesCat1"                      nvarchar(3) NULL
   ,"SalesCat2"                      nvarchar(3) NULL
   ,"SalesCat3"                      nvarchar(3) NULL
   ,"SalesCat4"                      nvarchar(3) NULL
   ,"SalesCat5"                      nvarchar(3) NULL
   ,"PurchasingCat1"                 nvarchar(3) NULL
   ,"PurchasingCat2"                 nvarchar(3) NULL
   ,"PurchasingCat3"                 nvarchar(3) NULL
   ,"PurchasingCat4"                 nvarchar(3) NULL
   ,"PurchasingCat5"                 nvarchar(3) NULL
   ,"JDEOrderCompany"                nvarchar(5) NULL
   ,"JDEOrderNumber"                 numeric(8,0) NULL
   ,"JDEOrderType"                   nvarchar(2) NULL
   ,"JDEOrderLine"                   numeric(7,4) NULL
   ,"JDEInvoiceCompany"              nvarchar(5) NULL
   ,"JDEInvoiceNumber"               numeric(8,0) NULL
   ,"JDEOInvoiceType"                nvarchar(2) NULL
   ,"JDEZBatchNumber"                nvarchar(15) NULL
   ,"JDEProcessID"                   numeric(3,0) NULL
   ,"SettlementID"                   numeric(8,0) NULL
   ,"ExtendedAmtVariance"            numeric(12,4) NULL
   ,"TaxAmountAmtVariance"           numeric(12,4) NULL
   ,"HoldCode"                       nvarchar(2) NULL
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("OrderID" ASC,"LineID" ASC) 
)
END
/* TableDDL - [BUSDTA].[Order_Detail] - End */
