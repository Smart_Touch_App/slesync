/* [BUSDTA].[UDCKEYLIST] - begins */

/* TableDDL - [BUSDTA].[UDCKEYLIST] - Start */
IF OBJECT_ID('[BUSDTA].[UDCKEYLIST]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UDCKEYLIST]
	(
	  [DTSY] NCHAR(4) NOT NULL
	, [DTRT] NCHAR(2) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_UDCKEYLIST_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_UDCKEYLIST] PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
/* TableDDL - [BUSDTA].[UDCKEYLIST] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[UDCKEYLIST] - Start */
IF OBJECT_ID('[BUSDTA].[UDCKEYLIST_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UDCKEYLIST_del]
	(
	  [DTSY] NCHAR(4)
	, [DTRT] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[UDCKEYLIST] - End */
/* TRIGGERS FOR UDCKEYLIST - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.UDCKEYLIST_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.UDCKEYLIST_ins
	ON BUSDTA.UDCKEYLIST AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.UDCKEYLIST_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.UDCKEYLIST_del.DTRT= inserted.DTRT AND BUSDTA.UDCKEYLIST_del.DTSY= inserted.DTSY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.UDCKEYLIST_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UDCKEYLIST_upd
	ON BUSDTA.UDCKEYLIST AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.UDCKEYLIST
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.UDCKEYLIST.DTRT= inserted.DTRT AND BUSDTA.UDCKEYLIST.DTSY= inserted.DTSY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.UDCKEYLIST_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.UDCKEYLIST_dlt
	ON BUSDTA.UDCKEYLIST AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.UDCKEYLIST_del (DTRT, DTSY, last_modified )
	SELECT deleted.DTRT, deleted.DTSY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR UDCKEYLIST - END */
