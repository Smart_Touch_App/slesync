/* [BUSDTA].[Response_Master] - begins */

/* TableDDL - [BUSDTA].[Response_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Response_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Response_Master]
	(
	  [ResponseId] NUMERIC(8,0) NOT NULL
	, [Response] NVARCHAR (50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ResponseId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Response_Master] - End */
