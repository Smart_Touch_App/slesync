


SELECT "BUSDTA"."Cash_Verification_Detail"."CashVerificationDetailId"  
,"BUSDTA"."Cash_Verification_Detail"."SettlementDetailId"  
,"BUSDTA"."Cash_Verification_Detail"."CashTypeId"  
,"BUSDTA"."Cash_Verification_Detail"."Quantity"  
,"BUSDTA"."Cash_Verification_Detail"."Amount"  
,"BUSDTA"."Cash_Verification_Detail"."RouteId"  
,"BUSDTA"."Cash_Verification_Detail"."CreatedBy"  
,"BUSDTA"."Cash_Verification_Detail"."CreatedDatetime"  
,"BUSDTA"."Cash_Verification_Detail"."UpdatedBy"  
,"BUSDTA"."Cash_Verification_Detail"."UpdatedDatetime"    

FROM "BUSDTA"."Cash_Verification_Detail", "BUSDTA"."Route_Settlement_Detail", "BUSDTA"."Route_Settlement"  
WHERE ("BUSDTA"."Cash_Verification_Detail"."last_modified" >= {ml s.last_table_download} 
or "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download} 
or "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download})  
AND "BUSDTA"."Cash_Verification_Detail"."SettlementDetailId" ="BUSDTA"."Route_Settlement_Detail"."SettlementDetailId" 
AND "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId"  
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     