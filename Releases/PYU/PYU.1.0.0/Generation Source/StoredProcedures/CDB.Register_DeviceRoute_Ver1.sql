
CREATE PROCEDURE [dbo].[Register_DeviceRoute]
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30)
)
AS
BEGIN
	IF EXISTS(SELECT 1 FROM BUSDTA.Route_Master WHERE RouteName=@RouteID)
	BEGIN
		UPDATE BUSDTA.Route_Master SET RouteStatusID=(SELECT RouteStatusID FROM busdta.RouteStatus WHERE RouteStatus='REGISTERED')
			WHERE RouteName=@RouteID
		UPDATE BUSDTA.Route_Device_Map SET Active=1 WHERE Device_Id=@DeviceId AND Route_Id=@RouteID
	END
END

GO
