
CREATE PROCEDURE [dbo].[USP_GetDownloadCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Download Cursor */
DECLARE @selectStmt NVARCHAR(MAX)
DECLARE @dc NVARCHAR(MAX)
SET @selectStmt = NULL
--SELECT @selectStmt = COALESCE(@selectStmt + ',','') + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"', COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
SELECT @selectStmt = COALESCE(@selectStmt + ',','') + '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '".' + '"' + COLUMN_NAME + '"' + CHAR(13) + CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME=@table AND COLUMN_NAME != 'last_modified'
ORDER BY TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION

SELECT @dc = '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '"'+CHAR(13) + CHAR(10)+
	'WHERE ' + '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '".' + '"last_modified">= {ml s.last_table_download}'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

PRINT 'SELECT ' + @selectStmt
PRINT 'FROM ' +@dc

/* Download Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END



GO
