 
/* Insert the row into the consolidated database. */
INSERT INTO "dbo"."tblEquipment"
("EquipmentID", "ProspectID", "EquipmentType", "EquipmentCategoryID", "EquipmentSubCategory", "EquipmentQuantity", "EquipmentOwned", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."EquipmentID"}, {ml r."ProspectID"}, {ml r."EquipmentType"}, {ml r."EquipmentCategoryID"}, {ml r."EquipmentSubCategory"}, {ml r."EquipmentQuantity"}, {ml r."EquipmentOwned"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
