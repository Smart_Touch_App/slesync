

/* Date Modified: 11-01-2016 */
 
SELECT "tblEquipment"."EquipmentID"
,"tblEquipment"."ProspectID"
,"tblEquipment"."EquipmentType"
,"tblEquipment"."EquipmentCategoryID"
,"tblEquipment"."EquipmentSubCategory"
,"tblEquipment"."EquipmentQuantity"
,"tblEquipment"."EquipmentOwned"
,"tblEquipment"."CreatedBy"
,"tblEquipment"."CreatedDatetime"
,"tblEquipment"."UpdatedBy"
,"tblEquipment"."UpdatedDatetime"

FROM "dbo"."tblEquipment"
INNER JOIN BUSDTA.Prospect_Master ON "tblEquipment".ProspectID=BUSDTA.Prospect_Master.ProspectId 
WHERE "dbo"."tblEquipment"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Master"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

 
