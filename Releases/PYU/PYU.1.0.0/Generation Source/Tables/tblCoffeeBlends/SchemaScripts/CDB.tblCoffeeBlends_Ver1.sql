/* [dbo].[tblCoffeeBlends] - begins */

/* TableDDL - [dbo].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[dbo].[tblCoffeeBlends]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCoffeeBlends]
	(
	  [CoffeeBlendID] INT NOT NULL IDENTITY(1,1)
	, [CoffeeBlend] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeBlends] PRIMARY KEY ([CoffeeBlendID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblCoffeeBlends] - End */

/* SHADOW TABLE FOR [dbo].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[dbo].[tblCoffeeBlends_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCoffeeBlends_del]
	(
	  [CoffeeBlendID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeBlendID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblCoffeeBlends] - End */
/* TRIGGERS FOR tblCoffeeBlends - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblCoffeeBlends_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblCoffeeBlends_ins
	ON dbo.tblCoffeeBlends AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblCoffeeBlends_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblCoffeeBlends_del.CoffeeBlendID= inserted.CoffeeBlendID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblCoffeeBlends_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblCoffeeBlends_upd
	ON dbo.tblCoffeeBlends AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblCoffeeBlends
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblCoffeeBlends.CoffeeBlendID= inserted.CoffeeBlendID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblCoffeeBlends_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblCoffeeBlends_dlt
	ON dbo.tblCoffeeBlends AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblCoffeeBlends_del (CoffeeBlendID, last_modified )
	SELECT deleted.CoffeeBlendID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeBlends - END */
