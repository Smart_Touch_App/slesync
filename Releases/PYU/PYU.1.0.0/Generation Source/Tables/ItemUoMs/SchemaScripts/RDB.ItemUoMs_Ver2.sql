
/* [BUSDTA].[ItemUoMs] - begins */

/* TableDDL - [BUSDTA].[ItemUoMs] - Start */
IF OBJECT_ID('[BUSDTA].[ItemUoMs]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemUoMs]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [UOM] NVARCHAR (2) NOT NULL
	, [CrossReferenceID] NUMERIC(8,0) NULL
	, [CanSell] BIT NULL
	, [CanSample] BIT NULL
	, [CanRestock] BIT NULL
	, [DisplaySeq] NUMERIC(2,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([ItemID] ASC, [UOM] ASC)
	)
END
/* TableDDL - [BUSDTA].[ItemUoMs] - End */

