
/* [BUSDTA].[Check_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Check_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Check_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Check_Verification_Detail]
	(
	  [CheckVerificationDetailId] NUMERIC(18,0) NOT NULL DEFAULT autoincrement
	, [SettlementDetailId] NUMERIC(18,0) NOT NULL
	, [CheckDetailsId] NUMERIC(18,0) NOT NULL
	, [RouteId] NUMERIC(18,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([CheckVerificationDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Check_Verification_Detail] - End */

