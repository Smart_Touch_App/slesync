/* [dbo].[tblBrandLable] - begins */

/* TableDDL - [dbo].[tblBrandLable] - Start */
IF OBJECT_ID('[dbo].[tblBrandLable]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblBrandLable]
	(
	  [BrandID] INT NOT NULL IDENTITY(1,1)
	, [BrandLable] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLBRANDLABLE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLBRANDLABLE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLBRANDLABLE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblBrandLable] PRIMARY KEY ([BrandID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblBrandLable] - End */

/* SHADOW TABLE FOR [dbo].[tblBrandLable] - Start */
IF OBJECT_ID('[dbo].[tblBrandLable_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblBrandLable_del]
	(
	  [BrandID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([BrandID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblBrandLable] - End */
/* TRIGGERS FOR tblBrandLable - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblBrandLable_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblBrandLable_ins
	ON dbo.tblBrandLable AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblBrandLable_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblBrandLable_del.BrandID= inserted.BrandID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblBrandLable_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblBrandLable_upd
	ON dbo.tblBrandLable AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblBrandLable
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblBrandLable.BrandID= inserted.BrandID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblBrandLable_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblBrandLable_dlt
	ON dbo.tblBrandLable AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblBrandLable_del (BrandID, last_modified )
	SELECT deleted.BrandID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblBrandLable - END */
