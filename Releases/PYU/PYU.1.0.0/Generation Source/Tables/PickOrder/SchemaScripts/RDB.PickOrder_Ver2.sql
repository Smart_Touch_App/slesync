/* [BUSDTA].[PickOrder] - begins */

/* TableDDL - [BUSDTA].[PickOrder] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder]
	(
	  [PickOrder_Id] INTEGER NOT NULL DEFAULT autoincrement
	, [Order_ID] INTEGER NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Item_Number] NVARCHAR (25) NULL
	, [Order_Qty] INTEGER NULL
	, [Order_UOM] NVARCHAR (2) NULL
	, [Picked_Qty_Primary_UOM] INTEGER NULL
	, [Primary_UOM] NVARCHAR (2) NULL
	, [Order_Qty_Primary_UOM] INTEGER NULL
	, [On_Hand_Qty_Primary] INTEGER NULL
	, [Last_Scan_Mode] BIT NULL
	, [Item_Scan_Sequence] INTEGER NULL
	, [Picked_By] INTEGER NULL
	, [IsOnHold] INTEGER NULL
	, [Reason_Code_Id] INTEGER NULL
	, [ManuallyPickCount] INTEGER NULL
	, PRIMARY KEY ([PickOrder_Id] ASC, [Order_ID] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[PickOrder] - End */
