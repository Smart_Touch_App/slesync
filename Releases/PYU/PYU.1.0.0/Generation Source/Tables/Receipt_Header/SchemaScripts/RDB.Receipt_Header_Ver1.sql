/* [BUSDTA].[Receipt_Header] - begins */

/* TableDDL - [BUSDTA].[Receipt_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Receipt_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Receipt_Header]
	(
	  [CustomerId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ReceiptID] NUMERIC(8,0) NOT NULL
	, [ReceiptNumber] NVARCHAR (20) NULL
	, [ReceiptDate] DATE NULL
	, [TransactionAmount] NUMERIC(8,4) NULL
	, [PaymentMode] NVARCHAR (1) NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [CreditReasonCodeId] NUMERIC(3,0) NULL
	, [CreditMemoNote] NVARCHAR (200) NULL
	, [TransactionMode] NVARCHAR (1) NULL
	, [ChequeNum] NVARCHAR (10) NULL
	, [ChequeDate] DATE NULL
	, [StatusId] NUMERIC(3,0) NULL
	, [JDEStatusId] NUMERIC(3,0) NULL
	, [JDEReferenceId] NUMERIC(8,0) NULL
	, [SettelmentId] NUMERIC(8,0) NULL
	, [AuthoritySignature] LONG BINARY NULL
	, [CustomerSignature] LONG BINARY NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([CustomerId] ASC, [RouteId] ASC, [ReceiptID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Receipt_Header] - End */
