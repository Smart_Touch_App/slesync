/* [dbo].[SearchTMP] - begins */

/* TableDDL - [dbo].[SearchTMP] - Start */
IF OBJECT_ID('[dbo].[SearchTMP]') IS NULL
BEGIN

	CREATE TABLE [dbo].[SearchTMP]
	(
	  [PMAN8] NUMERIC(8,0) NOT NULL
	, [PMALPH] NCHAR(40) NULL
	, [PMROUT] NCHAR(10) NOT NULL
	, [PMCRBY] NCHAR(25) NULL
	, [PMCRDT] DATE NULL
	, [PMUPBY] NCHAR(25) NULL
	, [PMUPDT] DATE NULL
	, [PMSRS] INT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_SEARCHTMP_last_modified DEFAULT(getdate())
	)

END
/* TableDDL - [dbo].[SearchTMP] - End */
