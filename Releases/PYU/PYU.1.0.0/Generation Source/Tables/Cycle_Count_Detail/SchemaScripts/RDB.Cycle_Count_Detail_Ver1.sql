
/* [BUSDTA].[Cycle_Count_Detail] - begins */

/* TableDDL - [BUSDTA].[Cycle_Count_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Detail]
	(
	  [CycleCountID] NUMERIC(8,0) NOT NULL
	, [CycleCountDetailID] NUMERIC(8,0) NOT NULL
	, [ReCountNumber] NUMERIC(2,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [CountedQty] NUMERIC(4,0) NOT NULL
	, [ItemStatus] NVARCHAR (1) NULL
	, [OnHandQty] NUMERIC(4,0) NULL
	, [HeldQty] NUMERIC(4,0) NULL
	, [CountAccepted] NVARCHAR (1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([CycleCountID] ASC, [CycleCountDetailID] ASC, [ReCountNumber] ASC, [ItemId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Cycle_Count_Detail] - End */

