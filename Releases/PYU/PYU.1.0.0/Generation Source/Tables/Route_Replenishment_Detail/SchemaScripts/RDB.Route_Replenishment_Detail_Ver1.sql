/* [BUSDTA].[Route_Replenishment_Detail] - begins */

/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Replenishment_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Replenishment_Detail]
	(
	  [ReplenishmentID] NUMERIC(8,0) NOT NULL
	, [ReplenishmentDetailID] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ReplenishmentQtyUM] NVARCHAR (2) NOT NULL
	, [ReplenishmentQty] NUMERIC(8,0) NOT NULL
	, [AdjustmentQty] NUMERIC(8,0) NOT NULL
	, [PickedQty] NUMERIC(8,0) NULL
	, [PickedStatusTypeID] NUMERIC(3,0) NULL
	, [AvailaibilityAtTime] NUMERIC(8,0) NULL
	, [CurrentAvailability] NUMERIC(8,0) NULL
	, [DemandQty] NUMERIC(8,0) NULL DEFAULT 0
	, [ParLevelQty] NUMERIC(8,0) NULL
	, [OpenReplnQty] NUMERIC(8,0) NULL
	, [ShippedQty] NUMERIC(8,0) NULL
	, [OrderID] NUMERIC(8,0) NULL
	, [LastSaved] DATETIME NULL
	, [SequenceNumber] NUMERIC(3,0) NULL DEFAULT 1
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ReplenishmentID] ASC, [ReplenishmentDetailID] ASC, [ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Replenishment_Detail] - End */
