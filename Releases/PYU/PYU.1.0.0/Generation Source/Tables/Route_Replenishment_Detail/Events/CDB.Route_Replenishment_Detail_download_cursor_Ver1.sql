 SELECT "BUSDTA"."Route_Replenishment_Detail"."ReplenishmentID"
,"BUSDTA"."Route_Replenishment_Detail"."ReplenishmentDetailID"
,"BUSDTA"."Route_Replenishment_Detail"."ItemId"
,"BUSDTA"."Route_Replenishment_Detail"."RouteId"
,"BUSDTA"."Route_Replenishment_Detail"."ReplenishmentQtyUM"
,"BUSDTA"."Route_Replenishment_Detail"."ReplenishmentQty"
,"BUSDTA"."Route_Replenishment_Detail"."AdjustmentQty"
,"BUSDTA"."Route_Replenishment_Detail"."PickedQty"
,"BUSDTA"."Route_Replenishment_Detail"."PickedStatusTypeID"
,"BUSDTA"."Route_Replenishment_Detail"."AvailaibilityAtTime"
,"BUSDTA"."Route_Replenishment_Detail"."CurrentAvailability"
,"BUSDTA"."Route_Replenishment_Detail"."DemandQty"
,"BUSDTA"."Route_Replenishment_Detail"."ParLevelQty"
,"BUSDTA"."Route_Replenishment_Detail"."OpenReplnQty"
,"BUSDTA"."Route_Replenishment_Detail"."ShippedQty"
,"BUSDTA"."Route_Replenishment_Detail"."OrderID"
,"BUSDTA"."Route_Replenishment_Detail"."LastSaved"
,"BUSDTA"."Route_Replenishment_Detail"."SequenceNumber"
,"BUSDTA"."Route_Replenishment_Detail"."CreatedBy"
,"BUSDTA"."Route_Replenishment_Detail"."CreatedDatetime"
,"BUSDTA"."Route_Replenishment_Detail"."UpdatedBy"
,"BUSDTA"."Route_Replenishment_Detail"."UpdatedDatetime"
 
FROM "BUSDTA"."Route_Replenishment_Detail", "BUSDTA"."Route_Replenishment_Header"
WHERE "BUSDTA"."Route_Replenishment_Detail"."UpdatedDatetime">= {ml s.last_table_download}
 AND "BUSDTA"."Route_Replenishment_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 AND "BUSDTA"."Route_Replenishment_Detail"."ReplenishmentID" = "BUSDTA"."Route_Replenishment_Header"."ReplenishmentID"


 

