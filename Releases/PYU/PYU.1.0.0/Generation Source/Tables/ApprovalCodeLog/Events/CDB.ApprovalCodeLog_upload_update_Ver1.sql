 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ApprovalCodeLog"
SET "ApprovalCode" = {ml r."ApprovalCode"}, "RequestCode" = {ml r."RequestCode"}, "Feature" = {ml r."Feature"}, "Amount" = {ml r."Amount"}, "RequestedByUser" = {ml r."RequestedByUser"}, "RequestedForCustomer" = {ml r."RequestedForCustomer"}, "Payload" = {ml r."Payload"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "LogID" = {ml r."LogID"} AND "RouteID" = {ml r."RouteID"}
 
