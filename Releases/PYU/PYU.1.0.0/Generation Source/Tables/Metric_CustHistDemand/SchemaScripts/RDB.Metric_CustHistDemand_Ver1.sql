/* [BUSDTA].[Metric_CustHistDemand] - begins */

/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - Start */
IF OBJECT_ID('[BUSDTA].[Metric_CustHistDemand]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Metric_CustHistDemand]
	(
	  [CustomerId] NUMERIC(8,0) NOT NULL
	, [ItemID] NUMERIC(8,0) NOT NULL
	, [DemandFromDate] DATE NULL
	, [DemandThroughDate] DATE NULL
	, [PlannedStops] NUMERIC(4,0) NULL
	, [ActualStops] NUMERIC(4,0) NULL
	, [TotalQtySold] NUMERIC(12,0) NULL
	, [TotalQtyReturned] NUMERIC(12,0) NULL
	, [QtySoldPerStop] NUMERIC(12,4) NULL
	, [QtyReturnedPerStop] NUMERIC(12,4) NULL
	, [NetQtySoldPerStop] NUMERIC(12,4) NULL
	, [QtySoldDeltaPerStop] NUMERIC(12,4) NULL
	, [QtyReturnedDeltaPerStop] NUMERIC(12,4) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([CustomerId] ASC, [ItemID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - End */
