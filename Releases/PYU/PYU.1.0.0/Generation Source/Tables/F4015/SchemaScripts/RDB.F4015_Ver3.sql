
/* [BUSDTA].[F4015] - begins */

/* TableDDL - [BUSDTA].[F4015] - Start */
IF OBJECT_ID('[BUSDTA].[F4015]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4015]
	(
	  [OTORTP] NVARCHAR (8) NOT NULL
	, [OTAN8] NUMERIC(8,0) NOT NULL
	, [OTOSEQ] NUMERIC(4,0)  NULL
	, [OTITM] NUMERIC(8,0) NOT NULL
	, [OTLITM] NVARCHAR (25) NULL
	, [OTQTYU] FLOAT NULL
	, [OTUOM] NVARCHAR (2) NULL
	, [OTLNTY] NVARCHAR (2) NULL
	, [OTEFTJ] NUMERIC(18,0) NULL
	, [OTEXDJ] NUMERIC(18,0) NULL	
	, PRIMARY KEY ([OTORTP] ASC, [OTAN8] ASC, [OTITM] ASC)
	)
END
/* TableDDL - [BUSDTA].[F4015] - End */

