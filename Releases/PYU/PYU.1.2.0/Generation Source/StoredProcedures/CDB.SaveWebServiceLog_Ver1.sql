
CREATE PROCEDURE [dbo].[SaveWebServiceLog]
(
	@DeviceID VARCHAR(100)='',
	@Request NVARCHAR(MAX),
	@Response NVARCHAR(MAX),
	@Environment VARCHAR(128)='Development',
	@ServiceName VARCHAR(100)='',
	@ServiceEndPoint VARCHAR(1000)='',
	@ServiceContract VARCHAR(1000)=''
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @DBName VARCHAR(128); 
	SET @DBName = (SELECT DB_NAME());

	EXEC [SystemDB].[dbo].[SaveWebServiceLog] 
			@DeviceID=@DeviceID, 
			@Request =@Request,
			@Response =@Response,
			@DBName = @DBName,
			@Environment = @Environment,
			@ServiceName = @ServiceName,
			@ServiceEndPoint = @ServiceEndPoint,
			@ServiceContract = @ServiceContract
SET NOCOUNT OFF
END
GO


