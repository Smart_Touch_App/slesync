

/* [BUSDTA].[Cash_Master] - begins */

/* TableDDL - [BUSDTA].[Cash_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Master]
	(
	  [CashId] NUMERIC(8,0) NOT NULL
	, [CashType] NVARCHAR (10) NULL
	, [CashCode] NVARCHAR (10) NULL
	, [CashDescription] NVARCHAR (100) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([CashId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Cash_Master] - End */
