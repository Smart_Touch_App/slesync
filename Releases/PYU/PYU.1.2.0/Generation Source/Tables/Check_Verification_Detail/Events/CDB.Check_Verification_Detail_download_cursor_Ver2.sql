
 SELECT "BUSDTA"."Check_Verification_Detail"."CheckVerificationDetailId"  ,
 "BUSDTA"."Check_Verification_Detail"."SettlementDetailId"  ,
 "BUSDTA"."Check_Verification_Detail"."CheckDetailsId"  ,
 "BUSDTA"."Check_Verification_Detail"."RouteId"  ,
 "BUSDTA"."Check_Verification_Detail"."CreatedBy"  ,
 "BUSDTA"."Check_Verification_Detail"."CreatedDatetime"  ,
 "BUSDTA"."Check_Verification_Detail"."UpdatedBy"  ,
 "BUSDTA"."Check_Verification_Detail"."UpdatedDatetime"   
  FROM "BUSDTA"."Check_Verification_Detail"   ,"BUSDTA"."Route_Settlement_Detail",      
  "BUSDTA"."Route_Settlement" 
   WHERE ("BUSDTA"."Check_Verification_Detail"."last_modified" >= {ml s.last_table_download} 
   or "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download} or 
   "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download})  AND 
   "BUSDTA"."Check_Verification_Detail"."SettlementDetailId" ="BUSDTA"."Route_Settlement_Detail"."SettlementDetailId"  AND 
   "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId" 
    AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})     