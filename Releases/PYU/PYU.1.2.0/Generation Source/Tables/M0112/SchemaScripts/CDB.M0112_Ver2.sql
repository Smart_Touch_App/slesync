
/* [BUSDTA].[M0112] - begins */

/* TableDDL - [BUSDTA].[M0112] - Start */
IF OBJECT_ID('[BUSDTA].[M0112]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0112]
	(
	  [NDAN8] NUMERIC(8,0) NOT NULL
	, [NDID] NUMERIC(8,0) NOT NULL
	, [NDDTTM] DATETIME NULL
	, [NDDTLS] NCHAR(500) NULL
	, [NDTYP] NCHAR(15) NULL
	, [NDDFLT] BIT NULL
	, [NDCRBY] NCHAR(50) NULL
	, [NDCRDT] DATE NULL
	, [NDUPBY] NCHAR(50) NULL
	, [NDUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M0112_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M0112] PRIMARY KEY ([NDAN8] ASC, [NDID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M0112] - End */

/* SHADOW TABLE FOR [BUSDTA].[M0112] - Start */
IF OBJECT_ID('[BUSDTA].[M0112_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0112_del]
	(
	  [NDAN8] NUMERIC(8,0)
	, [NDID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([NDAN8] ASC, [NDID] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M0112] - End */
/* TRIGGERS FOR M0112 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M0112_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M0112_ins
	ON BUSDTA.M0112 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M0112_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M0112_del.NDAN8= inserted.NDAN8 AND BUSDTA.M0112_del.NDID= inserted.NDID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M0112_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0112_upd
	ON BUSDTA.M0112 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M0112
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M0112.NDAN8= inserted.NDAN8 AND BUSDTA.M0112.NDID= inserted.NDID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M0112_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0112_dlt
	ON BUSDTA.M0112 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M0112_del (NDAN8, NDID, last_modified )
	SELECT deleted.NDAN8, deleted.NDID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M0112 - END */

