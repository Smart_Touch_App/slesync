/* [BUSDTA].[Prospect_Quote_Detail] - begins */

/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Detail]
	(
	  [ProspectQuoteId] NUMERIC(8,0) NOT NULL
	, [ProspectQuoteDetailId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [QuoteQty] NUMERIC(4,0) NOT NULL
	, [SampleQty] NUMERIC(4,0) NOT NULL
	, [SampleUM] NCHAR(5) NULL
	, [PricingAmt] NUMERIC(8,4) NULL
	, [PricingUM] NCHAR(2) NULL
	, [TransactionAmt] NUMERIC(8,4) NULL
	, [TransactionUM] NCHAR(2) NULL
	, [OtherAmt] NUMERIC(8,4) NULL
	, [OtherUM] NCHAR(2) NULL
	, [CompetitorName] NVARCHAR(50) NOT NULL
	, [UnitPriceAmt] NUMERIC(8,4) NULL
	, [UnitPriceUM] NCHAR(2) NULL
	, [AvailableQty] NUMERIC(4,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_QUOTE_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Quote_Detail] PRIMARY KEY ([ProspectQuoteId] ASC, [ProspectQuoteDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Detail_del]
	(
	  [ProspectQuoteId] NUMERIC(8,0)
	, [ProspectQuoteDetailId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectQuoteId] ASC, [ProspectQuoteDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Detail] - End */
/* TRIGGERS FOR Prospect_Quote_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Quote_Detail_ins
	ON BUSDTA.Prospect_Quote_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Quote_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Quote_Detail_del.ProspectQuoteDetailId= inserted.ProspectQuoteDetailId AND BUSDTA.Prospect_Quote_Detail_del.ProspectQuoteId= inserted.ProspectQuoteId AND BUSDTA.Prospect_Quote_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Detail_upd
	ON BUSDTA.Prospect_Quote_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Quote_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Quote_Detail.ProspectQuoteDetailId= inserted.ProspectQuoteDetailId AND BUSDTA.Prospect_Quote_Detail.ProspectQuoteId= inserted.ProspectQuoteId AND BUSDTA.Prospect_Quote_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Detail_dlt
	ON BUSDTA.Prospect_Quote_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Quote_Detail_del (ProspectQuoteDetailId, ProspectQuoteId, RouteId, last_modified )
	SELECT deleted.ProspectQuoteDetailId, deleted.ProspectQuoteId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Quote_Detail - END */
