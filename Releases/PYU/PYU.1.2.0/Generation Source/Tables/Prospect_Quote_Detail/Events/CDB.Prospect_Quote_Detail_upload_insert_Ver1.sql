 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Prospect_Quote_Detail"
("ProspectQuoteId", "ProspectQuoteDetailId", "RouteId", "ItemId", "QuoteQty", "SampleQty", "SampleUM", "PricingAmt", "PricingUM", "TransactionAmt", "TransactionUM", "OtherAmt", "OtherUM", "CompetitorName", "UnitPriceAmt", "UnitPriceUM", "AvailableQty", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ProspectQuoteId"}, {ml r."ProspectQuoteDetailId"}, {ml r."RouteId"}, {ml r."ItemId"}, {ml r."QuoteQty"}, {ml r."SampleQty"}, {ml r."SampleUM"}, {ml r."PricingAmt"}, {ml r."PricingUM"}, {ml r."TransactionAmt"}, {ml r."TransactionUM"}, {ml r."OtherAmt"}, {ml r."OtherUM"}, {ml r."CompetitorName"}, {ml r."UnitPriceAmt"}, {ml r."UnitPriceUM"}, {ml r."AvailableQty"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
