 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Replenishment_Detail"
SET "ReplenishmentQtyUM" = {ml r."ReplenishmentQtyUM"}, "ReplenishmentQty" = {ml r."ReplenishmentQty"}, "AdjustmentQty" = {ml r."AdjustmentQty"}, "PickedQty" = {ml r."PickedQty"}, "PickedStatusTypeID" = {ml r."PickedStatusTypeID"}, "AvailaibilityAtTime" = {ml r."AvailaibilityAtTime"}, "CurrentAvailability" = {ml r."CurrentAvailability"}, "DemandQty" = {ml r."DemandQty"}, "ParLevelQty" = {ml r."ParLevelQty"}, "OpenReplnQty" = {ml r."OpenReplnQty"}, "ShippedQty" = {ml r."ShippedQty"}, "OrderID" = {ml r."OrderID"}, "LastSaved" = {ml r."LastSaved"}, "SequenceNumber" = {ml r."SequenceNumber"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ReplenishmentID" = {ml r."ReplenishmentID"} AND "ReplenishmentDetailID" = {ml r."ReplenishmentDetailID"} AND "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 
