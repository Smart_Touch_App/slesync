 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."MoneyOrderDetails"
SET "MoneyOrderNumber" = {ml r."MoneyOrderNumber"}, "MoneyOrderAmount" = {ml r."MoneyOrderAmount"}, "MoneyOrderFeeAmount" = {ml r."MoneyOrderFeeAmount"}, "StatusId" = {ml r."StatusId"}, "MoneyOrderDatetime" = {ml r."MoneyOrderDatetime"}, "VoidReasonId" = {ml r."VoidReasonId"}, "RouteSettlementId" = {ml r."RouteSettlementId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "MoneyOrderId" = {ml r."MoneyOrderId"} AND "RouteId" = {ml r."RouteId"}
