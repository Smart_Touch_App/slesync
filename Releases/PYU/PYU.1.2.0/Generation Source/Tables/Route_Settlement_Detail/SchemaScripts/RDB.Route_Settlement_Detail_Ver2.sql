/* [BUSDTA].[Route_Settlement_Detail] - begins */

/* TableDDL - [BUSDTA].[Route_Settlement_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement_Detail]
	(
	  [SettlementId] NVARCHAR(8) NOT NULL
	, [SettlementDetailId] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [UserId] NUMERIC(8,0) NULL
	, [VerificationNum] NUMERIC(2,0) NOT NULL
	, [CashAmount] NUMERIC(8,4) NOT NULL
	, [CheckAmount] NUMERIC(8,4) NOT NULL
	, [MoneyOrderAmount] NUMERIC(8,4) NOT NULL
	, [TotalVerified] NUMERIC(8,4) NOT NULL
	, [Expenses] NUMERIC(8,4) NOT NULL
	, [Payments] NUMERIC(8,4) NOT NULL
	, [OverShortAmount] NUMERIC(8,4) NOT NULL
	, [VerSignature] LONG BINARY NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OriginatingRoute] NUMERIC(8,0) NOT NULL
	, [partitioningRoute] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([SettlementId] ASC, [SettlementDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Settlement_Detail] - End */
