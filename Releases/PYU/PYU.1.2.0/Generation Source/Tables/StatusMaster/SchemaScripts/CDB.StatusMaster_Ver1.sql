/* [BUSDTA].[StatusMaster] - begins */

/* TableDDL - [BUSDTA].[StatusMaster] - Start */
IF OBJECT_ID('[BUSDTA].[StatusMaster]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[StatusMaster]	
(
	  
[StatusId] INT NOT NULL IDENTITY(1,1)	
, [StatusCode] VARCHAR(5) NOT NULL	
, [StatusDescription] NVARCHAR(100) NULL	
, [StatusForType] NVARCHAR(50) NULL	
, [CreatedBy] NUMERIC(8,0) NULL	
, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_STATUSMASTER_CreatedDatetime DEFAULT(getdate())	
, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_STATUSMASTER_UpdatedDatetime DEFAULT(getdate())	
, last_modified DATETIME DEFAULT GETDATE()	
, CONSTRAINT [PK_StatusMaster] PRIMARY KEY ([StatusId] ASC)	
)
END
/* TableDDL - [BUSDTA].[StatusMaster] - End */

/* SHADOW TABLE FOR [BUSDTA].[StatusMaster] - Start */
IF OBJECT_ID('[BUSDTA].[StatusMaster_del]') IS NULL
BEGIN
	
CREATE TABLE [BUSDTA].[StatusMaster_del]	
(
	  
[StatusId] INT	
,last_modified DATETIME DEFAULT GETDATE()	
,PRIMARY KEY ([StatusId] ASC)	)
END

/* SHADOW TABLE FOR [BUSDTA].[StatusMaster] - End */
/* TRIGGERS FOR StatusMaster - START */

/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.StatusMaster_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.StatusMaster_ins
	ON BUSDTA.StatusMaster AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.StatusMaster_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.StatusMaster_del.StatusId= inserted.StatusId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.StatusMaster_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.StatusMaster_upd
	ON BUSDTA.StatusMaster AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.StatusMaster
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.StatusMaster.StatusId= inserted.StatusId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.StatusMaster_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.StatusMaster_dlt
	ON BUSDTA.StatusMaster AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.StatusMaster_del (StatusId, last_modified )
	SELECT deleted.StatusId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR StatusMaster - END */
