



/* TableDDL - [BUSDTA].[tblGroupTables] - Start */
IF OBJECT_ID('[BUSDTA].[tblGroupTables]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblGroupTables]
	(
	  [GroupTableID] INT NOT NULL IDENTITY(1,1)
	, [RefDBSchema] VARCHAR(128) NULL
	, [RefDBTable] VARCHAR(128) NULL
	, [ApplicationGroupID] INT NULL
	, [ConDBTable] VARCHAR(128) NULL
	, [IsMandatorySync] BIT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLGROUPTABLES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGroupTables] PRIMARY KEY ([GroupTableID] ASC)
	)

ALTER TABLE [BUSDTA].[tblGroupTables] WITH CHECK ADD CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID] FOREIGN KEY([ApplicationGroupID]) REFERENCES [BUSDTA].[tblApplicationGroup] ([ApplicationGroupID])
ALTER TABLE [BUSDTA].[tblGroupTables] CHECK CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID]

END
/* TableDDL - [BUSDTA].[tblGroupTables] - End */
