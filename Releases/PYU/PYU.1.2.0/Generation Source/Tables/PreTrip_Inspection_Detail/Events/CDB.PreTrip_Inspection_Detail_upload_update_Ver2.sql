 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PreTrip_Inspection_Detail"
SET "QuestionId" = {ml r."QuestionId"}, "ResponseID" = {ml r."ResponseID"}, "ResponseReason" = {ml r."ResponseReason"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "PreTripInspectionHeaderId" = {ml r."PreTripInspectionHeaderId"} AND "PreTripInspectionDetailId" = {ml r."PreTripInspectionDetailId"} AND "RouteId" = {ml r."RouteId"}
 
