/* [BUSDTA].[tblEquipment] - begins */

/* TableDDL - [BUSDTA].[tblEquipment] - Start */
IF OBJECT_ID('[BUSDTA].[tblEquipment]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblEquipment]
	(
	  [EquipmentID] INTEGER NOT NULL
	, [ProspectID] NUMERIC(10,0) NOT NULL
	, [EquipmentType] VARCHAR(200) NULL
	, [EquipmentCategoryID] INTEGER NULL
	, [EquipmentSubCategory] LONG VARCHAR NULL
	, [EquipmentQuantity] INTEGER NULL
	, [EquipmentOwned] BIT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([EquipmentID] ASC,"ProspectID" ASC)
	)
END
/* TableDDL - [BUSDTA].[tblEquipment] - End */
