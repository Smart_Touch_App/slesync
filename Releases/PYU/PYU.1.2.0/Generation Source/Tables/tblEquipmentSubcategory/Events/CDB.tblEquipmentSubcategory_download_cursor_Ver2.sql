 
SELECT "BUSDTA"."tblEquipmentSubcategory"."EquipmentSubcategoryID"
,"BUSDTA"."tblEquipmentSubcategory"."EquipmentCategoryID"
,"BUSDTA"."tblEquipmentSubcategory"."EquipmentSubCategory"
,"BUSDTA"."tblEquipmentSubcategory"."CreatedBy"
,"BUSDTA"."tblEquipmentSubcategory"."CreatedDatetime"
,"BUSDTA"."tblEquipmentSubcategory"."UpdatedBy"
,"BUSDTA"."tblEquipmentSubcategory"."UpdatedDatetime"

FROM "BUSDTA"."tblEquipmentSubcategory"
WHERE "BUSDTA"."tblEquipmentSubcategory"."last_modified">= {ml s.last_table_download}
