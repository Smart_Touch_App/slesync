
SELECT "Busdta"."tblAlliedProspCategory"."CategoryID"
,"Busdta"."tblAlliedProspCategory"."Category"
,"Busdta"."tblAlliedProspCategory"."CreatedBy"
,"Busdta"."tblAlliedProspCategory"."CreatedDatetime"
,"Busdta"."tblAlliedProspCategory"."UpdatedBy"
,"Busdta"."tblAlliedProspCategory"."UpdatedDatetime"

FROM "Busdta"."tblAlliedProspCategory"
WHERE "Busdta"."tblAlliedProspCategory"."last_modified">= {ml s.last_table_download}
