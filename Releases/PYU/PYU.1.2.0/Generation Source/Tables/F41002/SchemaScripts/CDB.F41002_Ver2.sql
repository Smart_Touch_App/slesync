
/* [BUSDTA].[F41002] - begins */

/* TableDDL - [BUSDTA].[F41002] - Start */
IF OBJECT_ID('[BUSDTA].[F41002]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F41002]
	(
	  [UMMCU] NCHAR(12) NOT NULL
	, [UMITM] NUMERIC(8,0) NOT NULL
	, [UMUM] NCHAR(2) NOT NULL
	, [UMRUM] NCHAR(2) NOT NULL
	, [UMUSTR] NCHAR(1) NULL
	, [UMCONV] FLOAT NULL
	, [UMCNV1] FLOAT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F41002_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F41002] PRIMARY KEY ([UMMCU] ASC, [UMITM] ASC, [UMUM] ASC, [UMRUM] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F41002] - End */

/* SHADOW TABLE FOR [BUSDTA].[F41002] - Start */
IF OBJECT_ID('[BUSDTA].[F41002_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F41002_del]
	(
	  [UMMCU] NCHAR(12)
	, [UMITM] NUMERIC(8,0)
	, [UMUM] NCHAR(2)
	, [UMRUM] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UMMCU] ASC, [UMITM] ASC, [UMUM] ASC, [UMRUM] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F41002] - End */
/* TRIGGERS FOR F41002 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F41002_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F41002_ins
	ON BUSDTA.F41002 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F41002_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F41002_del.UMITM= inserted.UMITM AND BUSDTA.F41002_del.UMMCU= inserted.UMMCU AND BUSDTA.F41002_del.UMRUM= inserted.UMRUM AND BUSDTA.F41002_del.UMUM= inserted.UMUM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F41002_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F41002_upd
	ON BUSDTA.F41002 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F41002
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F41002.UMITM= inserted.UMITM AND BUSDTA.F41002.UMMCU= inserted.UMMCU AND BUSDTA.F41002.UMRUM= inserted.UMRUM AND BUSDTA.F41002.UMUM= inserted.UMUM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F41002_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F41002_dlt
	ON BUSDTA.F41002 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F41002_del (UMITM, UMMCU, UMRUM, UMUM, last_modified )
	SELECT deleted.UMITM, deleted.UMMCU, deleted.UMRUM, deleted.UMUM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F41002 - END */

