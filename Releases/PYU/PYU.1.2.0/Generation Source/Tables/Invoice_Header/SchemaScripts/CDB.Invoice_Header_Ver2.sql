
/* [BUSDTA].[Invoice_Header] - begins */

/* TableDDL - [BUSDTA].[Invoice_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Invoice_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Invoice_Header]
	(
	  [InvoiceID] NUMERIC(8,0) NOT NULL
	, [CustShiptoId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OrderId] NUMERIC(8,0) NOT NULL
	, [ARInvoiceNumber] NCHAR(20) NULL
	, [DeviceInvoiceNumber] NCHAR(20) NULL
	, [ARInvoiceDate] DATE NULL
	, [DeviceInvoiceDate] DATE NULL
	, [DeviceStatusID] NUMERIC(3,0) NOT NULL
	, [ARStatusID] NUMERIC(3,0) NOT NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [DueDate] DATETIME NULL
	, [InvoicePaymentType] NCHAR(8) NULL
	, [GrossAmt] NUMERIC(8,4) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_INVOICE_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Invoice_Header] PRIMARY KEY ([InvoiceID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Invoice_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Invoice_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Invoice_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Invoice_Header_del]
	(
	  [InvoiceID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([InvoiceID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Invoice_Header] - End */
/* TRIGGERS FOR Invoice_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Invoice_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Invoice_Header_ins
	ON BUSDTA.Invoice_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Invoice_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Invoice_Header_del.InvoiceID= inserted.InvoiceID AND BUSDTA.Invoice_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Invoice_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Invoice_Header_upd
	ON BUSDTA.Invoice_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Invoice_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Invoice_Header.InvoiceID= inserted.InvoiceID AND BUSDTA.Invoice_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Invoice_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Invoice_Header_dlt
	ON BUSDTA.Invoice_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Invoice_Header_del (InvoiceID, RouteId, last_modified )
	SELECT deleted.InvoiceID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Invoice_Header - END */

