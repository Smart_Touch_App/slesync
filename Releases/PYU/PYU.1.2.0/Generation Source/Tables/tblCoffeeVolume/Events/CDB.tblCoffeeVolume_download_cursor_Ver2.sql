 
SELECT "BUSDTA"."tblCoffeeVolume"."CoffeeVolumeID"
,"BUSDTA"."tblCoffeeVolume"."Volume"
,"BUSDTA"."tblCoffeeVolume"."CreatedBy"
,"BUSDTA"."tblCoffeeVolume"."CreatedDatetime"
,"BUSDTA"."tblCoffeeVolume"."UpdatedBy"
,"BUSDTA"."tblCoffeeVolume"."UpdatedDatetime"

FROM "BUSDTA"."tblCoffeeVolume"
WHERE "BUSDTA"."tblCoffeeVolume"."last_modified">= {ml s.last_table_download}

