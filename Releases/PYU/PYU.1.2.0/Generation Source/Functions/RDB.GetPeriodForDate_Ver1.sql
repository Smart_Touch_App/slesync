CREATE OR REPLACE FUNCTION "BUSDTA"."GetPeriodForDate" (@CalendarDate date, @BaseDate date)
RETURNS int
AS BEGIN
	DECLARE @DateDifference int
	DECLARE @varDiv28 float
	DECLARE @PeriodNumber float
	DECLARE @ModNumber float

	select @DateDifference =  DATEDIFF(DAY,@BaseDate,@CalendarDate)+1
	select @varDiv28 = CEILING (@DateDifference / 28.00)
	select @ModNumber = @varDiv28%3
	if(@ModNumber = 0)
		select @PeriodNumber = 3
	else
		select @PeriodNumber = @ModNumber
	RETURN cast(@PeriodNumber as int)
END
GO