CREATE OR REPLACE FUNCTION "BUSDTA"."GetDayForDate" (CalendarDate date, BaseDate date)
RETURNS int
BEGIN
	DECLARE DateDifference int;

	set DateDifference = DATEPART (DW, CalendarDate)-1;

	RETURN DateDifference;
END
GO