/* [dbo].[tblStatsGroups] - begins */

/* TableDDL - [dbo].[tblStatsGroups] - Start */
IF OBJECT_ID('[dbo].[tblStatsGroups]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblStatsGroups]
	(
	  [StatsGroupID] INTEGER NOT NULL default autoincrement
	, [GroupName] VARCHAR(128) NULL

	, PRIMARY KEY ([StatsGroupID] ASC)
	)
END
/* TableDDL - [dbo].[tblStatsGroups] - End */
