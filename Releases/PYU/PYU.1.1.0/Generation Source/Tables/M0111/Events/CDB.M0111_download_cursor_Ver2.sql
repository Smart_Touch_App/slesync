
     SELECT "BUSDTA"."M0111"."CDAN8"  ,"BUSDTA"."M0111"."CDID"  ,"BUSDTA"."M0111"."CDIDLN"  ,"BUSDTA"."M0111"."CDRCK7"  ,
     "BUSDTA"."M0111"."CDCNLN"  ,"BUSDTA"."M0111"."CDAR1"  ,"BUSDTA"."M0111"."CDPH1"  ,"BUSDTA"."M0111"."CDEXTN1"  ,
     "BUSDTA"."M0111"."CDPHTP1"  ,"BUSDTA"."M0111"."CDDFLTPH1"  ,"BUSDTA"."M0111"."CDREFPH1"  ,"BUSDTA"."M0111"."CDAR2"  ,
     "BUSDTA"."M0111"."CDPH2"  ,"BUSDTA"."M0111"."CDEXTN2"  ,"BUSDTA"."M0111"."CDPHTP2"  ,"BUSDTA"."M0111"."CDDFLTPH2"  ,
     "BUSDTA"."M0111"."CDREFPH2"  ,"BUSDTA"."M0111"."CDAR3"  ,"BUSDTA"."M0111"."CDPH3"  ,"BUSDTA"."M0111"."CDEXTN3"  ,
     "BUSDTA"."M0111"."CDPHTP3"  ,"BUSDTA"."M0111"."CDDFLTPH3"  ,"BUSDTA"."M0111"."CDREFPH3"  ,"BUSDTA"."M0111"."CDAR4"  ,
     "BUSDTA"."M0111"."CDPH4"  ,"BUSDTA"."M0111"."CDEXTN4"  ,"BUSDTA"."M0111"."CDPHTP4"  ,"BUSDTA"."M0111"."CDDFLTPH4"  ,
     "BUSDTA"."M0111"."CDREFPH4"  ,"BUSDTA"."M0111"."CDEMAL1"  ,"BUSDTA"."M0111"."CDETP1"  ,"BUSDTA"."M0111"."CDDFLTEM1"  ,
     "BUSDTA"."M0111"."CDREFEM1"  ,"BUSDTA"."M0111"."CDEMAL2"  ,"BUSDTA"."M0111"."CDETP2"  ,"BUSDTA"."M0111"."CDDFLTEM2"  ,
     "BUSDTA"."M0111"."CDREFEM2"  ,"BUSDTA"."M0111"."CDEMAL3"  ,"BUSDTA"."M0111"."CDETP3"  ,"BUSDTA"."M0111"."CDDFLTEM3"  ,
     "BUSDTA"."M0111"."CDREFEM3"  ,"BUSDTA"."M0111"."CDGNNM"  ,"BUSDTA"."M0111"."CDMDNM"  ,"BUSDTA"."M0111"."CDSRNM"  ,
     "BUSDTA"."M0111"."CDTITL"  ,"BUSDTA"."M0111"."CDACTV"  ,"BUSDTA"."M0111"."CDDFLT"  ,"BUSDTA"."M0111"."CDSET"  ,
     "BUSDTA"."M0111"."CDCRBY"  ,"BUSDTA"."M0111"."CDCRDT"  ,"BUSDTA"."M0111"."CDUPBY"  ,"BUSDTA"."M0111"."CDUPDT"    
     FROM BUSDTA.M0111, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
     WHERE ("BUSDTA"."M0111"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
     or crm."last_modified" >= {ml s.last_table_download})    AND CDAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber   
     AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     