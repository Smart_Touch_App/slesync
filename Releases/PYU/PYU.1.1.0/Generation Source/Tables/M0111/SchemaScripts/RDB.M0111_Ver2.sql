
/* [BUSDTA].[M0111] - begins */

/* TableDDL - [BUSDTA].[M0111] - Start */
IF OBJECT_ID('[BUSDTA].[M0111]') IS NULL
BEGIN

CREATE TABLE "BUSDTA"."M0111" (
    "CDAN8"                          numeric(8,0) NOT NULL
   ,"CDID"                           numeric(8,0) NOT NULL DEFAULT autoincrement
   ,"CDIDLN"                         numeric(5,0) NOT NULL
   ,"CDRCK7"                         numeric(5,0) NOT NULL
   ,"CDCNLN"                         numeric(5,0) NOT NULL
   ,"CDAR1"                          nvarchar(6) NULL
   ,"CDPH1"                          nvarchar(20) NULL
   ,"CDEXTN1"                        nvarchar(8) NULL
   ,"CDPHTP1"                        numeric(8,0) NULL
   ,"CDDFLTPH1"                      bit NULL DEFAULT ((1))
   ,"CDREFPH1"                       integer NULL
   ,"CDAR2"                          nvarchar(6) NULL
   ,"CDPH2"                          nvarchar(20) NULL
   ,"CDEXTN2"                        nvarchar(8) NULL
   ,"CDPHTP2"                        numeric(8,0) NULL
   ,"CDDFLTPH2"                      bit NULL DEFAULT ((0))
   ,"CDREFPH2"                       integer NULL
   ,"CDAR3"                          nvarchar(6) NULL
   ,"CDPH3"                          nvarchar(20) NULL
   ,"CDEXTN3"                        nvarchar(8) NULL
   ,"CDPHTP3"                        numeric(8,0) NULL
   ,"CDDFLTPH3"                      bit NULL DEFAULT ((0))
   ,"CDREFPH3"                       integer NULL
   ,"CDAR4"                          nvarchar(6) NULL
   ,"CDPH4"                          nvarchar(20) NULL
   ,"CDEXTN4"                        nvarchar(8) NULL
   ,"CDPHTP4"                        numeric(8,0) NULL
   ,"CDDFLTPH4"                      bit NULL DEFAULT ((0))
   ,"CDREFPH4"                       integer NULL
   ,"CDEMAL1"                        nvarchar(256) NULL
   ,"CDETP1"                         numeric(3,0) NULL
   ,"CDDFLTEM1"                      bit NULL DEFAULT ((1))
   ,"CDREFEM1"                       integer NULL
   ,"CDEMAL2"                        nvarchar(256) NULL
   ,"CDETP2"                         numeric(3,0) NULL
   ,"CDDFLTEM2"                      bit NULL DEFAULT ((0))
   ,"CDREFEM2"                       integer NULL
   ,"CDEMAL3"                        nvarchar(256) NULL
   ,"CDETP3"                         numeric(3,0) NULL
   ,"CDDFLTEM3"                      bit NULL DEFAULT ((0))
   ,"CDREFEM3"                       integer NULL
   ,"CDGNNM"                         nvarchar(75) NULL
   ,"CDMDNM"                         nvarchar(25) NULL
   ,"CDSRNM"                         nvarchar(25) NULL
   ,"CDTITL"                         numeric(3,0) NULL
   ,"CDACTV"                         bit NULL
   ,"CDDFLT"                         bit NULL DEFAULT ((0))
   ,"CDSET"                          integer NULL
   ,"CDCRBY"                         nvarchar(25) NULL
   ,"CDCRDT"                         "datetime" NULL
   ,"CDUPBY"                         nvarchar(25) NULL
   ,"CDUPDT"                         "datetime" NULL
   ,PRIMARY KEY ("CDAN8" ASC,"CDID" ASC) 
)

END
/* TableDDL - [BUSDTA].[M0111] - End */

