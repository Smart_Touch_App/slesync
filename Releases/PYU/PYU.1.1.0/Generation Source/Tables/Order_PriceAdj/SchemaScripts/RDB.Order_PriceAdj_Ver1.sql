/* [BUSDTA].[Order_PriceAdj] - begins */

/* TableDDL - [BUSDTA].[Order_PriceAdj] - Start */
IF OBJECT_ID('[BUSDTA].[Order_PriceAdj]') IS NULL
BEGIN
	
CREATE TABLE "BUSDTA"."Order_PriceAdj" (
    "OrderID"                        numeric(8,0) NOT NULL
   ,"LineID"                         numeric(7,4) NOT NULL
   ,"AdjustmentSeq"                  numeric(4,0) NOT NULL
   ,"AdjName"                        nvarchar(20) NULL
   ,"AdjDesc"                        nvarchar(60) NULL
   ,"FactorValue"                    numeric(12,4) NULL
   ,"UnitPrice"                      numeric(12,4) NULL
   ,"OP"                             nvarchar(2) NULL
   ,"BC"                             nvarchar(2) NULL
   ,"BasicDesc"                      nvarchar(60) NULL
   ,"PriceAdjustmentKeyID"           numeric(8,0) NULL
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("OrderID" ASC,"LineID" ASC,"AdjustmentSeq" ASC) 
)


END
/* TableDDL - [BUSDTA].[Order_PriceAdj] - End */
