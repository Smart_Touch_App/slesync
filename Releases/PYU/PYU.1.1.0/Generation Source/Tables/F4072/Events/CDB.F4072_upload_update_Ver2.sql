
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4072"
SET "ADLITM" = {ml r."ADLITM"}, "ADAITM" = {ml r."ADAITM"}, "ADEFTJ" = {ml r."ADEFTJ"}, "ADBSCD" = {ml r."ADBSCD"}, "ADLEDG" = {ml r."ADLEDG"}, "ADFRMN" = {ml r."ADFRMN"}, "ADFVTR" = {ml r."ADFVTR"}, "ADFGY" = {ml r."ADFGY"}, "ADATID" = {ml r."ADATID"}, "ADNBRORD" = {ml r."ADNBRORD"}, "ADUOMVID" = {ml r."ADUOMVID"}, "ADFVUM" = {ml r."ADFVUM"}, "ADPARTFG" = {ml r."ADPARTFG"}, "ADAPRS" = {ml r."ADAPRS"}, "ADBKTPID" = {ml r."ADBKTPID"}, "ADCRCDVID" = {ml r."ADCRCDVID"}, "ADRULENAME" = {ml r."ADRULENAME"}
WHERE "ADAST" = {ml r."ADAST"} AND "ADITM" = {ml r."ADITM"} AND "ADAN8" = {ml r."ADAN8"} AND "ADIGID" = {ml r."ADIGID"} AND "ADCGID" = {ml r."ADCGID"} AND "ADOGID" = {ml r."ADOGID"} AND "ADCRCD" = {ml r."ADCRCD"} AND "ADUOM" = {ml r."ADUOM"} AND "ADMNQ" = {ml r."ADMNQ"} AND "ADEXDJ" = {ml r."ADEXDJ"} AND "ADUPMJ" = {ml r."ADUPMJ"} AND "ADTDAY" = {ml r."ADTDAY"}
 

