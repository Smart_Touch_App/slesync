/* [BUSDTA].[Prospect_Address] - begins */

/* TableDDL - [BUSDTA].[Prospect_Address] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Address]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Address]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [AddressLine1] NVARCHAR (40) NULL
	, [AddressLine2] NVARCHAR (40) NULL
	, [AddressLine3] NVARCHAR (40) NULL
	, [AddressLine4] NVARCHAR (40) NULL
	, [CityName] NVARCHAR (25) NULL
	, [StateName] NVARCHAR (25) NULL
	, [ZipCode] NVARCHAR (10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ProspectId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Prospect_Address] - End */
