/* [BUSDTA].[Prospect_Address] - begins */

/* TableDDL - [BUSDTA].[Prospect_Address] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Address]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Address]
	(
	  [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [AddressLine1] NCHAR(40) NULL
	, [AddressLine2] NCHAR(40) NULL
	, [AddressLine3] NCHAR(40) NULL
	, [AddressLine4] NCHAR(40) NULL
	, [CityName] NCHAR(25) NULL
	, [StateName] NCHAR(25) NULL
	, [ZipCode] NCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_ADDRESS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Address] PRIMARY KEY ([ProspectId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Address] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Address] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Address_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Address_del]
	(
	  [ProspectId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Address] - End */
/* TRIGGERS FOR Prospect_Address - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Address_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Address_ins
	ON BUSDTA.Prospect_Address AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Address_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Address_del.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Address_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Address_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Address_upd
	ON BUSDTA.Prospect_Address AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Address
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Address.ProspectId= inserted.ProspectId AND BUSDTA.Prospect_Address.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Address_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Address_dlt
	ON BUSDTA.Prospect_Address AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Address_del (ProspectId, RouteId, last_modified )
	SELECT deleted.ProspectId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Address - END */
