/* [dbo].[tblGroupTables] - begins */

/* TableDDL - [dbo].[tblGroupTables] - Start */
IF OBJECT_ID('[dbo].[tblGroupTables]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGroupTables]
	(
	  [GroupTableID] INTEGER NOT NULL DEFAULT autoincrement
	, [RefDBSchema] VARCHAR(128) NULL
	, [RefDBTable] VARCHAR(128) NULL
	, [ApplicationGroupID] INTEGER NULL
	, [ConDBTable] VARCHAR(128) NULL
	, [IsMandatorySync] BIT NULL

	, PRIMARY KEY ([GroupTableID] ASC)
	)
END
/* TableDDL - [dbo].[tblGroupTables] - End */
