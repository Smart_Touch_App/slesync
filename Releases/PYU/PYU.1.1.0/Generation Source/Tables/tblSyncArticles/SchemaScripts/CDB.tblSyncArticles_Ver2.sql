/* [dbo].[tblSyncArticles] - begins */

/* TableDDL - [dbo].[tblSyncArticles] - Start */
IF OBJECT_ID('[dbo].[tblSyncArticles]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblSyncArticles]
	(
	  [SyncArticleID] INT NOT NULL IDENTITY(1,1)
	, [PublicationName] VARCHAR(100) NULL
	, [SchemaName] VARCHAR(128) NULL
	, [TableName] VARCHAR(128) NULL
	, [ActiveYN] BIT NULL CONSTRAINT DF_TBLSYNCARTICLES_ActiveYN DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_EditedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLSYNCARTICLES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblSyncArticles] PRIMARY KEY ([SyncArticleID] ASC)
	)

END
/* TableDDL - [dbo].[tblSyncArticles] - End */

/* SHADOW TABLE FOR [dbo].[tblSyncArticles] - Start */
IF OBJECT_ID('[dbo].[tblSyncArticles_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblSyncArticles_del]
	(
	  [SyncArticleID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SyncArticleID] ASC)
	)

END
/* SHADOW TABLE FOR [dbo].[tblSyncArticles] - End */
/* TRIGGERS FOR tblSyncArticles - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblSyncArticles_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblSyncArticles_ins
	ON dbo.tblSyncArticles AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblSyncArticles_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblSyncArticles_del.SyncArticleID= inserted.SyncArticleID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblSyncArticles_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblSyncArticles_upd
	ON dbo.tblSyncArticles AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblSyncArticles
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblSyncArticles.SyncArticleID= inserted.SyncArticleID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblSyncArticles_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblSyncArticles_dlt
	ON dbo.tblSyncArticles AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblSyncArticles_del (SyncArticleID, last_modified )
	SELECT deleted.SyncArticleID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblSyncArticles - END */
