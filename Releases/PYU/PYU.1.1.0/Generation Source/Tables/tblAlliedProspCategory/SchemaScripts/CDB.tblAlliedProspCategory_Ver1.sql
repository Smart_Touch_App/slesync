/* [dbo].[tblAlliedProspCategory] - begins */

/* TableDDL - [dbo].[tblAlliedProspCategory] - Start */
IF OBJECT_ID('[dbo].[tblAlliedProspCategory]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblAlliedProspCategory]
	(
	  [CategoryID] INT NOT NULL IDENTITY(1,1)
	, [Category] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPCATEGORY_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPCATEGORY_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLALLIEDPROSPCATEGORY_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblAlliedProspCategory] PRIMARY KEY ([CategoryID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblAlliedProspCategory] - End */

/* SHADOW TABLE FOR [dbo].[tblAlliedProspCategory] - Start */
IF OBJECT_ID('[dbo].[tblAlliedProspCategory_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblAlliedProspCategory_del]
	(
	  [CategoryID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CategoryID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblAlliedProspCategory] - End */
/* TRIGGERS FOR tblAlliedProspCategory - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblAlliedProspCategory_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblAlliedProspCategory_ins
	ON dbo.tblAlliedProspCategory AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblAlliedProspCategory_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblAlliedProspCategory_del.CategoryID= inserted.CategoryID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblAlliedProspCategory_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblAlliedProspCategory_upd
	ON dbo.tblAlliedProspCategory AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblAlliedProspCategory
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblAlliedProspCategory.CategoryID= inserted.CategoryID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblAlliedProspCategory_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblAlliedProspCategory_dlt
	ON dbo.tblAlliedProspCategory AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblAlliedProspCategory_del (CategoryID, last_modified )
	SELECT deleted.CategoryID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblAlliedProspCategory - END */
