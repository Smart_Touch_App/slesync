
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M04012"
SET "PMALPH" = {ml r."PMALPH"}, "PMROUT" = {ml r."PMROUT"}, "PMSRS" = {ml r."PMSRS"}, "PMCRBY" = {ml r."PMCRBY"}, "PMCRDT" = {ml r."PMCRDT"}, "PMUPBY" = {ml r."PMUPBY"}, "PMUPDT" = {ml r."PMUPDT"}, "PMIBN" = {ml r."PMIBN"}, "PMISTP" = {ml r."PMISTP"}, "PMIBGP" = {ml r."PMIBGP"}, "PMILCT" = {ml r."PMILCT"}
WHERE "PMAN8" = {ml r."PMAN8"}
 

