
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."ExpenseDetails"
("ExpenseId", "RouteId", "TransactionId", "CategoryId", "ExpensesExplanation", "ExpenseAmount", "StatusId", "ExpensesDatetime", "VoidReasonId", "RouteSettlementId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ExpenseId"}, {ml r."RouteId"}, {ml r."TransactionId"}, {ml r."CategoryId"}, {ml r."ExpensesExplanation"}, {ml r."ExpenseAmount"}, {ml r."StatusId"}, {ml r."ExpensesDatetime"}, {ml r."VoidReasonId"}, {ml r."RouteSettlementId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 

