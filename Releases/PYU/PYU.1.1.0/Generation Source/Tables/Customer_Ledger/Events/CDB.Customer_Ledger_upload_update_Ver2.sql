
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Customer_Ledger"
SET "ReceiptID" = {ml r."ReceiptID"}, "InvoiceId" = {ml r."InvoiceId"}, "CustShipToId" = {ml r."CustShipToId"}, "IsActive" = {ml r."IsActive"}, "InvoiceGrossAmt" = {ml r."InvoiceGrossAmt"}, "InvoiceOpenAmt" = {ml r."InvoiceOpenAmt"}, "ReceiptUnAppliedAmt" = {ml r."ReceiptUnAppliedAmt"}, "ReceiptAppliedAmt" = {ml r."ReceiptAppliedAmt"}, "ConcessionAmt" = {ml r."ConcessionAmt"}, "ConcessionCodeId" = {ml r."ConcessionCodeId"}, "DateApplied" = {ml r."DateApplied"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}, "CustBillToId" = {ml r."CustBillToId"}
WHERE "CustomerLedgerId" = {ml r."CustomerLedgerId"} AND "RouteId" = {ml r."RouteId"}
 

