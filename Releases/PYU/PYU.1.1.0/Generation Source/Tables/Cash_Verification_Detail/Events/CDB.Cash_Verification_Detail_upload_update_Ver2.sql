 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Cash_Verification_Detail"
SET "SettlementDetailId" = {ml r."SettlementDetailId"}, "CashTypeId" = {ml r."CashTypeId"}, "Quantity" = {ml r."Quantity"}, "Amount" = {ml r."Amount"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CashVerificationDetailId" = {ml r."CashVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
 
