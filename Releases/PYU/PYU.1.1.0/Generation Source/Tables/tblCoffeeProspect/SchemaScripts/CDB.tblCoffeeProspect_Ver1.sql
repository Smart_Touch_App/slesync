/* [dbo].[tblCoffeeProspect] - begins */

/* TableDDL - [dbo].[tblCoffeeProspect] - Start */
IF OBJECT_ID('[dbo].[tblCoffeeProspect]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCoffeeProspect]
	(
	  [CoffeeProspectID] INT NOT NULL
	, [ProspectID] INT NOT NULL
	, [CoffeeBlendID] INT NULL
	, [CompetitorID] INT NULL
	, [UOMID] INT NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] NUMERIC(8,0) NULL
	, [UsageMeasurementID] INT NULL
	, [LiqCoffeeTypeID] INT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEPROSPECT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeProspect] PRIMARY KEY ([CoffeeProspectID] ASC,[ProspectID] ASC)
	)



END
GO
/* TableDDL - [dbo].[tblCoffeeProspect] - End */

/* SHADOW TABLE FOR [dbo].[tblCoffeeProspect] - Start */
IF OBJECT_ID('[dbo].[tblCoffeeProspect_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCoffeeProspect_del]
	(
	  [CoffeeProspectID] INT NOT NULL
	,[ProspectID] INT NOT NULL
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeProspectID] ASC,[ProspectID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblCoffeeProspect] - End */
/* TRIGGERS FOR tblCoffeeProspect - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblCoffeeProspect_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblCoffeeProspect_ins
	ON dbo.tblCoffeeProspect AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblCoffeeProspect_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblCoffeeProspect_del.CoffeeProspectID= inserted.CoffeeProspectID AND  dbo.tblCoffeeProspect_del.ProspectID= inserted.ProspectID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblCoffeeProspect_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblCoffeeProspect_upd
	ON dbo.tblCoffeeProspect AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblCoffeeProspect
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblCoffeeProspect.CoffeeProspectID= inserted.CoffeeProspectID AND dbo.tblCoffeeProspect.ProspectID= inserted.ProspectID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblCoffeeProspect_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblCoffeeProspect_dlt
	ON dbo.tblCoffeeProspect AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblCoffeeProspect_del (CoffeeProspectID,ProspectID, last_modified )
	SELECT deleted.CoffeeProspectID,ProspectID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeProspect - END */
