
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4092"
SET "GPDL01" = {ml r."GPDL01"}, "GPGPK1" = {ml r."GPGPK1"}, "GPGPK2" = {ml r."GPGPK2"}, "GPGPK3" = {ml r."GPGPK3"}, "GPGPK4" = {ml r."GPGPK4"}, "GPGPK5" = {ml r."GPGPK5"}, "GPGPK6" = {ml r."GPGPK6"}, "GPGPK7" = {ml r."GPGPK7"}, "GPGPK8" = {ml r."GPGPK8"}, "GPGPK9" = {ml r."GPGPK9"}, "GPGPK10" = {ml r."GPGPK10"}
WHERE "GPGPTY" = {ml r."GPGPTY"} AND "GPGPC" = {ml r."GPGPC"}
 
