DROP PUBLICATION IF EXISTS pub_immediate;
GO
/** Create publication 'pub_immediate'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_immediate" 

(
TABLE "BUSDTA"."CustomerConfig" ,
	TABLE "BUSDTA"."F0116" ,
	TABLE "BUSDTA"."F03012" ,
	TABLE "BUSDTA"."F4015" ,
	TABLE "BUSDTA"."M0111" ,
	TABLE "BUSDTA"."M0112" ,
	TABLE "BUSDTA"."M03042" ,
	TABLE "BUSDTA"."M04012" ,
	TABLE "BUSDTA"."M40111" ,
	TABLE "BUSDTA"."M40116" ,
	TABLE "BUSDTA"."M4016" ,
	TABLE "BUSDTA"."Order_Detail" ,
	TABLE "BUSDTA"."Order_Detail_BETA" ,
	TABLE "BUSDTA"."Order_Header" ,
	TABLE "BUSDTA"."Order_Header_BETA" ,
	TABLE "BUSDTA"."Order_PriceAdj" ,
	TABLE "BUSDTA"."Order_PriceAdj_BETA" ,
	TABLE "BUSDTA"."PickOrder" ,
	TABLE "BUSDTA"."PickOrder_Exception"    

)
GO

/** Create the user '{ml_userid}'. **/
IF NOT EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE site_name = '{ml_userid}') THEN
	CREATE SYNCHRONIZATION USER "{ml_userid}";
END IF
GO

/*DROP subscription*/
IF EXISTS (SELECT 1 FROM SYS.SYSSYNC WHERE subscription_name = 'subs_immediate') THEN
	DROP SYNCHRONIZATION SUBSCRIPTION "subs_immediate";
END IF
GO

/** Create subscription 'subs_immediate' to 'pub_immediate' for '{ml_userid}'. **/

CREATE SYNCHRONIZATION SUBSCRIPTION "subs_immediate" TO "pub_immediate" FOR "{ml_userid}" 
TYPE tcpip ADDRESS 'host=192.168.1.93;port=2439'
	OPTION lt='OFF'
	SCRIPT VERSION 'SLE_SyncScript_PYU.1.1.0'
GO


