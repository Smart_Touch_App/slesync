CREATE OR REPLACE FUNCTION "BUSDTA"."GetWeekForDate" (CalendarDate date, BaseDate date, WeeksPerCycle integer default 4)
RETURNS int
 BEGIN
	DECLARE DateDifference int;
	DECLARE varDiv7 float;
	DECLARE WeekNumber float;
	DECLARE ModNumber float;

    IF (WeeksPerCycle <> 0) THEN
    	select DATEDIFF(DAY,BaseDate,CalendarDate)+1 into DateDifference from dummy;
    	select CEILING (DateDifference / 7.00) into varDiv7 from dummy;
    	select varDiv7%WeeksPerCycle into ModNumber from dummy;
     	IF (ModNumber = 0) THEN
    		set WeekNumber = WeeksPerCycle;
    	ELSE
    		set WeekNumber = ModNumber;
    	END IF;
    END IF;
    RETURN cast(WeekNumber as int)
END
GO