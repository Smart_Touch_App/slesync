

CREATE PROCEDURE [dbo].[uspDeltaRows] 
(
	@remote_id VARCHAR(100),
	@script_version VARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON;
/*BUSDTA.F0101*/
		SELECT ABMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F0101' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0101 AS F0101 
		WHERE F0101.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0101' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY ABMCU
	UNION ALL
/*BUSDTA.F40073*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F40073' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F40073 AS F40073
		WHERE F40073.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F40073' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0004*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0004' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0004 AS F0004
		WHERE F0004.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0004' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0005*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0005' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0005 AS F0005
		WHERE F0005.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0005' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0014*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0014' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0014 AS F0014 
		WHERE F0014.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0014' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0150*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0150' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0150 AS F0150
		WHERE F0150.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0150' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.M0140*/
		SELECT M0140.RMMCU AS RouteID, 'BUSDTA' AS CTSchema, 'M0140' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.M0140 AS M0140
		WHERE M0140.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='M0140' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY M0140.RMMCU
	UNION ALL
/*BUSDTA.F0006*/
		SELECT F0006.MCMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F0006' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0006 AS F0006
		WHERE F0006.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0006' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F0006.MCMCU
	UNION ALL
		--Pricing
		--Product
/*BUSDTA.F4106*/
		SELECT F4106.BPMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F4106' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F4106 AS F4106
		WHERE F4106.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F4106' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F4106.BPMCU
	UNION ALL
/*BUSDTA.F41001*/
		SELECT F41001.CIMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F41001' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F41001 AS F41001
		WHERE F41001.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F41001' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F41001.CIMCU
	UNION ALL 
/*BUSDTA.F4105*/
		SELECT F4105.COMCU AS RouteID, 'BUSDTA' AS CTSchema, 'F4105' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F4105 AS F4105
		WHERE F4105.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F4105' AND remote_id=@remote_id AND script_version=@script_version)
		GROUP BY F4105.COMCU

		--Customer
	UNION ALL
/*BUSDTA.F0116*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F0116' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F0116 AS F0116 
		WHERE F0116.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F0116' AND remote_id=@remote_id AND script_version=@script_version)
	UNION ALL
/*BUSDTA.F0116*/
		SELECT NULL AS RouteID, 'BUSDTA' AS CTSchema, 'F03012' AS CTTable,COUNT(1) AS CTDeltaRows
		FROM BUSDTA.F03012 AS F03012
		WHERE F03012.last_modified>=
		(SELECT last_upload_time FROM [vwArticles] WHERE SchemaName='BUSDTA' AND TableName='F03012' AND remote_id=@remote_id AND script_version=@script_version)
SET NOCOUNT OFF;
END



GO


