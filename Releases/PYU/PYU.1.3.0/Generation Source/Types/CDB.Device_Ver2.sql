CREATE TYPE Device AS TABLE
(
	DeviceID VARCHAR(30),
	Manufacturer NVARCHAR(50),
	Model NVARCHAR(50)
);