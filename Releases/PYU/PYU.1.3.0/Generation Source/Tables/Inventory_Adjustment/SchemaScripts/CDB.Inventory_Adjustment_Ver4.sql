
/* [BUSDTA].[Inventory_Adjustment] - begins */

/* TableDDL - [BUSDTA].[Inventory_Adjustment] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Adjustment]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Adjustment]
	(
	  [InventoryAdjustmentId] DECIMAL(15,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionQty] DECIMAL(15,0) NULL
	, [TransactionQtyUM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUM] NCHAR(2) NULL
	, [ReasonCode] NUMERIC(3,0) NOT NULL
	, [IsApproved] BIT NULL CONSTRAINT DF_INVENTORY_ADJUSTMENT_IsApproved DEFAULT((0))
	, [IsApplied] BIT NULL CONSTRAINT DF_INVENTORY_ADJUSTMENT_IsApplied DEFAULT((0))
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [AdjustmentStatus] NUMERIC(3,0) NULL
	, [Source] NUMERIC(3,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_INVENTORY_ADJUSTMENT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Inventory_Adjustment] PRIMARY KEY ([InventoryAdjustmentId] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Inventory_Adjustment] - End */

/* SHADOW TABLE FOR [BUSDTA].[Inventory_Adjustment] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Adjustment_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Adjustment_del]
	(
	  [InventoryAdjustmentId] DECIMAL(15,0)
	, [ItemId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([InventoryAdjustmentId] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Inventory_Adjustment] - End */
/* TRIGGERS FOR Inventory_Adjustment - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Inventory_Adjustment_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Inventory_Adjustment_ins
	ON BUSDTA.Inventory_Adjustment AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Inventory_Adjustment_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Inventory_Adjustment_del.InventoryAdjustmentId= inserted.InventoryAdjustmentId AND BUSDTA.Inventory_Adjustment_del.ItemId= inserted.ItemId AND BUSDTA.Inventory_Adjustment_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Inventory_Adjustment_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Adjustment_upd
	ON BUSDTA.Inventory_Adjustment AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Inventory_Adjustment
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Inventory_Adjustment.InventoryAdjustmentId= inserted.InventoryAdjustmentId AND BUSDTA.Inventory_Adjustment.ItemId= inserted.ItemId AND BUSDTA.Inventory_Adjustment.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Inventory_Adjustment_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Adjustment_dlt
	ON BUSDTA.Inventory_Adjustment AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Inventory_Adjustment_del (InventoryAdjustmentId, ItemId, RouteId, last_modified )
	SELECT deleted.InventoryAdjustmentId, deleted.ItemId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Inventory_Adjustment - END */

