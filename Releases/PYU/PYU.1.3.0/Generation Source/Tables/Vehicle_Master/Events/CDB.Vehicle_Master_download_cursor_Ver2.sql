

SELECT "BUSDTA"."Vehicle_Master"."VehicleID"
,"BUSDTA"."Vehicle_Master"."VINNumber"
,"BUSDTA"."Vehicle_Master"."VehicleNumber"
,"BUSDTA"."Vehicle_Master"."VehicleMake"
,"BUSDTA"."Vehicle_Master"."VehicleColour"
,"BUSDTA"."Vehicle_Master"."VehicleAxle"
,"BUSDTA"."Vehicle_Master"."VehicleFuel"
,"BUSDTA"."Vehicle_Master"."VehicleMilage"
,"BUSDTA"."Vehicle_Master"."Isactive"
,"BUSDTA"."Vehicle_Master"."VehicleManufacturingDt"
,"BUSDTA"."Vehicle_Master"."VehicleExpiryDt"
,"BUSDTA"."Vehicle_Master"."VehicleOwner"
,"BUSDTA"."Vehicle_Master"."CreatedBy"
,"BUSDTA"."Vehicle_Master"."CreatedDatetime"
,"BUSDTA"."Vehicle_Master"."UpdatedBy"
,"BUSDTA"."Vehicle_Master"."UpdatedDatetime"

FROM "BUSDTA"."Vehicle_Master"
WHERE "BUSDTA"."Vehicle_Master"."last_modified">= {ml s.last_table_download}