
SELECT "LiqCoffeeTypeID"
,"LiqCoffeeType"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."tblLiqCoffeeType"
WHERE "BUSDTA"."tblLiqCoffeeType"."last_modified">= {ml s.last_table_download}
