
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M50052"
SET "SDKEY" = {ml r."SDKEY"}, "SDISYNCD" = {ml r."SDISYNCD"}, "SDTMSTMP" = {ml r."SDTMSTMP"}, "SDCRBY" = {ml r."SDCRBY"}, "SDCRDT" = {ml r."SDCRDT"}, "SDUPBY" = {ml r."SDUPBY"}, "SDUPDT" = {ml r."SDUPDT"}
WHERE "SDID" = {ml r."SDID"} AND "SDTXID" = {ml r."SDTXID"}
 

