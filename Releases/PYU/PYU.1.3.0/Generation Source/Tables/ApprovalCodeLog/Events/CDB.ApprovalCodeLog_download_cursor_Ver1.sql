

SELECT "BUSDTA"."ApprovalCodeLog"."LogID"  
,"BUSDTA"."ApprovalCodeLog"."RouteID"  
,"BUSDTA"."ApprovalCodeLog"."ApprovalCode"  
,"BUSDTA"."ApprovalCodeLog"."RequestCode"  
,"BUSDTA"."ApprovalCodeLog"."Feature"  
,"BUSDTA"."ApprovalCodeLog"."Amount"  
,"BUSDTA"."ApprovalCodeLog"."RequestedByUser"  
,"BUSDTA"."ApprovalCodeLog"."RequestedForCustomer"  
,"BUSDTA"."ApprovalCodeLog"."Payload" 
,"BUSDTA"."ApprovalCodeLog"."CreatedBy"  
,"BUSDTA"."ApprovalCodeLog"."CreatedDatetime"  
,"BUSDTA"."ApprovalCodeLog"."UpdatedBy"  
,"BUSDTA"."ApprovalCodeLog"."UpdatedDatetime"   

FROM "BUSDTA"."ApprovalCodeLog"  
WHERE "BUSDTA"."ApprovalCodeLog"."last_modified">= {ml s.last_table_download}  
AND "BUSDTA"."ApprovalCodeLog"."RouteId" = 
(select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})           