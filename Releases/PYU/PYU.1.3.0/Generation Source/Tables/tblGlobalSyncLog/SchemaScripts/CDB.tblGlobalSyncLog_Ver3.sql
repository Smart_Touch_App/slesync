

/* TableDDL - [BUSDTA].[tblGlobalSyncLog] - Start */
IF OBJECT_ID('[BUSDTA].[tblGlobalSyncLog]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblGlobalSyncLog]
	(
	  [GlobalSyncLogID] INT NOT NULL IDENTITY(1,1)
	, [GlobalSyncID] INT NULL
	, [DeviceID] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLGLOBALSYNCLOG_CreatedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLGLOBALSYNCLOG_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGlobalSyncLog] PRIMARY KEY ([GlobalSyncLogID] ASC)
	)

ALTER TABLE [BUSDTA].[tblGlobalSyncLog] WITH CHECK ADD CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync] FOREIGN KEY([GlobalSyncID]) REFERENCES [BUSDTA].[tblGlobalSync] ([GlobalSyncID])
ALTER TABLE [BUSDTA].[tblGlobalSyncLog] CHECK CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync]

END
/* TableDDL - [BUSDTA].[tblGlobalSyncLog] - End */
