
/* [BUSDTA].[Device_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[Device_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Environment_Map]
	(
	  [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [DeviceId] VARCHAR(30) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, PRIMARY KEY ([EnvironmentMasterId] ASC, [DeviceId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Device_Environment_Map] - End */
