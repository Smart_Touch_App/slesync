/* [BUSDTA].[Cash_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Verification_Detail]
	(
	  [CashVerificationDetailId] NUMERIC(15,0) NOT NULL DEFAULT autoincrement
	, [SettlementDetailId] NUMERIC(15,0) NOT NULL
	, [CashTypeId] NUMERIC(8,0) NOT NULL
	, [Quantity] NUMERIC(15,0) NULL
	, [Amount] NUMERIC(15,4) NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CreatedBy] NUMERIC(18,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(18,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([CashVerificationDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - End */
