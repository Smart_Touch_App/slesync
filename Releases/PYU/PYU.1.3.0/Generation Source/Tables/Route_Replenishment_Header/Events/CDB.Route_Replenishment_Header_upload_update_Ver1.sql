 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Replenishment_Header"
SET "StatusId" = {ml r."StatusId"}, "ReplenishmentTypeId" = {ml r."ReplenishmentTypeId"}, "TransDateFrom" = {ml r."TransDateFrom"}, "TransDateTo" = {ml r."TransDateTo"}, "RequestedBy" = {ml r."RequestedBy"}, "FromBranchId" = {ml r."FromBranchId"}, "ToBranchId" = {ml r."ToBranchId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ReplenishmentID" = {ml r."ReplenishmentID"} AND "RouteId" = {ml r."RouteId"}
