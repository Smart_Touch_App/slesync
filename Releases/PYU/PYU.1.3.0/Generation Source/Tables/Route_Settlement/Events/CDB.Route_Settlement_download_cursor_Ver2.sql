
SELECT "BUSDTA"."Route_Settlement"."SettlementID"
,"BUSDTA"."Route_Settlement"."SettlementNO"
,"BUSDTA"."Route_Settlement"."RouteId"
,"BUSDTA"."Route_Settlement"."Status"
,"BUSDTA"."Route_Settlement"."SettlementDateTime"
,"BUSDTA"."Route_Settlement"."UserId"
,"BUSDTA"."Route_Settlement"."Originator"
,"BUSDTA"."Route_Settlement"."Verifier"
,"BUSDTA"."Route_Settlement"."SettlementAmount"
,"BUSDTA"."Route_Settlement"."ExceptionAmount"
,"BUSDTA"."Route_Settlement"."Comment"
,"BUSDTA"."Route_Settlement"."OriginatingRoute"
,"BUSDTA"."Route_Settlement"."partitioningRoute"
,"BUSDTA"."Route_Settlement"."CreatedBy"
,"BUSDTA"."Route_Settlement"."CreatedDatetime"
,"BUSDTA"."Route_Settlement"."UpdatedBy"
,"BUSDTA"."Route_Settlement"."UpdatedDatetime"

FROM "BUSDTA"."Route_Settlement"
WHERE "BUSDTA"."Route_Settlement"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
