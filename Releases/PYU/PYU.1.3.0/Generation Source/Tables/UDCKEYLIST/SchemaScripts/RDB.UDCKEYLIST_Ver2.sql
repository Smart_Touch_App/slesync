/* [BUSDTA].[UDCKEYLIST] - begins */

/* TableDDL - [BUSDTA].[UDCKEYLIST] - Start */
IF OBJECT_ID('[BUSDTA].[UDCKEYLIST]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UDCKEYLIST]
	(
	  [DTSY] NVARCHAR (4) NOT NULL
	, [DTRT] NVARCHAR (2) NOT NULL
	, PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)
END
/* TableDDL - [BUSDTA].[UDCKEYLIST] - End */
