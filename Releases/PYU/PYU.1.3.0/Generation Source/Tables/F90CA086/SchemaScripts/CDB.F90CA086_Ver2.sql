/* [BUSDTA].[F90CA086] - begins */

/* TableDDL - [BUSDTA].[F90CA086] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA086]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA086]
	(
	  [CRCUAN8] NUMERIC(8,0) NOT NULL
	, [CRCRAN8] NUMERIC(8,0) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F90CA086_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F90CA086] PRIMARY KEY ([CRCUAN8] ASC, [CRCRAN8] ASC)
	)

END
/* TableDDL - [BUSDTA].[F90CA086] - End */

/* SHADOW TABLE FOR [BUSDTA].[F90CA086] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA086_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA086_del]
	(
	  [CRCUAN8] NUMERIC(8,0)
	, [CRCRAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CRCUAN8] ASC, [CRCRAN8] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[F90CA086] - End */
/* TRIGGERS FOR F90CA086 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F90CA086_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F90CA086_ins
	ON BUSDTA.F90CA086 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F90CA086_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F90CA086_del.CRCRAN8= inserted.CRCRAN8 AND BUSDTA.F90CA086_del.CRCUAN8= inserted.CRCUAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F90CA086_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA086_upd
	ON BUSDTA.F90CA086 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F90CA086
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F90CA086.CRCRAN8= inserted.CRCRAN8 AND BUSDTA.F90CA086.CRCUAN8= inserted.CRCUAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F90CA086_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA086_dlt
	ON BUSDTA.F90CA086 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F90CA086_del (CRCRAN8, CRCUAN8, last_modified )
	SELECT deleted.CRCRAN8, deleted.CRCUAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F90CA086 - END */
