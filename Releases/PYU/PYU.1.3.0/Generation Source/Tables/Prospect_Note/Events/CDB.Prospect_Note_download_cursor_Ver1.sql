SELECT "BUSDTA"."Prospect_Note"."ProspectId"
,"BUSDTA"."Prospect_Note"."ProspectNoteId"
,"BUSDTA"."Prospect_Note"."RouteId"
,"BUSDTA"."Prospect_Note"."ProspectNoteType"
,"BUSDTA"."Prospect_Note"."ProspectNoteDetail"
,"BUSDTA"."Prospect_Note"."IsDefault"
,"BUSDTA"."Prospect_Note"."CreatedBy"
,"BUSDTA"."Prospect_Note"."CreatedDatetime"
,"BUSDTA"."Prospect_Note"."UpdatedBy"
,"BUSDTA"."Prospect_Note"."UpdatedDatetime"

FROM "BUSDTA"."Prospect_Note"
WHERE "BUSDTA"."Prospect_Note"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Prospect_Note"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username}) 