
/* [BUSDTA].[Customer_Quote_Detail] - begins */

/* TableDDL - [BUSDTA].[Customer_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Quote_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Quote_Detail]
	(
	  [CustomerQuoteId] NUMERIC(8,0) NOT NULL
	, [CustomerQuoteDetailId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [QuoteQty] decimal (15,0) NOT NULL
	, [TransactionUM] NVARCHAR (2) NULL
	, [PricingAmt] decimal (15,4) NULL
	, [PricingUM] NVARCHAR (2) NULL
	, [TransactionAmt] decimal (15,4) NULL
	, [OtherAmt] decimal (15,4) NULL
	, [OtherUM] NVARCHAR (2) NULL
	, [CompetitorName] NVARCHAR(50) NOT NULL
	, [UnitPriceAmt] decimal (15,4) NULL
	, [UnitPriceUM] NVARCHAR (2) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([CustomerQuoteId] ASC, [CustomerQuoteDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Customer_Quote_Detail] - End */

