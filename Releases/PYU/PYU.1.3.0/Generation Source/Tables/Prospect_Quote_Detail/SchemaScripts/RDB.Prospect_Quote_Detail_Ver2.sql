/* [BUSDTA].[Prospect_Quote_Detail] - begins */

/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Detail]
	(
	  [ProspectQuoteId] decimal (15,0) NOT NULL
	, [ProspectQuoteDetailId] decimal (15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [QuoteQty] decimal (15,0) NOT NULL
	, [SampleQty] decimal (15,0) NOT NULL
	, [SampleUM] NVARCHAR (5) NULL
	, [PricingAmt] decimal (15,4) NULL
	, [PricingUM] NVARCHAR (2) NULL
	, [TransactionAmt] decimal (15,4) NULL
	, [TransactionUM] NVARCHAR (2) NULL
	, [OtherAmt] decimal (15,4) NULL
	, [OtherUM] NVARCHAR (2) NULL
	, [CompetitorName] NVARCHAR(50) NOT NULL
	, [UnitPriceAmt] decimal (15,4) NULL
	, [UnitPriceUM] NVARCHAR (2) NULL
	, [AvailableQty] decimal (15,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ProspectQuoteId] ASC, [ProspectQuoteDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Prospect_Quote_Detail] - End */
