
     SELECT "BUSDTA"."ItemConfiguration"."ItemID"  ,"BUSDTA"."ItemConfiguration"."RouteEnabled"  ,"BUSDTA"."ItemConfiguration"."Sellable"  ,
     "BUSDTA"."ItemConfiguration"."AllowSearch"  ,"BUSDTA"."ItemConfiguration"."PrimaryUM"  ,"BUSDTA"."ItemConfiguration"."PricingUM"  ,
     "BUSDTA"."ItemConfiguration"."TransactionUM"  ,"BUSDTA"."ItemConfiguration"."OtherUM1"  ,"BUSDTA"."ItemConfiguration"."OtherUM2"  ,
     "BUSDTA"."ItemConfiguration"."AllowLooseSample"  ,"BUSDTA"."ItemConfiguration"."CreatedBy"  ,"BUSDTA"."ItemConfiguration"."CreatedDatetime"  ,
     "BUSDTA"."ItemConfiguration"."UpdatedBy"  ,"BUSDTA"."ItemConfiguration"."UpdatedDatetime"    
     FROM "BUSDTA"."ItemConfiguration", "BUSDTA"."F4102", BUSDTA.Route_Master rm  
     WHERE ("BUSDTA"."ItemConfiguration"."last_modified">= {ml s.last_table_download}
      or "BUSDTA"."F4102"."last_modified">= {ml s.last_table_download}   or rm.last_modified>= {ml s.last_table_download})   
      AND ItemID=IBITM AND IBMCU = rm.BranchNumber   
      AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )     