/* [BUSDTA].[User_Role_Map] - begins */

/* TableDDL - [BUSDTA].[User_Role_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Role_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Role_Map]
	(
	  [App_user_id] INT NOT NULL
	, [Role] VARCHAR(8) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_USER_ROLE_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_User_Role_Map] PRIMARY KEY ([App_user_id] ASC)
	)

END
/* TableDDL - [BUSDTA].[User_Role_Map] - End */
GO
/* SHADOW TABLE FOR [BUSDTA].[User_Role_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Role_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Role_Map_del]
	(
	  [App_user_id] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([App_user_id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[User_Role_Map] - End */
/* TRIGGERS FOR User_Role_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.User_Role_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.User_Role_Map_ins
	ON BUSDTA.User_Role_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.User_Role_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.User_Role_Map_del.App_user_id= inserted.App_user_id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.User_Role_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Role_Map_upd
	ON BUSDTA.User_Role_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.User_Role_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.User_Role_Map.App_user_id= inserted.App_user_id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.User_Role_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Role_Map_dlt
	ON BUSDTA.User_Role_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.User_Role_Map_del (App_user_id, last_modified )
	SELECT deleted.App_user_id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR User_Role_Map - END */
