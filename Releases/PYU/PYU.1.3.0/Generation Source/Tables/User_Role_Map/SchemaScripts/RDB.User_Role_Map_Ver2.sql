/* [BUSDTA].[User_Role_Map] - begins */

/* TableDDL - [BUSDTA].[User_Role_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Role_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Role_Map]
	(
	  [App_user_id] INTEGER NOT NULL
	, [Role] VARCHAR(8) NULL
	, PRIMARY KEY ([App_user_id] ASC)
	)
END
/* TableDDL - [BUSDTA].[User_Role_Map] - End */
