
/* [BUSDTA].[Order_Header_BETA] - begins */

/* TableDDL - [BUSDTA].[Order_Header_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_BETA]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_Header_BETA]	(	  [OrderID] NUMERIC(8,0) NOT NULL	, [OrderTypeId] NUMERIC(3,0) NULL	, [OriginatingRouteID] NUMERIC(8,0) NOT NULL	, [Branch] NVARCHAR(12) NULL	, [CustShipToId] NUMERIC(8,0) NOT NULL	, [CustBillToId] NUMERIC(8,0) NOT NULL	, [CustParentId] NUMERIC(8,0) NOT NULL	, [OrderDate] DATE NULL	, [TotalCoffeeAmt] NUMERIC(12,4) NULL	, [TotalAlliedAmt] NUMERIC(12,4) NULL	, [OrderTotalAmt] NUMERIC(12,4) NULL	, [SalesTaxAmt] NUMERIC(12,4) NULL	, [InvoiceTotalAmt] NUMERIC(12,4) NULL	, [EnergySurchargeAmt] NUMERIC(12,4) NULL	, [VarianceAmt] NUMERIC(12,4) NULL	, [SurchargeReasonCodeId] NUMERIC(3,0) NULL	, [OrderStateId] NUMERIC(3,0) NULL	, [OrderSignature] VARBINARY(MAX) NULL	, [ChargeOnAccountSignature] VARBINARY(MAX) NULL	, [JDEOrderCompany] NVARCHAR(5) NULL	, [JDEOrderNumber] NUMERIC(8,0) NULL	, [JDEOrderType] NVARCHAR(2) NULL	, [JDEInvoiceCompany] NVARCHAR(5) NULL	, [JDEInvoiceNumber] NUMERIC(8,0) NULL	, [JDEOInvoiceType] NVARCHAR(2) NULL	, [JDEZBatchNumber] NVARCHAR(15) NULL	, [JDEProcessID] NUMERIC(3,0) NULL	, [SettlementID] NUMERIC(8,0) NULL	, [PaymentTerms] NVARCHAR(3) NULL	, [CustomerReference1] NVARCHAR(25) NULL	, [CustomerReference2] NVARCHAR(25) NULL	, [AdjustmentSchedule] NVARCHAR(8) NULL	, [TaxArea] NVARCHAR(10) NULL	, [TaxExplanationCode] NVARCHAR(1) NULL	, [VoidReasonCodeId] NUMERIC(3,0) NULL	, [ChargeOnAccount] NVARCHAR(1) NULL	, [HoldCommitted] NVARCHAR(1) NULL	, [CreatedBy] NUMERIC(8,0) NULL	, [CreatedDatetime] DATETIME NULL	, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL	, [Last_Modified] DATETIME null default getdate()	, CONSTRAINT [PK_Order_Header_BETA] PRIMARY KEY ([OrderID] ASC)	)END
/* TableDDL - [BUSDTA].[Order_Header_BETA] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Header_BETA] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_BETA_del]') IS NULL
BEGIN
	CREATE TABLE [BUSDTA].[Order_Header_BETA_del]	(	  [OrderID] NUMERIC(8,0)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([OrderID] ASC)	)END
/* SHADOW TABLE FOR [BUSDTA].[Order_Header_BETA] - End */
/* TRIGGERS FOR Order_Header_BETA - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Header_BETA_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Header_BETA_ins
	ON BUSDTA.Order_Header_BETA AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Header_BETA_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Header_BETA_del.OrderID= inserted.OrderID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Header_BETA_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_BETA_upd
	ON BUSDTA.Order_Header_BETA AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Header_BETA
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Header_BETA.OrderID= inserted.OrderID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Header_BETA_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_BETA_dlt
	ON BUSDTA.Order_Header_BETA AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Header_BETA_del (OrderID, last_modified )
	SELECT deleted.OrderID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Header_BETA - END */
