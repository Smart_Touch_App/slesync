/* [BUSDTA].[Status_Type] - begins */

/* TableDDL - [BUSDTA].[Status_Type] - Start */
IF OBJECT_ID('[BUSDTA].[Status_Type]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Status_Type]
	(
	  [StatusTypeID] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [StatusTypeCD] VARCHAR(50) NOT NULL
	, [StatusTypeDESC] NVARCHAR (50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([StatusTypeCD] ASC)
	)
END
/* TableDDL - [BUSDTA].[Status_Type] - End */
