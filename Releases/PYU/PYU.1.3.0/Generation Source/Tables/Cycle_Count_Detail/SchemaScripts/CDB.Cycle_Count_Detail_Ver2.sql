
/* [BUSDTA].[Cycle_Count_Detail] - begins */

/* TableDDL - [BUSDTA].[Cycle_Count_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Detail]
	(
	  [CycleCountID] DECIMAL(15,0) NOT NULL
	, [CycleCountDetailID] DECIMAL(15,0) NOT NULL
	, [ReCountNumber] NUMERIC(3,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [CountedQty] DECIMAL(15,0) NOT NULL
	, [ItemStatus] NCHAR(1) NULL
	, [OnHandQty] DECIMAL(15,0) NULL
	, [HeldQty] DECIMAL(15,0) NULL
	, [CountAccepted] NCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CYCLE_COUNT_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Cycle_Count_Detail] PRIMARY KEY ([CycleCountID] ASC, [CycleCountDetailID] ASC, [ReCountNumber] ASC, [ItemId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Cycle_Count_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cycle_Count_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cycle_Count_Detail_del]
	(
	  [CycleCountID] DECIMAL(15,0)
	, [CycleCountDetailID] DECIMAL(15,0)
	, [ReCountNumber] NUMERIC(3,0)
	, [ItemId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CycleCountID] ASC, [CycleCountDetailID] ASC, [ReCountNumber] ASC, [ItemId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Cycle_Count_Detail] - End */
/* TRIGGERS FOR Cycle_Count_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Cycle_Count_Detail_ins
	ON BUSDTA.Cycle_Count_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Cycle_Count_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Cycle_Count_Detail_del.CycleCountDetailID= inserted.CycleCountDetailID AND BUSDTA.Cycle_Count_Detail_del.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Detail_del.ItemId= inserted.ItemId AND BUSDTA.Cycle_Count_Detail_del.ReCountNumber= inserted.ReCountNumber
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Cycle_Count_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Detail_upd
	ON BUSDTA.Cycle_Count_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Cycle_Count_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Cycle_Count_Detail.CycleCountDetailID= inserted.CycleCountDetailID AND BUSDTA.Cycle_Count_Detail.CycleCountID= inserted.CycleCountID AND BUSDTA.Cycle_Count_Detail.ItemId= inserted.ItemId AND BUSDTA.Cycle_Count_Detail.ReCountNumber= inserted.ReCountNumber');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Cycle_Count_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Cycle_Count_Detail_dlt
	ON BUSDTA.Cycle_Count_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Cycle_Count_Detail_del (CycleCountDetailID, CycleCountID, ItemId, ReCountNumber, last_modified )
	SELECT deleted.CycleCountDetailID, deleted.CycleCountID, deleted.ItemId, deleted.ReCountNumber, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Cycle_Count_Detail - END */

