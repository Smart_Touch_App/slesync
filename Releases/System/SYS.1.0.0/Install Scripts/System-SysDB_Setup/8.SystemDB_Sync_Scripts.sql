
/* Table :Device_Environment_Map */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.Device_Environment_Map',
	'download_cursor',

'

SELECT "EnvironmentMasterId"
,"DeviceId"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"

FROM "BUSDTA"."Device_Environment_Map"
WHERE "BUSDTA"."Device_Environment_Map"."last_modified">= {drh s.last_table_download}
'

/* download_cursor - End */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.Device_Environment_Map',
	'download_delete_cursor',

'

 
SELECT "EnvironmentMasterId"
,"DeviceId"

FROM "BUSDTA"."Device_Environment_Map_del"
WHERE "BUSDTA"."Device_Environment_Map_del"."last_modified">= {drh s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :Device_Master */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.Device_Master',
	'download_cursor',

'

SELECT "Device_Id"
,"DeviceName"
,"Active"
,"manufacturer"
,"model"
FROM "BUSDTA"."Device_Master"
WHERE "BUSDTA"."Device_Master"."last_modified">= {drh s.last_table_download}
'

/* download_cursor - End */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.Device_Master',
	'download_delete_cursor',

'

 
SELECT "Device_Id"

FROM "BUSDTA"."Device_Master_del"
WHERE "BUSDTA"."Device_Master_del"."last_modified">= {drh s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :Environment_Master */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.Environment_Master',
	'download_cursor',

'

 
SELECT "EnvironmentMasterId"
,"EnvName"
,"EnvDescription"
,"IsActive"
,"ReleaseLevel"
,"MLServerAddress"
,"JDEWSAddress"
,"REFDBAddress"
,"ENVDBAddress"
,"CDBAddress"
,"CDBConKey"
,"IISAddress"
,"AppInstallerLocation"
,"RDBScriptLocation"
,"TaxLibLocation"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."Environment_Master"
WHERE "BUSDTA"."Environment_Master"."last_modified">= {drh s.last_table_download}
 
'

/* download_cursor - End */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.Environment_Master',
	'download_delete_cursor',

'

SELECT "EnvironmentMasterId"

FROM "BUSDTA"."Environment_Master_del"
WHERE "BUSDTA"."Environment_Master_del"."last_modified">= {drh s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :RemoteDBID_Master */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.RemoteDBID_Master',
	'',

'
--{drh_ignore}
'

/*  - End */

/* Table :Reserve_DeviceEnvironmentRoute */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.Reserve_DeviceEnvironmentRoute',
	'download_cursor',

'

 
SELECT "ReservationID"
,"DeviceId"
,"EnvironmentMasterId"
,"RouteID"
,"AppUserId"
,"ReservedDateTime"

FROM "BUSDTA"."Reserve_DeviceEnvironmentRoute"
WHERE "BUSDTA"."Reserve_DeviceEnvironmentRoute"."last_modified">= {drh s.last_table_download}
 

'

/* download_cursor - End */

/* Table :User_Environment_Map */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.User_Environment_Map',
	'download_cursor',

'

 
SELECT "UserEnvironmentMapId"
,"EnvironmentMasterId"
,"AppUserId"
,"IsEnvAdmin"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"

FROM "BUSDTA"."User_Environment_Map"
WHERE "BUSDTA"."User_Environment_Map"."last_modified">= {drh s.last_table_download}
 
'

/* download_cursor - End */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.User_Environment_Map',
	'download_delete_cursor',

'
 
 
SELECT "UserEnvironmentMapId"

FROM "BUSDTA"."User_Environment_Map_del"
WHERE "BUSDTA"."User_Environment_Map_del"."last_modified">= {drh s.last_table_download}
'

/* download_delete_cursor - End */

/* Table :user_master */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.user_master',
	'download_cursor',

'

SELECT "App_user_id"
,"App_User"
,"Name"
,"DomainUser"
,"AppPassword"
,"active"

FROM "BUSDTA"."user_master"
WHERE "BUSDTA"."user_master"."last_modified">= {drh s.last_table_download}
'

/* download_cursor - End */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'BUSDTA.user_master',
	'download_delete_cursor',

'

SELECT "App_user_id"

FROM "BUSDTA"."user_master_del"
WHERE "BUSDTA"."user_master_del"."last_modified">= {drh s.last_table_download}
 

'

/* download_delete_cursor - End */

/* Table :drh_script_version */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'drh_script_version.drh_script_version',
	'',

'
--{drh_ignore}
'

/*  - End */

/* Table :drh_table */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'drh_table.drh_table',
	'',

'
--{drh_ignore}
'

/*  - End */

/* Table :drh_script */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'drh_script.drh_script',
	'',

'
--{drh_ignore}
'

/*  - End */

/* Table :tblReserveConfig */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'tblReserveConfig.tblReserveConfig',
	'',

'
--{drh_ignore}
'

/*  - End */

/* Table :tblServiceResponses */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'tblServiceResponses.tblServiceResponses',
	'',

'
--{drh_ignore}
'

/*  - End */

/* Table :tblWebService_ReleaseMaster */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'tblWebService_ReleaseMaster.tblWebService_ReleaseMaster',
	'',

'
--{drh_ignore}
'

/*  - End */

/* Table :tblWebServiceLog */
/* Version : SLE_SyncScript_SYS.1.0.0 */


/* SLE_SyncScript_SYS.1.0.0 - Start */
EXEC drh_add_table_script
    'SLE_SyncScript_SYS.1.0.0',
	'tblWebServiceLog.tblWebServiceLog',
	'',

'
--{drh_ignore}
'

/*  - End */
