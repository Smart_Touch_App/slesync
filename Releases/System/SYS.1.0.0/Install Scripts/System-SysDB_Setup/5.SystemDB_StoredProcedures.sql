CREATE PROCEDURE [dbo].[drh_add_table_script]
    @version	VARCHAR(128),
    @schema_table		VARCHAR(256),
    @event		VARCHAR(128),
    @script		VARCHAR(MAX)
AS
BEGIN
    DECLARE @version_id 	INTEGER;
    DECLARE @table_id 		INTEGER;
    DECLARE @script_id 		INTEGER;
    DECLARE @upd_script_id	INTEGER;
    DECLARE @table		VARCHAR(128);
    DECLARE @schema		VARCHAR(128);
	
	IF(CHARINDEX('.', @schema_table)>0)
	BEGIN
		SELECT @schema=SUBSTRING(@schema_table, 1, CHARINDEX('.', @schema_table)-1), @table=SUBSTRING(@schema_table, CHARINDEX('.', @schema_table)+1, LEN(@schema_table))
	END
	ELSE
	BEGIN
		SELECT @table=@schema_table, @schema=schema_name();
	END

	SELECT @version_id = version_id FROM drh_script_version 
        WHERE name = @version;
	
	IF @version_id is null BEGIN
	    -- Insert to the drh_script_version
	    SELECT @version_id = MAX( version_id )+1 FROM drh_script_version;
	    IF @version_id is null BEGIN
			-- No rows are currently in drh_script_version
			SET @version_id = 1;
	    END;
	    INSERT INTO drh_script_version ( version_id, name ) VALUES ( @version_id, @version );
	END;
	
	SELECT @table_id = table_id FROM drh_table 
        WHERE [schema]=@schema AND name = @table;
	
	IF @table_id is null BEGIN
	    -- Insert to the drh_table
	    SELECT @table_id = MAX( table_id )+1 FROM drh_table;
	    IF @table_id is null BEGIN
			-- No rows are currently in drh_table
			SET @table_id = 1;
	    END;
	    INSERT INTO drh_table( table_id, [schema], name ) VALUES ( @table_id, @schema, @table );
	END;

	SELECT @script_id = MAX( script_id )+1 FROM drh_script;
	IF @script_id IS NULL BEGIN
	    -- No rows are currently in drh_script
	    SET @script_id = 1;
	END;
	select @upd_script_id = script_id from drh_script where version_id = @version_id and table_id = @table_id and [event] = @event
	IF @upd_script_id IS NULL BEGIN
		INSERT INTO drh_script (script_id, version_id, table_id, [event], script) VALUES (@script_id,@version_id, @table_id,@event,@script);
	END
	ELSE BEGIN
		UPDATE drh_script SET script = @script WHERE script_id=@upd_script_id
	END

END

GO
CREATE PROCEDURE [dbo].[usp_Event_Scripts]
(
	@script_version VARCHAR(128),
	@table_schema VARCHAR(128),
	@table_name VARCHAR(128),
	@event_name VARCHAR(128)
)
AS
BEGIN
	SELECT --ver.name AS script_version, tbl.[schema] AS table_schema, tbl.name AS table_name, scr.[event] AS event_name, 
		scr.script AS script
	FROM drh_script_version AS ver
	INNER JOIN drh_script AS scr ON scr.version_id=ver.version_id
	INNER JOIN drh_table AS tbl ON tbl.table_id=scr.table_id

	WHERE ver.name=@script_version AND tbl.[schema]=@table_schema AND tbl.name=@table_name AND scr.[event]=@event_name
END
GO
CREATE PROCEDURE [dbo].[USP_GetTRandShadow]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
--Print '/* ' + @table + ' - begins */
--'
--DECLARE @table NVARCHAR(100)=@table
--PRINT '/* Timestamp Column last_modified FOR USER DEFINED TABLE */'
--PRINT 'ALTER TABLE BUSDTA.['+ @table +'] ADD last_modified DATETIME DEFAULT GETDATE();'
--PRINT 'GO'
--PRINT ' '
DECLARE @uupkCols NVARCHAR(MAX)
SET @uupkCols = NULL
SELECT @uupkCols =COALESCE(@uupkCols + ', ','') + CONCAT( B.COLUMN_NAME ,' ', C.DATA_TYPE ,' ',CASE WHEN C.IS_NULLABLE = 'NO' THEN 'NOT NULL' END)
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS C
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND (B.TABLE_CATALOG=c.TABLE_CATALOG AND B.TABLE_SCHEMA=C.TABLE_SCHEMA AND B.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

DECLARE @uupk NVARCHAR(MAX)
SET @uupk = NULL
SELECT @uupk =COALESCE(@uupk + ', ','') + B.COLUMN_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS C
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND (B.TABLE_CATALOG=c.TABLE_CATALOG AND B.TABLE_SCHEMA=C.TABLE_SCHEMA AND B.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

--/* Create Shadow Table */
--DECLARE @uu NVARCHAR(MAX)
--SELECT @uu = CONCAT('CREATE TABLE ',TABLE_CATALOG,'.', TABLE_SCHEMA,'.',TABLE_NAME,'_del (')
--FROM INFORMATION_SCHEMA.TABLES 
--WHERE TABLE_NAME=@table
--ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

--PRINT '/* SHADOW TABLE FOR USER DEFINED TABLE */'
--PRINT 
--	@uu 
--	+CHAR(13) + CHAR(10)+ 
--	@uupk
--	+CHAR(13) + CHAR(10)+ 
--	', last_modified DATETIME DEFAULT GETDATE(),
--		PRIMARY KEY ('+ @uupk +')
--	);
--GO'
--PRINT ' '
--/* Create Shadow Table - END */
DECLARE @TableName VARCHAR(250), @SchemaName VARCHAR(250)
SELECT @TableName=TABLE_NAME, @SchemaName=TABLE_SCHEMA
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

/* Create Insert Trigger */
DECLARE @pkCols NVARCHAR(MAX)
SET @pkCols = NULL
SELECT @pkCols =COALESCE(@pkCols + ' AND ','') + CONCAT('', A.TABLE_SCHEMA,'.', A.TABLE_NAME,'.', B.COLUMN_NAME ,'= inserted.', B.COLUMN_NAME )
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

DECLARE @pkCols_del NVARCHAR(MAX)
SET @pkCols_del = NULL
SELECT @pkCols_del =COALESCE(@pkCols_del + ' AND ','') + CONCAT('', A.TABLE_SCHEMA,'.', A.TABLE_NAME,'_del.', B.COLUMN_NAME ,'= inserted.', B.COLUMN_NAME )
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

PRINT '/* TRIGGERS FOR '+ @TableName +' - START */'
PRINT '/* Create the shadow insert trigger */'
DECLARE @it NVARCHAR(MAX)

PRINT 'IF OBJECT_ID('''+ @SchemaName +'.'+ @TableName +'_ins'') IS NULL
BEGIN
	EXEC('''
PRINT '	CREATE TRIGGER '+ @SchemaName +'.'+ @TableName +'_ins
	ON '+ @SchemaName +'.'+ @TableName +' AFTER INSERT'
PRINT '	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/'
SELECT @it='	DELETE FROM '+ @SchemaName +'.'+ @TableName +'_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE '+ @pkCols_del +'
		);
	'')
END
GO'
PRINT @it
/* Create Insert Trigger - END */

/* Create Update Trigger */
PRINT ' '
PRINT '/* Create the shadow update trigger. */'

DECLARE @ut NVARCHAR(MAX)
SELECT @ut='IF OBJECT_ID('''+ @SchemaName +'.'+ @TableName +'_upd'') IS NULL
BEGIN
	EXEC('''
PRINT @ut
SELECT @ut='CREATE TRIGGER '+ @SchemaName +'.'+ @TableName +'_upd
	ON '+ @SchemaName +'.'+ @TableName +' AFTER UPDATE'
PRINT @ut
PRINT 'AS
/* Update the column last_modified in modified row. */'
SELECT @ut='UPDATE '+ @SchemaName +'.'+ @TableName +'
	SET last_modified = GETDATE()
	FROM inserted
		WHERE '+ @pkCols +''');
END
GO'
PRINT @ut

/* Create Update Trigger - END */

DECLARE @uupkDeleted NVARCHAR(MAX)
SET @uupkDeleted = NULL
SELECT @uupkDeleted =COALESCE(@uupkDeleted + ', ','') + 'deleted.'+B.COLUMN_NAME
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS C
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND (B.TABLE_CATALOG=c.TABLE_CATALOG AND B.TABLE_SCHEMA=C.TABLE_SCHEMA AND B.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

/* Create Delete Trigger */
PRINT ' '
PRINT '/* Create the shadow delete trigger */'
DECLARE @dt NVARCHAR(MAX)
SELECT @dt='IF OBJECT_ID('''+ @SchemaName +'.'+ @TableName +'_dlt'') IS NULL
BEGIN
	EXEC('''
PRINT @dt

SELECT @dt='CREATE TRIGGER '+ @SchemaName +'.'+ @TableName +'_dlt
	ON '+ @SchemaName +'.'+ @TableName +' AFTER DELETE'
PRINT @dt
PRINT 'AS
/* Insert the row into the shadow delete table. */'
SELECT @dt='INSERT INTO '+ @SchemaName +'.'+ @TableName +'_del ('+ @uupk +', last_modified )
	SELECT '+ @uupkDeleted +', GETDATE()
	FROM deleted;
	'');
END
GO'
PRINT @dt

/* Create Delete Trigger - END */

PRINT '/* TRIGGERS FOR '+ @TableName +' - END */'
SET NOCOUNT OFF
END

GO
CREATE PROCEDURE [dbo].[uspCDBScriptSchema] 
(
	@table_name SYSNAME,
	@add_timestamp_col bit=0,
	@generate_cdb_script_only bit=0
)
AS
BEGIN
/*CREATE TABLE SCRIPT - START*/
DECLARE @table SYSNAME

DECLARE 
      @object_name SYSNAME
    , @object_id INT

SELECT 
      @object_name = '[' + s.name + '].[' + o.name + ']'
	, @table = o.name
    , @object_id = o.[object_id]
FROM sys.objects o WITH (NOWAIT)
JOIN sys.schemas s WITH (NOWAIT) ON o.[schema_id] = s.[schema_id]
WHERE s.name + '.' + o.name = @table_name
    AND o.[type] = 'U'
    AND o.is_ms_shipped = 0

DECLARE @SQL NVARCHAR(MAX) = ''

PRINT '/*
/* '+ @object_name +' - begins */

/* TableDDL - '+ @object_name +' - Start */
IF OBJECT_ID('''+ @object_name +''') IS NULL'
PRINT 'BEGIN
'

;WITH 
fk_columns AS 
(
     SELECT 
          k.constraint_object_id
        , cname = c.name
        , rcname = rc.name
    FROM sys.foreign_key_columns k WITH (NOWAIT)
    JOIN sys.columns rc WITH (NOWAIT) ON rc.[object_id] = k.referenced_object_id AND rc.column_id = k.referenced_column_id 
    JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = k.parent_object_id AND c.column_id = k.parent_column_id
    WHERE k.parent_object_id = @object_id
)
SELECT @SQL = '	CREATE TABLE ' + @object_name + CHAR(13) + '	(' + CHAR(13) + STUFF((
    SELECT CHAR(9) + ', [' + c.name + '] ' + 
        CASE WHEN c.is_computed = 1
            THEN 'AS ' + cc.[definition] 
			WHEN tp.name IN ('INTEGER') 
                THEN 'INT'
			WHEN tp.name IN ('TEXT', 'NTEXT') 
                THEN tp.name 
            ELSE UPPER(tp.name) + 
                CASE WHEN tp.name IN ('varchar', 'char', 'varbinary', 'binary')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('nvarchar', 'nchar')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length / 2 AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('datetime2', 'time2', 'datetimeoffset') 
                       THEN '(' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'decimal' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'NUMERIC' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                    ELSE ''
                END
			END +
                --CASE WHEN c.collation_name IS NOT NULL THEN ' COLLATE ' + c.collation_name ELSE '' END +
                CASE WHEN c.is_nullable = 1 THEN ' NULL' ELSE ' NOT NULL' END +
                CASE WHEN dc.[definition] IS NOT NULL THEN ' CONSTRAINT DF_' + UPPER(OBJECT_NAME(C.object_id)) +'_' + C.name +' DEFAULT' + dc.[definition] ELSE '' END + 
                CASE WHEN ic.is_identity = 1 THEN ' IDENTITY(' + CAST(ISNULL(ic.seed_value, '0') AS CHAR(1)) + ',' + CAST(ISNULL(ic.increment_value, '1') AS CHAR(1)) + ')' ELSE '' END 
         + CHAR(13)
    FROM sys.columns c WITH (NOWAIT)
    JOIN sys.types tp WITH (NOWAIT) ON c.user_type_id = tp.user_type_id
    LEFT JOIN sys.computed_columns cc WITH (NOWAIT) ON c.[object_id] = cc.[object_id] AND c.column_id = cc.column_id
    LEFT JOIN sys.default_constraints dc WITH (NOWAIT) ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
    LEFT JOIN sys.identity_columns ic WITH (NOWAIT) ON c.is_identity = 1 AND c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
    WHERE c.[object_id] = @object_id
    ORDER BY c.column_id
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, CHAR(9) + ' ')
    + ISNULL((SELECT CHAR(9) + ', CONSTRAINT [PK_' + @table + '] PRIMARY KEY (' + --' + k.name + '
                    (SELECT STUFF((
                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                         FROM sys.index_columns ic WITH (NOWAIT)
                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
                             AND ic.[object_id] = k.parent_object_id 
                             AND ic.index_id = k.unique_index_id     
                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
            + ')' + CHAR(13)
            FROM sys.key_constraints k WITH (NOWAIT)
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '') + '	)'  + CHAR(13)
    + ISNULL((SELECT (
        SELECT CHAR(13) +
             'ALTER TABLE ' + @object_name + ' WITH' 
            + CASE WHEN fk.is_not_trusted = 1 
                THEN ' NOCHECK' 
                ELSE ' CHECK' 
              END + 
              ' ADD CONSTRAINT [' + fk.name  + '] FOREIGN KEY(' 
              + STUFF((
                SELECT ', [' + k.cname + ']'
                FROM fk_columns k
                WHERE k.constraint_object_id = fk.[object_id]
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
               + ')' +
              ' REFERENCES [' + SCHEMA_NAME(ro.[schema_id]) + '].[' + ro.name + '] ('
              + STUFF((
                SELECT ', [' + k.rcname + ']'
                FROM fk_columns k
                WHERE k.constraint_object_id = fk.[object_id]
                FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, '')
               + ')'
            + CASE 
                WHEN fk.delete_referential_action = 1 THEN ' ON DELETE CASCADE' 
                WHEN fk.delete_referential_action = 2 THEN ' ON DELETE SET NULL'
                WHEN fk.delete_referential_action = 3 THEN ' ON DELETE SET DEFAULT' 
                ELSE '' 
              END
            + CASE 
                WHEN fk.update_referential_action = 1 THEN ' ON UPDATE CASCADE'
                WHEN fk.update_referential_action = 2 THEN ' ON UPDATE SET NULL'
                WHEN fk.update_referential_action = 3 THEN ' ON UPDATE SET DEFAULT'  
                ELSE '' 
              END 
            + CHAR(13) + 'ALTER TABLE ' + @object_name + ' CHECK CONSTRAINT [' + fk.name  + ']' + CHAR(13)
        FROM sys.foreign_keys fk WITH (NOWAIT)
        JOIN sys.objects ro WITH (NOWAIT) ON ro.[object_id] = fk.referenced_object_id
        WHERE fk.parent_object_id = @object_id
        FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)')), '')

PRINT @SQL

PRINT 'END
/* TableDDL - '+ @object_name +' - End */'

/*CREATE TABLE SCRIPT - END*/

IF(@generate_cdb_script_only=0) 
BEGIN
/*CREATE TIMESTAMP COL - START*/
IF(@add_timestamp_col=1)
BEGIN
PRINT '
/* TIMESTAMP COL FOR '+ @object_name +' - Start */
'
	IF NOT EXISTS(
		SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME=@table_name AND COLUMN_NAME='last_modified'
		)
	BEGIN
		DECLARE @add_timestamp VARCHAR(512)='ALTER TABLE '+ @table_name +' ADD last_modified DATETIME CONSTRAINT DF_'+REPLACE(REPLACE(@table,'[',''),']','')+'_Last_Modified DEFAULT (GETDATE())'
		PRINT @add_timestamp
	END
PRINT '/* TIMESTAMP COL FOR '+ @object_name +' - End */'
END
/*CREATE TIMESTAMP COL - END*/

/*CREATE SHADOW TABLE SCRIPT - START*/
PRINT '
/* SHADOW TABLE FOR '+ @object_name +' - Start */'
PRINT 'IF OBJECT_ID('''+ substring(@object_name,1,len(@object_name)-1) +'_del]'+''') IS NULL'
PRINT 'BEGIN
'
SELECT @SQL = '	CREATE TABLE ' + substring(@object_name,1,len(@object_name)-1) +'_del]'+ CHAR(13) + '	(' + CHAR(13) + STUFF((
    SELECT CHAR(9) + ', [' + c.name + '] ' +
         UPPER(tp.name) + 
                CASE WHEN tp.name IN ('varchar', 'char', 'varbinary', 'binary', 'text')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('nvarchar', 'nchar', 'ntext')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN 'MAX' ELSE CAST(c.max_length / 2 AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('datetime2', 'time2', 'datetimeoffset') 
                       THEN '(' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'decimal' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'NUMERIC' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                    ELSE ''
                END 
        + CHAR(13)
    FROM sys.columns c WITH (NOWAIT)
    JOIN sys.types tp WITH (NOWAIT) ON c.user_type_id = tp.user_type_id
    LEFT JOIN sys.default_constraints dc WITH (NOWAIT) ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
	INNER JOIN SYS.key_constraints AS K ON C.object_id=K.parent_object_id -- WHERE parent_object_id=OBJECT_ID('BUSDTA.F0004')
	INNER JOIN SYS.index_columns AS IC ON IC.index_id=K.unique_index_id AND IC.object_id=K.parent_object_id AND IC.column_id=C.column_id
    WHERE c.[object_id] = @object_id
    ORDER BY c.column_id	
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, CHAR(9) + ' ')
    + ISNULL((SELECT CHAR(9) +', last_modified DATETIME DEFAULT GETDATE()' +CHAR(13)+ '	, PRIMARY KEY (' + 
                    (SELECT STUFF((
                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                         FROM sys.index_columns ic WITH (NOWAIT)
                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
                             AND ic.[object_id] = k.parent_object_id 
                             AND ic.index_id = k.unique_index_id     
                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
            + ')' + CHAR(13)
            FROM sys.key_constraints k WITH (NOWAIT)
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '')+ '	)'  + CHAR(13)

PRINT @SQL
PRINT 'END'
PRINT '/* SHADOW TABLE FOR '+ @object_name +' - End */'
/*CREATE SHADOW TABLE SCRIPT - END*/

	EXEC [dbo].[USP_GetTRandShadow] @table
END
PRINT '*/'
END
GO
CREATE PROCEDURE [dbo].[uspRDBScriptSchema]
(
	@table_name SYSNAME
)
AS
BEGIN
/*CREATE TABLE SCRIPT - START*/
DECLARE @table SYSNAME

DECLARE 
      @object_name SYSNAME
    , @object_id INT

SELECT 
      @object_name = '[' + s.name + '].[' + o.name + ']'
	, @table = o.name
    , @object_id = o.[object_id]
FROM sys.objects o WITH (NOWAIT)
JOIN sys.schemas s WITH (NOWAIT) ON o.[schema_id] = s.[schema_id]
WHERE s.name + '.' + o.name = @table_name
    AND o.[type] = 'U'
    AND o.is_ms_shipped = 0

DECLARE @SQL NVARCHAR(MAX) = ''

PRINT '/*
/* '+ @object_name +' - begins */

/* TableDDL - '+ @object_name +' - Start */
IF OBJECT_ID('''+ @object_name +''') IS NULL'
PRINT 'BEGIN
'

;WITH 
fk_columns AS 
(
     SELECT 
          k.constraint_object_id
        , cname = c.name
        , rcname = rc.name
    FROM sys.foreign_key_columns k WITH (NOWAIT)
    JOIN sys.columns rc WITH (NOWAIT) ON rc.[object_id] = k.referenced_object_id AND rc.column_id = k.referenced_column_id 
    JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = k.parent_object_id AND c.column_id = k.parent_column_id
    WHERE k.parent_object_id = @object_id
)
SELECT @SQL = '	CREATE TABLE ' + @object_name + CHAR(13) + '	(' + CHAR(13) + STUFF((
    SELECT CHAR(9) + ', [' + c.name + '] ' + 
        CASE WHEN c.is_computed = 1
				THEN 'AS ' + cc.[definition] 
			WHEN (tp.name IN ('varchar') AND c.max_length = -1) OR (tp.name IN ('ntext','text'))
                THEN 'LONG VARCHAR'
			WHEN tp.name IN ('varbinary') AND c.max_length = -1
                THEN 'LONG BINARY'
			WHEN tp.name IN ('nchar') AND c.max_length != -1
                THEN 'NVARCHAR ('+ CASE WHEN SUBSTRING(tp.name,1,1)='n' THEN CAST(c.max_length/2 AS VARCHAR(5)) ELSE CAST(c.max_length AS VARCHAR(5)) END +')'
			WHEN tp.name IN ('image') 
                THEN 'LONG BINARY'
			WHEN tp.name IN ('datetime2') 
                THEN 'TIMESTAMP'
			WHEN tp.name IN ('int') 
                THEN 'INTEGER'
            ELSE UPPER(tp.name) + 
                CASE
					WHEN tp.name IN ('varchar', 'char', 'varbinary', 'binary', 'text')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN '' ELSE CAST(c.max_length AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('nvarchar', 'nchar', 'ntext')
                       THEN '(' + CASE WHEN c.max_length = -1 THEN '' ELSE CAST(c.max_length / 2 AS VARCHAR(5)) END + ')'
                     WHEN tp.name IN ('time2', 'datetimeoffset') 
                       THEN '(' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'decimal' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                     WHEN tp.name = 'NUMERIC' 
                       THEN '(' + CAST(c.[precision] AS VARCHAR(5)) + ',' + CAST(c.scale AS VARCHAR(5)) + ')'
                    ELSE ''
                END
			END +
                --CASE WHEN c.collation_name IS NOT NULL THEN ' COLLATE ' + c.collation_name ELSE '' END +
                CASE WHEN c.is_nullable = 1 THEN ' NULL' ELSE ' NOT NULL' END +
                CASE WHEN dc.[definition] IS NOT NULL THEN ' DEFAULT' + dc.[definition] ELSE '' END + 
                CASE WHEN ic.is_identity = 1 THEN ' DEFAULT AUTOINCREMENT' ELSE '' END 
         + CHAR(13)
    FROM sys.columns c WITH (NOWAIT)
    JOIN sys.types tp WITH (NOWAIT) ON c.user_type_id = tp.user_type_id
    LEFT JOIN sys.computed_columns cc WITH (NOWAIT) ON c.[object_id] = cc.[object_id] AND c.column_id = cc.column_id
    LEFT JOIN sys.default_constraints dc WITH (NOWAIT) ON c.default_object_id != 0 AND c.[object_id] = dc.parent_object_id AND c.column_id = dc.parent_column_id
    LEFT JOIN sys.identity_columns ic WITH (NOWAIT) ON c.is_identity = 1 AND c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
    WHERE c.[object_id] = @object_id AND C.name !='last_modified'
    ORDER BY c.column_id
    FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, CHAR(9) + ' ')
    + ISNULL((SELECT CHAR(9) + ', PRIMARY KEY (' + --' + k.name + '
                    (SELECT STUFF((
                         SELECT ', [' + c.name + '] ' + CASE WHEN ic.is_descending_key = 1 THEN 'DESC' ELSE 'ASC' END
                         FROM sys.index_columns ic WITH (NOWAIT)
                         JOIN sys.columns c WITH (NOWAIT) ON c.[object_id] = ic.[object_id] AND c.column_id = ic.column_id
                         WHERE ic.is_included_column = 0
                             AND ic.[object_id] = k.parent_object_id 
                             AND ic.index_id = k.unique_index_id     
                         FOR XML PATH(N''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 2, ''))
            + ')' + CHAR(13)
            FROM sys.key_constraints k WITH (NOWAIT)
            WHERE k.parent_object_id = @object_id 
                AND k.[type] = 'PK'), '') + '	)'  

PRINT @SQL

PRINT 'END
/* TableDDL - '+ @object_name +' - End */
*/'

/*CREATE TABLE SCRIPT - END*/

END

GO
CREATE PROCEDURE [dbo].[UserDetail] 
(
	@DomainUser nvarchar(50) 
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT App_user_id, App_User, Name, DomainUser, AppPassword, active FROM BUSDTA.user_master
	WHERE DomainUser= @DomainUser
	SET NOCOUNT ON;
END
GO

/*
=============================================
Author:			Sakaravarthi J
Create date:	28-Sep-2015
Description:	Logs Webservices' request and response
	Logs WebServices source from all sources
		1. Stats Services
		2. Onboarding Services
Note: this Stored Procedures gets/logs by calling from CDB 
	(where the services configured to the database as the web service contact only CDB)
============================================= 
*/
CREATE PROCEDURE [dbo].[SaveWebServiceLog]
(
	@DeviceID VARCHAR(100)='',
	@Request VARCHAR(MAX),
	@Response VARCHAR(MAX),
	@Environment VARCHAR(128)='',
	@DBName VARCHAR(128)='',
	@ServiceName VARCHAR(100)='',
	@ServiceEndPoint VARCHAR(1000)='',
	@ServiceContract VARCHAR(1000)=''
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @ServiceLogID INT = 0;
	SELECT @ServiceLogID = ISNULL(MAX(ServiceLogID), 0)+1 FROM tblWebServiceLog;

	INSERT INTO [dbo].[tblWebServiceLog] ([ServiceLogID], [DeviceID], [Request], [Response], [Environment], [DBName], [ServiceName], [ServiceEndPoint], [ServiceContract])
	VALUES(@ServiceLogID, @DeviceID, CAST(@Request AS XML), CAST(@Response AS XML), @Environment, @DBName, @ServiceName, @ServiceEndPoint, @ServiceContract)
SET NOCOUNT OFF
END

GO
CREATE PROCEDURE [dbo].[uspValidateRegisteration]
(
	@DeviceId VARCHAR(50)
)
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins=(ExpireMins*-1) FROM dbo.tblReserveConfig 

	SELECT ISNULL(ReservationID,0) AS ReservationID, rdb.DeviceID, em.EnvName, rdb.RouteID, rdb.ActiveYN,ReservedDateTime
	FROM BUSDTA.RemoteDBID_Master AS rdb 
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der ON rdb.DeviceID=der.DeviceID AND rdb.EnvironmentMasterID=der.EnvironmentMasterID AND rdb.RouteID=der.RouteID 
	LEFT JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=rdb.EnvironmentMasterId  
	WHERE rdb.DeviceId=@DeviceId AND ISNULL(ReservedDateTime, GETDATE())>=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO
CREATE PROCEDURE [dbo].[ValidateDevice]
(
	@DeviceID nvarchar(50) 
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DM.Device_ID AS DeviceID, ISNULL(RM.ActiveYN,'N') AS IsRegistered, DM.Active AS IsDeviceActive FROM BUSDTA.Device_Master AS DM
	LEFT JOIN BUSDTA.RemoteDBID_Master AS RM ON RM.DeviceID=DM.Device_ID AND RM.ActiveYN='Y'
	WHERE DM.Device_ID=@DeviceID 
	SET NOCOUNT OFF;
END

GO
CREATE PROCEDURE [dbo].[uspCheckEnvironmentAvailability]
(
	@App_User VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@DeviceID VARCHAR(30)
)
AS
BEGIN
	SELECT um.App_User, dem.DeviceId, em.EnvName, em.CDBConKey
	FROM BUSDTA.Device_Environment_Map AS dem
	INNER JOIN BUSDTA.User_Environment_Map AS uem ON dem.EnvironmentMasterId=uem.EnvironmentMasterId 
	INNER JOIN BUSDTA.User_Master AS um ON uem.AppUserId=um.App_user_id
	INNER JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=uem.EnvironmentMasterId
	WHERE dem.DeviceId=@DeviceID AND um.App_User=@App_User AND em.EnvName=@EnvironmentName 
END

GO

CREATE PROCEDURE [dbo].[uspCheckRouteAvailabilityForReserve]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins-1) FROM tblReserveConfig 

	SELECT rdbM.DeviceID, um.App_User, EnvName, rdbM.RouteID, rdbM.ActiveYN, ReservedDateTime
	FROM BUSDTA.RemoteDBID_Master AS rdbM
	INNER JOIN BUSDTA.Environment_Master AS em ON rdbM.EnvironmentMasterId=em.EnvironmentMasterId 
	INNER JOIN BUSDTA.User_Master AS um ON rdbM.AppUserId=um.App_user_id
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der
		ON rdbM.EnvironmentMasterId=der.EnvironmentMasterId AND rdbM.RouteID=der.RouteID AND rdbM.DeviceID=der.DeviceID AND rdbM.AppUserID=der.AppUserID AND ReservedDateTime<=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO
CREATE PROCEDURE [dbo].[uspCleanupReservation_SysDB]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM tblReserveConfig 

	--Remove Entry from Reservation Table Reserve_DeviceEnvironmentRoute
	DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE ReservedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE())
	--DELETE FROM BUSDTA.RemoteDBID_Master WHERE CreatedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE()) AND ActiveYN='N'
END
GO
CREATE PROCEDURE [dbo].[GetEnvDetails]
(
	@AppUser nvarchar(50) 
)
AS
BEGIN
SET NOCOUNT ON;
	SELECT UM.App_User, EM.EnvironmentMasterId, EM.EnvName, EM.EnvDescription, RSM.Route_Id
		FROM BUSDTA.user_master UM
		INNER JOIN VW_All_RouteUserMap RSM ON RSM.App_user_id = UM.App_user_id
		INNER JOIN [BUSDTA].[User_Environment_Map] UEMAP ON UM.App_user_id = UEMAP.AppUserId 
		INNER JOIN [BUSDTA].[Environment_Master] EM ON EM.EnvironmentMasterId = UEMAP.EnvironmentMasterId AND RSM.EnvName = EM.EnvName
	WHERE UM.App_User = @AppUser
SET NOCOUNT OFF;
END


GO

CREATE PROCEDURE [dbo].[Register_DeviceEnvironment] 
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@AppUserName VARCHAR(30)
)
AS
BEGIN
DECLARE @UserEnvironmentMapId INT=0, @EnvironmentMasterId INT, @AppUserId INT;
SELECT @EnvironmentMasterId = EnvironmentMasterId FROM BUSDTA.Environment_Master WHERE EnvName=@EnvironmentName
SELECT @AppUserId = App_User_ID FROM BUSDTA.User_Master WHERE App_User=@AppUserName
SELECT @UserEnvironmentMapId = ISNULL(MAX(UserEnvironmentMapId), 0)+1 FROM BUSDTA.User_Environment_Map

	IF EXISTS(SELECT 1 FROM BUSDTA.User_Environment_Map WHERE EnvironmentMasterId=@EnvironmentMasterId)
	BEGIN
		UPDATE BUSDTA.RemoteDBID_Master SET ActiveYN='Y' WHERE DeviceId=@DeviceId AND RouteID=@RouteID AND EnvironmentMasterId=@EnvironmentMasterId
	END

	IF EXISTS(SELECT 1 FROM BUSDTA.User_Environment_Map WHERE EnvironmentMasterId=@EnvironmentMasterId)
	BEGIN
		DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceId AND RouteID=@RouteID AND EnvironmentMasterId=@EnvironmentMasterId
	END
END

GO
CREATE PROCEDURE [dbo].[GetMappedEnv]
(
	@AppUser nvarchar(50) 
)
AS
BEGIN
SET NOCOUNT ON;
	SELECT UM.App_User_ID, UM.App_User, EM.EnvName, EM.EnvDescription, EM.CDBConKey, EM.EnvironmentMasterID
		FROM BUSDTA.user_master UM
		INNER JOIN BUSDTA.User_Environment_Map UEMAP ON UM.App_user_id = UEMAP.AppUserId 
		INNER JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = UEMAP.EnvironmentMasterId 
	WHERE UM.DomainUser = @AppUser
SET NOCOUNT OFF;
END
GO
CREATE PROCEDURE [dbo].[USP_GetDownloadCursor] 
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Download Cursor */
DECLARE @selectStmt NVARCHAR(MAX)
DECLARE @dc NVARCHAR(MAX)
SET @selectStmt = NULL
SELECT @selectStmt = COALESCE(@selectStmt + ',','') + '"' + COLUMN_NAME + '"' + CHAR(13) + CHAR(10)
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME=@table AND COLUMN_NAME != 'last_modified'
ORDER BY TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION

SELECT @dc = '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '"'+CHAR(13) + CHAR(10)+
	'WHERE ' + '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '".' + '"last_modified">= {drh s.last_table_download}'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

PRINT 'SELECT ' + @selectStmt
PRINT 'FROM ' +@dc

/* Download Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO
CREATE PROCEDURE [dbo].[USP_GetDownloadDeleteCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Download Delete Cursor */
DECLARE @ddc NVARCHAR(MAX)
DECLARE @pkCols NVARCHAR(MAX)
SET @pkCols = NULL
SELECT @pkCols =COALESCE(@pkCols + ',','') + '"' + B.COLUMN_NAME + '"' + CHAR(13) + CHAR(10)
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS AS C
WHERE (CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME)
AND(A.TABLE_CATALOG=C.TABLE_CATALOG AND A.TABLE_SCHEMA=C.TABLE_SCHEMA AND A.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME,  C.ORDINAL_POSITION
SELECT @ddc = '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '_del"'+CHAR(13) + CHAR(10)+
	'WHERE ' + '"' + TABLE_SCHEMA + '".' + '"' + TABLE_NAME + '_del".' + '"last_modified">= {drh s.last_table_download}'
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

PRINT 'SELECT ' + @pkCols
PRINT 'FROM ' +@ddc
/* Download Delete Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO
CREATE PROCEDURE [dbo].[USP_GetUploadDeleteCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/*'
Print ''

/* Upload Delete Cursor */
DECLARE @ud NVARCHAR(MAX)
SELECT @ud = TABLE_SCHEMA+'.'+TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

DECLARE @udCols NVARCHAR(MAX)
SET @udCols = NULL
SELECT @udCols =COALESCE(@udCols + ' AND ','') + B.COLUMN_NAME+' = {drh r.'+B.COLUMN_NAME+'}'
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS AS C
WHERE (CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME)
AND(A.TABLE_CATALOG=C.TABLE_CATALOG AND A.TABLE_SCHEMA=C.TABLE_SCHEMA AND A.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME, C.ORDINAL_POSITION

PRINT '/* Delete the row in the consolidated database. */'
PRINT 'DELETE FROM ' + @ud
PRINT 'WHERE ' +@udCols

/* Upload Delete Cursor - END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO
CREATE PROCEDURE [dbo].[USP_GetUploadInsertCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* '
Print ''

/* Upload Insert Cursor */
DECLARE @ui NVARCHAR(MAX)
SELECT @ui = TABLE_SCHEMA + '.' + TABLE_NAME 
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

DECLARE @uiCols NVARCHAR(MAX)
SELECT @uiCols =COALESCE(@uiCols + ', ','') + A.COLUMN_NAME 
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME

DECLARE @uiColML NVARCHAR(MAX)
SELECT @uiColML =COALESCE(@uiColML + ', {drh r.','')  + A.COLUMN_NAME + '}'
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME
PRINT '/* Insert the row into the consolidated database. */'
PRINT 'INSERT INTO ' + @ui
PRINT '(' + @uiCols +')'
PRINT 'VALUES(drh r.' + @uiColML +')'
/* Upload Insert Cursor -END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO
CREATE PROCEDURE [dbo].[USP_GetUploadUpdateCursor]
(
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/*'
Print ''

/* Upload Update Cursor */
DECLARE @uu NVARCHAR(MAX)
SELECT @uu = TABLE_SCHEMA+'.'+''+TABLE_NAME
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_SCHEMA, TABLE_NAME

DECLARE @uuCols NVARCHAR(MAX)
SELECT @uuCols =COALESCE(@uuCols + ', ','') + A.COLUMN_NAME+' = {drh r.'+A.COLUMN_NAME+'}'
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified' AND A.COLUMN_NAME NOT IN 
(SELECT B1.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A1, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B1 WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A1.CONSTRAINT_NAME = B1.CONSTRAINT_NAME AND A1.TABLE_NAME=A.TABLE_NAME)
ORDER BY A.TABLE_NAME

DECLARE @uupkCols NVARCHAR(MAX)
SET @uupkCols = NULL
SELECT @uupkCols =COALESCE(@uupkCols + ' AND ','') + B.COLUMN_NAME+' = {drh r.'+B.COLUMN_NAME+'}'
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B, INFORMATION_SCHEMA.COLUMNS AS C
WHERE (CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME)
AND(A.TABLE_CATALOG=C.TABLE_CATALOG AND A.TABLE_SCHEMA=C.TABLE_SCHEMA AND A.TABLE_NAME=C.TABLE_NAME AND B.COLUMN_NAME=C.COLUMN_NAME)
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME, C.ORDINAL_POSITION

PRINT '/* Update the row in the consolidated database. */'
PRINT 'UPDATE ' + @uu
PRINT 'SET ' + @uuCols 
PRINT 'WHERE ' +@uupkCols
/* Upload Update Cursor - END */
Print ''
Print '*/'

SET NOCOUNT OFF
END

GO
CREATE PROCEDURE [dbo].[Reserve_DeviceEnvironmentRoute]
(
	@DeviceId VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@RouteID VARCHAR(30),
	@AppUserName VARCHAR(30)
)
AS
BEGIN
DECLARE @EnvironmentMasterId INT, @AppUserId INT, @rdb_script_location VARCHAR(MAX), @RemoteDBID INT;
SELECT @EnvironmentMasterId = EnvironmentMasterId, @rdb_script_location=RDBScriptLocation FROM BUSDTA.Environment_Master WHERE EnvName=@EnvironmentName
SELECT @AppUserId = App_User_ID FROM BUSDTA.User_Master WHERE DomainUser=@AppUserName
SELECT @RemoteDBID = ISNULL(MAX(RemoteDBID),1000) FROM BUSDTA.RemoteDBID_Master

	IF NOT EXISTS(SELECT 1 FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		INSERT INTO BUSDTA.Reserve_DeviceEnvironmentRoute(DeviceId, EnvironmentMasterId, RouteID, AppUserId)
		SELECT @DeviceId, @EnvironmentMasterId, @RouteID, @AppUserId
	END
	ELSE
	BEGIN
		UPDATE BUSDTA.Reserve_DeviceEnvironmentRoute SET ReservedDateTime=GETDATE() 
		WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID
	END

	--IF NOT EXISTS(SELECT 1 FROM BUSDTA.RemoteDBID_Master WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		SET @RemoteDBID=@RemoteDBID+1
		INSERT INTO BUSDTA.RemoteDBID_Master (RemoteDBID, AppUserId, DeviceID, RouteID, EnvironmentMasterID)
		VALUES (@RemoteDBID, @AppUserId, @DeviceId, @RouteID,@EnvironmentMasterId)
	END

	--SELECT @rdb_script_location RDBScriptLocation, @RemoteDBID RemoteDBID

	SELECT AppType, AppName, ReleasedDate, AppLocation, RemoteDBID = CASE WHEN AppType ='RemoteDB' THEN @RemoteDBID ELSE NULL END
	FROM (
		select at.AppType,
		ROW_NUMBER() OVER(PARTITION BY am.AppTypeID ORDER BY am.AppReleaseID DESC) Latest,
		am.AppName, am.ReleasedDate, am.AppLocation, at.SortOrder
		from BUSDTA.Environment_Master em
		inner join tblAppReleaseMaster am on em.EnvironmentMasterId=am.EnvironmentMasterId
		left join tblAppTypes at on at.AppTypeID=am.AppTypeID
		WHERE at.ClientSetup=1 and em.EnvironmentMasterId=@EnvironmentMasterId
	)AS k WHERE latest=1 order by SortOrder
END
GO
-- =============================================
-- Author:		Sakaravarthi J
-- Create date: 26-Dec-2015
-- Description:	cleanup entries from the table entries to decouple route device mappings
-- =============================================
CREATE PROCEDURE [dbo].[uspDeregisterRouteDevice]
(
	@DeviceID VARCHAR(50),
	@RouteName VARCHAR(50)
)
AS
BEGIN
	DECLARE @DBName VARCHAR(100), @sql VARCHAR(1000)
	SELECT @DBName = LTRIM(RTRIM(em.CDBAddress)) FROM BUSDTA.Environment_Master AS em 
	INNER JOIN BUSDTA.RemoteDBID_Master AS rm ON em.EnvironmentMasterId=rm.EnvironmentMasterID
	WHERE  rm.DeviceID=@DeviceID AND rm.RouteID=@RouteName

	DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceID
	DELETE FROM BUSDTA.RemoteDBID_Master WHERE DeviceId=@DeviceID
	--DELETE FROM MobileDataModel_FPS5_Release.BUSDTA.Route_Device_Map WHERE Device_Id=@DeviceID
	--UPDATE MobileDataModel_FPS5_Release.BUSDTA.Route_Master SET RouteStatusID=1 WHERE RouteName=@RouteName
	IF(LTRIM(RTRIM(@DBName))!='')
	BEGIN 
		SET @sql= CONCAT('DELETE FROM ',@DBName,'.BUSDTA.Route_Device_Map WHERE Device_Id=''',@DeviceID,'''')
		--PRINT @sql
		EXEC (@sql)
		
		SET @sql= CONCAT('UPDATE ',@DBName,'.BUSDTA.Route_Master SET RouteStatusID=1 WHERE RouteName=''',@RouteName,'''')
		--PRINT @sql
		EXEC (@sql)
	END
END

GO

-- =============================================
-- Author:		Sakaravarthi J
-- Create date: 19-Oct-2015
-- Description:	get applicaiton's latest release version and date
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAppVersion]
(
	@EnvironmentName VARCHAR(100),
	@AppName VARCHAR(100)=NULL
)
AS
BEGIN
	SELECT EnvName, AppName, B.AppType, VersionNumber, ReleasedDate, AppLocation, IsMandatory FROM(
			SELECT em.EnvName, AppName, AppTypeID, VersionNumber, ReleasedDate, AppLocation, IsMandatory, 
			RANK() OVER (PARTITION BY em.EnvName, AppName ORDER BY ReleasedDate DESC,AppReleaseID DESC) AS LatestRank
			FROM tblAppReleaseMaster AS arm INNER JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=arm.EnvironmentMasterId
		) AS Apps INNER JOIN tblAppTypes AS B ON Apps.AppTypeID=B.AppTypeID
	WHERE ((ISNULL(@AppName,'')='' AND LatestRank=1) OR (ISNULL(@AppName,'')!='' AND LatestRank=1 AND AppName=@AppName))
	AND EnvName=@EnvironmentName
	ORDER BY B.SortOrder
END

GO

-- =============================================
-- Author		Sakaravarthi J
-- Create date 08-Jan-2016
-- Description	Get Latest Application's Version  RemoteDB Release script Name
-- =============================================
CREATE PROCEDURE [dbo].[uspGetRemoteDBInfo] 
(
	@CdbVersion VARCHAR(50)=NULL,
	@SysDBVersion VARCHAR(50)=NULL
)
AS
BEGIN
	SELECT TOP 1 ReleaseName, SysDBVersion, CDBVersion 
	FROM vwRemoteDBRelease
	WHERE ((@CdbVersion IS NOT NULL AND CDBVersion = @CdbVersion) OR (@CdbVersion IS NULL))
		AND ((@SysDBVersion IS NOT NULL AND SysDBVersion = @SysDBVersion) OR (@SysDBVersion IS NULL))
	ORDER BY ReleaseName DESC
END
GO
