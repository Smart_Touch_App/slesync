CREATE VIEW [dbo].[vw_DeviceMaster]
AS
	SELECT Device_Id, DeviceName, Active, manufacturer, model, last_modified
	FROM BUSDTA.Device_Master

GO
CREATE VIEW [dbo].[vw_UserMaster]
AS
	SELECT App_user_id, App_User, Name, DomainUser, AppPassword, active, last_modified
	FROM BUSDTA.user_master
GO
