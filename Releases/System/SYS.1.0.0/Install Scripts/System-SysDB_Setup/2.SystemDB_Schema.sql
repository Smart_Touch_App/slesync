 IF (SCHEMA_ID('BUSDTA') IS NULL) 
BEGIN
    EXEC ('CREATE SCHEMA [BUSDTA] AUTHORIZATION [dbo]')
END

/* [BUSDTA].[Device_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[Device_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Environment_Map]
	(
	  [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [DeviceId] VARCHAR(30) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_DEVICE_ENVIRONMENT_MAP_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_DEVICE_ENVIRONMENT_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Device_Environment_Map] PRIMARY KEY ([EnvironmentMasterId] ASC, [DeviceId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Device_Environment_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Device_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Environment_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Environment_Map_del]
	(
	  [EnvironmentMasterId] NUMERIC(8,0)
	, [DeviceId] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EnvironmentMasterId] ASC, [DeviceId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Device_Environment_Map] - End */
/* TRIGGERS FOR Device_Environment_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Device_Environment_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Device_Environment_Map_ins
	ON BUSDTA.Device_Environment_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Device_Environment_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Device_Environment_Map_del.DeviceId= inserted.DeviceId AND BUSDTA.Device_Environment_Map_del.EnvironmentMasterId= inserted.EnvironmentMasterId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Device_Environment_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Environment_Map_upd
	ON BUSDTA.Device_Environment_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Device_Environment_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Device_Environment_Map.DeviceId= inserted.DeviceId AND BUSDTA.Device_Environment_Map.EnvironmentMasterId= inserted.EnvironmentMasterId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Device_Environment_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Environment_Map_dlt
	ON BUSDTA.Device_Environment_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Device_Environment_Map_del (DeviceId, EnvironmentMasterId, last_modified )
	SELECT deleted.DeviceId, deleted.EnvironmentMasterId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Device_Environment_Map - END */

/* [BUSDTA].[Device_Master] - begins */

/* TableDDL - [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master]
	(
	  [Device_Id] VARCHAR(30) NOT NULL
	, [DeviceName] VARCHAR(100) NULL
	, [Active] INT NULL
	, [manufacturer] NVARCHAR(50) NULL
	, [model] NVARCHAR(50) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_DEVICE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Device_Master] PRIMARY KEY ([Device_Id] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Device_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master_del]
	(
	  [Device_Id] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Device_Id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Device_Master] - End */
/* TRIGGERS FOR Device_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Device_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Device_Master_ins
	ON BUSDTA.Device_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Device_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Device_Master_del.Device_Id= inserted.Device_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Device_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Master_upd
	ON BUSDTA.Device_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Device_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Device_Master.Device_Id= inserted.Device_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Device_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Master_dlt
	ON BUSDTA.Device_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Device_Master_del (Device_Id, last_modified )
	SELECT deleted.Device_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Device_Master - END */
/* [BUSDTA].[Environment_Master] - begins */

/* TableDDL - [BUSDTA].[Environment_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Environment_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Environment_Master]
	(
	  [EnvironmentMasterId] INT NOT NULL IDENTITY(1,1)
	, [EnvName] VARCHAR(100) NULL
	, [EnvDescription] VARCHAR(100) NULL
	, [IsActive] VARCHAR(1) NULL
	, [ReleaseLevel] VARCHAR(30) NULL
	, [MLServerAddress] NCHAR(50) NULL
	, [JDEWSAddress] NCHAR(50) NULL
	, [REFDBAddress] NCHAR(50) NULL
	, [ENVDBAddress] NCHAR(50) NULL
	, [CDBAddress] NCHAR(50) NULL
	, [CDBConKey] VARCHAR(128) NULL
	, [IISAddress] NCHAR(50) NULL
	, [AppInstallerLocation] VARCHAR(4000) NULL
	, [RDBScriptLocation] VARCHAR(4000) NULL
	, [TaxLibLocation] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL  DEFAULT GETDATE()
	, [last_modified] DATETIME NULL  DEFAULT GETDATE()
	, CONSTRAINT [PK_Environment_Master] PRIMARY KEY ([EnvironmentMasterId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Environment_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Environment_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Environment_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Environment_Master_del]
	(
	  [EnvironmentMasterId] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EnvironmentMasterId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Environment_Master] - End */
/* TRIGGERS FOR Environment_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Environment_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Environment_Master_ins
	ON BUSDTA.Environment_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Environment_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Environment_Master_del.EnvironmentMasterId= inserted.EnvironmentMasterId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Environment_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Environment_Master_upd
	ON BUSDTA.Environment_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Environment_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Environment_Master.EnvironmentMasterId= inserted.EnvironmentMasterId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Environment_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Environment_Master_dlt
	ON BUSDTA.Environment_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Environment_Master_del (EnvironmentMasterId, last_modified )
	SELECT deleted.EnvironmentMasterId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Environment_Master - END */


/* [BUSDTA].[RemoteDBID_Master] - begins */

/* TableDDL - [BUSDTA].[RemoteDBID_Master] - Start */
IF OBJECT_ID('[BUSDTA].[RemoteDBID_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RemoteDBID_Master]
	(
	  [RemoteDBID] INT NOT NULL
	, [AppUserId] INT NULL
	, [DeviceID] VARCHAR(30) NULL
	, [RouteID] VARCHAR(30) NULL
	, [EnvironmentMasterID] INT NULL
	, [ActiveYN] CHAR(1) NOT NULL CONSTRAINT DF_REMOTEDBID_MASTER_ActiveYN DEFAULT('N')
	, [CreatedDateTime] DATETIME NULL CONSTRAINT DF_REMOTEDBID_MASTER_CreatedDateTime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_REMOTEDBID_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_RemoteDBID_Master] PRIMARY KEY ([RemoteDBID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[RemoteDBID_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[RemoteDBID_Master] - Start */
IF OBJECT_ID('[BUSDTA].[RemoteDBID_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RemoteDBID_Master_del]
	(
	  [RemoteDBID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RemoteDBID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[RemoteDBID_Master] - End */
/* TRIGGERS FOR RemoteDBID_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.RemoteDBID_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.RemoteDBID_Master_ins
	ON BUSDTA.RemoteDBID_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.RemoteDBID_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.RemoteDBID_Master_del.RemoteDBID= inserted.RemoteDBID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.RemoteDBID_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.RemoteDBID_Master_upd
	ON BUSDTA.RemoteDBID_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.RemoteDBID_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.RemoteDBID_Master.RemoteDBID= inserted.RemoteDBID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.RemoteDBID_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.RemoteDBID_Master_dlt
	ON BUSDTA.RemoteDBID_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.RemoteDBID_Master_del (RemoteDBID, last_modified )
	SELECT deleted.RemoteDBID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR RemoteDBID_Master - END */

/* [BUSDTA].[Reserve_DeviceEnvironmentRoute] - begins */

/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - Start */
IF OBJECT_ID('[BUSDTA].[Reserve_DeviceEnvironmentRoute]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Reserve_DeviceEnvironmentRoute]
	(
	  [ReservationID] INT NOT NULL IDENTITY(1,1)
	, [DeviceId] VARCHAR(30) NOT NULL
	, [EnvironmentMasterId] INT NOT NULL
	, [RouteID] VARCHAR(30) NOT NULL
	, [AppUserId] INT NOT NULL
	, [ReservedDateTime] DATETIME NOT NULL CONSTRAINT DF_RESERVE_DEVICEENVIRONMENTROUTE_ReservedDateTime DEFAULT(getdate())
	)

END

/* TRIGGERS FOR Reserve_DeviceEnvironmentRoute - END */

/* [BUSDTA].[User_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[User_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Environment_Map]
	(
	  [UserEnvironmentMapId] NUMERIC(8,0) NOT NULL
	, [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [AppUserId] NUMERIC(8,0) NOT NULL
	, [IsEnvAdmin] VARCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL
	, CONSTRAINT [PK_User_Environment_Map] PRIMARY KEY ([UserEnvironmentMapId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[User_Environment_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[User_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Environment_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Environment_Map_del]
	(
	  [UserEnvironmentMapId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UserEnvironmentMapId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[User_Environment_Map] - End */
/* TRIGGERS FOR User_Environment_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.User_Environment_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.User_Environment_Map_ins
	ON BUSDTA.User_Environment_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.User_Environment_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.User_Environment_Map_del.UserEnvironmentMapId= inserted.UserEnvironmentMapId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.User_Environment_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Environment_Map_upd
	ON BUSDTA.User_Environment_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.User_Environment_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.User_Environment_Map.UserEnvironmentMapId= inserted.UserEnvironmentMapId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.User_Environment_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Environment_Map_dlt
	ON BUSDTA.User_Environment_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.User_Environment_Map_del (UserEnvironmentMapId, last_modified )
	SELECT deleted.UserEnvironmentMapId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR User_Environment_Map - END */

/* [BUSDTA].[user_master] - begins */

/* TableDDL - [BUSDTA].[user_master] - Start */
IF OBJECT_ID('[BUSDTA].[user_master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[user_master]
	(
	  [App_user_id] INT NOT NULL IDENTITY(1,1)
	, [App_User] VARCHAR(30) NULL
	, [Name] VARCHAR(30) NULL
	, [DomainUser] VARCHAR(40) NULL
	, [AppPassword] VARCHAR(20) NULL
	, [active] BIT NULL CONSTRAINT DF_USER_MASTER_active DEFAULT((1))
	, [last_modified] DATETIME NULL CONSTRAINT DF_USER_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_user_master] PRIMARY KEY ([App_user_id] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[user_master] - End */

/* SHADOW TABLE FOR [BUSDTA].[user_master] - Start */
IF OBJECT_ID('[BUSDTA].[user_master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[user_master_del]
	(
	  [App_user_id] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([App_user_id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[user_master] - End */
/* TRIGGERS FOR user_master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.user_master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.user_master_ins
	ON BUSDTA.user_master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.user_master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.user_master_del.App_user_id= inserted.App_user_id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.user_master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.user_master_upd
	ON BUSDTA.user_master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.user_master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.user_master.App_user_id= inserted.App_user_id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.user_master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.user_master_dlt
	ON BUSDTA.user_master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.user_master_del (App_user_id, last_modified )
	SELECT deleted.App_user_id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR user_master - END */
/* [dbo].[drh_script_version] - begins */

/* TableDDL - [dbo].[drh_script_version] - Start */
IF OBJECT_ID('[dbo].[drh_script_version]') IS NULL
BEGIN

	CREATE TABLE [dbo].[drh_script_version]
	(
	  [version_id] INT NOT NULL
	, [name] VARCHAR(128) NOT NULL
	, [description] VARCHAR(MAX) NULL
	, CONSTRAINT [PK_drh_script_version] PRIMARY KEY ([version_id] ASC)
	)

END
/* TableDDL - [dbo].[drh_script_version] - End */
/* [dbo].[drh_table] - begins */

/* TableDDL - [dbo].[drh_table] - Start */
IF OBJECT_ID('[dbo].[drh_table]') IS NULL
BEGIN

	CREATE TABLE [dbo].[drh_table]
	(
	  [table_id] INT NOT NULL
	, [schema] VARCHAR(128) NOT NULL
	, [name] VARCHAR(128) NOT NULL
	, CONSTRAINT [PK_drh_table] PRIMARY KEY ([table_id] ASC)
	)

END
/* TableDDL - [dbo].[drh_table] - End */
/* [dbo].[drh_script] - begins */

/* TableDDL - [dbo].[drh_script] - Start */
IF OBJECT_ID('[dbo].[drh_script]') IS NULL
BEGIN

	CREATE TABLE [dbo].[drh_script]
	(
	  [script_id] INT NOT NULL
	, [version_id] INT NOT NULL
	, [table_id] INT NOT NULL
	, [event] VARCHAR(128) NOT NULL
	, [script] VARCHAR(MAX) NOT NULL
	, CONSTRAINT [PK_drh_script] PRIMARY KEY ([version_id] ASC, [table_id] ASC, [event] ASC)
	)

ALTER TABLE [dbo].[drh_script] WITH CHECK ADD CONSTRAINT [FK__drh_scrip__versi__70DDC3D8] FOREIGN KEY([version_id]) REFERENCES [dbo].[drh_script_version] ([version_id])
ALTER TABLE [dbo].[drh_script] CHECK CONSTRAINT [FK__drh_scrip__versi__70DDC3D8]

ALTER TABLE [dbo].[drh_script] WITH CHECK ADD CONSTRAINT [FK__drh_scrip__table__71D1E811] FOREIGN KEY([table_id]) REFERENCES [dbo].[drh_table] ([table_id])
ALTER TABLE [dbo].[drh_script] CHECK CONSTRAINT [FK__drh_scrip__table__71D1E811]

END
/* TableDDL - [dbo].[drh_script] - End */
/* [dbo].[tblReserveConfig] - begins */

/* TableDDL - [dbo].[tblReserveConfig] - Start */
IF OBJECT_ID('[dbo].[tblReserveConfig]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblReserveConfig]
	(
	  [ReserveConfigID] INT NOT NULL IDENTITY(1,1)
	, [ExpireMins] INT NOT NULL
	, [CreatedBy] [numeric](8, 0) NULL
	, [CreatedDatetime] [datetime] NULL default getdate()
	, [UpdatedBy] [numeric](8, 0) NULL
	, [UpdatedDatetime] [datetime] NULL default getdate()
	, CONSTRAINT [PK_tblReserveConfig] PRIMARY KEY ([ReserveConfigID] ASC)
	)

END
/* TableDDL - [dbo].[tblReserveConfig] - End */
/* [dbo].[tblServiceResponses] - begins */

/* TableDDL - [dbo].[tblServiceResponses] - Start */
IF OBJECT_ID('[dbo].[tblServiceResponses]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblServiceResponses]
	(
	  [ServiceResponseID] INT NOT NULL IDENTITY(1,1)
	, [MessageCode] VARCHAR(10) NULL
	, [MessageText] VARCHAR(500) NULL
	, [CreatedBy] [numeric](8, 0) NULL
	, [CreatedDatetime] [datetime] NULL default getdate()
	, [UpdatedBy] [numeric](8, 0) NULL
	, [UpdatedDatetime] [datetime] NULL default getdate()
	, CONSTRAINT [PK_tblServiceResponses] PRIMARY KEY ([ServiceResponseID] ASC)
	)

END
/* TableDDL - [dbo].[tblServiceResponses] - End */
/* [dbo].[tblWebService_ReleaseMaster] - begins */

/* TableDDL - [dbo].[tblWebService_ReleaseMaster] - Start */
IF OBJECT_ID('[dbo].[tblWebService_ReleaseMaster]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblWebService_ReleaseMaster]
	(
	  [ReleaseMasterID] INT NOT NULL IDENTITY(1,1)
	, [ServiceName] VARCHAR(200) NOT NULL
	, [EndPointURL] VARCHAR(1000) NOT NULL
	, [VersionNumber] VARCHAR(100) NOT NULL
	, [HostedServer] VARCHAR(100) NULL
	, [Environment] VARCHAR(100) NULL
	, [CDBServer] VARCHAR(500) NULL
	, [CDBName] VARCHAR(128) NULL
	, [SysDBServer] VARCHAR(500) NULL
	, [SysDBName] VARCHAR(128) NULL
	, [ReleasedDate] DATETIME NOT NULL
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedDateTime] DATETIME NULL CONSTRAINT DF_TBLWEBSERVICE_RELEASEMASTER_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [UpdatedDateTime] DATETIME NULL CONSTRAINT DF_TBLWEBSERVICE_RELEASEMASTER_EditedOn DEFAULT(getdate())
	, CONSTRAINT [PK_tblWebService_ReleaseMaster] PRIMARY KEY ([ReleaseMasterID] ASC)
	)

END
/* TableDDL - [dbo].[tblWebService_ReleaseMaster] - End */

/* [dbo].[tblWebServiceLog] - begins */

/* TableDDL - [dbo].[tblWebServiceLog] - Start */
IF OBJECT_ID('[dbo].[tblWebServiceLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblWebServiceLog]
	(
	  [ServiceLogID] INT NOT NULL
	, [DeviceID] VARCHAR(100) NULL
	, [Request] XML NULL
	, [Response] XML NULL
	, [ResponseOn] DATETIME NULL CONSTRAINT DF_TBLWEBSERVICELOG_ResponseOn DEFAULT(getdate())
	, [Environment] VARCHAR(128) NULL
	, [DBName] VARCHAR(128) NULL
	, [ServiceName] VARCHAR(100) NULL
	, [ServiceEndPoint] VARCHAR(1000) NULL
	, [ServiceContract] VARCHAR(1000) NULL
	, CONSTRAINT [PK_tblWebServiceLog] PRIMARY KEY ([ServiceLogID] ASC)
	)

END
/* TableDDL - [dbo].[tblWebServiceLog] - End */

