/* [dbo].[tblWebService_ReleaseMaster] - begins */

/* TableDDL - [dbo].[tblWebService_ReleaseMaster] - Start */
IF OBJECT_ID('[dbo].[tblWebService_ReleaseMaster]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblWebService_ReleaseMaster]
	(
	  [ReleaseMasterID] INT NOT NULL IDENTITY(1,1)
	, [ServiceName] VARCHAR(200) NOT NULL
	, [EndPointURL] VARCHAR(1000) NOT NULL
	, [VersionNumber] VARCHAR(100) NOT NULL
	, [HostedServer] VARCHAR(100) NULL
	, [Environment] VARCHAR(100) NULL
	, [CDBServer] VARCHAR(500) NULL
	, [CDBName] VARCHAR(128) NULL
	, [SysDBServer] VARCHAR(500) NULL
	, [SysDBName] VARCHAR(128) NULL
	, [ReleasedDate] DATETIME NOT NULL
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedDateTime] DATETIME NULL CONSTRAINT DF_TBLWEBSERVICE_RELEASEMASTER_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [UpdatedDateTime] DATETIME NULL CONSTRAINT DF_TBLWEBSERVICE_RELEASEMASTER_EditedOn DEFAULT(getdate())
	, CONSTRAINT [PK_tblWebService_ReleaseMaster] PRIMARY KEY ([ReleaseMasterID] ASC)
	)

END
/* TableDDL - [dbo].[tblWebService_ReleaseMaster] - End */

