/* [dbo].[drh_script_version] - begins */

/* TableDDL - [dbo].[drh_script_version] - Start */
IF OBJECT_ID('[dbo].[drh_script_version]') IS NULL
BEGIN

	CREATE TABLE [dbo].[drh_script_version]
	(
	  [version_id] INT NOT NULL
	, [name] VARCHAR(128) NOT NULL
	, [description] VARCHAR(MAX) NULL
	, CONSTRAINT [PK_drh_script_version] PRIMARY KEY ([version_id] ASC)
	)

END
/* TableDDL - [dbo].[drh_script_version] - End */
