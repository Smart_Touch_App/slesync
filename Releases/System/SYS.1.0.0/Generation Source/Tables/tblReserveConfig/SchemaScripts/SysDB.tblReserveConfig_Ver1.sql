/* [dbo].[tblReserveConfig] - begins */

/* TableDDL - [dbo].[tblReserveConfig] - Start */
IF OBJECT_ID('[dbo].[tblReserveConfig]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblReserveConfig]
	(
	  [ReserveConfigID] INT NOT NULL IDENTITY(1,1)
	, [ExpireMins] INT NOT NULL
	, [CreatedBy] [numeric](8, 0) NULL
	, [CreatedDatetime] [datetime] NULL default getdate()
	, [UpdatedBy] [numeric](8, 0) NULL
	, [UpdatedDatetime] [datetime] NULL default getdate()
	, CONSTRAINT [PK_tblReserveConfig] PRIMARY KEY ([ReserveConfigID] ASC)
	)

END
/* TableDDL - [dbo].[tblReserveConfig] - End */
