
/* [BUSDTA].[Device_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[Device_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Environment_Map]
	(
	  [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [DeviceId] VARCHAR(30) NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_DEVICE_ENVIRONMENT_MAP_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_DEVICE_ENVIRONMENT_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Device_Environment_Map] PRIMARY KEY ([EnvironmentMasterId] ASC, [DeviceId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Device_Environment_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Device_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Environment_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Environment_Map_del]
	(
	  [EnvironmentMasterId] NUMERIC(8,0)
	, [DeviceId] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EnvironmentMasterId] ASC, [DeviceId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Device_Environment_Map] - End */
/* TRIGGERS FOR Device_Environment_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Device_Environment_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Device_Environment_Map_ins
	ON BUSDTA.Device_Environment_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Device_Environment_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Device_Environment_Map_del.DeviceId= inserted.DeviceId AND BUSDTA.Device_Environment_Map_del.EnvironmentMasterId= inserted.EnvironmentMasterId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Device_Environment_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Environment_Map_upd
	ON BUSDTA.Device_Environment_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Device_Environment_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Device_Environment_Map.DeviceId= inserted.DeviceId AND BUSDTA.Device_Environment_Map.EnvironmentMasterId= inserted.EnvironmentMasterId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Device_Environment_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Environment_Map_dlt
	ON BUSDTA.Device_Environment_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Device_Environment_Map_del (DeviceId, EnvironmentMasterId, last_modified )
	SELECT deleted.DeviceId, deleted.EnvironmentMasterId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Device_Environment_Map - END */