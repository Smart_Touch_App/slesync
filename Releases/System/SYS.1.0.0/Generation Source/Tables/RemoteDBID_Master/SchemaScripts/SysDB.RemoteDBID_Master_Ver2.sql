
/* [BUSDTA].[RemoteDBID_Master] - begins */

/* TableDDL - [BUSDTA].[RemoteDBID_Master] - Start */
IF OBJECT_ID('[BUSDTA].[RemoteDBID_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RemoteDBID_Master]
	(
	  [RemoteDBID] INT NOT NULL
	, [AppUserId] INT NULL
	, [DeviceID] VARCHAR(30) NULL
	, [RouteID] VARCHAR(30) NULL
	, [EnvironmentMasterID] INT NULL
	, [ActiveYN] CHAR(1) NOT NULL CONSTRAINT DF_REMOTEDBID_MASTER_ActiveYN DEFAULT('N')
	, [CreatedDateTime] DATETIME NULL CONSTRAINT DF_REMOTEDBID_MASTER_CreatedDateTime DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_REMOTEDBID_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_RemoteDBID_Master] PRIMARY KEY ([RemoteDBID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[RemoteDBID_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[RemoteDBID_Master] - Start */
IF OBJECT_ID('[BUSDTA].[RemoteDBID_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[RemoteDBID_Master_del]
	(
	  [RemoteDBID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RemoteDBID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[RemoteDBID_Master] - End */
/* TRIGGERS FOR RemoteDBID_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.RemoteDBID_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.RemoteDBID_Master_ins
	ON BUSDTA.RemoteDBID_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.RemoteDBID_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.RemoteDBID_Master_del.RemoteDBID= inserted.RemoteDBID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.RemoteDBID_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.RemoteDBID_Master_upd
	ON BUSDTA.RemoteDBID_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.RemoteDBID_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.RemoteDBID_Master.RemoteDBID= inserted.RemoteDBID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.RemoteDBID_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.RemoteDBID_Master_dlt
	ON BUSDTA.RemoteDBID_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.RemoteDBID_Master_del (RemoteDBID, last_modified )
	SELECT deleted.RemoteDBID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR RemoteDBID_Master - END */
