
-- =============================================
-- Author		Sakaravarthi J
-- Create date 08-Jan-2016
-- Description	Get Latest Application's Version  RemoteDB Release script Name
-- =============================================
CREATE PROCEDURE [dbo].[uspGetRemoteDBInfo] 
(
	@CdbVersion VARCHAR(50)=NULL,
	@SysDBVersion VARCHAR(50)=NULL
)
AS
BEGIN
	SELECT TOP 1 ReleaseName, SysDBVersion, CDBVersion 
	FROM vwRemoteDBRelease
	WHERE ((@CdbVersion IS NOT NULL AND CDBVersion = @CdbVersion) OR (@CdbVersion IS NULL))
		AND ((@SysDBVersion IS NOT NULL AND SysDBVersion = @SysDBVersion) OR (@SysDBVersion IS NULL))
	ORDER BY ReleaseName DESC
END
GO
