CREATE PROCEDURE [dbo].[uspValidateRegisteration]
(
	@DeviceId VARCHAR(50)
)
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins=(ExpireMins*-1) FROM dbo.tblReserveConfig 

	SELECT ISNULL(ReservationID,0) AS ReservationID, rdb.DeviceID, em.EnvName, rdb.RouteID, rdb.ActiveYN,ReservedDateTime
	FROM BUSDTA.RemoteDBID_Master AS rdb 
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der ON rdb.DeviceID=der.DeviceID AND rdb.EnvironmentMasterID=der.EnvironmentMasterID AND rdb.RouteID=der.RouteID 
	LEFT JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=rdb.EnvironmentMasterId  
	WHERE rdb.DeviceId=@DeviceId AND ISNULL(ReservedDateTime, GETDATE())>=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO