CREATE PROCEDURE [dbo].[Reserve_DeviceEnvironmentRoute]
(
	@DeviceId VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@RouteID VARCHAR(30),
	@AppUserName VARCHAR(30)
)
AS
BEGIN
DECLARE @EnvironmentMasterId INT, @AppUserId INT, @rdb_script_location VARCHAR(MAX), @RemoteDBID INT;
SELECT @EnvironmentMasterId = EnvironmentMasterId, @rdb_script_location=RDBScriptLocation FROM BUSDTA.Environment_Master WHERE EnvName=@EnvironmentName
SELECT @AppUserId = App_User_ID FROM BUSDTA.User_Master WHERE DomainUser=@AppUserName
SELECT @RemoteDBID = ISNULL(MAX(RemoteDBID),1000) FROM BUSDTA.RemoteDBID_Master

	IF NOT EXISTS(SELECT 1 FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		INSERT INTO BUSDTA.Reserve_DeviceEnvironmentRoute(DeviceId, EnvironmentMasterId, RouteID, AppUserId)
		SELECT @DeviceId, @EnvironmentMasterId, @RouteID, @AppUserId
	END
	ELSE
	BEGIN
		UPDATE BUSDTA.Reserve_DeviceEnvironmentRoute SET ReservedDateTime=GETDATE() 
		WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID
	END

	--IF NOT EXISTS(SELECT 1 FROM BUSDTA.RemoteDBID_Master WHERE DeviceId=@DeviceId AND EnvironmentMasterId=@EnvironmentMasterId AND RouteID=@RouteID)
	BEGIN
		SET @RemoteDBID=@RemoteDBID+1
		INSERT INTO BUSDTA.RemoteDBID_Master (RemoteDBID, AppUserId, DeviceID, RouteID, EnvironmentMasterID)
		VALUES (@RemoteDBID, @AppUserId, @DeviceId, @RouteID,@EnvironmentMasterId)
	END

	--SELECT @rdb_script_location RDBScriptLocation, @RemoteDBID RemoteDBID

	SELECT AppType, AppName, ReleasedDate, AppLocation, RemoteDBID = CASE WHEN AppType ='RemoteDB' THEN @RemoteDBID ELSE NULL END
	FROM (
		select at.AppType,
		ROW_NUMBER() OVER(PARTITION BY am.AppTypeID ORDER BY am.AppReleaseID DESC) Latest,
		am.AppName, am.ReleasedDate, am.AppLocation, at.SortOrder
		from BUSDTA.Environment_Master em
		inner join tblAppReleaseMaster am on em.EnvironmentMasterId=am.EnvironmentMasterId
		left join tblAppTypes at on at.AppTypeID=am.AppTypeID
		WHERE at.ClientSetup=1 and em.EnvironmentMasterId=@EnvironmentMasterId
	)AS k WHERE latest=1 order by SortOrder
END
GO
