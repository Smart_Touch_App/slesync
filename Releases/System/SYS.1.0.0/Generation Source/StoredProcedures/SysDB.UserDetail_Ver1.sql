CREATE PROCEDURE [dbo].[UserDetail] 
(
	@DomainUser nvarchar(50) 
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT App_user_id, App_User, Name, DomainUser, AppPassword, active FROM BUSDTA.user_master
	WHERE DomainUser= @DomainUser
	SET NOCOUNT ON;
END
GO