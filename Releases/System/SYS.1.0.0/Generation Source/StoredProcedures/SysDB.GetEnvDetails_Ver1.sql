CREATE PROCEDURE [dbo].[GetEnvDetails]
(
	@AppUser nvarchar(50) 
)
AS
BEGIN
SET NOCOUNT ON;
	SELECT UM.App_User, EM.EnvironmentMasterId, EM.EnvName, EM.EnvDescription, RSM.Route_Id
		FROM BUSDTA.user_master UM
		INNER JOIN VW_All_RouteUserMap RSM ON RSM.App_user_id = UM.App_user_id
		INNER JOIN [BUSDTA].[User_Environment_Map] UEMAP ON UM.App_user_id = UEMAP.AppUserId 
		INNER JOIN [BUSDTA].[Environment_Master] EM ON EM.EnvironmentMasterId = UEMAP.EnvironmentMasterId AND RSM.EnvName = EM.EnvName
	WHERE UM.App_User = @AppUser
SET NOCOUNT OFF;
END


GO