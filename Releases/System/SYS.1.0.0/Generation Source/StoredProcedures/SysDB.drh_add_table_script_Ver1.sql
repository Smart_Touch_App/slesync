CREATE PROCEDURE [dbo].[drh_add_table_script]
    @version	VARCHAR(128),
    @schema_table		VARCHAR(256),
    @event		VARCHAR(128),
    @script		VARCHAR(MAX)
AS
BEGIN
    DECLARE @version_id 	INTEGER;
    DECLARE @table_id 		INTEGER;
    DECLARE @script_id 		INTEGER;
    DECLARE @upd_script_id	INTEGER;
    DECLARE @table		VARCHAR(128);
    DECLARE @schema		VARCHAR(128);
	
	IF(CHARINDEX('.', @schema_table)>0)
	BEGIN
		SELECT @schema=SUBSTRING(@schema_table, 1, CHARINDEX('.', @schema_table)-1), @table=SUBSTRING(@schema_table, CHARINDEX('.', @schema_table)+1, LEN(@schema_table))
	END
	ELSE
	BEGIN
		SELECT @table=@schema_table, @schema=schema_name();
	END

	SELECT @version_id = version_id FROM drh_script_version 
        WHERE name = @version;
	
	IF @version_id is null BEGIN
	    -- Insert to the drh_script_version
	    SELECT @version_id = MAX( version_id )+1 FROM drh_script_version;
	    IF @version_id is null BEGIN
			-- No rows are currently in drh_script_version
			SET @version_id = 1;
	    END;
	    INSERT INTO drh_script_version ( version_id, name ) VALUES ( @version_id, @version );
	END;
	
	SELECT @table_id = table_id FROM drh_table 
        WHERE [schema]=@schema AND name = @table;
	
	IF @table_id is null BEGIN
	    -- Insert to the drh_table
	    SELECT @table_id = MAX( table_id )+1 FROM drh_table;
	    IF @table_id is null BEGIN
			-- No rows are currently in drh_table
			SET @table_id = 1;
	    END;
	    INSERT INTO drh_table( table_id, [schema], name ) VALUES ( @table_id, @schema, @table );
	END;

	SELECT @script_id = MAX( script_id )+1 FROM drh_script;
	IF @script_id IS NULL BEGIN
	    -- No rows are currently in drh_script
	    SET @script_id = 1;
	END;
	select @upd_script_id = script_id from drh_script where version_id = @version_id and table_id = @table_id and [event] = @event
	IF @upd_script_id IS NULL BEGIN
		INSERT INTO drh_script (script_id, version_id, table_id, [event], script) VALUES (@script_id,@version_id, @table_id,@event,@script);
	END
	ELSE BEGIN
		UPDATE drh_script SET script = @script WHERE script_id=@upd_script_id
	END

END

GO