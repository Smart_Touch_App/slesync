CREATE PROCEDURE [dbo].[uspCheckEnvironmentAvailability]
(
	@App_User VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@DeviceID VARCHAR(30)
)
AS
BEGIN
	SELECT um.App_User, dem.DeviceId, em.EnvName, em.CDBConKey
	FROM BUSDTA.Device_Environment_Map AS dem
	INNER JOIN BUSDTA.User_Environment_Map AS uem ON dem.EnvironmentMasterId=uem.EnvironmentMasterId 
	INNER JOIN BUSDTA.User_Master AS um ON uem.AppUserId=um.App_user_id
	INNER JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=uem.EnvironmentMasterId
	WHERE dem.DeviceId=@DeviceID AND um.App_User=@App_User AND em.EnvName=@EnvironmentName 
END

GO