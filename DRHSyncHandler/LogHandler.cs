﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DRH
{
    public static class LogHandler
    {
        public static void LogText(string message, string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                }
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "\t" + "-" + "\t" + message);
                    sw.Close();
                }
            }
        }

        public static void ClearLog(string path)
        {
            if (!string.IsNullOrEmpty(path))
            {
                if (File.Exists(path))
                    File.Delete(path);
            }
        }
    }
}
