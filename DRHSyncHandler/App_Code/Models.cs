﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DRH
{

    public class ColumnAttributes
    {
        public string ColumnName { get; set; }
        public bool IsPK { get; set; }
        public string DataType { get; set; }
    }

    public class DownloadTable
    {
        public DownloadTable(string schema, string table, DateTime lastdownload, string scriptversion)
        {
            this.SchemaName = schema;
            this.TableName = table;
            this.LastDownload = lastdownload;
            this.ScriptVersion = scriptversion;
        }
        public string SchemaName { get; set; }
        public string TableName { get; set; }
        public DateTime LastDownload { get; set; }
        public string ScriptVersion { get; set; }
    }

}
