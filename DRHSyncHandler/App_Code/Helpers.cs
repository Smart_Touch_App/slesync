﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;
using iAnywhere.MobiLink.Script;

namespace DRH
{
    public static class Helpers
    {
        public static String getTableSchema(SqlConnection conn, string TableName)
        {
            List<ColumnAttributes> listCA = new List<ColumnAttributes>();
            try
            {
                if (conn.State == ConnectionState.Closed) { conn.Open(); }
                DataSet ds = new DataSet(); string table_schema = "dbo";
                SqlCommand cmd = new SqlCommand("SELECT TABLE_SCHEMA FROM INFORMATION_SCHEMA.TABLES WHERE Table_Name='" + TableName + "'", conn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);

                da.Fill(ds);
                if (ds != null)
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            table_schema = Convert.ToString(dr["TABLE_SCHEMA"]);
                        }
                    }
                }
                return table_schema;
            }
            catch (Exception ex)
            {
                return "dbo";
            }
        }

        public static string getAddTimestampCol(string exec_query, string last_download, DateTime last_download_time)
        {
            string download_query = "";
            try
            {
                string lastdwd=string.Concat("'",last_download_time.ToString("dd-MMM-yyyy hh:mm:ss tt"),"'");
                download_query = exec_query.Replace("{drh s." + last_download + "}", lastdwd);
                return download_query;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
                
        public static string getEventQuery(DownloadTable objDownloadTable, string event_name, SqlConnection conn)
        {
            string download_query = "";
            try
            {
                if (conn.State == ConnectionState.Closed) { conn.Open(); }
                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand("usp_Event_Scripts", conn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("script_version", System.Data.SqlDbType.VarChar).Value = objDownloadTable.ScriptVersion;
                cmd.Parameters.Add("table_schema", System.Data.SqlDbType.VarChar).Value = objDownloadTable.SchemaName;
                cmd.Parameters.Add("table_name", System.Data.SqlDbType.VarChar).Value = objDownloadTable.TableName;
                cmd.Parameters.Add("event_name", System.Data.SqlDbType.VarChar).Value = event_name;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                
                da.Fill(ds);
                if (ds != null)
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        foreach (DataColumn dc in dt.Columns)
                        {
                            download_query = Convert.ToString(dt.Rows[0][dc.ColumnName]);
                        }
                    }
                }
                return download_query;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string setColValue(string exec_query, IDataReader dr)
        {
            try
            {
                //LogHandler.LogText("before : " + exec_query);
                DataTable dt = dr.GetSchemaTable();
                DataColumnCollection cc = dt.Columns; DataColumn dc;

                var replacements = new Dictionary<string, string>();
                for (int c = 0; c < cc.Count; c = c + 1)
                {
                    dc = cc[c];
                    string colName = string.Concat("{drh r.", dc.ColumnName, "}");
                    string columnValue = "";
                    //LogHandler.LogText("col : " + colName, log_file_path);

                    columnValue = (Convert.ToString(dr[dc.ColumnName]) == string.Empty || Convert.ToString(dr[dc.ColumnName]) == "") ? "null" : Convert.ToString(dr[dc.ColumnName]);
                    //LogHandler.LogText("new col : " + columnValue, log_file_path);
                    if (columnValue != "null")
                    {
                        if (dc.DataType == typeof(DateTime))
                        {
                            columnValue = string.Concat("'", Convert.ToDateTime(dr[dc.ColumnName]).ToString("dd-MMM-yyyy hh:mm:ss tt"), "'");
                        }
                        else if (dc.DataType == typeof(bool))
                        {
                            columnValue = string.Concat("'", Convert.ToBoolean(dr[dc.ColumnName]).ToString(), "'");
                        }
                        else if (dc.DataType == typeof(int))
                        {
                            columnValue = Convert.ToString(dr[dc.ColumnName]);
                        }
                        else
                        {
                            columnValue = string.Concat("'", Convert.ToString(dr[dc.ColumnName]).Replace("\"",""), "'");
                        }
                    }
                    replacements.Add(colName, columnValue);
                }

                foreach (var replacement in replacements)
                {
                    exec_query = exec_query.Replace(replacement.Key.Replace("\"", ""), replacement.Value);
                }
                //LogHandler.LogText("after : " + exec_query, log_file_path);

                return exec_query;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static string setDelColValue(string exec_query, IDataReader dr)
        {
            try
            {
                //LogHandler.LogText("before : " + exec_query);
                exec_query = exec_query.Replace("\"", "");
                DataTable dt = dr.GetSchemaTable();
                DataColumnCollection cc = dt.Columns; DataColumn dc;

                var replacements = new Dictionary<string, string>();
                for (int c = 0; c < dr.FieldCount; c = c + 1)
                {
                    dc = cc[c];
                    
                    string colName = string.Concat("{drh r.", dc.ColumnName, "}");
                    string columnValue = "";

                    columnValue = (Convert.ToString(dr[dc.ColumnName]) == string.Empty || Convert.ToString(dr[dc.ColumnName]) == "") ? "null" : Convert.ToString(dr[dc.ColumnName]).Replace("\"","");
                    if (columnValue != "null")
                    {
                        if (dc.DataType == typeof(DateTime))
                        {
                            columnValue = string.Concat("'", Convert.ToDateTime(dr[dc.ColumnName]).ToString("dd-MMM-yyyy hh:mm:ss tt"), "'");
                        }
                        else if (dc.DataType == typeof(int))
                        {
                            columnValue = Convert.ToString(dr[dc.ColumnName]);
                        }
                        else
                        {
                            columnValue = string.Concat("'", Convert.ToString(dr[dc.ColumnName]), "'");
                        }
                    }
                    replacements.Add(colName, columnValue);
                }

                foreach (var replacement in replacements)
                {
                    exec_query = exec_query.Replace(replacement.Key, replacement.Value);
                }
                //LogHandler.LogText("after : " + exec_query);

                return exec_query;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }


        public static string getNonMobilinkDS(DBConnection ConsDB)
        {
            DBCommand cmd = ConsDB.CreateCommand();
            cmd.CommandText = "SELECT TOP 1 KeyValue FROM tblSystemSetup WHERE KeyName='SystemDBConnection' AND ActiveYN=1 ORDER BY SystemSetupID DESC";

            DBRowReader reader = cmd.ExecuteReader();
            DBRowReader row = reader;

            int r = 0; string conneciton_string = "";
            foreach (var val in reader.NextRow())
            {
                string ColumnName = row.ColumnNames[r].ToString();
                if (ColumnName == "KeyValue") { conneciton_string = val.ToString(); }
                r++;
            }

            return conneciton_string;
        }

        public static string getLogFileLocation(DBConnection ConsDB)
        {
            DBCommand cmd = ConsDB.CreateCommand();
            cmd.CommandText = "SELECT TOP 1 KeyValue FROM tblSystemSetup WHERE KeyName='LogFileLocaiton' AND ActiveYN=1 ORDER BY SystemSetupID DESC";

            DBRowReader reader = cmd.ExecuteReader();
            DBRowReader row = reader;

            int r = 0; string log_file_location = "";
            foreach (var val in reader.NextRow())
            {
                string ColumnName = row.ColumnNames[r].ToString();
                if (ColumnName == "KeyValue") { log_file_location = val.ToString(); }
                r++;
            }

            return log_file_location;
        }
    }
}
