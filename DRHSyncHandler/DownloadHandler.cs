﻿using System;
using System.Data;
using iAnywhere.MobiLink.Script;
using System.Collections.Generic;

namespace DRH
{
    /// <summary>
    /// Tests that scripts are called correctly for most sync events.
    /// Following script to be executed at CDB
    /// exec ml_add_dnet_connection_script
    ///   'ver1', 'handle_DownloadData',
    ///   'DRH.DownloadHandler.HandleDownload'

    /// </summary>
    public class DownloadHandler
    {
        private readonly string dbConnection;
        private readonly string log_file_path;

        private DBConnectionContext _cc;

        private string script_version = "";
        public DownloadHandler(DBConnectionContext cc)
        {
            try
            {
                log_file_path = Helpers.getLogFileLocation(cc.GetConnection());
                LogHandler.LogText("*********************************", log_file_path);
                LogHandler.LogText("DRH.DownloadHandler - Constructor", log_file_path);
                _cc = cc;
                script_version = cc.GetVersion();
                dbConnection = Helpers.getNonMobilinkDS(cc.GetConnection());
                LogHandler.LogText("DRH.DownloadHandler - DBConnectionContext initiated " + script_version + "; DB: " + dbConnection, log_file_path);
            }
            catch (Exception ex)
            {
                LogHandler.LogText("DRH.DownloadHandler - error", log_file_path);
                LogHandler.LogText(ex.Message.ToString(), log_file_path);
            }
        }

        ~DownloadHandler()
        {
        }

        public void HandleDownload()
        {
            LogHandler.LogText("DRH.DownloadHandler.HandleDownload", log_file_path);
            string commandtext = "";
            try
            {
                DataSet ds = new DataSet();
                // Get DownloadData instance for current synchronization.
                DownloadData my_dd = _cc.GetDownloadData();
                string Table_Schema = "dbo";
                
                #region download table
                // to list all tables to be downloaded
                DownloadTableData[] tdArray = my_dd.GetDownloadTables();
                foreach (DownloadTableData td in tdArray)
                {
                    // get current iteration table name
                    string current_Table = td.GetName();
                    DateTime last_download_time = td.GetLastDownloadTime();
                    LogHandler.LogText("********************************", log_file_path);
                    LogHandler.LogText(string.Format("Tables in the download insert/update: {0} - LastDownloadTime: {1}", current_Table, last_download_time.ToString("dd-MMM-yyyy hh:mm:ss tt")), log_file_path);

                    #region Upsert operation - table

                    #region fetch download insert/update dataset
                    using (var connection = new System.Data.SqlClient.SqlConnection(dbConnection))
                    {
                        connection.Open();
                        #region getTableSchema
                        Table_Schema = Helpers.getTableSchema(connection, current_Table);
                        LogHandler.LogText(string.Format("Table_Schema: {0} ", Table_Schema), log_file_path);
                        #endregion

                        DownloadTable objDownloadTable = new DownloadTable(Table_Schema, current_Table, last_download_time, script_version);
                        
                        //get exec_query from non mobilink source for the current table (drh_script)
                        string exec_query = Helpers.getEventQuery(objDownloadTable, "download_cursor", connection);
                        LogHandler.LogText(string.Format("sv: {0}; ts: {1}; tn: {2}; en: {3}", script_version, Table_Schema, current_Table, "download_cursor"), log_file_path);

                        //Add timestamp
                        exec_query = Helpers.getAddTimestampCol(exec_query, "last_table_download", last_download_time);
                        LogHandler.LogText(string.Format("exec_query: {0} ", exec_query), log_file_path);
                        //commandtext = exec_query;
                        if (exec_query != string.Empty)
                        {
                            using (var comm = new System.Data.SqlClient.SqlCommand(exec_query, connection))
                            {
                                using (var da = new System.Data.SqlClient.SqlDataAdapter(comm))
                                {
                                    ds = new DataSet();
                                    da.Fill(ds);

                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        LogHandler.LogText(string.Format("Found {0} Rows in Other Source ", ds.Tables[0].Rows.Count), log_file_path);
                                }
                            }
                        }
                        else
                        {
                            LogHandler.LogText(string.Format("Table {0} does not have {1} script", current_Table, "download_cursor"), log_file_path);
                        }
                    }
                    #endregion

                    #region process download insert/update into remote database
                    if (td != null)
                    {
                        // Get an IDbCommand for upsert (update/insert) operations.
                        IDbCommand upsert_stmt = td.GetUpsertCommand();
                        IDataParameterCollection parameters = upsert_stmt.Parameters;
                        LogHandler.LogText(string.Format("Adding {0} from other Source", current_Table), log_file_path);
                        
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            DataColumnCollection cc = ds.Tables[0].Columns;

                            int rIndex = 1;
                            foreach (DataRow r in ds.Tables[0].Rows)
                            {
                                LogHandler.LogText(string.Format("******************************** - Row : {0}", rIndex), log_file_path);
                                int cIndex = 0;
                                #region apply param values
                                foreach (DataColumn catt in cc)
                                {
                                    // Set values for one row.
                                    parameters = upsert_stmt.Parameters;
                                    if (r[catt.ColumnName] == DBNull.Value)
                                    {
                                        //LogHandler.LogText(string.Format("Col{0} {1} - {2} : ", cIndex, catt.ColumnName, catt.DataType, "NULL"));
                                        ((IDataParameter)(parameters[cIndex])).Value = DBNull.Value;
                                    }
                                    else
                                    {
                                        //LogHandler.LogText(string.Format("Col{0} {1} - {2} : {3} ", cIndex, catt.ColumnName, catt.DataType, r[catt.ColumnName]));
                                        if (catt.DataType == typeof(int))
                                        {
                                            ((IDataParameter)(parameters[cIndex])).Value = Convert.ToInt32(r[catt.ColumnName]);
                                        }
                                        else if (catt.DataType == typeof(bool))
                                        {
                                            ((IDataParameter)(parameters[cIndex])).Value = Convert.ToBoolean(r[catt.ColumnName]);
                                        }
                                        else if (catt.DataType==typeof(DateTime))
                                        {
                                            ((IDataParameter)(parameters[cIndex])).Value = Convert.ToDateTime(r[catt.ColumnName]).ToString("dd-MMM-yyyy hh:mm:ss tt");
                                        }
                                        else
                                        {
                                            ((IDataParameter)(parameters[cIndex])).Value = Convert.ToString(r[catt.ColumnName]);
                                        }
                                    }
                                    cIndex++;
                                }
                                #endregion
                                // execute to remote database (if pk matched then update else insert)
                                int update_result = upsert_stmt.ExecuteNonQuery();
                                rIndex++;
                            }
                        }
                    }
                    #endregion

                    #endregion

                    #region delete operation - table

                    #region fetch download delete dataset
                    using (var connection = new System.Data.SqlClient.SqlConnection(dbConnection))
                    {
                        connection.Open();
                        #region getTableSchema
                        Table_Schema = Helpers.getTableSchema(connection, current_Table);
                        //LogHandler.LogText(string.Format("Table_Schema: {0} ", Table_Schema));
                        #endregion

                        DownloadTable objDownloadTable = new DownloadTable(Table_Schema, current_Table, last_download_time, script_version);

                        //get exec_query from non mobilink source for the current table (drh_script)
                        string exec_query = Helpers.getEventQuery(objDownloadTable, "download_delete_cursor", connection);
                        //Add timestamp
                        exec_query = Helpers.getAddTimestampCol(exec_query, "last_table_download", last_download_time);
                        commandtext = exec_query;

                        //LogHandler.LogText(string.Format("exec_query: {0} ", exec_query));
                        if (exec_query != string.Empty || exec_query == "--{drh_ignore}")
                        {
                            using (var comm = new System.Data.SqlClient.SqlCommand(exec_query, connection))
                            {
                                using (var da = new System.Data.SqlClient.SqlDataAdapter(comm))
                                {
                                    ds = new DataSet();
                                    da.Fill(ds);

                                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                                        LogHandler.LogText(string.Format("Found {0} Rows in Other Source ", ds.Tables[0].Rows.Count), log_file_path);
                                }
                            }
                        }
                        else
                        {
                            LogHandler.LogText(string.Format("Table {0} does not implemented {1} event", current_Table, "download_delete_cursor"), log_file_path);
                        }
                    }
                    #endregion

                    #region process download delete into remote database
                    if (td != null)
                    {
                        // Get an IDbCommand for delete operations.
                        IDbCommand del_stmt = td.GetDeleteCommand();
                        IDataParameterCollection del_parameters = del_stmt.Parameters;

                        commandtext = del_stmt.CommandText;
                        LogHandler.LogText(string.Format("deleting {0} from other Source", current_Table), log_file_path);
                        if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            DataColumnCollection cc = ds.Tables[0].Columns;

                            int rIndex = 1;
                            foreach (DataRow r in ds.Tables[0].Rows)
                            {
                                LogHandler.LogText(string.Format("******************************** - Row : {0}", rIndex), log_file_path);
                                int cIndex = 0;
                                #region apply param values
                                foreach (DataColumn catt in cc)
                                {
                                    // Set values for one row.
                                    del_parameters = del_stmt.Parameters;
                                    if (r[catt.ColumnName] == DBNull.Value)
                                    {
                                        //LogHandler.LogText(string.Format("Col{0} {1} - {2} : ", cIndex, catt.ColumnName, catt.DataType, "NULL"));
                                        ((IDataParameter)(del_parameters[cIndex])).Value = DBNull.Value;
                                    }
                                    else
                                    {
                                        //LogHandler.LogText(string.Format("Col{0} {1} - {2} : {3} ", cIndex, catt.ColumnName, catt.DataType, r[catt.ColumnName]));
                                        if (catt.DataType == typeof(int))
                                        {
                                            ((IDataParameter)(del_parameters[cIndex])).Value = Convert.ToInt32(r[catt.ColumnName]);
                                        }
                                        else if (catt.DataType == typeof(bool))
                                        {
                                            ((IDataParameter)(del_parameters[cIndex])).Value = Convert.ToBoolean(r[catt.ColumnName]);
                                        }
                                        else if (catt.DataType == typeof(DateTime))
                                        {
                                            ((IDataParameter)(del_parameters[cIndex])).Value = Convert.ToDateTime(r[catt.ColumnName]).ToString("dd-MMM-yyyy hh:mm:ss tt");
                                        }
                                        else
                                        {
                                            ((IDataParameter)(del_parameters[cIndex])).Value = Convert.ToString(r[catt.ColumnName]);
                                        }
                                    }
                                    cIndex++;
                                }
                                #endregion

                                commandtext = del_stmt.CommandText;
                                // execute to remote database (if pk matched then update else insert)
                                int update_result = del_stmt.ExecuteNonQuery();
                                rIndex++;
                            }
                        }
                    }
                    #endregion

                    #endregion

                }
                #endregion
            }
            catch (Exception ex)
            {
                LogHandler.LogText(ex.Message.ToString(), log_file_path);
                LogHandler.LogText("CommandText: " + commandtext, log_file_path);
            }
        }

    }
}