﻿using System;
using System.Data;
using iAnywhere.MobiLink.Script;
using System.Data.SqlClient;
using System.Collections.Generic;

namespace DRH
{
    /// <summary>
    /// Tests that scripts are called correctly for most sync events.
    /// Following script to be executed at CDB
    /// exec ml_add_dnet_connection_script
    ///   'ver1', 'handle_UploadData',
    ///   'DRH.UploadHandler.HandleUpload'

    /// </summary>
    public class UploadHandler
    {
        private readonly string dbConnection ;
        private readonly string log_file_path;

        private DBConnectionContext _cc;
        string tableName = "";
        private string script_version = "";
        public UploadHandler(DBConnectionContext cc)
        {
            try
            {
                log_file_path = Helpers.getLogFileLocation(cc.GetConnection());
                LogHandler.ClearLog(log_file_path);
                LogHandler.LogText("*******************************", log_file_path);
                LogHandler.LogText("DRH.UploadHandler - Constructor", log_file_path);
                _cc = cc;
                script_version = cc.GetVersion();
                dbConnection = Helpers.getNonMobilinkDS(cc.GetConnection());
                LogHandler.LogText("DRH.UploadHandler - DBConnectionContext initiated " + script_version + "; DB: " + dbConnection, log_file_path);
            }
            catch (Exception ex)
            {
                LogHandler.LogText("DRH.UploadHandler - error", log_file_path);
                LogHandler.LogText(ex.Message.ToString(), log_file_path);
            }
        }

        ~UploadHandler()
        {
        }

        public void HandleUpload(UploadData ut)
        {
            LogHandler.LogText("DRH.UploadHandler.HandleUpload", log_file_path);
            try
            {
                using (var connection = new SqlConnection(dbConnection))
                {
                    connection.Open();
                    // fetch 
                    UploadedTableData[] tables = ut.GetUploadedTables(); int i = 0;
                    foreach (UploadedTableData utd in tables)
                    {
                        int current_index = i++;
                        string tableName = utd.GetName();
                        LogHandler.LogText("Table" + current_index + " name: " + tableName, log_file_path);

                        #region getTableSchema
                        string Table_Schema = Helpers.getTableSchema(connection, tableName);
                        //LogHandler.LogText(string.Format("Table_Schema: {0} ", Table_Schema));
                        #endregion
                        
                        DownloadTable objUploadTable = new DownloadTable(Table_Schema, tableName, DateTime.Now, script_version);

                        LogHandler.LogText("Inserting...", log_file_path);
                        drhUpload_Insert(utd.GetInserts(), connection, objUploadTable);

                        LogHandler.LogText("Updating...", log_file_path);
                        drhUpload_Update(utd.GetUpdates(), connection, objUploadTable);

                        LogHandler.LogText("Deleting...", log_file_path);
                        drhUpload_Delete(utd.GetDeletes(), connection, objUploadTable);

                    }
                }
            }
            catch (Exception ex)
            {
                LogHandler.LogText("DRH.HandleUpload - error", log_file_path);
                LogHandler.LogText(ex.Message.ToString(), log_file_path);
            }
            LogHandler.LogText("Data Added from other Source", log_file_path);
        }

        #region inserts drh_upload insert
        public void drhUpload_Insert(IDataReader dr, SqlConnection conn, DownloadTable objUploadTable)
        {
            try
            {
                while (dr.Read())
                {
                    string exec_query = Helpers.getEventQuery(objUploadTable, "upload_insert", conn);
                    exec_query = Helpers.setColValue(exec_query, dr);
                    LogHandler.LogText(exec_query, log_file_path);

                    if (exec_query != string.Empty)
                    {
                        SqlCommand cmd = new SqlCommand(exec_query, conn);
                        cmd.ExecuteNonQuery();
                    }
                }
                dr.Close();
            }
            catch (Exception ex)
            {
                LogHandler.LogText("Error upload_insert : " + ex.Message, log_file_path);
            }
        }
        #endregion

        #region updates drh_upload update
        public void drhUpload_Update(UpdateDataReader utr, SqlConnection conn, DownloadTable objUploadTable)
        {
            try
            {
                while (utr.Read())
                {
                    string exec_query = Helpers.getEventQuery(objUploadTable, "upload_update", conn);
                    exec_query = Helpers.setColValue(exec_query, utr);
                    LogHandler.LogText("plain query : "+exec_query, log_file_path);

                    if (exec_query != string.Empty)
                    {
                        SqlCommand cmd = new SqlCommand(exec_query, conn);
                        cmd.ExecuteNonQuery();
                    }
                }

                utr.Close();
            }
            catch (Exception ex)
            {
                LogHandler.LogText("Error upload_update : " + ex.Message, log_file_path);
            }
        }
        #endregion

        #region inserts drh_upload delete
        public void drhUpload_Delete(IDataReader dr, SqlConnection conn, DownloadTable objUploadTable)
        {
            try
            {
                while (dr.Read())
                {
                    string exec_query = Helpers.getEventQuery(objUploadTable, "upload_delete", conn);
                    exec_query = Helpers.setDelColValue(exec_query, dr);
                    LogHandler.LogText(exec_query, log_file_path);

                    if (exec_query != string.Empty)
                    {
                        SqlCommand cmd = new SqlCommand(exec_query, conn);
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                LogHandler.LogText("Error upload_delete : " + ex.Message, log_file_path);
            }
        }
        #endregion

        #region Print_rows
        public void printRow(IDataReader dr, int col_count)
        {
            String row = "( ";
            int c;
            try
            {
                for (c = 0; c < col_count; c = c + 1)
                {
                    // Get a column value.
                    String cur_col = dr.GetString(c);
                    //LogHandler.LogText("col :" + cur_col);

                    // Check for null values.
                    if (cur_col == null)
                    {
                        cur_col = "<NULL>";
                    }

                    // Add the column value to the row string.
                    row += cur_col;
                    if (c < col_count)
                    {
                        row += ", ";
                    }
                }

                row += " )";

                // Print out the row.
                LogHandler.LogText(row, log_file_path);
            }
            catch (Exception ex)
            {
                LogHandler.LogText("Error printrow : " + ex.Message, log_file_path);
            }
        }
        #endregion
                        
    }
}