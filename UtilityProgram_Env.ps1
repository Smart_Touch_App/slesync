
#Configurations

# Use this utility program to generate "SystemDB's Install Scripts"
################################################################################################
$curPath="\\ibm-1\Virtual_Directories\SalesLogicExpress\Docs\SLESync\PYU\slesync\"
#set location of the folder to execute
Set-Location $curPath
# initialize the items variable with the contents of a directory - check the folder with the name "control" (control file) only filter file with .xml extension
################################################################################
$items = Get-Childitem -filter *ControlFile*SYS.1.0.0* -Recurse -include "*.xml"
################################################################################

#Set Schema Name
################
$schema="BUSDTA"
################

#should not be modified as this is dedicated only for SysDB
$Environment="SysDB"; 
$OrgPath="Source\SystemDB";

#if($Environment -eq "SysDB"){
#    $OrgPath="Source\SystemDB";
#}

################################################################################################

# enumerate the items array
foreach ($item in $items)
{
    #get xml content
    [xml]$xml=get-content $item
    
    #Environment
    $Env=$xml.DBSchema.ReleaseEnvironment;
    
    $SaveFolder="{0}\Releases\{1}\{2}" -f $curPath, $Env, $xml.DBSchema.ModelRelease;
    #Remove ROOT directory if exists  -- to recreate
    if(Test-Path -Path $SaveFolder){
        Remove-Item -Path $SaveFolder -Force -Recurse #Delete contents of the directory without confirmation
    }
    
    #create ROOT Directory if doesn't exists  -- recreate
    #Write-Host $SaveFolder;
    if(!(Test-Path -Path $SaveFolder)){ 
        New-Item -Path $SaveFolder -ItemType directory
    }

    #Set Target Folder
    $SaveFolder="{0}\Releases\{1}\{2}\Install Scripts" -f $curPath, $Env, $xml.DBSchema.ModelRelease;
    #Write-Host $SaveFolder;
    
    #Save Script File
    $Script_log="{0}\Change_Track.log" -f $SaveFolder;
        
    #create ROOT Directory if doesn't exists
    #Write-Host $SaveFolder;
    if(!(Test-Path -Path $SaveFolder)) { 
        New-Item -Path $SaveFolder -ItemType directory
    }
    
    #Create Generation_Source directory
    $Generation_Source="{0}\Releases\{1}\{2}\Generation Source" -f $curPath, $Env, $xml.DBSchema.ModelRelease;
    #Write-Host $ConsSetup;
    if(!(Test-Path -Path $Generation_Source)) { 
        New-Item -Path $Generation_Source -ItemType directory
    }
    
    #Create Cons_Setup directory
    #create new folder for ConSetup Scripts if doesn't exists
    if($Environment -eq "SysDB"){
        $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "SysDB_Setup";
    }
    else{
        $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "ConsDB_Setup";
    }
    #Write-Host $ConsSetup;
    if(!(Test-Path -Path $ConsSetup)){ 
        New-Item -Path $ConsSetup -ItemType directory
    }

    ##Set filename to save consolidate event script -- Adding Evnet
    #if($Environment -eq "SysDB"){
    #    $CDBEvent_SaveFile="{0}\8.SystemDB_Sync_Scripts.sql" -f $ConsSetup;
    #}
    #else{
    #    $CDBEvent_SaveFile="{0}\8.Consolidated_Sync_Scripts.sql" -f $ConsSetup;
    #}
    
    #Add-Content $CDBEvent_SaveFile $cdbConn
    
    Write-Host "Creating Schema to create table table "
    if($Environment -eq "SysDB"){
        $CDBSchema_SaveFile="{0}\2.SystemDB_Schema.sql" -f $ConsSetup;
    }
    else{
        $CDBSchema_SaveFile="{0}\2.Consolidate_Schema.sql" -f $ConsSetup;
    }
    
    $cdbCreateSchema = " IF (SCHEMA_ID('{0}') IS NULL) 
BEGIN
    EXEC ('CREATE SCHEMA [{0}] AUTHORIZATION [dbo]')
END" -f $schema;
    Add-Content $CDBSchema_SaveFile $cdbCreateSchema
    
    #Iterating Table for Consolidated Script - Start
    foreach ($t in $xml.DBSchema.SystemDBSetup.Tables.Table) {
        #Consolidate Script Starts
        ###############################################################################
        #
        $table_name=$t.Name.split(".")[1]; #Write-Host $table_name
        $table_schema=$t.Name.split(".")[0]; #Write-Host $table_schema
        
        if(!$table_name) {
            $table_name=$table_schema;
        }

        #Get consolidate schema file name - Current Version
        if($Environment -eq "SysDB"){
            $CDBSchemafile="{2}\Tables\{0}\{0}_SchemaScripts\{0}_SysSchema_{1}.sql" -f $table_name, $t.Scripts.Schema.SystemDB.Version, $OrgPath;
        }
        #Write-Host "FILE: "$CDBSchemafile;
                
        if(Test-Path $CDBSchemafile) {
            #Create Generation_Source_Table directory
            $Generation_Source_Table="{0}\Tables" -f $Generation_Source;
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_Table)){ 
                New-Item -Path $Generation_Source_Table -ItemType directory
            }
            $Generation_Source_Table="{0}\{1}" -f $Generation_Source_Table, $table_name;
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_Table)){ 
                New-Item -Path $Generation_Source_Table -ItemType directory
            }
            $Generation_Source_Table="{0}\{1}" -f $Generation_Source_Table, "SchemaScripts";
            #Write-Host $ConsSetup;
            if(!(Test-Path -Path $Generation_Source_Table)){ 
                New-Item -Path $Generation_Source_Table -ItemType directory
            }
            if($Environment -eq "SysDB"){
                $cpyTagetFile="{0}\SysDB.{1}_{2}.sql"-f $Generation_Source_Table, $table_name, $t.Scripts.Schema.SystemDB.Version;
            }
            else{
                $cpyTagetFile="{0}\CDB.{1}_{2}.sql"-f $Generation_Source_Table, $table_name, $t.Scripts.Schema.SystemDB.Version;
            }
            
            #Write-Host "Target :" $cpyTagetFile;
            Copy-Item -Path $CDBSchemafile -Destination $cpyTagetFile
            
            if($Environment -eq "SysDB"){
                Write-Host "Writing Sys schema script for table : " $table_name
            }
            else{
                Write-Host "Writing CDB schema script for table : " $table_name
            }
            #Set filename to save consolidate table(schema) script
            if($Environment -eq "SysDB"){
                $CDBSchema_SaveFile="{0}\2.SystemDB_Schema.sql" -f $ConsSetup;
            }
            else{
                $CDBSchema_SaveFile="{0}\2.Consolidate_Schema.sql" -f $ConsSetup;
            }

            $cdbSchema = Get-Content -Path $CDBSchemafile
            Add-Content $CDBSchema_SaveFile $cdbSchema
        }
        else {
            Write-Host $CDBSchemafile;
            if($Environment -eq "SysDB") {
                $RespMsg="WARNING:   SysDB Table '{0}'- schema script does not exist " -f $table_name
            }
            else{
                $RespMsg="WARNING:   CDB Table '{0}'- schema script does not exist " -f $table_name
            }
            
            Write-Host $RespMsg
        }
        #Set filename to save consolidate event script
        if($Environment -eq "SysDB") {
            $CDBEvent_SaveFile="{0}\8.SystemDB_Sync_Scripts.sql" -f $ConsSetup;
        }
        else{
            $CDBEvent_SaveFile="{0}\8.Consolidated_Sync_Scripts.sql" -f $ConsSetup;
        }
        
        #Write-Host "Events for" $table_name
$tblVerEvent="
/* Table :{0} */
/* Version : {1} */
"-f $table_name, $xml.DBSchema.ScriptVersion;

        foreach ($a in $t.Scripts.Events.Event) {
            
            #Change Event Log - CDB Events
            if(($a.LastVersion -ne $null) -And ($a.LastVersion -ne $a.Version))
            {
                if($Environment -eq "SysDB"){
                    $logTxt = "SysDB Table - {2}: Event - {0}: {1}" -f $a.Name, "Event Script Modified", $table_name
                }
                else{
                    $logTxt = "CDB Table - {2}: Event - {0}: {1}" -f $a.Name, "Event Script Modified", $table_name
                }
                
                Add-Content $Script_log $logTxt
            }
        
        #if SysDB execute SysDB SP else execute ConsDB SP
        if($Environment -eq "SysDB"){
            $buildEvent="
/* {0} - Start */
EXEC drh_add_table_script
    '{0}',
	'{1}',
	'{2}',
" -f $xml.DBSchema.ScriptVersion, "$table_schema.$table_name", $a.Name;
        }
            #Get consolidate event file name
            $CDBEventfile="{3}\Tables\{0}\{0}_Events\{0}_{1}_{2}.sql" -f $table_name, $a.Name, $a.Version, $OrgPath;
            Write-Host "Checking..."
            Write-Host $CDBEventfile

            if(!(Test-Path $CDBEventfile)){ 
                $cdbEvents="--{drh_ignore}"
            }
            else{
                #Get Content of the file
                #$evt = Get-Content -Path $CDBEventfile
                $cdbEvents = Get-Content -Path $CDBEventfile
                #$cdbEvents=
#"'"+
#$evt+
#"'"
                
                #Copy Source File into Generation source file
                #Create Generation_Source_Table directory
                $Generation_Source_Schema="{0}\Tables" -f $Generation_Source;
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_Table)){ 
                    New-Item -Path $Generation_Source_Table -ItemType directory
                }
                $Generation_Source_Schema="{0}\{1}" -f $Generation_Source_Schema, $table_name;
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_Schema)){ 
                    New-Item -Path $Generation_Source_Schema -ItemType directory
                }
                $Generation_Source_Schema="{0}\{1}" -f $Generation_Source_Schema, "Events";
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_Schema)){ 
                    New-Item -Path $Generation_Source_Schema -ItemType directory
                }
                if($Environment -eq "SysDB"){
                    $cpyTagetFile="{0}\SysDB.{1}_{2}_{3}.sql"-f $Generation_Source_Schema, $table_name, $a.Name, $a.Version;
                }
                else{
                    $cpyTagetFile="{0}\CDB.{1}_{2}_{3}.sql"-f $Generation_Source_Schema, $table_name, $a.Name, $a.Version;
                }
                
                Copy-Item -Path $CDBEventfile -Destination $cpyTagetFile
            }
            #$cdbEvents=
            #$tblVerEvent+
            #$buildEvent+
            #$cdbEvents+"/* {0} - End */" -f $a.Name
            
            $endEvents="
/* {0} - End */" -f $a.Name
            Add-Content $CDBEvent_SaveFile $tblVerEvent;
            Add-Content $CDBEvent_SaveFile $buildEvent;
            Add-Content $CDBEvent_SaveFile "'";
            Add-Content $CDBEvent_SaveFile $cdbEvents;
            Add-Content $CDBEvent_SaveFile "'";
            Add-Content $CDBEvent_SaveFile $endEvents;

        $tblVerEvent=""; #Empty
        if($Environment -eq "SysDB"){
            $msg_Evt = "Writing SysDB events - '{1}' script for table : {0}" -f $table_name, $a.Name
        }
        else{
            $msg_Evt = "Writing CDB events - '{1}' script for table : {0}" -f $table_name, $a.Name
        }
        
        Write-Host $msg_Evt
        }
        #
        ###############################################################################
        #Consolidate Script Ends
    }
    
    #############################
    $GroupNo=2;
    #Iterating Other Objects for Consolidated Script - Start
    foreach ($objGroup in ($xml.SelectNodes("DBSchema/SystemDBSetup/*") | Select-Object -Expand Name  | Where-Object { ($_ -ne "Connection") -and ($_ -ne "Tables") })) {
        #
        #Get consolidate script file name
        #
        #Write-Host "GroupName "$objGroup
        $GroupNo=$GroupNo+1
        $objType=$objGroup.Substring(0,$objGroup.length-1);
        $node="DBSchema/SystemDBSetup/{0}/{1}" -f $objGroup, $objType;

        $objArray=$xml.SelectNodes($node)| Select-Object -Property Name, Version, LastVersion | ForEach-Object {
            $Name = $_.Name; $Version=$_.Version; $LastVersion=$_.LastVersion;
            if($objGroup -eq "Procedures"){$objGroup="StoredProcedures";}
         
            if($Environment -eq "SysDB"){
                $CDBGroupfile="{3}\{2}\SysDB.{0}_{1}.sql" -f $Name, $Version, $objGroup, $OrgPath;
            }
            else{
                $CDBGroupfile="{3}\{2}\CDB.{0}_{1}.sql" -f $Name, $Version, $objGroup, $OrgPath;
            }
            
            #Write-Host $CDBGroupfile;
            if(Test-Path $CDBGroupfile) {
            
                #Create Generation_Source_obj directory
                $Generation_Source_obj="{0}\{1}" -f $Generation_Source, $objGroup;
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $Generation_Source_obj)){ 
                    New-Item -Path $Generation_Source_obj -ItemType directory
                }
                if($Environment -eq "SysDB"){
                    $cpyTagetFile="{0}\SysDB.{1}_{2}.sql"-f $Generation_Source_obj, $Name, $Version;
                }
                else{
                    $cpyTagetFile="{0}\CDB.{1}_{2}.sql"-f $Generation_Source_obj, $Name, $Version;
                }
                
                Copy-Item -Path $CDBGroupfile -Destination $cpyTagetFile
                
                #Get Content of the file
                $cdbScript = Get-Content -Path $CDBGroupfile
                
                #Create Cons_Setup directory
                #create new folder for ConSetup Scripts if doesn't exists
                if($Environment -eq "SysDB"){
                    $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "SysDB_Setup";
                }
                else{
                    $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "ConsDB_Setup";
                }
                
                #Write-Host $ConsSetup;
                if(!(Test-Path -Path $ConsSetup)){ 
                    New-Item -Path $ConsSetup -ItemType directory
                }
                
                #Set filename to save consolidate event script
                if($Environment -eq "SysDB"){
                    $cdbScript_SaveFile="{0}\{2}.SystemDB_{1}.sql" -f $ConsSetup, $objGroup, $GroupNo;
                }
                else{
                    $cdbScript_SaveFile="{0}\{2}.Consolidated_{1}.sql" -f $ConsSetup, $objGroup, $GroupNo;
                }
                
                Add-Content $cdbScript_SaveFile $cdbScript;
                if($Environment -eq "SysDB"){
                    $RespMsg="Writing SysDB {0} : {1} " -f $objGroup, $Name;
                }
                else{
                    $RespMsg="Writing CDB {0} : {1} " -f $objGroup, $Name;
                }
                
                Write-Host $RespMsg;
            }
            else {
                $RespMsg="WARNING:   {0} '{1}'- script does not exist" -f $objGroup, $Name;
                Write-Host $RespMsg;
            }
        }
        #
    }
    #Iterating Other Objects for Consolidated Script - End
    #############################
    

    #Copy connection script to target folder for consolidated
    $CDBconnVer=$xml.DBSchema.SystemDBSetup.Connection.Version;
    $mlfile="{0}\{2}\{1}" -f $curPath, "Connections", $OrgPath;
    #Write-Host $mlfile;
    if(Test-Path -Path $mlfile) {
        $mlFileName="{0}\System_Setup_Exec_All_{1}.bat" -f $mlfile, $CDBconnVer;
        if(Test-Path $mlFileName){
            $tarPath="{0}\1.SystemDB_Setup.bat" -f $ConsSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "SystemDB connection script copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     SystemDB connection script not exist" 
        }
        
        $mlFileName="{0}\System_Connection_Vars.bat" -f $mlfile;
        if(Test-Path $mlFileName){
            $tarPath="{0}\System_Connection_Vars.bat" -f $ConsSetup;
            Copy-Item -Path $mlFileName -Destination $tarPath
            Write-Host "SystemDB connection variables copied into con setup directory";
        }
        else {
            Write-Host "WARNING:     SystemDB connection variables not exist" 
        }
    }
    else {
        Write-Host "WARNING:     SystemDB connection script folder not exist"
    }

    #Create Cons_Setup directory
    #create new folder for ConSetup Scripts if doesn't exists
    if($Environment -eq "SysDB"){
        $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "SysDB_Setup";
    }
    else{
        $ConsSetup="{0}\{1}-{2}" -f $SaveFolder, $Env, "ConsDB_Setup";
    }
    
    #Write-Host $ConsSetup;
    if(!(Test-Path -Path $ConsSetup)){ 
        New-Item -Path $ConsSetup -ItemType directory
    }
    
}

Write-Host "Script Generated Successfully"