
SELECT "dbo"."tblAlliedProspCategory"."CategoryID"
,"dbo"."tblAlliedProspCategory"."Category"
,"dbo"."tblAlliedProspCategory"."CreatedBy"
,"dbo"."tblAlliedProspCategory"."CreatedDatetime"
,"dbo"."tblAlliedProspCategory"."UpdatedBy"
,"dbo"."tblAlliedProspCategory"."UpdatedDatetime"

FROM "dbo"."tblAlliedProspCategory"
WHERE "dbo"."tblAlliedProspCategory"."last_modified">= {ml s.last_table_download}
