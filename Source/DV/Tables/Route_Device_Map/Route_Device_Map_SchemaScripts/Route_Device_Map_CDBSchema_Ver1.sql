
/* [BUSDTA].[Route_Device_Map] - begins */

/* TableDDL - [BUSDTA].[Route_Device_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Device_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Device_Map]	(	  [Route_Id] VARCHAR(8) NOT NULL	, [Device_Id] VARCHAR(30) NOT NULL	, [Active] INT NULL	, [Remote_Id] VARCHAR(30) NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_DEVICE_MAP_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_Route_Device_Map] PRIMARY KEY ([Route_Id] ASC, [Device_Id] ASC)	)
END
/* TableDDL - [BUSDTA].[Route_Device_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Device_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Device_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Device_Map_del]	(	  [Route_Id] VARCHAR(8)	, [Device_Id] VARCHAR(30)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([Route_Id] ASC, [Device_Id] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[Route_Device_Map] - End */
/* TRIGGERS FOR Route_Device_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Device_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Device_Map_ins
	ON BUSDTA.Route_Device_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Device_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Device_Map_del.Device_Id= inserted.Device_Id AND BUSDTA.Route_Device_Map_del.Route_Id= inserted.Route_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Device_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Device_Map_upd
	ON BUSDTA.Route_Device_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Device_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Device_Map.Device_Id= inserted.Device_Id AND BUSDTA.Route_Device_Map.Route_Id= inserted.Route_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Device_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Device_Map_dlt
	ON BUSDTA.Route_Device_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Device_Map_del (Device_Id, Route_Id, last_modified )
	SELECT deleted.Device_Id, deleted.Route_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Device_Map - END */
