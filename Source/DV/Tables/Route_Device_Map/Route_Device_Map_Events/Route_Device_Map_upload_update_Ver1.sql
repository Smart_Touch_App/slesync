/* Update the row in the consolidated database. */

UPDATE "BUSDTA"."Route_Device_Map"
SET "Active" = {ml r."Active"},
	"Remote_Id" = {ml r."Remote_Id"}
WHERE "Route_Id" = {ml r."Route_Id"}
	AND "Device_Id" = {ml r."Device_Id"}