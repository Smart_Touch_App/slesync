------- /*[BUSDTA].[F4075]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4075"     (
	"VBVBT" nvarchar(10) not null,
	"VBCRCD" nvarchar(3) null,
	"VBUOM" nvarchar(2) null,
	"VBUPRC" float null,
	"VBEFTJ" numeric(18, 0) not null,
	"VBEXDJ" numeric(18, 0) null,
	"VBAPRS" nvarchar(1) null,
	"VBUPMJ" numeric(18, 0) not null,
	"VBTDAY" numeric(6, 0) not null,
	PRIMARY KEY ("VBVBT", "VBEFTJ", "VBUPMJ", "VBTDAY")
)

------- /*[BUSDTA].[F4075]*/ - End