
/* [BUSDTA].[Inventory_Ledger] - begins */

/* TableDDL - [BUSDTA].[Inventory_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Ledger]
	(
	  [InventoryLedgerID] DECIMAL(15,0) NOT NULL DEFAULT autoincrement
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NVARCHAR (25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionQty] DECIMAL(15,0) NULL
	, [TransactionQtyUM] NVARCHAR (2) NULL
	, [TransactionQtyPrimaryUM] NVARCHAR (2) NULL
	, [TransactionType] NUMERIC(8,0) NULL
	, [TransactionId] DECIMAL(15,0) NULL
	, [SettlementID] DECIMAL(15,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([InventoryLedgerID] ASC, [ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Inventory_Ledger] - End */

