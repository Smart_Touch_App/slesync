 
SELECT "BUSDTA"."MoneyOrderDetails"."MoneyOrderId"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderNumber"
,"BUSDTA"."MoneyOrderDetails"."RouteId"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderAmount"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderFeeAmount"
,"BUSDTA"."MoneyOrderDetails"."StatusId"
,"BUSDTA"."MoneyOrderDetails"."MoneyOrderDatetime"
,"BUSDTA"."MoneyOrderDetails"."VoidReasonId"
,"BUSDTA"."MoneyOrderDetails"."CreatedBy"
,"BUSDTA"."MoneyOrderDetails"."CreatedDatetime"
,"BUSDTA"."MoneyOrderDetails"."UpdatedBy"
,"BUSDTA"."MoneyOrderDetails"."UpdatedDatetime"
FROM "BUSDTA"."MoneyOrderDetails"
WHERE "BUSDTA"."MoneyOrderDetails"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."MoneyOrderDetails"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

