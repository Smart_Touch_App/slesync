 
SELECT "dbo"."DDLEvents"."EvntID"
,"dbo"."DDLEvents"."EventDate"
,"dbo"."DDLEvents"."EventType"
,"dbo"."DDLEvents"."ObjectType"
,"dbo"."DDLEvents"."EventDDL"
,"dbo"."DDLEvents"."EventXML"
,"dbo"."DDLEvents"."DatabaseName"
,"dbo"."DDLEvents"."SchemaName"
,"dbo"."DDLEvents"."ObjectName"
,"dbo"."DDLEvents"."HostName"
,"dbo"."DDLEvents"."IPAddress"
,"dbo"."DDLEvents"."ProgramName"
,"dbo"."DDLEvents"."LoginName"
,"dbo"."DDLEvents"."Status"

FROM ."dbo"."DDLEvents"
WHERE "dbo"."DDLEvents"."last_modified">= {ml s.last_table_download}
 
