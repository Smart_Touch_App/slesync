/* [dbo].[DDLEvents] - begins */

/* TableDDL - [dbo].[DDLEvents] - Start */
IF OBJECT_ID('[dbo].[DDLEvents]') IS NULL
BEGIN

	CREATE TABLE [dbo].[DDLEvents]
	(
	  [EvntID] INT NOT NULL IDENTITY(1,1)
	, [EventDate] DATETIME NULL CONSTRAINT DF_DDLEVENTS_EventDate DEFAULT(getdate())
	, [EventType] NVARCHAR(64) NULL
	, [ObjectType] NVARCHAR(64) NULL
	, [EventDDL] NVARCHAR(MAX) NULL
	, [EventXML] XML NULL
	, [DatabaseName] NVARCHAR(255) NULL
	, [SchemaName] NVARCHAR(255) NULL
	, [ObjectName] NVARCHAR(255) NULL
	, [HostName] VARCHAR(64) NULL
	, [IPAddress] VARCHAR(32) NULL
	, [ProgramName] NVARCHAR(255) NULL
	, [LoginName] NVARCHAR(255) NULL
	, [Status] BIT NULL CONSTRAINT DF_DDLEVENTS_Status DEFAULT((1))
	)

END
/* TableDDL - [dbo].[DDLEvents] - End */

/* SHADOW TABLE FOR [dbo].[DDLEvents] - Start */
IF OBJECT_ID('[dbo].[DDLEvents_del]') IS NULL
BEGIN

 
END
/* SHADOW TABLE FOR [dbo].[DDLEvents] - End */
/* TRIGGERS FOR DDLEvents - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.DDLEvents_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.DDLEvents_ins
	ON dbo.DDLEvents AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
 
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.DDLEvents_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.DDLEvents_upd
	ON dbo.DDLEvents AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
 
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.DDLEvents_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.DDLEvents_dlt
	ON dbo.DDLEvents AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
 
/* TRIGGERS FOR DDLEvents - END */
