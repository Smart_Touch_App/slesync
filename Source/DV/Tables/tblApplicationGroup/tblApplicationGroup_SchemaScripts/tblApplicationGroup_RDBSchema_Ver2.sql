/* [dbo].[tblApplicationGroup] - begins */

/* TableDDL - [dbo].[tblApplicationGroup] - Start */
IF OBJECT_ID('[dbo].[tblApplicationGroup]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblApplicationGroup]
	(
	  [ApplicationGroupID] INTEGER NOT NULL default autoincrement
	, [ApplicationGroup] VARCHAR(128) NULL
	, [SortOrder] INTEGER NULL
	, [IsMandatorySync] BIT NULL

	, PRIMARY KEY ([ApplicationGroupID] ASC)
	)
END
/* TableDDL - [dbo].[tblApplicationGroup] - End */
