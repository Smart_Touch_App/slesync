
/* [BUSDTA].[M40116] - begins */

/* TableDDL - [BUSDTA].[M40116] - Start */
IF OBJECT_ID('[BUSDTA].[M40116]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M40116]
	(
	  [PAAN8] NUMERIC(8,0) NOT NULL
	, [PAADD1] NCHAR(40) NULL
	, [PAADD2] NCHAR(40) NULL
	, [PAADD3] NCHAR(40) NULL
	, [PAADD4] NCHAR(40) NULL
	, [PAADDZ] NCHAR(12) NULL
	, [PACTY1] NCHAR(25) NULL
	, [PACOUN] NCHAR(25) NULL
	, [PAADDS] NCHAR(3) NULL
	, [PACRBY] NCHAR(50) NULL
	, [PACRDT] DATE NULL
	, [PAUPBY] NCHAR(50) NULL
	, [PAUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M40116_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M40116] PRIMARY KEY ([PAAN8] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M40116] - End */

/* SHADOW TABLE FOR [BUSDTA].[M40116] - Start */
IF OBJECT_ID('[BUSDTA].[M40116_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M40116_del]
	(
	  [PAAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PAAN8] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M40116] - End */
/* TRIGGERS FOR M40116 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M40116_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M40116_ins
	ON BUSDTA.M40116 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M40116_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M40116_del.PAAN8= inserted.PAAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M40116_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M40116_upd
	ON BUSDTA.M40116 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M40116
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M40116.PAAN8= inserted.PAAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M40116_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M40116_dlt
	ON BUSDTA.M40116 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M40116_del (PAAN8, last_modified )
	SELECT deleted.PAAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M40116 - END */

