SELECT "BUSDTA"."M56M0004"."RPSTID"
,"BUSDTA"."M56M0004"."RPROUT"
,"BUSDTA"."M56M0004"."RPAN8"
,"BUSDTA"."M56M0004"."RPSTDT"
,"BUSDTA"."M56M0004"."RPOGDT"
,"BUSDTA"."M56M0004"."RPSN"
,"BUSDTA"."M56M0004"."RPVTTP"
,"BUSDTA"."M56M0004"."RPSTTP"
,"BUSDTA"."M56M0004"."RPRSTID"
,"BUSDTA"."M56M0004"."RPISRSN"
,"BUSDTA"."M56M0004"."RPACTID"
,"BUSDTA"."M56M0004"."RPRCID"
,"BUSDTA"."M56M0004"."RPCACT"
,"BUSDTA"."M56M0004"."RPPACT"
,"BUSDTA"."M56M0004"."RPCHOVRD"
,"BUSDTA"."M56M0004"."RPCRBY"
,"BUSDTA"."M56M0004"."RPCRDT"
,"BUSDTA"."M56M0004"."RPUPBY"
,"BUSDTA"."M56M0004"."RPUPDT"
FROM BUSDTA.M56M0004, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE ("BUSDTA"."M56M0004"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}
 or crm.last_modified >= {ml s.last_table_download})
AND  RPAN8 = crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
