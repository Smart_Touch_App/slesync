
------- /*[BUSDTA].[M56M0004]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M56M0004"     (
	"RPSTID" numeric(8, 0) not null DEFAULT autoincrement,
	"RPROUT" nvarchar(10) not null,
	"RPAN8" numeric(8, 0) not null,
	"RPSTDT" date not null,
	"RPOGDT" date null,
	"RPSN" numeric(4, 0) null,
	"RPVTTP" nvarchar(10) null,
	"RPSTTP" nvarchar(15) null,
	"RPRSTID" numeric(8, 0) null,
	"RPISRSN" bit null default ((0)),
	"RPACTID" nvarchar(6) null,
	"RPRCID" numeric(3, 0) null,
	"RPCACT" INTEGER NULL DEFAULT((0)),
	"RPPACT" INTEGER NULL DEFAULT((0)),
	"RPCRBY" nvarchar(50) null,
	"RPCRDT" datetime null,
	"RPUPBY" nvarchar(50) null,
	"RPUPDT" datetime null,
	PRIMARY KEY ("RPSTID", "RPAN8")
)

------- /*[BUSDTA].[M56M0004]*/ - End