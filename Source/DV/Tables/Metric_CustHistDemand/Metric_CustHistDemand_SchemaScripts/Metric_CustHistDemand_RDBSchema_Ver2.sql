/* [BUSDTA].[Metric_CustHistDemand] - begins */

/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - Start */
IF OBJECT_ID('[BUSDTA].[Metric_CustHistDemand]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Metric_CustHistDemand]
	(
	  [CustomerId] NUMERIC(8,0) NOT NULL
	, [ItemID] NUMERIC(8,0) NOT NULL
	, [DemandFromDate] DATE NULL
	, [DemandThroughDate] DATE NULL
	, [PlannedStops] NUMERIC(4,0) NULL
	, [ActualStops] NUMERIC(4,0) NULL
	, [TotalQtySold] DECIMAL(15,0) NULL
	, [TotalQtyReturned] DECIMAL(15,0) NULL
	, [QtySoldPerStop] DECIMAL(15,0) NULL
	, [QtyReturnedPerStop] DECIMAL(15,0) NULL
	, [NetQtySoldPerStop] DECIMAL(15,0) NULL
	, [QtySoldDeltaPerStop] DECIMAL(15,4) NULL
	, [QtyReturnedDeltaPerStop] DECIMAL(15,4) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([CustomerId] ASC, [ItemID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - End */
