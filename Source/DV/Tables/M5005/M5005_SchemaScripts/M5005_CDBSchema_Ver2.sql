
/* [BUSDTA].[M5005] - begins */

/* TableDDL - [BUSDTA].[M5005] - Start */
IF OBJECT_ID('[BUSDTA].[M5005]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5005]
	(
	  [SMKEY] VARCHAR(30) NOT NULL
	, [SMDIR] NCHAR(10) NULL
	, [SMPRTY] NCHAR(10) NULL
	, [SMCRBY] NCHAR(50) NULL
	, [SMCRDT] DATETIME NULL
	, [SMUPBY] NCHAR(50) NULL
	, [SMUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M5005_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M5005] PRIMARY KEY ([SMKEY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M5005] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5005] - Start */
IF OBJECT_ID('[BUSDTA].[M5005_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5005_del]
	(
	  [SMKEY] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([SMKEY] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M5005] - End */
/* TRIGGERS FOR M5005 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5005_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5005_ins
	ON BUSDTA.M5005 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5005_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5005_del.SMKEY= inserted.SMKEY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5005_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5005_upd
	ON BUSDTA.M5005 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5005
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5005.SMKEY= inserted.SMKEY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5005_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5005_dlt
	ON BUSDTA.M5005 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5005_del (SMKEY, last_modified )
	SELECT deleted.SMKEY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5005 - END */

