/* [BUSDTA].[Order_Detail] - begins */

/* TableDDL - [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail]
	(
	  [Order_Detail_Id] INT NOT NULL
	, [Order_ID] INT NOT NULL
	, [Item_Number] NCHAR(25) NULL
	, [Order_Qty] INT NULL
	, [Order_UOM] NCHAR(2) NULL
	, [Unit_Price] FLOAT NULL
	, [Extn_Price] FLOAT NULL
	, [Reason_Code] VARCHAR(5) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ORDER_DETAIL_last_modified DEFAULT(getdate())
	, [IsTaxable] BIT NULL
	, CONSTRAINT [PK_Order_Detail] PRIMARY KEY ([Order_Detail_Id] ASC, [Order_ID] ASC)
	)

END
/* TableDDL - [BUSDTA].[Order_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail_del]
	(
	  [Order_Detail_Id] INT
	, [Order_ID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Order_Detail_Id] ASC, [Order_ID] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Order_Detail] - End */
/* TRIGGERS FOR Order_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Detail_ins
	ON BUSDTA.Order_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Detail_del.Order_Detail_Id= inserted.Order_Detail_Id AND BUSDTA.Order_Detail_del.Order_ID= inserted.Order_ID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Detail_upd
	ON BUSDTA.Order_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Detail.Order_Detail_Id= inserted.Order_Detail_Id AND BUSDTA.Order_Detail.Order_ID= inserted.Order_ID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Detail_dlt
	ON BUSDTA.Order_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Detail_del (Order_Detail_Id, Order_ID, last_modified )
	SELECT deleted.Order_Detail_Id, deleted.Order_ID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Detail - END */
