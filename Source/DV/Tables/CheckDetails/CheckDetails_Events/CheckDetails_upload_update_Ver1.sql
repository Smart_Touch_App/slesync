 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."CheckDetails"
SET "CheckDetailsNumber" = {ml r."CheckDetailsNumber"}, "CheckNumber" = {ml r."CheckNumber"}, "CustomerId" = {ml r."CustomerId"}, "CustomerName" = {ml r."CustomerName"}, "CheckAmount" = {ml r."CheckAmount"}, "CheckDate" = {ml r."CheckDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CheckDetailsId" = {ml r."CheckDetailsId"} AND "RouteId" = {ml r."RouteId"}
 
