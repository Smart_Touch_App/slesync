------- /*[BUSDTA].[User_Role_Map]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."User_Role_Map"     (
	"App_user_id" integer not null,
	"Role" varchar(8) null,
	PRIMARY KEY ("App_user_id")
)

------- /*[BUSDTA].[User_Role_Map]*/ - End