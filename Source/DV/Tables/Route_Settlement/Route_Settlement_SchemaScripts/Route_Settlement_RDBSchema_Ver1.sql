/* [BUSDTA].[Route_Settlement] - begins */

/* TableDDL - [BUSDTA].[Route_Settlement] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Settlement]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Settlement]
	(
	  [SettlementID] NUMERIC NOT NULL
	, [SettlementNO] NCHAR(10) NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [Status] NUMERIC NULL
	, [SettlementDateTime] DATETIME NULL
	, [UserId] NUMERIC NULL
	, [Originator] NUMERIC NULL
	, [Verifier] NUMERIC NULL
	, [SettlementAmount] NUMERIC NULL
	, [ExceptionAmount] NUMERIC NULL
	, [Comment] NVARCHAR(100) NULL
	, [OriginatingRoute] NUMERIC NOT NULL
	, [partitioningRoute] NUMERIC NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([SettlementID] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Settlement] - End */
