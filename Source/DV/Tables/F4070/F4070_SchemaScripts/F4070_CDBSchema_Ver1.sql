
/* [BUSDTA].[F4070] - begins */

/* TableDDL - [BUSDTA].[F4070] - Start */
IF OBJECT_ID('[BUSDTA].[F4070]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4070]	(	  [SNASN] NCHAR(8) NOT NULL	, [SNOSEQ] NUMERIC NOT NULL	, [SNANPS] NUMERIC NOT NULL	, [SNAST] NCHAR(8) NULL	, [SNEFTJ] NUMERIC NULL	, [SNEXDJ] NUMERIC NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4070_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F4070] PRIMARY KEY ([SNASN] ASC, [SNOSEQ] ASC, [SNANPS] ASC)	)
END
/* TableDDL - [BUSDTA].[F4070] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4070] - Start */
IF OBJECT_ID('[BUSDTA].[F4070_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4070_del]	(	  [SNASN] NCHAR(8)	, [SNOSEQ] NUMERIC	, [SNANPS] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([SNASN] ASC, [SNOSEQ] ASC, [SNANPS] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[F4070] - End */
/* TRIGGERS FOR F4070 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4070_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4070_ins
	ON BUSDTA.F4070 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4070_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4070_del.SNANPS= inserted.SNANPS AND BUSDTA.F4070_del.SNASN= inserted.SNASN AND BUSDTA.F4070_del.SNOSEQ= inserted.SNOSEQ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4070_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4070_upd
	ON BUSDTA.F4070 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4070
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4070.SNANPS= inserted.SNANPS AND BUSDTA.F4070.SNASN= inserted.SNASN AND BUSDTA.F4070.SNOSEQ= inserted.SNOSEQ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4070_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4070_dlt
	ON BUSDTA.F4070 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4070_del (SNANPS, SNASN, SNOSEQ, last_modified )
	SELECT deleted.SNANPS, deleted.SNASN, deleted.SNOSEQ, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4070 - END */
