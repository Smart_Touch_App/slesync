
------- /*[BUSDTA].[M56M0003]*/ - End

CREATE TABLE IF NOT EXISTS "BUSDTA"."M56M0003"     (
	"RSROUT" varchar(10) not null,
	"RSAN8" numeric(8, 0) not null,
	"RSWN" numeric(4, 0) not null,
	"RSDN" numeric(4, 0) not null,
	"RSSN" numeric(4, 0) null,
	"RSCRBY" nvarchar(50) null,
	"RSCRDT" datetime null,
	"RSUPBY" nvarchar(50) null,
	"RSUPDT" datetime null,
	PRIMARY KEY ("RSROUT", "RSAN8", "RSWN", "RSDN")
)

------- /*[BUSDTA].[M56M0003]*/ - End