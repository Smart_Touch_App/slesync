 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0003"
SET "RSSN" = {ml r."RSSN"},  "RSCRBY" = {ml r."RSCRBY"}, "RSCRDT" = {ml r."RSCRDT"}, "RSUPBY" = {ml r."RSUPBY"}, "RSUPDT" = {ml r."RSUPDT"}
WHERE "RSAN8" = {ml r."RSAN8"} AND "RSDN" = {ml r."RSDN"} AND "RSROUT" = {ml r."RSROUT"} AND "RSWN" = {ml r."RSWN"}
 
