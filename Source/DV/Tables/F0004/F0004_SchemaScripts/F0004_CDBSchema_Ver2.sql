
/* [BUSDTA].[F0004] - begins */

/* TableDDL - [BUSDTA].[F0004] - Start */
IF OBJECT_ID('[BUSDTA].[F0004]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0004]
	(
	  [DTSY] NCHAR(4) NOT NULL
	, [DTRT] NCHAR(2) NOT NULL
	, [DTDL01] NCHAR(30) NULL
	, [DTCDL] FLOAT NULL
	, [DTLN2] NCHAR(1) NULL
	, [DTCNUM] NCHAR(1) NULL
	, [DTYN] BIT NULL CONSTRAINT DF_F0004_DTYN DEFAULT((0))
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0004_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0004] PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0004] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0004] - Start */
IF OBJECT_ID('[BUSDTA].[F0004_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0004_del]
	(
	  [DTSY] NCHAR(4)
	, [DTRT] NCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DTSY] ASC, [DTRT] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0004] - End */
/* TRIGGERS FOR F0004 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0004_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0004_ins
	ON BUSDTA.F0004 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0004_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0004_del.DTRT= inserted.DTRT AND BUSDTA.F0004_del.DTSY= inserted.DTSY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0004_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0004_upd
	ON BUSDTA.F0004 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0004
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0004.DTRT= inserted.DTRT AND BUSDTA.F0004.DTSY= inserted.DTSY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0004_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0004_dlt
	ON BUSDTA.F0004 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0004_del (DTRT, DTSY, last_modified )
	SELECT deleted.DTRT, deleted.DTSY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0004 - END */

