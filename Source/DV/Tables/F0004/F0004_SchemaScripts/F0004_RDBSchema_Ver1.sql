------- /*[BUSDTA].[F0004]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F0004"     (
	"DTSY" nvarchar(4) not null,
	"DTRT" nvarchar(2) not null,
	"DTDL01" nvarchar(30) null,
	"DTCDL" float null,
	"DTYN" bit null default ((0)),
	PRIMARY KEY ("DTSY", "DTRT")
)

------- /*[BUSDTA].[F0004]*/ - End