
/* [BUSDTA].[M50052] - begins */

/* TableDDL - [BUSDTA].[M50052] - Start */
IF OBJECT_ID('[BUSDTA].[M50052]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50052]	(	  [SDID] NUMERIC NOT NULL	, [SDTXID] NUMERIC NOT NULL	, [SDKEY] NCHAR(15) NULL	, [SDISYNCD] BIT NOT NULL	, [SDTMSTMP] DATETIME NULL	, [SDCRBY] NCHAR(50) NULL	, [SDCRDT] DATETIME NULL	, [SDUPBY] NCHAR(50) NULL	, [SDUPDT] DATETIME NULL	, last_modified DATETIME DEFAULT GETDATE()	, CONSTRAINT [PK_M50052] PRIMARY KEY ([SDID] ASC, [SDTXID] ASC)	)
END
/* TableDDL - [BUSDTA].[M50052] - End */

/* SHADOW TABLE FOR [BUSDTA].[M50052] - Start */
IF OBJECT_ID('[BUSDTA].[M50052_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50052_del]	(	  [SDID] NUMERIC	, [SDTXID] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([SDID] ASC, [SDTXID] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[M50052] - End */
/* TRIGGERS FOR M50052 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M50052_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M50052_ins
	ON BUSDTA.M50052 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M50052_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M50052_del.SDID= inserted.SDID AND BUSDTA.M50052_del.SDTXID= inserted.SDTXID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M50052_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50052_upd
	ON BUSDTA.M50052 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M50052
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M50052.SDID= inserted.SDID AND BUSDTA.M50052.SDTXID= inserted.SDTXID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M50052_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M50052_dlt
	ON BUSDTA.M50052 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M50052_del (SDID, SDTXID, last_modified )
	SELECT deleted.SDID, deleted.SDTXID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M50052 - END */
