
/* [BUSDTA].[F4013] - begins */

/* TableDDL - [BUSDTA].[F4013] - Start */
IF OBJECT_ID('[BUSDTA].[F4013]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4013]	(	  [SXXRTC] NCHAR(2) NOT NULL	, [SXXRVF] NCHAR(30) NOT NULL	, [SXXRVT] NCHAR(30) NOT NULL	, [SXEDF1] NCHAR(1) NOT NULL	, [SXDESC] NCHAR(30) NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4013_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F4013] PRIMARY KEY ([SXXRTC] ASC, [SXXRVF] ASC, [SXXRVT] ASC, [SXEDF1] ASC)	)
END
/* TableDDL - [BUSDTA].[F4013] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4013] - Start */
IF OBJECT_ID('[BUSDTA].[F4013_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4013_del]	(	  [SXXRTC] NCHAR(2)	, [SXXRVF] NCHAR(30)	, [SXXRVT] NCHAR(30)	, [SXEDF1] NCHAR(1)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([SXXRTC] ASC, [SXXRVF] ASC, [SXXRVT] ASC, [SXEDF1] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[F4013] - End */
/* TRIGGERS FOR F4013 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4013_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4013_ins
	ON BUSDTA.F4013 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4013_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4013_del.SXEDF1= inserted.SXEDF1 AND BUSDTA.F4013_del.SXXRTC= inserted.SXXRTC AND BUSDTA.F4013_del.SXXRVF= inserted.SXXRVF AND BUSDTA.F4013_del.SXXRVT= inserted.SXXRVT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4013_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4013_upd
	ON BUSDTA.F4013 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4013
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4013.SXEDF1= inserted.SXEDF1 AND BUSDTA.F4013.SXXRTC= inserted.SXXRTC AND BUSDTA.F4013.SXXRVF= inserted.SXXRVF AND BUSDTA.F4013.SXXRVT= inserted.SXXRVT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4013_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4013_dlt
	ON BUSDTA.F4013 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4013_del (SXEDF1, SXXRTC, SXXRVF, SXXRVT, last_modified )
	SELECT deleted.SXEDF1, deleted.SXXRTC, deleted.SXXRVF, deleted.SXXRVT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4013 - END */
