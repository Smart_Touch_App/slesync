/* [BUSDTA].[",] - begins */

/* TableDDL - [BUSDTA].[",] - Start */
IF OBJECT_ID('[BUSDTA].[",]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[",]
	(
	  [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [Entity] NCHAR(50) NOT NULL
	, [BucketSize] DECIMAL(4,0) NULL
	, [NoOfBuckets] DECIMAL(4,0) NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	
	, PRIMARY KEY ([EntityBucketMasterId] ASC)
	)
END
/* TableDDL - [BUSDTA].[",] - End */
