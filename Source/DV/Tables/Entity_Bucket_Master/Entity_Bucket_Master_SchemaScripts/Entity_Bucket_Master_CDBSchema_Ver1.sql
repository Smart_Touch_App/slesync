/* [BUSDTA].[",] - begins */

/* TableDDL - [BUSDTA].[",] - Start */
IF OBJECT_ID('[BUSDTA].[",]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[",]
	(
	  [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [Entity] NCHAR(50) NOT NULL
	, [BucketSize] DECIMAL(4,0) NULL
	, [NoOfBuckets] DECIMAL(4,0) NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_",_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_",] PRIMARY KEY ([EntityBucketMasterId] ASC)
	)

END
/* TableDDL - [BUSDTA].[",] - End */

/* SHADOW TABLE FOR [BUSDTA].[",] - Start */
IF OBJECT_ID('[BUSDTA].[",_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[",_del]
	(
	  [EntityBucketMasterId] DECIMAL(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EntityBucketMasterId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[",] - End */
/* TRIGGERS FOR ", - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.",_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.",_ins
	ON BUSDTA.", AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.",_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.",_del.EntityBucketMasterId= inserted.EntityBucketMasterId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.",_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.",_upd
	ON BUSDTA.", AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.",
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.",.EntityBucketMasterId= inserted.EntityBucketMasterId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.",_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.",_dlt
	ON BUSDTA.", AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.",_del (EntityBucketMasterId, last_modified )
	SELECT deleted.EntityBucketMasterId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ", - END */
