 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Question_Template"
SET "TemplateName" = {ml r."TemplateName"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "QuestionId" = {ml r."QuestionId"} AND "ResponseID" = {ml r."ResponseID"} AND "RevisionId" = {ml r."RevisionId"} AND "TemplateId" = {ml r."TemplateId"}
 
