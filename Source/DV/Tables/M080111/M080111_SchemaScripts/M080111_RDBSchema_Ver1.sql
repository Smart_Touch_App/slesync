------- /*[BUSDTA].[M080111]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M080111"     (
	"CTID" numeric(8, 0) not null,
	"CTTYP" nvarchar(10) null,
	"CTCD" nvarchar(5) not null,
	"CTDSC1" nvarchar(50) null,
	"CTTXA1" nvarchar(10) null,
	PRIMARY KEY ("CTID")
)

------- /*[BUSDTA].[M080111]*/ - End