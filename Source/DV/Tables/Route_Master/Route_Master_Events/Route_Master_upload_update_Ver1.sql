 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Master"
SET "RouteDescription" = {ml r."RouteDescription"}, "RouteAdressBookNumber" = {ml r."RouteAdressBookNumber"}, "BranchNumber" = {ml r."BranchNumber"}, "BranchAdressBookNumber" = {ml r."BranchAdressBookNumber"}, "DefaultUser" = {ml r."DefaultUser"}, "VehicleID" = {ml r."VehicleID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "RouteMasterID" = {ml r."RouteMasterID"} AND "RouteName" = {ml r."RouteName"}
 
