
/* [BUSDTA].[Customer_Ledger] - begins */

/* TableDDL - [BUSDTA].[Customer_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Ledger]
	(
	  [CustomerLedgerId] NUMERIC(30,6) NOT NULL DEFAULT autoincrement
	, [ReceiptID] NUMERIC(8,0) NULL
	, [InvoiceId] NUMERIC(8,0) NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CustShipToId] NUMERIC(8,0) NOT NULL
	, [IsActive] NVARCHAR (1) NULL
	, [InvoiceGrossAmt] NUMERIC(10,4) NULL
	, [InvoiceOpenAmt] NUMERIC(10,4) NULL
	, [ReceiptUnAppliedAmt] NUMERIC(10,4) NULL
	, [ReceiptAppliedAmt] NUMERIC(10,4) NULL
	, [ConcessionAmt] NUMERIC(10,4) NULL
	, [ConcessionCodeId] NUMERIC(3,0) NULL
	, [DateApplied] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, PRIMARY KEY ([CustomerLedgerId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Customer_Ledger] - End */

