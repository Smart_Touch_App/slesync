
/* [BUSDTA].[Customer_Ledger] - begins */

/* TableDDL - [BUSDTA].[Customer_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Ledger]
	(
	  [CustomerLedgerId] decimal(15,0) NOT NULL DEFAULT autoincrement
	, [ReceiptID] decimal(15,0) NULL
	, [InvoiceId] decimal(15,0) NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CustShipToId] NUMERIC(8,0) NOT NULL
	, [IsActive] NVARCHAR (1) NULL
	, [InvoiceGrossAmt] decimal(15,4) NULL
	, [InvoiceOpenAmt] decimal(15,4) NULL
	, [ReceiptUnAppliedAmt] decimal(15,4) NULL
	, [ReceiptAppliedAmt] decimal(15,4) NULL
	, [ConcessionAmt] decimal(15,4) NULL
	, [ConcessionCodeId] NUMERIC(3,0) NULL
	, [DateApplied] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, PRIMARY KEY ([CustomerLedgerId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Customer_Ledger] - End */

