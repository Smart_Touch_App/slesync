
/* [dbo].[tblStatsGroups] - begins */

/* TableDDL - [dbo].[tblStatsGroups] - Start */
IF OBJECT_ID('[dbo].[tblStatsGroups]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblStatsGroups]
	(
	  [StatsGroupID] INT NOT NULL IDENTITY(1,1)
	, [GroupName] VARCHAR(128) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLSTATSGROUPS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblStatsGroups] PRIMARY KEY ([StatsGroupID] ASC)
	)

END
/* TableDDL - [dbo].[tblStatsGroups] - End */

/* SHADOW TABLE FOR [dbo].[tblStatsGroups] - Start */
IF OBJECT_ID('[dbo].[tblStatsGroups_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblStatsGroups_del]
	(
	  [StatsGroupID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([StatsGroupID] ASC)
	)

END
/* SHADOW TABLE FOR [dbo].[tblStatsGroups] - End */
/* TRIGGERS FOR tblStatsGroups - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblStatsGroups_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblStatsGroups_ins
	ON dbo.tblStatsGroups AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblStatsGroups_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblStatsGroups_del.StatsGroupID= inserted.StatsGroupID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblStatsGroups_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblStatsGroups_upd
	ON dbo.tblStatsGroups AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblStatsGroups
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblStatsGroups.StatsGroupID= inserted.StatsGroupID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblStatsGroups_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblStatsGroups_dlt
	ON dbo.tblStatsGroups AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblStatsGroups_del (StatsGroupID, last_modified )
	SELECT deleted.StatsGroupID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblStatsGroups - END */
