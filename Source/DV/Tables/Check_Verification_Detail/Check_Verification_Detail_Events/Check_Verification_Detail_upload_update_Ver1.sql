 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Check_Verification_Detail"
SET "SettlementDetailId" = {ml r."SettlementDetailId"}, "CheckDetailsId" = {ml r."CheckDetailsId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CheckVerificationDetailId" = {ml r."CheckVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
 
