/*
/* [BUSDTA].[Check_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Check_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Check_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Check_Verification_Detail]
	(
	  [CheckVerificationDetailId] NUMERIC NOT NULL
	, [SettlementDetailId] NUMERIC NOT NULL
	, [CheckDetailsId] NUMERIC NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([CheckVerificationDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Check_Verification_Detail] - End */
*/
