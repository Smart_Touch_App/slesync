
/* [BUSDTA].[Inventory] - begins */

/* TableDDL - [BUSDTA].[Inventory] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory]
	(
	  [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NVARCHAR (25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OnHandQuantity] DECIMAL(15,0) NULL
	, [CommittedQuantity] DECIMAL(15,0) NULL
	, [HeldQuantity] DECIMAL(15,0) NULL
	, [ParLevel] DECIMAL(15,0) NULL
	, [LastReceiptDate] DATETIME NULL
	, [LastConsumeDate] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Inventory] - End */

