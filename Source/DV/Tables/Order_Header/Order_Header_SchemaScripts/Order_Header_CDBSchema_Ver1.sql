/*
/* [BUSDTA].[Order_Header] - begins */

/* TableDDL - [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header]
	(
	  [Order_ID] INT NOT NULL
	, [Customer_Id] FLOAT NULL
	, [Order_Date] DATE NULL
	, [Created_By] INT NULL
	, [Created_On] DATE NOT NULL
	, [Is_Deleted] BIT NULL
	, [Total_Coffee] FLOAT NULL
	, [Total_Allied] FLOAT NULL
	, [Energy_Surcharge] FLOAT NULL
	, [Order_Total_Amt] FLOAT NULL
	, [Sales_Tax_Amt] FLOAT NULL
	, [Invoice_Total] FLOAT NULL
	, [Surcharge_Reason_Code] VARCHAR(5) NULL
	, [payment_type] NVARCHAR(10) NULL
	, [payment_id] NVARCHAR(10) NULL
	, [Order_State] NVARCHAR(20) NULL
	, [Order_Sub_State] NVARCHAR(20) NULL
	, [updated_at] DATETIME NULL
	, [VoidReason] BIGINT NULL
	, [OrderSeries] INT NOT NULL
	, [RouteNo] NCHAR(3) NULL
	, [ChargeOnAccount] BIT NULL
	, [HoldCommitted] BIT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ORDER_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Order_Header] PRIMARY KEY ([Order_ID] ASC, [OrderSeries] ASC)
	)

END
/* TableDDL - [BUSDTA].[Order_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header_del]
	(
	  [Order_ID] INT
	, [OrderSeries] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Order_ID] ASC, [OrderSeries] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Order_Header] - End */
/* TRIGGERS FOR Order_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Header_ins
	ON BUSDTA.Order_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Header_del.Order_ID= inserted.Order_ID AND BUSDTA.Order_Header_del.OrderSeries= inserted.OrderSeries
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_upd
	ON BUSDTA.Order_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Header.Order_ID= inserted.Order_ID AND BUSDTA.Order_Header.OrderSeries= inserted.OrderSeries');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Header_dlt
	ON BUSDTA.Order_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Header_del (Order_ID, OrderSeries, last_modified )
	SELECT deleted.Order_ID, deleted.OrderSeries, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Header - END */
*/
