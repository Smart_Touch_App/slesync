 
SELECT "BUSDTA"."Order_Header"."Order_ID"
,"BUSDTA"."Order_Header"."Customer_Id"
,"BUSDTA"."Order_Header"."Order_Date"
,"BUSDTA"."Order_Header"."Created_By"
,"BUSDTA"."Order_Header"."Created_On"
,"BUSDTA"."Order_Header"."Is_Deleted"
,"BUSDTA"."Order_Header"."Total_Coffee"
,"BUSDTA"."Order_Header"."Total_Allied"
,"BUSDTA"."Order_Header"."Energy_Surcharge"
,"BUSDTA"."Order_Header"."Order_Total_Amt"
,"BUSDTA"."Order_Header"."Sales_Tax_Amt"
,"BUSDTA"."Order_Header"."Invoice_Total"
,"BUSDTA"."Order_Header"."Surcharge_Reason_Code"
,"BUSDTA"."Order_Header"."payment_type"
,"BUSDTA"."Order_Header"."payment_id"
,"BUSDTA"."Order_Header"."Order_State"
,"BUSDTA"."Order_Header"."Order_Sub_State"
,"BUSDTA"."Order_Header"."updated_at"
,"BUSDTA"."Order_Header"."VoidReason"
,"BUSDTA"."Order_Header"."OrderSeries"
,"BUSDTA"."Order_Header"."RouteNo"
,"BUSDTA"."Order_Header"."HoldCommitted"

FROM  BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.Order_Header
WHERE "BUSDTA"."Order_Header"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and Customer_Id=CRCRAN8 and FFUSER= {ml s.username}
 
