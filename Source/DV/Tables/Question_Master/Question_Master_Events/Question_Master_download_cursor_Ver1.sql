 
SELECT "BUSDTA"."Question_Master"."QuestionId"
,"BUSDTA"."Question_Master"."QuestionTitle"
,"BUSDTA"."Question_Master"."QuestionDescription"
,"BUSDTA"."Question_Master"."IsMandatory"
,"BUSDTA"."Question_Master"."IsMultivalue"
,"BUSDTA"."Question_Master"."IsDescriptive"
,"BUSDTA"."Question_Master"."CreatedBy"
,"BUSDTA"."Question_Master"."CreatedDatetime"
,"BUSDTA"."Question_Master"."UpdatedBy"
,"BUSDTA"."Question_Master"."UpdatedDatetime"

FROM "BUSDTA"."Question_Master"
WHERE "BUSDTA"."Question_Master"."last_modified">= {ml s.last_table_download}
 
