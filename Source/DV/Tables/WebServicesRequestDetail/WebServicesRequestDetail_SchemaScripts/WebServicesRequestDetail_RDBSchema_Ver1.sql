
IF OBJECT_ID('[BUSDTA].[WebServicesRequestDetail]') IS NULL
BEGIN

CREATE TABLE "BUSDTA"."WebServicesRequestDetail" (
    "RequestID"                      numeric(8,0) NOT NULL DEFAULT autoincrement
   ,"ResquestBLOB"                   long nvarchar NULL
   ,"ResquestType"                   nvarchar(700) NULL
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("RequestID" ASC) 
)
END