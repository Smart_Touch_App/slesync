 
/* Delete the row in the consolidated database. */
DELETE FROM "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
WHERE "SalesOrderId" = {ml r."SalesOrderId"} AND "ReturnOrderID" = {ml r."ReturnOrderID"} AND "OrderDetailID" = {ml r."OrderDetailID"}
