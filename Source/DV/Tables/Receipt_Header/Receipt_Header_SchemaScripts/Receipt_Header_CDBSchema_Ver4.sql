/* [BUSDTA].[Receipt_Header] - begins */

/* TableDDL - [BUSDTA].[Receipt_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Receipt_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Receipt_Header]
	(
	  [CustShipToId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ReceiptID] decimal (15,0) NOT NULL
	, [ReceiptNumber] NCHAR(20) NULL
	, [ReceiptDate] DATE NULL
	, [TransactionAmount] decimal (15,4) NULL
	, [PaymentMode] NCHAR(1) NULL
	, [ReasonCodeId] NUMERIC(3,0) NULL
	, [CreditReasonCodeId] NUMERIC(3,0) NULL
	, [CreditMemoNote] NCHAR(200) NULL
	, [TransactionMode] NCHAR(1) NULL
	, [ChequeNum] NCHAR(10) NULL
	, [ChequeDate] DATE NULL
	, [StatusId] NUMERIC(3,0) NULL
	, [JDEStatusId] NUMERIC(3,0) NULL
	, [JDEReferenceId] decimal (15,0) NULL
	, [SettelmentId] decimal (15,0) NULL
	, [AuthoritySignature] VARBINARY(MAX) NULL
	, [CustomerSignature] VARBINARY(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_RECEIPT_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Receipt_Header] PRIMARY KEY ([CustShipToId] ASC, [RouteId] ASC, [ReceiptID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Receipt_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Receipt_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Receipt_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Receipt_Header_del]
	(
	  [CustShipToId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, [ReceiptID] decimal (15,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustShipToId] ASC, [RouteId] ASC, [ReceiptID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Receipt_Header] - End */
/* TRIGGERS FOR Receipt_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Receipt_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Receipt_Header_ins
	ON BUSDTA.Receipt_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Receipt_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Receipt_Header_del.CustShipToId= inserted.CustShipToId AND BUSDTA.Receipt_Header_del.ReceiptID= inserted.ReceiptID AND BUSDTA.Receipt_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Receipt_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Receipt_Header_upd
	ON BUSDTA.Receipt_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Receipt_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Receipt_Header.CustShipToId= inserted.CustShipToId AND BUSDTA.Receipt_Header.ReceiptID= inserted.ReceiptID AND BUSDTA.Receipt_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Receipt_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Receipt_Header_dlt
	ON BUSDTA.Receipt_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Receipt_Header_del (CustShipToId, ReceiptID, RouteId, last_modified )
	SELECT deleted.CustShipToId, deleted.ReceiptID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Receipt_Header - END */
