
SELECT "EquipmentTypeID"
,"EquipmentType"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "dbo"."tblEquipmentType"
WHERE "dbo"."tblEquipmentType"."last_modified">= {ml s.last_table_download}
