/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M0112"
SET "NDDTTM" = {ml r."NDDTTM"}, "NDDTLS" = {ml r."NDDTLS"}, "NDTYP" = {ml r."NDTYP"}, "NDDFLT" = {ml r."NDDFLT"}, "NDCRBY" = {ml r."NDCRBY"}, "NDCRDT" = {ml r."NDCRDT"}, "NDUPBY" = {ml r."NDUPBY"}, "NDUPDT" = {ml r."NDUPDT"}
WHERE "NDAN8" = {ml r."NDAN8"} AND "NDID" = {ml r."NDID"}