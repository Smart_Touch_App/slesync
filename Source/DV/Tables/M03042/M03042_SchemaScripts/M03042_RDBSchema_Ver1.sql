------- /*[BUSDTA].[M03042]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M03042"     (
	"PDAN8" numeric(8, 0) not null,
	"PDCO" varchar(5) not null,
	"PDID" numeric(8, 0) not null,
	"PDPAMT" numeric(8, 4) null,
	"PDPMODE" bit null,
	"PDCHQNO" nvarchar(10) null,
	"PDCHQDT" date null,
	"PDCRBY" nvarchar(10) null,
	"PDCRDT" date null,
	"PDUPBY" nvarchar(10) null,
	"PDUPDT" date null,
	"PDRCID" integer null,
	"PDTRMD" bit null,
	PRIMARY KEY ("PDAN8", "PDCO", "PDID")
)

------- /*[BUSDTA].[M03042]*/ - End