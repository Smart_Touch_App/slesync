
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Invoice_Header"
SET "CustomerId" = {ml r."CustomerId"}, "OrderId" = {ml r."OrderId"}, "ARInvoiceNumber" = {ml r."ARInvoiceNumber"}, "DeviceInvoiceNumber" = {ml r."DeviceInvoiceNumber"}, "ARInvoiceDate" = {ml r."ARInvoiceDate"}, "DeviceInvoiceDate" = {ml r."DeviceInvoiceDate"}, "DeviceStatusID" = {ml r."DeviceStatusID"}, "ARStatusID" = {ml r."ARStatusID"}, "ReasonCodeId" = {ml r."ReasonCodeId"}, "DueDate" = {ml r."DueDate"}, "InvoicePaymentType" = {ml r."InvoicePaymentType"}, "GrossAmt" = {ml r."GrossAmt"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "InvoiceID" = {ml r."InvoiceID"} AND "RouteId" = {ml r."RouteId"}
 

