/* [dbo].[tblResponseLog] - begins */

/* TableDDL - [dbo].[tblResponseLog] - Start */
IF OBJECT_ID('[dbo].[tblResponseLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblResponseLog]
	(
	  [ResponseLogID] INTEGER NOT NULL default autoincrement
	, [DeviceID] VARCHAR(100) NULL
	, [ResponseOn] DATETIME NULL DEFAULT(getdate())
	, [Request] XML NULL
	, [Response] XML NULL
	)
END
/* TableDDL - [dbo].[tblResponseLog] - End */
