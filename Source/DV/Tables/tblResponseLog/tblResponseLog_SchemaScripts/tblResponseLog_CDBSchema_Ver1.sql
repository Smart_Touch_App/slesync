/*
/* [dbo].[tblResponseLog] - begins */

/* TableDDL - [dbo].[tblResponseLog] - Start */
IF OBJECT_ID('[dbo].[tblResponseLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblResponseLog]
	(
	  [ResponseLogID] INT NOT NULL IDENTITY(1,1)
	, [DeviceID] VARCHAR(100) NULL
	, [ResponseOn] DATETIME NULL CONSTRAINT DF_TBLRESPONSELOG_ResponseOn DEFAULT(getdate())
	, [Request] XML NULL
	, [Response] XML NULL
	)

END
/* TableDDL - [dbo].[tblResponseLog] - End */

