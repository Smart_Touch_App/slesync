/* [BUSDTA].[PickOrder_Exception] - begins */

/* TableDDL - [BUSDTA].[PickOrder_Exception] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder_Exception]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder_Exception]
	(
	  [PickOrder_Exception_Id] INT NOT NULL
	, [Order_Id] INT NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Item_Number] NCHAR(25) NULL
	, [Exception_Qty] INT NULL
	, [UOM] NCHAR(2) NULL
	, [Exception_Reason] VARCHAR(25) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PICKORDER_EXCEPTION_last_modified DEFAULT(getdate())
	, [ManualPickReasonCode] INT NULL
	, [ManuallyPickCount] INT NULL
	, CONSTRAINT [PK_PickOrder_Exception] PRIMARY KEY ([PickOrder_Exception_Id] ASC, [Order_Id] ASC, [RouteId] ASC)
	)

END
/* TableDDL - [BUSDTA].[PickOrder_Exception] - End */

/* SHADOW TABLE FOR [BUSDTA].[PickOrder_Exception] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder_Exception_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder_Exception_del]
	(
	  [PickOrder_Exception_Id] INT
	, [Order_Id] INT
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PickOrder_Exception_Id] ASC, [Order_Id] ASC, [RouteId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[PickOrder_Exception] - End */
/* TRIGGERS FOR PickOrder_Exception - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PickOrder_Exception_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PickOrder_Exception_ins
	ON BUSDTA.PickOrder_Exception AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PickOrder_Exception_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PickOrder_Exception_del.Order_Id= inserted.Order_Id AND BUSDTA.PickOrder_Exception_del.PickOrder_Exception_Id= inserted.PickOrder_Exception_Id AND BUSDTA.PickOrder_Exception_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PickOrder_Exception_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_Exception_upd
	ON BUSDTA.PickOrder_Exception AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PickOrder_Exception
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PickOrder_Exception.Order_Id= inserted.Order_Id AND BUSDTA.PickOrder_Exception.PickOrder_Exception_Id= inserted.PickOrder_Exception_Id AND BUSDTA.PickOrder_Exception.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PickOrder_Exception_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_Exception_dlt
	ON BUSDTA.PickOrder_Exception AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PickOrder_Exception_del (Order_Id, PickOrder_Exception_Id, RouteId, last_modified )
	SELECT deleted.Order_Id, deleted.PickOrder_Exception_Id, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PickOrder_Exception - END */
