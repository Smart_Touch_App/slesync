/* [BUSDTA].[PreTrip_Inspection_Header] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Header]
	(
	  [PreTripInspectionHeaderId] decimal (15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TemplateId] NUMERIC(8,0) NULL
	, [PreTripDateTime] DATETIME NULL
	, [UserName] NCHAR(50) NULL
	, [StatusId] NUMERIC(3,0) NULL
	, [VehicleMake] NCHAR(25) NULL
	, [VehicleNumber] decimal (15,0) NULL
	, [OdoMmeterReading] decimal (15,4) NULL
	, [Comment] NCHAR(100) NULL
	, [VerSignature] VARBINARY(MAX) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PRETRIP_INSPECTION_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_PreTrip_Inspection_Header] PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Header] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Header_del]
	(
	  [PreTripInspectionHeaderId] decimal (15,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Header] - End */
/* TRIGGERS FOR PreTrip_Inspection_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PreTrip_Inspection_Header_ins
	ON BUSDTA.PreTrip_Inspection_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PreTrip_Inspection_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PreTrip_Inspection_Header_del.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Header_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Header_upd
	ON BUSDTA.PreTrip_Inspection_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PreTrip_Inspection_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PreTrip_Inspection_Header.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Header.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Header_dlt
	ON BUSDTA.PreTrip_Inspection_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PreTrip_Inspection_Header_del (PreTripInspectionHeaderId, RouteId, last_modified )
	SELECT deleted.PreTripInspectionHeaderId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PreTrip_Inspection_Header - END */
