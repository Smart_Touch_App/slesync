/* [BUSDTA].[PreTrip_Inspection_Header] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Header]
	(
	  [PreTripInspectionHeaderId] NUMERIC NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [TemplateId] NUMERIC NULL
	, [PreTripDateTime] DATETIME NULL
	, [UserName] NCHAR(50) NULL
	, [StatusId] NUMERIC NULL
	, [VehicleMake] NCHAR(25) NULL
	, [VehicleNumber] NUMERIC NULL
	, [OdoMmeterReading] NUMERIC NULL
	, [Comment] NCHAR(100) NULL
	, [VerSignature] VARBINARY(MAX) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Header] - End */
