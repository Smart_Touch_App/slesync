 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Response_Master"
SET "Response" = {ml r."Response"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ResponseId" = {ml r."ResponseId"}
 
