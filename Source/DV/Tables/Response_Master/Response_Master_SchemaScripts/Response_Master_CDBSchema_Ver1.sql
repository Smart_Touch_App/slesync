/* [BUSDTA].[Response_Master] - begins */

/* TableDDL - [BUSDTA].[Response_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Response_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Response_Master]
	(
	  [ResponseId] NUMERIC NOT NULL
	, [Response] NCHAR(50) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_RESPONSE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Response_Master] PRIMARY KEY ([ResponseId] ASC)
	)

END
/* TableDDL - [BUSDTA].[Response_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Response_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Response_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Response_Master_del]
	(
	  [ResponseId] NUMERIC
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ResponseId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Response_Master] - End */
/* TRIGGERS FOR Response_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Response_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Response_Master_ins
	ON BUSDTA.Response_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Response_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Response_Master_del.ResponseId= inserted.ResponseId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Response_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Response_Master_upd
	ON BUSDTA.Response_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Response_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Response_Master.ResponseId= inserted.ResponseId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Response_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Response_Master_dlt
	ON BUSDTA.Response_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Response_Master_del (ResponseId, last_modified )
	SELECT deleted.ResponseId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Response_Master - END */
