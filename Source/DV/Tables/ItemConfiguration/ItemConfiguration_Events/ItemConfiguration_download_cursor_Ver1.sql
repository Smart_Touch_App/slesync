 
SELECT "BUSDTA"."ItemConfiguration"."ItemID"
,"BUSDTA"."ItemConfiguration"."RouteEnabled"
,"BUSDTA"."ItemConfiguration"."Sellable"
,"BUSDTA"."ItemConfiguration"."AllowSearch"
,"BUSDTA"."ItemConfiguration"."PrimaryUM"
,"BUSDTA"."ItemConfiguration"."PricingUM"
,"BUSDTA"."ItemConfiguration"."TransactionUM"
,"BUSDTA"."ItemConfiguration"."OtherUM1"
,"BUSDTA"."ItemConfiguration"."OtherUM2"
,"BUSDTA"."ItemConfiguration"."AllowLooseSample"
,"BUSDTA"."ItemConfiguration"."CreatedBy"
,"BUSDTA"."ItemConfiguration"."CreatedDatetime"
,"BUSDTA"."ItemConfiguration"."UpdatedBy"
,"BUSDTA"."ItemConfiguration"."UpdatedDatetime"

FROM "BUSDTA"."ItemConfiguration"
WHERE "BUSDTA"."ItemConfiguration"."last_modified">= {ml s.last_table_download}
 
