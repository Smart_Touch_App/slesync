/* [dbo].[tblUOM] - begins */

/* TableDDL - [dbo].[tblUOM] - Start */
IF OBJECT_ID('[dbo].[tblUOM]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblUOM]
	(
	  [UOMID] INTEGER NOT NULL 
	, [UOMLable] VARCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([UOMID] ASC)
	)
END
/* TableDDL - [dbo].[tblUOM] - End */
