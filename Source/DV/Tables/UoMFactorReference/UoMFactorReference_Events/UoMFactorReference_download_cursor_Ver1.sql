 
SELECT "BUSDTA"."UoMFactorReference"."ItemID"
,"BUSDTA"."UoMFactorReference"."FromUOM"
,"BUSDTA"."UoMFactorReference"."ToUOM"
,"BUSDTA"."UoMFactorReference"."ConversionFactor"
,"BUSDTA"."UoMFactorReference"."GenerationDate"
,"BUSDTA"."UoMFactorReference"."CreatedBy"
,"BUSDTA"."UoMFactorReference"."CreatedDatetime"
,"BUSDTA"."UoMFactorReference"."UpdatedBy"
,"BUSDTA"."UoMFactorReference"."UpdatedDatetime"

FROM "BUSDTA"."UoMFactorReference"
WHERE "BUSDTA"."UoMFactorReference"."last_modified">= {ml s.last_table_download}
 
