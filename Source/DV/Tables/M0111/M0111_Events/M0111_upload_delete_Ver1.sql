/* Delete the row from the consolidated database. */
DELETE FROM "BUSDTA"."M0111"
WHERE "CDAN8" = {ml r."CDAN8"}
	AND "CDIDLN" = {ml r."CDIDLN"}
	AND "CDRCK7" = {ml r."CDRCK7"}
	AND "CDCNLN" = {ml r."CDCNLN"}
	AND "CDID" = {ml r."CDID"}