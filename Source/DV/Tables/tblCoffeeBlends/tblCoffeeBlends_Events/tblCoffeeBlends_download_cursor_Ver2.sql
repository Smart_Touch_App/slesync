 
SELECT "BUSDTA"."tblCoffeeBlends"."CoffeeBlendID"
,"BUSDTA"."tblCoffeeBlends"."CoffeeBlend"
,"BUSDTA"."tblCoffeeBlends"."CreatedBy"
,"BUSDTA"."tblCoffeeBlends"."CreatedDatetime"
,"BUSDTA"."tblCoffeeBlends"."UpdatedBy"
,"BUSDTA"."tblCoffeeBlends"."UpdatedDatetime"

FROM "BUSDTA"."tblCoffeeBlends"
WHERE "BUSDTA"."tblCoffeeBlends"."last_modified">= {ml s.last_table_download}
