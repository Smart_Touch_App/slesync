
------- /*[BUSDTA].[ReasonCodeMaster]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."ReasonCodeMaster"     (
	"ReasonCodeId" integer not null,
	"ReasonCode" nvarchar(5) null,
	"ReasonCodeDescription" nvarchar(100) null,
	"ReasonCodeType" nvarchar(50) null,
	PRIMARY KEY ("ReasonCodeId")
)

------- /*[BUSDTA].[ReasonCodeMaster]*/ - End