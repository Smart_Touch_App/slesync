
/* [BUSDTA].[M50012] - begins */

/* TableDDL - [BUSDTA].[M50012] - Start */
IF OBJECT_ID('[BUSDTA].[M50012]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M50012]
	(
	  [TDID] DECIMAL(15,0) NOT NULL DEFAULT autoincrement
	, [TDROUT] VARCHAR(10) NOT NULL
	, [TDTYP] NVARCHAR (50) NOT NULL
	, [TDCLASS] NVARCHAR (100) NULL
	, [TDDTLS] LONG VARCHAR NULL
	, [TDSTRTTM] "datetime" NULL
	, [TDENDTM] "datetime" NULL
	, [TDSTID] NVARCHAR (15) NULL
	, [TDAN8] NVARCHAR (15) NULL
	, [TDSTTLID] NVARCHAR (15) NULL
	, [TDPNTID] DECIMAL(15,0) NULL
	, [TDSTAT] NVARCHAR (100) NULL
	, [TDREFHDRID] DECIMAL(15,0) NULL
	, [TDREFHDRTYP] NVARCHAR (30) NULL
	, [TDCRBY] NVARCHAR (50) NULL
	, [TDCRDT] DATETIME NULL
	, [TDUPBY] NVARCHAR (50) NULL
	, [TDUPDT] DATETIME NULL
	, PRIMARY KEY ([TDID] ASC, [TDROUT] ASC)
	)
END
/* TableDDL - [BUSDTA].[M50012] - End */

