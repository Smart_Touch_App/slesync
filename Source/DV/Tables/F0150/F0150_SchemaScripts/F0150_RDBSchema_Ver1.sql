
------- /*[BUSDTA].[F0150]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F0150"     (
	"MAOSTP" nvarchar(3) not null,
	"MAPA8" numeric(8, 0) not null,
	"MAAN8" numeric(8, 0) not null,
	"MABEFD" numeric(18, 0) null,
	"MAEEFD" numeric(18, 0) null,
	PRIMARY KEY ("MAOSTP", "MAPA8", "MAAN8")
)

------- /*[BUSDTA].[F0150]*/ - End