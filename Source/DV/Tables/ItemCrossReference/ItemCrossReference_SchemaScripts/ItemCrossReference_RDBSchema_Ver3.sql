
/* [BUSDTA].[ItemCrossReference] - begins */

/* TableDDL - [BUSDTA].[ItemCrossReference] - Start */
IF OBJECT_ID('[BUSDTA].[ItemCrossReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemCrossReference]
	(
	  [ItemReferenceID] DECIMAL(15,0) NOT NULL
	, [CrossReferenceID] DECIMAL(15,0) NOT NULL
	, [MobileType] NVARCHAR (10) NULL
	, [EffectiveFrom] DATE NULL
	, [EffectiveThru] DATE NULL
	, [CrossData] NVARCHAR (50) NULL
	, [AddressNumber] NUMERIC(8,0) NULL
	, [CrossReferenceType] NVARCHAR (50) NULL
	, [ItemNumber] NUMERIC(8,0) NULL
	, [ItemRevisionLevel] NVARCHAR (50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME 
	, PRIMARY KEY ([ItemReferenceID] ASC, [CrossReferenceID] ASC)
	)
END
/* TableDDL - [BUSDTA].[ItemCrossReference] - End */

