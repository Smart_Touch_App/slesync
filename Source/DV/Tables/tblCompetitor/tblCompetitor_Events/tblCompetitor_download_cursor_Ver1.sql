 
SELECT "dbo"."tblCompetitor"."CompetitorID"
,"dbo"."tblCompetitor"."Competitor"
,"dbo"."tblCompetitor"."CreatedBy"
,"dbo"."tblCompetitor"."CreatedDatetime"
,"dbo"."tblCompetitor"."UpdatedBy"
,"dbo"."tblCompetitor"."UpdatedDatetime"

FROM "dbo"."tblCompetitor"
WHERE "dbo"."tblCompetitor"."last_modified">= {ml s.last_table_download}
