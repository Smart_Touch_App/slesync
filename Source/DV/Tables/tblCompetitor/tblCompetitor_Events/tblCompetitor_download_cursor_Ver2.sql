 
SELECT "BUSDTA"."tblCompetitor"."CompetitorID"
,"BUSDTA"."tblCompetitor"."Competitor"
,"BUSDTA"."tblCompetitor"."CreatedBy"
,"BUSDTA"."tblCompetitor"."CreatedDatetime"
,"BUSDTA"."tblCompetitor"."UpdatedBy"
,"BUSDTA"."tblCompetitor"."UpdatedDatetime"

FROM "BUSDTA"."tblCompetitor"
WHERE "BUSDTA"."tblCompetitor"."last_modified">= {ml s.last_table_download}
