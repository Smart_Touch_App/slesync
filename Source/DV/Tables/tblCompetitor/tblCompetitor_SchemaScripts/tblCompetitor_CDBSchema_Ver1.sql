/* [dbo].[tblCompetitor] - begins */

/* TableDDL - [dbo].[tblCompetitor] - Start */
IF OBJECT_ID('[dbo].[tblCompetitor]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCompetitor]
	(
	  [CompetitorID] INT NOT NULL IDENTITY(1,1)
	, [Competitor] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOMPETITOR_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOMPETITOR_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOMPETITOR_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCompetitor] PRIMARY KEY ([CompetitorID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblCompetitor] - End */

/* SHADOW TABLE FOR [dbo].[tblCompetitor] - Start */
IF OBJECT_ID('[dbo].[tblCompetitor_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCompetitor_del]
	(
	  [CompetitorID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CompetitorID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblCompetitor] - End */
/* TRIGGERS FOR tblCompetitor - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblCompetitor_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblCompetitor_ins
	ON dbo.tblCompetitor AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblCompetitor_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblCompetitor_del.CompetitorID= inserted.CompetitorID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblCompetitor_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblCompetitor_upd
	ON dbo.tblCompetitor AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblCompetitor
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblCompetitor.CompetitorID= inserted.CompetitorID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblCompetitor_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblCompetitor_dlt
	ON dbo.tblCompetitor AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblCompetitor_del (CompetitorID, last_modified )
	SELECT deleted.CompetitorID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCompetitor - END */
