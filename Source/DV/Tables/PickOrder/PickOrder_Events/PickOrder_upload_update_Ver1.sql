 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."PickOrder"
SET "Item_Number" = {ml r."Item_Number"}, "Order_Qty" = {ml r."Order_Qty"}, "Order_UOM" = {ml r."Order_UOM"}, "Picked_Qty_Primary_UOM" = {ml r."Picked_Qty_Primary_UOM"}, "Primary_UOM" = {ml r."Primary_UOM"}, "Order_Qty_Primary_UOM" = {ml r."Order_Qty_Primary_UOM"}, "On_Hand_Qty_Primary" = {ml r."On_Hand_Qty_Primary"}, "Last_Scan_Mode" = {ml r."Last_Scan_Mode"}, "Item_Scan_Sequence" = {ml r."Item_Scan_Sequence"}, "Picked_By" = {ml r."Picked_By"}, "IsOnHold" = {ml r."IsOnHold"}, "Reason_Code_Id" = {ml r."Reason_Code_Id"}, "ManuallyPickCount" = {ml r."ManuallyPickCount"}
WHERE "Order_ID" = {ml r."Order_ID"} AND "PickOrder_Id" = {ml r."PickOrder_Id"} AND "RouteId" = {ml r."RouteId"}
 
