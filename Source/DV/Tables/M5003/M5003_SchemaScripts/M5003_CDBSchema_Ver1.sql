
/* [BUSDTA].[M5003] - begins */

/* TableDDL - [BUSDTA].[M5003] - Start */
IF OBJECT_ID('[BUSDTA].[M5003]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5003]	(	  [ALID] NUMERIC NOT NULL	, [ALHDID] NUMERIC NULL	, [ALTMSTMP] DATETIME NOT NULL	, [ALDSC] NCHAR(250) NULL	, [ALTYP] NCHAR(50) NOT NULL	, [ALITX] BIT NULL	, [ALCRBY] NCHAR(50) NULL	, [ALCRDT] DATETIME NULL	, [ALUPBY] NCHAR(50) NULL	, [ALUPDT] DATETIME NULL	, last_modified DATETIME DEFAULT GETDATE()	, CONSTRAINT [PK_M5003] PRIMARY KEY ([ALID] ASC)	)
END
/* TableDDL - [BUSDTA].[M5003] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5003] - Start */
IF OBJECT_ID('[BUSDTA].[M5003_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5003_del]	(	  [ALID] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([ALID] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[M5003] - End */
/* TRIGGERS FOR M5003 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5003_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5003_ins
	ON BUSDTA.M5003 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5003_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5003_del.ALID= inserted.ALID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5003_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5003_upd
	ON BUSDTA.M5003 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5003
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5003.ALID= inserted.ALID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5003_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5003_dlt
	ON BUSDTA.M5003 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5003_del (ALID, last_modified )
	SELECT deleted.ALID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5003 - END */
