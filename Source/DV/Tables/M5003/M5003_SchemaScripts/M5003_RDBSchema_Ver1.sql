------- /*[BUSDTA].[M5003]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M5003"(	-- ActivityLedger
	ALID			numeric(8,0) NOT NULL DEFAULT autoincrement,	-- ActivityID
	ALHDID			numeric(8,0) NULL,								-- ActivityHeaderID
	ALTMSTMP		datetime NOT NULL,								-- ActivityTimestamp
	ALDSC			nvarchar(250) NULL,								-- ActivityDescription
	ALTYP			nvarchar(50) NOT NULL,							-- ActivityTypeKey
	ALITX bit NULL,													-- IsTxActivity
	ALCRBY						nvarchar(50),
    ALCRDT						datetime,
    ALUPBY						nvarchar(50),
    ALUPDT						datetime,
 PRIMARY KEY ("ALID" ASC) 
 ) 

 ------- /*[BUSDTA].[M5003]*/ - End