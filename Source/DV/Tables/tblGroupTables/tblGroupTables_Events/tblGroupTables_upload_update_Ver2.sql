 
/* Update the row in the consolidated database. */
UPDATE ."dbo"."tblGroupTables"
SET "RefDBSchema" = {ml r."RefDBSchema"}, "RefDBTable" = {ml r."RefDBTable"}, "ApplicationGroupID" = {ml r."ApplicationGroupID"}, "ConDBTable" = {ml r."ConDBTable"}, "IsMandatorySync" = {ml r."IsMandatorySync"}
WHERE "GroupTableID" = {ml r."GroupTableID"}
 
