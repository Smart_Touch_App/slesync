 
SELECT "dbo"."tblGroupTables"."GroupTableID"
,"dbo"."tblGroupTables"."RefDBSchema"
,"dbo"."tblGroupTables"."RefDBTable"
,"dbo"."tblGroupTables"."ApplicationGroupID"
,"dbo"."tblGroupTables"."ConDBTable"
,"dbo"."tblGroupTables"."IsMandatorySync"

FROM ."dbo"."tblGroupTables"
WHERE "dbo"."tblGroupTables"."last_modified">= {ml s.last_table_download}
 
