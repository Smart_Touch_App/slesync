
/* [BUSDTA].[M5002] - begins */

/* TableDDL - [BUSDTA].[M5002] - Start */
IF OBJECT_ID('[BUSDTA].[M5002]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5002]	(	  [TNID] NUMERIC NOT NULL	, [TNKEY] NCHAR(30) NOT NULL	, [TNTYP] NCHAR(25) NOT NULL	, [TNACTN] NCHAR(30) NULL	, [TNADSC] NCHAR(50) NULL	, [TNACTR] NCHAR(15) NULL	, [TNILDGRD] BIT NULL	, [TNCRBY] NCHAR(50) NULL	, [TNCRDT] DATETIME NULL	, [TNUPBY] NCHAR(50) NULL	, [TNUPDT] DATETIME NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M5002_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_M5002] PRIMARY KEY ([TNID] ASC)	)
END
/* TableDDL - [BUSDTA].[M5002] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5002] - Start */
IF OBJECT_ID('[BUSDTA].[M5002_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5002_del]	(	  [TNID] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([TNID] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[M5002] - End */
/* TRIGGERS FOR M5002 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5002_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5002_ins
	ON BUSDTA.M5002 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5002_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5002_del.TNID= inserted.TNID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5002_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5002_upd
	ON BUSDTA.M5002 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5002
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5002.TNID= inserted.TNID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5002_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5002_dlt
	ON BUSDTA.M5002 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5002_del (TNID, last_modified )
	SELECT deleted.TNID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5002 - END */
