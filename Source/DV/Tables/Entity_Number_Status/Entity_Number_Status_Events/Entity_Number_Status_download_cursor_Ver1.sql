 
SELECT "BUSDTA"."Entity_Number_Status"."EntityNumberStatusID"
,"BUSDTA"."Entity_Number_Status"."RouteId"
,"BUSDTA"."Entity_Number_Status"."Bucket"
,"BUSDTA"."Entity_Number_Status"."EntityBucketMasterId"
,"BUSDTA"."Entity_Number_Status"."RangeStart"
,"BUSDTA"."Entity_Number_Status"."RangeEnd"
,"BUSDTA"."Entity_Number_Status"."CurrentAllocated"
,"BUSDTA"."Entity_Number_Status"."IsConsumed"
,"BUSDTA"."Entity_Number_Status"."ConsumedOnDate"
,"BUSDTA"."Entity_Number_Status"."CreatedBy"
,"BUSDTA"."Entity_Number_Status"."CreatedDatetime"
,"BUSDTA"."Entity_Number_Status"."UpdatedBy"
,"BUSDTA"."Entity_Number_Status"."UpdatedDatetime"
FROM "BUSDTA"."Entity_Number_Status"
WHERE "BUSDTA"."Entity_Number_Status"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."Entity_Number_Status"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

