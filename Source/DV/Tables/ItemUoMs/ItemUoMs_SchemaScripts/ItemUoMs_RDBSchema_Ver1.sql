/* [BUSDTA].[ItemUoMs] - begins */

/* TableDDL - [BUSDTA].[ItemUoMs] - Start */
IF OBJECT_ID('[BUSDTA].[ItemUoMs]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemUoMs]
	(
	  [ItemID] NUMERIC NOT NULL
	, [UOM] NCHAR(2) NOT NULL
	, [CrossReferenceID] NUMERIC NULL
	, [CanSell] BIT NULL
	, [CanSample] BIT NULL
	, [CanRestock] BIT NULL
	, [DisplaySeq] NUMERIC NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([ItemID] ASC, [UOM] ASC)
	)
END
/* TableDDL - [BUSDTA].[ItemUoMs] - End */
