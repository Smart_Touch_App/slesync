
------- /*[BUSDTA].[F40942]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F40942"     (
	"CKCPGP" nvarchar(8) not null,
	"CKCGP1" nvarchar(3) not null,
	"CKCGP2" nvarchar(3) not null,
	"CKCGP3" nvarchar(3) not null,
	"CKCGP4" nvarchar(3) not null,
	"CKCGP5" nvarchar(3) not null,
	"CKCGP6" nvarchar(3) not null,
	"CKCGP7" nvarchar(3) not null,
	"CKCGP8" nvarchar(3) not null,
	"CKCGP9" nvarchar(3) not null,
	"CKCGP10" nvarchar(3) not null,
	"CKCGID" float null,
	PRIMARY KEY ("CKCPGP", "CKCGP1", "CKCGP2", "CKCGP3", "CKCGP4", "CKCGP5", "CKCGP6", "CKCGP7", "CKCGP8", "CKCGP9", "CKCGP10")
)

------- /*[BUSDTA].[F40942]*/ - End