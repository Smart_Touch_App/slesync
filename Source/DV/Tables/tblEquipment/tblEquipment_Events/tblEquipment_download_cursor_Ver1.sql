 
SELECT "dbo"."tblEquipment"."EquipmentID"
,"dbo"."tblEquipment"."ProspectID"
,"dbo"."tblEquipment"."EquipmentType"
,"dbo"."tblEquipment"."EquipmentCategoryID"
,"dbo"."tblEquipment"."EquipmentSubCategory"
,"dbo"."tblEquipment"."EquipmentQuantity"
,"dbo"."tblEquipment"."EquipmentOwned"
,"dbo"."tblEquipment"."CreatedBy"
,"dbo"."tblEquipment"."CreatedDatetime"
,"dbo"."tblEquipment"."UpdatedBy"
,"dbo"."tblEquipment"."UpdatedDatetime"

FROM "dbo"."tblEquipment"
WHERE "dbo"."tblEquipment"."last_modified">= {ml s.last_table_download}
 
