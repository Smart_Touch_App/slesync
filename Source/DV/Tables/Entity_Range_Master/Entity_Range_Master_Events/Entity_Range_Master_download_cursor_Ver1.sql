 
SELECT "BUSDTA"."Entity_Range_Master"."EntityRangeMasterID"
,"BUSDTA"."Entity_Range_Master"."EntityBucketMasterId"
,"BUSDTA"."Entity_Range_Master"."RangeStart"
,"BUSDTA"."Entity_Range_Master"."RangeEnd"
,"BUSDTA"."Entity_Range_Master"."LastAlloted"
,"BUSDTA"."Entity_Range_Master"."Size"
,"BUSDTA"."Entity_Range_Master"."Source"
,"BUSDTA"."Entity_Range_Master"."Active"
,"BUSDTA"."Entity_Range_Master"."CreatedBy"
,"BUSDTA"."Entity_Range_Master"."CreatedDatetime"
,"BUSDTA"."Entity_Range_Master"."UpdatedBy"
,"BUSDTA"."Entity_Range_Master"."UpdatedDatetime"

FROM "BUSDTA"."Entity_Range_Master"
WHERE "BUSDTA"."Entity_Range_Master"."last_modified">= {ml s.last_table_download}
 
