

SELECT "UsageMeasurementID"
,"UsageMeasurement"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "dbo"."tblUsageMeasurement"
WHERE "dbo"."tblUsageMeasurement"."last_modified">= {ml s.last_table_download}
