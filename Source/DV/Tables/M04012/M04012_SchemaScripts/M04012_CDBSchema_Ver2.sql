/* [BUSDTA].[M04012] - begins */

/* TableDDL - [BUSDTA].[M04012] - Start */
IF OBJECT_ID('[BUSDTA].[M04012]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M04012]
	(
	  [PMAN8] NUMERIC(8,0) NOT NULL
	, [PMALPH] NCHAR(40) NULL
	, [PMROUT] NCHAR(10) NOT NULL
	, [PMSRS] INT NULL
	, [PMCRBY] NCHAR(25) NULL
	, [PMCRDT] DATE NULL
	, [PMUPBY] NCHAR(25) NULL
	, [PMUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M04012_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M04012] PRIMARY KEY ([PMAN8] ASC)
	)

END
/* TableDDL - [BUSDTA].[M04012] - End */

/* SHADOW TABLE FOR [BUSDTA].[M04012] - Start */
IF OBJECT_ID('[BUSDTA].[M04012_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M04012_del]
	(
	  [PMAN8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PMAN8] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[M04012] - End */
/* TRIGGERS FOR M04012 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M04012_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M04012_ins
	ON BUSDTA.M04012 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M04012_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M04012_del.PMAN8= inserted.PMAN8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M04012_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M04012_upd
	ON BUSDTA.M04012 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M04012
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M04012.PMAN8= inserted.PMAN8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M04012_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M04012_dlt
	ON BUSDTA.M04012 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M04012_del (PMAN8, last_modified )
	SELECT deleted.PMAN8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M04012 - END */
