------- /*[BUSDTA].[F90CA086]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F90CA086"     (
	"CRCUAN8" numeric(8, 0) not null,
	"CRCRAN8" numeric(8, 0) not null,
	PRIMARY KEY ("CRCUAN8", "CRCRAN8")
)

------- /*[BUSDTA].[F90CA086]*/ - End