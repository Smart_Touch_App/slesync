 
/* Update the row in the consolidated database. */
UPDATE ."dbo"."tblGlobalSyncLog"
SET "GlobalSyncID" = {ml r."GlobalSyncID"}, "DeviceID" = {ml r."DeviceID"}, "CreatedOn" = {ml r."CreatedOn"}
WHERE "GlobalSyncLogID" = {ml r."GlobalSyncLogID"}
 
