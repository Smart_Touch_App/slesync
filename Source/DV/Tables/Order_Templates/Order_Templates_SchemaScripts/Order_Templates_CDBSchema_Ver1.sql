
/* [dbo].[Order_Templates] - begins */

/* TableDDL - [dbo].[Order_Templates] - Start */
IF OBJECT_ID('[dbo].[Order_Templates]') IS NULL
BEGIN
	CREATE TABLE [dbo].[Order_Templates]	(	  [TemplateType] NCHAR(10) NOT NULL	, [ShipToID] NUMERIC(8,0) NOT NULL	, [TemplateDetailID] NUMERIC(12,0) NOT NULL	, [DisplaySeq] NUMERIC(5,0) NULL	, [ItemID] NUMERIC(8,0) NULL	, [LongItem] NCHAR(25) NULL	, [TemplateQuantity] NUMERIC(8,0) NULL	, [TemplateUoM] NCHAR(2) NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ORDER_TEMPLATES_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_Order_Templates] PRIMARY KEY ([TemplateType] ASC, [ShipToID] ASC, [TemplateDetailID] ASC)	)END
/* TableDDL - [dbo].[Order_Templates] - End */

/* SHADOW TABLE FOR [dbo].[Order_Templates] - Start */
IF OBJECT_ID('[dbo].[Order_Templates_del]') IS NULL
BEGIN
	CREATE TABLE [dbo].[Order_Templates_del]	(	  [TemplateType] NCHAR(10)	, [ShipToID] NUMERIC(8,0)	, [TemplateDetailID] NUMERIC(12,0)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([TemplateType] ASC, [ShipToID] ASC, [TemplateDetailID] ASC)	)END
/* SHADOW TABLE FOR [dbo].[Order_Templates] - End */
/* TRIGGERS FOR Order_Templates - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.Order_Templates_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.Order_Templates_ins
	ON dbo.Order_Templates AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.Order_Templates_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.Order_Templates_del.ShipToID= inserted.ShipToID AND dbo.Order_Templates_del.TemplateDetailID= inserted.TemplateDetailID AND dbo.Order_Templates_del.TemplateType= inserted.TemplateType
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.Order_Templates_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.Order_Templates_upd
	ON dbo.Order_Templates AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.Order_Templates
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.Order_Templates.ShipToID= inserted.ShipToID AND dbo.Order_Templates.TemplateDetailID= inserted.TemplateDetailID AND dbo.Order_Templates.TemplateType= inserted.TemplateType');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.Order_Templates_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.Order_Templates_dlt
	ON dbo.Order_Templates AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.Order_Templates_del (ShipToID, TemplateDetailID, TemplateType, last_modified )
	SELECT deleted.ShipToID, deleted.TemplateDetailID, deleted.TemplateType, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Templates - END */
