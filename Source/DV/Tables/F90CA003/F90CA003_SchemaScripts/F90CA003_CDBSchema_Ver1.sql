
/* [BUSDTA].[F90CA003] - begins */

/* TableDDL - [BUSDTA].[F90CA003] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA003]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA003]	(	  [SMAN8] FLOAT NOT NULL	, [SMSLSM] FLOAT NOT NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F90CA003_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F90CA003] PRIMARY KEY ([SMAN8] ASC, [SMSLSM] ASC)	)
END
/* TableDDL - [BUSDTA].[F90CA003] - End */

/* SHADOW TABLE FOR [BUSDTA].[F90CA003] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA003_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA003_del]	(	  [SMAN8] FLOAT	, [SMSLSM] FLOAT	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([SMAN8] ASC, [SMSLSM] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[F90CA003] - End */
/* TRIGGERS FOR F90CA003 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F90CA003_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F90CA003_ins
	ON BUSDTA.F90CA003 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F90CA003_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F90CA003_del.SMAN8= inserted.SMAN8 AND BUSDTA.F90CA003_del.SMSLSM= inserted.SMSLSM
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F90CA003_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA003_upd
	ON BUSDTA.F90CA003 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F90CA003
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F90CA003.SMAN8= inserted.SMAN8 AND BUSDTA.F90CA003.SMSLSM= inserted.SMSLSM');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F90CA003_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA003_dlt
	ON BUSDTA.F90CA003 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F90CA003_del (SMAN8, SMSLSM, last_modified )
	SELECT deleted.SMAN8, deleted.SMSLSM, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F90CA003 - END */
