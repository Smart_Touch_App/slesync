------- /*[BUSDTA].[F90CA003]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F90CA003"     (
	"SMAN8" float not null,
	"SMSLSM" float not null,
	PRIMARY KEY ("SMAN8", "SMSLSM")
)

------- /*[BUSDTA].[F90CA003]*/ - End