------- /*[BUSDTA].[Device_Master]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."Device_Master"     (
	"Device_Id" varchar(30) not null,
	"Active" integer null,
	PRIMARY KEY ("Device_Id")
)

------- /*[BUSDTA].[Device_Master]*/ - End