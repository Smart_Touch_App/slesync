/* [BUSDTA].[Device_Master] - begins */

/* TableDDL - [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master]
	(
	  [Device_Id] VARCHAR(30) NOT NULL
	, [Active] INTEGER NULL
	
	, [manufacturer] NVARCHAR(50) NULL
	, [model] NVARCHAR(50) NULL
	, PRIMARY KEY ([Device_Id] ASC)
	)
END
/* TableDDL - [BUSDTA].[Device_Master] - End */
