/* [BUSDTA].[tblAlliedProspect] - begins */

/* TableDDL - [BUSDTA].[tblAlliedProspect] - Start */
IF OBJECT_ID('[BUSDTA].[tblAlliedProspect]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblAlliedProspect]
	(
	  [AlliedProspectID] INTEGER NOT NULL
	, [ProspectID] INTEGER NOT NULL
	, [CompetitorID] INTEGER NULL
	, [CategoryID] INTEGER NULL
	, [SubCategoryID] INTEGER NULL
	, [BrandID] INTEGER NULL
	, [UOMID] INTEGER NULL
	, [PackSize] NUMERIC(8,0) NULL
	, [CS_PK_LB] NUMERIC(8,0) NULL
	, [Price] decimal (15,4) NULL
	, [UsageMeasurementID] INTEGER NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([AlliedProspectID] ASC,[ProspectID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblAlliedProspect] - End */
