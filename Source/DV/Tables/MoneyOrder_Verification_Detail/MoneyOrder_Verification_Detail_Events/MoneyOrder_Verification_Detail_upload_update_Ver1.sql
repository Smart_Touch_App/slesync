 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."MoneyOrder_Verification_Detail"
SET "SettlementDetailId" = {ml r."SettlementDetailId"}, "MoneyOrderId" = {ml r."MoneyOrderId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "MoneyOrderVerificationDetailId" = {ml r."MoneyOrderVerificationDetailId"} AND "RouteId" = {ml r."RouteId"}
 
