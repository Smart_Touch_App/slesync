/*
/* [BUSDTA].[MoneyOrder_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrder_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrder_Verification_Detail]
	(
	  [MoneyOrderVerificationDetailId] NUMERIC NOT NULL
	, [SettlementDetailId] NUMERIC NOT NULL
	, [MoneyOrderId] NUMERIC NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([MoneyOrderVerificationDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[MoneyOrder_Verification_Detail] - End */
*/
