------- /*[BUSDTA].[F0116]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F0116"     (
	"ALAN8" numeric(8, 0) not null,
	"ALEFTB" numeric(18, 0) not null,
	"ALEFTF" nvarchar(1) null,
	"ALADD1" nvarchar(40) null,
	"ALADD2" nvarchar(40) null,
	"ALADD3" nvarchar(40) null,
	"ALADD4" nvarchar(40) null,
	"ALADDZ" nvarchar(12) null,
	"ALCTY1" nvarchar(25) null,
	"ALCOUN" nvarchar(25) null,
	"ALADDS" nvarchar(3) null,
	PRIMARY KEY ("ALAN8", "ALEFTB")
)

------- /*[BUSDTA].[F0116]*/ - End