
/* [BUSDTA].[F90CA042] - begins */

/* TableDDL - [BUSDTA].[F90CA042] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA042]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA042]
	(
	  [EMAN8] NUMERIC(8,0) NOT NULL
	, [EMPA8] NUMERIC(8,0) NOT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F90CA042_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F90CA042] PRIMARY KEY ([EMAN8] ASC, [EMPA8] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F90CA042] - End */

/* SHADOW TABLE FOR [BUSDTA].[F90CA042] - Start */
IF OBJECT_ID('[BUSDTA].[F90CA042_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F90CA042_del]
	(
	  [EMAN8] NUMERIC(8,0)
	, [EMPA8] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EMAN8] ASC, [EMPA8] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F90CA042] - End */
/* TRIGGERS FOR F90CA042 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F90CA042_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F90CA042_ins
	ON BUSDTA.F90CA042 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F90CA042_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F90CA042_del.EMAN8= inserted.EMAN8 AND BUSDTA.F90CA042_del.EMPA8= inserted.EMPA8
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F90CA042_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA042_upd
	ON BUSDTA.F90CA042 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F90CA042
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F90CA042.EMAN8= inserted.EMAN8 AND BUSDTA.F90CA042.EMPA8= inserted.EMPA8');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F90CA042_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F90CA042_dlt
	ON BUSDTA.F90CA042 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F90CA042_del (EMAN8, EMPA8, last_modified )
	SELECT deleted.EMAN8, deleted.EMPA8, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F90CA042 - END */

