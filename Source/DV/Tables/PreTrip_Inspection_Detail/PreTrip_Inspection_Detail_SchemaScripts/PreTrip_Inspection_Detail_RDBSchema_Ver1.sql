
/* [BUSDTA].[PreTrip_Inspection_Detail] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Detail]
	(
	  [PreTripInspectionHeaderId] NUMERIC NOT NULL
	, [PreTripInspectionDetailId] NUMERIC NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [QuestionId] NUMERIC NOT NULL
	, [ResponseID] NUMERIC NULL
	, [ResponseReason] NCHAR(50) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [PreTripInspectionDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - End */
