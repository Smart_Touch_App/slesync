
/* [BUSDTA].[Inventory_Adjustment] - begins */

/* TableDDL - [BUSDTA].[Inventory_Adjustment] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Adjustment]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Adjustment]
	(
	  [InventoryAdjustmentId] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NVARCHAR (25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionQty] NUMERIC(4,0) NULL
	, [TransactionQtyUM] NVARCHAR (2) NULL
	, [TransactionQtyPrimaryUM] NVARCHAR (2) NULL
	, [ReasonCode] NUMERIC(3,0) NOT NULL
	, [IsApproved] BIT NULL DEFAULT((0))
	, [IsApplied] BIT NULL DEFAULT((0))
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [AdjustmentStatus] NUMERIC(3,0) NULL
	, [Source] NUMERIC(3,0) NULL
	, PRIMARY KEY ([InventoryAdjustmentId] ASC, [ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Inventory_Adjustment] - End */

