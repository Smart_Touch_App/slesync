CREATE OR REPLACE PROCEDURE "BUSDTA"."calculateEnergySurcharge" (CustomerNum varchar(10), OrderTotal float)
BEGIN
	DECLARE  FreightHandlingCode varchar(10);
	DECLARE  SpecialHandlingCode varchar(10);
	DECLARE  DescriptionCode varchar(10);
	DECLARE  HandlingCodeLength INTEGER ;

	Declare str_drsy varchar(10);
	Declare str_drrt varchar(10);
	Declare UPto varchar(10);
	Declare surcharge varchar(10);

	Select ltrim(rtrim(AIFRTH)) into FreightHandlingCode from busdta.F03012 where aian8 = CustomerNum;
	IF FreightHandlingCode = '' or FreightHandlingCode is null THEN
		select 0 from dummy;
	END IF;

	select  ltrim(rtrim(DRDL02)), ltrim(rtrim(DRSPHD)) into DescriptionCode, SpecialHandlingCode from busdta.F0005 where DRSY = '42' and DRRT = 'FR' and DRKY like '%'+FreightHandlingCode+'';
	
	IF DescriptionCode = 'MB|STATIC' THEN
		select SpecialHandlingCode from dummy;
	ELSEIF DescriptionCode = 'MB|UDC' THEN
		set str_drsy = substring(SpecialHandlingCode, 1, charindex('|',SpecialHandlingCode)-1);
		set str_drrt = substring(SpecialHandlingCode,  charindex('|',SpecialHandlingCode)+1);
		select  DRDL01 as UPto ,  DRDL02  as surcharge  into #tbdummysurcharge  from busdta.F0005 where DRSY = ''+str_drsy+'' and DRRT = ''+str_drrt+'';
			PRINT 'Procedure called successfully' ;
			IF OrderTotal <= 100 THEN
				select surcharge from #tbdummysurcharge where Upto <=100;
			ELSEIF OrderTotal > 100 AND OrderTotal <= 250 THEN
				select surcharge from #tbdummysurcharge where Upto >100 and Upto <=250
			ELSE
				select surcharge from #tbdummysurcharge where Upto >250
			END IF;
	ELSEIF DescriptionCode like '' THEN
		PRINT 'Procedure called successfully' ;
		select 0 from dummy;
	ELSE
		PRINT 'Procedure called successfully' ;
		if OrderTotal <= 100 then
		  select 45 from dummy
		elseif OrderTotal > 100 and OrderTotal <= 250 then
		  select 30 from dummy
		else
		  select 0 from dummy
		end if
	END IF;
END
GO