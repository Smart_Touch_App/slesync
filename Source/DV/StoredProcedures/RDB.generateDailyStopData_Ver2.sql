
/*--------------------------------------------------------------------------------------------
*             This procedure pre-populates table M56M0004 tables, which is table for daily stops.
*			  This procedure executes the very first time when daily stop is hit and then generates 
*			  data for pre-determined future dates.
*--------------------------------------------------------------------------------------------*/

CREATE OR REPLACE PROCEDURE "BUSDTA"."generateDailyStopData" (In SelectedDate date,In BaseDate date, In FutureDays int default 500) 
BEGIN
    DECLARE rowCount INTEGER;
    DECLARE i INTEGER;
    
	select count(1) into rowCount from BUSDTA.M56M0004 where  RPSTDT = SelectedDate;
	--Check if future stops present
	--Else if Data present for few days
	--No data present at all
	SET i = 1;
    IF rowCount = 0 THEN
    	WHILE i <= FutureDays LOOP
     		insert into busdta.M56M0004(RPROUT, RPAN8, RPSTDT, RPSN, RPVTTP, RPSTTP,RPCRBY,RPCRDT,RPUPBY,RPUPDT)
    		(select r.RSROUT, r.RSAN8, cast(SelectedDate as date), r.RSSN, 'Cust', 'Planned','SYS',GETDATE(),'SYS',GETDATE()  
    		from BUSDTA.F03012 c join BUSDTA.M56M0003 r on c.aian8 = r.RSAN8 join BUSDTA.M56M0002 d on d.DCDDC = c.AISTOP 
			join busdta.Customer_Route_Map crm on  c.aian8 = crm.CustomerShipToNumber 
    		where   
    		R.RSWN = BUSDTA.GetWeekForDate(SelectedDate, BaseDate) and 
    		d.DCDN = BUSDTA.GetDayForDate(SelectedDate, BaseDate) and 
			crm.IsActive = 'Y' and crm.RelationshipType = 'AN85');
    		set SelectedDate = dateadd(day, 1, SelectedDate);
    		SET i = i + 1;
    	END LOOP;   
        /*Set all past stop dates as NOSALE*/
        update busdta.M56M0004
        set RPACTID = 'NOSALE', RPRCID = 7, RPCACT = 1, RPPACT = 0
        WHERE RPSTDT < cast(getdate() as date);
    END IF;
END

GO