/****** Object:  StoredProcedure [BUSDTA].[CheckForNumberReplenishment]    Script Date: 8/14/2015 12:49:26 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- Entity Property
---- Columns
--EntityName
--NumberRange
--NumberOfBuckets
--Padding
--Format
/* 
select * from BUSDTA.Entity_Bucket_Master
select *  from BUSDTA.Entity_Number_Status
select *  from BUSDTA.Entity_Range_Master
 */ --drop type EntityNumberUpdateType
--*******************************************************************************************************
--Create type EntityNumberUpdateType as table
--(
--    [ID] [int]  NOT NULL,
--	[RouteId] [varchar](10) NULL,
--	[Bucket] [varchar](30) NULL,
--	[EntityBucketMasterId] [varchar](30) NULL,
--	[RangeStart] [int] NULL,
--	[RangeEnd] [int] NULL,
--	[Current] [int] NULL,
--	[IsConsumed] [bit] NULL ,
--	[ConsumedOnDate] [datetime] NULL,
--	[CreatedDate] [datetime] NULL
--)
-- [ID],RouteId,Bucket,EntityBucketMasterId,RangeStart,RangeEnd,[Current],[IsConsumed],[ConsumedOnDate],[CreatedDate]
----*******************************************************************************************************
CREATE procedure [BUSDTA].[CheckForNumberReplenishment]
(
    @UpdateDetails busdta.EntityNumberUpdateType readonly
)
as
Begin
		--select [Current] from @UpdateDetails
--		print getdate()
--End
		--*******************************************************************************************************
		declare  @lastConsumed numeric(8,0);
		declare  @routeId varchar(50);
		
		declare  @count int= 0;
		declare  @countTemp int= 1;
		--select @lastConsumed
		select distinct top 1 @routeId = [RouteId] from @UpdateDetails;
		--select @routeId
		select *  from @UpdateDetails;
		select @count=COUNT([EntityBucketMasterId]) from @UpdateDetails where IsConsumed=1;
		if @count=0
			begin
				return ;
			end
		set @count=0;	
		
		-- Check for Entities Who's buckets are consumed
		--*******************************************************************************************************
		DECLARE @userData TABLE(
			[ID] [int] IDENTITY(1,1) NOT NULL,
			[RouteId] varchar(10) NOT NULL,
			[Bucket] varchar(30) NOT NULL,
			[EntityBucketMasterId] varchar(30) NOT NULL, -- This is the ID of the Entity, which is in the Entity_Bucket_Master
			[RangeEnd] int NOT NULL,
			[BucketSize] numeric(18,0),
			[Replenished] bit  default(0)
		);
		;WITH cte AS
		(
		   SELECT ens.*,ebp.BucketSize, ROW_NUMBER() OVER (PARTITION BY ens.[RouteId] order BY ens.Bucket DESC) AS rn
		   FROM @UpdateDetails ens left outer join busdta.Entity_Bucket_Master ebp
		   on ens.EntityBucketMasterId = ebp.EntityBucketMasterId
		   where IsConsumed=1 and ens.RouteId = @routeId
		   --having COUNT(ens.Entity) < (select BucketSize from dbo.EntityBucketProperties where Entity = ens.Entity)
		)
		insert into @userData SELECT RouteId, Bucket,EntityBucketMasterId ,RangeEnd,BucketSize,0 FROM cte  order by Bucket asc--WHERE rn = 1
		SELECT * FROM @userData
		--*******************************************************************************************************
		-- Get the number of bucket entries which are to be replenished
		SELECT @count = COUNT([Replenished])  FROM @userData where [Replenished]=0
		--*******************************************************************************************************
		--select @count 
		declare @entity varchar(50);
		declare @currentBucket varchar(50);
		declare @newRange numeric(18,0);
		declare @bucketExists int;
		-- Iterate through the entries for refilling bucket with new number range
		while @countTemp <= @count  
			begin 
				--1. Get the Entity which is to be updated
				select @entity=[EntityBucketMasterId] from @userData where ID=@countTemp --2
				select @currentBucket=[Bucket] from @userData where ID=@countTemp --bucket 2
				
				--2. Get the last allocated number from the EntityRangeMaster
				SELECT @lastConsumed = isnull(LastAlloted,RangeStart) FROM busdta.Entity_Range_Master em where em.EntityBucketMasterId =@entity--501
				
				--3. Calculate the new range to be alloted
				select @newRange=(@lastConsumed + BucketSize) from @userData where ID=@countTemp --501 +500
				
				select @bucketExists = COUNT(*) from busdta.Entity_Number_Status where 
				EntityBucketMasterId=@entity
				and isconsumed=0
				and RouteId=@routeId
				and [Bucket]=@currentBucket
				
				if @bucketExists =0 
					Begin
						--4. Insert a new entry for bucket with new range 
						insert into busdta.Entity_Number_Status([RouteId],[Bucket],[EntityBucketMasterId],[RangeStart],[RangeEnd],CreatedDatetime)
						select [RouteId],[Bucket],[EntityBucketMasterId],@lastConsumed+1 ,@newRange,getdate() from @userData where ID=@countTemp
						
						--5. Update EntityRangeMaster with the new range for the current entity
						update busdta.Entity_Range_Master set LastAlloted=@newRange where EntityBucketMasterId =@entity
					End
				--6. Update the replensihed flag in table var
				update @userData set [Replenished] = 1
				
				--7. Update the loop counter and continue
				set @countTemp = @countTemp +1
			end
		--*******************************************************************************************************
		if @count > 0 
		begin
			select * from busdta.Entity_Number_Status --where IsConsumed=0
		end
End

GO

