CREATE OR REPLACE PROCEDURE "BUSDTA"."SaveMasterInvoice"( @InvoiceNo varchar(10),@OrderQty integer ,@UM varchar(2) ,@ItemCode varchar(10) ,@ProductDesc varchar(30),
@UnitPrice float,@ExtendedPrice float,@OrderDate varchar(50), @SalesCat1 varchar(3)
 )
/* RESULT( column_name column_type, ... ) */
BEGIN
	insert into BUSDTA.InvoiceDetails(
InvoiceNo,
OrderQty,
UM,
ItemCode,
ProductDesc,
UnitPrice,
ExtendedPrice,
OrderDate,
SalesCat1
)values(
@InvoiceNo,
@OrderQty,
@UM,
@ItemCode,
@ProductDesc,
@UnitPrice,
@ExtendedPrice,
@OrderDate,
@SalesCat1
);
RETURN  1;
END
GO