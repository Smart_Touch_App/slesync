
CREATE PROCEDURE [dbo].[USP_PullF4106toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F4106'

	DECLARE @TEMP TABLE(Act varchar(6),BPLITM nchar(50))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F4106)
	BEGIN
	
		INSERT INTO BUSDTA.F4106
		(BPITM, BPLITM, BPMCU, BPLOCN, BPLOTN, BPAN8, BPIGID, BPCGID, BPLOTG, BPFRMP, BPCRCD, BPUOM, BPEFTJ, BPEXDJ, BPUPRC, BPUPMJ, BPTDAY)
		SELECT     BPITM, BPLITM, BPMCU, BPLOCN, BPLOTN, BPAN8, BPIGID, BPCGID, BPLOTG, BPFRMP, BPCRCD, BPUOM, 
		BPEFTJ, BPEXDJ, BPUPRC, BPUPMJ, BPTDAY FROM [JDEData_CRP_SQDR_Model].dbo.F4106
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4106')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4106'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4106',@synchronization_version,GETDATE())		
		
	END
	ELSE


	--Check the TABLE data whether which has been refreshed
	IF EXISTS(SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
	WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F4106')
	BEGIN
		TRUNCATE TABLE BUSDTA.F4106
		TRUNCATE TABLE BUSDTA.F4106_del
			
		INSERT INTO BUSDTA.F4106
		(BPITM, BPLITM, BPMCU, BPLOCN, BPLOTN, BPAN8, BPIGID, BPCGID, BPLOTG, BPFRMP, BPCRCD, BPUOM, BPEFTJ, BPEXDJ, BPUPRC, BPUPMJ, BPTDAY)
		SELECT     BPITM, BPLITM, BPMCU, BPLOCN, BPLOTN, BPAN8, BPIGID, BPCGID, BPLOTG, BPFRMP, BPCRCD, BPUOM, 
		BPEFTJ, BPEXDJ, BPUPRC, BPUPMJ, BPTDAY FROM [JDEData_CRP_SQDR_Model].dbo.F4106
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4106')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4106'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4106',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		
			--INSERT INTO [BUSDTA].[F4106_shadow](BPITM,BPLITM,BPAITM,BPMCU,BPLOCN,BPLOTN,BPAN8,BPIGID,BPCGID,BPLOTG,BPFRMP,BPCRCD,
			--BPUOM,BPEFTJ,BPEXDJ,BPUPRC,BPACRD,BPBSCD,BPLEDG,BPFVTR,	BPFRMN,BPURCD,BPURDT,BPURAT,BPURAB,BPURRF,BPAPRS,BPUSER,BPPID,
			--BPJOBN,BPUPMJ,BPTDAY,Operation)
			--SELECT F4106.BPITM,F4106.BPLITM,F4106.BPAITM,F4106.BPMCU,F4106.BPLOCN,F4106.BPLOTN,F4106.BPAN8,F4106.BPIGID,F4106.BPCGID,
			--F4106.BPLOTG,F4106.BPFRMP,F4106.BPCRCD,F4106.BPUOM,F4106.BPEFTJ,F4106.BPEXDJ,F4106.BPUPRC,F4106.BPACRD,F4106.BPBSCD,
			--F4106.BPLEDG,F4106.BPFVTR,F4106.BPFRMN,F4106.BPURCD,F4106.BPURDT,F4106.BPURAT,F4106.BPURAB,F4106.BPURRF,F4106.BPAPRS,
			--F4106.BPUSER,F4106.BPPID,F4106.BPJOBN,F4106.BPUPMJ,F4106.BPTDAY,CT.SYS_CHANGE_OPERATION
			--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4106, @Last_Sync_Version) CT
			--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4106 AS F4106 ON CT.BPITM = F4106.BPITM
			--AND CT.BPMCU = F4106.BPMCU AND CT.BPLOCN = F4106.BPLOCN
			--AND CT.BPLOTN = F4106.BPLOTN AND CT.BPAN8 = F4106.BPAN8
			--AND CT.BPIGID = F4106.BPIGID
			--AND CT.BPCGID = F4106.BPCGID
			--AND CT.BPLOTG = F4106.BPLOTG
			--AND CT.BPFRMP = F4106.BPFRMP
			--AND CT.BPCRCD = F4106.BPCRCD
			--AND CT.BPUOM = F4106.BPUOM
			--AND CT.BPEXDJ = F4106.BPEXDJ
			--AND CT.BPUPMJ = F4106.BPUPMJ
			--AND CT.BPTDAY = F4106.BPTDAY
				
		/***********Check the changes for replicatoin****************/
		--IF EXISTS (SELECT TOP 1 1 FROM BUSDTA.F4106_shadow)
		--BEGIN

		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES JDEData_CRP_SQDR_Model.dbo.F4106, @Last_Sync_Version)AS CT) 
		BEGIN

		WITH CTE AS
		(
		SELECT F4106.BPITM,F4106.BPLITM,F4106.BPAITM,F4106.BPMCU,F4106.BPLOCN,F4106.BPLOTN,F4106.BPAN8,F4106.BPIGID,F4106.BPCGID,
			F4106.BPLOTG,F4106.BPFRMP,F4106.BPCRCD,F4106.BPUOM,F4106.BPEFTJ,F4106.BPEXDJ,F4106.BPUPRC,F4106.BPACRD,F4106.BPBSCD,
			F4106.BPLEDG,F4106.BPFVTR,F4106.BPFRMN,F4106.BPURCD,F4106.BPURDT,F4106.BPURAT,F4106.BPURAB,F4106.BPURRF,F4106.BPAPRS,
			F4106.BPUSER,F4106.BPPID,F4106.BPJOBN,F4106.BPUPMJ,F4106.BPTDAY,CT.SYS_CHANGE_OPERATION AS Operation
			FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4106, @Last_Sync_Version) CT
			LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4106 AS F4106 ON CT.BPITM = F4106.BPITM
			AND CT.BPMCU = F4106.BPMCU AND CT.BPLOCN = F4106.BPLOCN
			AND CT.BPLOTN = F4106.BPLOTN AND CT.BPAN8 = F4106.BPAN8
			AND CT.BPIGID = F4106.BPIGID
			AND CT.BPCGID = F4106.BPCGID
			AND CT.BPLOTG = F4106.BPLOTG
			AND CT.BPFRMP = F4106.BPFRMP
			AND CT.BPCRCD = F4106.BPCRCD
			AND CT.BPUOM = F4106.BPUOM
			AND CT.BPEXDJ = F4106.BPEXDJ
			AND CT.BPUPMJ = F4106.BPUPMJ
			AND CT.BPTDAY = F4106.BPTDAY
		)

		MERGE BUSDTA.F4106 AS TARGET
		USING CTE AS SOURCE 
		ON (TARGET.BPITM = SOURCE.BPITM) AND (TARGET.BPMCU = SOURCE.BPMCU)
		AND (TARGET.BPLOCN = SOURCE.BPLOCN) AND (TARGET.BPLOTN = SOURCE.BPLOTN)
		AND (TARGET.BPAN8 = SOURCE.BPAN8) 
		AND (TARGET.BPIGID = SOURCE.BPIGID)
		AND (TARGET.BPCGID = SOURCE.BPCGID)
		AND (TARGET.BPLOTG = SOURCE.BPLOTG)
		AND (TARGET.BPFRMP = SOURCE.BPFRMP)
		AND (TARGET.BPCRCD = SOURCE.BPCRCD)
		AND (TARGET.BPUOM = SOURCE.BPUOM)
		AND (TARGET.BPEXDJ = SOURCE.BPEXDJ)
		AND (TARGET.BPUPMJ = SOURCE.BPUPMJ) AND (TARGET.BPTDAY = SOURCE.BPTDAY) 

		WHEN MATCHED AND SOURCE.Operation='U' THEN 
		UPDATE 
		SET 
		
			TARGET.BPLITM = SOURCE.BPLITM,
			TARGET.BPEFTJ = SOURCE.BPEFTJ,
			TARGET.BPUPRC = SOURCE.BPUPRC
	
		WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
		INSERT (BPITM, BPLITM, BPMCU, BPLOCN, BPLOTN, BPAN8, BPIGID, BPCGID, BPLOTG, BPFRMP, BPCRCD, BPUOM, 
		BPEFTJ, BPEXDJ, BPUPRC, BPUPMJ, BPTDAY)		
		VALUES (SOURCE.BPITM,SOURCE.BPLITM,SOURCE.BPMCU,SOURCE.BPLOCN,SOURCE.BPLOTN,SOURCE.BPAN8,SOURCE.BPIGID,SOURCE.BPCGID,SOURCE.BPLOTG,
		SOURCE.BPFRMP,SOURCE.BPCRCD,SOURCE.BPUOM,BPEFTJ,SOURCE.BPEXDJ,SOURCE.BPUPRC,SOURCE.BPUPMJ,SOURCE.BPTDAY)

		WHEN MATCHED AND SOURCE.Operation='D' THEN 
		DELETE

		OUTPUT $action,
		INSERTED.BPLITM AS SourceBPLITM INTO @Temp (Act,BPLITM);
		SELECT @@ROWCOUNT AS F4106;
			
			IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4106')
			
			UPDATE [dbo].[tblSyncDetails] 
			SET LastSyncVersion = @synchronization_version
			WHERE Objectname = 'DBO.F4106'
			ELSE
			INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F4106',@synchronization_version)
		
		END
		--TRUNCATE TABLE [busdta].[F4106_shadow]
	END

END









GO


