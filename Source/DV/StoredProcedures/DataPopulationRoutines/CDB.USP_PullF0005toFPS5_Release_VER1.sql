
CREATE PROCEDURE [dbo].[USP_PullF0005toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'dbo.F0005'

	DECLARE @TEMP TABLE(Act varchar(6),DRSY nchar(10))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F0005)
	BEGIN
	
		INSERT INTO BUSDTA.F0005
		(DRSY, DRRT, DRKY, DRDL01, DRDL02, DRSPHD, DRHRDC)
		SELECT DRSY, DRRT, DRKY, DRDL01, DRDL02, DRSPHD, DRHRDC
		FROM  [JDEData_CRP_SQDR_Model].dbo.F0005 
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'dbo.F0005')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'dbo.F0005'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('dbo.F0005',@synchronization_version,GETDATE())
		
		
	END
	ELSE


	--Check the table data whether which has been refreshed
	IF EXISTS (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'dbo.F0005')
	BEGIN
		TRUNCATE TABLE BUSDTA.F0005
		TRUNCATE TABLE BUSDTA.F0005_DEL
		
		INSERT INTO BUSDTA.F0005
		(DRSY, DRRT, DRKY, DRDL01, DRDL02, DRSPHD, DRHRDC)
		SELECT DRSY, DRRT, DRKY, DRDL01, DRDL02, DRSPHD, DRHRDC
		FROM  [JDEData_CRP_SQDR_Model].dbo.F0005
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'dbo.F0005')
		
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'dbo.F0005'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('dbo.F0005',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		--SELECT  @TSQL = '
		--INSERT INTO [BUSDTA].[F0005_shadow](DRSY,DRRT,DRKY,DRDL01,DRDL02,DRSPHD,DRHRDC,Operation)
		--SELECT CT.DRSY,CT.DRRT,CT.DRKY,F0005.DRDL01,F0005.DRDL02,F0005.DRSPHD,F0005.DRHRDC,CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [[JDEData_CRP_SQDR_Model]].BUSDTA.F0005,@Last_Sync_Version) CT
		--LEFT OUTER JOIN [[JDEData_CRP_SQDR_Model]].BUSDTA.F0005 AS F0005 ON CT.DRSY = F0005.DRSY AND CT.DRRT = F0005.DRRT AND CT.DRKY = F0005.DRKY
		--'	EXEC (@TSQL)
				
		/***********Check the changes for replicatoin****************/
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0005, @Last_Sync_Version)AS CT)
		BEGIN
		WITH CTE AS
		(
		SELECT CT.DRSY,CT.DRRT,CT.DRKY,F0005.DRDL01,F0005.DRDL02,F0005.DRSPHD,F0005.DRHRDC,CT.SYS_CHANGE_OPERATION AS Operation
		FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0005,@Last_Sync_Version) CT
		LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F0005 AS F0005 ON CT.DRSY = F0005.DRSY AND CT.DRRT = F0005.DRRT AND CT.DRKY = F0005.DRKY
		)

		MERGE BUSDTA.F0005 AS TARGET
		USING CTE AS SOURCE 
		ON (TARGET.DRSY = SOURCE.DRSY) AND (TARGET.DRRT = SOURCE.DRRT) AND (TARGET.DRKY = SOURCE.DRKY) 

		WHEN MATCHED AND SOURCE.Operation='U' THEN 
		UPDATE 
		SET 
			TARGET.DRDL01 = SOURCE.DRDL01,
			TARGET.DRDL02 = SOURCE.DRDL02,
			TARGET.DRSPHD = SOURCE.DRSPHD,
			TARGET.DRHRDC = SOURCE.DRHRDC
				
		WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
		INSERT (DRSY,DRRT,DRKY,DRDL01,DRDL02,DRSPHD,DRHRDC)

		VALUES (SOURCE.DRSY,SOURCE.DRRT,SOURCE.DRKY,SOURCE.DRDL01,SOURCE.DRDL02,SOURCE.DRSPHD,SOURCE.DRHRDC)

		WHEN MATCHED AND SOURCE.Operation='D' THEN 
		DELETE

		OUTPUT $action,
		INSERTED.DRSY AS SourceDRSY INTO @TEMP (Act,DRSY);
		SELECT @@ROWCOUNT AS F0005;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'dbo.F0005')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		Where Objectname = 'dbo.F0005'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F0005',@synchronization_version)
		
		END
	--TRUNCATE TABLE [busdta].[F0005_shadow]
	
END

END






GO


