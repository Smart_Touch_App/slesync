
CREATE PROCEDURE [dbo].[USP_PullF4075toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F4075'

	DECLARE @TEMP TABLE(Act varchar(6),VBVBT numeric(18,0))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F4075)
	BEGIN
	
		INSERT INTO BUSDTA.F4075
		(VBVBT, VBCRCD, VBUOM, VBUPRC, VBEFTJ, VBEXDJ, VBAPRS, VBUPMJ, VBTDAY)
		SELECT VBVBT, VBCRCD, VBUOM, VBUPRC, VBEFTJ, VBEXDJ, VBAPRS, VBUPMJ, VBTDAY
		FROM [JDEData_CRP_SQDR_Model].dbo.F4075
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4075')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4075'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4075',@synchronization_version,GETDATE())		
		
	END
	ELSE

	--Check the table data whether which has been refreshed
	IF EXISTS  (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F4075')
	BEGIN
		TRUNCATE TABLE BUSDTA.F4075
		TRUNCATE TABLE BUSDTA.F4075_del
				
		INSERT INTO BUSDTA.F4075
		(VBVBT, VBCRCD, VBUOM, VBUPRC, VBEFTJ, VBEXDJ, VBAPRS, VBUPMJ, VBTDAY)
		SELECT VBVBT, VBCRCD, VBUOM, VBUPRC, VBEFTJ, VBEXDJ, VBAPRS, VBUPMJ, VBTDAY
		FROM [JDEData_CRP_SQDR_Model].dbo.F4075

		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4075')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4075'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4075',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		
		--INSERT INTO [BUSDTA].[F4075_shadow](VBVBT,VBCRCD,VBUOM,VBUPRC,VBEFTJ,VBEXDJ,VBAPRS,VBUPMJ,VBTDAY,[Operation])
		--SELECT CT.VBVBT,VBCRCD,VBUOM,VBUPRC,CT.VBEFTJ,VBEXDJ,VBAPRS,CT.VBUPMJ,CT.VBTDAY,CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4075, @Last_Sync_Version) CT
		--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4075 AS F00
		--ON CT.VBVBT = F00.VBVBT AND CT.VBEFTJ = F00.VBEFTJ 
		--AND CT.VBUPMJ = F00.VBUPMJ AND CT.VBTDAY = F00.VBTDAY
		
		/***********Check the changes for replicatoin****************/
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES JDEData_CRP_SQDR_Model.dbo.F4075, @Last_Sync_Version)AS CT)
		BEGIN

		WITH CTE AS(
			SELECT CT.VBVBT,VBCRCD,VBUOM,VBUPRC,CT.VBEFTJ,VBEXDJ,VBAPRS,CT.VBUPMJ,CT.VBTDAY,CT.SYS_CHANGE_OPERATION AS Operation
			FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4075, @Last_Sync_Version) CT
			LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4075 AS F00
			ON CT.VBVBT = F00.VBVBT AND CT.VBEFTJ = F00.VBEFTJ 
			AND CT.VBUPMJ = F00.VBUPMJ AND CT.VBTDAY = F00.VBTDAY)

			MERGE BUSDTA.F4075 AS TARGET
			USING CTE AS SOURCE 
			ON (TARGET.VBVBT = SOURCE.VBVBT ) AND (TARGET.VBEFTJ = SOURCE.VBEFTJ)  
			AND (TARGET.VBUPMJ = SOURCE.VBUPMJ) AND (TARGET.VBTDAY = SOURCE.VBTDAY)

			WHEN MATCHED AND SOURCE.Operation='U' THEN 
			UPDATE 
			SET TARGET.[VBCRCD] = SOURCE.VBCRCD
					,TARGET.[VBUOM] = SOURCE.VBUOM
					,TARGET.[VBUPRC] = SOURCE.VBUPRC
					,TARGET.[VBEXDJ] = SOURCE.VBEXDJ
					,TARGET.[VBAPRS] = SOURCE.VBAPRS
		
			WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
			INSERT ([VBVBT],[VBCRCD],[VBUOM],[VBUPRC],[VBEFTJ],[VBEXDJ],[VBAPRS],[VBUPMJ],[VBTDAY]) 
			VALUES (SOURCE.VBVBT,SOURCE.VBCRCD,SOURCE.VBUOM,SOURCE.VBUPRC,SOURCE.VBEFTJ,SOURCE.VBEXDJ,SOURCE.VBAPRS,SOURCE.VBUPMJ,SOURCE.VBTDAY)

			WHEN MATCHED AND SOURCE.Operation='D' THEN 
			DELETE

			OUTPUT $action,
			INSERTED.VBVBT AS SourceVBVBT INTO @Temp (Act,VBVBT);
			SELECT @@ROWCOUNT AS F4075;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4075')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4075'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F4075',@synchronization_version)
		
		END
		--TRUNCATE TABLE [BUSDTA].[F4075_shadow]	
END

END









GO


