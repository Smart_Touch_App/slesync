DROP SYNONYM [dbo].[ReferenceSyncVersion]
GO
CREATE SYNONYM [dbo].[ReferenceSyncVersion] FOR [JDEData_PY].[dbo].[vw_CTCurrentVersion]
GO
