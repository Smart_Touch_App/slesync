
/*------------------------------------------------------------------------------
*            Function will return the default phone number from the list of phones 
*-----------------------------------------------------------------------------*/
CREATE OR REPLACE FUNCTION "BUSDTA"."GetDefaultPhone"( in "CustomerId" numeric(8) ) 
RETURNS nchar(25)
BEGIN
  DECLARE "PhoneNumber" nchar(25);
  DECLARE "ABCount" integer;
  DECLARE "PH1" bit;
  DECLARE "PH2" bit;
  DECLARE "PH3" bit;
  DECLARE "PH4" bit;
  DECLARE "whoswho" bit;
  DECLARE "LineNum" bit;
  DECLARE cur_CustomerPhone CURSOR FOR
      SELECT CDDFLTPH1, CDDFLTPH2, CDDFLTPH3, CDDFLTPH4, CDIDLN, CDRCK7
      FROM BUSDTA.M0111 
      WHERE CDAN8 = CustomerId AND CDIDLN=0;
  DECLARE cur_ProspectPhone CURSOR FOR
      SELECT IsDefaultPhone1, IsDefaultPhone2, IsDefaultPhone3, IsDefaultPhone4  
      FROM BUSDTA.Prospect_Contact
      WHERE ProspectId = CustomerId;
  set PhoneNumber = 'NA';
  set "ABCount" = 0;

  select "count"("ProspectId") into "ABCount" from "busdta"."Prospect_Master" where "ProspectId" = "CustomerId";
  IF "ABCount" = 0 THEN
    OPEN cur_CustomerPhone;    
        ilp: LOOP
            FETCH next cur_CustomerPhone into PH1, PH2, PH3, PH4, whoswho, LineNum;
            IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                IF "PH1" = 1 THEN
                  select STRING(CASE ISNULL(CDAR1,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR1),') ') END,RTRIM(CDPH1)  , CASE ISNULL(CDEXTN1,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN1)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSEIF "PH2" = 1 THEN
                  select STRING(CASE ISNULL(CDAR2,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR2),') ') END,RTRIM(CDPH2)  , CASE ISNULL(CDEXTN2,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN2)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSEIF "PH3" = 1 THEN
                  select STRING(CASE ISNULL(CDAR3,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR3),') ') END,RTRIM(CDPH3)  , CASE ISNULL(CDEXTN3,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN3)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSEIF "PH4" = 1 THEN
                  select STRING(CASE ISNULL(CDAR4,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(CDAR4),') ') END,RTRIM(CDPH4)  , CASE ISNULL(CDEXTN4,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(CDEXTN4)) END)  into "PhoneNumber" from "BUSDTA"."M0111" where "CDAN8" = "CustomerId" and "CDIDLN" = whoswho and "CDRCK7" = LineNum;
                  LEAVE ilp;
                ELSE
                  select 'NA' into "PhoneNumber" from "dummy";
            END IF;
        END LOOP;     
    CLOSE cur_CustomerPhone;
  ELSE 
       OPEN cur_ProspectPhone;    
        ilp: LOOP
            FETCH next cur_ProspectPhone into PH1, PH2, PH3, PH4;--, whoswho;
            IF SQLCODE <> 0 THEN LEAVE ilp END IF;
                IF "PH1" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode1,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode1),') ') END,RTRIM(Phone1)  , CASE ISNULL(EXTN1,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN1)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId"; --and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSEIF "PH2" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode2,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode2),') ') END,RTRIM(Phone2)  , CASE ISNULL(EXTN2,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN2)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId";-- and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSEIF "PH3" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode3,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode3),') ') END,RTRIM(Phone3)  , CASE ISNULL(EXTN3,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN3)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId";-- and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSEIF "PH4" = 1 THEN
                  select STRING(CASE ISNULL(AreaCode4,'') WHEN '' THEN '' ELSE STRING('(' ,RTRIM(AreaCode4),') ') END,RTRIM(Phone4)  , CASE ISNULL(EXTN4,'') WHEN '' THEN '' ELSE STRING(' ' ,RTRIM(EXTN4)) END)  into "PhoneNumber" from "BUSDTA"."Prospect_Contact" where "ProspectId" = "CustomerId";-- and "PCIDLN" = whoswho;
                  LEAVE ilp;
                ELSE
                  select 'NA' into "PhoneNumber" from "dummy";
            END IF;
        END LOOP;     
    CLOSE cur_ProspectPhone;
  END IF;
  RETURN "PhoneNumber";
END;

GO