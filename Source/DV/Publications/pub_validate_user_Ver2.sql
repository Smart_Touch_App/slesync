DROP PUBLICATION IF EXISTS pub_validate_user;
GO
/** Create publication 'pub_validate_user'. **/
CREATE PUBLICATION "pub_validate_user" (
    
    TABLE "BUSDTA"."Route_Device_Map" ,
    TABLE "BUSDTA"."Route_User_Map" ,
    TABLE "BUSDTA"."User_Role_Map" 
)
GO
