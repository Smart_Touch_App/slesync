
/*
=============================================
Author:			Sakaravarthi J
Create date:	28-Sep-2015
Description:	Logs Webservices' request and response
	Logs WebServices source from all sources
		1. Stats Services
		2. Onboarding Services
Note: this Stored Procedures gets/logs by calling from CDB 
	(where the services configured to the database as the web service contact only CDB)
============================================= 
*/
CREATE PROCEDURE [dbo].[SaveWebServiceLog]
(
	@DeviceID VARCHAR(100)='',
	@Request VARCHAR(MAX),
	@Response VARCHAR(MAX),
	@Environment VARCHAR(128)='',
	@DBName VARCHAR(128)='',
	@ServiceName VARCHAR(100)='',
	@ServiceEndPoint VARCHAR(1000)='',
	@ServiceContract VARCHAR(1000)=''
)
AS
BEGIN
SET NOCOUNT ON
	DECLARE @ServiceLogID INT = 0;
	SELECT @ServiceLogID = ISNULL(MAX(ServiceLogID), 0)+1 FROM tblWebServiceLog;

	INSERT INTO [dbo].[tblWebServiceLog] ([ServiceLogID], [DeviceID], [Request], [Response], [Environment], [DBName], [ServiceName], [ServiceEndPoint], [ServiceContract])
	VALUES(@ServiceLogID, @DeviceID, CAST(@Request AS XML), CAST(@Response AS XML), @Environment, @DBName, @ServiceName, @ServiceEndPoint, @ServiceContract)
SET NOCOUNT OFF
END

GO