
CREATE PROCEDURE [dbo].[uspGetAppVersion] 
(
	@AppName VARCHAR(100)=NULL
)
AS
BEGIN
	SELECT AppName, B.AppType, VersionNumber, ReleasedDate, AppLocation, IsMandatory FROM(
		SELECT AppName, AppTypeID, VersionNumber, ReleasedDate, AppLocation, IsMandatory, 
		RANK() OVER (PARTITION BY AppName ORDER BY ReleasedDate DESC,AppReleaseID DESC) AS LatestRank
		FROM tblAppReleaseMaster
		) AS Apps INNER JOIN tblAppTypes AS B ON Apps.AppTypeID=B.AppTypeID
	WHERE (ISNULL(@AppName,'')='' AND LatestRank=1) OR (ISNULL(@AppName,'')!='' AND LatestRank=1 AND AppName=@AppName)
	ORDER BY B.SortOrder
END
GO