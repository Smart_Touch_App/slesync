
CREATE PROCEDURE [dbo].[Register_DeviceEnvironment] 
(
	@DeviceId VARCHAR(30),
	@RouteID VARCHAR(30),
	@EnvironmentName VARCHAR(30),
	@AppUserName VARCHAR(30)
)
AS
BEGIN
DECLARE @UserEnvironmentMapId INT=0, @EnvironmentMasterId INT, @AppUserId INT;
SELECT @EnvironmentMasterId = EnvironmentMasterId FROM BUSDTA.Environment_Master WHERE EnvName=@EnvironmentName
SELECT @AppUserId = App_User_ID FROM BUSDTA.User_Master WHERE App_User=@AppUserName
SELECT @UserEnvironmentMapId = ISNULL(MAX(UserEnvironmentMapId), 0)+1 FROM BUSDTA.User_Environment_Map

	IF EXISTS(SELECT 1 FROM BUSDTA.User_Environment_Map WHERE EnvironmentMasterId=@EnvironmentMasterId)
	BEGIN
		UPDATE BUSDTA.RemoteDBID_Master SET ActiveYN='Y' WHERE DeviceId=@DeviceId AND RouteID=@RouteID AND EnvironmentMasterId=@EnvironmentMasterId
	END

	IF EXISTS(SELECT 1 FROM BUSDTA.User_Environment_Map WHERE EnvironmentMasterId=@EnvironmentMasterId)
	BEGIN
		DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceId AND RouteID=@RouteID AND EnvironmentMasterId=@EnvironmentMasterId
	END
END

GO