CREATE PROCEDURE [dbo].[ValidateDevice]
(
	@DeviceID nvarchar(50) 
)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT DM.Device_ID AS DeviceID, ISNULL(RM.ActiveYN,'N') AS IsRegistered, DM.Active AS IsDeviceActive FROM BUSDTA.Device_Master AS DM
	LEFT JOIN BUSDTA.RemoteDBID_Master AS RM ON RM.DeviceID=DM.Device_ID AND RM.ActiveYN='Y'
	WHERE DM.Device_ID=@DeviceID 
	SET NOCOUNT OFF;
END

GO