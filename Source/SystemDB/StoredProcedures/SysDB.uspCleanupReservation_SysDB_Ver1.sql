CREATE PROCEDURE [dbo].[uspCleanupReservation_SysDB]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins*-1) FROM tblReserveConfig 

	--Remove Entry from Reservation Table Reserve_DeviceEnvironmentRoute
	DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE ReservedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE())
	--DELETE FROM BUSDTA.RemoteDBID_Master WHERE CreatedDateTime<DATEADD(MINUTE, @ExpMins, GETDATE()) AND ActiveYN='N'
END
GO