-- =============================================
-- Author:		Sakaravarthi J
-- Create date: 26-Dec-2015
-- Description:	cleanup entries from the table entries to decouple route device mappings
-- =============================================
CREATE PROCEDURE [dbo].[uspDeregisterRouteDevice]
(
	@DeviceID VARCHAR(50),
	@RouteName VARCHAR(50)
)
AS
BEGIN
	DECLARE @DBName VARCHAR(100), @sql VARCHAR(1000)
	SELECT @DBName = LTRIM(RTRIM(em.CDBAddress)) FROM BUSDTA.Environment_Master AS em 
	INNER JOIN BUSDTA.RemoteDBID_Master AS rm ON em.EnvironmentMasterId=rm.EnvironmentMasterID
	WHERE  rm.DeviceID=@DeviceID AND rm.RouteID=@RouteName

	DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute WHERE DeviceId=@DeviceID
	DELETE FROM BUSDTA.RemoteDBID_Master WHERE DeviceId=@DeviceID
	--DELETE FROM MobileDataModel_FPS5_Release.BUSDTA.Route_Device_Map WHERE Device_Id=@DeviceID
	--UPDATE MobileDataModel_FPS5_Release.BUSDTA.Route_Master SET RouteStatusID=1 WHERE RouteName=@RouteName
	IF(LTRIM(RTRIM(@DBName))!='')
	BEGIN 
		SET @sql= CONCAT('DELETE FROM ',@DBName,'.BUSDTA.Route_Device_Map WHERE Device_Id=''',@DeviceID,'''')
		--PRINT @sql
		EXEC (@sql)
		
		SET @sql= CONCAT('UPDATE ',@DBName,'.BUSDTA.Route_Master SET RouteStatusID=1 WHERE RouteName=''',@RouteName,'''')
		--PRINT @sql
		EXEC (@sql)
	END
END

GO