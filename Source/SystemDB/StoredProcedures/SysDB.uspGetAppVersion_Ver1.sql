-- Stored Procedure

-- =============================================
-- Author:		Sakaravarthi J
-- Create date: 19-Oct-2015
-- Description:	get applicaiton's latest release version and date
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAppVersion] 
(
	@AppName VARCHAR(100)=NULL
)
AS
BEGIN
	SELECT AppName, B.AppType, VersionNumber, ReleasedDate, AppLocation FROM(
		SELECT AppName, AppTypeID, VersionNumber, ReleasedDate, AppLocation, RANK() OVER (PARTITION BY AppName ORDER BY ReleasedDate DESC) AS LatestRank
		FROM tblAppReleaseMaster
		) AS Apps INNER JOIN tblAppTypes AS B ON Apps.AppTypeID=B.AppTypeID
	WHERE (@AppName IS NULL AND LatestRank=1) OR (@AppName IS NOT NULL AND LatestRank=1 AND AppName=@AppName)
	ORDER BY B.SortOrder
END

GO