CREATE PROCEDURE [dbo].[usp_Event_Scripts]
(
	@script_version VARCHAR(128),
	@table_schema VARCHAR(128),
	@table_name VARCHAR(128),
	@event_name VARCHAR(128)
)
AS
BEGIN
	SELECT --ver.name AS script_version, tbl.[schema] AS table_schema, tbl.name AS table_name, scr.[event] AS event_name, 
		scr.script AS script
	FROM drh_script_version AS ver
	INNER JOIN drh_script AS scr ON scr.version_id=ver.version_id
	INNER JOIN drh_table AS tbl ON tbl.table_id=scr.table_id

	WHERE ver.name=@script_version AND tbl.[schema]=@table_schema AND tbl.name=@table_name AND scr.[event]=@event_name
END
GO
