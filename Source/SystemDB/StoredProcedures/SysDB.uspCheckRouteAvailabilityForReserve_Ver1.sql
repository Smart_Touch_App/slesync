
CREATE PROCEDURE [dbo].[uspCheckRouteAvailabilityForReserve]
AS
BEGIN
	DECLARE @ExpMins INT;
	/*get reservation expire minutes*/
	SELECT @ExpMins = (ExpireMins-1) FROM tblReserveConfig 

	SELECT rdbM.DeviceID, um.App_User, EnvName, rdbM.RouteID, rdbM.ActiveYN, ReservedDateTime
	FROM BUSDTA.RemoteDBID_Master AS rdbM
	INNER JOIN BUSDTA.Environment_Master AS em ON rdbM.EnvironmentMasterId=em.EnvironmentMasterId 
	INNER JOIN BUSDTA.User_Master AS um ON rdbM.AppUserId=um.App_user_id
	LEFT JOIN BUSDTA.Reserve_DeviceEnvironmentRoute AS der
		ON rdbM.EnvironmentMasterId=der.EnvironmentMasterId AND rdbM.RouteID=der.RouteID AND rdbM.DeviceID=der.DeviceID AND rdbM.AppUserID=der.AppUserID AND ReservedDateTime<=DATEADD(MINUTE, @ExpMins, GETDATE())
END
GO