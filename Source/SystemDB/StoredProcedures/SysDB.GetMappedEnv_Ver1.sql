CREATE PROCEDURE [dbo].[GetMappedEnv]
(
	@AppUser nvarchar(50) 
)
AS
BEGIN
SET NOCOUNT ON;
	SELECT UM.App_User_ID, UM.App_User, EM.EnvName, EM.EnvDescription, EM.CDBConKey, EM.EnvironmentMasterID
		FROM BUSDTA.user_master UM
		INNER JOIN BUSDTA.User_Environment_Map UEMAP ON UM.App_user_id = UEMAP.AppUserId 
		INNER JOIN BUSDTA.Environment_Master EM ON EM.EnvironmentMasterId = UEMAP.EnvironmentMasterId 
	WHERE UM.DomainUser = @AppUser
SET NOCOUNT OFF;
END
GO