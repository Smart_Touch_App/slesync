
-- =============================================
-- Author:		Sakaravarthi J
-- Create date: 19-Oct-2015
-- Description:	get applicaiton's latest release version and date
-- =============================================
CREATE PROCEDURE [dbo].[uspGetAppVersion]
(
	@EnvironmentName VARCHAR(100),
	@AppName VARCHAR(100)=NULL
)
AS
BEGIN
	SELECT EnvName, AppName, B.AppType, VersionNumber, ReleasedDate, AppLocation, IsMandatory FROM(
			SELECT em.EnvName, AppName, AppTypeID, VersionNumber, ReleasedDate, AppLocation, IsMandatory, 
			RANK() OVER (PARTITION BY em.EnvName, AppName ORDER BY ReleasedDate DESC,AppReleaseID DESC) AS LatestRank
			FROM tblAppReleaseMaster AS arm INNER JOIN BUSDTA.Environment_Master AS em ON em.EnvironmentMasterId=arm.EnvironmentMasterId
		) AS Apps INNER JOIN tblAppTypes AS B ON Apps.AppTypeID=B.AppTypeID
	WHERE ((ISNULL(@AppName,'')='' AND LatestRank=1) OR (ISNULL(@AppName,'')!='' AND LatestRank=1 AND AppName=@AppName))
	AND EnvName=@EnvironmentName
	ORDER BY B.SortOrder
END

GO