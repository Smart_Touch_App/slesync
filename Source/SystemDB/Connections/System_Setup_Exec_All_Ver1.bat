
@ECHO OFF
setlocal

CALL System_Connection_Vars.bat

echo Executing Consolidated Setup...

@echo off
 for %%f in (*.sql) do (
	IF EXIST "%cd%\%%~nf.sql" (
		echo Executing %%~nf.sql
		sqlcmd -S %server% -U %uname% -P %pass% -d %SysDB% -i "%cd%\%%~nf.sql"
		echo                - Executed Successfully.
	) ELSE (
        	echo WARNING: script " %%~nf.sql " file does NOT exist.
	)
    )

echo All consolidated scripts are executed successfully.
pause
