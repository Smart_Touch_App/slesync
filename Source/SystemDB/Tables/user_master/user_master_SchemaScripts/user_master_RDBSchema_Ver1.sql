/* [BUSDTA].[user_master] - begins */

/* TableDDL - [BUSDTA].[user_master] - Start */
IF OBJECT_ID('[BUSDTA].[user_master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[user_master]
	(
	  [App_user_id] INTEGER NOT NULL DEFAULT AUTOINCREMENT
	, [App_User] VARCHAR(30) NULL
	, [Name] VARCHAR(30) NULL
	, [DomainUser] VARCHAR(20) NULL
	, [AppPassword] VARCHAR(20) NULL
	, [active] BIT NULL DEFAULT((1))
	, [last_modified] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([App_user_id] ASC)
	)
END
/* TableDDL - [BUSDTA].[user_master] - End */
