/* [BUSDTA].[Environment_Master] - begins */

/* TableDDL - [BUSDTA].[Environment_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Environment_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Environment_Master]
	(
	  [EnvironmentMasterId] INTEGER NOT NULL DEFAULT AUTOINCREMENT
	, [EnvName] VARCHAR(30) NULL
	, [EnvDescription] VARCHAR(30) NULL
	, [IsActive] VARCHAR(1) NULL
	, [ReleaseLevel] VARCHAR(30) NULL
	, [MLServerAddress] NVARCHAR (50) NULL
	, [JDEWSAddress] NVARCHAR (50) NULL
	, [REFDBAddress] NVARCHAR (50) NULL
	, [ENVDBAddress] NVARCHAR (50) NULL
	, [CDBAddress] NVARCHAR (50) NULL
	, [IISAddress] NVARCHAR (50) NULL
	, [AppInstallerLocation] NVARCHAR (50) NULL
	, [RDBScriptLocation] NVARCHAR (50) NULL
	, [TaxLibLocation] NVARCHAR (50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL
	, PRIMARY KEY ([EnvironmentMasterId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Environment_Master] - End */
