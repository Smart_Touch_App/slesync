/* [BUSDTA].[Environment_Master] - begins */

/* TableDDL - [BUSDTA].[Environment_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Environment_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Environment_Master]
	(
	  [EnvironmentMasterId] INT NOT NULL IDENTITY(1,1)
	, [EnvName] VARCHAR(100) NULL
	, [EnvDescription] VARCHAR(100) NULL
	, [IsActive] VARCHAR(1) NULL
	, [ReleaseLevel] VARCHAR(30) NULL
	, [MLServerAddress] NCHAR(50) NULL
	, [JDEWSAddress] NCHAR(50) NULL
	, [REFDBAddress] NCHAR(50) NULL
	, [ENVDBAddress] NCHAR(50) NULL
	, [CDBAddress] NCHAR(50) NULL
	, [CDBConKey] VARCHAR(128) NULL
	, [IISAddress] NCHAR(50) NULL
	, [AppInstallerLocation] VARCHAR(4000) NULL
	, [RDBScriptLocation] VARCHAR(4000) NULL
	, [TaxLibLocation] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, CONSTRAINT [PK_Environment_Master] PRIMARY KEY ([EnvironmentMasterId] ASC)
	)

END
/* TableDDL - [BUSDTA].[Environment_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Environment_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Environment_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Environment_Master_del]
	(
	  [EnvironmentMasterId] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EnvironmentMasterId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Environment_Master] - End */
/* TRIGGERS FOR Environment_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Environment_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Environment_Master_ins
	ON BUSDTA.Environment_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Environment_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Environment_Master_del.EnvironmentMasterId= inserted.EnvironmentMasterId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Environment_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Environment_Master_upd
	ON BUSDTA.Environment_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Environment_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Environment_Master.EnvironmentMasterId= inserted.EnvironmentMasterId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Environment_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Environment_Master_dlt
	ON BUSDTA.Environment_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Environment_Master_del (EnvironmentMasterId, last_modified )
	SELECT deleted.EnvironmentMasterId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Environment_Master - END */
