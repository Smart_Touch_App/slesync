/* [BUSDTA].[Environment_Master] - begins */

/* TableDDL - [BUSDTA].[Environment_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Environment_Master]') IS NULL
BEGIN
CREATE TABLE "BUSDTA"."Environment_Master" (
    "EnvironmentMasterId"            integer NOT NULL
   ,"EnvName"                        varchar(100) NULL
   ,"EnvDescription"                 varchar(100) NULL
   ,"IsActive"                       varchar(1) NULL
   ,"ReleaseLevel"                   varchar(30) NULL
   ,"MLServerAddress"                nvarchar(50) NULL
   ,"JDEWSAddress"                   nvarchar(50) NULL
   ,"REFDBAddress"                   nvarchar(50) NULL
   ,"ENVDBAddress"                   nvarchar(50) NULL
   ,"CDBAddress"                     nvarchar(50) NULL
   ,"CDBConKey"                      nvarchar(128) NULL
   ,"IISAddress"                     nvarchar(50) NULL
   ,"AppInstallerLocation"           long varchar NULL
   ,"RDBScriptLocation"              long varchar NULL
   ,"TaxLibLocation"                 nvarchar(50) NULL
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("EnvironmentMasterId" ASC) 
)
END
/* TableDDL - [BUSDTA].[Environment_Master] - End */
