
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Environment_Master"
SET "EnvName" = {ml r."EnvName"}, "EnvDescription" = {ml r."EnvDescription"}, "IsActive" = {ml r."IsActive"}, "ReleaseLevel" = {ml r."ReleaseLevel"}, "MLServerAddress" = {ml r."MLServerAddress"}, "JDEWSAddress" = {ml r."JDEWSAddress"}, "REFDBAddress" = {ml r."REFDBAddress"}, "ENVDBAddress" = {ml r."ENVDBAddress"}, "CDBAddress" = {ml r."CDBAddress"}, "CDBConKey" = {ml r."CDBConKey"}, "IISAddress" = {ml r."IISAddress"}, "AppInstallerLocation" = {ml r."AppInstallerLocation"}, "RDBScriptLocation" = {ml r."RDBScriptLocation"}, "TaxLibLocation" = {ml r."TaxLibLocation"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "EnvironmentMasterId" = {ml r."EnvironmentMasterId"}
