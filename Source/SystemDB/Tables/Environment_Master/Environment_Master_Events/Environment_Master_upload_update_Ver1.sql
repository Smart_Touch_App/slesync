 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Environment_Master"
SET "EnvName" = {drh r."EnvName"}, "EnvDescription" = {drh r."EnvDescription"}, "IsActive" = {drh r."IsActive"}, "ReleaseLevel" = {drh r."ReleaseLevel"}, "MLServerAddress" = {drh r."MLServerAddress"}, "JDEWSAddress" = {drh r."JDEWSAddress"}, "REFDBAddress" = {drh r."REFDBAddress"}, "ENVDBAddress" = {drh r."ENVDBAddress"}, "CDBAddress" = {drh r."CDBAddress"}, "IISAddress" = {drh r."IISAddress"}, "AppInstallerLocation" = {drh r."AppInstallerLocation"}, "RDBScriptLocation" = {drh r."RDBScriptLocation"}, "TaxLibLocation" = {drh r."TaxLibLocation"}, "CreatedBy" = {drh r."CreatedBy"}, "CreatedDatetime" = {drh r."CreatedDatetime"}, "UpdatedBy" = {drh r."UpdatedBy"}
WHERE "EnvironmentMasterId" = {drh r."EnvironmentMasterId"}
 
