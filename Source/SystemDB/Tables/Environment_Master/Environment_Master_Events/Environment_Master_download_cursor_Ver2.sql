
 
SELECT "EnvironmentMasterId"
,"EnvName"
,"EnvDescription"
,"IsActive"
,"ReleaseLevel"
,"MLServerAddress"
,"JDEWSAddress"
,"REFDBAddress"
,"ENVDBAddress"
,"CDBAddress"
,"CDBConKey"
,"IISAddress"
,"AppInstallerLocation"
,"RDBScriptLocation"
,"TaxLibLocation"
,"CreatedBy"
,"CreatedDatetime"
,"UpdatedBy"
,"UpdatedDatetime"

FROM "BUSDTA"."Environment_Master"
WHERE "BUSDTA"."Environment_Master"."last_modified">= {drh s.last_table_download}
 

