

SELECT 
"BUSDTA"."Environment_Master"."EnvironmentMasterId"
,"BUSDTA"."Environment_Master"."EnvName"
,"BUSDTA"."Environment_Master"."EnvDescription"
,"BUSDTA"."Environment_Master"."IsActive"
,"BUSDTA"."Environment_Master"."ReleaseLevel"
,"BUSDTA"."Environment_Master"."MLServerAddress"
,"BUSDTA"."Environment_Master"."JDEWSAddress"
,"BUSDTA"."Environment_Master"."REFDBAddress"
,"BUSDTA"."Environment_Master"."ENVDBAddress"
,"BUSDTA"."Environment_Master"."CDBAddress"
,"BUSDTA"."Environment_Master"."CDBConKey"
,"BUSDTA"."Environment_Master"."IISAddress"
,"BUSDTA"."Environment_Master"."AppInstallerLocation"
,"BUSDTA"."Environment_Master"."RDBScriptLocation"
,"BUSDTA"."Environment_Master"."TaxLibLocation"
,"BUSDTA"."Environment_Master"."CreatedBy"
,"BUSDTA"."Environment_Master"."CreatedDatetime"
,"BUSDTA"."Environment_Master"."UpdatedBy"
,"BUSDTA"."Environment_Master"."UpdatedDatetime"

FROM "BUSDTA"."Environment_Master"
WHERE "BUSDTA"."Environment_Master"."UpdatedDatetime">= {drh s.last_table_download}