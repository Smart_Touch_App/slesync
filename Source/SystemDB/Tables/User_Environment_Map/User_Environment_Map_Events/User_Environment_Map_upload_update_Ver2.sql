
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."User_Environment_Map"
SET "EnvironmentMasterId" = {drh r."EnvironmentMasterId"}, "AppUserId" = {drh r."AppUserId"}, "IsEnvAdmin" = {drh r."IsEnvAdmin"}, "CreatedBy" = {drh r."CreatedBy"}, "CreatedDatetime" = {drh r."CreatedDatetime"}, "UpdatedBy" = {drh r."UpdatedBy"}
WHERE "UserEnvironmentMapId" = {drh r."UserEnvironmentMapId"}
 

