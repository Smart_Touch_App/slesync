

/* [BUSDTA].[User_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[User_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Environment_Map]
	(
	  [UserEnvironmentMapId] NUMERIC(8,0) NOT NULL
	, [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [AppUserId] NUMERIC(8,0) NOT NULL
	, [IsEnvAdmin] VARCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL
	, CONSTRAINT [PK_User_Environment_Map] PRIMARY KEY ([UserEnvironmentMapId] ASC)
	)

END
/* TableDDL - [BUSDTA].[User_Environment_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[User_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Environment_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Environment_Map_del]
	(
	  [UserEnvironmentMapId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([UserEnvironmentMapId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[User_Environment_Map] - End */
/* TRIGGERS FOR User_Environment_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.User_Environment_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.User_Environment_Map_ins
	ON BUSDTA.User_Environment_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.User_Environment_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.User_Environment_Map_del.UserEnvironmentMapId= inserted.UserEnvironmentMapId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.User_Environment_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Environment_Map_upd
	ON BUSDTA.User_Environment_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.User_Environment_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.User_Environment_Map.UserEnvironmentMapId= inserted.UserEnvironmentMapId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.User_Environment_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.User_Environment_Map_dlt
	ON BUSDTA.User_Environment_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.User_Environment_Map_del (UserEnvironmentMapId, last_modified )
	SELECT deleted.UserEnvironmentMapId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR User_Environment_Map - END */

