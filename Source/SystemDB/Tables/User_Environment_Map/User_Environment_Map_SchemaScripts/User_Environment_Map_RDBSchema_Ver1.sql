/* [BUSDTA].[User_Environment_Map] - begins */

/* TableDDL - [BUSDTA].[User_Environment_Map] - Start */
IF OBJECT_ID('[BUSDTA].[User_Environment_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[User_Environment_Map]
	(
	  [UserEnvironmentMapId] NUMERIC(8,0) NOT NULL
	, [EnvironmentMasterId] NUMERIC(8,0) NOT NULL
	, [AppUserId] NUMERIC(8,0) NOT NULL
	, [IsEnvAdmin] VARCHAR(1) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL
	, PRIMARY KEY ([UserEnvironmentMapId] ASC)
	)
END
/* TableDDL - [BUSDTA].[User_Environment_Map] - End */
