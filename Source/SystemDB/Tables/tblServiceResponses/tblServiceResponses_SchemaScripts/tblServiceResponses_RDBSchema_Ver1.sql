/* [dbo].[tblServiceResponses] - begins */

/* TableDDL - [dbo].[tblServiceResponses] - Start */
IF OBJECT_ID('[dbo].[tblServiceResponses]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblServiceResponses]
	(
	  [ServiceResponseID] INTEGER NOT NULL DEFAULT AUTOINCREMENT
	, [MessageCode] VARCHAR(10) NULL
	, [MessageText] VARCHAR(500) NULL
	, [CreatedBy] [numeric](8, 0) NULL
	, [CreatedDatetime] [datetime] NULL default getdate()
	, [UpdatedBy] [numeric](8, 0) NULL
	, [UpdatedDatetime] [datetime] NULL default getdate()
	, PRIMARY KEY ([ServiceResponseID] ASC)
	)
END
/* TableDDL - [dbo].[tblServiceResponses] - End */
