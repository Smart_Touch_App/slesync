/* Update the row in the consolidated database. */
UPDATE "dbo"."tblServiceResponses"
SET "MessageCode" = {drh r."MessageCode"}, "MessageText" = {drh r."MessageText"}, "CreatedBy" = {drh r."CreatedBy"}, "CreatedDateTime" = {drh r."CreatedDateTime"}, "EditedBy" = {drh r."EditedBy"}, "EditedDateTime" = {drh r."EditedDateTime"}
WHERE "ServiceResponseID" = {drh r."ServiceResponseID"}
