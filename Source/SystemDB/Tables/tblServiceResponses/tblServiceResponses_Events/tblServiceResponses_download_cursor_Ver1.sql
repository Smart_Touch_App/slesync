SELECT "ServiceResponseID"
,"MessageCode"
,"MessageText"
,"CreatedBy"
,"CreatedDateTime"
,"EditedBy"
,"EditedDateTime"

FROM "dbo"."tblServiceResponses"
WHERE "dbo"."tblServiceResponses"."EditedDateTime">= {drh s.last_table_download}
