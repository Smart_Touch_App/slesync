/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Device_Master"
SET "Active" = {drh r."Active"}, "manufacturer" = {drh r."manufacturer"}, "model" = {drh r."model"}
WHERE "Device_Id" = {drh r."Device_Id"}
