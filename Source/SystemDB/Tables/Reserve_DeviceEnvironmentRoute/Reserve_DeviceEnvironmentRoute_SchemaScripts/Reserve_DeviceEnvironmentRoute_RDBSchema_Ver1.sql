
/* [BUSDTA].[Reserve_DeviceEnvironmentRoute] - begins */

/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - Start */
IF OBJECT_ID('[BUSDTA].[Reserve_DeviceEnvironmentRoute]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Reserve_DeviceEnvironmentRoute]
	(
	  [ReservationID] INTEGER NOT NULL autoincrement
	, [DeviceId] VARCHAR(30) NOT NULL
	, [EnvironmentMasterId] INTEGER NOT NULL
	, [RouteID] VARCHAR(30) NOT NULL
	, [AppUserId] INTEGER NOT NULL
	, [ReservedDateTime] DATETIME NOT NULL DEFAULT(getdate())
	, PRIMARY KEY ([ReservationID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - End */
