
/* [BUSDTA].[Reserve_DeviceEnvironmentRoute] - begins */

/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - Start */
IF OBJECT_ID('[BUSDTA].[Reserve_DeviceEnvironmentRoute]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Reserve_DeviceEnvironmentRoute]
	(
	  [ReservationID] INT NOT NULL IDENTITY(1,1)
	, [DeviceId] VARCHAR(30) NOT NULL
	, [EnvironmentMasterId] INT NOT NULL
	, [RouteID] VARCHAR(30) NOT NULL
	, [AppUserId] INT NOT NULL
	, [ReservedDateTime] DATETIME NOT NULL CONSTRAINT DF_RESERVE_DEVICEENVIRONMENTROUTE_ReservedDateTime DEFAULT(getdate())
	)

END

/* TRIGGERS FOR Reserve_DeviceEnvironmentRoute - END */