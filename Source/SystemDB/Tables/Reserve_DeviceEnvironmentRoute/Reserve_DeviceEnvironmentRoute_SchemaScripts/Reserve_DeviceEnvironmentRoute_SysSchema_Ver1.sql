/* [BUSDTA].[Reserve_DeviceEnvironmentRoute] - begins */

/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - Start */
IF OBJECT_ID('[BUSDTA].[Reserve_DeviceEnvironmentRoute]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Reserve_DeviceEnvironmentRoute]
	(
	  [ReservationID] INT NOT NULL IDENTITY(1,1)
	, [DeviceId] VARCHAR(30) NOT NULL
	, [EnvironmentMasterId] INT NOT NULL
	, [RouteID] VARCHAR(30) NOT NULL
	, [AppUserId] INT NOT NULL
	, [ReservedDateTime] DATETIME NOT NULL CONSTRAINT DF_RESERVE_DEVICEENVIRONMENTROUTE_ReservedDateTime DEFAULT(getdate())
	, CONSTRAINT [PK_Reserve_DeviceEnvironmentRoute] PRIMARY KEY ([ReservationID] ASC)
	)

END
/* TableDDL - [BUSDTA].[Reserve_DeviceEnvironmentRoute] - End */

/* SHADOW TABLE FOR [BUSDTA].[Reserve_DeviceEnvironmentRoute] - Start */
IF OBJECT_ID('[BUSDTA].[Reserve_DeviceEnvironmentRoute_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Reserve_DeviceEnvironmentRoute_del]
	(
	  [ReservationID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ReservationID] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Reserve_DeviceEnvironmentRoute] - End */
/* TRIGGERS FOR Reserve_DeviceEnvironmentRoute - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Reserve_DeviceEnvironmentRoute_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Reserve_DeviceEnvironmentRoute_ins
	ON BUSDTA.Reserve_DeviceEnvironmentRoute AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Reserve_DeviceEnvironmentRoute_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Reserve_DeviceEnvironmentRoute_del.ReservationID= inserted.ReservationID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Reserve_DeviceEnvironmentRoute_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Reserve_DeviceEnvironmentRoute_upd
	ON BUSDTA.Reserve_DeviceEnvironmentRoute AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Reserve_DeviceEnvironmentRoute
	SET ReservedDateTime = GETDATE()
	FROM inserted
		WHERE BUSDTA.Reserve_DeviceEnvironmentRoute.ReservationID= inserted.ReservationID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Reserve_DeviceEnvironmentRoute_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Reserve_DeviceEnvironmentRoute_dlt
	ON BUSDTA.Reserve_DeviceEnvironmentRoute AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Reserve_DeviceEnvironmentRoute_del (ReservationID, last_modified )
	SELECT deleted.ReservationID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Reserve_DeviceEnvironmentRoute - END */
