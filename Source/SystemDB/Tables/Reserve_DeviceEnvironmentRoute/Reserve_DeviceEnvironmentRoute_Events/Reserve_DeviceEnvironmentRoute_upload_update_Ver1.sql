/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Reserve_DeviceEnvironmentRoute"
SET "DeviceId" = {drh r."DeviceId"}, "EnvironmentMasterId" = {drh r."EnvironmentMasterId"}, "RouteID" = {drh r."RouteID"}, "AppUserId" = {drh r."AppUserId"}, "ReservedDateTime" = {drh r."ReservedDateTime"}
WHERE "ReservationID" = {drh r."ReservationID"}
