SELECT "ServiceLogID"
,"DeviceID"
,"Request"
,"Response"
,"ResponseOn"
,"Environment"
,"DBName"
,"ServiceName"
,"ServiceEndPoint"
,"ServiceContract"

FROM "dbo"."tblWebServiceLog"
WHERE "dbo"."tblWebServiceLog"."last_modified">= {drh s.last_table_download}
