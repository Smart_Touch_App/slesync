/* Update the row in the consolidated database. */
UPDATE "dbo"."tblWebServiceLog"
SET "DeviceID" = {drh r."DeviceID"}, "Request" = {drh r."Request"}, "Response" = {drh r."Response"}, "ResponseOn" = {drh r."ResponseOn"}, "Environment" = {drh r."Environment"}, "DBName" = {drh r."DBName"}, "ServiceName" = {drh r."ServiceName"}, "ServiceEndPoint" = {drh r."ServiceEndPoint"}, "ServiceContract" = {drh r."ServiceContract"}
WHERE "ServiceLogID" = {drh r."ServiceLogID"}
