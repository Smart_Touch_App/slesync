/* [dbo].[tblWebServiceLog] - begins */

/* TableDDL - [dbo].[tblWebServiceLog] - Start */
IF OBJECT_ID('[dbo].[tblWebServiceLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblWebServiceLog]
	(
	  [ServiceLogID] INTEGER NOT NULL
	, [DeviceID] VARCHAR(100) NULL
	, [Request] XML NULL
	, [Response] XML NULL
	, [ResponseOn] DATETIME NULL DEFAULT(getdate())
	, [Environment] VARCHAR(128) NULL
	, [DBName] VARCHAR(128) NULL
	, [ServiceName] VARCHAR(100) NULL
	, [ServiceEndPoint] VARCHAR(1000) NULL
	, [ServiceContract] VARCHAR(1000) NULL
	, PRIMARY KEY ([ServiceLogID] ASC)
	)
END
/* TableDDL - [dbo].[tblWebServiceLog] - End */
