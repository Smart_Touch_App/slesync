/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."RemoteDBID_Master"
SET "AppUserId" = {drh r."AppUserId"}, "DeviceID" = {drh r."DeviceID"}, "RouteID" = {drh r."RouteID"}, "EnvironmentMasterID" = {drh r."EnvironmentMasterID"}, "ActiveYN" = {drh r."ActiveYN"}, "CreatedOn" = {drh r."CreatedOn"}
WHERE "RemoteDBID" = {drh r."RemoteDBID"}
