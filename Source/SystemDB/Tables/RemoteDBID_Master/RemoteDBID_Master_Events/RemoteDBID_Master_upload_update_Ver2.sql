/*
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."RemoteDBID_Master"
SET "AppUserId" = {drh r."AppUserId"}, "DeviceID" = {drh r."DeviceID"}, "RouteID" = {drh r."RouteID"}, "EnvironmentMasterID" = {drh r."EnvironmentMasterID"}, "ActiveYN" = {drh r."ActiveYN"}, "CreatedDateTime" = {drh r."CreatedDateTime"}
WHERE "RemoteDBID" = {drh r."RemoteDBID"}
 
*/
