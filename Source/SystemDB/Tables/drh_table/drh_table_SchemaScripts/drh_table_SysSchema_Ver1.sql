/* [dbo].[drh_table] - begins */

/* TableDDL - [dbo].[drh_table] - Start */
IF OBJECT_ID('[dbo].[drh_table]') IS NULL
BEGIN

	CREATE TABLE [dbo].[drh_table]
	(
	  [table_id] INT NOT NULL
	, [schema] VARCHAR(128) NOT NULL
	, [name] VARCHAR(128) NOT NULL
	, CONSTRAINT [PK_drh_table] PRIMARY KEY ([table_id] ASC)
	)

END
/* TableDDL - [dbo].[drh_table] - End */
