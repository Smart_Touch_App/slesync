/* [dbo].[drh_script] - begins */

/* TableDDL - [dbo].[drh_script] - Start */
IF OBJECT_ID('[dbo].[drh_script]') IS NULL
BEGIN

	CREATE TABLE [dbo].[drh_script]
	(
	  [script_id] INT NOT NULL
	, [version_id] INT NOT NULL
	, [table_id] INT NOT NULL
	, [event] VARCHAR(128) NOT NULL
	, [script] VARCHAR(MAX) NOT NULL
	, CONSTRAINT [PK_drh_script] PRIMARY KEY ([version_id] ASC, [table_id] ASC, [event] ASC)
	)

ALTER TABLE [dbo].[drh_script] WITH CHECK ADD CONSTRAINT [FK__drh_scrip__versi__70DDC3D8] FOREIGN KEY([version_id]) REFERENCES [dbo].[drh_script_version] ([version_id])
ALTER TABLE [dbo].[drh_script] CHECK CONSTRAINT [FK__drh_scrip__versi__70DDC3D8]

ALTER TABLE [dbo].[drh_script] WITH CHECK ADD CONSTRAINT [FK__drh_scrip__table__71D1E811] FOREIGN KEY([table_id]) REFERENCES [dbo].[drh_table] ([table_id])
ALTER TABLE [dbo].[drh_script] CHECK CONSTRAINT [FK__drh_scrip__table__71D1E811]

END
/* TableDDL - [dbo].[drh_script] - End */
