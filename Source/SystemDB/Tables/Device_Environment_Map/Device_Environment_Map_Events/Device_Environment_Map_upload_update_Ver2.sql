
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Device_Environment_Map"
SET "CreatedBy" = {drh r."CreatedBy"}, "CreatedDatetime" = {drh r."CreatedDatetime"}, "UpdatedBy" = {drh r."UpdatedBy"}
WHERE "EnvironmentMasterId" = {drh r."EnvironmentMasterId"} AND "DeviceId" = {drh r."DeviceId"}
 

