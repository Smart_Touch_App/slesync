/* Update the row in the consolidated database. */
UPDATE "dbo"."tblReserveConfig"
SET "ExpireMins" = {drh r."ExpireMins"}, "CreatedBy" = {drh r."CreatedBy"}, "CreatedOn" = {drh r."CreatedOn"}, "EditedBy" = {drh r."EditedBy"}, "EditedOn" = {drh r."EditedOn"}
WHERE "ReserveConfigID" = {drh r."ReserveConfigID"}
