/* [dbo].[tblReserveConfig] - begins */

/* TableDDL - [dbo].[tblReserveConfig] - Start */
IF OBJECT_ID('[dbo].[tblReserveConfig]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblReserveConfig]
	(
	  [ReserveConfigID] INTEGER NOT NULL DEFAULT AUTOINCREMENT
	, [ExpireMins] INTEGER NOT NULL
	, [CreatedBy] [numeric](8, 0) NULL
	, [CreatedDatetime] [datetime] NULL default getdate()
	, [UpdatedBy] [numeric](8, 0) NULL
	, [UpdatedDatetime] [datetime] NULL default getdate()
	, PRIMARY KEY ([ReserveConfigID] ASC)
	)
END
/* TableDDL - [dbo].[tblReserveConfig] - End */
