/*
/* [dbo].[tblSystemSetup] - begins */

/* TableDDL - [dbo].[tblSystemSetup] - Start */
IF OBJECT_ID('[dbo].[tblSystemSetup]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblSystemSetup]
	(
	  [SystemSetupID] INT NOT NULL IDENTITY(1,1)
	, [KeyName] VARCHAR(128) NOT NULL
	, [KeyValue] VARCHAR(MAX) NOT NULL
	, [Description] VARCHAR(MAX) NULL
	, [ActiveYN] BIT NULL CONSTRAINT DF_TBLSYSTEMSETUP_ActiveYN DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLSYSTEMSETUP_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYSTEMSETUP_EditedOn DEFAULT(getdate())
	, CONSTRAINT [PK_tblSystemSetup] PRIMARY KEY ([SystemSetupID] ASC)
	)

END
/* TableDDL - [dbo].[tblSystemSetup] - End */
*/
