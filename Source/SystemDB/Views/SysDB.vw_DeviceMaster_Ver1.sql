CREATE VIEW [dbo].[vw_DeviceMaster]
AS
	SELECT Device_Id, DeviceName, Active, manufacturer, model, last_modified
	FROM BUSDTA.Device_Master

GO