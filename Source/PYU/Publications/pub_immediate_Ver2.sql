DROP PUBLICATION IF EXISTS pub_immediate;
GO
/** Create publication 'pub_immediate'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_immediate" 

(
TABLE "BUSDTA"."CustomerConfig" ,
	TABLE "BUSDTA"."F0116" ,
	TABLE "BUSDTA"."F03012" ,
	TABLE "BUSDTA"."F4015" ,
	TABLE "BUSDTA"."M0111" ,
	TABLE "BUSDTA"."M0112" ,
	TABLE "BUSDTA"."M03042" ,
	TABLE "BUSDTA"."M04012" ,
	TABLE "BUSDTA"."M40111" ,
	TABLE "BUSDTA"."M40116" ,
	TABLE "BUSDTA"."M4016" ,
	TABLE "BUSDTA"."Order_Detail" ,
	TABLE "BUSDTA"."Order_Detail_BETA" ,
	TABLE "BUSDTA"."Order_Header" ,
	TABLE "BUSDTA"."Order_Header_BETA" ,
	TABLE "BUSDTA"."Order_PriceAdj" ,
	TABLE "BUSDTA"."Order_PriceAdj_BETA" ,
	TABLE "BUSDTA"."PickOrder" ,
	TABLE "BUSDTA"."PickOrder_Exception"    

)
GO
