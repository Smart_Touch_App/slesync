DROP PUBLICATION IF EXISTS pub_immediate;
GO
/** Create publication 'pub_immediate'. **/
CREATE PUBLICATION IF NOT EXISTS "pub_immediate" (
    TABLE "MobileDataModel"."BUSDTA"."F0116" ,
    TABLE "MobileDataModel"."BUSDTA"."F03012" ,
    TABLE "MobileDataModel"."BUSDTA"."F4015" ,
    TABLE "MobileDataModel"."BUSDTA"."M0111" ,
    TABLE "MobileDataModel"."BUSDTA"."M0112" ,
    TABLE "MobileDataModel"."BUSDTA"."M03042" ,
    TABLE "MobileDataModel"."BUSDTA"."M4016" ,
    TABLE "MobileDataModel"."BUSDTA"."Order_Detail" ,
    TABLE "MobileDataModel"."BUSDTA"."Order_Header" ,
    TABLE "MobileDataModel"."BUSDTA"."PickOrder_Exception" ,
    TABLE "MobileDataModel"."BUSDTA"."PickOrder" ,
    TABLE "MobileDataModel"."BUSDTA"."M04012" ,
    TABLE "MobileDataModel"."BUSDTA"."M40116" ,
    TABLE "MobileDataModel"."BUSDTA"."M40111" 
)
GO
