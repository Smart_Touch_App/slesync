DROP PUBLICATION IF EXISTS "SystemDB_pub_everything";
GO
/** Create publication 'SystemDB_pub_everything'. **/
CREATE PUBLICATION IF NOT EXISTS "SystemDB_pub_everything" (
    TABLE "BUSDTA"."Device_Environment_Map" ,
    TABLE "BUSDTA"."Device_Master" ,
    TABLE "BUSDTA"."Environment_Master" ,
    TABLE "BUSDTA"."User_Environment_Map" ,
    TABLE "BUSDTA"."user_master" 
)
GO
