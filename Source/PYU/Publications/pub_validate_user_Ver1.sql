DROP PUBLICATION IF EXISTS pub_validate_user;
GO
/** Create publication 'pub_validate_user'. **/
CREATE PUBLICATION "pub_validate_user" (
    TABLE "MobileDataModel"."BUSDTA"."Device_Master" ,
    TABLE "MobileDataModel"."BUSDTA"."Route_Device_Map" ,
    TABLE "MobileDataModel"."BUSDTA"."Route_User_Map" ,
    TABLE "MobileDataModel"."BUSDTA"."user_master" ,
    TABLE "MobileDataModel"."BUSDTA"."User_Role_Map" 
)
GO
