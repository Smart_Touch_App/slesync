-- ***************************************************************************
-- Copyright (c) 2014 SAP AG or an SAP affiliate company. All rights reserved.
-- ***************************************************************************

--
-- Create the MobiLink Server system tables and stored procedures in
-- a Microsoft SQL Server consolidated database.
--

set ANSI_PADDING OFF
go
create table ml_ldap_server (
    ldsrv_id		integer		not null identity,
    ldsrv_name		varchar( 128 )	not null,
    search_url		varchar( 1024 )	not null,
    access_dn		varchar( 1024 )	not null,
    access_dn_pwd	varchar( 256 )	not null,
    auth_url		varchar( 1024 )	not null,
    num_retries		tinyint		not null default 3,
    timeout		integer		not null default 10,
    start_tls		tinyint		not null default 0,
    unique( ldsrv_name ),
    primary key ( ldsrv_id ) ) 
go

create table ml_trusted_certificates_file (
    file_name		varchar( 1024 ) not null ) 
go

create table ml_user_auth_policy (
    policy_id			integer		not null identity,
    policy_name			varchar( 128 )	not null,
    primary_ldsrv_id		integer		not null,
    secondary_ldsrv_id		integer		null,
    ldap_auto_failback_period	integer		not null default 900,
    ldap_failover_to_std	tinyint		not null default 1,
    unique( policy_name ),
    foreign key( primary_ldsrv_id ) references ml_ldap_server( ldsrv_id ),
    foreign key( secondary_ldsrv_id ) references ml_ldap_server( ldsrv_id ),
    primary key( policy_id ) ) 
go

create table ml_user (
    user_id		integer		not null identity,
    name		varchar( 128 )	not null,
    hashed_password	binary(32)	null,
    policy_id		integer		null,
    user_dn		varchar( 1024 )	null,
    unique( name ),
    foreign key( policy_id ) references ml_user_auth_policy( policy_id ),
    primary key( user_id ) ) 
go

create table ml_database (
    rid			integer		not null identity,
    remote_id		varchar( 128 )	not null unique,
    script_ldt		datetime	default CONVERT( DATETIME, '01/01/1900', 101 ) not null,
    seq_id		binary(16)	null,
    seq_uploaded	integer		default 0 not null,
    sync_key		varchar( 40 )	null,
    description		varchar( 128 )	null,
    primary key( rid ) )
go

create table ml_subscription (
    rid			    integer	    not null,
    subscription_id	    varchar( 128 )  default '<unknown>' not null,
    user_id		    integer	    not null,
    progress		    numeric( 20 )   default 0 not null,
    publication_name	    varchar( 128 )  default '<unknown>' not null,
    last_upload_time	    datetime	    default CONVERT( DATETIME, '01/01/1900', 101 ) not null,
    last_download_time	    datetime	    default CONVERT( DATETIME, '01/01/1900', 101 ) not null,
    primary key( rid, subscription_id ),
    foreign key( rid ) references ml_database ( rid ),
    foreign key( user_id ) references ml_user ( user_id ) )
go

create table ml_table (
    table_id		integer			not null,
    name		varchar( 128 )     	not null unique,
    primary key( table_id ) )
go
    
create table ml_script (
    script_id		integer			not null,
    script		varchar(max)		not null,
    script_language	varchar( 128 )		default 'sql' not null
    ,
    checksum		varchar( 64 )		null
    ,
    primary key( script_id ) )
go

create table ml_script_version (
    version_id		integer			not null, 
    name		varchar( 128 )		not null unique,
    description		varchar(max)		null,
    primary key( version_id ) )
go
       
create table ml_connection_script (
    version_id		integer			not null,
    event		varchar( 128 )     	not null,
    script_id		integer			not null,
    foreign key( version_id ) references ml_script_version ( version_id ),
    foreign key( script_id ) references ml_script ( script_id ),
    primary key( version_id, event ) )
go
    
create table ml_table_script (
    version_id		integer			not null,
    table_id		integer			not null,
    event		varchar( 128 )		not null,
    script_id		integer			not null,
    foreign key( version_id ) references ml_script_version ( version_id ),
    foreign key( table_id ) references ml_table ( table_id ),
    foreign key( script_id ) references ml_script ( script_id ),
    primary key( version_id, table_id, event ) )
go

create table ml_property (
    component_name		varchar( 128 )	not null,
    property_set_name		varchar( 128 )	not null,
    property_name		varchar( 128 )	not null,
    property_value		varchar(max)	not null,
    primary key( component_name, property_set_name, property_name ) )
go
    
create table ml_scripts_modified (
    last_modified	datetime		primary key not null )
go

delete from ml_scripts_modified
go

insert into ml_scripts_modified ( last_modified ) values ( GETDATE() )
go

create table ml_column (
    version_id	integer		not null,
    table_id	integer		not null,
    idx		integer		not null,
    name	varchar( 128 )	not null,
    type	varchar( 128 )	null,
    primary key( idx, version_id, table_id ),
    unique( version_id, table_id, name ),
    foreign key( version_id ) references ml_script_version ( version_id ),
    foreign key( table_id ) references ml_table ( table_id ) )
go

create trigger ml_script_trigger 
    on ml_script
    for delete, insert, update
as
begin
    update ml_scripts_modified set last_modified = GETDATE();
end
go

create trigger ml_table_script_trigger 
    on ml_table_script
    for delete, insert, update
as
begin
    update ml_scripts_modified set last_modified = GETDATE();
end
go

create trigger ml_connection_script_trigger 
    on ml_connection_script
    for delete, insert, update
as
begin
    update ml_scripts_modified set last_modified = GETDATE();
end
go

create trigger ml_column_trigger 
    on ml_column
    for delete, insert, update
as
begin
    update ml_scripts_modified set last_modified = GETDATE();
end
go

create table ml_primary_server (
    server_id		integer		not null identity,
    name		varchar( 128 )	not null unique,
    connection_info	varchar( 2048 )	not null,
    instance_key	binary( 32 )	not null,
    start_time		datetime	not null default getdate(),
    primary key( server_id ) )
go

-- quoted identifier is on by default, but dynamic sql
-- executed within a procedure has quoted identifier set to
-- OFF unless its explicitly set when creating the procedure
set QUOTED_IDENTIFIER ON
go
create table ml_model_schema (
    schema_type		varchar( 32 )	not null,
    schema_name		varchar( 128 )  not null,
    table_name		varchar( 128 )  not null,
    object_name		varchar( 128 )  not null,
    drop_stmt		varchar( 2000 ) not null,
    checksum		varchar( 64 )   not null,
    db_checksum		varchar( 64 )   null,
    locked		bit		not null,
    primary key( schema_type, schema_name, table_name, object_name ) ) 
go

create table ml_model_schema_use (
    version_id		integer		not null,
    schema_type		varchar( 32 )   not null,
    schema_name		varchar( 128 )  not null,
    table_name		varchar( 128 )  not null,
    object_name		varchar( 128 )  not null,
    checksum		varchar( 64 )   not null,
    primary key( version_id, schema_type, schema_name, table_name, object_name ) ) 
go

create procedure ml_model_begin_check
    @version		varchar( 128 )
as
begin
    declare @version_id		integer
    
    set nocount on

    select @version_id = version_id from ml_script_version 
	where name = @version

    -- If this same script version was previously installed, 
    -- clean-up any meta-data associated with it.
    delete from ml_model_schema_use where version_id = @version_id
end;
go

create procedure ml_model_begin_install
    @version		varchar( 128 )
as
begin
    declare @version_id		integer
    
    set nocount on

    select @version_id = version_id from ml_script_version 
	where name = @version

    -- If this same script version was previously installed, 
    -- clean-up any meta-data associated with it.
    delete from ml_column where version_id = @version_id
    delete from ml_connection_script where version_id = @version_id
    delete from ml_table_script where version_id = @version_id
    delete from ml_model_schema_use where version_id = @version_id
    delete from ml_script_version where version_id = @version_id
end;
go

create function ml_model_get_catalog_checksum(
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 ) )
    returns varchar( 64 )
as
begin
    return null;
end;
go

create procedure ml_model_register_schema 
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 ),
    @drop_stmt		varchar( 2000 ),
    @checksum		varchar( 64 ),
    @locked		bit 
as
begin
    declare @db_checksum varchar( 64 )

    set nocount on

    if @drop_stmt is null 
	select @drop_stmt = drop_stmt from ml_model_schema 
	    where schema_type = @schema_type and schema_name = @schema_name
		and table_name = @table_name and object_name = @object_name

    if @checksum is null
	select @checksum = checksum from ml_model_schema 
	    where schema_type = @schema_type and schema_name = @schema_name
		and table_name = @table_name and object_name = @object_name

    if @locked is null
	select @locked = locked from ml_model_schema 
	    where schema_type = @schema_type and schema_name = @schema_name
		and table_name = @table_name and object_name = @object_name

    exec @db_checksum = ml_model_get_catalog_checksum @schema_type, @schema_name, @table_name, @object_name;

    if exists( select * from ml_model_schema where schema_type = @schema_type 
	    and schema_name = @schema_name and table_name = @table_name and object_name = @object_name )
	update ml_model_schema set drop_stmt = @drop_stmt, checksum = @checksum, db_checksum = @db_checksum, locked = @locked
	    where schema_type = @schema_type and schema_name = @schema_name
		and table_name = @table_name and object_name = @object_name
    else
	insert into ml_model_schema
	    ( schema_type, schema_name, table_name, object_name, drop_stmt, checksum, db_checksum, locked )
	    values( @schema_type, @schema_name, @table_name, @object_name, @drop_stmt, @checksum, @db_checksum, @locked )
end
go

create procedure ml_model_deregister_schema 
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 )
as    
begin
    set nocount on
    if @schema_type = 'TABLE'
	delete from ml_model_schema 
	    where schema_type = @schema_type 
		and schema_name = @schema_name 
		and table_name = @table_name
    else
	delete from ml_model_schema 
	    where schema_type = @schema_type 
		and schema_name = @schema_name 
		and table_name = @table_name
		and object_name = @object_name
end
go

create procedure ml_model_register_schema_use
    @version		varchar( 128 ),
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 ),
    @checksum		varchar( 64 )
as
begin
    declare @version_id	integer
    
    set nocount on
    
    select @version_id = version_id from ml_script_version 
	where name = @version
    if @version_id is null begin
	-- Insert to the ml_script_version
	select @version_id = max( version_id )+1 from ml_script_version
	if @version_id is null begin
	    -- No rows are currently in ml_script_version
	    set @version_id = 1;
	end
	insert into ml_script_version ( version_id, name )
		values ( @version_id, @version );
    end

    if exists ( select * from ml_model_schema_use
	where version_id = @version_id and schema_type = @schema_type
	    and schema_name = @schema_name and table_name = @table_name
	    and object_name = @object_name ) 
	update ml_model_schema_use set checksum = @checksum
	    where version_id = @version_id and schema_type = @schema_type
		and schema_name = @schema_name and table_name = @table_name
		and object_name = @object_name
    else		
	insert into ml_model_schema_use
	    ( version_id, schema_type, schema_name, table_name, object_name, checksum )
	    values( @version_id, @schema_type, @schema_name, @table_name, @object_name, @checksum )
end
go

create procedure ml_model_mark_schema_verified
    @version		varchar( 128 ),
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 )
as
begin
    declare @checksum	    varchar( 64 )
    declare @version_id	    integer
    declare @locked	    bit

    set nocount on

    select @version_id = version_id from ml_script_version 
	where name = @version

    select @checksum = checksum from ml_model_schema
	where schema_type = @schema_type and schema_name = @schema_name 
	    and table_name = @table_name and object_name = @object_name
	    
    if @checksum is not null
	update ml_model_schema_use set checksum = 'IGNORE'
	    where version_id = @version_id and schema_type = @schema_type and schema_name = @schema_name
		and table_name = @table_name and object_name = @object_name
    else begin
	select @checksum = checksum from ml_model_schema_use 
	    where version_id = @version_id and schema_type = @schema_type
		and schema_name = @schema_name and table_name = @table_name
		and object_name = @object_name
	select @locked = case when @schema_type = 'COLUMN' then 1 else 0 end
	exec ml_model_register_schema @schema_type, @schema_name, @table_name, @object_name, 
	    '-- Not dropped during uninstall', @checksum, @locked
    end
end
go

create function ml_model_check_catalog(
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 ) )
    returns varchar( 32 )
as
begin
    declare @checksum	      varchar(64)
    declare @orig_db_checksum varchar(64)
    declare @db_checksum     varchar(64)
    
    -- Return values
    -- 'UNUSED' - The requested schema isn't referenced by any ML meta-data
    -- 'MISSING' - The requested schema is not installed.
    -- 'MISMATCH' - The current schema doesn't match the ML meta-data
    -- 'UNVERIFIED' - A full schema comparison wasn't done, 
    --                generally we assume the schema is correct in this case
    -- 'INSTALLED' - The required schema is correctly installed.

    if ( @schema_type = 'TABLE'
	    and exists( select 1 from sys.tables t join sys.schemas s on t.schema_id = s.schema_id
		where t.name = @table_name and s.name = @schema_name ) )
	or ( @schema_type = 'TRIGGER'
	    and exists( select 1 from sys.triggers t 
		join sys.objects o on o.object_id = t.parent_id 
		join sys.schemas s on s.schema_id = o.schema_id
		where t.name = @object_name and o.name = @table_name and s.name = @schema_name ) )
	or ( @schema_type = 'INDEX'
	    and exists( SELECT 1 FROM sys.indexes x
		join sys.objects t on t.object_id = x.object_id
		join sys.schemas s on s.schema_id = t.schema_id
		WHERE  x.name = @object_name AND t.name = @table_name AND s.name = @schema_name ) )
	or ( @schema_type = 'COLUMN'
	    and exists( SELECT 1 FROM sys.columns c
		join sys.objects t on t.object_id = c.object_id
		join sys.schemas s on s.schema_id = t.schema_id
		WHERE c.name = @object_name AND t.name = @table_name AND s.name = @schema_name ) )
	or ( @schema_type = 'PROCEDURE'
	    and exists( SELECT 1 FROM sys.procedures p
		join sys.objects o on o.object_id = p.object_id
		join sys.schemas s on s.schema_id = o.schema_id
		WHERE p.name = @object_name AND s.name = @schema_name ) )
    begin
	-- The schema exists

	exec @db_checksum = ml_model_get_catalog_checksum @schema_type, @schema_name, @table_name, @object_name
	select @checksum = s.checksum, @orig_db_checksum = s.db_checksum from ml_model_schema s
		where s.schema_type = @schema_type and s.schema_name = @schema_name
		    and s.table_name = @table_name and s.object_name = @object_name

	if @checksum is null return 'UNUSED';
	if @orig_db_checksum is null or @db_checksum is null return 'UNVERIFIED';
	if @orig_db_checksum = @db_checksum return 'INSTALLED';
	return 'MISMATCH';
    end
	   
    -- The schema does not exist
    return 'MISSING'
end
go

create function ml_model_check_schema (
    @version		varchar( 128 ),
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 ) )
    returns varchar( 32 )
as
begin
    declare @status	varchar(32)
    declare @db_status	 varchar(32)

    -- Return values
    -- 'UNUSED' - The requested schema isn't needed for this version.
    -- 'MISSING' - The required schema is not installed.
    -- 'MISMATCH' - The current schema doesn't match what is needed and must be replaced.
    -- 'UNVERIFIED' - The existing schema must be manually checked to see if it matches what is needed.
    -- 'INSTALLED' - The required schema is correctly installed.

    select @status = case when s.checksum is null then 'MISSING' else
	    case when u.checksum = 'IGNORE' or u.checksum = s.checksum then 'INSTALLED' else 'MISMATCH' end
	end 
    from ml_model_schema_use u
	join ml_script_version v on v.version_id = u.version_id 
	left outer join ml_model_schema s 
	    on s.schema_type = u.schema_type and s.schema_name = u.schema_name 
		and s.table_name = u.table_name and s.object_name = u.object_name
    where v.name = @version and u.schema_type = @schema_type and u.schema_name = @schema_name
	and u.table_name = @table_name and u.object_name = @object_name
    if @status is null set @status = 'UNUSED'

    exec @db_status = ml_model_check_catalog @schema_type, @schema_name, @table_name, @object_name
    if @db_status = 'MISSING' return 'MISSING'
    if @status = 'UNUSED' or @status = 'MISMATCH' return @status
    if @status = 'MISSING' return 'UNVERIFIED'

    -- @status = 'INSTALLED'
    if @db_status = 'MISMATCH' return 'MISMATCH'

    -- If @db_status = 'UNVERIFIED' we are optimistic and assume it is correct
    return 'INSTALLED'
end
go

create function ml_model_get_schema_action (
    @version		varchar( 128 ),
    @schema_type	varchar( 32 ),
    @schema_name	varchar( 128 ),
    @table_name		varchar( 128 ),
    @object_name	varchar( 128 ),
    @upd_mode		varchar( 64 ) )
    returns varchar( 32 )
as
begin
    declare @status	varchar(32)
    declare @locked	bit

    exec @status = ml_model_check_schema @version, @schema_type, @schema_name, @table_name, @object_name; 

    if @status = 'MISSING' return 'CREATE'

    if @status = 'UNUSED' or @status = 'INSTALLED' or @upd_mode != 'OVERWRITE' or @schema_type = 'COLUMN' 
	-- Preserve the existing schema
	-- Note, 'REPLACE' won't work for columns because the column is likely 
	--     in an index and the drop will fail.  If the status is 'MISMATCH' 
	--     then the column will need to be manually altered in the database.
	return 'SKIP'

    if exists ( select locked from ml_model_schema
	    where schema_type = @schema_type and schema_name = @schema_name and table_name = @table_name 
		and object_name = @object_name and locked != 0 )
	-- The schema is marked as locked, preserve it.
	return 'SKIP'

    exec @status = ml_model_check_catalog @schema_type, @schema_name, @table_name, @object_name
    if @status = 'MISMATCH'
	-- The schema was modified since ML was deployed, we are careful not to destroy any schema
	-- that was not created by ML
	return 'SKIP'

    -- The existing schema doesn't match what is needed so replace it.
    return 'REPLACE'
end
go

create procedure ml_model_check_all_schema
as
begin
    declare @schema	varchar(32)

    set nocount on

    select @schema = s.name from sys.objects o join sys.schemas s on s.schema_id = o.schema_id 
	where object_id = object_id( 'ml_model_check_schema' )

    EXECUTE( '
	select 
	    case when s.schema_name is null then u.schema_name else s.schema_name end schema_name, 
	    case when s.table_name is null then u.table_name else s.table_name end table_name, 
	    case when s.schema_type is null then u.schema_type else s.schema_type end schema_type, 
	    case when s.object_name is null then u.object_name else s.object_name end object_name, 
	    s.locked, 
	    ver.name used_by,
	    ' + @schema + '.ml_model_check_schema( ver.name, 
		case when s.schema_type is null then u.schema_type else s.schema_type end, 
		case when s.schema_name is null then u.schema_name else s.schema_name end, 
		case when s.table_name is null then u.table_name else s.table_name end, 
		case when s.object_name is null then u.object_name else s.object_name end ) status,
	    case when ver.name is null then null else ' + @schema + '.ml_model_get_schema_action( ver.name, u.schema_type, u.schema_name, u.table_name, u.object_name, ''OVERWRITE'' ) end overwrite_action,
	    case when ver.name is null then null else ' + @schema + '.ml_model_get_schema_action( ver.name, u.schema_type, u.schema_name, u.table_name, u.object_name, ''PRESERVE_EXISTING_SCHEMA'' ) end preserve_action
	from ml_model_schema s 
	    full outer join ml_model_schema_use u on
		u.schema_type = s.schema_type and u.schema_name = s.schema_name
		and u.table_name = s.table_name and u.object_name = s.object_name
	    left outer join ml_script_version ver on
		u.version_id = ver.version_id
	order by schema_name, table_name, schema_type, object_name, used_by'
    );
end
go

create procedure ml_model_check_version_schema
    @version	varchar( 128 )
as
begin
    declare @schema	varchar(32)

    set nocount on

    select @schema = s.name from sys.objects o join sys.schemas s on s.schema_id = o.schema_id 
	where object_id = object_id( 'ml_model_check_schema' )

    EXECUTE( '
	select u.schema_name, u.table_name, u.schema_type, u.object_name, s.locked,
	    ' + @schema + '.ml_model_check_schema( ver.name, u.schema_type, u.schema_name, u.table_name, u.object_name ) status,
	    ' + @schema + '.ml_model_get_schema_action( ver.name, u.schema_type, u.schema_name, u.table_name, u.object_name, ''OVERWRITE'' ) overwrite_action,
	    ' + @schema + '.ml_model_get_schema_action( ver.name, u.schema_type, u.schema_name, u.table_name, u.object_name, ''PRESERVE_EXISTING_SCHEMA'' ) preserve_action
	from ml_model_schema_use u join ml_script_version ver on u.version_id = ver.version_id
	    left outer join ml_model_schema s on
		u.schema_type = s.schema_type and u.schema_name = s.schema_name
		and u.table_name = s.table_name and u.object_name = s.object_name
	where ver.name = ''' + @version + '''
	order by u.schema_name, u.table_name, u.schema_type, u.object_name'
    );
end
go

create procedure ml_model_drop_unused_schema
as
begin
    declare @status	    varchar(32)
    declare @schema_type    varchar(32)
    declare @schema_name    varchar(128)
    declare @object_name    varchar(128)
    declare @table_name     varchar(128)
    declare @qualified_table varchar(256)
    declare @drop_stmt	    varchar(2000)
    declare @drop_stmt2	    varchar(2000)

    declare drop_crsr cursor for
	select s.schema_type, s.schema_name, s.table_name, s.object_name, s.drop_stmt 
	from ml_model_schema s 
	    left outer join ml_model_schema_use u 
	    on u.schema_type = s.schema_type and u.schema_name = s.schema_name 
		and u.table_name = s.table_name and u.object_name = s.object_name
	where u.object_name is null and s.locked = 0 and s.drop_stmt not like '--%'
	    order by ( case s.schema_type
		when 'INDEX' then 1
		when 'TRIGGER' then 2
		when 'PROCEDURE' then 3
		when 'COLUMN' then 4
		when 'TABLE' then 5
		else 6
		end )

    set nocount on

    open drop_crsr
    while 1 = 1 begin
	fetch drop_crsr into @schema_type, @schema_name, @table_name, @object_name, @drop_stmt
	if @@fetch_status != 0 break
	
	-- We don't drop any schema modified since ML was deployed.
	exec @status = ml_model_check_catalog @schema_type, @schema_name, @table_name, @object_name
	if @status != 'MISMATCH' and @status != 'MISSING' begin
	    if @schema_type = 'COLUMN' begin
		-- Column defaults need to be manually dropped before the column can be dropped
		select @qualified_table = '"' + @schema_name + '"."' + @table_name + '"'
		select @drop_stmt2 = 'ALTER TABLE ' + @qualified_table + ' DROP CONSTRAINT "' + dc.name + '"'
		    from sys.default_constraints dc 
		    join sys.columns sc on sc.object_id = dc.parent_object_id and dc.parent_column_id = sc.column_id
		    where dc.parent_object_id = object_id( @schema_name + '.' + @table_name )
			and sc.name = @object_name
		if @drop_stmt2 is not null exec( @drop_stmt2 );
	    end

	    exec( @drop_stmt );
	end 
	exec ml_model_deregister_schema @schema_type, @schema_name, @table_name, @object_name
    end
    close drop_crsr
    deallocate drop_crsr
end
go

create procedure ml_model_drop
    @version	    varchar( 128 )
as
begin
    declare @version_id    integer

    set nocount on

    select @version_id = version_id from ml_script_version 
	where name = @version

    delete from ml_model_schema_use where version_id = @version_id
    delete from ml_column where version_id = @version_id
    delete from ml_connection_script where version_id = @version_id
    delete from ml_table_script where version_id = @version_id
    delete from ml_script_version where version_id = @version_id
    exec ml_model_drop_unused_schema
end
go

create table ml_passthrough_script (
    script_id			integer		not null identity,
    script_name			varchar( 128 )	not null unique,
    flags			varchar( 256 )	null,
    affected_pubs		varchar(max)	null,
    script			varchar(max)	not null,
    description 		varchar( 2000 )	null,
    primary key( script_id ) )
go

create table ml_passthrough (
    remote_id			varchar( 128 )	not null,
    run_order			integer		not null,
    script_id			integer		not null,
    last_modified 		datetime	not null default getdate(),
    primary key( remote_id, run_order ),
    foreign key( remote_id ) references ml_database( remote_id ),
    foreign key( script_id ) references ml_passthrough_script( script_id ) )
go

create table ml_passthrough_status (
    status_id			integer		not null identity,
    remote_id			varchar( 128 )	not null,
    run_order			integer		not null,
    script_id			integer		not null,
    script_status		char( 1 )	not null,
    error_code			integer		null,
    error_text			varchar(max)	null,
    remote_run_time		datetime	not null,
    primary key( status_id ),
    unique( remote_id, run_order, remote_run_time ),
    foreign key( remote_id ) references ml_database( remote_id ) )
go

create table ml_passthrough_repair (
    failed_script_id		integer		not null,
    error_code			integer		not null,
    new_script_id		integer		null,
    action			char( 1 )	not null,
    primary key( failed_script_id, error_code ),
    foreign key( failed_script_id ) references ml_passthrough_script( script_id ) )
go

create trigger ml_passthrough_trigger on ml_passthrough for update as
begin
    declare crsr cursor for select remote_id, run_order from inserted
    declare @rid   varchar( 128 )
    declare @run   integer

    open crsr
    while 1 = 1 begin
	fetch crsr into @rid, @run
	if @@FETCH_STATUS != 0 break
	update ml_passthrough set last_modified = getdate()
	    where remote_id = @rid and run_order = @run
    end
    close crsr
end
go

create procedure ml_add_passthrough_script
    @script_name		varchar( 128 ),
    @flags			varchar( 256 ),
    @affected_pubs		varchar(max),
    @script			varchar(max),
    @description		varchar( 2000 )
as
    declare @v_substr		varchar(256)
    declare @v_str		varchar(256)
    declare @v_start		integer
    declare @v_end		integer
    declare @v_done		integer
    declare @v_error		integer
    declare @v_msg		varchar(500)
    
    if @script_name is not null and @script is not null begin
	select @v_error = 0
	if @flags is not null begin
	    select @v_str = @flags
	    select @v_start = 1
	    select @v_done = 0
	    while 1 = 1 begin
		select @v_end = charindex( ';', @v_str )
		if @v_end = 0 begin
		    select @v_end = datalength( @flags ) + 1
		    select @v_done = 1
		end
		select @v_substr = substring( @v_str, @v_start, @v_end - @v_start ) 
		if @v_substr is not null and datalength( @v_substr ) <> 0 begin
		    if @v_substr not in ( 'manual', 'exclusive', 'schema_diff' ) begin
			select @v_msg = 'Invalid flag: "' + @v_substr + '"'
			raiserror( @v_msg, 15, -1 )
			select @v_error = 1
			break
		    end
		end
		if @v_done = 1 break
		select @v_str = substring( @v_str, @v_end + 1, datalength( @flags ) - @v_end ) 
	    end
	end
	if @v_error = 0 begin
	    if not exists ( select * from ml_passthrough_script where script_name = @script_name ) begin
		insert into ml_passthrough_script( script_name, flags, affected_pubs,
				      script, description )
		    values( @script_name, @flags, @affected_pubs,
			    @script, @description )
	    end 
	    else begin
		select @v_msg = 'The script name "' + @script_name + '" already exists in the ml_passthrough_script table.  Please choose another script name.'
		raiserror( @v_msg, 15, -1 )
	    end
	end
    end 
    else begin
	select @v_msg = 'Neither passthrough script name nor script content can be null.'
	raiserror( @v_msg, 15, -1 )
    end
go

create procedure ml_delete_passthrough_script
    @script_name		varchar( 128 )
as
    declare @script_id integer
    
    select @script_id = script_id from ml_passthrough_script where script_name = @script_name
    if @script_id is not null begin
        if not exists ( select * from ml_passthrough
			    where script_id = @script_id ) and
	   not exists ( select * from ml_passthrough_repair
			    where failed_script_id = @script_id ) and
	   not exists ( select * from ml_passthrough_repair
			    where new_script_id = @script_id ) begin
	    delete from ml_passthrough_script where script_id = @script_id
	end
    end
go

create procedure ml_add_passthrough
    @remote_id		varchar( 128 ),
    @script_name	varchar( 128 ),
    @run_order		integer
as
    declare @rid varchar( 128 )
    declare @order integer
    declare @name varchar( 128 )
    declare @script_id integer
    declare @msg varchar(500)
		
    select @script_id = script_id from ml_passthrough_script where script_name = @script_name
    if @script_id is not null begin
	if @run_order is not null and @run_order < 0 begin
	    select @msg = 'A negative value for run_order is not allowed'
	    raiserror( @msg, 15, -1 )
	end
	else begin
	    if @remote_id is null begin
		declare rid_crsr cursor for select remote_id from ml_database
		
		if @run_order is null
		    select @order = isnull( max( run_order ) + 10, 10 )
			from ml_passthrough
		else
		    select @order = @run_order
		open rid_crsr
		while 1 = 1 begin
		    fetch rid_crsr into @rid
		    if @@FETCH_STATUS != 0 break
		    insert into ml_passthrough( remote_id, run_order, script_id )
			values( @rid, @order, @script_id )
		end
		close rid_crsr
		deallocate rid_crsr
	    end
	    else begin
		if @run_order is null begin
		    select @order = isnull( max( run_order ) + 10, 10 )
			from ml_passthrough where remote_id = @remote_id
		    insert into ml_passthrough( remote_id, run_order, script_id )
			values( @remote_id, @order, @script_id )
		end
		else begin
		    if exists (select * from ml_passthrough
			       where remote_id = @remote_id and run_order = @run_order)
			update ml_passthrough set script_id = @script_id,
			    last_modified = getdate()
			    where remote_id = @remote_id and run_order = @run_order
		    else
			insert into ml_passthrough( remote_id, run_order, script_id )
			    values( @remote_id, @run_order, @script_id )
		end
	    end
	end
    end
    else begin
	select @msg = 'Passthrough script name: "' + @script_name +
			'" does not exist in the ml_passthrough_script table.'
	raiserror( @msg, 15, -1 )
    end 
go

create procedure ml_delete_passthrough
    @remote_id		varchar( 128 ),
    @script_name	varchar( 128 ),
    @run_order		integer
as
    if @remote_id is null begin
	if @run_order is null begin
	    delete from ml_passthrough
		where script_id in
		    (select script_id from ml_passthrough_script where script_name = @script_name)
	end
	else begin
	    delete from ml_passthrough
		where run_order = @run_order and script_id in
		    (select script_id from ml_passthrough_script where script_name = @script_name)
	end
    end 
    else begin
	if @run_order is null begin
	    delete from ml_passthrough
		where remote_id = @remote_id and script_id in
		    (select script_id from ml_passthrough_script where script_name = @script_name)
	end
	else begin
	    delete from ml_passthrough
		where remote_id = @remote_id and run_order = @run_order and script_id in
		    (select script_id from ml_passthrough_script where script_name = @script_name)
	end
    end
go

create procedure ml_add_passthrough_repair
    @failed_script_name	varchar( 128 ),
    @error_code		integer,
    @new_script_name	varchar( 128 ),
    @action		char( 1 ) 
as
    declare @failed_script_id integer
    declare @new_script_id integer
    declare @name varchar(128)
    declare @msg varchar(500)
    
    select @failed_script_id = script_id from ml_passthrough_script
	where script_name = @failed_script_name
    if @failed_script_id is not null begin
	if @action in ( 'R', 'S', 'P', 'H', 'r', 's', 'p', 'h' ) begin
	    if @action in ( 'R', 'r' ) and @new_script_name is null begin
		select @msg = 'The new_script_name cannot be null for action "' + @action + '".'
		raiserror( @msg, 15, -1 )
	    end
	    else if @action in ( 'S', 'P', 'H', 's', 'p', 'h' ) and @new_script_name is not null begin
		select @msg = 'The new_script_name should be null for action "' + @action + '".'
		raiserror( @msg, 15, -1 )
	    end
	    else if @new_script_name is not null and
		not exists ( select * from ml_passthrough_script
			       where script_name = @new_script_name ) begin
		select @msg = 'Invalid new_script_name: "' + @new_script_name + '".'
		raiserror( @msg, 15, -1 )
	    end
	    else begin
		select @new_script_id = script_id from ml_passthrough_script
		    where script_name = @new_script_name
		if exists ( select * from ml_passthrough_repair
			       where failed_script_id = @failed_script_id and
				     error_code = @error_code ) begin
		    update ml_passthrough_repair
			set new_script_id = @new_script_id, action = @action
			where failed_script_id = @failed_script_id and
			     error_code = @error_code
		end 
		else begin
		    insert into ml_passthrough_repair
			(failed_script_id, error_code, new_script_id, action)
			values( @failed_script_id, @error_code,
				@new_script_id, @action )
		end
	    end
	end
	else begin
	    set @msg = 'Invalid action: "' + @action + '".'
	    raiserror( @msg, 15, -1 )
	end
    end
    else begin
	set @msg = 'Invalid failed_script_name: "' + @failed_script_name + '".'
	raiserror( @msg, 15, -1 )
    end
go

create procedure ml_delete_passthrough_repair
    @failed_script_name	varchar( 128 ),
    @error_code		integer 
as
    if @error_code is null begin
	delete from ml_passthrough_repair
	    where failed_script_id =
		(select script_id from ml_passthrough_script where script_name = @failed_script_name)
    end
    else begin
	delete from ml_passthrough_repair
	    where failed_script_id =
		(select script_id from ml_passthrough_script where script_name = @failed_script_name) and
		error_code = @error_code
    end
go

create view ml_connection_scripts as
select ml_script_version.name version,
       ml_connection_script.event,
       ml_script.script_language,
       ml_script.script
from ml_connection_script,
     ml_script_version,
     ml_script
where ml_connection_script.version_id = ml_script_version.version_id
and ml_connection_script.script_id = ml_script.script_id
go

create view ml_table_scripts as
select ml_script_version.name version,
       ml_table_script.event,
       ml_table.name table_name,
       ml_script.script_language,
       ml_script.script
from ml_table_script,
     ml_script_version,
     ml_script,
     ml_table
where ml_table_script.version_id = ml_script_version.version_id
and ml_table_script.script_id = ml_script.script_id
and ml_table_script.table_id = ml_table.table_id
go

create view ml_columns as
select ml_script_version.name version,
      ml_table.name table_name,
      ml_column.name column_name,
      ml_column.type data_type,
      ml_column.idx column_order
from ml_script_version,
    ml_table,
    ml_column
where ml_column.version_id = ml_script_version.version_id
and ml_column.table_id = ml_table.table_id
go

create procedure ml_delete_remote_id
    @remote_id		varchar(128)
as
    declare @rid integer
    
    select @rid = rid from ml_database where remote_id = @remote_id
    if @rid is not null begin
	delete from ml_subscription where rid = @rid
	delete from ml_passthrough_status where remote_id = @remote_id
	delete from ml_passthrough where remote_id = @remote_id
	delete from ml_database where rid = @rid
    end
go

create procedure ml_delete_user_state
    @user	varchar( 128 )
as
begin
    declare @uid	integer
    declare @rid	integer
    declare @remote_id	varchar(128)
    
    set nocount on
    
    select @uid = user_id from ml_user where name = @user
    if @uid is not null begin
	declare	crsr cursor for select rid from ml_subscription
				    where user_id = @uid for read only
	open crsr
	fetch crsr into @rid
	while( @@FETCH_STATUS = 0 )
	begin
	    delete from ml_subscription where user_id = @uid and rid = @rid
	    if not exists (select * from ml_subscription where rid = @rid) begin
		select @remote_id = remote_id
		    from ml_database where rid = @rid
		exec ml_delete_remote_id @remote_id
	    end
	    fetch crsr into @rid
	end
	close crsr
	deallocate crsr
    end
end
go

create procedure ml_delete_user
    @user		varchar( 128 )
as
begin
    exec ml_delete_user_state @user
    delete from ml_user where name = @user
end
go

create procedure ml_delete_sync_state
    @user		varchar( 128 ),
    @remote_id		varchar( 128 )
as
begin
    declare @uid	integer
    declare @rid	integer
    
    set nocount on
    
    select @uid = user_id from ml_user where name = @user
    select @rid = rid from ml_database where remote_id = @remote_id
    
    if @user is not null and @remote_id is not null begin
	delete from ml_subscription where user_id = @uid and rid = @rid
	if not exists (select * from ml_subscription where rid = @rid)
	    exec ml_delete_remote_id @remote_id
    end
    else if @user is not null begin
	exec ml_delete_user_state @user
    end
    else if @remote_id is not null begin
	exec ml_delete_remote_id @remote_id
    end
end
go

create procedure ml_reset_sync_state
    @user		varchar( 128 ),
    @remote_id		varchar( 128 )
as
begin
    declare @uid	integer
    declare @rid	integer

    set nocount on
    
    select @uid = user_id from ml_user where name = @user
    select @rid = rid from ml_database where remote_id = @remote_id
    
    if @user is not null and @remote_id is not null begin
	update ml_subscription
	    set progress = 0,
		last_upload_time   = CONVERT( DATETIME, '01/01/1900', 101 ),
		last_download_time = CONVERT( DATETIME, '01/01/1900', 101 )
	    where user_id = @uid and rid = @rid
    end
    else if @user is not null begin 
	update ml_subscription
	    set progress = 0,
		last_upload_time   = CONVERT( DATETIME, '01/01/1900', 101 ),
		last_download_time = CONVERT( DATETIME, '01/01/1900', 101 )
	    where user_id = @uid
    end
    else if @remote_id is not null begin	
	update ml_subscription
	    set progress = 0,
		last_upload_time   = CONVERT( DATETIME, '01/01/1900', 101 ),
		last_download_time = CONVERT( DATETIME, '01/01/1900', 101 )
	    where rid = @rid
    end
    update ml_database
	set sync_key = NULL,
	    seq_id = NULL,
	    seq_uploaded = 0,
	    script_ldt = CONVERT( DATETIME, '01/01/1900', 101 )
	where remote_id = @remote_id
end
go

create procedure ml_delete_sync_state_before @ts datetime
as
begin
    declare @rid	integer
    declare @remote_id	varchar(128)
    
    set nocount on
    
    if @ts is not null begin
	declare	crsr cursor for select rid from ml_subscription
				    where last_upload_time < @ts and
					  last_download_time < @ts
				    for read only
	open crsr
	fetch crsr into @rid
	while( @@FETCH_STATUS = 0 )
	begin
	    delete from ml_subscription where rid = @rid and
					      last_upload_time < @ts and
					      last_download_time < @ts
	    if not exists (select * from ml_subscription where rid = @rid) begin
		select @remote_id = remote_id from ml_database where rid = @rid
		exec ml_delete_remote_id @remote_id
	    end
	    fetch crsr into @rid
	end
	close crsr
	deallocate crsr
    end
end
go

create procedure ml_add_lang_table_script_chk
    @version		varchar( 128 ),
    @table		varchar( 128 ),
    @event		varchar( 128 ),
    @script_language	varchar( 128 ),
    @script		varchar(max),
    @checksum		varchar( 64 )
as
begin
    declare @version_id 	integer;
    declare @table_id 		integer;
    declare @script_id 		integer;
    declare @upd_script_id	integer;
    
    set nocount on
    
    select @version_id = version_id from ml_script_version 
        where name = @version;
    select @table_id = table_id from ml_table where name = @table;
    if @script is not null begin
	if @version_id is null begin
	    -- Insert to the ml_script_version
	    select @version_id = max( version_id )+1 from ml_script_version;
	    if @version_id is null begin
		-- No rows are currently in ml_script_version
		set @version_id = 1;
	    end;
	    insert into ml_script_version ( version_id, name )
		    values ( @version_id, @version );
	end;
	
	if @table_id is null begin
	    -- Insert to the ml_table
	    select @table_id = max( table_id )+1 from ml_table;
	    if @table_id is null begin
		-- No rows are currently in ml_table
		set @table_id = 1;			
	    end;
	    insert into ml_table ( table_id, name ) 
	        values ( @table_id, @table );
	end;
	
	select @script_id = max( script_id )+1 from ml_script;
	if @script_id is null begin
	    -- No rows are currently in ml_script
	    set @script_id = 1;
	end;
	insert into ml_script ( script_id, script_language, script, checksum ) 
    		values ( @script_id, @script_language, @script, @checksum );
	select @upd_script_id = script_id from ml_table_script where version_id = @version_id and table_id = @table_id and event = @event
	if @upd_script_id is null begin
	    insert into ml_table_script ( version_id, table_id, event, script_id ) 
		values ( @version_id, @table_id, @event, @script_id );
	    end;
	else begin
	    update ml_table_script set script_id = @script_id where version_id = @version_id and table_id = @table_id and event = @event
	end;
	end;
    else begin
	if @version_id is not null and @table_id is not null begin
	    delete from ml_table_script where version_id = @version_id 
	        and table_id = @table_id and event = @event;
	end;
    end;
end
go

create procedure ml_add_lang_table_script
    @version		varchar( 128 ),
    @table		varchar( 128 ),
    @event		varchar( 128 ),
    @script_language	varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_table_script_chk @version, @table, @event, @script_language, @script, null
end
go

create procedure ml_add_table_script
    @version		varchar( 128 ),
    @table		varchar( 128 ),
    @event		varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_table_script @version, @table, @event, 'sql', @script
end
go

create procedure ml_add_java_table_script
    @version		varchar( 128 ),
    @table		varchar( 128 ),
    @event		varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_table_script @version, @table, @event, 'java', @script
end
go

create procedure ml_add_dnet_table_script
    @version		varchar( 128 ),
    @table		varchar( 128 ),
    @event		varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_table_script @version, @table, @event, 'dnet', @script
end
go

create procedure ml_add_lang_conn_script_chk
    @version		varchar( 128 ),
    @event		varchar( 128 ),
    @script_language	varchar( 128 ),
    @script		varchar(max),
    @checksum		varchar( 64 )
as
begin
    declare @version_id 	integer;
    declare @script_id 		integer;
    declare @upd_script_id	integer;
    
    set nocount on

    select @version_id = version_id from ml_script_version 
        where name = @version;
    if @script is not null begin
	if @version_id is null begin
	    -- Insert to the ml_script_version
	    select @version_id = max( version_id )+1 from ml_script_version;
	    if @version_id is null begin
		-- No rows are currently in ml_script_version
		set @version_id = 1;
	    end;
	    insert into ml_script_version ( version_id, name ) 
		    values ( @version_id, @version );
	end;
    
	select @script_id = max( script_id )+1 from ml_script;
	if @script_id is null begin
	    -- No rows are currently in ml_script
	    set @script_id = 1;
	end;
	insert into ml_script ( script_id, script_language, script, checksum ) 
    		values ( @script_id, @script_language, @script, @checksum );
	select @upd_script_id = script_id from ml_connection_script 
	    where version_id = @version_id and event = @event
	if @upd_script_id is null begin
	    insert into ml_connection_script ( version_id, event, script_id ) 
    		values ( @version_id, @event, @script_id );
	end;
	else begin
	    update ml_connection_script set script_id = @script_id 
	        where version_id = @version_id and event = @event
	end;
    end;
    else begin
	if @version_id is not null begin
	    delete from ml_connection_script 
	        where version_id = @version_id and event = @event;
	end;
    end;
end
go

create procedure ml_add_lang_connection_script
    @version		varchar( 128 ),
    @event		varchar( 128 ),
    @script_language	varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_conn_script_chk @version, @event, @script_language, @script, null
end
go

create procedure ml_add_connection_script
    @version		varchar( 128 ),
    @event		varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_connection_script @version, @event, 'sql', @script
end
go

create procedure ml_add_java_connection_script
    @version		varchar( 128 ),
    @event		varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_connection_script @version, @event, 'java', @script
end
go

create procedure ml_add_dnet_connection_script
    @version		varchar( 128 ),
    @event		varchar( 128 ),
    @script		varchar(max)
as
begin
    exec ml_add_lang_connection_script @version, @event, 'dnet', @script
end
go

create procedure ml_add_property
    @comp_name	    varchar( 128 ),
    @prop_set_name  varchar( 128 ),
    @prop_name	    varchar( 128 ),
    @prop_value	    varchar(max)
as
begin
    declare 	@p_name 	varchar( 128 );

    if @prop_value is null begin
	delete from ml_property where component_name  = @comp_name
				and property_set_name = @prop_set_name
				and property_name     = @prop_name;
        end;
    else begin
	select @p_name = property_name from ml_property 
				    where component_name  = @comp_name
				    and property_set_name = @prop_set_name
				    and property_name	  = @prop_name;
	if @p_name is null begin
	    insert into ml_property
		(component_name,property_set_name,property_name,property_value)
		values (@comp_name, @prop_set_name, @prop_name, @prop_value );
	    end;
	else begin
	    update ml_property set property_value = @prop_value
				    where component_name  = @comp_name
				    and property_set_name = @prop_set_name
				    and property_name	  = @prop_name;
	end;
    end;
end
go

create procedure ml_add_column 
    @version	varchar( 128 ),
    @table	varchar( 128 ),
    @column	varchar( 128 ),
    @type	varchar( 128 )
as
begin
    declare 	@version_id	integer
    declare 	@table_id	integer
    declare 	@idx		integer
    
    select @version_id = version_id from ml_script_version 
        where name = @version;
    select @table_id = table_id from ml_table where name = @table;
    if @column is not null begin
    	if @version_id is null begin
	    -- Insert to the ml_script_version table
	    select @version_id = max( version_id )+1 from ml_script_version;
	    if @version_id is null begin
	    	-- No rows are currently in ml_script_version
	    	select @version_id = 1;
	    end;
	    insert into ml_script_version ( version_id, name ) 
		    values ( @version_id, @version );
    	end;

    	if @table_id is null begin
	    -- Insert to the ml_table table
	    select @table_id = max( table_id )+1 from ml_table;
	    if @table_id is null begin
	    	-- No rows are currently in ml_table
	    	select @table_id = 1;
	    end;		
	    insert into ml_table ( table_id, name ) 
	        values ( @table_id, @table );
    	end;
    
	select @idx = max( idx )+1 from ml_column
	    where version_id = @version_id and table_id = @table_id;
	if @idx is null begin
	    select @idx = 1;
	end
	insert into ml_column ( version_id, table_id, idx, name, type ) 
	    values ( @version_id, @table_id, @idx, @column, @type );
    end;
    else begin
	if @version_id is not null and @table_id is not null begin
	    delete from ml_column 
	        where version_id = @version_id and table_id = @table_id;
	end;
    end;
end
go

create procedure ml_share_all_scripts 
    @version		varchar( 128 ),
    @other_version	varchar( 128 ) 
as
begin
    declare @version_id		integer
    declare @other_version_id	integer
    
    select @version_id = version_id from ml_script_version 
		where name = @version
    select @other_version_id = version_id from ml_script_version 
		where name = @other_version

    if @version_id is null begin
	-- Insert to the ml_script_version
	select @version_id = max( version_id )+1 from ml_script_version;
	if @version_id is null begin
	    -- No rows are currently in ml_script_version
	    set @version_id = 1;
	end;
	insert into ml_script_version ( version_id, name ) 
		values ( @version_id, @version );
    end;

    insert into ml_table_script( version_id, table_id, event, script_id )
	select @version_id, table_id, event, script_id from ml_table_script 
	    where version_id = @other_version_id
    
    insert into ml_connection_script( version_id, event, script_id )
	select @version_id, event, script_id from ml_connection_script 
	    where version_id = @other_version_id
end
go

create procedure ml_add_missing_dnld_scripts
    @script_version	varchar( 128 )
as
    declare @version_id	integer
    declare @table_id	integer
    declare @count_1	integer
    declare @count_2	integer
    declare @table_name	varchar(128)
    declare @first	integer
    declare @tid	integer
    
    select @version_id = version_id from ml_script_version
	where name = @script_version
    if @version_id is not null begin
	declare crsr cursor for
	    select t.table_id from ml_table_script t, ml_script_version v
		where t.version_id = v.version_id and
		      v.name = @script_version order by 1
	select @first = 1
	open crsr
	while 1 = 1 begin
	    fetch crsr into @table_id;
	    if @@FETCH_STATUS != 0 break
	    if @first = 1 or @table_id <> @tid begin
		if not exists (select * from ml_table_script
				where version_id = @version_id and
				    table_id = @table_id and
				    event = 'download_cursor')
		    select @count_1 = 0
		else
		    select @count_1 = 1
		if not exists (select * from ml_table_script
				where version_id = @version_id and
				    table_id = @table_id and
				    event = 'download_delete_cursor')
		    select @count_2 = 0
		else
		    select @count_2 = 1
		if @count_1 = 0 or @count_2 = 0 begin
		    select @table_name = name from ml_table where table_id = @table_id;
		    if @count_1 = 0
			exec ml_add_table_script @script_version, @table_name,
			    'download_cursor', '--{ml_ignore}' 
		    if @count_2 = 0
			exec ml_add_table_script @script_version, @table_name,
			    'download_delete_cursor', '--{ml_ignore}'
		end
		select @first = 0
		select @tid = @table_id
	    end
	end
	close crsr
	deallocate crsr
    end
go

-- Create a stored procedure to get the connections
-- that are currently blocking the connections given
-- by @@spids for more than @block_time seconds

create procedure ml_get_blocked_info
    @spids		varchar(2000),
    @block_time		integer
as
    declare @sql	varchar( 2000 );

    select @sql = 'select p.spid, p.blocked, p.waittime/1000, 1, object_schema_name(t.object_id) + ''.'' + object_name(t.object_id)' +
	' from sys.dm_tran_locks l, sys.partitions t, sys.sysprocesses p' +
	' where p.blocked > 0 and l.resource_associated_entity_id = t.hobt_id and' +
	' p.spid = l.request_session_id and upper(request_status) = ''WAIT'' and' +
	' p.spid in ( ' + @spids + ' ) and p.waittime/1000 > ' + convert(varchar(10),@block_time) + 
	' order by 1';

    exec( @sql );
go

create procedure ml_lock_rid
    @rid	integer,
    @sync_key	varchar( 40 )	OUTPUT,
    @failure	integer		OUTPUT
as
begin
    begin try
	SET @failure = 0
	SET LOCK_TIMEOUT 0
	SELECT @sync_key = sync_key FROM ml_database WITH (XLOCK) WHERE rid = @rid
	SET LOCK_TIMEOUT -1
    end try
    begin catch
	SET LOCK_TIMEOUT -1
	SET @failure = 1
    end catch
end
go

create procedure ml_add_ldap_server 
    @ldsrv_name		varchar( 128 ),
    @search_url    	varchar( 1024 ),
    @access_dn    	varchar( 1024 ),
    @access_dn_pwd	varchar( 256 ),
    @auth_url		varchar( 1024 ),
    @conn_retries	tinyint,
    @conn_timeout	tinyint,
    @start_tls		tinyint 
as
    declare @sh_url	varchar( 1024 )
    declare @as_dn	varchar( 1024 )
    declare @as_pwd	varchar( 256 )
    declare @au_url	varchar( 1024 )
    declare @timeout	tinyint
    declare @retries	tinyint
    declare @tls	tinyint
    
    set nocount on
    
    if @ldsrv_name is not null begin
	if @search_url is null and
	    @access_dn is null and
	    @access_dn_pwd is null and
	    @auth_url is null and
	    @conn_timeout is null and
	    @conn_retries is null and
	    @start_tls is null begin
	    
	    -- delete the server if not used
	    if not exists (select s.ldsrv_id from ml_ldap_server s,
				ml_user_auth_policy p
			    where ( s.ldsrv_id = p.primary_ldsrv_id or
				    s.ldsrv_id = p.secondary_ldsrv_id ) and
				    s.ldsrv_name = @ldsrv_name ) begin
		delete from ml_ldap_server where ldsrv_name = @ldsrv_name 
	    end
	end
	else begin
	    if not exists ( select * from ml_ldap_server where
				ldsrv_name = @ldsrv_name ) begin
		-- add a new ldap server
		if @conn_timeout is null
		    select @timeout = 10
		else
		    select @timeout = @conn_timeout
		if @conn_retries is null
		    select @retries = 3
		else
		    select @retries = @conn_retries
		if @start_tls is null
		    select @tls = 0
		else
		    select @tls = @start_tls
		
		insert into ml_ldap_server ( ldsrv_name, search_url,
			access_dn, access_dn_pwd, auth_url,
			timeout, num_retries, start_tls )
		values( @ldsrv_name, @search_url,
			@access_dn, @access_dn_pwd,
			@auth_url, @timeout, @retries, @tls )
	    end
	    else begin
		-- update the ldap server info
		select @sh_url = search_url,
			@as_dn = access_dn,
			@as_pwd = access_dn_pwd,
			@au_url = auth_url,
			@timeout = timeout,
			@retries = num_retries,
			@tls = start_tls
		    from ml_ldap_server where ldsrv_name = @ldsrv_name
		    
		if @search_url is not null
		    select @sh_url = @search_url
		if @access_dn is not null
		    select @as_dn = @access_dn
		if @access_dn_pwd is not null
		    select @as_pwd = @access_dn_pwd
		if @auth_url is not null
		    select @au_url = @auth_url
		if @conn_timeout is not null
		    select @timeout = @conn_timeout
		if @conn_retries is not null
		    select @retries = @conn_retries
		if @start_tls is not null
		    select @tls = @start_tls
		    
		update ml_ldap_server set
			search_url = @sh_url,
			access_dn = @as_dn,
			access_dn_pwd = @as_pwd,
			auth_url = @au_url,
			timeout = @timeout,
			num_retries = @retries,
			start_tls = @tls
		where ldsrv_name = @ldsrv_name
	    end
	end
    end
go

create procedure ml_add_certificates_file 
    @file_name		varchar( 1024 ) 
as
    set nocount on
    
    if @file_name is not null begin
	delete from ml_trusted_certificates_file
	insert into ml_trusted_certificates_file ( file_name ) values( @file_name )
    end
go

create procedure ml_add_user_auth_policy
    @policy_name		varchar( 128 ),
    @primary_ldsrv_name		varchar( 128 ),
    @secondary_ldsrv_name	varchar( 128 ),
    @ldap_auto_failback_period	integer,
    @ldap_failover_to_std	integer 
as
    declare @pldsrv_id	integer
    declare @sldsrv_id	integer
    declare @pid	integer
    declare @sid	integer
    declare @period	integer
    declare @failover	integer
    declare @error	integer
    declare @msg	varchar( 1024 )
    
    set nocount on
    
    if @policy_name is not null begin
	if @primary_ldsrv_name is null and 
	    @secondary_ldsrv_name is null and 
	    @ldap_auto_failback_period is null and 
	    @ldap_failover_to_std is null begin
	    
	    -- delete the policy name if not used
	    if not exists ( select p.policy_id from ml_user u,
				ml_user_auth_policy p where
				    u.policy_id = p.policy_id and
				    policy_name = @policy_name ) begin
		delete from ml_user_auth_policy
		    where policy_name = @policy_name
	    end
	end
	else if @primary_ldsrv_name is null begin
	    -- error
	    select @msg = 'The primary LDAP server cannot be NULL.'
	    raiserror( @msg, 15, -1 )
	end
	else begin
	    select @error = 0
	    if @primary_ldsrv_name is not null begin
		select @pldsrv_id = ldsrv_id from ml_ldap_server where
		    ldsrv_name = @primary_ldsrv_name
		if @pldsrv_id is null begin
		    select @error = 1
		    select @msg = 'Primary LDAP server "' + @primary_ldsrv_name + '" is not defined.'
		    raiserror( @msg, 15, -1 )
		end
	    end
	    else begin
		select @pldsrv_id = null
	    end
	    if @secondary_ldsrv_name is not null begin
		select @sldsrv_id = ldsrv_id from ml_ldap_server where
		    ldsrv_name = @secondary_ldsrv_name
		if @sldsrv_id is null begin
		    select @error = 1
		    select @msg = 'Secondary LDAP server "' + @secondary_ldsrv_name + '" is not defined.'
		    raiserror( @msg, 15, -1 )
		end
	    end
	    else begin
		select @sldsrv_id = null
	    end
	    if @error = 0 begin
		if not exists ( select * from ml_user_auth_policy
				where policy_name = @policy_name ) begin
		    if @ldap_auto_failback_period is null
			select @period = 900
		    else
			select @period = @ldap_auto_failback_period
		    if @ldap_failover_to_std is null
			select @failover = 1
		    else
			select @failover = @ldap_failover_to_std
		    
		    -- add a new user auth policy
		    insert into ml_user_auth_policy
			( policy_name, primary_ldsrv_id, secondary_ldsrv_id,
			  ldap_auto_failback_period, ldap_failover_to_std )
			values( @policy_name, @pldsrv_id, @sldsrv_id,
				@period, @failover )
		end 
		else begin
		    select @pid = primary_ldsrv_id,
			   @sid = secondary_ldsrv_id,
			   @period = ldap_auto_failback_period,
			   @failover = ldap_failover_to_std
			from ml_user_auth_policy where policy_name = @policy_name
    
		    if @pldsrv_id is not null
			select @pid = @pldsrv_id
		    if @sldsrv_id is not null
			select @sid = @sldsrv_id
		    if @ldap_auto_failback_period is not null
			select @period = @ldap_auto_failback_period
		    if @ldap_failover_to_std is not null
			select @failover = @ldap_failover_to_std

		    -- update the user auth policy
		    update ml_user_auth_policy set
				primary_ldsrv_id = @pid,
				secondary_ldsrv_id = @sid,
				ldap_auto_failback_period = @period,
				ldap_failover_to_std = @failover
			where policy_name = @policy_name
		end
	    end
	end
    end
go

create procedure ml_add_user
    @user		varchar( 128 ),
    @password		binary( 32 ),
    @policy_name	varchar( 128 ) 
as
    declare @user_id	integer
    declare @policy_id	integer
    declare @error	integer
    declare @msg	varchar( 1024 )
    
    set nocount on
    
    if @user is not null begin
	select @error = 0
	if @policy_name is not null begin
	    select @policy_id = policy_id from ml_user_auth_policy
		where policy_name = @policy_name
	    if @policy_id is null begin
		select @msg = 'Unable to find the user authentication policy: "' +
			    @policy_name + '"'
		raiserror( @msg, 15, -1 )
		select @error = 1
	    end
	end
	else begin 
	    select @policy_id = null
	end
	if @error = 0 begin
	    select @user_id = user_id from ml_user where name = @user
	    if @user_id is null
		insert into ml_user ( name, hashed_password, policy_id )
		    values ( @user, @password, @policy_id )
	    else
		update ml_user set hashed_password = @password,
				    policy_id = @policy_id
		    where user_id = @user_id
	end
    end
go

create table ml_device (
    device_name		varchar( 255 )	not null,
    listener_version	varchar( 128 )	not null,
    listener_protocol	integer		not null,
    info		varchar( 255 )	not null,
    ignore_tracking	varchar( 1 )	not null,
    source		varchar( 255 )	not null,
    primary key( device_name )
)
go

create table ml_device_address (
    device_name		varchar( 255 )	not null,
    medium		varchar( 255 )	not null,
    address		varchar( 255 )	not null,
    active		varchar( 1 )	not null,
    last_modified	datetime	not null default GETDATE(),
    ignore_tracking	varchar( 1 )	not null,
    source		varchar( 255 )	not null,
    primary key( device_name, medium ),
    foreign key(device_name) references ml_device ( device_name )
)
go

create table ml_listening (
    name		varchar( 128 )	not null,
    device_name		varchar( 255 )	not null,
    listening		varchar( 1 )	not null,
    ignore_tracking	varchar( 1 )	not null,
    source		varchar( 255 )	not null,
    primary key( name ),
    foreign key( device_name ) references ml_device ( device_name )
)
go

create table ml_sis_sync_state (
    remote_id		varchar( 128 ) not null,
    subscription_id	varchar( 128 ) not null,
    publication_name	varchar( 128 ) not null,
    user_name		varchar( 128 ) not null,
    last_upload		datetime not null,
    last_download	datetime not null,
    primary key( remote_id, subscription_id )
)
go

create procedure ml_set_device
    @device		varchar( 255 ),
    @listener_version	varchar( 128 ),
    @listener_protocol	integer,
    @info		varchar( 255 ),
    @ignore_tracking	varchar( 1 ),
    @source		varchar( 255 )
as
begin
    declare @d		varchar( 255 );

    select @d = device_name from ml_device where device_name = @device;
    if @d is null begin
	insert into ml_device( device_name, listener_version, listener_protocol, info, ignore_tracking, source )
		values( @device, @listener_version, @listener_protocol, @info, @ignore_tracking, @source );
        end;
    else begin
        if @source = 'tracking' begin
	    update ml_device 
		set listener_version = @listener_version,
		    listener_protocol = @listener_protocol,
		    info = @info,
		    ignore_tracking = @ignore_tracking,
		    source = @source
		where device_name = @device and ignore_tracking = 'n';
	    end;
	else begin
	    update ml_device 
		set listener_version = @listener_version,
		    listener_protocol = @listener_protocol,
		    info = @info,
		    ignore_tracking = @ignore_tracking,
		    source = @source
		where device_name = @device;
	end;
    end;
end
go

create procedure ml_set_device_address
    @device		varchar( 255 ),
    @medium		varchar( 255 ),
    @address		varchar( 255 ),
    @active		varchar( 1 ),
    @ignore_tracking	varchar( 1 ),
    @source		varchar( 255 )
as
begin
    declare @d		varchar( 255 );

    select @d = device_name from ml_device_address
		where device_name = @device and medium = @medium;
    if @d is null begin
	insert into ml_device_address( device_name, medium, address, active, ignore_tracking, source )
		values( @device, @medium, @address, @active, @ignore_tracking, @source );
        end
    else begin
	if @source = 'tracking' begin
	    update ml_device_address 
		set address = @address,
		    active = @active,
		    ignore_tracking = @ignore_tracking,
		    source = @source
		where device_name = @device and medium = @medium and ignore_tracking = 'n';
	    end;
	else begin
	    update ml_device_address 
		set address = @address,
		    active = @active,
		    ignore_tracking = @ignore_tracking,
		    source = @source
		where device_name = @device and medium = @medium;
	end;
    end;
end
go

create procedure ml_upload_update_device_address
    @address		varchar( 255 ),
    @active		varchar( 1 ),
    @device		varchar( 255 ),
    @medium		varchar( 255 )
as
begin
    exec ml_set_device_address @device, @medium, @address, @active, 'n', 'tracking';
end
go

create procedure ml_set_listening
    @name		varchar( 128 ),
    @device		varchar( 255 ),
    @listening		varchar( 1 ),
    @ignore_tracking	varchar( 1 ),
    @source		varchar( 255 )
as
begin
    declare @d varchar( 255 );

    select @d = device_name from ml_listening where name = @name;
    if @d is null begin
	insert into ml_listening( name, device_name, listening, ignore_tracking, source )
	    values( @name, @device, @listening, @ignore_tracking, @source );
        end;
    else begin
        if @source = 'tracking' begin
	    update ml_listening
		set device_name = @device,
		    listening = @listening,
		    ignore_tracking = @ignore_tracking,
		    source = @source
		where name = @name and ignore_tracking = 'n';
	    end;
	else begin
	    update ml_listening
		set device_name = @device,
		    listening = @listening,
		    ignore_tracking = @ignore_tracking,
		    source = @source
		where name = @name;
	end;
    end;
end
go

create procedure ml_set_sis_sync_state
    @remote_id		varchar( 128 ),
    @subscription_id	varchar( 128 ),
    @publication_name	varchar( 128 ),
    @user_name		varchar( 128 ),
    @last_upload	datetime,
    @last_download	datetime
as
begin
    declare @r varchar( 128 );
    declare @sid varchar( 128 );
    declare @lut datetime;
       
    if @subscription_id IS NULL begin 
	SET @sid = 's:' + @publication_name;
	end;
    else begin
	SET @sid = @subscription_id;
	end;
				    
    if @last_upload IS NULL begin
	SET @lut = ( SELECT last_upload FROM ml_sis_sync_state
	    WHERE remote_id = @remote_id and subscription_id = @sid );
	if @lut IS NULL begin
	    SET @lut = '1900-01-01 00:00:00.000';
	    end;
	end;
    else begin
	SET @lut = @last_upload;
	end;
    
    select @r = remote_id from ml_sis_sync_state
	    where remote_id = @remote_id
	    and subscription_id = @sid;
    if @r is null begin
	insert into ml_sis_sync_state(remote_id, subscription_id, publication_name, user_name, last_upload, last_download )
	    values( @remote_id, @sid, @publication_name, @user_name, @lut, @last_download );
	end;
    else begin
	update ml_sis_sync_state
	    set publication_name = @publication_name,
		user_name = @user_name,
		last_upload = @lut,
		last_download = @last_download
	    where remote_id = @remote_id
		and subscription_id = @sid;
    end;
end
go

create procedure ml_upload_update_listening
    @device		varchar( 255 ),
    @listening		varchar( 1 ),
    @name		varchar( 128 )
as
begin
    exec ml_set_listening @name, @device, @listening, 'n', 'tracking';
end
go

create procedure ml_delete_device_address
    @device		varchar( 255 ),
    @medium		varchar( 255 )
as
begin
    delete from ml_device_address where device_name = @device and medium = @medium;
end
go

create procedure ml_delete_listening
    @name		varchar( 128 )
as
begin
    delete from ml_listening where name = @name;
end
go

create procedure ml_delete_device 
    @device		varchar( 255 )
as
begin
    delete from ml_device_address where device_name = @device;
    delete from ml_listening where device_name = @device;
    delete from ml_device where device_name = @device;
end
go


exec ml_add_property 'SIS', 'DeviceTracker(Default-DeviceTracker)', 'enable', 'yes'
exec ml_add_property 'SIS', 'DeviceTracker(Default-DeviceTracker)', 'smtp_gateway', 'Default-SMTP'
exec ml_add_property 'SIS', 'DeviceTracker(Default-DeviceTracker)', 'udp_gateway', 'Default-UDP'
exec ml_add_property 'SIS', 'DeviceTracker(Default-DeviceTracker)', 'sync_gateway', 'Default-SYNC'
exec ml_add_property 'SIS', 'SMTP(Default-SMTP)', 'enable', 'yes'
exec ml_add_property 'SIS', 'UDP(Default-UDP)', 'enable', 'yes'
exec ml_add_property 'SIS', 'SYNC(Default-SYNC)', 'enable', 'yes'
exec ml_add_property 'SIS', 'BESHTTP(BES_HTTP)', 'enable', 'yes'
exec ml_add_property 'SIS', 'BESHTTP(BES_HTTP)', 'bes', 'localhost'
exec ml_add_property 'SIS', 'BESHTTP(BES_HTTP)', 'port', '8080'
exec ml_add_property 'SIS', 'BESHTTP(BES_HTTP)', 'client_port', '4400'
exec ml_add_property 'SIS', 'Carrier(Rogers)', 'enable', 'yes'
exec ml_add_property 'SIS', 'Carrier(Rogers)', 'network_provider_id', 'ROGERS'
exec ml_add_property 'SIS', 'Carrier(Rogers)', 'sms_email_user_prefix', '1'
exec ml_add_property 'SIS', 'Carrier(Rogers)', 'sms_email_domain', 'pcs.rogers.com'
exec ml_add_property 'SIS', 'Carrier(Bell Mobility)', 'enable', 'yes'
exec ml_add_property 'SIS', 'Carrier(Bell Mobility)', 'network_provider_id', 'BELL'
exec ml_add_property 'SIS', 'Carrier(Bell Mobility)', 'sms_email_domain', 'txt.bellmobility.ca'
exec ml_add_property 'SIS', 'Carrier(Bell Mobility 1x)', 'enable', 'yes'
exec ml_add_property 'SIS', 'Carrier(Bell Mobility 1x)', 'network_provider_id', 'CDMA1x:16420:65535'
exec ml_add_property 'SIS', 'Carrier(Bell Mobility 1x)', 'sms_email_domain', 'txt.bellmobility.ca'
go


---------------------------------------------------
--   Schema for ML Remote administration
---------------------------------------------------

create table ml_ra_schema_name (
    schema_name                     varchar( 128 ) not null,
    remote_type                    varchar(1) not null,
    last_modified                  datetime not null,
    description		           varchar( 2048 ) null,
    primary key( schema_name ) 
)
go

create table ml_ra_agent (
    aid                            integer not null identity,
    agent_id                       varchar( 128 ) not null unique,
    taskdb_rid                     integer null,
    primary key( aid ),
    foreign key( taskdb_rid ) references ml_database ( rid )
)
go
create index tdb_rid on ml_ra_agent( taskdb_rid ) 
go

create table ml_ra_task (
    task_id                        bigint not null identity,
    task_name                      varchar( 128 ) not null unique,
    schema_name			   varchar( 128 ) null,
    max_running_time               integer null,
    max_number_of_attempts         integer null,
    delay_between_attempts         integer null,
    flags                          bigint not null,
    cond                           varchar( max ) null,
    remote_event                   varchar( max ) null,
    random_delay_interval	   integer not null default 0,
    primary key( task_id ), 
    foreign key( schema_name ) references ml_ra_schema_name( schema_name )
)
go

create table ml_ra_deployed_task (
    task_instance_id               bigint not null identity,
    aid                            integer not null,
    task_id                        bigint not null,
    assignment_time                datetime not null default getdate(),
    state                          varchar( 4 ) not null default 'P',
    previous_exec_count            bigint not null default 0,
    previous_error_count           bigint not null default 0,
    previous_attempt_count         bigint not null default 0,
    reported_exec_count            bigint not null default 0,
    reported_error_count           bigint not null default 0,
    reported_attempt_count         bigint not null default 0,
    last_modified                  datetime not null,
    unique( aid, task_id ),
    primary key( task_instance_id ), 
    foreign key( aid ) references ml_ra_agent( aid ),
    foreign key( task_id ) references ml_ra_task( task_id )
)
go
create index dt_tid_idx on ml_ra_deployed_task( task_id )
go

create table ml_ra_task_command (
    task_id                        bigint not null,
    command_number                 integer not null,
    flags                          bigint not null default 0,
    action_type                    varchar( 4 ) not null,
    action_parm                    varchar( max ) not null,
    primary key( task_id, command_number ),
    foreign key( task_id ) references ml_ra_task( task_id )
)
go

create table ml_ra_event (
    event_id                       bigint not null identity,
    event_class                    varchar( 4 ) not null,
    event_type                     varchar( 8 ) not null,
    aid				   integer null,
    task_id			   bigint null,
    command_number                 integer null,
    run_number                     bigint null,
    duration                       integer null,
    event_time                     datetime not null,
    event_received                 datetime not null default getdate(),
    result_code                    bigint null,
    result_text                    varchar( max ) null,
    primary key( event_id ) 
)
go
create index ev_tn_idx on ml_ra_event( task_id )
go
create index ev_time_idx on ml_ra_event( event_received )
go
create index ev_agent_idx on ml_ra_event( aid )
go

create table ml_ra_event_staging (
    taskdb_rid			   integer not null,
    remote_event_id                bigint not null,
    event_class                    varchar( 4 ) not null,
    event_type                     varchar( 8 ) not null,
    task_instance_id               bigint null,
    command_number                 integer null,
    run_number                     bigint null,
    duration                       integer null,
    event_time                     datetime not null,
    result_code                    bigint null,
    result_text                    varchar( max ) null,
    primary key( taskdb_rid, remote_event_id ) 
)
go
create index evs_type_idx on ml_ra_event_staging( event_type )
go

create table ml_ra_notify (
    agent_poll_key                 varchar( 128 ) not null,
    task_instance_id               bigint not null,
    last_modified                  datetime not null,
    primary key( agent_poll_key, task_instance_id ),
    foreign key( agent_poll_key ) references ml_ra_agent( agent_id )
)
go

create table ml_ra_task_property (
    task_id                        bigint not null,
    property_name                  varchar( 128 ) not null,
    last_modified                  datetime not null,
    property_value                 varchar( max ) null,
    primary key( property_name, task_id ), 
    foreign key( task_id ) references ml_ra_task( task_id )
)
go

create table ml_ra_task_command_property (
    task_id                        bigint not null,
    command_number                 integer not null,
    property_name                  varchar( 128 ) not null,
    property_value                 varchar( 2048 ) null,
    last_modified                  datetime not null,
    primary key( task_id, command_number, property_name ), 
    foreign key( task_id, command_number ) references ml_ra_task_command( task_id, command_number )
)
go

create table ml_ra_managed_remote (
    mrid			   integer not null identity,
    remote_id                      varchar(128) null,
    aid                            integer not null,
    schema_name			   varchar( 128 ) not null,
    conn_str		           varchar( 2048 ) not null,
    last_modified                  datetime not null,
    unique( aid, schema_name ),
    primary key( mrid ),
    foreign key( aid ) references ml_ra_agent( aid ),
    foreign key( schema_name ) references ml_ra_schema_name( schema_name )
)
go

create table ml_ra_agent_property (
    aid                            integer not null,
    property_name                  varchar( 128 ) not null,
    property_value                 varchar( 2048 ) null,
    last_modified                  datetime not null,
    primary key( aid, property_name ),
    foreign key( aid ) references ml_ra_agent( aid )
)
go

create table ml_ra_agent_staging (
    taskdb_rid			   integer not null,
    property_name                  varchar( 128 ) not null,
    property_value                 varchar( 2048 ) null,
    primary key( taskdb_rid, property_name ) 
)
go

-----------------------------------------------------------------
-- Stored procedures for Tasks
-----------------------------------------------------------------

-- Assign a remote task to a specific agent.

create procedure ml_ra_assign_task
    @agent_id	varchar( 128 ),  
    @task_name	varchar( 128 )
as
begin
    declare @task_id		bigint
    declare @task_instance_id	bigint
    declare @old_state		varchar( 4 )
    declare @aid		integer
    declare @rid		integer

    set nocount on

    select @task_id = task_id
	from ml_ra_task where task_name = @task_name
    if @task_id is null begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'bad task name'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
	return
    end

    select @aid = aid from ml_ra_agent where agent_id = @agent_id
    if @aid is null begin 
	exec sp_addmessage @msgnum = 99002, @severity = 15, @msgtext = 'bad agent id'
	raiserror( 99002, 15, 1 )
	exec sp_dropmessage @msgnum = 99002
	return
    end

    select @old_state = state, @task_instance_id = task_instance_id
	from ml_ra_deployed_task where task_id = @task_id and aid = @aid
    if @task_instance_id is null begin
	insert into ml_ra_deployed_task( aid, task_id, last_modified ) 
	    values ( @aid, @task_id, getdate() )
    end
    else if @old_state != 'A' and @old_state != 'P' begin
	-- Re-activate the task
	update ml_ra_deployed_task 
	    set state = 'P',
	    previous_exec_count = reported_exec_count + previous_exec_count,
	    previous_error_count = reported_error_count + previous_error_count,
	    previous_attempt_count = reported_attempt_count + previous_attempt_count,
	    reported_exec_count = 0,
	    reported_error_count = 0,
	    reported_attempt_count = 0,
	    last_modified = getdate()
	where task_instance_id = @task_instance_id
    end
    -- if the task is already active then do nothing 
end
go

create procedure ml_ra_int_cancel_notification
    @agent_id		varchar( 128 ),
    @task_instance_id	bigint,
    @request_time	datetime 
as
begin
    set nocount on

    delete from ml_ra_notify
	where agent_poll_key = @agent_id
	    and task_instance_id = @task_instance_id
	    and last_modified <= @request_time
end
go

create procedure ml_ra_cancel_notification
    @agent_id	varchar( 128 ),
    @task_name	varchar( 128 )
as
begin
    declare @task_instance_id	bigint
    declare @ts			datetime

    set nocount on

    select @task_instance_id = task_instance_id
	from ml_ra_agent
	    join ml_ra_deployed_task on ml_ra_deployed_task.aid = ml_ra_agent.aid
	    join ml_ra_task on ml_ra_deployed_task.task_id = ml_ra_task.task_id
	where agent_id = @agent_id 
	    and task_name = @task_name

    select @ts = getdate()
    exec ml_ra_int_cancel_notification @agent_id, @task_instance_id, @ts
end
go

create procedure ml_ra_cancel_task_instance
    @agent_id	varchar( 128 ), 
    @task_name	varchar( 128 )
as
begin
    declare @task_id		bigint
    declare @aid		integer

    set nocount on

    select @task_id = task_id from ml_ra_task where task_name = @task_name
    select @aid = ml_ra_agent.aid from ml_ra_agent where agent_id = @agent_id
    update ml_ra_deployed_task set state = 'CP', last_modified = getdate()
	where aid = @aid and task_id = @task_id 
	    and ( state = 'A' or state = 'P' )
    if @@rowcount = 0 begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'bad task instance'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
	return
    end
    exec ml_ra_cancel_notification @agent_id, @task_name
end
go

-- If error is raised then caller must rollback

create procedure ml_ra_delete_task
    @task_name	varchar( 128 ) 
as
begin
    declare @task_id		bigint

    set nocount on

    select @task_id = task_id from ml_ra_task where task_name = @task_name
    if @task_id is null begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'bad task name'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
    end

    -- Only delete inactive instances, operation
    -- will fail if active instances exist.
    delete from ml_ra_deployed_task where task_id = @task_id 
	and ( state != 'A' and state != 'P' and state != 'CP' )
    delete from ml_ra_task_command_property where task_id = @task_id	
    delete from ml_ra_task_command where task_id = @task_id	
    delete from ml_ra_task_property where task_id = @task_id	
    delete from ml_ra_task where task_id = @task_id	
end
go

-- result contains a row for each deployed instance of every task

create procedure ml_ra_get_task_status
    @agent_id	varchar( 128 ),
    @task_name	varchar( 128 ) 
as
begin
    set nocount on

    select agent_id,
	mr.remote_id,
	t.task_name,
	t.task_id,
	dt.state,
	dt.reported_exec_count + dt.previous_exec_count,
	dt.reported_error_count + dt.previous_error_count,
	dt.reported_attempt_count + dt.previous_attempt_count,
	dt.last_modified,
	( select max( event_time ) from ml_ra_event 
	    where ml_ra_event.task_id = t.task_id ),
	dt.assignment_time
    from ml_ra_task t 
	join ml_ra_deployed_task dt on t.task_id = dt.task_id
	join ml_ra_agent a on a.aid = dt.aid
	left outer join ml_ra_managed_remote mr on mr.schema_name = t.schema_name
	    and mr.aid = a.aid
    where
	( @agent_id is null or a.agent_id = @agent_id )
	and ( @task_name is null or t.task_name = @task_name )
    order by agent_id, t.task_name
end
go

create procedure ml_ra_notify_agent_sync
    @agent_id	varchar( 128 )
as
begin
    declare @aid integer
   
    set nocount on

    select @aid = aid from ml_ra_agent where agent_id = @agent_id
    if @aid is null begin
	return
    end

    if exists ( select * from ml_ra_notify where
		    agent_poll_key = @agent_id and task_instance_id = -1 )
	update ml_ra_notify set last_modified = getdate()
	where agent_poll_key = @agent_id and task_instance_id = -1
    else
	insert into ml_ra_notify( agent_poll_key, task_instance_id,
				  last_modified )
	    values( @agent_id, -1, getdate() ) 
end
go

create procedure ml_ra_notify_task
    @agent_id	varchar( 128 ), 
    @task_name	varchar( 128 ) 
as
begin
    declare @task_instance_id	bigint

    set nocount on

    select @task_instance_id = task_instance_id
	from ml_ra_agent
	    join ml_ra_deployed_task on ml_ra_deployed_task.aid = ml_ra_agent.aid
	    join ml_ra_task on ml_ra_deployed_task.task_id = ml_ra_task.task_id
	where agent_id = @agent_id 
	    and task_name = @task_name

    if exists (select * from ml_ra_notify where 
		    agent_poll_key = @agent_id and
		    task_instance_id = @task_instance_id) 
	update ml_ra_notify set last_modified = getdate()
	    where agent_poll_key = @agent_id and
		task_instance_id = @task_instance_id
    else
	insert into ml_ra_notify( agent_poll_key, task_instance_id,
				  last_modified )
	    values( @agent_id, @task_instance_id, getdate() ) 
end
go

create procedure ml_ra_get_latest_event_id
    @event_id	bigint out 
as
begin
    set nocount on
    select @event_id = max( event_id ) from ml_ra_event
end
go

create procedure ml_ra_get_agent_events
    @start_at_event_id		bigint, 
    @max_events_to_fetch	bigint
as
begin
    set nocount on
    
    select top (@max_events_to_fetch)
	event_id, 
	event_class, 
	event_type,
	ml_ra_agent.agent_id, 
	mr.remote_id,
	t.task_name,
	command_number,
	run_number,
	duration,
	event_time, 
	event_received,
	result_code, 
	result_text
    from ml_ra_event e
	left outer join ml_ra_agent on ml_ra_agent.aid = e.aid
	left outer join ml_ra_task t on t.task_id = e.task_id 
	left outer join ml_ra_managed_remote mr on 
	    mr.schema_name = t.schema_name and mr.aid = ml_ra_agent.aid
    where
	event_id >= @start_at_event_id
    order by event_id
end
go

create procedure ml_ra_get_task_results 
    @agent_id	varchar( 128 ), 
    @task_name	varchar( 128 ),
    @run_number	integer
as
begin
    declare @aid integer
    declare @task_id bigint
    declare @remote_id varchar(128)

    set nocount on

    select @aid = aid from ml_ra_agent where agent_id = @agent_id
    select @task_id = task_id, @remote_id = remote_id from ml_ra_task t
	left outer join ml_ra_managed_remote mr 
	    on mr.schema_name = t.schema_name and mr.aid = @aid
	where task_name = @task_name

    if @run_number is null begin
	-- get the latest run
	select @run_number = max( run_number ) from ml_ra_event
	    where ml_ra_event.aid = @aid and
		ml_ra_event.task_id = @task_id
    end

    select 
	event_id, 
	event_class, 
	event_type,
	@agent_id, 
	@remote_id,
	@task_name,
	command_number,
	run_number,
	duration,
	event_time, 
	event_received,
	result_code, 
	result_text
    from ml_ra_event e where
	e.aid = @aid and e.task_id = @task_id and e.run_number = @run_number
    order by event_id
end
go


-- Maintenance functions ----------------------------------

create procedure ml_ra_get_agent_ids
as
begin
    set nocount on

    select agent_id, 
	( select max( last_download_time ) from ml_subscription mlsb where mlsb.rid = taskdb_rid ), 
	( select max( last_upload_time ) from ml_subscription mlsb where mlsb.rid = taskdb_rid ), 
	( select count(*) from ml_ra_deployed_task where ml_ra_deployed_task.aid = ml_ra_agent.aid
	    and (state = 'A' or state = 'P' or state = 'CP') ),
	remote_id,
	property_value
    from ml_ra_agent left outer join ml_database on ml_database.rid = taskdb_rid
	left outer join ml_ra_agent_property
	on ml_ra_agent.aid = ml_ra_agent_property.aid
	    and property_name = 'ml_ra_description'
    order by agent_id
end
go

create procedure ml_ra_get_remote_ids
as
begin
    set nocount on

    select ml_database.remote_id,
	schema_name,
	agent.agent_id,
	conn_str,
	( select max( last_download_time ) from ml_subscription mlsb where mlsb.rid = ml_database.rid ), 
	( select max( last_upload_time ) from ml_subscription mlsb where mlsb.rid = ml_database.rid ), 
	description
    from ml_database 
	left outer join ml_ra_managed_remote on ml_database.remote_id = ml_ra_managed_remote.remote_id
	left outer join ml_ra_agent agent on agent.aid = ml_ra_managed_remote.aid
	left outer join ml_ra_agent_staging s on s.taskdb_rid = ml_database.rid 
	    and property_name = 'agent_id'
    where property_value is null
    order by ml_database.remote_id
end
go

create procedure ml_ra_set_agent_property
    @agent_id		varchar( 128 ),
    @property_name	varchar( 128 ),
    @property_value	varchar( 2048 )
as
begin
    declare @aid		integer
    declare @server_interval	integer
    declare @old_agent_interval integer
    declare @new_agent_interval integer
    declare @autoset		varchar(3)
    declare @temp		varchar(20)

    set nocount on

    select @aid = aid from ml_ra_agent where agent_id = @agent_id
    
    if @property_name = 'lwp_freq' begin
	select @autoset = property_value from ml_property where 
	    component_name = 'SIRT'
	    and property_set_name = 'RTNotifier(RTNotifier1)'
	    and property_name = 'autoset_poll_every'
	if @autoset = 'yes' begin
	    select @server_interval = cast( cast( property_value as varchar ) as integer ) from ml_property where 
		component_name = 'SIRT'
		and property_set_name = 'RTNotifier(RTNotifier1)'
		and property_name = 'poll_every'
	    -- The next statement is a convoluted no-op, it works around an 
	    -- over-optimization bug where the following cast is evaluated outside
	    -- the lwp_freq check which ends up failing when @property_value is not an integer
	    set @property_value = substring( cast( @aid as varchar(20) ) + @property_value, 
				    datalength( cast( @aid as varchar(20) ) ) + 1, 
				    datalength( @property_value ) )
	    set @new_agent_interval = cast( @property_value as integer )
	    if @new_agent_interval < @server_interval begin
		exec ml_add_property 'SIRT', 'RTNotifier(RTNotifier1)', 'poll_every', @property_value
	    end else if @new_agent_interval > @server_interval begin
		select @old_agent_interval = cast( property_value as integer ) from ml_ra_agent_property where
		    aid = @aid
		    and property_name = 'lwp_freq'
		if @new_agent_interval > @old_agent_interval and @old_agent_interval <= @server_interval begin
		    -- This agents interval is increasing, check if server interval should increase too
		    if not exists( select * from ml_ra_agent_property where property_name = 'lwp_freq'
			and cast(property_value as integer) <= @old_agent_interval
			and aid != @aid ) begin
			-- Need to compute the new server interval
			select @server_interval = min( cast( property_value as integer ) ) from ml_ra_agent_property 
			    where property_name = 'lwp_freq' and aid != @aid
			if @server_interval is null begin
			    set @server_interval = @new_agent_interval
			end
			set @temp = cast(@server_interval as varchar)
			exec ml_add_property 'SIRT', 'RTNotifier(RTNotifier1)', 'poll_every', @temp
		    end
		end
	    end
	end
    end

    if exists (select * from ml_ra_agent_property where
		    aid = @aid and property_name = @property_name)
	update ml_ra_agent_property
	    set property_value = @property_value,
		last_modified = getdate()
	    where aid = @aid and property_name = @property_name
    else
	insert into ml_ra_agent_property( aid, property_name,
					  property_value, last_modified )
	    values( @aid, @property_name, @property_value, getdate() )
end
go

-- If error is raised then caller must rollback

create procedure ml_ra_clone_agent_properties
    @dst_agent_id	varchar( 128 ),
    @src_agent_id	varchar( 128 )
as
begin
    declare @dst_aid	integer
    declare @src_aid	integer

    set nocount on

    select @dst_aid = aid from ml_ra_agent where agent_id = @dst_agent_id
    select @src_aid = aid from ml_ra_agent where agent_id = @src_agent_id
    if @src_aid is null begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'bad source agent id'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
	return
    end

    delete from ml_ra_agent_property
	where aid = @dst_aid
	    and property_name != 'agent_id'
	    and property_name not like( 'ml[_]ra[_]%' )

    insert into ml_ra_agent_property( aid, property_name, property_value, last_modified )
	select @dst_aid, src.property_name, src.property_value, getdate() 
	from ml_ra_agent_property src 
	where src.aid = @src_aid 
	    and property_name != 'agent_id' 
	    and property_name not like( 'ml[_]ra[_]%' )
end
go

create procedure ml_ra_get_agent_properties
    @agent_id	varchar( 128 )
as
begin
    declare @aid integer
    
    set nocount on

    select @aid = aid from ml_ra_agent where agent_id = @agent_id
    select property_name, property_value, last_modified from ml_ra_agent_property 
	where aid = @aid
	    and property_name != 'agent_id'
	    and property_name not like( 'ml[_]ra[_]%' )
	order by property_name
end
go

-- If error is raised then caller must rollback

create procedure ml_ra_add_agent_id
    @agent_id	varchar( 128 )
as
begin
    declare @aid integer

    set nocount on

    insert into ml_ra_agent( agent_id ) values ( @agent_id )

    select @aid = @@identity
    insert into ml_ra_event(event_class, event_type, aid, event_time ) 
	values( 'I', 'ANEW', @aid, getdate() )

    exec ml_ra_set_agent_property @agent_id, 'agent_id', @agent_id
    exec ml_ra_set_agent_property @agent_id, 'max_taskdb_sync_interval', '86400'
    exec ml_ra_set_agent_property @agent_id, 'lwp_freq', '900'
    exec ml_ra_set_agent_property @agent_id, 'agent_id_status', 'OK'
end
go

-- If error is raised then caller must rollback

create procedure ml_ra_manage_remote_db
    @agent_id		varchar( 128 ), 
    @schema_name	varchar( 128 ),
    @conn_str		varchar( 2048 )
as
begin
    declare @aid integer
    declare @ldt datetime

    set nocount on

    select @aid = aid, @ldt = last_download_time from 
	ml_ra_agent left outer join ml_subscription on taskdb_rid = rid
    where agent_id = @agent_id

    insert into ml_ra_managed_remote(aid, remote_id, schema_name, conn_str, last_modified ) 
	values( @aid, null, @schema_name, @conn_str, getdate() )
	
    update ml_ra_deployed_task set state = 'A' 
	where aid = @aid and state = 'P' and last_modified < @ldt
	    and exists( select * from ml_ra_task t where t.task_id = ml_ra_deployed_task.task_id and t.schema_name = @schema_name )
end
go

-- If error is raised then caller must rollback

create procedure ml_ra_unmanage_remote_db
    @agent_id		varchar( 128 ),
    @schema_name	varchar( 128 )
as
begin
    declare @aid		integer
    declare @has_tasks		integer

    set nocount on

    select @aid = aid from ml_ra_agent where agent_id = @agent_id

    if exists( select * from ml_ra_deployed_task dt join ml_ra_task t
		    on dt.task_id = t.task_id
		    where dt.aid = @aid
			and t.schema_name = @schema_name
			and (state = 'A' or state = 'P' or state = 'CP') ) 
	select @has_tasks = 1
    else
	select @has_tasks = 0

    if @has_tasks = 1 begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'has active tasks'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
	return
    end

    delete from ml_ra_deployed_task
	where aid = @aid and state != 'A' and state != 'P' and state != 'CP'
	    and exists( select * from ml_ra_task where ml_ra_task.task_id = ml_ra_deployed_task.task_id
		and ml_ra_task.schema_name = @schema_name )
    delete from ml_ra_managed_remote where aid = @aid and schema_name = @schema_name
end
go


-- If error is raised then caller must rollback

create procedure ml_ra_delete_agent_id
    @agent_id		varchar( 128 )
as
begin
    declare @aid		integer
    declare @taskdb_rid		integer
    declare @taskdb_remote_id	varchar( 128 )
    
    set nocount on

    select @aid = aid, @taskdb_rid = taskdb_rid
	from ml_ra_agent where agent_id = @agent_id
    if @aid is null begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'bad agent id'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
	return
    end

    exec ml_ra_set_agent_property @agent_id, 'lwp_freq', '2147483647'

    -- Delete all dependent rows
    delete from ml_ra_agent_property where aid = @aid
    delete from ml_ra_deployed_task where aid = @aid
    delete from ml_ra_notify where agent_poll_key = @agent_id
    delete from ml_ra_managed_remote where aid = @aid

    -- Delete the agent
    delete from ml_ra_agent where aid = @aid

    -- Clean up any taskdbs that were associated with this agent_id
    begin
	declare taskdb_crsr cursor for
	    select taskdb_rid, remote_id from ml_ra_agent_staging
		join ml_database on ml_database.rid = taskdb_rid	
		where property_name = 'agent_id' and property_value = @agent_id
    
	open taskdb_crsr
	while 1 = 1 begin
	    fetch taskdb_crsr into @taskdb_rid, @taskdb_remote_id
	    if @@fetch_status != 0 break
	    delete from ml_ra_event_staging where taskdb_rid = @taskdb_rid
	    delete from ml_ra_agent_staging where taskdb_rid = @taskdb_rid
	    exec ml_delete_remote_id @taskdb_remote_id
	end 
	close taskdb_crsr
	deallocate taskdb_crsr
    end
end
go

create procedure ml_ra_int_move_events
    @aid		integer, 
    @taskdb_rid		integer
as
begin
    set nocount on

    -- Copy events into ml_ra_event from staging table
    insert into ml_ra_event( event_class, event_type, aid, task_id,
			     command_number, run_number, duration, event_time,
			     event_received, result_code, result_text )
	select event_class, event_type, @aid, dt.task_id, command_number,
	       run_number, duration, event_time, getdate(), result_code,
	       result_text
	    from ml_ra_event_staging es
		left outer join ml_ra_deployed_task dt on dt.task_instance_id = es.task_instance_id
	    where es.taskdb_rid = @taskdb_rid
	    order by remote_event_id

    -- Clean up staged values
    delete from ml_ra_event_staging where taskdb_rid = @taskdb_rid
end
go

create procedure ml_ra_delete_events_before
    @delete_rows_older_than	datetime
as
begin
    set nocount on
    delete from ml_ra_event where event_received <= @delete_rows_older_than
end
go

create procedure ml_ra_get_orphan_taskdbs
as
begin
    set nocount on
    select remote_id,
	property_value,
	( select max( last_upload_time ) from ml_subscription mlsb where mlsb.rid = ml_database.rid ) 
    from ml_database 
	left outer join ml_ra_agent agent on agent.taskdb_rid = rid
	left outer join ml_ra_agent_staging s on s.taskdb_rid = rid and property_name = 'agent_id'
    where property_value is not null and agent_id is null
    order by remote_id
end
go

-- If error is raised then caller must rollback

create procedure ml_ra_reassign_taskdb
    @taskdb_remote_id	varchar( 128 ),
    @new_agent_id	varchar( 128 )
as
begin
    declare @other_taskdb_rid	integer
    declare @taskdb_rid		integer
    declare @other_agent_aid	integer
    declare @old_agent_id	varchar( 128 )
    declare @new_aid		integer

    set nocount on

    select @taskdb_rid = rid from ml_database where remote_id = @taskdb_remote_id
    if @taskdb_rid is null begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'bad remote'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
	return
    end

    select @old_agent_id = property_value from ml_ra_agent_staging where
	taskdb_rid = @taskdb_rid and property_name = 'agent_id'
    if @old_agent_id is null begin
	exec sp_addmessage @msgnum = 99001, @severity = 15, @msgtext = 'bad remote'
	raiserror( 99001, 15, 1 )
	exec sp_dropmessage @msgnum = 99001
	return
    end

    select @other_taskdb_rid = count(*) from ml_ra_agent
	where agent_id = @new_agent_id
    if @other_taskdb_rid = 0 begin
	exec ml_ra_add_agent_id @new_agent_id
    end
    -- if @other_taskdb_rid is not null then it becomes a new orphan taskdb

    -- If the taskdb isn't already orphaned then break the link with its original agent_id
    update ml_ra_agent set taskdb_rid = null where taskdb_rid = @taskdb_rid

    update ml_ra_agent_staging set property_value = @new_agent_id
	where taskdb_rid = @taskdb_rid
	    and property_name = 'agent_id'

    -- Preserve any events that have been uploaded
    -- Note, no task state is updated here, these
    -- events are stale and may no longer apply.
    select @new_aid = aid from ml_ra_agent where agent_id = @new_agent_id
    exec ml_ra_int_move_events @new_aid, @taskdb_rid
    if @@error != 0 begin return end

    -- The next time the agent syncs it will receive its new agent_id
    exec ml_ra_notify_agent_sync @old_agent_id
end
go

-----------------------------------------------------------------
-- Synchronization scripts for the remote agent's task database
-- Note, there is no authenticate user script here, this will need
-- to be provided by the user.
-----------------------------------------------------------------

create procedure ml_ra_ss_end_upload 
    @taskdb_remote_id	varchar( 128 )
as
begin
    declare @taskdb_rid		integer
    declare @consdb_taskdb_rid	integer
    declare @consdb_taskdb_remote_id varchar( 128 )
    declare @agent_id		varchar( 128 )
    declare @provided_id	varchar( 128 )
    declare @old_machine_name	varchar( 128 )
    declare @new_machine_name	varchar( 128 )
    declare @aid		integer
    declare @used		varchar( 128 )
    declare @name		varchar( 128 )
    declare @value		varchar( 2048 )
    declare @old_value		varchar( 2048 )
    declare @schema_name	varchar( 128 )

    declare @task_instance_id	bigint
    declare @result_code	bigint
    declare @event_type		varchar(8)
    declare @event_time		datetime

    set nocount on

    select @taskdb_rid = rid, @agent_id = agent_id, @aid = aid 
	from ml_database left outer join ml_ra_agent on taskdb_rid = rid 
	where remote_id = @taskdb_remote_id

    if @agent_id is null begin 
	-- This taskdb isn't linked to an agent_id in the consolidated yet
	delete from ml_ra_agent_staging where taskdb_rid = @taskdb_rid and property_name = 'agent_id_status'
	select @provided_id = property_value from ml_ra_agent_staging 
	    where taskdb_rid = @taskdb_rid and property_name = 'agent_id'
	if @provided_id is null begin
	    -- Agent failed to provide an agent_id
	    insert into ml_ra_agent_staging( taskdb_rid,
		    property_name, property_value )
		values( @taskdb_rid, 'agent_id_status', 'RESET' )
	    return
	end
	    
	select @consdb_taskdb_rid = taskdb_rid, @aid = aid
	    from ml_ra_agent where agent_id = @provided_id
	if @consdb_taskdb_rid is not null begin
	    -- We have 2 remote task databases using the same agent_id.
	    -- Attempt to determine if its a reset of an agent or 2 separate 
	    -- agents conflicting with each other.
	    select @consdb_taskdb_remote_id = remote_id
		from ml_database where rid = @consdb_taskdb_rid
	    select @old_machine_name = substring( @consdb_taskdb_remote_id,
				7, datalength(@consdb_taskdb_remote_id) - 43 )
	    select @new_machine_name = substring( @taskdb_remote_id,
				7, datalength(@taskdb_remote_id) - 43 )
		
	    if @old_machine_name != @new_machine_name begin
		-- There are 2 agents with conflicting agent_ids
		-- This taskdb will not be allowed to download tasks.
		insert into ml_ra_event(event_class, event_type, aid, event_time, result_text ) 
		    values( 'E', 'ADUP', @aid, getdate(), @taskdb_remote_id )
		insert into ml_ra_agent_staging( taskdb_rid,
			property_name, property_value )
			values( @taskdb_rid, 'agent_id_status', 'DUP' )
		return
	    end -- Otherwise, we allow replacement of the taskdb
	end	    

	set @agent_id = @provided_id
	if @aid is null begin
	    -- We have a new agent_id
	    exec ml_ra_add_agent_id @agent_id
	    select @aid = aid from ml_ra_agent where agent_id = @agent_id
	end

	select @used = property_value from ml_ra_agent_staging 
	    where taskdb_rid = @taskdb_rid and property_name = 'ml_ra_used'
	if @used is not null begin
	    -- We can only establish a mapping between new taskdb_remote_ids and agent_ids
	    insert into ml_ra_agent_staging( taskdb_rid,
		    property_name, property_value )
		    values( @taskdb_rid, 'agent_id_status', 'RESET' )
	    -- Preserve any events that may have been uploaded
	    -- Note, no task state is updated here, these
	    -- events could be stale and may no longer apply.
	    exec ml_ra_int_move_events @aid, @taskdb_rid
	    return
	end else begin	    
	    insert into ml_ra_agent_staging( taskdb_rid, property_name, property_value )
		values( @taskdb_rid, 'ml_ra_used', '1' )
	end

	-- Store the link between this agent_id and remote_id
	update ml_ra_agent set taskdb_rid = @taskdb_rid where agent_id = @agent_id

	select @used = property_value from ml_ra_agent_property
	    where aid = @aid and property_name = 'ml_ra_used'
	if @used is null begin
	    -- This is the first taskdb for an agent
	    insert into ml_ra_event(event_class, event_type, aid, event_time ) 
		values( 'I', 'AFIRST', @aid, getdate() )
	    insert into ml_ra_agent_property( aid, property_name, property_value, last_modified )
		values( @aid, 'ml_ra_used', '1', getdate() )
	end
	else begin
	    -- A new taskdb is taking over
	    insert into ml_ra_event(event_class, event_type, aid, event_time, result_text ) 
		values( 'I', 'ARESET', @aid, getdate(), @consdb_taskdb_remote_id )

	    update ml_ra_deployed_task set
		state = ( case state 
		    when 'A' then 'P'
		    when 'CP' then 'C'
		    else state end ),
		previous_exec_count = reported_exec_count + previous_exec_count,
		previous_error_count = reported_error_count + previous_error_count,
		previous_attempt_count = reported_attempt_count + previous_attempt_count,
		reported_exec_count = 0,
		reported_error_count = 0,
		reported_attempt_count = 0,
		last_modified = getdate()
	    where aid = @aid
	end
    end

    begin
	-- Update the status of deployed tasks
	declare event_crsr cursor for 
	    select event_type, result_code, result_text, task_instance_id, event_time from ml_ra_event_staging
	    where taskdb_rid = @taskdb_rid  order by remote_event_id
	open event_crsr
	while 1 = 1 begin
	    fetch event_crsr into @event_type, @result_code, @value, @task_instance_id, @event_time
	    if @@FETCH_STATUS != 0 break

	    if @event_type like 'TI%' or @event_type like 'TF%' begin
		update ml_ra_deployed_task  
		    set reported_exec_count = case when @event_type = 'TIE' then @result_code else reported_exec_count end,
			reported_error_count = case when @event_type = 'TIF' then @result_code else reported_error_count end,
			reported_attempt_count = case when @event_type = 'TIA' then @result_code else reported_attempt_count end,
			state = case when @event_type like('TF%') then substring( @event_type, 3, datalength( @event_type ) - 2 ) else 
			    state end
		    where task_instance_id = @task_instance_id
	    end

	    -- Store any updated remote_ids
	    if @event_type = 'TRID' begin
		select @schema_name = t.schema_name from ml_ra_deployed_task dt
		    join ml_ra_task t on t.task_id = dt.task_id
		    where dt.task_instance_id = @task_instance_id
		update ml_ra_managed_remote set remote_id = @value
		    where aid = @aid and schema_name = @schema_name
	    end

	    -- Update remote schema names
	    if @event_type = 'CR' and @value like 'CHSN:%' begin
		select @value = substring( @value, 6, datalength( @value ) - 5 )
		select @schema_name = t.schema_name from ml_ra_deployed_task dt
		    join ml_ra_task t on t.task_id = dt.task_id
		    where dt.task_instance_id = @task_instance_id
		update ml_ra_managed_remote set schema_name = @value
		    where aid = @aid and schema_name = @schema_name
			and exists( select * from ml_ra_schema_name where schema_name = @value )
		update ml_ra_deployed_task set state = 'P' 
		    where aid = @aid and state = 'A'
			and exists( select * from ml_ra_task t left outer join ml_ra_managed_remote mr
				on t.schema_name = mr.schema_name and mr.aid = @aid
				where t.task_id = ml_ra_deployed_task.task_id
				    and t.schema_name is not null 
				    and mr.schema_name is null )
	    end

	    -- Process SIRT ack
	    if @event_type like 'TS%'
		delete from ml_ra_notify where 
		    ml_ra_notify.task_instance_id = @task_instance_id 
		    and last_modified <= @event_time

	    -- TI status rows are not true events
	    if @event_type like 'TI%'
	    delete from ml_ra_event_staging
		where current of event_crsr
	end
	close event_crsr
	deallocate event_crsr
    end
   

    -- Cleanup any obsolete SIRT requests
    delete from ml_ra_notify where agent_poll_key = @agent_id and task_instance_id != -1 
	and not exists(	select * from ml_ra_deployed_task where
	    ml_ra_deployed_task.task_instance_id = ml_ra_notify.task_instance_id )

    -- Get properties from the agent
    begin
	declare as_crsr cursor for
	    select property_name, property_value
		from ml_ra_agent_staging
		where taskdb_rid = @taskdb_rid and
		    property_name not like ( 'ml[_]ra[_]%' )
	open as_crsr
	while 1 = 1 begin
	    fetch as_crsr into @name, @value
	    if @@FETCH_STATUS != 0 break
	    if not exists (select * from ml_ra_agent_property
				where aid = @aid and property_name = @name) begin
		insert into ml_ra_agent_property( aid, property_name,
				      property_value, last_modified )
		    values( @aid, @name, @value, getdate() )
	    end else begin
		select @old_value = property_value from ml_ra_agent_property
		    where aid = @aid and property_name = @name
		if (@old_value is null and @value is not null )
			or (@old_value is not null and @value is null )
			or (@old_value != @value)
		    update ml_ra_agent_property set property_value = @value, last_modified = getdate()
			where aid = @aid and property_name = @name
	    end
	end
	close as_crsr
	deallocate as_crsr
    end

    delete from ml_ra_agent_staging where taskdb_rid = @taskdb_rid 
	and property_name not like ( 'ml[_]ra[_]%' )
	and property_name != 'agent_id'
    exec ml_ra_int_move_events @aid, @taskdb_rid
end
go

create procedure ml_ra_ss_download_prop
    @taskdb_remote_id		varchar( 128 ), 
    @last_table_download	datetime
as
begin
    declare @aid integer
    declare @taskdb_rid integer

    select @aid = a.aid, @taskdb_rid = d.rid from ml_database d 
	left outer join ml_ra_agent a on a.taskdb_rid = d.rid
	where d.remote_id = @taskdb_remote_id

    if @aid is null begin
	select property_name, property_value from ml_ra_agent_staging 
	    where taskdb_rid = @taskdb_rid 
		and property_name not like 'ml[_]ra[_]%'
    end
    else begin
	select property_name, property_value from ml_ra_agent_property p 
	    where p.aid = @aid and property_name not like 'ml[_]ra[_]%' 
		and last_modified >= @last_table_download
    end
end
go

create procedure ml_ra_ss_upload_prop
    @taskdb_remote_id varchar(128), 
    @property_name varchar(128), 
    @property_value varchar(2048) 
as
begin
    declare @taskdb_rid integer

    set nocount on

    select @taskdb_rid = rid from ml_database where remote_id = @taskdb_remote_id

    if exists( select * from ml_ra_agent_staging 
	where taskdb_rid = @taskdb_rid and property_name = @property_name ) 
    begin
	update ml_ra_agent_staging set property_value = @property_value
	    where taskdb_rid = @taskdb_rid and property_name = @property_name 
    end
    else begin
	insert into ml_ra_agent_staging( taskdb_rid, property_name, property_value )
	    values( @taskdb_rid, @property_name, @property_value ) 
    end
end
go

create procedure ml_ra_ss_upload_event
    @taskdb_remote_id	varchar( 128 ), 
    @remote_event_id	bigint, 
    @event_class	varchar(1), 
    @event_type		varchar( 4 ), 
    @task_instance_id	bigint, 
    @command_number	integer, 
    @run_number		bigint, 
    @duration		integer,
    @event_time		datetime,
    @result_code	bigint, 
    @result_text	varchar( max )
as
begin
    declare @taskdb_rid	integer

    set nocount on

    select @taskdb_rid = rid from ml_database
	where remote_id = @taskdb_remote_id

    if not exists (select * from ml_ra_event_staging
		    where taskdb_rid = @taskdb_rid and
			remote_event_id = @remote_event_id)
	insert into ml_ra_event_staging( taskdb_rid, remote_event_id, 
		event_class, event_type, task_instance_id,
		command_number, run_number, duration, event_time,
		result_code, result_text )
	    values ( @taskdb_rid, @remote_event_id, @event_class,
		@event_type, @task_instance_id, @command_number,
		@run_number, @duration, @event_time, @result_code,
		@result_text )
end
go

create procedure ml_ra_ss_download_ack
    @taskdb_remote_id	varchar( 128 ), 
    @ldt		datetime
as
begin
    declare @aid	integer
    declare @agent_id	varchar( 128 )

    set nocount on

    select @aid = aid, @agent_id = agent_id
	from ml_ra_agent
	    join ml_database on taskdb_rid = rid
	where remote_id = @taskdb_remote_id

    update ml_ra_deployed_task set state = 'A' 
	where ml_ra_deployed_task.aid = @aid and state = 'P' and last_modified < @ldt
	    and exists( select * from ml_ra_task t left outer join ml_ra_managed_remote mr
		    on t.schema_name = mr.schema_name and mr.aid = @aid
		    where t.task_id = ml_ra_deployed_task.task_id
			and ( t.schema_name is null or mr.schema_name is not null ) )
   
    delete from ml_ra_notify
	where agent_poll_key = @agent_id
	    and task_instance_id = -1 
	    and last_modified <= @ldt
end
go

-- Default file transfer scripts for upload and download

create procedure ml_ra_ss_agent_auth_file_xfer
    @requested_direction varchar(1),
    @auth_code	INTEGER		out,
    @ml_user	varchar( 128 ),
    @remote_key	varchar( 128 ),
    @fsize	BIGINT,
    @filename	varchar( 128 )	out,
    @sub_dir	varchar( 128 )	out  
as
begin
    declare @offset		integer
    declare @cmd_num		integer
    declare @tiid		bigint
    declare @tid		bigint
    declare @aid		integer
    declare @task_state		varchar( 4 )
    declare @max_size		bigint
    declare @direction		varchar(1)
    declare @server_sub_dir	varchar( 128 )
    declare @server_filename	varchar( 128 )

    set nocount on

    -- By convention file transfer commands will send up the remote key with...
    -- task_instance_id command_number
    -- eg 1 5	-- task_instance_id=1 command_number=5
    select @offset = charindex( ' ', @remote_key )
    if @offset = 0 begin
	select @auth_code = 2000
	return
    end

    select @tiid = cast( substring( @remote_key, 0, @offset ) as BIGINT ) 
    select @cmd_num = cast( substring( @remote_key, @offset + 1, datalength( @remote_key ) - @offset ) as BIGINT )
    if @tiid is null or @tiid < 1 or @cmd_num is null or @cmd_num < 0 begin	
	select @auth_code = 2000
	return
    end

    -- fetch properties of the task
    select @tid = task_id, @aid = aid, @task_state = state 
	from ml_ra_deployed_task where task_instance_id = @tiid

    -- Disallow transfer if the task is no longer active
    if @task_state is null or (@task_state != 'A' and @task_state != 'P') begin 
	select @auth_code = 2001
	return
    end

    -- Make sure the file isn't too big
    select @max_size = property_value from ml_ra_task_command_property
	where task_id = @tid and
	    command_number = @cmd_num and
	    property_name = 'mlft_max_file_size'
    if @max_size > 0 and @fsize > @max_size begin
	select @auth_code = 2002
	return
    end

    -- Make sure the direction is correct
    select @direction = property_value from ml_ra_task_command_property
	where task_id = @tid and
	    command_number = @cmd_num and
	    property_name = 'mlft_transfer_direction'
    if @direction != @requested_direction begin
	select @auth_code = 2003
	return
    end

    -- set the filename output parameter
    select @server_filename = property_value from ml_ra_task_command_property
	where task_id = @tid and command_number = @cmd_num and
	    property_name = 'mlft_server_filename'
    if @server_filename is not null begin
	select @filename = replace( replace( @server_filename, '{ml_username}', @ml_user ), '{agent_id}',
	    ( select agent_id from ml_ra_agent where aid = @aid ) )
    end

    -- set the sub_dir output parameter
    select @server_sub_dir = property_value from ml_ra_task_command_property
	where task_id = @tid and
	    command_number = @cmd_num and
	    property_name = 'mlft_server_sub_dir'

    if @server_sub_dir is null begin
	select @sub_dir = ''
    end
    else begin
	select @sub_dir = replace( replace( @server_sub_dir, '{ml_username}', @ml_user ), '{agent_id}',
	    ( select agent_id from ml_ra_agent where aid = @aid ) )
    end

    -- Everything is ok, allow the file transfer
    select @auth_code = 1000
end
go

exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_adminprop', 'upload_insert', 
    '{call ml_ra_ss_upload_prop( {ml s.remote_id}, {ml r.name}, {ml r.value} )}'    
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_adminprop', 'upload_update', 
    '{call ml_ra_ss_upload_prop( {ml s.remote_id}, {ml r.name}, {ml r.value} )}'    
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_adminprop', 'download_cursor', 
    '{call ml_ra_ss_download_prop( {ml s.remote_id}, {ml s.last_table_download} )}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_adminprop', 'download_delete_cursor', '--{ml_ignore}'
go

exec ml_add_connection_script 'ml_ra_agent_12', 'end_upload',
    '{call ml_ra_ss_end_upload( {ml s.remote_id} )}'
go
exec ml_add_connection_script 'ml_ra_agent_12', 'nonblocking_download_ack',
    '{call ml_ra_ss_download_ack( {ml s.remote_id}, {ml s.last_download} )}'
go

exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_task', 'download_cursor', 
    '{call ml_ra_ss_download_task( {ml s.remote_id} )}'
go

exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_task', 'download_delete_cursor', '--{ml_ignore}'
go

exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_command', 'download_cursor', 
    '{call ml_ra_ss_download_task_cmd( {ml s.remote_id} )}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_command', 'download_delete_cursor', '--{ml_ignore}'
go


exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_remote_db', 'download_cursor', 
    '{call ml_ra_ss_download_remote_dbs( {ml s.remote_id}, {ml s.last_table_download} )}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_remote_db', 'download_delete_cursor', '--{ml_ignore}'
go

exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_status', 'upload_insert', 
    '{call ml_ra_ss_upload_event( 
    {ml s.remote_id}, {ml r.id}, {ml r.class}, {ml r.status}, 
    {ml r.task_instance_id}, {ml r.command_number}, {ml r.exec_count}, 
    {ml r.duration}, {ml r.status_time}, {ml r.status_code}, {ml r.text} )}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_status', 'upload_update', 
    '{call ml_ra_ss_upload_event( 
    {ml s.remote_id}, {ml r.id}, {ml r.class}, {ml r.status}, 
    {ml r.task_instance_id}, {ml r.command_number}, {ml r.exec_count}, 
    {ml r.duration}, {ml r.status_time}, {ml r.status_code}, {ml r.text} )}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_status', 'download_cursor', '--{ml_ignore}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_status', 'download_delete_cursor', '--{ml_ignore}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_remote_db', 'upload_insert', '--{ml_ignore}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_remote_db', 'upload_update', '--{ml_ignore}'
go
exec ml_add_table_script 'ml_ra_agent_12', 'ml_ra_agent_remote_db', 'upload_delete', '--{ml_ignore}'
go
exec ml_add_connection_script 'ml_ra_agent_12', 'authenticate_file_upload', 
    '{ call ml_ra_ss_agent_auth_file_xfer( ''U'', {ml s.file_authentication_code}, {ml s.username}, {ml s.remote_key}, {ml s.file_size}, {ml s.filename}, {ml s.subdir} ) }'
go

exec ml_add_connection_script 'ml_ra_agent_12', 'authenticate_file_transfer', 
    '{ call ml_ra_ss_agent_auth_file_xfer( ''D'', {ml s.file_authentication_code}, {ml s.username}, {ml s.remote_key}, 0, {ml s.filename}, {ml s.subdir} ) }'
go

exec ml_add_property 'SIRT', 'RTNotifier(RTNotifier1)', 'request_cursor', 'select agent_poll_key,task_instance_id,last_modified from ml_ra_notify order by agent_poll_key'
go

-- RT Notifier doesn't begin polling until an agent is created
exec ml_add_property 'SIRT', 'RTNotifier(RTNotifier1)', 'poll_every', '2147483647'
go

-- Set to 'no' to disable auto setting 'poll_every', then manually set 'poll_every'
exec ml_add_property 'SIRT', 'RTNotifier(RTNotifier1)', 'autoset_poll_every', 'yes'
go

exec ml_add_property 'SIRT', 'RTNotifier(RTNotifier1)', 'enable', 'yes'
go

-- Check for updates to started notifiers every minute
exec ml_add_property 'SIRT', 'Global', 'update_poll_every', '60'
go

create procedure ml_ra_ss_download_task
    @taskdb_remote_id		varchar( 128 )
as
    select task_instance_id, task_name, ml_ra_task.schema_name,
	max_number_of_attempts, delay_between_attempts,
	max_running_time, ml_ra_task.flags,
	case dt.state 
	    when 'P' then 'A'
	    when 'CP' then 'C'
	end,
	cond, remote_event
    from ml_database task_db
	join ml_ra_agent on ml_ra_agent.taskdb_rid = task_db.rid
	join ml_ra_deployed_task dt on dt.aid = ml_ra_agent.aid
	join ml_ra_task on dt.task_id = ml_ra_task.task_id
    where task_db.remote_id = @taskdb_remote_id
	and ( dt.state = 'CP' or dt.state = 'P' )
go

create procedure ml_ra_ss_download_task_cmd
    @taskdb_remote_id		varchar( 128 )
as
    select task_instance_id, command_number, ml_ra_task_command.flags,
	action_type, action_parm
    from ml_database task_db
	join ml_ra_agent on ml_ra_agent.taskdb_rid = task_db.rid
	join ml_ra_deployed_task dt on dt.aid = ml_ra_agent.aid
	join ml_ra_task on dt.task_id = ml_ra_task.task_id
	join ml_ra_task_command on dt.task_id = ml_ra_task_command.task_id
    where task_db.remote_id = @taskdb_remote_id
	and dt.state = 'P'
go

create procedure ml_ra_ss_download_remote_dbs
    @taskdb_remote_id		varchar( 128 ),
    @last_download		datetime
as
    select ml_ra_schema_name.schema_name, ml_ra_managed_remote.remote_id, conn_str, remote_type 
    from ml_database taskdb
	join ml_ra_agent on ml_ra_agent.taskdb_rid = taskdb.rid
	join ml_ra_managed_remote on ml_ra_managed_remote.aid = ml_ra_agent.aid
	join ml_ra_schema_name on ml_ra_schema_name.schema_name = ml_ra_managed_remote.schema_name
    where taskdb.remote_id = @taskdb_remote_id
	and ml_ra_managed_remote.last_modified >= @last_download
go

create procedure ml_ra_ss_download_task2
    @taskdb_remote_id		varchar( 128 )
as
    select task_instance_id, task_name, ml_ra_task.schema_name,
	max_number_of_attempts, delay_between_attempts,
	max_running_time, ml_ra_task.flags,
	case dt.state 
	    when 'P' then 'A'
	    when 'CP' then 'C'
	end,
	cond, remote_event, random_delay_interval
    from ml_database task_db
	join ml_ra_agent on ml_ra_agent.taskdb_rid = task_db.rid
	join ml_ra_deployed_task dt on dt.aid = ml_ra_agent.aid
	join ml_ra_task on dt.task_id = ml_ra_task.task_id
    where task_db.remote_id = @taskdb_remote_id
	and ( dt.state = 'CP' or dt.state = 'P' )
go

/* Updated Script for 12.0.1 */
exec ml_share_all_scripts 'ml_ra_agent_12_1', 'ml_ra_agent_12' 
go
exec ml_add_table_script 'ml_ra_agent_12_1', 'ml_ra_agent_task', 'download_cursor', 
   '{ call ml_ra_ss_download_task2( {ml s.remote_id} ) }' 
go
