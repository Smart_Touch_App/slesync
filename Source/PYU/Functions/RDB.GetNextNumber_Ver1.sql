CREATE OR REPLACE FUNCTION "BUSDTA"."GetNextNumber"(  RouteID nvarchar(50), EntityID  nvarchar(50) )
	RETURNS nvarchar(50)
BEGIN
	declare poolentityID nvarchar(50);
	declare poolroute nvarchar(50);
	declare returnNumber nvarchar(50);
	declare returnNumberFormat nvarchar(50);
	declare poolcount numeric(10);
	declare poolbucket nvarchar(50);
	declare poolstart integer;
	declare poolend integer;
	set poolentityID=EntityID;
	set poolroute=RouteID;
		select FIRST RangeStart,RangeEnd,isnull(CurrentAllocated,RangeStart-1)+1,Bucket into poolstart ,poolend ,poolcount, poolbucket from   BUSDTA.Entity_Number_Status 
			where 
			EntityBucketMasterId=poolentityID
			and isconsumed=0
			and RouteID=poolroute
			order by CreatedDatetime asc,bucket asc;
		UPDATE  BUSDTA.Entity_Number_Status
			SET CurrentAllocated = isnull(poolcount,poolstart)
			where 
			EntityBucketMasterId=poolentityID
			and isconsumed=0
			and RouteID=poolroute
			and Bucket=poolbucket;
		IF poolcount >= poolend THEN
			UPDATE  BUSDTA.Entity_Number_Status 
			SET isconsumed = 1,
			ConsumedOnDate=getdate()
			where 
			EntityBucketMasterId=poolentityID
			and isconsumed=0
			and RouteID=poolroute
			and Bucket=poolbucket
		END IF;
		--IF poolcount > 0 THEN
		--	select ''+ replicate('0',"Padding" - datalength(convert(varchar(10),poolcount))) + convert(varchar(10),poolcount),"Format" into returnNumber,returnNumberFormat 
		--  from BUSDTA.Entity_Bucket_Master 
		--	where EntityBucketMasterId=poolentityID;
		--	set returnNumber = replace(returnNumberFormat,'{0}',returnNumber);
		--END IF;
	COMMIT;
	return poolcount;
END