
------- /*[BUSDTA].[Route_User_Map]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."Route_User_Map"     (
	"App_user_id" integer not null,
	"Route_Id" varchar(8) not null,
	"Active" integer null,
	"Default_Route" integer null,
	PRIMARY KEY ("App_user_id", "Route_Id")
)

------- /*[BUSDTA].[Route_User_Map]*/ - End
