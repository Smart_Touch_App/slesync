 
SELECT "BUSDTA"."ItemUoMs"."ItemID"
,"BUSDTA"."ItemUoMs"."UOM"
,"BUSDTA"."ItemUoMs"."CrossReferenceID"
,"BUSDTA"."ItemUoMs"."CanSell"
,"BUSDTA"."ItemUoMs"."CanSample"
,"BUSDTA"."ItemUoMs"."CanRestock"
,"BUSDTA"."ItemUoMs"."DisplaySeq"
,"BUSDTA"."ItemUoMs"."CreatedBy"
,"BUSDTA"."ItemUoMs"."CreatedDatetime"
,"BUSDTA"."ItemUoMs"."UpdatedBy"
,"BUSDTA"."ItemUoMs"."UpdatedDatetime"

FROM "BUSDTA"."ItemUoMs"
WHERE "BUSDTA"."ItemUoMs"."last_modified">= {ml s.last_table_download}
 
