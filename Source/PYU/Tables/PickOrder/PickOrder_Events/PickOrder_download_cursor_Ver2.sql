

SELECT "BUSDTA"."PickOrder"."PickOrder_Id"
,"BUSDTA"."PickOrder"."Order_ID"
,"BUSDTA"."PickOrder"."RouteId"
,"BUSDTA"."PickOrder"."Item_Number"
,"BUSDTA"."PickOrder"."Order_Qty"
,"BUSDTA"."PickOrder"."Order_UOM"
,"BUSDTA"."PickOrder"."Picked_Qty_Primary_UOM"
,"BUSDTA"."PickOrder"."Primary_UOM"
,"BUSDTA"."PickOrder"."Order_Qty_Primary_UOM"
,"BUSDTA"."PickOrder"."On_Hand_Qty_Primary"
,"BUSDTA"."PickOrder"."Last_Scan_Mode"
,"BUSDTA"."PickOrder"."Item_Scan_Sequence"
,"BUSDTA"."PickOrder"."Picked_By"
,"BUSDTA"."PickOrder"."IsOnHold"
,"BUSDTA"."PickOrder"."Reason_Code_Id"
,"BUSDTA"."PickOrder"."ManuallyPickCount"

FROM BUSDTA.Order_Header, BUSDTA.PickOrder, BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm
WHERE "BUSDTA"."PickOrder"."last_modified" >= {ml s.last_table_download}
AND CustShipToID = crm.RelatedAddressBookNumber and BUSDTA.Order_Header.OrderID = busdta.PickOrder.Order_ID AND crm.BranchNumber = rm.BranchNumber
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )
