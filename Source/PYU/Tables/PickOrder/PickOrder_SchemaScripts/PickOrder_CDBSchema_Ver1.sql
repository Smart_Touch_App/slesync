
/* [BUSDTA].[PickOrder] - begins */

/* TableDDL - [BUSDTA].[PickOrder] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder]
	(
	  [PickOrder_Id] INT NOT NULL
	, [Order_ID] INT NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Item_Number] NCHAR(25) NULL
	, [Order_Qty] INT NULL
	, [Order_UOM] NCHAR(2) NULL
	, [Picked_Qty_Primary_UOM] INT NULL
	, [Primary_UOM] NCHAR(2) NULL
	, [Order_Qty_Primary_UOM] INT NULL
	, [On_Hand_Qty_Primary] INT NULL
	, [Last_Scan_Mode] BIT NULL
	, [Item_Scan_Sequence] INT NULL
	, [Picked_By] INT NULL
	, [IsOnHold] INT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PICKORDER_last_modified DEFAULT(getdate())
	, [Reason_Code_Id] INT NULL
	, [ManuallyPickCount] INT NULL
	, CONSTRAINT [PK_PickOrder] PRIMARY KEY ([PickOrder_Id] ASC, [Order_ID] ASC, [RouteId] ASC)
	)

END
/* TableDDL - [BUSDTA].[PickOrder] - End */

/* SHADOW TABLE FOR [BUSDTA].[PickOrder] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder_del]
	(
	  [PickOrder_Id] INT
	, [Order_ID] INT
	, [RouteId] NUMERIC
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PickOrder_Id] ASC, [Order_ID] ASC, [RouteId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[PickOrder] - End */
/* TRIGGERS FOR PickOrder - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PickOrder_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PickOrder_ins
	ON BUSDTA.PickOrder AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PickOrder_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PickOrder_del.Order_ID= inserted.Order_ID AND BUSDTA.PickOrder_del.PickOrder_Id= inserted.PickOrder_Id AND BUSDTA.PickOrder_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PickOrder_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_upd
	ON BUSDTA.PickOrder AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PickOrder
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PickOrder.Order_ID= inserted.Order_ID AND BUSDTA.PickOrder.PickOrder_Id= inserted.PickOrder_Id AND BUSDTA.PickOrder.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PickOrder_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PickOrder_dlt
	ON BUSDTA.PickOrder AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PickOrder_del (Order_ID, PickOrder_Id, RouteId, last_modified )
	SELECT deleted.Order_ID, deleted.PickOrder_Id, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PickOrder - END */

