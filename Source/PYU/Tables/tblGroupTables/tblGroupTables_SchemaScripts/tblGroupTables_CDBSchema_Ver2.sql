



/* TableDDL - [dbo].[tblGroupTables] - Start */
IF OBJECT_ID('[dbo].[tblGroupTables]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGroupTables]
	(
	  [GroupTableID] INT NOT NULL IDENTITY(1,1)
	, [RefDBSchema] VARCHAR(128) NULL
	, [RefDBTable] VARCHAR(128) NULL
	, [ApplicationGroupID] INT NULL
	, [ConDBTable] VARCHAR(128) NULL
	, [IsMandatorySync] BIT NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLGROUPTABLES_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGroupTables] PRIMARY KEY ([GroupTableID] ASC)
	)

ALTER TABLE [dbo].[tblGroupTables] WITH CHECK ADD CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID] FOREIGN KEY([ApplicationGroupID]) REFERENCES [dbo].[tblApplicationGroup] ([ApplicationGroupID])
ALTER TABLE [dbo].[tblGroupTables] CHECK CONSTRAINT [FK_tblGroupTables_tblApplicationGroup_ApplicationGroupID]

END
/* TableDDL - [dbo].[tblGroupTables] - End */
