------- /*[BUSDTA].[Route_Device_Map]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."Route_Device_Map"     (
	"Route_Id" varchar(8) not null,
	"Device_Id" varchar(30) not null,
	"Active" integer null,
	"Remote_Id" varchar(30) null,
	PRIMARY KEY ("Route_Id", "Device_Id")
)

------- /*[BUSDTA].[Route_Device_Map]*/ - End