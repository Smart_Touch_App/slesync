

/* [BUSDTA].[MasterNotification] - begins */

/* TableDDL - [BUSDTA].[MasterNotification] - Start */

IF OBJECT_ID('[BUSDTA].[MasterNotification]') IS NULL
BEGIN

CREATE TABLE [BUSDTA].[MasterNotification]	
(	  
[MasterNotificationID] DECIMAL(15,0) NOT NULL	
, [RouteID] NUMERIC(8,0) NOT NULL	
, [Title] NVARCHAR(200) NOT NULL	
, [Description] NVARCHAR(300) NOT NULL	
, [TypeID] NUMERIC(4,0) NULL	
, [TransactionID] DECIMAL(15,0) NULL
, [Status] NUMERIC(4,0) NULL	
, [Category] NUMERIC(4,0) NULL	
, [Read] NVARCHAR(1) NULL CONSTRAINT DF_MASTERNOTIFICATION_Read DEFAULT('N')	
, [ValidTill] DATE NULL	, [Acknowledged] NVARCHAR(1) NULL CONSTRAINT DF_MASTERNOTIFICATION_Acknowledged DEFAULT('N')	
, [Initiator] NUMERIC(8,0) NULL	, [ResponseType] NUMERIC(3,0) NULL	
, [ResponseValue] CHAR(10) NULL	, [CreatedBy] NUMERIC(8,0) NULL	
, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_MASTERNOTIFICATION_CreatedDatetime DEFAULT(getdate())	
, [UpdatedBy] NUMERIC(8,0) NULL	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_MASTERNOTIFICATION_UpdatedDatetime DEFAULT(getdate())	
, last_modified DATETIME DEFAULT GETDATE()	, CONSTRAINT [PK_MasterNotification] PRIMARY KEY ([MasterNotificationID] ASC, [RouteID] ASC)	
)END
/* TableDDL - [BUSDTA].[MasterNotification] - End */


/* SHADOW TABLE FOR [BUSDTA].[MasterNotification] - Start */
IF OBJECT_ID('[BUSDTA].[MasterNotification_del]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[MasterNotification_del]	
(	  
[MasterNotificationID] DECIMAL(15,0)	
, [RouteID] NUMERIC(8,0)	
, last_modified DATETIME DEFAULT GETDATE()	
, PRIMARY KEY ([MasterNotificationID] ASC, [RouteID] ASC)	
)END


/* SHADOW TABLE FOR [BUSDTA].[MasterNotification] - End */
/* TRIGGERS FOR MasterNotification - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.MasterNotification_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.MasterNotification_ins
	ON BUSDTA.MasterNotification AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.MasterNotification_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.MasterNotification_del.MasterNotificationID= inserted.MasterNotificationID AND BUSDTA.MasterNotification_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.MasterNotification_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MasterNotification_upd
	ON BUSDTA.MasterNotification AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.MasterNotification
	SET last_modified = GETDATE()
	FROM inserted
	WHERE BUSDTA.MasterNotification.MasterNotificationID= inserted.MasterNotificationID AND BUSDTA.MasterNotification.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.MasterNotification_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MasterNotification_dlt
	ON BUSDTA.MasterNotification AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.MasterNotification_del (MasterNotificationID, RouteID, last_modified )
	SELECT deleted.MasterNotificationID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO