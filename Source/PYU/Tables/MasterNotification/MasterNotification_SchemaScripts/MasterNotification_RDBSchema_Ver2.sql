

IF OBJECT_ID('[BUSDTA].[MasterNotification]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[MasterNotification]	
(
[MasterNotificationID] DECIMAL(15,0) NOT NULL	
, [RouteID] NUMERIC(8,0) NOT NULL	
, [Title] NVARCHAR(200) NOT NULL	
, [Description] NVARCHAR(300) NOT NULL	
, [TypeID] NUMERIC(4,0) NULL	
, [TransactionID] DECIMAL(15,0) NULL	
, [Status] NUMERIC(4,0) NULL	
, [Category] NUMERIC(4,0) NULL	
, [Read] NVARCHAR(1) NULL DEFAULT('N')	
, [ValidTill] DATE NULL	
, [Acknowledged] NVARCHAR(1) NULL DEFAULT('N')	
, [Initiator] NUMERIC(8,0) NULL	
, [ResponseType] NUMERIC(3,0) NULL	
, [ResponseValue] CHAR(10) NULL	
, [CreatedBy] NUMERIC(8,0) NULL	
, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())	
, [UpdatedBy] NUMERIC(8,0) NULL	
, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())	
, PRIMARY KEY ([MasterNotificationID] ASC, [RouteID] ASC)	
)
END