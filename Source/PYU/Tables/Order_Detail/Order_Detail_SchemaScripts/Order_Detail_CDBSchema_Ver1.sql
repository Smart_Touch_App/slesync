/*
/* [BUSDTA].[Order_Detail] - begins */

/* TableDDL - [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail]
	(
	  [OrderDetailId] NUMERIC(8,0) NOT NULL
	, [OrderID] NUMERIC(8,0) NOT NULL
	, [RouteID] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [OrderQty] NUMERIC(4,0) NOT NULL
	, [OrderUM] NCHAR(2) NULL
	, [UnitPriceAmt] NUMERIC(8,4) NULL
	, [ExtnPriceAmt] NUMERIC(8,4) NULL
	, [ItemSalesTaxAmt] NUMERIC(8,4) NULL
	, [PriceOverrideReasonCodeId] NUMERIC(3,0) NULL
	, [IsTaxable] NCHAR(1) NULL
	, [ReturnHeldQty] NUMERIC(4,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_ORDER_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Order_Detail] PRIMARY KEY ([OrderDetailId] ASC, [OrderID] ASC, [RouteID] ASC)
	)

END
/* TableDDL - [BUSDTA].[Order_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail_del]
	(
	  [OrderDetailId] NUMERIC(8,0)
	, [OrderID] NUMERIC(8,0)
	, [RouteID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([OrderDetailId] ASC, [OrderID] ASC, [RouteID] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Order_Detail] - End */
/* TRIGGERS FOR Order_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Order_Detail_ins
	ON BUSDTA.Order_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Order_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Order_Detail_del.OrderDetailId= inserted.OrderDetailId AND BUSDTA.Order_Detail_del.OrderID= inserted.OrderID AND BUSDTA.Order_Detail_del.RouteID= inserted.RouteID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Detail_upd
	ON BUSDTA.Order_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Order_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Order_Detail.OrderDetailId= inserted.OrderDetailId AND BUSDTA.Order_Detail.OrderID= inserted.OrderID AND BUSDTA.Order_Detail.RouteID= inserted.RouteID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Order_Detail_dlt
	ON BUSDTA.Order_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Order_Detail_del (OrderDetailId, OrderID, RouteID, last_modified )
	SELECT deleted.OrderDetailId, deleted.OrderID, deleted.RouteID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Order_Detail - END */
*/
