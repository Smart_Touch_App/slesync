 
SELECT "BUSDTA"."Order_Detail"."Order_Detail_Id"
,"BUSDTA"."Order_Detail"."Order_ID"
,"BUSDTA"."Order_Detail"."Item_Number"
,"BUSDTA"."Order_Detail"."Order_Qty"
,"BUSDTA"."Order_Detail"."Order_UOM"
,"BUSDTA"."Order_Detail"."Unit_Price"
,"BUSDTA"."Order_Detail"."Extn_Price"
,"BUSDTA"."Order_Detail"."Reason_Code"
,"BUSDTA"."Order_Detail"."IsTaxable"

FROM BUSDTA.F56M0001 , BUSDTA.F90CA003 , BUSDTA.F90CA086, BUSDTA.Order_Header, BUSDTA.Order_Detail
WHERE "BUSDTA"."Order_Detail"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and Customer_Id=CRCRAN8 and BUSDTA.Order_Header.Order_ID = busdta.Order_Detail.Order_ID and FFUSER= {ml s.username}
 
