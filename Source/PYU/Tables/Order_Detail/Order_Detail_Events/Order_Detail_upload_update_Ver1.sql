/*
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_Detail"
SET "ItemId" = {ml r."ItemId"}, "OrderQty" = {ml r."OrderQty"}, "OrderUM" = {ml r."OrderUM"}, "UnitPriceAmt" = {ml r."UnitPriceAmt"}, "ExtnPriceAmt" = {ml r."ExtnPriceAmt"}, "ItemSalesTaxAmt" = {ml r."ItemSalesTaxAmt"}, "PriceOverrideReasonCodeId" = {ml r."PriceOverrideReasonCodeId"}, "IsTaxable" = {ml r."IsTaxable"}, "ReturnHeldQty" = {ml r."ReturnHeldQty"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "OrderDetailId" = {ml r."OrderDetailId"} AND "OrderID" = {ml r."OrderID"} AND "RouteID" = {ml r."RouteID"}
 
*/
