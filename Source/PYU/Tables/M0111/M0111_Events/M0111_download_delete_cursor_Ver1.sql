SELECT "BUSDTA"."M0111_del"."CDAN8",
	"BUSDTA"."M0111_del"."CDIDLN",
	"BUSDTA"."M0111_del"."CDRCK7",
	"BUSDTA"."M0111_del"."CDCNLN",
    "BUSDTA"."M0111_del"."CDID"
FROM "BUSDTA"."M0111_del"
WHERE "BUSDTA"."M0111_del"."last_modified" >= {ml s.last_table_download}