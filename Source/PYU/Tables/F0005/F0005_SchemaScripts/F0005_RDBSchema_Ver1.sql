
------- /*[BUSDTA].[F0005]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F0005"     (
	"DRSY" nvarchar(4) not null,
	"DRRT" nvarchar(2) not null,
	"DRKY" nvarchar(10) not null,
	"DRDL01" nvarchar(30) null,
	"DRDL02" nvarchar(30) null,
	"DRSPHD" nvarchar(10) null,
	"DRHRDC" nvarchar(1) null,
	"DRYN" bit null default ((1)),
	PRIMARY KEY ("DRSY", "DRRT", "DRKY")
)

------- /*[BUSDTA].[F0005]*/ - End