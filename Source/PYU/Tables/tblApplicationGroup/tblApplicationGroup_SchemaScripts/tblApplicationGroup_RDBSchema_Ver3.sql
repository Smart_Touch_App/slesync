/* [BUSDTA].[tblApplicationGroup] - begins */

/* TableDDL - [BUSDTA].[tblApplicationGroup] - Start */
IF OBJECT_ID('[BUSDTA].[tblApplicationGroup]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblApplicationGroup]
	(
	  [ApplicationGroupID] INTEGER NOT NULL default autoincrement
	, [ApplicationGroup] VARCHAR(128) NULL
	, [SortOrder] INTEGER NULL
	, [IsMandatorySync] BIT NULL

	, PRIMARY KEY ([ApplicationGroupID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblApplicationGroup] - End */
