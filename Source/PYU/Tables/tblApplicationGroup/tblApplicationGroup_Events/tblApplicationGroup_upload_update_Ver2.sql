 
/* Update the row in the consolidated database. */
UPDATE ."dbo"."tblApplicationGroup"
SET "ApplicationGroup" = {ml r."ApplicationGroup"}, "SortOrder" = {ml r."SortOrder"}, "IsMandatorySync" = {ml r."IsMandatorySync"}
WHERE "ApplicationGroupID" = {ml r."ApplicationGroupID"}
 
