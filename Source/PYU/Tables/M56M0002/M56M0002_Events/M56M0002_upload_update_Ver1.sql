/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0002"
SET "DCISST" = {ml r."DCISST"}, "DCCRBY" = {ml r."DCCRBY"}, "DCCRDT" = {ml r."DCCRDT"}, "DCUPBY" = {ml r."DCUPBY"}, "DCUPDT" = {ml r."DCUPDT"}
WHERE "DCDDC" = {ml r."DCDDC"} AND "DCDN" = {ml r."DCDN"} AND "DCWN" = {ml r."DCWN"}