
------- /*[BUSDTA].[M56M0002]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M56M0002"     (
	"DCDDC" varchar(3) not null,
	"DCWN" numeric(4, 0) not null,
	"DCDN" numeric(4, 0) not null,
	"DCISST" numeric(4, 0) null default ((1)),
	"DCCRBY" nvarchar(50) null,
	"DCCRDT" datetime null,
	"DCUPBY" nvarchar(50) null,
	"DCUPDT" datetime null,
	PRIMARY KEY ("DCDDC", "DCWN", "DCDN")
)

------- /*[BUSDTA].[M56M0002]*/ - End