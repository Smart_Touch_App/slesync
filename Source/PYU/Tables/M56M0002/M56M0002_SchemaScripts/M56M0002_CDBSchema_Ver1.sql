
/* [BUSDTA].[M56M0002] - begins */

/* TableDDL - [BUSDTA].[M56M0002] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0002]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0002]	(	  [DCDDC] VARCHAR(3) NOT NULL	, [DCWN] NUMERIC NOT NULL	, [DCDN] NUMERIC NOT NULL	, [DCISST] NUMERIC NULL	, [DCCRBY] NCHAR(50) NULL	, [DCCRDT] DATETIME NULL	, [DCUPBY] NCHAR(50) NULL	, [DCUPDT] DATETIME NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M56M0002_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_M56M0002] PRIMARY KEY ([DCDDC] ASC, [DCWN] ASC, [DCDN] ASC)	)
END
/* TableDDL - [BUSDTA].[M56M0002] - End */

/* SHADOW TABLE FOR [BUSDTA].[M56M0002] - Start */
IF OBJECT_ID('[BUSDTA].[M56M0002_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M56M0002_del]	(	  [DCDDC] VARCHAR(3)	, [DCWN] NUMERIC	, [DCDN] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([DCDDC] ASC, [DCWN] ASC, [DCDN] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[M56M0002] - End */
/* TRIGGERS FOR M56M0002 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M56M0002_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M56M0002_ins
	ON BUSDTA.M56M0002 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M56M0002_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M56M0002_del.DCDDC= inserted.DCDDC AND BUSDTA.M56M0002_del.DCDN= inserted.DCDN AND BUSDTA.M56M0002_del.DCWN= inserted.DCWN
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M56M0002_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0002_upd
	ON BUSDTA.M56M0002 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M56M0002
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M56M0002.DCDDC= inserted.DCDDC AND BUSDTA.M56M0002.DCDN= inserted.DCDN AND BUSDTA.M56M0002.DCWN= inserted.DCWN');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M56M0002_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M56M0002_dlt
	ON BUSDTA.M56M0002 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M56M0002_del (DCDDC, DCDN, DCWN, last_modified )
	SELECT deleted.DCDDC, deleted.DCDN, deleted.DCWN, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M56M0002 - END */
