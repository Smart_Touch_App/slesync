
/* [BUSDTA].[Prospect_Quote_Header] - begins */

/* TableDDL - [BUSDTA].[Prospect_Quote_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Header]
	(
	  [ProspectQuoteId] NUMERIC(8,0) NOT NULL
	, [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(3,0) NOT NULL
	, [IsPrinted] NVARCHAR (1) NULL
	, [IsSampled] NVARCHAR (1) NULL
	, [SettlementId] NUMERIC(8,0) NOT NULL
	, [PickStatus] NVARCHAR (1) NULL
	, [IsMasterSetup] NVARCHAR (1) NULL
	, [AIAC05District] NVARCHAR (3) NULL
	, [AIAC05DistrictMst] NVARCHAR (3) NULL
	, [AIAC06Region] NVARCHAR (3) NULL
	, [AIAC06RegionMst] NVARCHAR (3) NULL
	, [AIAC07Reporting] NVARCHAR (3) NULL
	, [AIAC07ReportingMst] NVARCHAR (3) NULL
	, [AIAC08Chain] NVARCHAR (3) NULL
	, [AIAC08ChainMst] NVARCHAR (3) NULL
	, [AIAC09BrewmaticAgentCode] NVARCHAR (3) NULL
	, [AIAC09BrewmaticAgentCodeMst] NVARCHAR (3) NULL
	, [AIAC10NTR] NVARCHAR (3) NULL
	, [AIAC10NTRMst] NVARCHAR (3) NULL
	, [AIAC11CustomerTaxGrp] NVARCHAR (3) NULL
	, [AIAC11CustomerTaxGrpMst] NVARCHAR (3) NULL
	, [AIAC12CategoryCode12] NVARCHAR (3) NULL
	, [AIAC12CategoryCode12Mst] NVARCHAR (3) NULL
	, [AIAC13APCheckCode] NVARCHAR (3) NULL
	, [AIAC13APCheckCodeMst] NVARCHAR (3) NULL
	, [AIAC14CategoryCode14] NVARCHAR (3) NULL
	, [AIAC14CategoryCode14Mst] NVARCHAR (3) NULL
	, [AIAC15CategoryCode15] NVARCHAR (3) NULL
	, [AIAC15CategoryCode15Mst] NVARCHAR (3) NULL
	, [AIAC16CategoryCode16] NVARCHAR (3) NULL
	, [AIAC16CategoryCode16Mst] NVARCHAR (3) NULL
	, [AIAC17CategoryCode17] NVARCHAR (3) NULL
	, [AIAC17CategoryCode17Mst] NVARCHAR (3) NULL
	, [AIAC18CategoryCode18] NVARCHAR (3) NULL
	, [AIAC18CategoryCode18Mst] NVARCHAR (3) NULL
	, [AIAC22POSUpCharge] NVARCHAR (3) NULL
	, [AIAC22POSUpChargeMst] NVARCHAR (3) NULL
	, [AIAC23LiquidCoffee] NVARCHAR (3) NULL
	, [AIAC23LiquidCoffeeMst] NVARCHAR (3) NULL
	, [AIAC24PriceProtection] NVARCHAR (3) NULL
	, [AIAC24PriceProtectionMst] NVARCHAR (3) NULL
	, [AIAC27AlliedDiscount] NVARCHAR (3) NULL
	, [AIAC27AlliedDiscountMst] NVARCHAR (3) NULL
	, [AIAC28CoffeeVolume] NVARCHAR (3) NULL
	, [AIAC28CoffeeVolumeMst] NVARCHAR (3) NULL
	, [AIAC29EquipmentProgPts] NVARCHAR (3) NULL
	, [AIAC29EquipmentProgPtsMst] NVARCHAR (3) NULL
	, [AIAC30SpecialCCP] NVARCHAR (3) NULL
	, [AIAC30SpecialCCPMst] NVARCHAR (3) NULL
	, [PriceDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ProspectQuoteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Prospect_Quote_Header] - End */
