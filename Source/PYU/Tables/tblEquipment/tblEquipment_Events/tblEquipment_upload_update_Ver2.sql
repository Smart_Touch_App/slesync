 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."tblEquipment"
SET "EquipmentType" = {ml r."EquipmentType"}, "EquipmentCategoryID" = {ml r."EquipmentCategoryID"}, "EquipmentSubCategory" = {ml r."EquipmentSubCategory"}, "EquipmentQuantity" = {ml r."EquipmentQuantity"}, "EquipmentOwned" = {ml r."EquipmentOwned"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "EquipmentID" = {ml r."EquipmentID"} AND "ProspectID" = {ml r."ProspectID"}

