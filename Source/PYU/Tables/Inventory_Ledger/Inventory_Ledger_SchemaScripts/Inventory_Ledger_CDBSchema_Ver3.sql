
/* [BUSDTA].[Inventory_Ledger] - begins */

/* TableDDL - [BUSDTA].[Inventory_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Ledger]
	(
	  [InventoryLedgerID] DECIMAL(15,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionQty] DECIMAL(15,0) NULL
	, [TransactionQtyUM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUM] NCHAR(2) NULL
	, [TransactionType] NUMERIC(8,0) NULL
	, [TransactionId] DECIMAL(15,0) NULL
	, [SettlementID] DECIMAL(15,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_INVENTORY_LEDGER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Inventory_Ledger] PRIMARY KEY ([InventoryLedgerID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Inventory_Ledger] - End */

/* SHADOW TABLE FOR [BUSDTA].[Inventory_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Ledger_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Ledger_del]
	(
	  [InventoryLedgerID] DECIMAL(15,0)
	, [ItemId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([InventoryLedgerID] ASC, [ItemId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Inventory_Ledger] - End */
/* TRIGGERS FOR Inventory_Ledger - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Inventory_Ledger_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Inventory_Ledger_ins
	ON BUSDTA.Inventory_Ledger AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Inventory_Ledger_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Inventory_Ledger_del.InventoryLedgerID= inserted.InventoryLedgerID AND BUSDTA.Inventory_Ledger_del.ItemId= inserted.ItemId AND BUSDTA.Inventory_Ledger_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Inventory_Ledger_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Ledger_upd
	ON BUSDTA.Inventory_Ledger AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Inventory_Ledger
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Inventory_Ledger.InventoryLedgerID= inserted.InventoryLedgerID AND BUSDTA.Inventory_Ledger.ItemId= inserted.ItemId AND BUSDTA.Inventory_Ledger.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Inventory_Ledger_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Inventory_Ledger_dlt
	ON BUSDTA.Inventory_Ledger AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Inventory_Ledger_del (InventoryLedgerID, ItemId, RouteId, last_modified )
	SELECT deleted.InventoryLedgerID, deleted.ItemId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Inventory_Ledger - END */

