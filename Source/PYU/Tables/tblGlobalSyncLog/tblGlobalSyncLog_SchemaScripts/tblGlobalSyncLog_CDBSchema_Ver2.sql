/* [dbo].[tblGlobalSyncLog] - begins */

/* TableDDL - [dbo].[tblGlobalSyncLog] - Start */
IF OBJECT_ID('[dbo].[tblGlobalSyncLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGlobalSyncLog]
	(
	  [GlobalSyncLogID] INT NOT NULL IDENTITY(1,1)
	, [GlobalSyncID] INT NULL
	, [DeviceID] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLGLOBALSYNCLOG_CreatedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLGLOBALSYNCLOG_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGlobalSyncLog] PRIMARY KEY ([GlobalSyncLogID] ASC)
	)

ALTER TABLE [dbo].[tblGlobalSyncLog] WITH CHECK ADD CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync] FOREIGN KEY([GlobalSyncID]) REFERENCES [dbo].[tblGlobalSync] ([GlobalSyncID])
ALTER TABLE [dbo].[tblGlobalSyncLog] CHECK CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync]

END
/* TableDDL - [dbo].[tblGlobalSyncLog] - End */

/* SHADOW TABLE FOR [dbo].[tblGlobalSyncLog] - Start */
IF OBJECT_ID('[dbo].[tblGlobalSyncLog_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGlobalSyncLog_del]
	(
	  [GlobalSyncLogID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([GlobalSyncLogID] ASC)
	)

END
/* SHADOW TABLE FOR [dbo].[tblGlobalSyncLog] - End */
/* TRIGGERS FOR tblGlobalSyncLog - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblGlobalSyncLog_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblGlobalSyncLog_ins
	ON dbo.tblGlobalSyncLog AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblGlobalSyncLog_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblGlobalSyncLog_del.GlobalSyncLogID= inserted.GlobalSyncLogID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblGlobalSyncLog_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblGlobalSyncLog_upd
	ON dbo.tblGlobalSyncLog AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblGlobalSyncLog
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblGlobalSyncLog.GlobalSyncLogID= inserted.GlobalSyncLogID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblGlobalSyncLog_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblGlobalSyncLog_dlt
	ON dbo.tblGlobalSyncLog AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblGlobalSyncLog_del (GlobalSyncLogID, last_modified )
	SELECT deleted.GlobalSyncLogID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblGlobalSyncLog - END */
