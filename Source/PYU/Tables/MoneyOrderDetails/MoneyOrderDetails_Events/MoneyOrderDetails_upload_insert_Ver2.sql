 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."MoneyOrderDetails"
("MoneyOrderId", "MoneyOrderNumber", "RouteId", "MoneyOrderAmount", "MoneyOrderFeeAmount", "StatusId", "MoneyOrderDatetime", "VoidReasonId", "RouteSettlementId", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."MoneyOrderId"}, {ml r."MoneyOrderNumber"}, {ml r."RouteId"}, {ml r."MoneyOrderAmount"}, {ml r."MoneyOrderFeeAmount"}, {ml r."StatusId"}, {ml r."MoneyOrderDatetime"}, {ml r."VoidReasonId"}, {ml r."RouteSettlementId"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
