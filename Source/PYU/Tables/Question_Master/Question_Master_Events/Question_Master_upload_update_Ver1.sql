 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Question_Master"
SET "QuestionTitle" = {ml r."QuestionTitle"}, "QuestionDescription" = {ml r."QuestionDescription"}, "IsMandatory" = {ml r."IsMandatory"}, "IsMultivalue" = {ml r."IsMultivalue"}, "IsDescriptive" = {ml r."IsDescriptive"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "QuestionId" = {ml r."QuestionId"}
 
