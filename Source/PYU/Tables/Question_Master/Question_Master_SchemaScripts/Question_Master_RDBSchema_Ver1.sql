/* [BUSDTA].[Question_Master] - begins */

/* TableDDL - [BUSDTA].[Question_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Master]
	(
	  [QuestionId] NUMERIC NOT NULL
	, [QuestionTitle] NCHAR(100) NULL
	, [QuestionDescription] NCHAR(100) NULL
	, [IsMandatory] BIT NULL
	, [IsMultivalue] BIT NULL
	, [IsDescriptive] BIT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([QuestionId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Question_Master] - End */
