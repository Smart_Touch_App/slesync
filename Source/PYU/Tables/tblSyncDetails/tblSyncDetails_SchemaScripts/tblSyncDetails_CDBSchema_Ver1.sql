/* [dbo].[tblSyncDetails] - begins */

/* TableDDL - [dbo].[tblSyncDetails] - Start */
IF OBJECT_ID('[dbo].[tblSyncDetails]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblSyncDetails]
	(
	  [LastSyncID] INT NOT NULL IDENTITY(1,1)
	, [Objectname] VARCHAR(50) NULL
	, [LastSyncVersion] INT NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCDETAILS_EditedOn DEFAULT(getdate())
	)

END
/* TableDDL - [dbo].[tblSyncDetails] - End */
