
------- /*[BUSDTA].[F90CA042]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F90CA042"     (
	"EMAN8" numeric(8, 0) not null,
	"EMPA8" numeric(8, 0) not null,
	--"EMEMH" float null,
	--"EMEDATE" datetime null,
	--"EMUSER" nvarchar(10) null,
	--"EMUDTTM" datetime null,
	--"EMMKEY" nvarchar(15) null,
	--"EMENTDBY" float null,
	PRIMARY KEY ("EMAN8", "EMPA8")
)

------- /*[BUSDTA].[F90CA042]*/ - End