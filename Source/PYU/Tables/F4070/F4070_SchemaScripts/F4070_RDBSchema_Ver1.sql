------- /*[BUSDTA].[F4070]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4070"     (
	"SNASN" nvarchar(8) not null,
	"SNOSEQ" numeric(4, 0) not null,
	"SNANPS" numeric(8, 0) not null,
	"SNAST" nvarchar(8) null,
	"SNEFTJ" numeric(18, 0) null,
	"SNEXDJ" numeric(18, 0) null,
	PRIMARY KEY ("SNASN", "SNOSEQ", "SNANPS")
)

------- /*[BUSDTA].[F4070]*/ - End