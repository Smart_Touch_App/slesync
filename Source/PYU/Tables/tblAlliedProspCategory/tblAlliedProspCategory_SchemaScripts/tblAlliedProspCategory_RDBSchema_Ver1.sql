
/* [dbo].[tblAlliedProspCategory] - begins */

/* TableDDL - [dbo].[tblAlliedProspCategory] - Start */
IF OBJECT_ID('[dbo].[tblAlliedProspCategory]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblAlliedProspCategory]
	(
	  [CategoryID] INTEGER NOT NULL 
	, [Category] VARCHAR(500) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CategoryID] ASC)
	)
END
/* TableDDL - [dbo].[tblAlliedProspCategory] - End */

