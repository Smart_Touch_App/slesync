 
SELECT "dbo"."tblAlliedProspect"."AlliedProspectID"
,"dbo"."tblAlliedProspect"."ProspectID"
,"dbo"."tblAlliedProspect"."CompetitorID"
,"dbo"."tblAlliedProspect"."CategoryID"
,"dbo"."tblAlliedProspect"."SubCategoryID"
,"dbo"."tblAlliedProspect"."BrandID"
,"dbo"."tblAlliedProspect"."UOMID"
,"dbo"."tblAlliedProspect"."PackSize"
,"dbo"."tblAlliedProspect"."CS_PK_LB"
,"dbo"."tblAlliedProspect"."Price"
,"dbo"."tblAlliedProspect"."UsageMeasurementID"
,"dbo"."tblAlliedProspect"."CreatedBy"
,"dbo"."tblAlliedProspect"."CreatedDatetime"
,"dbo"."tblAlliedProspect"."UpdatedBy"
,"dbo"."tblAlliedProspect"."UpdatedDatetime"

FROM "dbo"."tblAlliedProspect"
WHERE "dbo"."tblAlliedProspect"."last_modified">= {ml s.last_table_download}
