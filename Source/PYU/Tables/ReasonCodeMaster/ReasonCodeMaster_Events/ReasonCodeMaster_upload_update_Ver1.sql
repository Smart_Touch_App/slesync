 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ReasonCodeMaster"
SET "ReasonCodeId" = {ml r."ReasonCodeId"}, "ReasonCodeDescription" = {ml r."ReasonCodeDescription"}, "ReasonCodeType" = {ml r."ReasonCodeType"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ReasonCode" = {ml r."ReasonCode"}
 
