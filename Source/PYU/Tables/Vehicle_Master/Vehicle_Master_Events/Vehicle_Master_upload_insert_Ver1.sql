 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Vehicle_Master"
("VehicleID", "VINNumber", "VehicleNumber", "VehicleMake", "VehicleColour", "VehicleAxle", "VehicleFuel", "VehicleMilage", "Isactive", "VehicleManufacturingDt", "VehicleExpiryDt", "VehicleOwner", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."VehicleID"}, {ml r."VINNumber"}, {ml r."VehicleNumber"}, {ml r."VehicleMake"}, {ml r."VehicleColour"}, {ml r."VehicleAxle"}, {ml r."VehicleFuel"}, {ml r."VehicleMilage"}, {ml r."Isactive"}, {ml r."VehicleManufacturingDt"}, {ml r."VehicleExpiryDt"}, {ml r."VehicleOwner"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
