/* [BUSDTA].[Vehicle_Master] - begins */

/* TableDDL - [BUSDTA].[Vehicle_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Vehicle_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Vehicle_Master]
	(
	  [VehicleID] NUMERIC NOT NULL
	, [VINNumber] NVARCHAR(20) NOT NULL
	, [VehicleNumber] NUMERIC NULL
	, [VehicleMake] NCHAR(30) NULL
	, [VehicleColour] NCHAR(20) NULL
	, [VehicleAxle] NUMERIC NULL
	, [VehicleFuel] NCHAR(10) NULL
	, [VehicleMilage] NUMERIC NULL
	, [Isactive] BIT NULL
	, [VehicleManufacturingDt] DATETIME NULL
	, [VehicleExpiryDt] DATE NULL
	, [VehicleOwner] NCHAR(25) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_VEHICLE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Vehicle_Master] PRIMARY KEY ([VehicleID] ASC, [VINNumber] ASC)
	)

END
/* TableDDL - [BUSDTA].[Vehicle_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Vehicle_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Vehicle_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Vehicle_Master_del]
	(
	  [VehicleID] NUMERIC
	, [VINNumber] NVARCHAR(20)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([VehicleID] ASC, [VINNumber] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Vehicle_Master] - End */
/* TRIGGERS FOR Vehicle_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Vehicle_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Vehicle_Master_ins
	ON BUSDTA.Vehicle_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Vehicle_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Vehicle_Master_del.VehicleID= inserted.VehicleID AND BUSDTA.Vehicle_Master_del.VINNumber= inserted.VINNumber
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Vehicle_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Vehicle_Master_upd
	ON BUSDTA.Vehicle_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Vehicle_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Vehicle_Master.VehicleID= inserted.VehicleID AND BUSDTA.Vehicle_Master.VINNumber= inserted.VINNumber');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Vehicle_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Vehicle_Master_dlt
	ON BUSDTA.Vehicle_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Vehicle_Master_del (VehicleID, VINNumber, last_modified )
	SELECT deleted.VehicleID, deleted.VINNumber, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Vehicle_Master - END */
