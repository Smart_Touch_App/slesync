/* [BUSDTA].[Entity_Range_Master] - begins */

/* TableDDL - [BUSDTA].[Entity_Range_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Range_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Range_Master]
	(
	  [EntityRangeMasterID] DECIMAL(8,0) NOT NULL
	, [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [RangeStart] DECIMAL(18,0) NULL
	, [RangeEnd] DECIMAL(18,0) NULL
	, [LastAlloted] DECIMAL(18,0) NULL
	, [Size] DECIMAL(8,0) NULL
	, [Source] VARCHAR(100) NULL
	, [Active] BIT NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	
	, PRIMARY KEY ([EntityRangeMasterID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Entity_Range_Master] - End */
