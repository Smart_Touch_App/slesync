/* [BUSDTA].[Entity_Range_Master] - begins */

/* TableDDL - [BUSDTA].[Entity_Range_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Range_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Range_Master]
	(
	  [EntityRangeMasterID] DECIMAL(8,0) NOT NULL
	, [EntityBucketMasterId] DECIMAL(8,0) NOT NULL
	, [RangeStart] DECIMAL(18,0) NULL
	, [RangeEnd] DECIMAL(18,0) NULL
	, [LastAlloted] DECIMAL(18,0) NULL
	, [Size] DECIMAL(8,0) NULL
	, [Source] VARCHAR(100) NULL
	, [Active] BIT NULL
	, [CreatedBy] DECIMAL(8,0) NULL
	, [CreatedDatetime] SMALLDATETIME NULL
	, [UpdatedBy] DECIMAL(8,0) NULL
	, [UpdatedDatetime] SMALLDATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ENTITY_RANGE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Entity_Range_Master] PRIMARY KEY ([EntityRangeMasterID] ASC)
	)

END
/* TableDDL - [BUSDTA].[Entity_Range_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Entity_Range_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Entity_Range_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Entity_Range_Master_del]
	(
	  [EntityRangeMasterID] DECIMAL(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EntityRangeMasterID] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Entity_Range_Master] - End */
/* TRIGGERS FOR Entity_Range_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Entity_Range_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Entity_Range_Master_ins
	ON BUSDTA.Entity_Range_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Entity_Range_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Entity_Range_Master_del.EntityRangeMasterID= inserted.EntityRangeMasterID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Entity_Range_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Range_Master_upd
	ON BUSDTA.Entity_Range_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Entity_Range_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Entity_Range_Master.EntityRangeMasterID= inserted.EntityRangeMasterID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Entity_Range_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Entity_Range_Master_dlt
	ON BUSDTA.Entity_Range_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Entity_Range_Master_del (EntityRangeMasterID, last_modified )
	SELECT deleted.EntityRangeMasterID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Entity_Range_Master - END */
