/* [BUSDTA].[Pick_Detail] - begins */

/* TableDDL - [BUSDTA].[Pick_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Pick_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Pick_Detail]
	(
	  [PickDetailID] NUMERIC(8,0) NOT NULL DEFAULT autoincrement
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionID] NUMERIC(8,0) NOT NULL
	, [TransactionDetailID] NUMERIC(8,0) NOT NULL
	, [TransactionTypeId] NUMERIC(8,0) NOT NULL
	, [TransactionStatusId] NUMERIC(8,0) NULL
	, [ItemID] NVARCHAR (25) NULL
	, [TransactionQty] NUMERIC(8,0) NULL
	, [TransactionUOM] NVARCHAR (2) NULL
	, [TransactionQtyPrimaryUOM] NUMERIC(8,0) NULL
	, [PrimaryUOM] NVARCHAR (2) NULL
	, [PickedQty] NUMERIC(8,0) NULL
	, [PickQtyPrimaryUOM] NUMERIC(8,0) NULL
	, [LastScanMode] NUMERIC(3,0) NULL
	, [ItemScanSeq] NUMERIC(3,0) NULL
	, [IsOnHold] NVARCHAR (1) NULL
	, [PickAdjusted] NUMERIC(1,0) NOT NULL DEFAULT (0)
	, [AdjustedQty] NUMERIC(4,0) NOT NULL DEFAULT (0)
	, [ReasonCodeId] NUMERIC(8,0) NULL
	, [ManuallyPickCount] NUMERIC(8,0) NULL
	, [IsInException] NVARCHAR (1) NULL
	, [ExceptionReasonCodeId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT "getdate"()
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([PickDetailID] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Pick_Detail] - End */
