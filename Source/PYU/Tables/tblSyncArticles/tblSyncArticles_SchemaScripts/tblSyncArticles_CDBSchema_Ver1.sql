
/* [dbo].[tblSyncArticles] - begins */

/* TableDDL - [dbo].[tblSyncArticles] - Start */
IF OBJECT_ID('[dbo].[tblSyncArticles]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblSyncArticles]
	(
	  [SyncArticleID] INT NOT NULL IDENTITY(1,1)
	, [PublicationName] VARCHAR(100) NULL
	, [SchemaName] VARCHAR(128) NULL
	, [TableName] VARCHAR(128) NULL
	, [ActiveYN] BIT NULL CONSTRAINT DF_TBLSYNCARTICLES_ActiveYN DEFAULT((1))
	, [CreatedBy] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_CreatedOn DEFAULT(getdate())
	, [EditedBy] VARCHAR(100) NULL
	, [EditedOn] DATETIME NULL CONSTRAINT DF_TBLSYNCARTICLES_EditedOn DEFAULT(getdate())
	, CONSTRAINT [PK_tblSyncArticles] PRIMARY KEY ([SyncArticleID] ASC)
	)

END
/* TableDDL - [dbo].[tblSyncArticles] - End */
