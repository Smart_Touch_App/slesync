/*
/* [BUSDTA].[M04012] - begins */

/* TableDDL - [BUSDTA].[M04012] - Start */
IF OBJECT_ID('[BUSDTA].[M04012]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M04012]
	(
	  [PMAN8] NUMERIC(8,0) NOT NULL
	, [PMALPH] NVARCHAR (40) NULL
	, [PMROUT] NVARCHAR (10) NOT NULL
	, [PMCRBY] NVARCHAR (25) NULL
	, [PMCRDT] DATE NULL
	, [PMUPBY] NVARCHAR (25) NULL
	, [PMUPDT] DATE NULL
	, [PMSRS] INTEGER NULL

	, PRIMARY KEY ([PMAN8] ASC)
	)
END
/* TableDDL - [BUSDTA].[M04012] - End */
*/
