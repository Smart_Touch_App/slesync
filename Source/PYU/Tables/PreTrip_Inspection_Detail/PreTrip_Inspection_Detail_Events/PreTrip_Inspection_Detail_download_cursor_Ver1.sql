 
 
SELECT "BUSDTA"."PreTrip_Inspection_Detail"."PreTripInspectionHeaderId"
,"BUSDTA"."PreTrip_Inspection_Detail"."PreTripInspectionDetailId"
,"BUSDTA"."PreTrip_Inspection_Detail"."RouteId"
,"BUSDTA"."PreTrip_Inspection_Detail"."QuestionId"
,"BUSDTA"."PreTrip_Inspection_Detail"."ResponseID"
,"BUSDTA"."PreTrip_Inspection_Detail"."ResponseReason"
,"BUSDTA"."PreTrip_Inspection_Detail"."CreatedBy"
,"BUSDTA"."PreTrip_Inspection_Detail"."CreatedDatetime"
,"BUSDTA"."PreTrip_Inspection_Detail"."UpdatedBy"
,"BUSDTA"."PreTrip_Inspection_Detail"."UpdatedDatetime"

FROM "BUSDTA"."PreTrip_Inspection_Detail"
WHERE "BUSDTA"."PreTrip_Inspection_Detail"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."PreTrip_Inspection_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 
