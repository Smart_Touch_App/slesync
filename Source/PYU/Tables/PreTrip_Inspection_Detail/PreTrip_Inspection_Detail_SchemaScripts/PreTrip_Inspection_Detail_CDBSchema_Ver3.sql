/* [BUSDTA].[PreTrip_Inspection_Detail] - begins */

/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Detail]
	(
	  [PreTripInspectionHeaderId] decimal (15,0) NOT NULL
	, [PreTripInspectionDetailId] decimal (15,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [QuestionId] NUMERIC(8,0) NOT NULL
	, [ResponseID] NUMERIC(8,0) NULL
	, [ResponseReason] NCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PRETRIP_INSPECTION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_PreTrip_Inspection_Detail] PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [PreTripInspectionDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[PreTrip_Inspection_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[PreTrip_Inspection_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PreTrip_Inspection_Detail_del]
	(
	  [PreTripInspectionHeaderId] decimal (15,0)
	, [PreTripInspectionDetailId] decimal (15,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PreTripInspectionHeaderId] ASC, [PreTripInspectionDetailId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[PreTrip_Inspection_Detail] - End */
/* TRIGGERS FOR PreTrip_Inspection_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.PreTrip_Inspection_Detail_ins
	ON BUSDTA.PreTrip_Inspection_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.PreTrip_Inspection_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.PreTrip_Inspection_Detail_del.PreTripInspectionDetailId= inserted.PreTripInspectionDetailId AND BUSDTA.PreTrip_Inspection_Detail_del.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Detail_upd
	ON BUSDTA.PreTrip_Inspection_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.PreTrip_Inspection_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.PreTrip_Inspection_Detail.PreTripInspectionDetailId= inserted.PreTripInspectionDetailId AND BUSDTA.PreTrip_Inspection_Detail.PreTripInspectionHeaderId= inserted.PreTripInspectionHeaderId AND BUSDTA.PreTrip_Inspection_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.PreTrip_Inspection_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.PreTrip_Inspection_Detail_dlt
	ON BUSDTA.PreTrip_Inspection_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.PreTrip_Inspection_Detail_del (PreTripInspectionDetailId, PreTripInspectionHeaderId, RouteId, last_modified )
	SELECT deleted.PreTripInspectionDetailId, deleted.PreTripInspectionHeaderId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR PreTrip_Inspection_Detail - END */
