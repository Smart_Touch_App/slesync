/*
/* [BUSDTA].[Check_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Check_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Check_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Check_Verification_Detail]
	(
	  [CheckVerificationDetailId] NUMERIC NOT NULL
	, [SettlementDetailId] NUMERIC NOT NULL
	, [CheckDetailsId] NUMERIC NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CHECK_VERIFICATION_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Check_Verification_Detail] PRIMARY KEY ([CheckVerificationDetailId] ASC, [RouteId] ASC)
	)

END
/* TableDDL - [BUSDTA].[Check_Verification_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Check_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Check_Verification_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Check_Verification_Detail_del]
	(
	  [CheckVerificationDetailId] NUMERIC
	, [RouteId] NUMERIC
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CheckVerificationDetailId] ASC, [RouteId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Check_Verification_Detail] - End */
/* TRIGGERS FOR Check_Verification_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Check_Verification_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Check_Verification_Detail_ins
	ON BUSDTA.Check_Verification_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Check_Verification_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Check_Verification_Detail_del.CheckVerificationDetailId= inserted.CheckVerificationDetailId AND BUSDTA.Check_Verification_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Check_Verification_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Check_Verification_Detail_upd
	ON BUSDTA.Check_Verification_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Check_Verification_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Check_Verification_Detail.CheckVerificationDetailId= inserted.CheckVerificationDetailId AND BUSDTA.Check_Verification_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Check_Verification_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Check_Verification_Detail_dlt
	ON BUSDTA.Check_Verification_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Check_Verification_Detail_del (CheckVerificationDetailId, RouteId, last_modified )
	SELECT deleted.CheckVerificationDetailId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Check_Verification_Detail - END */
