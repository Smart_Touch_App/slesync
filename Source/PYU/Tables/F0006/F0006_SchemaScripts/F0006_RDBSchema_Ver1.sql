
------- /*[BUSDTA].[F0006]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F0006"     (
	"MCMCU" nvarchar(12) not null,
	"MCSTYL" nvarchar(2) null,
	"MCLDM" nvarchar(1) null,
	"MCCO" nvarchar(5) null,
	"MCAN8" float null,
	"MCDL01" nvarchar(30) null,
	"MCRP01" nvarchar(3) null,
	"MCRP02" nvarchar(3) null,
	"MCRP03" nvarchar(3) null,
	"MCRP04" nvarchar(3) null,
	"MCRP05" nvarchar(3) null,
	"MCRP06" nvarchar(3) null,
	"MCRP07" nvarchar(3) null,
	"MCRP08" nvarchar(3) null,
	"MCRP09" nvarchar(3) null,
	"MCRP10" nvarchar(3) null,
	"MCRP11" nvarchar(3) null,
	"MCRP12" nvarchar(3) null,
	"MCRP13" nvarchar(3) null,
	"MCRP14" nvarchar(3) null,
	"MCRP15" nvarchar(3) null,
	"MCRP16" nvarchar(3) null,
	"MCRP17" nvarchar(3) null,
	"MCRP18" nvarchar(3) null,
	"MCRP19" nvarchar(3) null,
	"MCRP20" nvarchar(3) null,
	"MCRP21" nvarchar(10) null,
	"MCRP22" nvarchar(10) null,
	"MCRP23" nvarchar(10) null,
	"MCRP24" nvarchar(10) null,
	"MCRP25" nvarchar(10) null,
	"MCRP26" nvarchar(10) null,
	"MCRP27" nvarchar(10) null,
	"MCRP28" nvarchar(10) null,
	"MCRP29" nvarchar(10) null,
	"MCRP30" nvarchar(10) null,
	PRIMARY KEY ("MCMCU")
)

------- /*[BUSDTA].[F0006]*/ - End