SELECT "BUSDTA"."Feature_Code_Mapping"."RequestCode"
,"BUSDTA"."Feature_Code_Mapping"."AuthorizationFormatCode"
,"BUSDTA"."Feature_Code_Mapping"."Feature"
,"BUSDTA"."Feature_Code_Mapping"."RouteId"
,"BUSDTA"."Feature_Code_Mapping"."Description"
,"BUSDTA"."Feature_Code_Mapping"."CreatedBy"
,"BUSDTA"."Feature_Code_Mapping"."CreatedDatetime"
,"BUSDTA"."Feature_Code_Mapping"."UpdatedBy"
,"BUSDTA"."Feature_Code_Mapping"."UpdatedDatetime"

FROM "BUSDTA"."Feature_Code_Mapping"
WHERE "BUSDTA"."Feature_Code_Mapping"."last_modified">= {ml s.last_table_download}