
------- /*[BUSDTA].[F40073]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F40073"     (
	"HYPRFR" nvarchar(2) not null,
	"HYHYID" nvarchar(10) not null,
	"HYHY01" float null,
	"HYHY02" float null,
	"HYHY03" float null,
	"HYHY04" float null,
	"HYHY05" float null,
	"HYHY06" float null,
	"HYHY07" float null,
	"HYHY08" float null,
	"HYHY09" float null,
	"HYHY10" float null,
	"HYHY11" float null,
	"HYHY12" float null,
	"HYHY13" float null,
	"HYHY14" float null,
	"HYHY15" float null,
	"HYHY16" float null,
	"HYHY17" float null,
	"HYHY18" float null,
	"HYHY19" float null,
	"HYHY20" float null,
	"HYHY21" float null,
	PRIMARY KEY ("HYPRFR", "HYHYID")
)

------- /*[BUSDTA].[F40073]*/ - End