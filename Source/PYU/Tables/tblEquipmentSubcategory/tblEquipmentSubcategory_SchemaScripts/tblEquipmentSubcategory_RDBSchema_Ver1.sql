/* [dbo].[tblEquipmentSubcategory] - begins */

/* TableDDL - [dbo].[tblEquipmentSubcategory] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentSubcategory]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentSubcategory]
	(
	  [EquipmentSubcategoryID] INTEGER NOT NULL 
	, [EquipmentCategoryID] INTEGER NULL
	, [EquipmentSubCategory] LONG VARCHAR NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([EquipmentSubcategoryID] ASC)
	)
END
/* TableDDL - [dbo].[tblEquipmentSubcategory] - End */
