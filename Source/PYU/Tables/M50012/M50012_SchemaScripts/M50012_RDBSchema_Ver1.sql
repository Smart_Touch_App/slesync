
------- /*[BUSDTA].[M50012]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M50012"     (	-- TransactionActivityDetail
	"TDID" numeric(8, 0) not null DEFAULT autoincrement,   -- TransactionID
	"TDROUT" varchar(10) not null,                     -- RouteID
	"TDTYP" nvarchar(50) not null,                      -- TransactionType
	"TDCLASS" nvarchar(100) null,                       -- TransactionDetailClass
	"TDDTLS" long nvarchar null,                        -- TransactionDetails
	"TDSTRTTM" datetime null,                           -- TransactionStartTime
	"TDENDTM" datetime null,                            -- TransactionEnd
	"TDSTID" nvarchar(15) null,                         -- StopInstanceID
	"TDAN8" nvarchar(15) null,                          -- CustomerId
	"TDSTTLID" nvarchar(15) null,                       -- SettlementID
	"TDPNTID" numeric(8, 0) null,                       -- ParentTransactionID
	"TDSTAT" nvarchar(100) null,                        -- TransactionStatus
	"TDCRBY" nvarchar(50) null,
	"TDCRDT" datetime null,
	"TDUPBY" nvarchar(50) null,
	"TDUPDT" datetime null,
	PRIMARY KEY ("TDID", "TDROUT")
)

------- /*[BUSDTA].[M50012]*/ - End