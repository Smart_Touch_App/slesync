
------- /*[BUSDTA].[F40941]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F40941"     (
	"IKPRGR" nvarchar(8) not null,
	"IKIGP1" nvarchar(6) not null,
	"IKIGP2" nvarchar(6) not null,
	"IKIGP3" nvarchar(6) not null,
	"IKIGP4" nvarchar(6) not null,
	"IKIGP5" nvarchar(6) not null,
	"IKIGP6" nvarchar(6) not null,
	"IKIGP7" nvarchar(6) not null,
	"IKIGP8" nvarchar(6) not null,
	"IKIGP9" nvarchar(6) not null,
	"IKIGP10" nvarchar(6) not null,
	"IKIGID" float null,
	PRIMARY KEY ("IKPRGR", "IKIGP1", "IKIGP2", "IKIGP3", "IKIGP4", "IKIGP5", "IKIGP6", "IKIGP7", "IKIGP8", "IKIGP9", "IKIGP10")
)

------- /*[BUSDTA].[F40941]*/ - End