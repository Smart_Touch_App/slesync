------- /*[BUSDTA].[F0014]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F0014"     (
	"PNPTC" nvarchar(3) not null,
	"PNPTD" nvarchar(30) null,
	"PNDCP" float null,
	"PNDCD" float null,
	"PNNDTP" float null,
	"PNNSP" float null,
	"PNDTPA" float null,
	"PNPXDM" float null,
	"PNPXDD" float null,
	PRIMARY KEY ("PNPTC")
)

------- /*[BUSDTA].[F0014]*/ - End