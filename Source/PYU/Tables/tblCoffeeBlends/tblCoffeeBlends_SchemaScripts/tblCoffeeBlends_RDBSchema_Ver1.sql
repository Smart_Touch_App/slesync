/* [dbo].[tblCoffeeBlends] - begins */

/* TableDDL - [dbo].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[dbo].[tblCoffeeBlends]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblCoffeeBlends]
	(
	  [CoffeeBlendID] INTEGER NOT NULL 
	, [CoffeeBlend] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([CoffeeBlendID] ASC)
	)
END
/* TableDDL - [dbo].[tblCoffeeBlends] - End */
