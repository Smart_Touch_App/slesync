
/* [BUSDTA].[Inventory] - begins */

/* TableDDL - [BUSDTA].[Inventory] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory]
	(
	  [ItemId] NUMERIC(8,0) NOT NULL
	, [ItemNumber] NVARCHAR (25) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [OnHandQuantity] NUMERIC(4,0) NULL
	, [CommittedQuantity] NUMERIC(4,0) NULL
	, [HeldQuantity] NUMERIC(4,0) NULL
	, [ParLevel] NUMERIC(4,0) NULL
	, [LastReceiptDate] DATETIME NULL
	, [LastConsumeDate] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL	
	, PRIMARY KEY ([ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Inventory] - End */

