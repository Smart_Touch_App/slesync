
     SELECT "BUSDTA"."F0101"."ABAN8"  ,"BUSDTA"."F0101"."ABALKY"  ,"BUSDTA"."F0101"."ABTAX"  ,"BUSDTA"."F0101"."ABALPH"  ,"BUSDTA"."F0101"."ABMCU"  ,
     "BUSDTA"."F0101"."ABSIC"  ,"BUSDTA"."F0101"."ABLNGP"  ,"BUSDTA"."F0101"."ABAT1"  ,"BUSDTA"."F0101"."ABCM"  ,"BUSDTA"."F0101"."ABTAXC"  ,
     "BUSDTA"."F0101"."ABAT2"  ,"BUSDTA"."F0101"."ABAN81"  ,"BUSDTA"."F0101"."ABAN82"  ,"BUSDTA"."F0101"."ABAN83"  ,"BUSDTA"."F0101"."ABAN84"  ,
     "BUSDTA"."F0101"."ABAN86"  ,"BUSDTA"."F0101"."ABAN85"  ,"BUSDTA"."F0101"."ABAC01"  ,"BUSDTA"."F0101"."ABAC02"  ,"BUSDTA"."F0101"."ABAC03"  ,
     "BUSDTA"."F0101"."ABAC04"  ,"BUSDTA"."F0101"."ABAC05"  ,"BUSDTA"."F0101"."ABAC06"  ,"BUSDTA"."F0101"."ABAC07"  ,"BUSDTA"."F0101"."ABAC08"  ,
     "BUSDTA"."F0101"."ABAC09"  ,"BUSDTA"."F0101"."ABAC10"  ,"BUSDTA"."F0101"."ABAC11"  ,"BUSDTA"."F0101"."ABAC12"  ,"BUSDTA"."F0101"."ABAC13"  ,
     "BUSDTA"."F0101"."ABAC14"  ,"BUSDTA"."F0101"."ABAC15"  ,"BUSDTA"."F0101"."ABAC16"  ,"BUSDTA"."F0101"."ABAC17"  ,"BUSDTA"."F0101"."ABAC18"  ,
     "BUSDTA"."F0101"."ABAC19"  ,"BUSDTA"."F0101"."ABAC20"  ,"BUSDTA"."F0101"."ABAC21"  ,"BUSDTA"."F0101"."ABAC22"  ,"BUSDTA"."F0101"."ABAC23"  ,
     "BUSDTA"."F0101"."ABAC24"  ,"BUSDTA"."F0101"."ABAC25"  ,"BUSDTA"."F0101"."ABAC26"  ,"BUSDTA"."F0101"."ABAC27"  ,"BUSDTA"."F0101"."ABAC28"  ,
     "BUSDTA"."F0101"."ABAC29"  ,"BUSDTA"."F0101"."ABAC30"  ,"BUSDTA"."F0101"."ABRMK"  ,"BUSDTA"."F0101"."ABTXCT"  ,"BUSDTA"."F0101"."ABTX2"  ,
     "BUSDTA"."F0101"."ABALP1"   
      FROM "BUSDTA"."F0101", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
      WHERE ("BUSDTA"."F0101"."last_modified" >= {ml s.last_table_download} or rm.last_modified >= {ml s.last_table_download}   
      or crm.last_modified >= {ml s.last_table_download}) and   ABAN8=crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber  
      AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )    