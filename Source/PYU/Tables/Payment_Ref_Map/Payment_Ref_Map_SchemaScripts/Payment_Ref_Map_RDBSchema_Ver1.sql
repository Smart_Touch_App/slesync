------- /*[BUSDTA].[Payment_Ref_Map]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."Payment_Ref_Map"     (
	"Payment_Ref_Map_Id" integer not null DEFAULT autoincrement,
	"Payment_Id" integer not null,
	"Ref_Id" integer not null,
	"Ref_Type" nvarchar(3) null,
	PRIMARY KEY ("Payment_Ref_Map_Id", "Payment_Id", "Ref_Id")
)

------- /*[BUSDTA].[Payment_Ref_Map]*/ - End