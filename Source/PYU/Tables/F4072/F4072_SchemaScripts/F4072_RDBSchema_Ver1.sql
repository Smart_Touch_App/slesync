
------- /*[BUSDTA].[F4072]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4072"     (
	"ADAST" nvarchar(8) not null,
	"ADITM" numeric(8, 0) not null,
	"ADLITM" nvarchar(25) null,
	"ADAITM" nvarchar(25) null,
	"ADAN8" numeric(8, 0) not null,
	"ADIGID" numeric(8, 0) not null,
	"ADCGID" numeric(8, 0) not null,
	"ADOGID" numeric(8, 0) not null,
	"ADCRCD" nvarchar(3) not null,
	"ADUOM" nvarchar(2) not null,
	"ADMNQ" numeric(15, 0) not null,
	"ADEFTJ" numeric(18, 0) null,
	"ADEXDJ" numeric(18, 0) not null,
	"ADBSCD" nvarchar(1) null,
	"ADLEDG" nvarchar(2) null,
	"ADFRMN" nvarchar(10) null,
	"ADFVTR" float null,
	"ADFGY" nvarchar(1) null,
	"ADATID" float null,
	"ADNBRORD" float null,
	"ADUOMVID" nvarchar(2) null,
	"ADFVUM" nvarchar(2) null,
	"ADPARTFG" nvarchar(1) null,
	"ADAPRS" nvarchar(1) null,
	"ADUPMJ" numeric(18, 0) not null,
	"ADTDAY" numeric(6, 0) not null,
	"ADBKTPID" float null,
	"ADCRCDVID" nvarchar(3) null,
	"ADRULENAME" nvarchar(10) null,
	PRIMARY KEY ("ADAST", "ADITM", "ADAN8", "ADIGID", "ADCGID", "ADOGID", "ADCRCD", "ADUOM", "ADMNQ", "ADEXDJ", "ADUPMJ", "ADTDAY")
)

------- /*[BUSDTA].[F4072]*/ - End