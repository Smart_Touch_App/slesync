 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."","
SET "Entity" = {ml r."Entity"}, "BucketSize" = {ml r."BucketSize"}, "NoOfBuckets" = {ml r."NoOfBuckets"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "EntityBucketMasterId" = {ml r."EntityBucketMasterId"}
 
