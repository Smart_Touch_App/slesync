------- /*[BUSDTA].[M5005]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M5005"     (	-- TransactionSyncMaster
	"SMKEY" varchar(30) not null,						-- TransactionKey
	"SMDIR" nvarchar(10) null,							-- SyncDirection
	"SMPRTY" nvarchar(10) null,							-- SyncPriority
	"SMCRBY" nvarchar(50) null,
	"SMCRDT" datetime null,
	"SMUPBY" nvarchar(50) null,
	"SMUPDT" datetime null,
	PRIMARY KEY ("SMKEY")
)

------- /*[BUSDTA].[M5005]*/ - End