/* [BUSDTA].[Order_Header] - begins */

/* TableDDL - [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header]') IS NULL
BEGIN

CREATE TABLE "BUSDTA"."Order_Header" (
    "OrderID"                        numeric(8,0) NOT NULL
   ,"OrderTypeId"                    numeric(3,0) NULL
   ,"OriginatingRouteID"             numeric(8,0) NOT NULL
   ,"Branch"                         nvarchar(12) NULL
   ,"CustShipToId"                   numeric(8,0) NOT NULL
   ,"CustBillToId"                   numeric(8,0) NOT NULL
   ,"CustParentId"                   numeric(8,0) NOT NULL
   ,"OrderDate"                      date NULL
   ,"TotalCoffeeAmt"                 decimal (15,4) NULL
   ,"TotalAlliedAmt"                 decimal (15,4) NULL
   ,"OrderTotalAmt"                  decimal (15,4) NULL
   ,"SalesTaxAmt"                    decimal (15,4) NULL
   ,"InvoiceTotalAmt"                decimal (15,4) NULL
   ,"EnergySurchargeAmt"             decimal (15,4) NULL
   ,"VarianceAmt"                    decimal (15,4) NULL
   ,"SurchargeReasonCodeId"          numeric(3,0) NULL
   ,"OrderStateId"                   numeric(3,0) NULL
   ,"OrderSignature"                 long binary NULL
   ,"ChargeOnAccountSignature"       long binary NULL
   ,"JDEOrderCompany"                nvarchar(5) NULL
   ,"JDEOrderNumber"                 numeric(8,0) NULL
   ,"JDEOrderType"                   nvarchar(2) NULL
   ,"JDEInvoiceCompany"              nvarchar(5) NULL
   ,"JDEInvoiceNumber"               numeric(8,0) NULL
   ,"JDEOInvoiceType"                nvarchar(2) NULL
   ,"JDEZBatchNumber"                nvarchar(15) NULL
   ,"JDEProcessID"                   numeric(3,0) NULL
   ,"SettlementID"                   decimal (15,0) NULL
   ,"PaymentTerms"                   nvarchar(3) NULL
   ,"CustomerReference1"             nvarchar(25) NULL
   ,"CustomerReference2"             nvarchar(25) NULL
   ,"AdjustmentSchedule"             nvarchar(8) NULL
   ,"TaxArea"                        nvarchar(10) NULL
   ,"TaxExplanationCode"             nvarchar(1) NULL
   ,"VoidReasonCodeId"               numeric(3,0) NULL
   ,"ChargeOnAccount"                nvarchar(1) NULL
   ,"HoldCommitted"                  nvarchar(1) NULL
   ,"CreatedBy"                      numeric(8,0) NULL
   ,"CreatedDatetime"                "datetime" NULL
   ,"UpdatedBy"                      numeric(8,0) NULL
   ,"UpdatedDatetime"                "datetime" NULL
   ,PRIMARY KEY ("OrderID" ASC) 
)	


END
/* TableDDL - [BUSDTA].[Order_Header] - End */
