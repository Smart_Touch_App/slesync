/*
/* [BUSDTA].[Order_Header] - begins */

/* TableDDL - [BUSDTA].[Order_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Header]
	(
	  [Order_ID] INT NOT NULL
	, [Customer_Id] FLOAT NULL
	, [Order_Date] DATE NULL
	, [Created_By] INT NULL
	, [Created_On] DATE NOT NULL
	, [Is_Deleted] BIT NULL
	, [Total_Coffee] FLOAT NULL
	, [Total_Allied] FLOAT NULL
	, [Energy_Surcharge] FLOAT NULL
	, [Order_Total_Amt] FLOAT NULL
	, [Sales_Tax_Amt] FLOAT NULL
	, [Invoice_Total] FLOAT NULL
	, [Surcharge_Reason_Code] VARCHAR(5) NULL
	, [payment_type] NVARCHAR(10) NULL
	, [payment_id] NVARCHAR(10) NULL
	, [Order_State] NVARCHAR(20) NULL
	, [Order_Sub_State] NVARCHAR(20) NULL
	, [updated_at] DATETIME NULL
	, [VoidReason] BIGINT NULL
	, [OrderSeries] INT NOT NULL
	, [RouteNo] NCHAR(3) NULL
	, [ChargeOnAccount] BIT NULL
	, [HoldCommitted] BIT NULL

	, PRIMARY KEY ([Order_ID] ASC, [OrderSeries] ASC)
	)
END
/* TableDDL - [BUSDTA].[Order_Header] - End */
*/
