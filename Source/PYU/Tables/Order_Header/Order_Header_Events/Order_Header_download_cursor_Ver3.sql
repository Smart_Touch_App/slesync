
 
SELECT "BUSDTA"."Order_Header"."OrderID"
,"BUSDTA"."Order_Header"."RouteID"
,"BUSDTA"."Order_Header"."OrderTypeId"
,"BUSDTA"."Order_Header"."CustomerId"
,"BUSDTA"."Order_Header"."OrderDate"
,"BUSDTA"."Order_Header"."TotalCoffeeAmt"
,"BUSDTA"."Order_Header"."TotalAlliedAmt"
,"BUSDTA"."Order_Header"."OrderTotalAmt"
,"BUSDTA"."Order_Header"."SalesTaxAmt"
,"BUSDTA"."Order_Header"."InvoiceTotalAmt"
,"BUSDTA"."Order_Header"."EnergySurchargeAmt"
,"BUSDTA"."Order_Header"."SurchargeReasonCodeId"
,"BUSDTA"."Order_Header"."OrderStateId"
,"BUSDTA"."Order_Header"."OrderSign"
,"BUSDTA"."Order_Header"."ChargeOnAccountSign"
,"BUSDTA"."Order_Header"."VoidReasonCodeId"
,"BUSDTA"."Order_Header"."ChargeOnAccount"
,"BUSDTA"."Order_Header"."HoldCommitted"
,"BUSDTA"."Order_Header"."CreatedBy"
,"BUSDTA"."Order_Header"."CreatedDatetime"
,"BUSDTA"."Order_Header"."UpdatedBy"
,"BUSDTA"."Order_Header"."UpdatedDatetime"

FROM "BUSDTA"."Order_Header"
WHERE "BUSDTA"."Order_Header"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Order_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username}) 
