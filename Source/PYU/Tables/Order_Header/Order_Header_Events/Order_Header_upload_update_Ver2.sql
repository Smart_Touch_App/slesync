 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_Header"
SET "Customer_Id" = {ml r."Customer_Id"}, "Order_Date" = {ml r."Order_Date"}, "Created_By" = {ml r."Created_By"}, "Created_On" = {ml r."Created_On"}, "Is_Deleted" = {ml r."Is_Deleted"}, "Total_Coffee" = {ml r."Total_Coffee"}, "Total_Allied" = {ml r."Total_Allied"}, "Energy_Surcharge" = {ml r."Energy_Surcharge"}, "Order_Total_Amt" = {ml r."Order_Total_Amt"}, "Sales_Tax_Amt" = {ml r."Sales_Tax_Amt"}, "Invoice_Total" = {ml r."Invoice_Total"}, "Surcharge_Reason_Code" = {ml r."Surcharge_Reason_Code"}, "payment_type" = {ml r."payment_type"}, "payment_id" = {ml r."payment_id"}, "Order_State" = {ml r."Order_State"}, "Order_Sub_State" = {ml r."Order_Sub_State"}, "updated_at" = {ml r."updated_at"}, "VoidReason" = {ml r."VoidReason"}, "RouteNo" = {ml r."RouteNo"},  "HoldCommitted" = {ml r."HoldCommitted"}
WHERE "Order_ID" = {ml r."Order_ID"} AND "OrderSeries" = {ml r."OrderSeries"}
 
