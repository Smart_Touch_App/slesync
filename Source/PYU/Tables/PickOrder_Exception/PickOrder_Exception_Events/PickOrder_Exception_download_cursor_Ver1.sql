 
SELECT "BUSDTA"."PickOrder_Exception"."PickOrder_Exception_Id"
,"BUSDTA"."PickOrder_Exception"."Order_Id"
,"BUSDTA"."PickOrder_Exception"."RouteId"
,"BUSDTA"."PickOrder_Exception"."Item_Number"
,"BUSDTA"."PickOrder_Exception"."Exception_Qty"
,"BUSDTA"."PickOrder_Exception"."UOM"
,"BUSDTA"."PickOrder_Exception"."Exception_Reason"
,"BUSDTA"."PickOrder_Exception"."ManualPickReasonCode"
,"BUSDTA"."PickOrder_Exception"."ManuallyPickCount"

FROM  MobileDataModel.BUSDTA.F56M0001, MobileDataModel.BUSDTA.F90CA003, MobileDataModel.BUSDTA.F90CA086, MobileDataModel.BUSDTA.Order_Header, MobileDataModel.BUSDTA.PickOrder_Exception
WHERE "BUSDTA"."PickOrder_Exception"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and Customer_Id=CRCRAN8 and BUSDTA.Order_Header.Order_ID = busdta.PickOrder_Exception.Order_ID and FFUSER= {ml s.username}
 
