/* [BUSDTA].[PickOrder_Exception] - begins */

/* TableDDL - [BUSDTA].[PickOrder_Exception] - Start */
IF OBJECT_ID('[BUSDTA].[PickOrder_Exception]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[PickOrder_Exception]
	(
	  [PickOrder_Exception_Id] INTEGER NOT NULL
	, [Order_Id] INTEGER NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [Item_Number] NVARCHAR (25) NULL
	, [Exception_Qty] INTEGER NULL
	, [UOM] NVARCHAR (2) NULL
	, [Exception_Reason] VARCHAR(25) NULL

	, [ManualPickReasonCode] INTEGER NULL
	, [ManuallyPickCount] INTEGER NULL
	, PRIMARY KEY ([PickOrder_Exception_Id] ASC, [Order_Id] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[PickOrder_Exception] - End */
