
------- /*[BUSDTA].[M56M0001]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M56M0001"     (
	"DMDDC" varchar(3) not null,
	"DMDDCD" nvarchar(50) not null,
	"DMSTAT" bit not null,
	"DMWPC" integer not null,
	"DMCRBY" nvarchar(50) null,
	"DMCRDT" datetime null,
	"DMUPBY" nvarchar(50) null,
	"DMUPDT" datetime null,
	PRIMARY KEY ("DMDDC")
)

------- /*[BUSDTA].[M56M0001]*/ - End