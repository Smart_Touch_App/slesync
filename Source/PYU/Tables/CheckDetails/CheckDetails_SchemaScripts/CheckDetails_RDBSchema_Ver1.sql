/* [BUSDTA].[CheckDetails] - begins */

/* TableDDL - [BUSDTA].[CheckDetails] - Start */
IF OBJECT_ID('[BUSDTA].[CheckDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[CheckDetails]
	(
	  [CheckDetailsId] NUMERIC NOT NULL
	, [CheckDetailsNumber] VARCHAR(10) NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [CheckNumber] NCHAR(10) NULL
	, [CustomerId] NCHAR(10) NULL
	, [CustomerName] NCHAR(10) NULL
	, [CheckAmount] NUMERIC NULL
	, [CheckDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([CheckDetailsId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[CheckDetails] - End */
