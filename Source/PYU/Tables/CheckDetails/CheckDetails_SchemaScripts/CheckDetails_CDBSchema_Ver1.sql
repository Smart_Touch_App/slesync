/* [BUSDTA].[CheckDetails] - begins */

/* TableDDL - [BUSDTA].[CheckDetails] - Start */
IF OBJECT_ID('[BUSDTA].[CheckDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[CheckDetails]
	(
	  [CheckDetailsId] NUMERIC NOT NULL
	, [CheckDetailsNumber] VARCHAR(10) NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [CheckNumber] NCHAR(10) NULL
	, [CustomerId] NCHAR(10) NULL
	, [CustomerName] NCHAR(10) NULL
	, [CheckAmount] NUMERIC NULL
	, [CheckDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_CHECKDETAILS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_CheckDetails] PRIMARY KEY ([CheckDetailsId] ASC, [RouteId] ASC)
	)

END
/* TableDDL - [BUSDTA].[CheckDetails] - End */

/* SHADOW TABLE FOR [BUSDTA].[CheckDetails] - Start */
IF OBJECT_ID('[BUSDTA].[CheckDetails_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[CheckDetails_del]
	(
	  [CheckDetailsId] NUMERIC
	, [RouteId] NUMERIC
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CheckDetailsId] ASC, [RouteId] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[CheckDetails] - End */
/* TRIGGERS FOR CheckDetails - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.CheckDetails_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.CheckDetails_ins
	ON BUSDTA.CheckDetails AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.CheckDetails_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.CheckDetails_del.CheckDetailsId= inserted.CheckDetailsId AND BUSDTA.CheckDetails_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.CheckDetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CheckDetails_upd
	ON BUSDTA.CheckDetails AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.CheckDetails
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.CheckDetails.CheckDetailsId= inserted.CheckDetailsId AND BUSDTA.CheckDetails.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.CheckDetails_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.CheckDetails_dlt
	ON BUSDTA.CheckDetails AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.CheckDetails_del (CheckDetailsId, RouteId, last_modified )
	SELECT deleted.CheckDetailsId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR CheckDetails - END */
