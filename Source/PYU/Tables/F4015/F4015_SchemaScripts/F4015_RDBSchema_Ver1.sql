
------- /*[BUSDTA].[F4015]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4015"     (
	"OTORTP" nvarchar(8) not null,
	"OTAN8" numeric(8, 0) not null,
	"OTOSEQ" numeric(4, 0) not null,
	"OTITM" float null,
	"OTLITM" nvarchar(25) null,
	--"OTAITM" nvarchar(25) null,
	"OTQTYU" float null,
	"OTUOM" nvarchar(2) null,
	"OTLNTY" nvarchar(2) null,
	"OTEFTJ" numeric(18, 0) null,
	"OTEXDJ" numeric(18, 0) null,
	PRIMARY KEY ("OTORTP", "OTAN8", "OTOSEQ")
)

------- /*[BUSDTA].[F4015]*/ - End