/* [BUSDTA].[F40205] - begins */

/* TableDDL - [BUSDTA].[F40205] - Start */
IF OBJECT_ID('[BUSDTA].[F40205]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40205]
	(
	  [LFLNTY] NVARCHAR(2) NOT NULL
	, [LFLNDS] NVARCHAR(30) NULL
	, [LFGLI] NVARCHAR(1) NULL
	, [LFIVI] NVARCHAR(1) NULL
	, [LFARI] NVARCHAR(1) NULL
	, [LFAPI] NVARCHAR(1) NULL
	, [LFRSGN] NVARCHAR(1) NULL
	, [LFTXYN] NVARCHAR(1) NULL
	, [LFPRFT] NVARCHAR(1) NULL
	, [LFCDSC] NVARCHAR(1) NULL
	, [LFTX01] NVARCHAR(1) NULL
	, [LFTX02] NVARCHAR(1) NULL
	, [LFGLC] NVARCHAR(4) NULL
	, [LFPDC1] NVARCHAR(1) NULL
	, [LFPDC2] NVARCHAR(1) NULL
	, [LFPDC3] NVARCHAR(1) NULL
	, [LFCSJ] NVARCHAR(1) NULL
	, [LFDCTO] NVARCHAR(2) NULL
	, [LFART] NVARCHAR(1) NULL
	, [LFAFT] NVARCHAR(1) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_F40205_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F40205] PRIMARY KEY ([LFLNTY] ASC)
	)

END
/* TableDDL - [BUSDTA].[F40205] - End */

/* SHADOW TABLE FOR [BUSDTA].[F40205] - Start */
IF OBJECT_ID('[BUSDTA].[F40205_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F40205_del]
	(
	  [LFLNTY] NVARCHAR(2)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([LFLNTY] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[F40205] - End */
/* TRIGGERS FOR F40205 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F40205_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F40205_ins
	ON BUSDTA.F40205 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F40205_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F40205_del.LFLNTY= inserted.LFLNTY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F40205_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40205_upd
	ON BUSDTA.F40205 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F40205
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F40205.LFLNTY= inserted.LFLNTY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F40205_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F40205_dlt
	ON BUSDTA.F40205 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F40205_del (LFLNTY, last_modified )
	SELECT deleted.LFLNTY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F40205 - END */
