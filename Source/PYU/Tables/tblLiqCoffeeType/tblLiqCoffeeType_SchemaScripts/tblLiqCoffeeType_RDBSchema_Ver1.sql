/* [dbo].[tblLiqCoffeeType] - begins */

/* TableDDL - [dbo].[tblLiqCoffeeType] - Start */
IF OBJECT_ID('[dbo].[tblLiqCoffeeType]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblLiqCoffeeType]
	(
	  [LiqCoffeeTypeID] INTEGER NOT NULL 
	, [LiqCoffeeType] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([LiqCoffeeTypeID] ASC)
	)
END
/* TableDDL - [dbo].[tblLiqCoffeeType] - End */
