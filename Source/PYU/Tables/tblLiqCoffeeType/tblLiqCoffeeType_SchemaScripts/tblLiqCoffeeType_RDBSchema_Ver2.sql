/* [BUSDTA].[tblLiqCoffeeType] - begins */

/* TableDDL - [BUSDTA].[tblLiqCoffeeType] - Start */
IF OBJECT_ID('[BUSDTA].[tblLiqCoffeeType]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblLiqCoffeeType]
	(
	  [LiqCoffeeTypeID] INTEGER NOT NULL 
	, [LiqCoffeeType] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([LiqCoffeeTypeID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblLiqCoffeeType] - End */
