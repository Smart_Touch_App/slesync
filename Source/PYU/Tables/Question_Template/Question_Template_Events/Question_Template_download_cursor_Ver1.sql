 
SELECT "BUSDTA"."Question_Template"."TemplateId"
,"BUSDTA"."Question_Template"."TemplateName"
,"BUSDTA"."Question_Template"."QuestionId"
,"BUSDTA"."Question_Template"."ResponseID"
,"BUSDTA"."Question_Template"."RevisionId"
,"BUSDTA"."Question_Template"."CreatedBy"
,"BUSDTA"."Question_Template"."CreatedDatetime"
,"BUSDTA"."Question_Template"."UpdatedBy"
,"BUSDTA"."Question_Template"."UpdatedDatetime"

FROM "BUSDTA"."Question_Template"
WHERE "BUSDTA"."Question_Template"."last_modified">= {ml s.last_table_download}
 
