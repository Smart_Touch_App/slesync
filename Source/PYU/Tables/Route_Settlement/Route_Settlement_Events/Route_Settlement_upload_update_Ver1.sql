 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Settlement"
SET "SettlementNO" = {ml r."SettlementNO"}, "Status" = {ml r."Status"}, "SettlementDateTime" = {ml r."SettlementDateTime"}, "UserId" = {ml r."UserId"}, "Originator" = {ml r."Originator"}, "Verifier" = {ml r."Verifier"}, "SettlementAmount" = {ml r."SettlementAmount"}, "ExceptionAmount" = {ml r."ExceptionAmount"}, "Comment" = {ml r."Comment"}, "OriginatingRoute" = {ml r."OriginatingRoute"}, "partitioningRoute" = {ml r."partitioningRoute"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "RouteId" = {ml r."RouteId"} AND "SettlementID" = {ml r."SettlementID"}
 
