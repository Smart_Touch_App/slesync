SELECT "BUSDTA"."M03042"."PDAN8",
	"BUSDTA"."M03042"."PDCO",
	"BUSDTA"."M03042"."PDID",
	"BUSDTA"."M03042"."PDPAMT",
	"BUSDTA"."M03042"."PDPMODE",
	"BUSDTA"."M03042"."PDCHQNO",
	"BUSDTA"."M03042"."PDCHQDT",
	"BUSDTA"."M03042"."PDCRBY",
	"BUSDTA"."M03042"."PDCRDT",
	"BUSDTA"."M03042"."PDUPBY",
	"BUSDTA"."M03042"."PDUPDT",
	"BUSDTA"."M03042"."PDRCID",
	"BUSDTA"."M03042"."PDTRMD"
FROM "BUSDTA"."M03042"
WHERE "BUSDTA"."M03042"."last_modified" >= {ml s.last_table_download}