

IF OBJECT_ID('[BUSDTA].[F01151]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[F01151](
	[EAAN8] [numeric](8, 0) NOT NULL,
	[EAIDLN] [numeric](5, 0) NOT NULL,
	[EARCK7] [numeric](5, 0) NOT NULL,
	[EAETP] [nchar](4) NULL,
	[EAEMAL] [nvarchar](256) NULL,
	[last_modified] [datetime] NOT NULL DEFAULT (getdate()),
 CONSTRAINT [PK_F01151] PRIMARY KEY CLUSTERED 
(
	[EAAN8] ASC,
	[EAIDLN] ASC,
	[EARCK7] ASC
)) ON [PRIMARY]

END