/* [BUSDTA].[M4016] - begins */

/* TableDDL - [BUSDTA].[M4016] - Start */
IF OBJECT_ID('[BUSDTA].[M4016]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M4016]
	(
	  [POORTP] VARCHAR(8) NOT NULL
	, [POAN8] NUMERIC NOT NULL
	, [POITM] VARCHAR(25) NOT NULL
	, [POSTDT] DATE NOT NULL
	, [POOSEQ] NUMERIC NULL
	, [POLITM] NCHAR(25) NULL
	, [POQTYU] INT NULL
	, [POUOM] NCHAR(2) NULL
	, [POLNTY] NCHAR(2) NULL
	, [POSRP1] NCHAR(3) NULL
	, [POSRP5] NCHAR(3) NULL
	, [POSTFG] BIT NULL
	, [POCRBY] NCHAR(10) NULL
	, [POCRDT] DATETIME NULL
	, [POUPBY] NCHAR(10) NULL
	, [POUPDT] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M4016_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M4016] PRIMARY KEY ([POORTP] ASC, [POAN8] ASC, [POITM] ASC, [POSTDT] ASC)
	)

END
/* TableDDL - [BUSDTA].[M4016] - End */

/* SHADOW TABLE FOR [BUSDTA].[M4016] - Start */
IF OBJECT_ID('[BUSDTA].[M4016_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M4016_del]
	(
	  [POORTP] VARCHAR(8)
	, [POAN8] NUMERIC
	, [POITM] VARCHAR(25)
	, [POSTDT] DATE
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([POORTP] ASC, [POAN8] ASC, [POITM] ASC, [POSTDT] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[M4016] - End */
/* TRIGGERS FOR M4016 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M4016_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M4016_ins
	ON BUSDTA.M4016 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M4016_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M4016_del.POAN8= inserted.POAN8 AND BUSDTA.M4016_del.POITM= inserted.POITM AND BUSDTA.M4016_del.POORTP= inserted.POORTP AND BUSDTA.M4016_del.POSTDT= inserted.POSTDT
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M4016_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M4016_upd
	ON BUSDTA.M4016 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M4016
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M4016.POAN8= inserted.POAN8 AND BUSDTA.M4016.POITM= inserted.POITM AND BUSDTA.M4016.POORTP= inserted.POORTP AND BUSDTA.M4016.POSTDT= inserted.POSTDT');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M4016_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M4016_dlt
	ON BUSDTA.M4016 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M4016_del (POAN8, POITM, POORTP, POSTDT, last_modified )
	SELECT deleted.POAN8, deleted.POITM, deleted.POORTP, deleted.POSTDT, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M4016 - END */
