
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M4016"
SET "POOSEQ" = {ml r."POOSEQ"}, "POLITM" = {ml r."POLITM"}, "POQTYU" = {ml r."POQTYU"}, "POUOM" = {ml r."POUOM"}, "POLNTY" = {ml r."POLNTY"}, "POSRP1" = {ml r."POSRP1"}, "POSRP5" = {ml r."POSRP5"}, "POSTFG" = {ml r."POSTFG"}, "POCRBY" = {ml r."POCRBY"}, "POCRDT" = {ml r."POCRDT"}, "POUPBY" = {ml r."POUPBY"}, "POUPDT" = {ml r."POUPDT"}
WHERE "POORTP" = {ml r."POORTP"} AND "POAN8" = {ml r."POAN8"} AND "POITM" = {ml r."POITM"} AND "POSTDT" = {ml r."POSTDT"}
 

