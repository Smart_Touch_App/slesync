
/* [BUSDTA].[M5001] - begins */

/* TableDDL - [BUSDTA].[M5001] - Start */
IF OBJECT_ID('[BUSDTA].[M5001]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5001]	(	  [TTID] NUMERIC NOT NULL	, [TTKEY] NCHAR(30) NOT NULL	, [TTTYP] NCHAR(25) NOT NULL	, [TTACTN] NCHAR(100) NULL	, [TTADSC] NCHAR(50) NULL	, [TTACTR] NCHAR(15) NULL	, [TTSTAT] NCHAR(15) NULL	, [TTISTRT] BIT NULL	, [TTIEND] BIT NULL	, [TTCRBY] NCHAR(50) NULL	, [TTCRDT] DATETIME NULL	, [TTUPBY] NCHAR(50) NULL	, [TTUPDT] DATETIME NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M5001_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_M5001] PRIMARY KEY ([TTID] ASC)	)
END
/* TableDDL - [BUSDTA].[M5001] - End */

/* SHADOW TABLE FOR [BUSDTA].[M5001] - Start */
IF OBJECT_ID('[BUSDTA].[M5001_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M5001_del]	(	  [TTID] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([TTID] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[M5001] - End */
/* TRIGGERS FOR M5001 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M5001_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M5001_ins
	ON BUSDTA.M5001 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M5001_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M5001_del.TTID= inserted.TTID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M5001_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5001_upd
	ON BUSDTA.M5001 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M5001
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M5001.TTID= inserted.TTID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M5001_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M5001_dlt
	ON BUSDTA.M5001 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M5001_del (TTID, last_modified )
	SELECT deleted.TTID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M5001 - END */
