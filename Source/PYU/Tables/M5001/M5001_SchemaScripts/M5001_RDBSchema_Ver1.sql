
------- /*[BUSDTA].[M5001]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M5001"     (	-- TransactionActivityType
	"TTID" numeric(8, 0) not null DEFAULT autoincrement,   -- ActivityID
	"TTKEY" nvarchar(30) not null,                      -- ActivityKey
	"TTTYP" nvarchar(25) not null,                      -- ActivityType
	"TTACTN" nvarchar(100) null,                        -- ActivityAction
	"TTADSC" nvarchar(50) null,                         -- ActionDescription
	"TTACTR" nvarchar(15) null,                         -- Actor
	"TTSTAT" nvarchar(15) null,                         -- ActivityState
	"TTISTRT" bit null,                                 -- IsStart
	"TTIEND" bit null,                                  -- IsEnd
	"TTCRBY" nvarchar(50) null,
	"TTCRDT" datetime null,
	"TTUPBY" nvarchar(50) null,
	"TTUPDT" datetime null,
	PRIMARY KEY ("TTID")
)

------- /*[BUSDTA].[M5001]*/ - End