
 
SELECT "BUSDTA"."Route_Replenishment_Header"."ReplenishmentID"
,"BUSDTA"."Route_Replenishment_Header"."RouteId"
,"BUSDTA"."Route_Replenishment_Header"."StatusId"
,"BUSDTA"."Route_Replenishment_Header"."ReplenishmentTypeId"
,"BUSDTA"."Route_Replenishment_Header"."TransDateFrom"
,"BUSDTA"."Route_Replenishment_Header"."TransDateTo"
,"BUSDTA"."Route_Replenishment_Header"."RequestedBy"
,"BUSDTA"."Route_Replenishment_Header"."FromBranchId"
,"BUSDTA"."Route_Replenishment_Header"."ToBranchId"
,"BUSDTA"."Route_Replenishment_Header"."CreatedBy"
,"BUSDTA"."Route_Replenishment_Header"."CreatedDatetime"
,"BUSDTA"."Route_Replenishment_Header"."UpdatedBy"
,"BUSDTA"."Route_Replenishment_Header"."UpdatedDatetime"

FROM "BUSDTA"."Route_Replenishment_Header"
WHERE "BUSDTA"."Route_Replenishment_Header"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Route_Replenishment_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
