/* [BUSDTA].[Route_Master] - begins */

/* TableDDL - [BUSDTA].[Route_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Master]
	(
	  [RouteMasterID] NUMERIC NOT NULL
	, [RouteName] VARCHAR(10) NOT NULL
	, [RouteDescription] NCHAR(50) NULL
	, [RouteAdressBookNumber] NUMERIC NOT NULL
	, [BranchNumber] NCHAR(12) NULL
	, [BranchAdressBookNumber] NUMERIC NULL
	, [DefaultUser] NUMERIC NULL
	, [VehicleID] NUMERIC NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ROUTE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Route_Master] PRIMARY KEY ([RouteMasterID] ASC, [RouteName] ASC)
	)

END
/* TableDDL - [BUSDTA].[Route_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Route_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Master_del]
	(
	  [RouteMasterID] NUMERIC
	, [RouteName] VARCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([RouteMasterID] ASC, [RouteName] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Route_Master] - End */
/* TRIGGERS FOR Route_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Route_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Route_Master_ins
	ON BUSDTA.Route_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Route_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Route_Master_del.RouteMasterID= inserted.RouteMasterID AND BUSDTA.Route_Master_del.RouteName= inserted.RouteName
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Route_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Master_upd
	ON BUSDTA.Route_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Route_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Route_Master.RouteMasterID= inserted.RouteMasterID AND BUSDTA.Route_Master.RouteName= inserted.RouteName');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Route_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Route_Master_dlt
	ON BUSDTA.Route_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Route_Master_del (RouteMasterID, RouteName, last_modified )
	SELECT deleted.RouteMasterID, deleted.RouteName, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Route_Master - END */
