/* [BUSDTA].[Device_Master] - begins */

/* TableDDL - [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master]
	(
	  [Device_Id] VARCHAR(30) NOT NULL
	, [Active] INT NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_DEVICE_MASTER_last_modified DEFAULT(getdate())
	, [manufacturer] NVARCHAR(50) NULL
	, [model] NVARCHAR(50) NULL
	, CONSTRAINT [PK_Device_Master] PRIMARY KEY ([Device_Id] ASC)
	)

END
/* TableDDL - [BUSDTA].[Device_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master_del]
	(
	  [Device_Id] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Device_Id] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[Device_Master] - End */
/* TRIGGERS FOR Device_Master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Device_Master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Device_Master_ins
	ON BUSDTA.Device_Master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Device_Master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Device_Master_del.Device_Id= inserted.Device_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Device_Master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Master_upd
	ON BUSDTA.Device_Master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Device_Master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Device_Master.Device_Id= inserted.Device_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Device_Master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Device_Master_dlt
	ON BUSDTA.Device_Master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Device_Master_del (Device_Id, last_modified )
	SELECT deleted.Device_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Device_Master - END */
