/* [BUSDTA].[Device_Master] - begins */

/* TableDDL - [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master]
	(
	  [Device_Id] VARCHAR(30) NOT NULL
	, [Active] INT NULL
	, [manufacturer] NVARCHAR(50) NULL
	, [model] NVARCHAR(50) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_DEVICE_MASTER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK__Device_M__1907C7AEDBC10E1C] PRIMARY KEY ([Device_Id] ASC)
	)
END
/* TableDDL - [BUSDTA].[Device_Master] - End */

/* SHADOW TABLE FOR [BUSDTA].[Device_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Device_Master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Device_Master_del]
	(
	  [Device_Id] VARCHAR(30)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Device_Id] ASC)
	)
END
/* SHADOW TABLE FOR [BUSDTA].[Device_Master] - End */

/* TRIGGERS FOR [BUSDTA].[Device_Master] - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('[BUSDTA].[Device_Master_ins]') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER [BUSDTA].[Device_Master_ins] ON Device_Master AFTER INSERT 
		AS
		/*
		* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
		* primary keys can be re-inserted.)
		*/
		DELETE FROM [BUSDTA].[Device_Master_del
			WHERE EXISTS ( SELECT 1
				FROM inserted
				WHERE [BUSDTA].[Device_Master_del].DTRT= inserted.DTRT AND [BUSDTA].[Device_Master_del.DTSY= inserted.DTSY
			');
END

/* Create the shadow update trigger. */
IF OBJECT_ID('[BUSDTA].[Device_Master_upd]') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER  [BUSDTA].[Device_Master_upd] ON Device_Master AFTER UPDATE 
		AS
		/* Update the column last_modified in modified row. */
		UPDATE [BUSDTA].[Device_Master]
			SET last_modified = GETDATE()
			FROM inserted
				WHERE [BUSDTA].[Device_Master].DTRT= inserted.DTRT AND [BUSDTA].[Device_Master].DTSY= inserted.DTSY')
END

/* Create the shadow delete trigger */
IF OBJECT_ID('[BUSDTA].[Device_Master_dlt]') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER  [BUSDTA].[Device_Master_dlt] ON Device_Master AFTER DELETE 
		AS
		/* Insert the row into the shadow delete table. */
		INSERT INTO [BUSDTA].[Device_Master_del] (DTRT, DTSY, last_modified )
		SELECT deleted.DTRT, deleted.DTSY, GETDATE()
			FROM deleted;')
END
/* TRIGGERS FOR [BUSDTA].[Device_Master] - END */