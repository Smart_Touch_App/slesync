SELECT "BUSDTA"."F0004"."DTSY",
	"BUSDTA"."F0004"."DTRT",
	"BUSDTA"."F0004"."DTDL01",
	"BUSDTA"."F0004"."DTCNUM"
FROM "BUSDTA"."F0004"
WHERE "BUSDTA"."F0004"."last_modified" >= {ml s.last_table_download}