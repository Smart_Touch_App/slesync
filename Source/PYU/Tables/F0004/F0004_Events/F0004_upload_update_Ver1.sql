 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0004"
SET "DTDL01" = {ml r."DTDL01"}, "DTCDL" = {ml r."DTCDL"}, "DTLN2" = {ml r."DTLN2"}, "DTCNUM" = {ml r."DTCNUM"}, "DTYN" = {ml r."DTYN"}
WHERE "DTRT" = {ml r."DTRT"} AND "DTSY" = {ml r."DTSY"}
 
