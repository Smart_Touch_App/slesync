/* [dbo].[tblGlobalSync] - begins */

/* TableDDL - [dbo].[tblGlobalSync] - Start */
IF OBJECT_ID('[dbo].[tblGlobalSync]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGlobalSync]
	(
	  [GlobalSyncID] INTEGER NOT NULL default autoincrement
	, [VersionNum] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL DEFAULT(getdate())

	, PRIMARY KEY ([GlobalSyncID] ASC)
	)
END
/* TableDDL - [dbo].[tblGlobalSync] - End */
