/* [BUSDTA].[user_master] - begins */

/* TableDDL - [BUSDTA].[user_master] - Start */
IF OBJECT_ID('[BUSDTA].[user_master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[user_master]
	(
	  [App_user_id] INT NOT NULL IDENTITY(1,1)
	, [App_User] VARCHAR(30) NULL
	, [Name] VARCHAR(30) NULL
	, [DomainUser] VARCHAR(20) NULL
	, [AppPassword] VARCHAR(20) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_USER_MASTER_last_modified DEFAULT(getdate())
	, [active] BIT NULL CONSTRAINT DF_USER_MASTER_active DEFAULT((1))
	, [Created_On] DATETIME NULL CONSTRAINT DF_USER_MASTER_Created_On DEFAULT(getdate())
	, [Contact] NVARCHAR(15) NULL
	, CONSTRAINT [PK_user_master] PRIMARY KEY ([App_user_id] ASC)
	)

END
/* TableDDL - [BUSDTA].[user_master] - End */

/* SHADOW TABLE FOR [BUSDTA].[user_master] - Start */
IF OBJECT_ID('[BUSDTA].[user_master_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[user_master_del]
	(
	  [App_user_id] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([App_user_id] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[user_master] - End */
/* TRIGGERS FOR user_master - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.user_master_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.user_master_ins
	ON BUSDTA.user_master AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.user_master_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.user_master_del.App_user_id= inserted.App_user_id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.user_master_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.user_master_upd
	ON BUSDTA.user_master AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.user_master
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.user_master.App_user_id= inserted.App_user_id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.user_master_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.user_master_dlt
	ON BUSDTA.user_master AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.user_master_del (App_user_id, last_modified )
	SELECT deleted.App_user_id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR user_master - END */
