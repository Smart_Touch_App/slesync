/* [BUSDTA].[tblUOM] - begins */

/* TableDDL - [BUSDTA].[tblUOM] - Start */
IF OBJECT_ID('[BUSDTA].[tblUOM]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblUOM]
	(
	  [UOMID] INTEGER NOT NULL 
	, [UOMLable] VARCHAR(10) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([UOMID] ASC)
	)
END
/* TableDDL - [BUSDTA].[tblUOM] - End */
