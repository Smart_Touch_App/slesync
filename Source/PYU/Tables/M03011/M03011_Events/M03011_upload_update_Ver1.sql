/* Update the row in the consolidated database. */

UPDATE "BUSDTA"."M03011"
SET "CSUAMT" = {ml r."CSUAMT"},
	"CSOBAL" = {ml r."CSOBAL"},
	"CSCRBY" = {ml r."CSCRBY"},
	"CSCRDT" = {ml r."CSCRDT"},
	"CSUPBY" = {ml r."CSUPBY"},
	"CSUPDT" = {ml r."CSUPDT"}
WHERE "CSAN8" = {ml r."CSAN8"}
	AND "CSCO" = {ml r."CSCO"}