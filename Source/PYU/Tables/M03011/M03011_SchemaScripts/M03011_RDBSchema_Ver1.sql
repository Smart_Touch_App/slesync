------- /*[BUSDTA].[M03011]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M03011"     (
	"CSAN8" numeric(8, 0) not null,
	"CSCO" varchar(5) not null,
	"CSUAMT" numeric(8, 4) null,
	"CSOBAL" numeric(8, 4) null,
	"CSCRBY" nvarchar(10) null,
	"CSCRDT" date null,
	"CSUPBY" nvarchar(10) null,
	"CSUPDT" date null,
	PRIMARY KEY ("CSAN8", "CSCO")
)

------- /*[BUSDTA].[M03011]*/ - End