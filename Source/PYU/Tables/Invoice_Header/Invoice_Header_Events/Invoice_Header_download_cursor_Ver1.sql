
     SELECT "BUSDTA"."Invoice_Header"."InvoiceID"  ,"BUSDTA"."Invoice_Header"."CustomerId"  ,"BUSDTA"."Invoice_Header"."RouteId"  ,
     "BUSDTA"."Invoice_Header"."OrderId"  ,"BUSDTA"."Invoice_Header"."ARInvoiceNumber"  ,"BUSDTA"."Invoice_Header"."DeviceInvoiceNumber"  ,
     "BUSDTA"."Invoice_Header"."ARInvoiceDate"  ,"BUSDTA"."Invoice_Header"."DeviceInvoiceDate"  ,"BUSDTA"."Invoice_Header"."DeviceStatusID"  ,
     "BUSDTA"."Invoice_Header"."ARStatusID"  ,"BUSDTA"."Invoice_Header"."ReasonCodeId"  ,"BUSDTA"."Invoice_Header"."DueDate"  ,
     "BUSDTA"."Invoice_Header"."InvoicePaymentType"  ,"BUSDTA"."Invoice_Header"."GrossAmt"  ,"BUSDTA"."Invoice_Header"."CreatedBy"  ,
     "BUSDTA"."Invoice_Header"."CreatedDatetime"  ,"BUSDTA"."Invoice_Header"."UpdatedBy"  ,"BUSDTA"."Invoice_Header"."UpdatedDatetime"    
     FROM "BUSDTA"."Invoice_Header"  
     WHERE "BUSDTA"."Invoice_Header"."last_modified">= {ml s.last_table_download}   
     AND "BUSDTA"."Invoice_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})    