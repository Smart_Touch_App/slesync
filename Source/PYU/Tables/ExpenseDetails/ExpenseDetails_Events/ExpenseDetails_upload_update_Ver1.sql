 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ExpenseDetails"
SET "CategoryId" = {ml r."CategoryId"}, "ExpensesExplanation" = {ml r."ExpensesExplanation"}, "ExpenseAmount" = {ml r."ExpenseAmount"}, "StatusId" = {ml r."StatusId"}, "ExpensesDatetime" = {ml r."ExpensesDatetime"}, "VoidReasonId" = {ml r."VoidReasonId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ExpenseId" = {ml r."ExpenseId"} AND "RouteId" = {ml r."RouteId"}
 
