
    SELECT "BUSDTA"."ExpenseDetails"."ExpenseId"  ,"BUSDTA"."ExpenseDetails"."RouteId"  ,"BUSDTA"."ExpenseDetails"."TransactionId"  ,
    "BUSDTA"."ExpenseDetails"."CategoryId"  ,"BUSDTA"."ExpenseDetails"."ExpensesExplanation"  ,"BUSDTA"."ExpenseDetails"."ExpenseAmount"  ,
    "BUSDTA"."ExpenseDetails"."StatusId"  ,"BUSDTA"."ExpenseDetails"."ExpensesDatetime"  ,"BUSDTA"."ExpenseDetails"."VoidReasonId"  ,
    "BUSDTA"."ExpenseDetails"."RouteSettlementId"  ,"BUSDTA"."ExpenseDetails"."CreatedBy"  ,"BUSDTA"."ExpenseDetails"."CreatedDatetime"  ,
    "BUSDTA"."ExpenseDetails"."UpdatedBy"  ,"BUSDTA"."ExpenseDetails"."UpdatedDatetime"    
    FROM "BUSDTA"."ExpenseDetails"  
    WHERE "BUSDTA"."ExpenseDetails"."last_modified">= {ml s.last_table_download}  
    AND "BUSDTA"."ExpenseDetails"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})  