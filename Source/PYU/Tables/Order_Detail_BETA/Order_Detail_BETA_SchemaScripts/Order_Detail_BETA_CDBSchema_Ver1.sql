
IF OBJECT_ID('[BUSDTA].[Order_Detail_BETA]') IS NULL
BEGIN
CREATE TABLE [BUSDTA].[Order_Detail_BETA]
(
	[OrderID] [numeric](8, 0) NOT NULL,
	[LineID] [numeric](7, 4) NOT NULL,
	[ItemId] [numeric](8, 0) NULL,
	[LongItem] [nchar](25) NULL,
	[ItemLocation] [nchar](20) NULL,
	[ItemLotNumber] [nchar](30) NULL,
	[LineType] [nchar](2) NULL,
	[IsNonStock] [nchar](1) NULL,
	[OrderQty] [numeric](8, 0) NOT NULL,
	[OrderUM] [nchar](2) NULL,
	[OrderQtyInPricingUM] [numeric](8, 0) NOT NULL,
	[PricingUM] [nchar](2) NULL,
	[UnitPriceOriginalAmtInPriceUM] [numeric](12, 4) NULL,
	[UnitPriceAmtInPriceUoM] [numeric](12, 4) NULL,
	[UnitPriceAmt] [numeric](12, 4) NULL,
	[ExtnPriceAmt] [numeric](12, 4) NULL,
	[ItemSalesTaxAmt] [numeric](12, 4) NULL,
	[Discounted] [nchar](1) NULL,
	[PriceOverriden] [nchar](1) NULL,
	[PriceOverrideReasonCodeId] [numeric](3, 0) NULL,
	[IsTaxable] [nchar](1) NULL,
	[ReturnHeldQty] [numeric](8, 0) NULL,
	[SalesCat1] [nchar](3) NULL,
	[SalesCat2] [nchar](3) NULL,
	[SalesCat3] [nchar](3) NULL,
	[SalesCat4] [nchar](3) NULL,
	[SalesCat5] [nchar](3) NULL,
	[PurchasingCat1] [nchar](3) NULL,
	[PurchasingCat2] [nchar](3) NULL,
	[PurchasingCat3] [nchar](3) NULL,
	[PurchasingCat4] [nchar](3) NULL,
	[PurchasingCat5] [nchar](3) NULL,
	[JDEOrderCompany] [nchar](5) NULL,
	[JDEOrderNumber] [numeric](8, 0) NULL,
	[JDEOrderType] [nchar](2) NULL,
	[JDEOrderLine] [numeric](7, 4) NULL,
	[JDEInvoiceCompany] [nchar](5) NULL,
	[JDEInvoiceNumber] [numeric](8, 0) NULL,
	[JDEOInvoiceType] [nchar](2) NULL,
	[JDEZBatchNumber] [nchar](15) NULL,
	[JDEProcessID] [numeric](3, 0) NULL,
	[SettlementID] [numeric](8, 0) NULL,
	[ExtendedAmtVariance] [numeric](12, 4) NULL,
	[TaxAmountAmtVariance] [numeric](12, 4) NULL,
	[HoldCode] [nchar](2) NULL,
	[CreatedBy] [numeric](8, 0) NULL,
	[CreatedDatetime] [datetime] NULL,
	[UpdatedBy] [numeric](8, 0) NULL,
	[UpdatedDatetime] [datetime] NULL,
	[last_modified] [datetime] NULL DEFAULT (getdate()),
 PRIMARY KEY ([OrderID] ASC,[LineID] ASC)) 
END

IF OBJECT_ID('[BUSDTA].[Order_Detail_BETA_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail_BETA_del]
	(
	[OrderID] [numeric](8, 0) NOT NULL,
	[LineID] [numeric](7, 4) NOT NULL,
	[last_modified] [datetime] NULL,
	PRIMARY KEY ([OrderID] ASC,	[LineID] ASC)
	)
	
END

IF OBJECT_ID('BUSDTA.Order_Detail_BETA_ins') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER [BUSDTA].[Order_Detail_BETA_ins]
		ON [BUSDTA].[Order_Detail_BETA] AFTER INSERT
		AS

		DELETE FROM BUSDTA.Order_Detail_BETA_del
		WHERE EXISTS ( SELECT 1
		FROM inserted
		WHERE BUSDTA.Order_Detail_BETA_del.OrderId= inserted.OrderId AND BUSDTA.Order_Detail_BETA_del.LineID= inserted.LineID 
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Order_Detail_BETA_upd') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER [BUSDTA].[Order_Detail_BETA_upd]
			ON [BUSDTA].[Order_Detail_BETA] AFTER UPDATE
		AS
		UPDATE BUSDTA.Order_Detail_BETA
			SET last_modified = GETDATE()
			FROM inserted
				WHERE BUSDTA.Order_Detail_BETA.OrderId= inserted.OrderId AND BUSDTA.Order_Detail_BETA.LineID= inserted.LineID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Order_Detail_BETA_dlt') IS NULL
BEGIN
	EXEC('
		CREATE TRIGGER [BUSDTA].[Order_Detail_BETA_dlt]
			ON [BUSDTA].[Order_Detail_BETA] AFTER DELETE
		AS
		/* Insert the row into the shadow delete table. */
		INSERT INTO BUSDTA.Order_Detail_BETA_del (OrderID, LineID, last_modified )
			SELECT deleted.OrderID, deleted.LineID, GETDATE()
			FROM deleted;
	');
END
GO
