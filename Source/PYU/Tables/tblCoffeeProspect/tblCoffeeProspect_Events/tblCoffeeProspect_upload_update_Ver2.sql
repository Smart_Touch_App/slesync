 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."tblCoffeeProspect"
SET "CoffeeBlendID" = {ml r."CoffeeBlendID"}, "CompetitorID" = {ml r."CompetitorID"}, "UOMID" = {ml r."UOMID"}, "PackSize" = {ml r."PackSize"}, "CS_PK_LB" = {ml r."CS_PK_LB"}, "Price" = {ml r."Price"}, "UsageMeasurementID" = {ml r."UsageMeasurementID"}, "LiqCoffeeTypeID" = {ml r."LiqCoffeeTypeID"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CoffeeProspectID" = {ml r."CoffeeProspectID"} AND "ProspectID" = {ml r."ProspectID"}
