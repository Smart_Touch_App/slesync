/* [BUSDTA].[ItemConfiguration] - begins */

/* TableDDL - [BUSDTA].[ItemConfiguration] - Start */
IF OBJECT_ID('[BUSDTA].[ItemConfiguration]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemConfiguration]
	(
	  [ItemID] NUMERIC NOT NULL
	, [RouteEnabled] BIT NULL
	, [Sellable] BIT NULL
	, [AllowSearch] BIT NULL
	, [PrimaryUM] NCHAR(2) NULL
	, [PricingUM] NCHAR(2) NULL
	, [TransactionUM] NCHAR(2) NULL
	, [OtherUM1] NCHAR(2) NULL
	, [OtherUM2] NCHAR(2) NULL
	, [AllowLooseSample] BIT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([ItemID] ASC)
	)
END
/* TableDDL - [BUSDTA].[ItemConfiguration] - End */
