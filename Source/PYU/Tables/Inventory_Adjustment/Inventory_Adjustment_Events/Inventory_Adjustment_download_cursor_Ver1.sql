 
SELECT "BUSDTA"."Inventory_Adjustment"."InventoryAdjustmentId"
,"BUSDTA"."Inventory_Adjustment"."ItemId"
,"BUSDTA"."Inventory_Adjustment"."ItemNumber"
,"BUSDTA"."Inventory_Adjustment"."RouteId"
,"BUSDTA"."Inventory_Adjustment"."TransactionQty"
,"BUSDTA"."Inventory_Adjustment"."TransactionQtyUM"
,"BUSDTA"."Inventory_Adjustment"."TransactionQtyPrimaryUM"
,"BUSDTA"."Inventory_Adjustment"."ReasonCode"
,"BUSDTA"."Inventory_Adjustment"."IsApproved"
,"BUSDTA"."Inventory_Adjustment"."IsApplied"
,"BUSDTA"."Inventory_Adjustment"."CreatedBy"
,"BUSDTA"."Inventory_Adjustment"."CreatedDatetime"
,"BUSDTA"."Inventory_Adjustment"."UpdatedBy"
,"BUSDTA"."Inventory_Adjustment"."UpdatedDatetime"
FROM "BUSDTA"."Inventory_Adjustment"
WHERE "BUSDTA"."Inventory_Adjustment"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."Inventory_Adjustment"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

