/* [BUSDTA].[Inventory_Adjustment] - begins */

/* TableDDL - [BUSDTA].[Inventory_Adjustment] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Adjustment]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Adjustment]
	(
	  [InventoryAdjustmentId] NUMERIC NOT NULL
	, [ItemId] NUMERIC NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [TransactionQty] NUMERIC NULL
	, [TransactionQtyUM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUM] NCHAR(2) NULL
	, [ReasonCode] NUMERIC NOT NULL
	, [IsApproved] BIT NULL DEFAULT((0))
	, [IsApplied] BIT NULL DEFAULT((0))
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([InventoryAdjustmentId] ASC, [ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Inventory_Adjustment] - End */
