

IF OBJECT_ID('[BUSDTA].[F0116_Audit]') IS NULL
BEGIN

                CREATE TABLE [BUSDTA].[F0116_Audit]
                (
                  TransID numeric(8,0) NOT NULL,
                  TransType nvarchar(50) NULL DEFAULT (('AddressBook')),
                  [ALAN8] NUMERIC(8,0) NOT NULL
                , [ALEFTB] NUMERIC(18,0) NOT NULL
                , [ALEFTF] NVARCHAR (1) NULL
                , [ALADD1] NVARCHAR (40) NULL
                , [ALADD2] NVARCHAR (40) NULL
                , [ALADD3] NVARCHAR (40) NULL
                , [ALADD4] NVARCHAR (40) NULL
                , [ALADDZ] NVARCHAR (12) NULL
                , [ALCTY1] NVARCHAR (25) NULL
                , [ALCOUN] NVARCHAR (25) NULL
                , [ALADDS] NVARCHAR (3) NULL
                , [WSValidation] NVARCHAR (50) NULL
                , [Status] NVARCHAR (50) NULL
                , [RejReason] NVARCHAR (50)
		, [SyncID] INTEGER NULL 
                , [UpdatedDatetime] DATETIME NULL DEFAULT(getdate()),
                PRIMARY KEY ([ALAN8] ASC, [ALEFTB] ASC, [TransID] ASC)
                )
END