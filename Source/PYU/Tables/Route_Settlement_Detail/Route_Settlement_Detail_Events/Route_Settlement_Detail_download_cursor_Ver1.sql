 
SELECT "BUSDTA"."Route_Settlement_Detail"."SettlementId"
,"BUSDTA"."Route_Settlement_Detail"."SettlementDetailId"
,"BUSDTA"."Route_Settlement_Detail"."UserId"
,"BUSDTA"."Route_Settlement_Detail"."VerificationNum"
,"BUSDTA"."Route_Settlement_Detail"."CashAmount"
,"BUSDTA"."Route_Settlement_Detail"."CheckAmount"
,"BUSDTA"."Route_Settlement_Detail"."MoneyOrderAmount"
,"BUSDTA"."Route_Settlement_Detail"."TotalVerified"
,"BUSDTA"."Route_Settlement_Detail"."Expenses"
,"BUSDTA"."Route_Settlement_Detail"."Payments"
,"BUSDTA"."Route_Settlement_Detail"."OverShortAmount"
,"BUSDTA"."Route_Settlement_Detail"."VerSignature"
,"BUSDTA"."Route_Settlement_Detail"."RouteId"
,"BUSDTA"."Route_Settlement_Detail"."OriginatingRoute"
,"BUSDTA"."Route_Settlement_Detail"."partitioningRoute"
,"BUSDTA"."Route_Settlement_Detail"."CreatedBy"
,"BUSDTA"."Route_Settlement_Detail"."CreatedDatetime"
,"BUSDTA"."Route_Settlement_Detail"."UpdatedBy"
,"BUSDTA"."Route_Settlement_Detail"."UpdatedDatetime"

FROM "BUSDTA"."Route_Settlement_Detail"
, "BUSDTA"."Route_Settlement"
WHERE "BUSDTA"."Route_Settlement_Detail"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."Route_Settlement_Detail"."SettlementId" = "BUSDTA"."Route_Settlement"."SettlementId"
AND "BUSDTA"."Route_Settlement"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 
