
 
SELECT "BUSDTA"."PreTrip_Inspection_Header"."PreTripInspectionHeaderId"
,"BUSDTA"."PreTrip_Inspection_Header"."RouteId"
,"BUSDTA"."PreTrip_Inspection_Header"."TemplateId"
,"BUSDTA"."PreTrip_Inspection_Header"."PreTripDateTime"
,"BUSDTA"."PreTrip_Inspection_Header"."UserName"
,"BUSDTA"."PreTrip_Inspection_Header"."StatusId"
,"BUSDTA"."PreTrip_Inspection_Header"."VehicleMake"
,"BUSDTA"."PreTrip_Inspection_Header"."VehicleNumber"
,"BUSDTA"."PreTrip_Inspection_Header"."OdoMmeterReading"
,"BUSDTA"."PreTrip_Inspection_Header"."Comment"
,"BUSDTA"."PreTrip_Inspection_Header"."VerSignature"
,"BUSDTA"."PreTrip_Inspection_Header"."CreatedBy"
,"BUSDTA"."PreTrip_Inspection_Header"."CreatedDatetime"
,"BUSDTA"."PreTrip_Inspection_Header"."UpdatedBy"
,"BUSDTA"."PreTrip_Inspection_Header"."UpdatedDatetime"

FROM "BUSDTA"."PreTrip_Inspection_Header"
WHERE "BUSDTA"."PreTrip_Inspection_Header"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."PreTrip_Inspection_Header"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})
 
