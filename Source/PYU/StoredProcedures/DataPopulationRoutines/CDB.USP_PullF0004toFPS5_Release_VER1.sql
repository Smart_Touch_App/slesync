
CREATE PROCEDURE [dbo].[USP_PullF0004toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'dbo.F0004'

	DECLARE @TEMP TABLE(Act varchar(6),DTSY NCHAR(50))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F0004)
	BEGIN
	
		INSERT INTO BUSDTA.F0004
		(DTSY, DTRT, DTDL01,DTCDL, DTLN2, DTCNUM)
		SELECT DTSY, DTRT, DTDL01, DTCDL, DTLN2, DTCNUM FROM [JDEData_CRP_SQDR_Model].dbo.F0004 
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'dbo.F0004')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'dbo.F0004'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('dbo.F0004',@synchronization_version,GETDATE())
		
		
	END
	ELSE
	
	--Check the data whether which has been refreshed
	IF EXISTS (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'dbo.F0004')
	BEGIN
		TRUNCATE TABLE BUSDTA.F0004
		TRUNCATE TABLE BUSDTA.F0004_del
			
		INSERT INTO BUSDTA.F0004
		(DTSY, DTRT, DTDL01,DTCDL, DTLN2, DTCNUM)
		SELECT DTSY, DTRT, DTDL01, DTCDL, DTLN2, DTCNUM FROM [JDEData_CRP_SQDR_Model].dbo.F0004 
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'dbo.F0004')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'dbo.F0004'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('dbo.F0004',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		--SELECT  @TSQL = '
		--INSERT INTO [BUSDTA].[F0004_shadow](DTSY,DTRT,DTDL01,DTCDL,DTLN2,DTCNUM,Operation)
		--SELECT CT.DTSY,CT.DTRT,DTDL01,DTCDL,DTLN2,DTCNUM,CT.SYS_CHANGE_OPERATION 
		--FROM CHANGETABLE(CHANGES [[JDEData_CRP_SQDR_Model]].BUSDTA.F0004, @Last_Sync_Version) CT
		--LEFT OUTER JOIN [[JDEData_CRP_SQDR_Model]].BUSDTA.F0004 AS F0004 ON CT.DTSY = F0004.DTSY AND CT.DTRT = F0004.DTRT
		--'EXEC (@TSQL)
		
		
		/***********Check the changes for replicatoin****************/
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES  [JDEData_CRP_SQDR_Model].dbo.F0004, @Last_Sync_Version)AS CT) 
		BEGIN

		WITH CTE AS
		(
		SELECT CT.DTSY,CT.DTRT,DTDL01,DTCDL,DTLN2,DTCNUM,CT.SYS_CHANGE_OPERATION AS Operation
		FROM CHANGETABLE(CHANGES JDEData_CRP_SQDR_Model.dbo.F0004, @Last_Sync_Version) CT
		LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F0004 AS F0004 ON CT.DTSY = F0004.DTSY AND CT.DTRT = F0004.DTRT
		)

		MERGE BUSDTA.F0004 AS TARGET
		USING CTE AS SOURCE 
		ON (TARGET.DTSY = SOURCE.DTSY) AND (TARGET.DTRT = SOURCE.DTRT) 

		WHEN MATCHED AND SOURCE.Operation='U' THEN 
		UPDATE 
		SET 
			TARGET.DTDL01 = SOURCE.DTDL01,
			TARGET.DTCDL = SOURCE.DTCDL,
			TARGET.DTLN2 = SOURCE.DTLN2,
			TARGET.DTCNUM = SOURCE.DTCNUM
			
				
		WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
		INSERT (DTSY,DTRT,DTDL01,DTCDL,DTLN2,DTCNUM)

		VALUES (SOURCE.DTSY,SOURCE.DTRT,SOURCE.DTDL01,SOURCE.DTCDL,SOURCE.DTLN2,SOURCE.DTCNUM)

		WHEN  MATCHED AND SOURCE.Operation='D' THEN 
		DELETE

		OUTPUT $action,
		INSERTED.DTSY AS SourceDTSY INTO @Temp (Act,DTSY);
		SELECT @@ROWCOUNT AS F0004;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'dbo.F0004')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		Where Objectname = 'dbo.F0004'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F0004',@synchronization_version)
		
		END
	--TRUNCATE TABLE [busdta].[F0004_shadow]
	
END

END


GO


