
CREATE PROCEDURE [dbo].[USP_PullF0150toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F0150'

	DECLARE @TEMP TABLE(Act varchar(6),MAPA8 numeric(18,0))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F0150)
	BEGIN
	
		INSERT INTO BUSDTA.F0150
		(MAOSTP, MAPA8, MAAN8, MABEFD, MAEEFD)
		SELECT MAOSTP, MAPA8, MAAN8, MABEFD, MAEEFD
		FROM  [JDEData_CRP_SQDR_Model].dbo.F0150
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F0150')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F0150'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F0150',@synchronization_version,GETDATE())		
		
	END
	ELSE


	--Check the table data whether which has been refreshed
	IF EXISTS  (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F0150')
	BEGIN
		TRUNCATE TABLE BUSDTA.F0150
		TRUNCATE TABLE BUSDTA.F0150_del
			
		INSERT INTO BUSDTA.F0150
		(MAOSTP, MAPA8, MAAN8, MABEFD, MAEEFD)
		SELECT MAOSTP, MAPA8, MAAN8, MABEFD, MAEEFD
		FROM  [JDEData_CRP_SQDR_Model].dbo.F0150

		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F0150')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F0150'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F0150',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		--INSERT INTO [BUSDTA].[F0150_shadow](MAOSTP, MAPA8, MAAN8, MABEFD, MAEEFD, Operation)
		--SELECT CT.MAOSTP, CT.MAPA8, CT.MAAN8, MABEFD, MAEEFD,CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0150,@Last_Sync_Version) CT
		--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F0150 AS F0150 ON CT.MAOSTP = F0150.MAOSTP
		--AND CT.MAPA8 = F0150.MAPA8 AND CT.MAAN8 = F0150.MAAN8
						
		/***********Check the changes for replicatoin**************/
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0150, @Last_Sync_Version)AS CT) 
		BEGIN
		WITH CTE AS
		(
		SELECT CT.MAOSTP, CT.MAPA8, CT.MAAN8, MABEFD, MAEEFD,CT.SYS_CHANGE_OPERATION AS Operation
		FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0150,@Last_Sync_Version) CT
		LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F0150 AS F0150 ON CT.MAOSTP = F0150.MAOSTP
		AND CT.MAPA8 = F0150.MAPA8 AND CT.MAAN8 = F0150.MAAN8
		)
		MERGE BUSDTA.F0150 AS TARGET
		USING CTE AS SOURCE 
		ON TARGET.MAOSTP = SOURCE.MAOSTP
		AND TARGET.MAPA8 = SOURCE.MAPA8
		AND TARGET.MAAN8 = SOURCE.MAAN8

		WHEN MATCHED AND SOURCE.Operation='U' THEN 
		UPDATE 
		SET 
		TARGET.[MABEFD] = SOURCE.MABEFD,
		TARGET.[MAEEFD] = SOURCE.MAEEFD
	
		WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
		INSERT (MAOSTP, MAPA8, MAAN8, MABEFD, MAEEFD)
		VALUES (SOURCE.MAOSTP, SOURCE.MAPA8, SOURCE.MAAN8, SOURCE.MABEFD, SOURCE.MAEEFD)

		WHEN MATCHED AND SOURCE.Operation='D' THEN 
		DELETE

		OUTPUT $action,
		INSERTED.MAPA8 AS SourceMAPA8 INTO @TEMP (Act,MAPA8);
		SELECT @@ROWCOUNT AS F0150;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F0150')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F0150'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('DBO.F0150',@synchronization_version)
		
		END
		--TRUNCATE TABLE [BUSDTA].[F0150_shadow]	
END

END








GO


