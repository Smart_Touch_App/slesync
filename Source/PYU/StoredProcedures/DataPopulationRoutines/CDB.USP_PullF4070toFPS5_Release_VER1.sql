
CREATE PROCEDURE [dbo].[USP_PullF4070toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F4070'

	DECLARE @TEMP TABLE(Act varchar(6),SNASN numeric(18,0))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F4070)
	BEGIN
	
		INSERT INTO BUSDTA.F4070
		(SNASN, SNOSEQ, SNANPS, SNAST, SNEFTJ, SNEXDJ)
		SELECT SNASN, SNOSEQ, SNANPS, SNAST, SNEFTJ, SNEXDJ
		FROM [JDEData_CRP_SQDR_Model].dbo.F4070
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4070')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4070'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4070',@synchronization_version,GETDATE())		
		
	END
	ELSE

	--Check the table data whether which has been refreshed
	IF EXISTS  (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F4070')
	BEGIN
		TRUNCATE TABLE BUSDTA.F4070
		TRUNCATE TABLE BUSDTA.F4070_del
				
		INSERT INTO BUSDTA.F4070
		(SNASN, SNOSEQ, SNANPS, SNAST, SNEFTJ, SNEXDJ)
		SELECT SNASN, SNOSEQ, SNANPS, SNAST, SNEFTJ, SNEXDJ
		FROM [JDEData_CRP_SQDR_Model].dbo.F4070

		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4070')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4070'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4070',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		
		--INSERT INTO [BUSDTA].[F4070_shadow]([SNASN],[SNOSEQ],[SNANPS],[SNAST],[SNEFTJ],[SNEXDJ],Operation)
		--SELECT CT.SNASN,CT.SNOSEQ,CT.SNANPS,F00.SNAST,F00.SNEFTJ,F00.SNEXDJ,CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4070, @Last_Sync_Version) CT
		--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4070 AS F00
		--ON CT.SNASN = F00.SNASN AND CT.SNOSEQ = F00.SNOSEQ AND  CT.SNANPS = F00.SNANPS

		/***********Check the changes for replicatoin****************/
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES JDEData_CRP_SQDR_Model.dbo.F4070, @Last_Sync_Version)AS CT)
		BEGIN
			WITH CTE AS
			(
			SELECT CT.SNASN,CT.SNOSEQ,CT.SNANPS,F00.SNAST,F00.SNEFTJ,F00.SNEXDJ,CT.SYS_CHANGE_OPERATION AS Operation
			FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4070, @Last_Sync_Version) CT
			LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4070 AS F00
			ON CT.SNASN = F00.SNASN AND CT.SNOSEQ = F00.SNOSEQ AND  CT.SNANPS = F00.SNANPS
			)

			MERGE BUSDTA.F4070 AS TARGET
			USING CTE AS SOURCE 
			ON (TARGET.SNASN = SOURCE.SNASN ) AND (TARGET.SNOSEQ = SOURCE.SNOSEQ)  AND (TARGET.SNANPS = SOURCE.SNANPS) 
			WHEN MATCHED AND SOURCE.Operation='U' THEN 
			UPDATE 
				SET TARGET.[SNAST] = SOURCE.SNAST
				,TARGET.[SNEFTJ] = SOURCE.SNEFTJ
				,TARGET.[SNEXDJ] = SOURCE.SNEXDJ		

			WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
			INSERT ([SNASN],[SNOSEQ],[SNANPS],[SNAST],[SNEFTJ],[SNEXDJ]) 
			VALUES (SOURCE.SNASN,SOURCE.SNOSEQ,SOURCE.SNANPS,SOURCE.SNAST,SOURCE.SNEFTJ,SOURCE.SNEXDJ)

			WHEN MATCHED AND SOURCE.Operation='D' THEN 
			DELETE

			OUTPUT $action,
			INSERTED.SNASN AS SourceSNASN INTO @Temp (Act,SNASN);
			SELECT @@ROWCOUNT AS F4070;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4070')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4070'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F4070',@synchronization_version)
		
		END
		--TRUNCATE TABLE [BUSDTA].[F4070_shadow]	
END

END









GO


