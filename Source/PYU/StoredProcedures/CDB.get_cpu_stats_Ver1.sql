CREATE PROCEDURE [dbo].[get_cpu_stats]
(@DBName VARCHAR(100)='MobileDataModel')
AS
BEGIN
	DECLARE @ts_now BIGINT, @cpu DECIMAL(4,2);
	SELECT @ts_now = ms_ticks FROM sys.dm_os_sys_info
	SELECT TOP 1 
	@cpu=(100 - SystemIdle)
	FROM (
	SELECT record.value('(./Record/@id)[1]', 'int') AS record_id, record.value('(./Record/SchedulerMonitorEvent/SystemHealth/SystemIdle)[1]', 'int') AS SystemIdle,
	record.value('(./Record/SchedulerMonitorEvent/SystemHealth/ProcessUtilization)[1]', 'int') AS SQLProcessUtilization,
	TIMESTAMP FROM ( SELECT TIMESTAMP, CONVERT(XML, record) AS record FROM sys.dm_os_ring_buffers WHERE ring_buffer_type = N'RING_BUFFER_SCHEDULER_MONITOR' 
	AND record LIKE '%<SystemHealth>%') AS x ) AS y ORDER BY record_id DESC

	
	DECLARE @db INT, @nondb INT;

	SELECT @db = COUNT(dbid) 
		FROM sys.sysprocesses 
		WHERE dbid > 0 AND DB_NAME(dbid)=@DBName
		
	SELECT @nondb = COUNT(dbid) 
		FROM sys.sysprocesses 
		WHERE dbid > 0 AND DB_NAME(dbid)!=@DBName
		
	SELECT @DBName database_name, @db AS db_connections_count, @nondb AS nondb_connections_count, @cpu AS cpu_usage
END

GO