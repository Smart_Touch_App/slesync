CREATE OR REPLACE FUNCTION "BUSDTA"."getLastStopDate"(CustomerId numeric(8), SelectedDate date, BaseDate date, WithUnplanned bit)
RETURNS DATE
BEGIN

    DECLARE LastStopDate int;
    DECLARE StopType varchar(15);
    DECLARE RefStopId NUMERIC(8,0);
    DECLARE prevDate date;
    DECLARE  Week numeric(4,0); -- Week Number for Delivery Day Code
    DECLARE  Day numeric(4,0); -- Day Number for Delivery Day Code
    DECLARE  WeeksPerCycle integer;
    DECLARE  WeekNumber numeric(2,0); -- Week Number for SelectedDate
	DECLARE  DayNumber numeric(2,0); -- Day Number for SelectedDate
    DECLARE i INTEGER = 1;
    DECLARE Years INTEGER = 1;
    DECLARE loopUpTo INTEGER = (Years * 100);

    SELECT DCWN, DCDN, DMWPC INTO Week, Day, WeeksPerCycle
      FROM BUSDTA.M56M0002, BUSDTA.M56M0001
      WHERE DCDDC = (Select (AISTOP) from busdta.F03012 where AIAN8 = CustomerId) and DCDDC = DMDDC;
	/*Consider Unplanned stops also*/
    IF WithUnplanned=1 THEN
        SELECT TOP 1 RPSTDT INTO LastStopDate FROM BUSDTA.M56M0004
        where RPAN8 = CustomerId and RPSTDT < SelectedDate and RPSTTP <> 'Moved'
        order by RPSTDT desc;  
        /*If no last stop exists, traverse the date in reverse order and compare with pattern as per the DDC*/
        if isnull(LastStopDate,0) = 0 then
            ilp: LOOP
            IF i > loopUpTo THEN LEAVE ilp END IF;
                select dateadd(day, -i, SelectedDate) into prevDate from dummy; 
                select busdta.GetWeekForDate(prevDate, BaseDate, WeeksPerCycle) into WeekNumber from dummy;
                select busdta.GetDayForDate(prevDate, BaseDate) into DayNumber from dummy ;
                IF Week = WeekNumber AND Day = DayNumber THEN
                    set LastStopDate =  prevDate;
                    LEAVE ilp;
                 END IF;
                set i = i+1;
            END LOOP;
        end if;
    ELSE
            select top 1 CAST(Cast(val as Date) AS INT) into LastStopDate from 
            "BUSDTA"."getPreOrderDatesForCustomer"("CustomerNum" = CustomerId,"SelectedDate" = BaseDate,"BaseDate" = BaseDate) 
            where val < SelectedDate 
            ORDER BY val desc;

            select RPSTTP, RPRSTID into StopType,RefStopId  from busdta.M56M0004 where RPAN8 =  CustomerId and RPSTDT = LastStopDate and RPSTTP <> 'Unplanned';
			/*Check if Last stop is moved*/
            IF StopType = 'Moved' THEN
                select RPSTDT into LastStopDate from busdta.M56M0004 where RPSTID = RefStopId;
                IF LastStopDate >= SelectedDate THEN
                    select LstDT  into LastStopDate  from (
                    select  CAST(Cast(val as Date) AS INT) as 'LstDT', (row_number() over (order by LstDT desc)) as RowNum from 
                    "BUSDTA"."getPreOrderDatesForCustomer"("CustomerNum" = CustomerId,"SelectedDate" = BaseDate,"BaseDate" = BaseDate) 
                    where val < SelectedDate 
                    ORDER BY val desc) a where RowNum = 2;

                END IF;
            END IF;
    END IF;

RETURN LastStopDate;
END
GO