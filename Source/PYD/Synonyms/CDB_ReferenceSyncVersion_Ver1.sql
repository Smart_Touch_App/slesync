


IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ReferenceSyncVersion')
DROP SYNONYM [dbo].[ReferenceSyncVersion]
GO
CREATE SYNONYM [dbo].[ReferenceSyncVersion] FOR [CIS54\SQLEXPRESS2012].[JDEData_CRP].[dbo].[vw_CTCurrentVersion]
GO

