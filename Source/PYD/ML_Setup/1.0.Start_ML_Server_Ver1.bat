@ECHO OFF
setlocal

CALL Remote_Connection_Vars.bat

echo Starting Remote Server....

mlsrv16 -c "DSN=%dsn%;UID=%uname%;PWD=%pass%" -x TCPIP(host="%ip%";port="%port%") -o %cd%\mlserber.mls -v+ -dl -zf -zu+ -sl dnet (-MLAutoLoadPath=%cd%\drh_dll)

echo Remote Server Started.

GOTO End

:End
EXIT
