/*
/* [dbo].[tblGlobalSync] - begins */

/* TableDDL - [dbo].[tblGlobalSync] - Start */
IF OBJECT_ID('[dbo].[tblGlobalSync]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGlobalSync]
	(
	  [GlobalSyncID] INT NOT NULL IDENTITY(1,1)
	, [VersionNum] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLGLOBALSYNC_CreatedOn DEFAULT(getdate())
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLGLOBALSYNC_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblGlobalSync] PRIMARY KEY ([GlobalSyncID] ASC)
	)

END
/* TableDDL - [dbo].[tblGlobalSync] - End */

/* SHADOW TABLE FOR [dbo].[tblGlobalSync] - Start */
IF OBJECT_ID('[dbo].[tblGlobalSync_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGlobalSync_del]
	(
	  [GlobalSyncID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([GlobalSyncID] ASC)
	)

END
/* SHADOW TABLE FOR [dbo].[tblGlobalSync] - End */
/* TRIGGERS FOR tblGlobalSync - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblGlobalSync_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblGlobalSync_ins
	ON dbo.tblGlobalSync AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblGlobalSync_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblGlobalSync_del.GlobalSyncID= inserted.GlobalSyncID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblGlobalSync_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblGlobalSync_upd
	ON dbo.tblGlobalSync AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblGlobalSync
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblGlobalSync.GlobalSyncID= inserted.GlobalSyncID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblGlobalSync_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblGlobalSync_dlt
	ON dbo.tblGlobalSync AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblGlobalSync_del (GlobalSyncID, last_modified )
	SELECT deleted.GlobalSyncID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblGlobalSync - END */
*/
