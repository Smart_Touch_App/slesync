
/* Update the row in the consolidated database. */  
UPDATE "BUSDTA"."F4015"  SET "OTOSEQ" = {ml r."OTOSEQ"}, 
"OTLITM" = {ml r."OTLITM"}, "OTQTYU" = {ml r."OTQTYU"}, 
"OTUOM" = {ml r."OTUOM"}, "OTLNTY" = {ml r."OTLNTY"}, 
"OTEFTJ" = {ml r."OTEFTJ"}, "OTEXDJ" = {ml r."OTEXDJ"}  
WHERE "OTORTP" = {ml r."OTORTP"} AND 
"OTAN8" = {ml r."OTAN8"} AND "OTITM" = {ml r."OTITM"}