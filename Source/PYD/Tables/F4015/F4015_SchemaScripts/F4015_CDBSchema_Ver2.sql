
/* [BUSDTA].[F4015] - begins */

/* TableDDL - [BUSDTA].[F4015] - Start */
IF OBJECT_ID('[BUSDTA].[F4015]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4015]
	(
	  [OTORTP] NCHAR(8) NOT NULL
	, [OTAN8] NUMERIC(8,0) NOT NULL
	, [OTOSEQ] NUMERIC(4,0) NOT NULL
	, [OTITM] FLOAT NULL
	, [OTLITM] NCHAR(25) NULL
	, [OTQTYU] FLOAT NULL
	, [OTUOM] NCHAR(2) NULL
	, [OTLNTY] NCHAR(2) NULL
	, [OTEFTJ] NUMERIC(18,0) NULL
	, [OTEXDJ] NUMERIC(18,0) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4015_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F4015] PRIMARY KEY ([OTORTP] ASC, [OTAN8] ASC, [OTOSEQ] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F4015] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4015] - Start */
IF OBJECT_ID('[BUSDTA].[F4015_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4015_del]
	(
	  [OTORTP] NCHAR(8)
	, [OTAN8] NUMERIC(8,0)
	, [OTOSEQ] NUMERIC(4,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([OTORTP] ASC, [OTAN8] ASC, [OTOSEQ] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F4015] - End */
/* TRIGGERS FOR F4015 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4015_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4015_ins
	ON BUSDTA.F4015 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4015_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4015_del.OTAN8= inserted.OTAN8 AND BUSDTA.F4015_del.OTORTP= inserted.OTORTP AND BUSDTA.F4015_del.OTOSEQ= inserted.OTOSEQ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4015_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4015_upd
	ON BUSDTA.F4015 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4015
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4015.OTAN8= inserted.OTAN8 AND BUSDTA.F4015.OTORTP= inserted.OTORTP AND BUSDTA.F4015.OTOSEQ= inserted.OTOSEQ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4015_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4015_dlt
	ON BUSDTA.F4015 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4015_del (OTAN8, OTORTP, OTOSEQ, last_modified )
	SELECT deleted.OTAN8, deleted.OTORTP, deleted.OTOSEQ, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4015 - END */

