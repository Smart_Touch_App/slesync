 
SELECT "dbo"."tblAlliedProspSubcategory"."SubCategoryID"
,"dbo"."tblAlliedProspSubcategory"."SubCategory"
,"dbo"."tblAlliedProspSubcategory"."CategoryID"
,"dbo"."tblAlliedProspSubcategory"."CreatedBy"
,"dbo"."tblAlliedProspSubcategory"."CreatedDatetime"
,"dbo"."tblAlliedProspSubcategory"."UpdatedBy"
,"dbo"."tblAlliedProspSubcategory"."UpdatedDatetime"

FROM "dbo"."tblAlliedProspSubcategory"
WHERE "dbo"."tblAlliedProspSubcategory"."last_modified">= {ml s.last_table_download}
 
