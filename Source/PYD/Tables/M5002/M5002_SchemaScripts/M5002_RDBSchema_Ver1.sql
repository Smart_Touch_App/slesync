
------- /*[BUSDTA].[M5002]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M5002"     (	-- NonTransactionActivityType
	"TNID" numeric(8, 0) not null DEFAULT autoincrement,   -- ActivityID
	"TNKEY" nvarchar(30) not null,                      -- ActivityKey
	"TNTYP" nvarchar(25) not null,                      -- ActivityType
	"TNACTN" nvarchar(30) null,                         -- ActivityAction
	"TNADSC" nvarchar(50) null,                         -- ActionDescription
	"TNACTR" nvarchar(15) null,                         -- Actor
	"TNILDGRD" bit null,                                -- IsLedgered
	"TNCRBY" nvarchar(50) null,
	"TNCRDT" datetime null,
	"TNUPBY" nvarchar(50) null,
	"TNUPDT" datetime null,
	PRIMARY KEY ("TNID")
)

------- /*[BUSDTA].[M5002]*/ - End