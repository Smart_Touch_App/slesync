/* [dbo].[DDLEvents] - begins */

/* TableDDL - [dbo].[DDLEvents] - Start */
IF OBJECT_ID('[dbo].[DDLEvents]') IS NULL
BEGIN

	CREATE TABLE [dbo].[DDLEvents]
	(
	  [EvntID] INTEGER NOT NULL default autoincrement
	, [EventDate] DATETIME NULL DEFAULT(getdate())
	, [EventType] NVARCHAR(64) NULL
	, [ObjectType] NVARCHAR(64) NULL
	, [EventDDL] NVARCHAR() NULL
	, [EventXML] XML NULL
	, [DatabaseName] NVARCHAR(255) NULL
	, [SchemaName] NVARCHAR(255) NULL
	, [ObjectName] NVARCHAR(255) NULL
	, [HostName] VARCHAR(64) NULL
	, [IPAddress] VARCHAR(32) NULL
	, [ProgramName] NVARCHAR(255) NULL
	, [LoginName] NVARCHAR(255) NULL
	, [Status] BIT NULL DEFAULT((1))
	)
END
/* TableDDL - [dbo].[DDLEvents] - End */
