/* [dbo].[tblEquipmentType] - begins */

/* TableDDL - [dbo].[tblEquipmentType] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentType]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentType]
	(
	  [EquipmentTypeID] INT NOT NULL IDENTITY(1,1)
	, [EquipmentType] VARCHAR(200) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLEQUIPMENTTYPE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblEquipmentType] PRIMARY KEY ([EquipmentTypeID] ASC)
	)

END
GO
/* TableDDL - [dbo].[tblEquipmentType] - End */

/* SHADOW TABLE FOR [dbo].[tblEquipmentType] - Start */
IF OBJECT_ID('[dbo].[tblEquipmentType_del]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblEquipmentType_del]
	(
	  [EquipmentTypeID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([EquipmentTypeID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [dbo].[tblEquipmentType] - End */
/* TRIGGERS FOR tblEquipmentType - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblEquipmentType_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblEquipmentType_ins
	ON dbo.tblEquipmentType AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM dbo.tblEquipmentType_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE dbo.tblEquipmentType_del.EquipmentTypeID= inserted.EquipmentTypeID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblEquipmentType_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipmentType_upd
	ON dbo.tblEquipmentType AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE dbo.tblEquipmentType
	SET last_modified = GETDATE()
	FROM inserted
		WHERE dbo.tblEquipmentType.EquipmentTypeID= inserted.EquipmentTypeID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblEquipmentType_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblEquipmentType_dlt
	ON dbo.tblEquipmentType AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO dbo.tblEquipmentType_del (EquipmentTypeID, last_modified )
	SELECT deleted.EquipmentTypeID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblEquipmentType - END */
