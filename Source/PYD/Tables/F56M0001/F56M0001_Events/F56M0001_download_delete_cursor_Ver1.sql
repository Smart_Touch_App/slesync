SELECT "BUSDTA"."F56M0001_del"."FFUSER",
	"BUSDTA"."F56M0001_del"."FFROUT",
	"BUSDTA"."F56M0001_del"."FFMCU"
FROM "BUSDTA"."F56M0001_del"
WHERE "BUSDTA"."F56M0001_del"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."F56M0001_del"."FFUSER" = {ml s.username}