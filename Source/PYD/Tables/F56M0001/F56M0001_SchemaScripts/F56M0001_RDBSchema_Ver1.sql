
------- /*[BUSDTA].[F56M0001]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F56M0001"     (
	"FFUSER" nvarchar(10) not null,
	"FFROUT" nvarchar(3) not null,
	"FFMCU" nvarchar(12) not null,
	"FFHMCU" nvarchar(12) null,
	"FFBUVAL" nvarchar(12) null,
	"FFAN8" float null,
	"FFPA8" float null,
	"FFSTOP" nvarchar(3) null,
	"FFZON" nvarchar(3) null,
	"FFLOCN" nvarchar(20) null,
	"FFLOCF" nvarchar(20) null,
	"FFEV01" nvarchar(1) null,
	"FFEV02" nvarchar(1) null,
	"FFEV03" nvarchar(1) null,
	"FFMATH01" float null,
	"FFMATH02" float null,
	"FFMATH03" float null,
	"FFCXPJ" numeric(18, 0) null,
	"FFCLRJ" numeric(18, 0) null,
	"FFDTE" numeric(18, 0) null,
	PRIMARY KEY ("FFUSER", "FFROUT", "FFMCU")
)

------- /*[BUSDTA].[F56M0001]*/ - End