/* [BUSDTA].[Pick_Detail] - begins */

/* TableDDL - [BUSDTA].[Pick_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Pick_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Pick_Detail]
	(
	  [PickDetailID] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [TransactionID] NUMERIC(8,0) NOT NULL
	, [TransactionDetailID] NUMERIC(8,0) NOT NULL
	, [TransactionTypeId] NUMERIC(8,0) NOT NULL
	, [TransactionStatusId] NUMERIC(8,0) NULL
	, [ItemID] NCHAR(25) NULL
	, [TransactionQty] NUMERIC(8,0) NULL
	, [TransactionUOM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUOM] NUMERIC(8,0) NULL
	, [PrimaryUOM] NCHAR(2) NULL
	, [PickedQty] NUMERIC(8,0) NULL
	, [PickQtyPrimaryUOM] NUMERIC(8,0) NULL
	, [LastScanMode] NUMERIC(3,0) NULL
	, [ItemScanSeq] NUMERIC(3,0) NULL
	, [IsOnHold] NCHAR(1) NULL
	, [PickAdjusted] NUMERIC(1,0) NOT NULL
	, [AdjustedQty] NUMERIC(4,0) NOT NULL
	, [ReasonCodeId] NUMERIC(8,0) NULL
	, [ManuallyPickCount] NUMERIC(8,0) NULL
	, [IsInException] NCHAR(1) NULL
	, [ExceptionReasonCodeId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PICK_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Pick_Detail] PRIMARY KEY ([PickDetailID] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Pick_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Pick_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Pick_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Pick_Detail_del]
	(
	  [PickDetailID] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([PickDetailID] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Pick_Detail] - End */
/* TRIGGERS FOR Pick_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Pick_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Pick_Detail_ins
	ON BUSDTA.Pick_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Pick_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Pick_Detail_del.PickDetailID= inserted.PickDetailID AND BUSDTA.Pick_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Pick_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Pick_Detail_upd
	ON BUSDTA.Pick_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Pick_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Pick_Detail.PickDetailID= inserted.PickDetailID AND BUSDTA.Pick_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Pick_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Pick_Detail_dlt
	ON BUSDTA.Pick_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Pick_Detail_del (PickDetailID, RouteId, last_modified )
	SELECT deleted.PickDetailID, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Pick_Detail - END */
