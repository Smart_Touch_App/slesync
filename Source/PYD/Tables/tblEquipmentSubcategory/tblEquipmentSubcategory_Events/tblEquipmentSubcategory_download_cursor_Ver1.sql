 
SELECT "dbo"."tblEquipmentSubcategory"."EquipmentSubcategoryID"
,"dbo"."tblEquipmentSubcategory"."EquipmentCategoryID"
,"dbo"."tblEquipmentSubcategory"."EquipmentSubCategory"
,"dbo"."tblEquipmentSubcategory"."CreatedBy"
,"dbo"."tblEquipmentSubcategory"."CreatedDatetime"
,"dbo"."tblEquipmentSubcategory"."UpdatedBy"
,"dbo"."tblEquipmentSubcategory"."UpdatedDatetime"

FROM "dbo"."tblEquipmentSubcategory"
WHERE "dbo"."tblEquipmentSubcategory"."last_modified">= {ml s.last_table_download}
