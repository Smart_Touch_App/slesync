 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ItemConfiguration"
SET "RouteEnabled" = {ml r."RouteEnabled"}, "Sellable" = {ml r."Sellable"}, "AllowSearch" = {ml r."AllowSearch"}, "PrimaryUM" = {ml r."PrimaryUM"}, "PricingUM" = {ml r."PricingUM"}, "TransactionUM" = {ml r."TransactionUM"}, "OtherUM1" = {ml r."OtherUM1"}, "OtherUM2" = {ml r."OtherUM2"}, "AllowLooseSample" = {ml r."AllowLooseSample"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ItemID" = {ml r."ItemID"}
 
