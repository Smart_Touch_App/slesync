/* [BUSDTA].[ItemConfiguration] - begins */

/* TableDDL - [BUSDTA].[ItemConfiguration] - Start */
IF OBJECT_ID('[BUSDTA].[ItemConfiguration]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemConfiguration]
	(
	  [ItemID] NUMERIC NOT NULL
	, [RouteEnabled] BIT NULL
	, [Sellable] BIT NULL
	, [AllowSearch] BIT NULL
	, [PrimaryUM] NCHAR(2) NULL
	, [PricingUM] NCHAR(2) NULL
	, [TransactionUM] NCHAR(2) NULL
	, [OtherUM1] NCHAR(2) NULL
	, [OtherUM2] NCHAR(2) NULL
	, [AllowLooseSample] BIT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ITEMCONFIGURATION_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ItemConfiguration] PRIMARY KEY ([ItemID] ASC)
	)

END
/* TableDDL - [BUSDTA].[ItemConfiguration] - End */

/* SHADOW TABLE FOR [BUSDTA].[ItemConfiguration] - Start */
IF OBJECT_ID('[BUSDTA].[ItemConfiguration_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemConfiguration_del]
	(
	  [ItemID] NUMERIC
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemID] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[ItemConfiguration] - End */
/* TRIGGERS FOR ItemConfiguration - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ItemConfiguration_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ItemConfiguration_ins
	ON BUSDTA.ItemConfiguration AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ItemConfiguration_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ItemConfiguration_del.ItemID= inserted.ItemID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ItemConfiguration_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemConfiguration_upd
	ON BUSDTA.ItemConfiguration AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ItemConfiguration
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ItemConfiguration.ItemID= inserted.ItemID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ItemConfiguration_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemConfiguration_dlt
	ON BUSDTA.ItemConfiguration AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ItemConfiguration_del (ItemID, last_modified )
	SELECT deleted.ItemID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ItemConfiguration - END */
