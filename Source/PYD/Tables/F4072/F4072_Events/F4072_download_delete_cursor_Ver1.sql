SELECT "BUSDTA"."F4072_del"."ADAST",
	"BUSDTA"."F4072_del"."ADITM",
	"BUSDTA"."F4072_del"."ADAN8",
	"BUSDTA"."F4072_del"."ADIGID",
	"BUSDTA"."F4072_del"."ADCGID",
	"BUSDTA"."F4072_del"."ADOGID",
	"BUSDTA"."F4072_del"."ADCRCD",
	"BUSDTA"."F4072_del"."ADUOM",
	"BUSDTA"."F4072_del"."ADMNQ",
	"BUSDTA"."F4072_del"."ADEXDJ",
	"BUSDTA"."F4072_del"."ADUPMJ",
	"BUSDTA"."F4072_del"."ADTDAY"
FROM "BUSDTA"."F4072_del"
WHERE "BUSDTA"."F4072_del"."last_modified" >= {ml s.last_table_download}