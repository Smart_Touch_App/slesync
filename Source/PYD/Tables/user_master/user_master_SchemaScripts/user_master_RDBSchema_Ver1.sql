------- /*[BUSDTA].[user_master]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."user_master"     (
	"App_user_id" integer not null,
	"App_User" varchar(30) null,
	"Name" varchar(30) null,
	"DomainUser" varchar(20) null,
	"AppPassword" varchar(20) null,
	"active" bit null default ((1)),
	"Created_On" datetime null default (getdate()),
	PRIMARY KEY ("App_user_id")
)

------- /*[BUSDTA].[user_master]*/ - End