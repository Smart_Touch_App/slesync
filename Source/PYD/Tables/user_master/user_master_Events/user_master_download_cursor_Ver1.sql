SELECT "BUSDTA"."user_master"."App_user_id",
	"BUSDTA"."user_master"."App_User",
	"BUSDTA"."user_master"."Name",
	"BUSDTA"."user_master"."DomainUser",
	"BUSDTA"."user_master"."AppPassword",
	"BUSDTA"."user_master"."active",
	"BUSDTA"."user_master"."Created_On"
FROM "BUSDTA"."user_master"
WHERE "BUSDTA"."user_master"."last_modified" >= {ml s.last_table_download}