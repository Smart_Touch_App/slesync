
------- /*[BUSDTA].[F4106]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4106"     (
	"BPITM" numeric(8, 0) not null,
	"BPLITM" nvarchar(25) null,
	"BPMCU" nvarchar(12) not null,
	"BPLOCN" nvarchar(20) not null,
	"BPLOTN" nvarchar(30) not null,
	"BPAN8" numeric(8, 0) not null,
	"BPIGID" numeric(8, 0) not null,
	"BPCGID" numeric(8, 0) not null,
	"BPLOTG" nvarchar(3) not null,
	"BPFRMP" numeric(7, 0) not null,
	"BPCRCD" nvarchar(3) not null,
	"BPUOM" nvarchar(2) not null,
	"BPEFTJ" numeric(18, 0) null,
	"BPEXDJ" numeric(18, 0) not null,
	"BPUPRC" float null,
	"BPUPMJ" numeric(18, 0) not null,
	"BPTDAY" float not null,
	PRIMARY KEY ("BPITM", "BPMCU", "BPLOCN", "BPLOTN", "BPAN8", "BPIGID", "BPCGID", "BPLOTG", "BPFRMP", "BPCRCD", "BPUOM", "BPEXDJ", "BPUPMJ", "BPTDAY")
)

------- /*[BUSDTA].[F4106]*/ - End