
------- /*[BUSDTA].[M0112]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M0112"     (
	"NDAN8" numeric(8, 0) not null,
	"NDID" numeric(8, 0)  not null  DEFAULT autoincrement,
	"NDDTTM" datetime null,
	"NDDTLS" nvarchar(500) null,
	"NDTYP" nvarchar(15) null,
	"NDDFLT" bit null,
	"NDCRBY" nvarchar(50) null,
	"NDCRDT" date null,
	"NDUPBY" nvarchar(50) null,
	"NDUPDT" date null,
	PRIMARY KEY ("NDAN8", "NDID")
)

------- /*[BUSDTA].[M0112]*/ - End