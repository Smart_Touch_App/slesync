

/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."MasterNotification"
SET "Title" = {ml r."Title"}, "Description" = {ml r."Description"}, "TypeID" = {ml r."TypeID"}, "TransactionID" = {ml r."TransactionID"}, "Status" = {ml r."Status"}, "Category" = {ml r."Category"}, "Read" = {ml r."Read"}, "ValidTill" = {ml r."ValidTill"}, "Acknowledged" = {ml r."Acknowledged"}, "Initiator" = {ml r."Initiator"}, "ResponseType" = {ml r."ResponseType"}, "ResponseValue" = {ml r."ResponseValue"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "MasterNotificationID" = {ml r."MasterNotificationID"} AND "RouteID" = {ml r."RouteID"}
