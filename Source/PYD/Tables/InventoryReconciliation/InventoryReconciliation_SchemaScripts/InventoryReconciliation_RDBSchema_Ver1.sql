

IF OBJECT_ID('[BUSDTA].[InventoryReconciliation]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[InventoryReconciliation]
	(
	[InventoryAdjustmentId] decimal (15,0) NOT NULL,
	[ItemId] numeric(8,0) NOT NULL,
	[ItemNumber] nchar(25) NOT NULL,
	[RouteId] numeric(8,0) NOT NULL,
	[TransactionQty] decimal (15,0) NULL,
	[TransactionQtyUM] NCHAR(2) NULL,
	[TransactionQtyPrimaryUM] NCHAR(2) NULL,
	[ReasonCode] numeric(3,0) NULL,
	[InventoryLedgerId] decimal (15,0) NULL,
	[TransactionID] INT NULL,
	[TranactionType] numeric(3,0) NOT NULL,
	[Status] numeric(3,0) NULL,
	[JDETransactionType] NCHAR(2) NULL,
	[JDEDocumentNumber] numeric(8,0) NULL,
	[JDEDocumentType] CHAR(2) NULL,
	[StatusFlow] numeric(3,0) NULL,
	[CreatedBy] numeric(8,0) NULL,
	[CreatedDatetime] DATETIME NULL,
	[UpdateBy] numeric(18,0) NULL,
	[UpdateDatetime] DATETIME NULL DEFAULT(getdate())
	, PRIMARY KEY ([InventoryAdjustmentId] ASC,[ItemId] ASC,[RouteId] ASC,[TranactionType] ASC)
	)
END


