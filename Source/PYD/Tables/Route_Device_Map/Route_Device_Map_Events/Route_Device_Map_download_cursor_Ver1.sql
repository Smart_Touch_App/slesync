SELECT "BUSDTA"."Route_Device_Map"."Route_Id",
	"BUSDTA"."Route_Device_Map"."Device_Id",
	"BUSDTA"."Route_Device_Map"."Active",
	"BUSDTA"."Route_Device_Map"."Remote_Id"
FROM "BUSDTA"."Route_Device_Map"
WHERE "BUSDTA"."Route_Device_Map"."last_modified" >= {ml s.last_table_download}