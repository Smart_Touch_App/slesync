/* [BUSDTA].[UoMFactorReference] - begins */

/* TableDDL - [BUSDTA].[UoMFactorReference] - Start */
IF OBJECT_ID('[BUSDTA].[UoMFactorReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UoMFactorReference]
	(
	  [ItemID] NUMERIC NOT NULL
	, [FromUOM] NCHAR(2) NOT NULL
	, [ToUOM] NCHAR(2) NOT NULL
	, [ConversionFactor] NUMERIC NULL
	, [GenerationDate] DATETIME NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([ItemID] ASC, [FromUOM] ASC, [ToUOM] ASC)
	)
END
/* TableDDL - [BUSDTA].[UoMFactorReference] - End */
