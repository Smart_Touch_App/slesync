/* [BUSDTA].[UoMFactorReference] - begins */

/* TableDDL - [BUSDTA].[UoMFactorReference] - Start */
IF OBJECT_ID('[BUSDTA].[UoMFactorReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[UoMFactorReference]
	(
	  [ItemID] NUMERIC(8,0) NOT NULL
	, [FromUOM] NVARCHAR (2) NOT NULL
	, [ToUOM] NVARCHAR (2) NOT NULL
	, [ConversionFactor] decimal (15,4) NULL
	, [GenerationDate] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([ItemID] ASC, [FromUOM] ASC, [ToUOM] ASC)
	)
END
/* TableDDL - [BUSDTA].[UoMFactorReference] - End */
