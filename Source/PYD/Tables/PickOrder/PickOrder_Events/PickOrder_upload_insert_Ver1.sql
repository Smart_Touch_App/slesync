 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."PickOrder"
("PickOrder_Id", "Order_ID", "RouteId", "Item_Number", "Order_Qty", "Order_UOM", "Picked_Qty_Primary_UOM", "Primary_UOM", "Order_Qty_Primary_UOM", "On_Hand_Qty_Primary", "Last_Scan_Mode", "Item_Scan_Sequence", "Picked_By", "IsOnHold", "Reason_Code_Id", "ManuallyPickCount")
VALUES({ml r."PickOrder_Id"}, {ml r."Order_ID"}, {ml r."RouteId"}, {ml r."Item_Number"}, {ml r."Order_Qty"}, {ml r."Order_UOM"}, {ml r."Picked_Qty_Primary_UOM"}, {ml r."Primary_UOM"}, {ml r."Order_Qty_Primary_UOM"}, {ml r."On_Hand_Qty_Primary"}, {ml r."Last_Scan_Mode"}, {ml r."Item_Scan_Sequence"}, {ml r."Picked_By"}, {ml r."IsOnHold"}, {ml r."Reason_Code_Id"}, {ml r."ManuallyPickCount"})
 
