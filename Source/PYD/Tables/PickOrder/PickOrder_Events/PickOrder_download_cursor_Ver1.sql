 
SELECT "BUSDTA"."PickOrder"."PickOrder_Id"
,"BUSDTA"."PickOrder"."Order_ID"
,"BUSDTA"."PickOrder"."RouteId"
,"BUSDTA"."PickOrder"."Item_Number"
,"BUSDTA"."PickOrder"."Order_Qty"
,"BUSDTA"."PickOrder"."Order_UOM"
,"BUSDTA"."PickOrder"."Picked_Qty_Primary_UOM"
,"BUSDTA"."PickOrder"."Primary_UOM"
,"BUSDTA"."PickOrder"."Order_Qty_Primary_UOM"
,"BUSDTA"."PickOrder"."On_Hand_Qty_Primary"
,"BUSDTA"."PickOrder"."Last_Scan_Mode"
,"BUSDTA"."PickOrder"."Item_Scan_Sequence"
,"BUSDTA"."PickOrder"."Picked_By"
,"BUSDTA"."PickOrder"."IsOnHold"
,"BUSDTA"."PickOrder"."Reason_Code_Id"
,"BUSDTA"."PickOrder"."ManuallyPickCount"

FROM MobileDataModel.BUSDTA.F56M0001, MobileDataModel.BUSDTA.F90CA003, MobileDataModel.BUSDTA.F90CA086, 
MobileDataModel.BUSDTA.Order_Header, MobileDataModel.BUSDTA.PickOrder
WHERE "BUSDTA"."PickOrder"."last_modified" >= {ml s.last_table_download}
AND FFAN8=SMSLSM and SMAN8=CRCUAN8 and Customer_Id=CRCRAN8 and BUSDTA.Order_Header.Order_ID = busdta.PickOrder.Order_ID and FFUSER= {ml s.username}

 
