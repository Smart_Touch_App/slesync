 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Inventory"
SET "ItemNumber" = {ml r."ItemNumber"}, "OnHandQuantity" = {ml r."OnHandQuantity"}, "CommittedQuantity" = {ml r."CommittedQuantity"}, "HeldQuantity" = {ml r."HeldQuantity"}, "ParLevel" = {ml r."ParLevel"}, "LastReceiptDate" = {ml r."LastReceiptDate"}, "LastConsumeDate" = {ml r."LastConsumeDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ItemId" = {ml r."ItemId"} AND "RouteId" = {ml r."RouteId"}
 
