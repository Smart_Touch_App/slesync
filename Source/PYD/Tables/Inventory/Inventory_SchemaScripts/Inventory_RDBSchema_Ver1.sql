/* [BUSDTA].[Inventory] - begins */

/* TableDDL - [BUSDTA].[Inventory] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory]
	(
	  [ItemId] NUMERIC NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [OnHandQuantity] NUMERIC NULL
	, [CommittedQuantity] NUMERIC NULL
	, [HeldQuantity] NUMERIC NULL
	, [ParLevel] NUMERIC NULL
	, [LastReceiptDate] DATETIME NULL
	, [LastConsumeDate] DATETIME NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Inventory] - End */
