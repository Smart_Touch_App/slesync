
/* [BUSDTA].[M0111] - begins */

/* TableDDL - [BUSDTA].[M0111] - Start */
IF OBJECT_ID('[BUSDTA].[M0111]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0111]	(	  [CDAN8] NUMERIC NOT NULL	, [CDID] NUMERIC NOT NULL	, [CDIDLN] NUMERIC NOT NULL	, [CDRCK7] NUMERIC NOT NULL	, [CDCNLN] NUMERIC NOT NULL	, [CDAR1] NCHAR(6) NULL	, [CDPH1] NCHAR(20) NULL	, [CDEXTN1] NCHAR(8) NULL	, [CDPHTP1] NUMERIC NULL	, [CDDFLTPH1] BIT NULL	, [CDREFPH1] INT NULL	, [CDAR2] NCHAR(6) NULL	, [CDPH2] NCHAR(20) NULL	, [CDEXTN2] NCHAR(8) NULL	, [CDPHTP2] NUMERIC NULL	, [CDDFLTPH2] BIT NULL	, [CDREFPH2] INT NULL	, [CDAR3] NCHAR(6) NULL	, [CDPH3] NCHAR(20) NULL	, [CDEXTN3] NCHAR(8) NULL	, [CDPHTP3] NUMERIC NULL	, [CDDFLTPH3] BIT NULL	, [CDREFPH3] INT NULL	, [CDAR4] NCHAR(6) NULL	, [CDPH4] NCHAR(20) NULL	, [CDEXTN4] NCHAR(8) NULL	, [CDPHTP4] NUMERIC NULL	, [CDDFLTPH4] BIT NULL	, [CDREFPH4] INT NULL	, [CDEMAL1] NVARCHAR(256) NULL	, [CDETP1] NUMERIC NULL	, [CDDFLTEM1] BIT NULL	, [CDREFEM1] INT NULL	, [CDEMAL2] NVARCHAR(256) NULL	, [CDETP2] NUMERIC NULL	, [CDDFLTEM2] BIT NULL	, [CDREFEM2] INT NULL	, [CDEMAL3] NVARCHAR(256) NULL	, [CDETP3] NUMERIC NULL	, [CDDFLTEM3] BIT NULL	, [CDREFEM3] INT NULL	, [CDGNNM] NCHAR(75) NULL	, [CDMDNM] NCHAR(25) NULL	, [CDSRNM] NCHAR(25) NULL	, [CDTITL] NCHAR(50) NULL	, [CDACTV] BIT NULL	, [CDDFLT] BIT NULL	, [CDSET] INT NULL	, [CDCRBY] NCHAR(25) NULL	, [CDCRDT] DATETIME NULL	, [CDUPBY] NCHAR(25) NULL	, [CDUPDT] DATETIME NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M0111_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_M0111] PRIMARY KEY ([CDAN8] ASC, [CDID] ASC)	)
END
/* TableDDL - [BUSDTA].[M0111] - End */

/* SHADOW TABLE FOR [BUSDTA].[M0111] - Start */
IF OBJECT_ID('[BUSDTA].[M0111_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M0111_del]	(	  [CDAN8] NUMERIC	, [CDID] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([CDAN8] ASC, [CDID] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[M0111] - End */
/* TRIGGERS FOR M0111 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M0111_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M0111_ins
	ON BUSDTA.M0111 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M0111_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M0111_del.CDAN8= inserted.CDAN8 AND BUSDTA.M0111_del.CDID= inserted.CDID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M0111_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0111_upd
	ON BUSDTA.M0111 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M0111
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M0111.CDAN8= inserted.CDAN8 AND BUSDTA.M0111.CDID= inserted.CDID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M0111_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M0111_dlt
	ON BUSDTA.M0111 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M0111_del (CDAN8, CDID, last_modified )
	SELECT deleted.CDAN8, deleted.CDID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M0111 - END */
