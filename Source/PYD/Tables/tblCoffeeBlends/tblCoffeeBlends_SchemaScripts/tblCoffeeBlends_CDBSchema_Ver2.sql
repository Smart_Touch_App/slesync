/* [BUSDTA].[tblCoffeeBlends] - begins */

/* TableDDL - [BUSDTA].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeBlends]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeBlends]
	(
	  [CoffeeBlendID] INT NOT NULL IDENTITY(1,1)
	, [CoffeeBlend] VARCHAR(50) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_CreatedDatetime DEFAULT(getdate())
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_UpdatedDatetime DEFAULT(getdate())
	, [last_modified] DATETIME NULL CONSTRAINT DF_TBLCOFFEEBLENDS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblCoffeeBlends] PRIMARY KEY ([CoffeeBlendID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[tblCoffeeBlends] - End */

/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeBlends] - Start */
IF OBJECT_ID('[BUSDTA].[tblCoffeeBlends_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblCoffeeBlends_del]
	(
	  [CoffeeBlendID] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CoffeeBlendID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[tblCoffeeBlends] - End */
/* TRIGGERS FOR tblCoffeeBlends - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeBlends_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.tblCoffeeBlends_ins
	ON BUSDTA.tblCoffeeBlends AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.tblCoffeeBlends_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.tblCoffeeBlends_del.CoffeeBlendID= inserted.CoffeeBlendID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.tblCoffeeBlends_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeBlends_upd
	ON BUSDTA.tblCoffeeBlends AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.tblCoffeeBlends
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.tblCoffeeBlends.CoffeeBlendID= inserted.CoffeeBlendID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.tblCoffeeBlends_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.tblCoffeeBlends_dlt
	ON BUSDTA.tblCoffeeBlends AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.tblCoffeeBlends_del (CoffeeBlendID, last_modified )
	SELECT deleted.CoffeeBlendID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR tblCoffeeBlends - END */
