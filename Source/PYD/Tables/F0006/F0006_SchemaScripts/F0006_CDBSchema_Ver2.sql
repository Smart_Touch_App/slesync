/* [BUSDTA].[F0006] - begins */

/* TableDDL - [BUSDTA].[F0006] - Start */
IF OBJECT_ID('[BUSDTA].[F0006]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0006]
	(
	  [MCMCU] NCHAR(12) NOT NULL
	, [MCSTYL] NCHAR(2) NULL
	, [MCLDM] NCHAR(1) NULL
	, [MCCO] NCHAR(5) NULL
	, [MCAN8] FLOAT NULL
	, [MCDL01] NCHAR(30) NULL
	, [MCRP01] NCHAR(3) NULL
	, [MCRP02] NCHAR(3) NULL
	, [MCRP03] NCHAR(3) NULL
	, [MCRP04] NCHAR(3) NULL
	, [MCRP05] NCHAR(3) NULL
	, [MCRP06] NCHAR(3) NULL
	, [MCRP07] NCHAR(3) NULL
	, [MCRP08] NCHAR(3) NULL
	, [MCRP09] NCHAR(3) NULL
	, [MCRP10] NCHAR(3) NULL
	, [MCRP11] NCHAR(3) NULL
	, [MCRP12] NCHAR(3) NULL
	, [MCRP13] NCHAR(3) NULL
	, [MCRP14] NCHAR(3) NULL
	, [MCRP15] NCHAR(3) NULL
	, [MCRP16] NCHAR(3) NULL
	, [MCRP17] NCHAR(3) NULL
	, [MCRP18] NCHAR(3) NULL
	, [MCRP19] NCHAR(3) NULL
	, [MCRP20] NCHAR(3) NULL
	, [MCRP21] NCHAR(10) NULL
	, [MCRP22] NCHAR(10) NULL
	, [MCRP23] NCHAR(10) NULL
	, [MCRP24] NCHAR(10) NULL
	, [MCRP25] NCHAR(10) NULL
	, [MCRP26] NCHAR(10) NULL
	, [MCRP27] NCHAR(10) NULL
	, [MCRP28] NCHAR(10) NULL
	, [MCRP29] NCHAR(10) NULL
	, [MCRP30] NCHAR(10) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0006_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0006] PRIMARY KEY ([MCMCU] ASC)
	)

END
/* TableDDL - [BUSDTA].[F0006] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0006] - Start */
IF OBJECT_ID('[BUSDTA].[F0006_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0006_del]
	(
	  [MCMCU] NCHAR(12)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MCMCU] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[F0006] - End */
/* TRIGGERS FOR F0006 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0006_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0006_ins
	ON BUSDTA.F0006 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0006_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0006_del.MCMCU= inserted.MCMCU
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0006_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0006_upd
	ON BUSDTA.F0006 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0006
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0006.MCMCU= inserted.MCMCU');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0006_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0006_dlt
	ON BUSDTA.F0006 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0006_del (MCMCU, last_modified )
	SELECT deleted.MCMCU, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0006 - END */
