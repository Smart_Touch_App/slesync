
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."StatusMaster"
SET "StatusCode" = {ml r."StatusCode"}, "StatusDescription" = {ml r."StatusDescription"}, "StatusForType" = {ml r."StatusForType"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "StatusId" = {ml r."StatusId"}