 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F40073"
SET "HYHY01" = {ml r."HYHY01"}, "HYHY02" = {ml r."HYHY02"}, "HYHY03" = {ml r."HYHY03"}, "HYHY04" = {ml r."HYHY04"}, "HYHY05" = {ml r."HYHY05"}, "HYHY06" = {ml r."HYHY06"}, "HYHY07" = {ml r."HYHY07"}, "HYHY08" = {ml r."HYHY08"}, "HYHY09" = {ml r."HYHY09"}, "HYHY10" = {ml r."HYHY10"}, "HYHY11" = {ml r."HYHY11"}, "HYHY12" = {ml r."HYHY12"}, "HYHY13" = {ml r."HYHY13"}, "HYHY14" = {ml r."HYHY14"}, "HYHY15" = {ml r."HYHY15"}, "HYHY16" = {ml r."HYHY16"}, "HYHY17" = {ml r."HYHY17"}, "HYHY18" = {ml r."HYHY18"}, "HYHY19" = {ml r."HYHY19"}, "HYHY20" = {ml r."HYHY20"}, "HYHY21" = {ml r."HYHY21"}
WHERE "HYPRFR" = {ml r."HYPRFR"} AND "HYHYID" = {ml r."HYHYID"}
