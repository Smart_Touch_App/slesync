
SELECT "BUSDTA"."Customer_Route_Map"."BranchNumber"  ,"BUSDTA"."Customer_Route_Map"."CustomerShipToNumber"  ,
"BUSDTA"."Customer_Route_Map"."RelationshipType"  ,"BUSDTA"."Customer_Route_Map"."RelatedAddressBookNumber"  ,
"BUSDTA"."Customer_Route_Map"."IsActive"  ,"BUSDTA"."Customer_Route_Map"."CreatedBy"  ,
"BUSDTA"."Customer_Route_Map"."CreatedDatetime"  ,"BUSDTA"."Customer_Route_Map"."UpdatedBy"  ,
"BUSDTA"."Customer_Route_Map"."UpdatedDatetime"    
FROM "BUSDTA"."Customer_Route_Map", BUSDTA.Route_Master rm  
WHERE "BUSDTA"."Customer_Route_Map"."last_modified">= {ml s.last_table_download}   
AND "BUSDTA"."Customer_Route_Map"."BranchNumber" = rm.BranchNumber   
AND rm.RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} )    