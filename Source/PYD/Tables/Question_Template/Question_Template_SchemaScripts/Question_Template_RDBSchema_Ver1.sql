/* [BUSDTA].[Question_Template] - begins */

/* TableDDL - [BUSDTA].[Question_Template] - Start */
IF OBJECT_ID('[BUSDTA].[Question_Template]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Question_Template]
	(
	  [TemplateId] NUMERIC NOT NULL
	, [TemplateName] NCHAR(100) NULL
	, [QuestionId] NUMERIC NOT NULL
	, [ResponseID] NUMERIC NOT NULL
	, [RevisionId] NUMERIC NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([TemplateId] ASC, [QuestionId] ASC, [ResponseID] ASC, [RevisionId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Question_Template] - End */
