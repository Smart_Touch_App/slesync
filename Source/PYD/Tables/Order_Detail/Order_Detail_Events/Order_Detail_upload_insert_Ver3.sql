 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Detail"
("OrderDetailId", "OrderID", "RouteID", "ItemId", "OrderQty", "OrderUM", "UnitPriceAmt", "ExtnPriceAmt", "ItemSalesTaxAmt", "PriceOverrideReasonCodeId", "IsTaxable", "ReturnHeldQty", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."OrderDetailId"}, {ml r."OrderID"}, {ml r."RouteID"}, {ml r."ItemId"}, {ml r."OrderQty"}, {ml r."OrderUM"}, {ml r."UnitPriceAmt"}, {ml r."ExtnPriceAmt"}, {ml r."ItemSalesTaxAmt"}, {ml r."PriceOverrideReasonCodeId"}, {ml r."IsTaxable"}, {ml r."ReturnHeldQty"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
