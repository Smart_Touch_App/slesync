 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Order_Detail"
SET "Item_Number" = {ml r."Item_Number"}, "Order_Qty" = {ml r."Order_Qty"}, "Order_UOM" = {ml r."Order_UOM"}, "Unit_Price" = {ml r."Unit_Price"}, "Extn_Price" = {ml r."Extn_Price"}, "Reason_Code" = {ml r."Reason_Code"}, "IsTaxable" = {ml r."IsTaxable"}
WHERE "Order_Detail_Id" = {ml r."Order_Detail_Id"} AND "Order_ID" = {ml r."Order_ID"}
 
