
 
 
SELECT "BUSDTA"."Order_Detail"."OrderDetailId"
,"BUSDTA"."Order_Detail"."OrderID"
,"BUSDTA"."Order_Detail"."RouteID"
,"BUSDTA"."Order_Detail"."ItemId"
,"BUSDTA"."Order_Detail"."OrderQty"
,"BUSDTA"."Order_Detail"."OrderUM"
,"BUSDTA"."Order_Detail"."UnitPriceAmt"
,"BUSDTA"."Order_Detail"."ExtnPriceAmt"
,"BUSDTA"."Order_Detail"."ItemSalesTaxAmt"
,"BUSDTA"."Order_Detail"."PriceOverrideReasonCodeId"
,"BUSDTA"."Order_Detail"."IsTaxable"
,"BUSDTA"."Order_Detail"."ReturnHeldQty"
,"BUSDTA"."Order_Detail"."CreatedBy"
,"BUSDTA"."Order_Detail"."CreatedDatetime"
,"BUSDTA"."Order_Detail"."UpdatedBy"
,"BUSDTA"."Order_Detail"."UpdatedDatetime"

FROM "BUSDTA"."Order_Detail"
WHERE "BUSDTA"."Order_Detail"."last_modified">= {ml s.last_table_download}
AND "BUSDTA"."Order_Detail"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username}) 
