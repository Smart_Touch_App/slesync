/* [BUSDTA].[Order_Detail] - begins */

/* TableDDL - [BUSDTA].[Order_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Order_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Order_Detail]
	(
	  [Order_Detail_Id] INTEGER NOT NULL
	, [Order_ID] INTEGER NOT NULL
	, [Item_Number] NVARCHAR (25) NULL
	, [Order_Qty] INTEGER NULL
	, [Order_UOM] NVARCHAR (2) NULL
	, [Unit_Price] FLOAT NULL
	, [Extn_Price] FLOAT NULL
	, [Reason_Code] VARCHAR(5) NULL

	, [IsTaxable] BIT NULL
	, PRIMARY KEY ([Order_Detail_Id] ASC, [Order_ID] ASC)
	)
END
/* TableDDL - [BUSDTA].[Order_Detail] - End */
