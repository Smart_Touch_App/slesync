/* [BUSDTA].[Metric_CustHistDemand] - begins */

/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - Start */
IF OBJECT_ID('[BUSDTA].[Metric_CustHistDemand]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Metric_CustHistDemand]
	(
	  [CustomerId] NUMERIC(8,0) NOT NULL
	, [ItemID] NUMERIC(8,0) NOT NULL
	, [DemandFromDate] DATE NULL
	, [DemandThroughDate] DATE NULL
	, [PlannedStops] NUMERIC(4,0) NULL
	, [ActualStops] NUMERIC(4,0) NULL
	, [TotalQtySold] NUMERIC(12,0) NULL
	, [TotalQtyReturned] NUMERIC(12,0) NULL
	, [QtySoldPerStop] NUMERIC(12,4) NULL
	, [QtyReturnedPerStop] NUMERIC(12,4) NULL
	, [NetQtySoldPerStop] NUMERIC(12,4) NULL
	, [QtySoldDeltaPerStop] NUMERIC(12,4) NULL
	, [QtyReturnedDeltaPerStop] NUMERIC(12,4) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_METRIC_CUSTHISTDEMAND_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Metric_CustHistDemand] PRIMARY KEY ([CustomerId] ASC, [ItemID] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Metric_CustHistDemand] - End */

/* SHADOW TABLE FOR [BUSDTA].[Metric_CustHistDemand] - Start */
IF OBJECT_ID('[BUSDTA].[Metric_CustHistDemand_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Metric_CustHistDemand_del]
	(
	  [CustomerId] NUMERIC(8,0)
	, [ItemID] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerId] ASC, [ItemID] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Metric_CustHistDemand] - End */
/* TRIGGERS FOR Metric_CustHistDemand - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Metric_CustHistDemand_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Metric_CustHistDemand_ins
	ON BUSDTA.Metric_CustHistDemand AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Metric_CustHistDemand_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Metric_CustHistDemand_del.CustomerId= inserted.CustomerId AND BUSDTA.Metric_CustHistDemand_del.ItemID= inserted.ItemID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Metric_CustHistDemand_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Metric_CustHistDemand_upd
	ON BUSDTA.Metric_CustHistDemand AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Metric_CustHistDemand
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Metric_CustHistDemand.CustomerId= inserted.CustomerId AND BUSDTA.Metric_CustHistDemand.ItemID= inserted.ItemID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Metric_CustHistDemand_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Metric_CustHistDemand_dlt
	ON BUSDTA.Metric_CustHistDemand AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Metric_CustHistDemand_del (CustomerId, ItemID, last_modified )
	SELECT deleted.CustomerId, deleted.ItemID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Metric_CustHistDemand - END */
