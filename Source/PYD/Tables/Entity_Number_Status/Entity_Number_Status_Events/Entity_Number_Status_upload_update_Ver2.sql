 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Entity_Number_Status"
SET "RouteId" = {ml r."RouteId"}, "Bucket" = {ml r."Bucket"}, "EntityBucketMasterId" = {ml r."EntityBucketMasterId"}, "RangeStart" = {ml r."RangeStart"}, "RangeEnd" = {ml r."RangeEnd"}, "CurrentAllocated" = {ml r."CurrentAllocated"}, "IsConsumed" = {ml r."IsConsumed"}, "ConsumedOnDate" = {ml r."ConsumedOnDate"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "EntityNumberStatusID" = {ml r."EntityNumberStatusID"}
