 

/* Update the row in the consolidated database. */
UPDATE "dbo"."Order_Templates"
SET "DisplaySeq" = {ml r."DisplaySeq"}, "ItemID" = {ml r."ItemID"}, "LongItem" = {ml r."LongItem"}, "TemplateQuantity" = {ml r."TemplateQuantity"}, "TemplateUoM" = {ml r."TemplateUoM"}
WHERE "TemplateType" = {ml r."TemplateType"} AND "ShipToID" = {ml r."ShipToID"} AND "TemplateDetailID" = {ml r."TemplateDetailID"}
 
