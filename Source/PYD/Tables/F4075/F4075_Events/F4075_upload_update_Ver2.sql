
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F4075"
SET "VBCRCD" = {ml r."VBCRCD"}, "VBUOM" = {ml r."VBUOM"}, "VBUPRC" = {ml r."VBUPRC"}, "VBEXDJ" = {ml r."VBEXDJ"}, "VBAPRS" = {ml r."VBAPRS"}
WHERE "VBVBT" = {ml r."VBVBT"} AND "VBEFTJ" = {ml r."VBEFTJ"} AND "VBUPMJ" = {ml r."VBUPMJ"} AND "VBTDAY" = {ml r."VBTDAY"}
 

