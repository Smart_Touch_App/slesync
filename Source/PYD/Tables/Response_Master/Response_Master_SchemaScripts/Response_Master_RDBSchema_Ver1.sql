/* [BUSDTA].[Response_Master] - begins */

/* TableDDL - [BUSDTA].[Response_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Response_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Response_Master]
	(
	  [ResponseId] NUMERIC NOT NULL
	, [Response] NCHAR(50) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([ResponseId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Response_Master] - End */
