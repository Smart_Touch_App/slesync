
/* [BUSDTA].[F0116] - begins */

/* TableDDL - [BUSDTA].[F0116] - Start */
IF OBJECT_ID('[BUSDTA].[F0116]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0116]	(	  [ALAN8] NUMERIC NOT NULL	, [ALEFTB] NUMERIC NOT NULL	, [ALEFTF] NCHAR(1) NULL	, [ALADD1] NCHAR(40) NULL	, [ALADD2] NCHAR(40) NULL	, [ALADD3] NCHAR(40) NULL	, [ALADD4] NCHAR(40) NULL	, [ALADDZ] NCHAR(12) NULL	, [ALCTY1] NCHAR(25) NULL	, [ALCOUN] NCHAR(25) NULL	, [ALADDS] NCHAR(3) NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0116_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F0116] PRIMARY KEY ([ALAN8] ASC, [ALEFTB] ASC)	)
END
/* TableDDL - [BUSDTA].[F0116] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0116] - Start */
IF OBJECT_ID('[BUSDTA].[F0116_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0116_del]	(	  [ALAN8] NUMERIC	, [ALEFTB] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([ALAN8] ASC, [ALEFTB] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[F0116] - End */
/* TRIGGERS FOR F0116 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0116_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0116_ins
	ON BUSDTA.F0116 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0116_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0116_del.ALAN8= inserted.ALAN8 AND BUSDTA.F0116_del.ALEFTB= inserted.ALEFTB
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0116_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_upd
	ON BUSDTA.F0116 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0116
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0116.ALAN8= inserted.ALAN8 AND BUSDTA.F0116.ALEFTB= inserted.ALEFTB');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0116_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0116_dlt
	ON BUSDTA.F0116 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0116_del (ALAN8, ALEFTB, last_modified )
	SELECT deleted.ALAN8, deleted.ALEFTB, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0116 - END */
