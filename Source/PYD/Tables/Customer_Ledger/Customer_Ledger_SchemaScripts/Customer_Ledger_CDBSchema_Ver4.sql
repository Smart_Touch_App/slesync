
/* [BUSDTA].[Customer_Ledger] - begins */

/* TableDDL - [BUSDTA].[Customer_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Ledger]
	(
	  [CustomerLedgerId] decimal(15,0) NOT NULL
	, [ReceiptID] decimal(15,0) NULL
	, [InvoiceId] decimal(15,0) NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [CustShipToId] NUMERIC(8,0) NOT NULL
	, [IsActive] NCHAR(1) NULL
	, [InvoiceGrossAmt] decimal(15,4) NULL
	, [InvoiceOpenAmt] decimal(15,4) NULL
	, [ReceiptUnAppliedAmt] decimal(15,4) NULL
	, [ReceiptAppliedAmt] decimal(15,4) NULL
	, [ConcessionAmt] decimal(15,4) NULL
	, [ConcessionCodeId] NUMERIC(3,0) NULL
	, [DateApplied] DATETIME NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [CustBillToId] NUMERIC(8,0) NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CUSTOMER_LEDGER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Customer_Ledger] PRIMARY KEY ([CustomerLedgerId] ASC, [RouteId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Customer_Ledger] - End */

/* SHADOW TABLE FOR [BUSDTA].[Customer_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Ledger_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Ledger_del]
	(
	  [CustomerLedgerId] decimal(15,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerLedgerId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Customer_Ledger] - End */
/* TRIGGERS FOR Customer_Ledger - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Customer_Ledger_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Customer_Ledger_ins
	ON BUSDTA.Customer_Ledger AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Customer_Ledger_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Customer_Ledger_del.CustomerLedgerId= inserted.CustomerLedgerId AND BUSDTA.Customer_Ledger_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Customer_Ledger_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Ledger_upd
	ON BUSDTA.Customer_Ledger AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Customer_Ledger
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Customer_Ledger.CustomerLedgerId= inserted.CustomerLedgerId AND BUSDTA.Customer_Ledger.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Customer_Ledger_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Ledger_dlt
	ON BUSDTA.Customer_Ledger AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Customer_Ledger_del (CustomerLedgerId, RouteId, last_modified )
	SELECT deleted.CustomerLedgerId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Customer_Ledger - END */

