
SELECT "BUSDTA"."Customer_Ledger"."CustomerLedgerId"  ,
"BUSDTA"."Customer_Ledger"."ReceiptID"  ,"BUSDTA"."Customer_Ledger"."InvoiceId"  ,"BUSDTA"."Customer_Ledger"."RouteId"  ,
"BUSDTA"."Customer_Ledger"."CustShipToId"  ,"BUSDTA"."Customer_Ledger"."IsActive"  ,"BUSDTA"."Customer_Ledger"."InvoiceGrossAmt"  ,
"BUSDTA"."Customer_Ledger"."InvoiceOpenAmt"  ,"BUSDTA"."Customer_Ledger"."ReceiptUnAppliedAmt"  ,
"BUSDTA"."Customer_Ledger"."ReceiptAppliedAmt"  ,"BUSDTA"."Customer_Ledger"."ConcessionAmt"  ,
"BUSDTA"."Customer_Ledger"."ConcessionCodeId"  ,"BUSDTA"."Customer_Ledger"."DateApplied"  ,"BUSDTA"."Customer_Ledger"."CreatedBy"  ,
"BUSDTA"."Customer_Ledger"."CreatedDatetime"  ,"BUSDTA"."Customer_Ledger"."UpdatedBy"  ,"BUSDTA"."Customer_Ledger"."UpdatedDatetime" ,"BUSDTA"."Customer_Ledger"."CustBillToId"
FROM "BUSDTA"."Customer_Ledger"  
WHERE "BUSDTA"."Customer_Ledger"."last_modified">= {ml s.last_table_download}   
AND "BUSDTA"."Customer_Ledger"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})  