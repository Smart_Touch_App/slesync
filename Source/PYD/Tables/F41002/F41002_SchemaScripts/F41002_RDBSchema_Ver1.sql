
------- /*[BUSDTA].[F41002]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F41002"     (
	"UMMCU" nvarchar(12) not null,
	"UMITM" numeric(8, 0) not null,
	"UMUM" nvarchar(2) not null,
	"UMRUM" nvarchar(2) not null,
	"UMUSTR" nvarchar(1) null,
	"UMCONV" float null,
	"UMCNV1" float null,
	PRIMARY KEY ("UMMCU", "UMITM", "UMUM", "UMRUM")
)
GO

------- /*[BUSDTA].[F41002]*/ - End