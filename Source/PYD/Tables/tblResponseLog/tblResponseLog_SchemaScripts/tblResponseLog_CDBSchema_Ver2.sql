/* [dbo].[tblResponseLog] - begins */

/* TableDDL - [dbo].[tblResponseLog] - Start */
IF OBJECT_ID('[dbo].[tblResponseLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblResponseLog]
	(
	  [ResponseLogID] INT NOT NULL IDENTITY(1,1)
	, [DeviceID] VARCHAR(100) NULL
	, [ResponseOn] DATETIME NULL CONSTRAINT DF_TBLRESPONSELOG_ResponseOn DEFAULT(getdate())
	, [Request] XML NULL
	, [Response] XML NULL
	)

END
/* TableDDL - [dbo].[tblResponseLog] - End */

/* SHADOW TABLE FOR [dbo].[tblResponseLog] - Start */
IF OBJECT_ID('[dbo].[tblResponseLog_del]') IS NULL
BEGIN

 
END
/* SHADOW TABLE FOR [dbo].[tblResponseLog] - End */
/* TRIGGERS FOR tblResponseLog - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('dbo.tblResponseLog_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER dbo.tblResponseLog_ins
	ON dbo.tblResponseLog AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
 
 
/* Create the shadow update trigger. */
IF OBJECT_ID('dbo.tblResponseLog_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblResponseLog_upd
	ON dbo.tblResponseLog AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
 
 
/* Create the shadow delete trigger */
IF OBJECT_ID('dbo.tblResponseLog_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER dbo.tblResponseLog_dlt
	ON dbo.tblResponseLog AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
 
/* TRIGGERS FOR tblResponseLog - END */
