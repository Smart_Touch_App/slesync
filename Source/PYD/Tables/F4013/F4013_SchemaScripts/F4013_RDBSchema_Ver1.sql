------- /*[BUSDTA].[F4013]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4013"     (
	"SXXRTC" nvarchar(2) not null,
	"SXXRVF" nvarchar(30) not null,
	"SXXRVT" nvarchar(30) not null,
	"SXEDF1" nvarchar(1) not null,
	"SXDESC" nvarchar(30) null,
	PRIMARY KEY ("SXXRTC", "SXXRVF", "SXXRVT", "SXEDF1")
)

------- /*[BUSDTA].[F4013]*/ - End