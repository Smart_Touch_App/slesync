 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ItemUoMs"
SET "CrossReferenceID" = {ml r."CrossReferenceID"}, "CanSell" = {ml r."CanSell"}, "CanSample" = {ml r."CanSample"}, "CanRestock" = {ml r."CanRestock"}, "DisplaySeq" = {ml r."DisplaySeq"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "ItemID" = {ml r."ItemID"} AND "UOM" = {ml r."UOM"}
 
