SELECT "BUSDTA"."ReasonCodeMaster"."ReasonCodeId",
	"BUSDTA"."ReasonCodeMaster"."ReasonCode",
	"BUSDTA"."ReasonCodeMaster"."ReasonCodeDescription",
	"BUSDTA"."ReasonCodeMaster"."ReasonCodeType"
FROM "BUSDTA"."ReasonCodeMaster"
WHERE "BUSDTA"."ReasonCodeMaster"."last_modified" >= {ml s.last_table_download}