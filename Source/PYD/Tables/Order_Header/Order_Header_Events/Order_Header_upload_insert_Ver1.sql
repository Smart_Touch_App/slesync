/* 
 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Order_Header"
("Order_ID", "Customer_Id", "Order_Date", "Created_By", "Created_On", "Is_Deleted", "Total_Coffee", "Total_Allied", "Energy_Surcharge", "Order_Total_Amt", "Sales_Tax_Amt", "Invoice_Total", "Surcharge_Reason_Code", "payment_type", "payment_id", "Order_State", "Order_Sub_State", "updated_at", "VoidReason", "OrderSeries", "RouteNo", "ChargeOnAccount", "HoldCommitted")
VALUES({ml r."Order_ID"}, {ml r."Customer_Id"}, {ml r."Order_Date"}, {ml r."Created_By"}, {ml r."Created_On"}, {ml r."Is_Deleted"}, {ml r."Total_Coffee"}, {ml r."Total_Allied"}, {ml r."Energy_Surcharge"}, {ml r."Order_Total_Amt"}, {ml r."Sales_Tax_Amt"}, {ml r."Invoice_Total"}, {ml r."Surcharge_Reason_Code"}, {ml r."payment_type"}, {ml r."payment_id"}, {ml r."Order_State"}, {ml r."Order_Sub_State"}, {ml r."updated_at"}, {ml r."VoidReason"}, {ml r."OrderSeries"}, {ml r."RouteNo"}, {ml r."ChargeOnAccount"}, {ml r."HoldCommitted"})
 
*/
