/* [BUSDTA].[MoneyOrderDetails] - begins */

/* TableDDL - [BUSDTA].[MoneyOrderDetails] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrderDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrderDetails]
	(
	  [MoneyOrderId] NUMERIC NOT NULL
	, [MoneyOrderNumber] NCHAR(10) NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [MoneyOrderAmount] NUMERIC NULL
	, [MoneyOrderFeeAmount] NUMERIC NULL
	, [StatusId] NUMERIC NULL
	, [MoneyOrderDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([MoneyOrderId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[MoneyOrderDetails] - End */
