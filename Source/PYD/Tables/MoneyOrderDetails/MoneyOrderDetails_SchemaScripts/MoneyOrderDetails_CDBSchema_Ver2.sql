/* [BUSDTA].[MoneyOrderDetails] - begins */

/* TableDDL - [BUSDTA].[MoneyOrderDetails] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrderDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrderDetails]
	(
	  [MoneyOrderId] NUMERIC(8,0) NOT NULL
	, [MoneyOrderNumber] NCHAR(10) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [MoneyOrderAmount] NUMERIC(8,4) NULL
	, [MoneyOrderFeeAmount] NUMERIC(8,4) NULL
	, [StatusId] NUMERIC(8,0) NULL
	, [MoneyOrderDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC(8,0) NULL
	, [RouteSettlementId] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_MONEYORDERDETAILS_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_MoneyOrderDetails] PRIMARY KEY ([MoneyOrderId] ASC, [RouteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[MoneyOrderDetails] - End */

/* SHADOW TABLE FOR [BUSDTA].[MoneyOrderDetails] - Start */
IF OBJECT_ID('[BUSDTA].[MoneyOrderDetails_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[MoneyOrderDetails_del]
	(
	  [MoneyOrderId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([MoneyOrderId] ASC, [RouteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[MoneyOrderDetails] - End */
/* TRIGGERS FOR MoneyOrderDetails - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.MoneyOrderDetails_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.MoneyOrderDetails_ins
	ON BUSDTA.MoneyOrderDetails AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.MoneyOrderDetails_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.MoneyOrderDetails_del.MoneyOrderId= inserted.MoneyOrderId AND BUSDTA.MoneyOrderDetails_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.MoneyOrderDetails_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrderDetails_upd
	ON BUSDTA.MoneyOrderDetails AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.MoneyOrderDetails
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.MoneyOrderDetails.MoneyOrderId= inserted.MoneyOrderId AND BUSDTA.MoneyOrderDetails.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.MoneyOrderDetails_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.MoneyOrderDetails_dlt
	ON BUSDTA.MoneyOrderDetails AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.MoneyOrderDetails_del (MoneyOrderId, RouteId, last_modified )
	SELECT deleted.MoneyOrderId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR MoneyOrderDetails - END */
