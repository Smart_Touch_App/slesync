
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[tblRefreshDetails]') AND type in (N'U'))
DROP TABLE [dbo].[tblRefreshDetails]
GO
CREATE TABLE [dbo].[tblRefreshDetails](
	[ObjectID] [int] IDENTITY(1,1) NOT NULL,
	[Objectname] [varchar](50) NULL,
	[TruncTimeAtRefDB] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[ObjectID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_CTCurrentVersion]'))
DROP VIEW [dbo].[vw_CTCurrentVersion]
GO
CREATE VIEW [dbo].[vw_CTCurrentVersion] 
AS
SELECT CHANGE_TRACKING_CURRENT_VERSION() CTCV
GO
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_GetRefreshdetails]'))
DROP VIEW [dbo].[vw_GetRefreshdetails]
GO
CREATE VIEW [dbo].[vw_GetRefreshdetails] 
AS
SELECT * FROM dbo.tblRefreshDetails
GO

USE [MobileDataModel]
GO
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ReferenceRefreshDetails')
DROP SYNONYM [dbo].[ReferenceRefreshDetails]
GO
IF  EXISTS (SELECT * FROM sys.synonyms WHERE name = N'ReferenceSyncVersion')
DROP SYNONYM [dbo].[ReferenceSyncVersion]
GO
CREATE SYNONYM [dbo].[ReferenceRefreshDetails] FOR [CIS54\SQLEXPRESS2012].[JDEData_CRP].[dbo].[vw_GetRefreshdetails]
GO
CREATE SYNONYM [dbo].[ReferenceSyncVersion] FOR [CIS54\SQLEXPRESS2012].[JDEData_CRP].[dbo].[vw_CTCurrentVersion]
GO

