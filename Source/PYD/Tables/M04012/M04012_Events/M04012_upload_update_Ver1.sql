/*
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M04012"
SET "PMALPH" = {ml r."PMALPH"}, "PMROUT" = {ml r."PMROUT"}, "PMCRBY" = {ml r."PMCRBY"}, "PMCRDT" = {ml r."PMCRDT"}, "PMUPBY" = {ml r."PMUPBY"}, "PMUPDT" = {ml r."PMUPDT"}, "PMSRS" = {ml r."PMSRS"}
WHERE "PMAN8" = {ml r."PMAN8"}
 
*/
