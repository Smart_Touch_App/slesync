 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Route_Settlement_Detail"
SET "UserId" = {ml r."UserId"}, "VerificationNum" = {ml r."VerificationNum"}, "CashAmount" = {ml r."CashAmount"}, "CheckAmount" = {ml r."CheckAmount"}, "MoneyOrderAmount" = {ml r."MoneyOrderAmount"}, "TotalVerified" = {ml r."TotalVerified"}, "Expenses" = {ml r."Expenses"}, "Payments" = {ml r."Payments"}, "OverShortAmount" = {ml r."OverShortAmount"}, "VerSignature" = {ml r."VerSignature"}, "OriginatingRoute" = {ml r."OriginatingRoute"}, "partitioningRoute" = {ml r."partitioningRoute"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "RouteId" = {ml r."RouteId"} AND "SettlementDetailId" = {ml r."SettlementDetailId"} AND "SettlementId" = {ml r."SettlementId"}
 
