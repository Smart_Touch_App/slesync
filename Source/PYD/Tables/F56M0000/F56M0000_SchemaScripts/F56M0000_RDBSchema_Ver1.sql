------- /*[BUSDTA].[F56M0000]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F56M0000"     (
	"GFSY" nvarchar(4) not null,
	"GFCXPJ" numeric(18, 0) null,
	"GFDTEN" numeric(18, 0) null,
	"GFLAVJ" numeric(18, 0) null,
	"GFOBJ" nvarchar(6) null,
	"GFMCU" nvarchar(12) null,
	"GFSUB" nvarchar(8) null,
	"GFPST" nvarchar(1) null,
	"GFEV01" nvarchar(1) null,
	"GFEV02" nvarchar(1) null,
	"GFEV03" nvarchar(1) null,
	"GFMATH01" float null,
	"GFMATH02" float null,
	"GFMATH03" float null,
	"GFCFSTR1" nvarchar(3) null,
	"GFCFSTR2" nvarchar(8) null,
	"GFGS1A" nvarchar(10) null,
	"GFGS1B" nvarchar(10) null,
	"GFGS2A" nvarchar(20) null,
	"GFGS2B" nvarchar(20) null,
	PRIMARY KEY ("GFSY")
)

------- /*[BUSDTA].[F56M0000]*/ - End