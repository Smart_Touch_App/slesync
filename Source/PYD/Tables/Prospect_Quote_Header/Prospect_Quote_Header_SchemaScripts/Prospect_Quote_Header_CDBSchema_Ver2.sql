/* [BUSDTA].[Prospect_Quote_Header] - begins */

/* TableDDL - [BUSDTA].[Prospect_Quote_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Header]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Header]
	(
	  [ProspectQuoteId] decimal (15,0) NOT NULL
	, [ProspectId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [StatusId] NUMERIC(3,0) NOT NULL
	, [IsPrinted] NCHAR(1) NULL
	, [IsSampled] NCHAR(1) NULL
	, [SettlementId] decimal (15,0) NOT NULL
	, [PickStatus] NCHAR(1) NULL
	, [IsMasterSetup] NCHAR(1) NULL
	, [AIAC05District] NCHAR(3) NULL
	, [AIAC05DistrictMst] NCHAR(3) NULL
	, [AIAC06Region] NCHAR(3) NULL
	, [AIAC06RegionMst] NCHAR(3) NULL
	, [AIAC07Reporting] NCHAR(3) NULL
	, [AIAC07ReportingMst] NCHAR(3) NULL
	, [AIAC08Chain] NCHAR(3) NULL
	, [AIAC08ChainMst] NCHAR(3) NULL
	, [AIAC09BrewmaticAgentCode] NCHAR(3) NULL
	, [AIAC09BrewmaticAgentCodeMst] NCHAR(3) NULL
	, [AIAC10NTR] NCHAR(3) NULL
	, [AIAC10NTRMst] NCHAR(3) NULL
	, [AIAC11CustomerTaxGrp] NCHAR(3) NULL
	, [AIAC11CustomerTaxGrpMst] NCHAR(3) NULL
	, [AIAC12CategoryCode12] NCHAR(3) NULL
	, [AIAC12CategoryCode12Mst] NCHAR(3) NULL
	, [AIAC13APCheckCode] NCHAR(3) NULL
	, [AIAC13APCheckCodeMst] NCHAR(3) NULL
	, [AIAC14CategoryCode14] NCHAR(3) NULL
	, [AIAC14CategoryCode14Mst] NCHAR(3) NULL
	, [AIAC15CategoryCode15] NCHAR(3) NULL
	, [AIAC15CategoryCode15Mst] NCHAR(3) NULL
	, [AIAC16CategoryCode16] NCHAR(3) NULL
	, [AIAC16CategoryCode16Mst] NCHAR(3) NULL
	, [AIAC17CategoryCode17] NCHAR(3) NULL
	, [AIAC17CategoryCode17Mst] NCHAR(3) NULL
	, [AIAC18CategoryCode18] NCHAR(3) NULL
	, [AIAC18CategoryCode18Mst] NCHAR(3) NULL
	, [AIAC22POSUpCharge] NCHAR(3) NULL
	, [AIAC22POSUpChargeMst] NCHAR(3) NULL
	, [AIAC23LiquidCoffee] NCHAR(3) NULL
	, [AIAC23LiquidCoffeeMst] NCHAR(3) NULL
	, [AIAC24PriceProtection] NCHAR(3) NULL
	, [AIAC24PriceProtectionMst] NCHAR(3) NULL
	, [AIAC27AlliedDiscount] NCHAR(3) NULL
	, [AIAC27AlliedDiscountMst] NCHAR(3) NULL
	, [AIAC28CoffeeVolume] NCHAR(3) NULL
	, [AIAC28CoffeeVolumeMst] NCHAR(3) NULL
	, [AIAC29EquipmentProgPts] NCHAR(3) NULL
	, [AIAC29EquipmentProgPtsMst] NCHAR(3) NULL
	, [AIAC30SpecialCCP] NCHAR(3) NULL
	, [AIAC30SpecialCCPMst] NCHAR(3) NULL
	, [PriceDate] DATETIME NOT NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_PROSPECT_QUOTE_HEADER_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Prospect_Quote_Header] PRIMARY KEY ([ProspectQuoteId] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Prospect_Quote_Header] - End */

/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Header] - Start */
IF OBJECT_ID('[BUSDTA].[Prospect_Quote_Header_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Prospect_Quote_Header_del]
	(
	  [ProspectQuoteId] decimal (15,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ProspectQuoteId] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Prospect_Quote_Header] - End */
/* TRIGGERS FOR Prospect_Quote_Header - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Header_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Prospect_Quote_Header_ins
	ON BUSDTA.Prospect_Quote_Header AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Prospect_Quote_Header_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Prospect_Quote_Header_del.ProspectQuoteId= inserted.ProspectQuoteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Header_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Header_upd
	ON BUSDTA.Prospect_Quote_Header AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Prospect_Quote_Header
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Prospect_Quote_Header.ProspectQuoteId= inserted.ProspectQuoteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Prospect_Quote_Header_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Prospect_Quote_Header_dlt
	ON BUSDTA.Prospect_Quote_Header AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Prospect_Quote_Header_del (ProspectQuoteId, last_modified )
	SELECT deleted.ProspectQuoteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Prospect_Quote_Header - END */
