
/* [BUSDTA].[Customer_Quote_Detail] - begins */

/* TableDDL - [BUSDTA].[Customer_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Quote_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Quote_Detail]
	(
	  [CustomerQuoteId] NUMERIC(8,0) NOT NULL
	, [CustomerQuoteDetailId] NUMERIC(8,0) NOT NULL
	, [RouteId] NUMERIC(8,0) NOT NULL
	, [ItemId] NUMERIC(8,0) NOT NULL
	, [QuoteQty] NUMERIC(4,0) NOT NULL
	, [TransactionUM] NCHAR(2) NULL
	, [PricingAmt] NUMERIC(8,4) NULL
	, [PricingUM] NCHAR(2) NULL
	, [TransactionAmt] NUMERIC(8,4) NULL
	, [OtherAmt] NUMERIC(8,4) NULL
	, [OtherUM] NCHAR(2) NULL
	, [CompetitorName] NVARCHAR(50) NOT NULL
	, [UnitPriceAmt] NUMERIC(8,4) NULL
	, [UnitPriceUM] NCHAR(2) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NULL CONSTRAINT DF_CUSTOMER_QUOTE_DETAIL_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Customer_Quote_Detail] PRIMARY KEY ([CustomerQuoteId] ASC, [CustomerQuoteDetailId] ASC, [RouteId] ASC)
	)

END
Go
/* TableDDL - [BUSDTA].[Customer_Quote_Detail] - End */

/* SHADOW TABLE FOR [BUSDTA].[Customer_Quote_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Customer_Quote_Detail_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Customer_Quote_Detail_del]
	(
	  [CustomerQuoteId] NUMERIC(8,0)
	, [CustomerQuoteDetailId] NUMERIC(8,0)
	, [RouteId] NUMERIC(8,0)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CustomerQuoteId] ASC, [CustomerQuoteDetailId] ASC, [RouteId] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[Customer_Quote_Detail] - End */
/* TRIGGERS FOR Customer_Quote_Detail - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Customer_Quote_Detail_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Customer_Quote_Detail_ins
	ON BUSDTA.Customer_Quote_Detail AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Customer_Quote_Detail_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Customer_Quote_Detail_del.CustomerQuoteDetailId= inserted.CustomerQuoteDetailId AND BUSDTA.Customer_Quote_Detail_del.CustomerQuoteId= inserted.CustomerQuoteId AND BUSDTA.Customer_Quote_Detail_del.RouteId= inserted.RouteId
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Customer_Quote_Detail_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Quote_Detail_upd
	ON BUSDTA.Customer_Quote_Detail AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Customer_Quote_Detail
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Customer_Quote_Detail.CustomerQuoteDetailId= inserted.CustomerQuoteDetailId AND BUSDTA.Customer_Quote_Detail.CustomerQuoteId= inserted.CustomerQuoteId AND BUSDTA.Customer_Quote_Detail.RouteId= inserted.RouteId');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Customer_Quote_Detail_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Customer_Quote_Detail_dlt
	ON BUSDTA.Customer_Quote_Detail AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Customer_Quote_Detail_del (CustomerQuoteDetailId, CustomerQuoteId, RouteId, last_modified )
	SELECT deleted.CustomerQuoteDetailId, deleted.CustomerQuoteId, deleted.RouteId, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Customer_Quote_Detail - END */

