
/* [BUSDTA].[F4076] - begins */

/* TableDDL - [BUSDTA].[F4076] - Start */
IF OBJECT_ID('[BUSDTA].[F4076]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4076]	(	  [FMFRMN] NCHAR(10) NOT NULL	, [FMFML] NCHAR(160) NULL	, [FMAPRS] NCHAR(1) NULL	, [FMUPMJ] NUMERIC NOT NULL	, [FMTDAY] NUMERIC NOT NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4076_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F4076] PRIMARY KEY ([FMFRMN] ASC, [FMUPMJ] ASC, [FMTDAY] ASC)	)
END
/* TableDDL - [BUSDTA].[F4076] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4076] - Start */
IF OBJECT_ID('[BUSDTA].[F4076_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4076_del]	(	  [FMFRMN] NCHAR(10)	, [FMUPMJ] NUMERIC	, [FMTDAY] NUMERIC	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([FMFRMN] ASC, [FMUPMJ] ASC, [FMTDAY] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[F4076] - End */
/* TRIGGERS FOR F4076 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4076_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4076_ins
	ON BUSDTA.F4076 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4076_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4076_del.FMFRMN= inserted.FMFRMN AND BUSDTA.F4076_del.FMTDAY= inserted.FMTDAY AND BUSDTA.F4076_del.FMUPMJ= inserted.FMUPMJ
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4076_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4076_upd
	ON BUSDTA.F4076 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4076
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4076.FMFRMN= inserted.FMFRMN AND BUSDTA.F4076.FMTDAY= inserted.FMTDAY AND BUSDTA.F4076.FMUPMJ= inserted.FMUPMJ');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4076_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4076_dlt
	ON BUSDTA.F4076 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4076_del (FMFRMN, FMTDAY, FMUPMJ, last_modified )
	SELECT deleted.FMFRMN, deleted.FMTDAY, deleted.FMUPMJ, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4076 - END */
