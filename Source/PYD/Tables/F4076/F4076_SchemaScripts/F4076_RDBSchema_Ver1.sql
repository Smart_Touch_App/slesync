
------- /*[BUSDTA].[F4076]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4076"     (
	"FMFRMN" nvarchar(10) not null,
	"FMFML" nvarchar(160) null,
	"FMAPRS" nvarchar(1) null,
	"FMUPMJ" numeric(18, 0) not null,
	"FMTDAY" numeric(6, 0) not null,
	PRIMARY KEY ("FMFRMN", "FMUPMJ", "FMTDAY")
)

------- /*[BUSDTA].[F4076]*/ - End