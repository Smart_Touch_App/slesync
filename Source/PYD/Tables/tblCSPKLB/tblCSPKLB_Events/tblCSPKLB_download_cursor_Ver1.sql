 
SELECT "dbo"."tblCSPKLB"."CSPKLBID"
,"dbo"."tblCSPKLB"."CSPKLBType"
,"dbo"."tblCSPKLB"."CreatedBy"
,"dbo"."tblCSPKLB"."CreatedDatetime"
,"dbo"."tblCSPKLB"."UpdatedBy"
,"dbo"."tblCSPKLB"."UpdatedDatetime"

FROM "dbo"."tblCSPKLB"
WHERE "dbo"."tblCSPKLB"."last_modified">= {ml s.last_table_download}
