 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Route_Replenishment_Detail"
("ReplenishmentID", "ReplenishmentDetailID", "ItemId", "RouteId", "ReplenishmentQtyUM", "ReplenishmentQty", "AdjustmentQty", "PickedQty", "PickedStatusTypeID", "AvailaibilityAtTime", "CurrentAvailability", "DemandQty", "ParLevelQty", "OpenReplnQty", "ShippedQty", "OrderID", "LastSaved", "SequenceNumber", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."ReplenishmentID"}, {ml r."ReplenishmentDetailID"}, {ml r."ItemId"}, {ml r."RouteId"}, {ml r."ReplenishmentQtyUM"}, {ml r."ReplenishmentQty"}, {ml r."AdjustmentQty"}, {ml r."PickedQty"}, {ml r."PickedStatusTypeID"}, {ml r."AvailaibilityAtTime"}, {ml r."CurrentAvailability"}, {ml r."DemandQty"}, {ml r."ParLevelQty"}, {ml r."OpenReplnQty"}, {ml r."ShippedQty"}, {ml r."OrderID"}, {ml r."LastSaved"}, {ml r."SequenceNumber"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
