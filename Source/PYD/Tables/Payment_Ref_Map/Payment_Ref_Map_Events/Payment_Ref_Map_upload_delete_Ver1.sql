/* Delete the row from the consolidated database. */
DELETE FROM "BUSDTA"."Payment_Ref_Map"
WHERE "Payment_Ref_Map_Id" = {ml r."Payment_Ref_Map_Id"}
	AND "Payment_Id" = {ml r."Payment_Id"}
	AND "Ref_Id" = {ml r."Ref_Id"}