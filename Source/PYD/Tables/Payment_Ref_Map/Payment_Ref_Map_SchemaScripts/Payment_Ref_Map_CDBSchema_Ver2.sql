/* [BUSDTA].[Payment_Ref_Map] - begins */

/* TableDDL - [BUSDTA].[Payment_Ref_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Payment_Ref_Map]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Payment_Ref_Map]
	(
	  [Payment_Ref_Map_Id] INT NOT NULL
	, [Payment_Id] INT NOT NULL
	, [Ref_Id] INT NOT NULL
	, [Ref_Type] NCHAR(3) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_PAYMENT_REF_MAP_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_Payment_Ref_Map] PRIMARY KEY ([Payment_Ref_Map_Id] ASC, [Payment_Id] ASC, [Ref_Id] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[Payment_Ref_Map] - End */

/* SHADOW TABLE FOR [BUSDTA].[Payment_Ref_Map] - Start */
IF OBJECT_ID('[BUSDTA].[Payment_Ref_Map_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Payment_Ref_Map_del]
	(
	  [Payment_Ref_Map_Id] INT
	, [Payment_Id] INT
	, [Ref_Id] INT
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([Payment_Ref_Map_Id] ASC, [Payment_Id] ASC, [Ref_Id] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[Payment_Ref_Map] - End */
/* TRIGGERS FOR Payment_Ref_Map - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.Payment_Ref_Map_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.Payment_Ref_Map_ins
	ON BUSDTA.Payment_Ref_Map AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.Payment_Ref_Map_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.Payment_Ref_Map_del.Payment_Id= inserted.Payment_Id AND BUSDTA.Payment_Ref_Map_del.Payment_Ref_Map_Id= inserted.Payment_Ref_Map_Id AND BUSDTA.Payment_Ref_Map_del.Ref_Id= inserted.Ref_Id
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.Payment_Ref_Map_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Payment_Ref_Map_upd
	ON BUSDTA.Payment_Ref_Map AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.Payment_Ref_Map
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.Payment_Ref_Map.Payment_Id= inserted.Payment_Id AND BUSDTA.Payment_Ref_Map.Payment_Ref_Map_Id= inserted.Payment_Ref_Map_Id AND BUSDTA.Payment_Ref_Map.Ref_Id= inserted.Ref_Id');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.Payment_Ref_Map_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.Payment_Ref_Map_dlt
	ON BUSDTA.Payment_Ref_Map AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.Payment_Ref_Map_del (Payment_Id, Payment_Ref_Map_Id, Ref_Id, last_modified )
	SELECT deleted.Payment_Id, deleted.Payment_Ref_Map_Id, deleted.Ref_Id, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR Payment_Ref_Map - END */
