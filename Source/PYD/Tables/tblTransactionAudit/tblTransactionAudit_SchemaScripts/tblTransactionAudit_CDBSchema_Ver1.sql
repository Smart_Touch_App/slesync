/* [BUSDTA].[tblTransactionAudit] - begins */

/* TableDDL - [BUSDTA].[tblTransactionAudit] - Start */
IF OBJECT_ID('[BUSDTA].[tblTransactionAudit]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[tblTransactionAudit]
	(
	  [AuditID] UNIQUEIDENTIFIER NOT NULL
	, [Action] NVARCHAR(50) NULL
	, [SchemaName] NVARCHAR(255) NOT NULL
	, [TableName] VARCHAR(200) NULL
	, [FieldName] VARCHAR(200) NULL
	, [OldValue] NVARCHAR(4000) NULL
	, [NewValue] NVARCHAR(4000) NULL
	, [IsPkey] BIT NULL
	, [TransactionID] UNIQUEIDENTIFIER NULL
	, [CreatedOn] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONAUDIT_CreatedOn DEFAULT(getdate())
	, [Createdby] VARCHAR(50) NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_TBLTRANSACTIONAUDIT_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_tblTransactionAudit] PRIMARY KEY ([AuditID] ASC)
	)

END
/* TableDDL - [BUSDTA].[tblTransactionAudit] - End */
