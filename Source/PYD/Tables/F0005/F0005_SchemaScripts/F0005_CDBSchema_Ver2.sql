
/* [BUSDTA].[F0005] - begins */

/* TableDDL - [BUSDTA].[F0005] - Start */
IF OBJECT_ID('[BUSDTA].[F0005]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0005]
	(
	  [DRSY] NCHAR(4) NOT NULL
	, [DRRT] NCHAR(2) NOT NULL
	, [DRKY] NCHAR(10) NOT NULL
	, [DRDL01] NCHAR(30) NULL
	, [DRDL02] NCHAR(30) NULL
	, [DRSPHD] NCHAR(10) NULL
	, [DRHRDC] NCHAR(1) NULL
	, [DRYN] BIT NULL CONSTRAINT DF_F0005_DRYN DEFAULT((1))
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F0005_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_F0005] PRIMARY KEY ([DRSY] ASC, [DRRT] ASC, [DRKY] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[F0005] - End */

/* SHADOW TABLE FOR [BUSDTA].[F0005] - Start */
IF OBJECT_ID('[BUSDTA].[F0005_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F0005_del]
	(
	  [DRSY] NCHAR(4)
	, [DRRT] NCHAR(2)
	, [DRKY] NCHAR(10)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([DRSY] ASC, [DRRT] ASC, [DRKY] ASC)
	)

END
GO
/* SHADOW TABLE FOR [BUSDTA].[F0005] - End */
/* TRIGGERS FOR F0005 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F0005_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F0005_ins
	ON BUSDTA.F0005 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F0005_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F0005_del.DRKY= inserted.DRKY AND BUSDTA.F0005_del.DRRT= inserted.DRRT AND BUSDTA.F0005_del.DRSY= inserted.DRSY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F0005_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0005_upd
	ON BUSDTA.F0005 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F0005
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F0005.DRKY= inserted.DRKY AND BUSDTA.F0005.DRRT= inserted.DRRT AND BUSDTA.F0005.DRSY= inserted.DRSY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F0005_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F0005_dlt
	ON BUSDTA.F0005 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F0005_del (DRKY, DRRT, DRSY, last_modified )
	SELECT deleted.DRKY, deleted.DRRT, deleted.DRSY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F0005 - END */

