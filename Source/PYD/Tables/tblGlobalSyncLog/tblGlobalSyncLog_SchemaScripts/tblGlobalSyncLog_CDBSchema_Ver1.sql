
/* [dbo].[tblGlobalSyncLog] - begins */

/* TableDDL - [dbo].[tblGlobalSyncLog] - Start */
IF OBJECT_ID('[dbo].[tblGlobalSyncLog]') IS NULL
BEGIN

	CREATE TABLE [dbo].[tblGlobalSyncLog]
	(
	  [GlobalSyncLogID] INT NOT NULL IDENTITY(1,1)
	, [GlobalSyncID] INT NULL
	, [DeviceID] VARCHAR(100) NULL
	, [CreatedOn] DATETIME NULL CONSTRAINT DF_TBLGLOBALSYNCLOG_CreatedOn DEFAULT(getdate())
	, CONSTRAINT [PK_tblGlobalSyncLog] PRIMARY KEY ([GlobalSyncLogID] ASC)
	)

ALTER TABLE [dbo].[tblGlobalSyncLog] WITH CHECK ADD CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync] FOREIGN KEY([GlobalSyncID]) REFERENCES [dbo].[tblGlobalSync] ([GlobalSyncID])
ALTER TABLE [dbo].[tblGlobalSyncLog] CHECK CONSTRAINT [FK_tblGlobalSyncLog_tblGlobalSync]

END
/* TableDDL - [dbo].[tblGlobalSyncLog] - End */
