SELECT "BUSDTA"."F0116_Audit"."TransID"
,"BUSDTA"."F0116_Audit"."TransType"
,"BUSDTA"."F0116_Audit"."ALAN8"
,"BUSDTA"."F0116_Audit"."ALEFTB"
,"BUSDTA"."F0116_Audit"."ALEFTF"
,"BUSDTA"."F0116_Audit"."ALADD1"
,"BUSDTA"."F0116_Audit"."ALADD2"
,"BUSDTA"."F0116_Audit"."ALADD3"
,"BUSDTA"."F0116_Audit"."ALADD4"
,"BUSDTA"."F0116_Audit"."ALADDZ"
,"BUSDTA"."F0116_Audit"."ALCTY1"
,"BUSDTA"."F0116_Audit"."ALCOUN"
,"BUSDTA"."F0116_Audit"."ALADDS"
,"BUSDTA"."F0116_Audit"."WSValidation"
,"BUSDTA"."F0116_Audit"."Status"
,"BUSDTA"."F0116_Audit"."RejReason"
,"BUSDTA"."F0116_Audit"."SyncID"
,"BUSDTA"."F0116_Audit"."CreatedDateTime"
,"BUSDTA"."F0116_Audit"."UserID"
,"BUSDTA"."F0116_Audit"."RouteID"

FROM "BUSDTA"."F0116_Audit", BUSDTA.Route_Master rm, BUSDTA.Customer_Route_Map crm  
WHERE ("BUSDTA"."F0116_Audit"."last_modified" >= {ml s.last_table_download} or rm."last_modified" >= {ml s.last_table_download}  
or crm."last_modified" >= {ml s.last_table_download})   AND ALAN8=crm.RelatedAddressBookNumber AND crm.BranchNumber = rm.BranchNumber  
AND RouteMasterID = (select RouteMasterID from busdta.Route_Master where RouteName = {ml s.username} ) 