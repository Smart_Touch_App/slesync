/* [BUSDTA].[Inventory_Ledger] - begins */

/* TableDDL - [BUSDTA].[Inventory_Ledger] - Start */
IF OBJECT_ID('[BUSDTA].[Inventory_Ledger]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Inventory_Ledger]
	(
	  [InventoryLedgerID] NUMERIC NOT NULL
	, [ItemId] NUMERIC NOT NULL
	, [ItemNumber] NCHAR(25) NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [TransactionQty] NUMERIC NULL
	, [TransactionQtyUM] NCHAR(2) NULL
	, [TransactionQtyPrimaryUM] NCHAR(2) NULL
	, [TransactionType] NUMERIC NULL
	, [TransactionId] NUMERIC NULL
	, [SettlementID] NUMERIC NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([InventoryLedgerID] ASC, [ItemId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Inventory_Ledger] - End */
