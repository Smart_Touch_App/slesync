 
/* Insert the row into the consolidated database. */
INSERT INTO "BUSDTA"."Inventory_Ledger"
("InventoryLedgerID", "ItemId", "ItemNumber", "RouteId", "TransactionQty", "TransactionQtyUM", "TransactionQtyPrimaryUM", "TransactionType", "TransactionId", "SettlementID", "CreatedBy", "CreatedDatetime", "UpdatedBy", "UpdatedDatetime")
VALUES({ml r."InventoryLedgerID"}, {ml r."ItemId"}, {ml r."ItemNumber"}, {ml r."RouteId"}, {ml r."TransactionQty"}, {ml r."TransactionQtyUM"}, {ml r."TransactionQtyPrimaryUM"}, {ml r."TransactionType"}, {ml r."TransactionId"}, {ml r."SettlementID"}, {ml r."CreatedBy"}, {ml r."CreatedDatetime"}, {ml r."UpdatedBy"}, {ml r."UpdatedDatetime"})
 
