 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."SalesOrder_ReturnOrder_Mapping"
SET "ItemID" = {ml r."ItemID"}, "RouteId" = {ml r."RouteId"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "SalesOrderId" = {ml r."SalesOrderId"} AND "ReturnOrderID" = {ml r."ReturnOrderID"} AND "OrderDetailID" = {ml r."OrderDetailID"}
