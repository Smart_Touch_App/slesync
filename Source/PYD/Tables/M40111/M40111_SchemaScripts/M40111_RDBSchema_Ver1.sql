
------- /*[BUSDTA].[M40111]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M40111" (
	"PCAN8" NUMERIC(8,0) NOT NULL,
	"PCIDLN" NUMERIC(5,0) NOT NULL,
	"PCAR1" NVARCHAR(6) NULL,
	"PCPH1" NVARCHAR(20) NULL,
	"PCEXTN1" NVARCHAR(8) NULL,
	"PCPHTP1" NUMERIC(8,0) NULL,
	"PCDFLTPH1" BIT NULL DEFAULT ((1)),
	"PCREFPH1" INTEGER NULL,
	"PCAR2" NVARCHAR(6) NULL,
	"PCPH2" NVARCHAR(20) NULL,
	"PCEXTN2" NVARCHAR(8) NULL,
	"PCPHTP2" NUMERIC(8,0) NULL,
	"PCDFLTPH2" BIT NULL DEFAULT ((0)),
	"PCREFPH2" INTEGER NULL,
	"PCAR3" NVARCHAR(6) NULL,
	"PCPH3" NVARCHAR(20) NULL,
	"PCEXTN3" NVARCHAR(8) NULL,
	"PCPHTP3" NUMERIC(8,0) NULL,
	"PCDFLTPH3" BIT NULL DEFAULT ((0)),
	"PCREFPH3" INTEGER NULL,
	"PCAR4" NVARCHAR(6) NULL,
	"PCPH4" NVARCHAR(20) NULL,
	"PCEXTN4" NVARCHAR(8) NULL,
	"PCPHTP4" NUMERIC(8,0) NULL,
	"PCDFLTPH4" BIT NULL DEFAULT ((0)),
	"PCREFPH4" INTEGER NULL,
	"PCEMAL1" NVARCHAR(256) NULL,
	"PCETP1" NUMERIC(3,0) NULL,
	"PCDFLTEM1" BIT NULL DEFAULT ((1)),
	"PCREFEM1" INTEGER NULL,
	"PCEMAL2" NVARCHAR(256) NULL,
	"PCETP2" NUMERIC(3,0) NULL,
	"PCDFLTEM2" BIT NULL DEFAULT ((0)),
	"PCREFEM2" INTEGER NULL,
	"PCEMAL3" NVARCHAR(256) NULL,
	"PCETP3" NUMERIC(3,0) NULL,
	"PCDFLTEM3" BIT NULL DEFAULT ((0)),
	"PCREFEM3" INTEGER NULL,
	"PCGNNM" NVARCHAR(75) NULL,
	"PCMDNM" NVARCHAR(25) NULL,
	"PCSRNM" NVARCHAR(25) NULL,
	"PCTITL" NVARCHAR(50) NULL,
	"PCACTV" BIT NULL,
	"PCDFLT" BIT NULL DEFAULT ((0)),
	"PCSET" INTEGER NULL,
	"PCCRBY" NCHAR(25) NULL,
	"PCCRDT" DATE NULL,
	"PCUPBY" NCHAR(25) NULL,
	"PCUPDT" DATE NULL,
	PRIMARY KEY ( "PCAN8" ASC, "PCIDLN" ASC )
)

------- /*[BUSDTA].[M40111]*/ - End
