/* [BUSDTA].[ExpenseDetails] - begins */

/* TableDDL - [BUSDTA].[ExpenseDetails] - Start */
IF OBJECT_ID('[BUSDTA].[ExpenseDetails]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ExpenseDetails]
	(
	  [ExpenseId] NUMERIC NOT NULL
	, [RouteId] NUMERIC NOT NULL
	, [CategoryId] NUMERIC NOT NULL
	, [ExpensesExplanation] NCHAR(100) NULL
	, [ExpenseAmount] NUMERIC NULL
	, [StatusId] NUMERIC NULL
	, [ExpensesDatetime] DATETIME NULL
	, [VoidReasonId] NUMERIC NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([ExpenseId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[ExpenseDetails] - End */
