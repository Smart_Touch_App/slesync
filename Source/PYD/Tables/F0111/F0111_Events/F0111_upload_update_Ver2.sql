
 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."F0111"
SET "WWDSS5" = {ml r."WWDSS5"}, "WWMLNM" = {ml r."WWMLNM"}, "WWATTL" = {ml r."WWATTL"}, "WWREM1" = {ml r."WWREM1"}, "WWSLNM" = {ml r."WWSLNM"}, "WWALPH" = {ml r."WWALPH"}, "WWDC" = {ml r."WWDC"}, "WWGNNM" = {ml r."WWGNNM"}, "WWMDNM" = {ml r."WWMDNM"}, "WWSRNM" = {ml r."WWSRNM"}, "WWTYC" = {ml r."WWTYC"}, "WWW001" = {ml r."WWW001"}, "WWW002" = {ml r."WWW002"}, "WWW003" = {ml r."WWW003"}, "WWW004" = {ml r."WWW004"}, "WWW005" = {ml r."WWW005"}, "WWW006" = {ml r."WWW006"}, "WWW007" = {ml r."WWW007"}, "WWW008" = {ml r."WWW008"}, "WWW009" = {ml r."WWW009"}, "WWW010" = {ml r."WWW010"}, "WWMLN1" = {ml r."WWMLN1"}, "WWALP1" = {ml r."WWALP1"}, "WWUSER" = {ml r."WWUSER"}, "WWPID" = {ml r."WWPID"}, "WWUPMJ" = {ml r."WWUPMJ"}, "WWJOBN" = {ml r."WWJOBN"}, "WWUPMT" = {ml r."WWUPMT"}, "WWNTYP" = {ml r."WWNTYP"}, "WWNICK" = {ml r."WWNICK"}, "WWGEND" = {ml r."WWGEND"}, "WWDDATE" = {ml r."WWDDATE"}, "WWDMON" = {ml r."WWDMON"}, "WWDYR" = {ml r."WWDYR"}, "WWWN001" = {ml r."WWWN001"}, "WWWN002" = {ml r."WWWN002"}, "WWWN003" = {ml r."WWWN003"}, "WWWN004" = {ml r."WWWN004"}, "WWWN005" = {ml r."WWWN005"}, "WWWN006" = {ml r."WWWN006"}, "WWWN007" = {ml r."WWWN007"}, "WWWN008" = {ml r."WWWN008"}, "WWWN009" = {ml r."WWWN009"}, "WWWN010" = {ml r."WWWN010"}, "WWFUCO" = {ml r."WWFUCO"}, "WWPCM" = {ml r."WWPCM"}, "WWPCF" = {ml r."WWPCF"}, "WWACTIN" = {ml r."WWACTIN"}, "WWCFRGUID" = {ml r."WWCFRGUID"}, "WWSYNCS" = {ml r."WWSYNCS"}, "WWCAAD" = {ml r."WWCAAD"}
WHERE "WWAN8" = {ml r."WWAN8"} AND "WWIDLN" = {ml r."WWIDLN"}
 

