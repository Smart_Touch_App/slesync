 
SELECT "dbo"."tblCoffeeProspect"."CoffeeProspectID"
,"dbo"."tblCoffeeProspect"."ProspectID"
,"dbo"."tblCoffeeProspect"."CoffeeBlendID"
,"dbo"."tblCoffeeProspect"."CompetitorID"
,"dbo"."tblCoffeeProspect"."UOMID"
,"dbo"."tblCoffeeProspect"."PackSize"
,"dbo"."tblCoffeeProspect"."CS_PK_LB"
,"dbo"."tblCoffeeProspect"."Price"
,"dbo"."tblCoffeeProspect"."UsageMeasurementID"
,"dbo"."tblCoffeeProspect"."LiqCoffeeTypeID"
,"dbo"."tblCoffeeProspect"."CreatedBy"
,"dbo"."tblCoffeeProspect"."CreatedDatetime"
,"dbo"."tblCoffeeProspect"."UpdatedBy"
,"dbo"."tblCoffeeProspect"."UpdatedDatetime"

FROM "dbo"."tblCoffeeProspect"
WHERE "dbo"."tblCoffeeProspect"."last_modified">= {ml s.last_table_download}
