 
SELECT "BUSDTA"."CheckDetails"."CheckDetailsId"
,"BUSDTA"."CheckDetails"."CheckDetailsNumber"
,"BUSDTA"."CheckDetails"."RouteId"
,"BUSDTA"."CheckDetails"."CheckNumber"
,"BUSDTA"."CheckDetails"."CustomerId"
,"BUSDTA"."CheckDetails"."CustomerName"
,"BUSDTA"."CheckDetails"."CheckAmount"
,"BUSDTA"."CheckDetails"."CheckDate"
,"BUSDTA"."CheckDetails"."CreatedBy"
,"BUSDTA"."CheckDetails"."CreatedDatetime"
,"BUSDTA"."CheckDetails"."UpdatedBy"
,"BUSDTA"."CheckDetails"."UpdatedDatetime"
FROM "BUSDTA"."CheckDetails"
WHERE "BUSDTA"."CheckDetails"."last_modified" >= {ml s.last_table_download}
AND "BUSDTA"."CheckDetails"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

