
------- /*[BUSDTA].[UDCKEYLIST]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."UDCKEYLIST"     (
	"DTSY" nvarchar(4) not null,
	"DTRT" nvarchar(2) not null,
	PRIMARY KEY ("DTSY", "DTRT")
)

------- /*[BUSDTA].[UDCKEYLIST]*/ - End