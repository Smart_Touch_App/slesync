 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Entity_Range_Master"
SET "EntityBucketMasterId" = {ml r."EntityBucketMasterId"}, "RangeStart" = {ml r."RangeStart"}, "RangeEnd" = {ml r."RangeEnd"}, "LastAlloted" = {ml r."LastAlloted"}, "Size" = {ml r."Size"}, "Source" = {ml r."Source"}, "Active" = {ml r."Active"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "EntityRangeMasterID" = {ml r."EntityRangeMasterID"}
 
