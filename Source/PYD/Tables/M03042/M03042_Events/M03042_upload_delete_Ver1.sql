/* Delete the row from the consolidated database. */
DELETE FROM "BUSDTA"."M03042"
WHERE "PDAN8" = {ml r."PDAN8"}
	AND "PDCO" = {ml r."PDCO"}
	AND "PDID" = {ml r."PDID"}