/* Update the row in the consolidated database. */

UPDATE "BUSDTA"."M03042"
SET "PDPAMT" = {ml r."PDPAMT"},
	"PDPMODE" = {ml r."PDPMODE"},
	"PDCHQNO" = {ml r."PDCHQNO"},
	"PDCHQDT" = {ml r."PDCHQDT"},
	"PDCRBY" = {ml r."PDCRBY"},
	"PDCRDT" = {ml r."PDCRDT"},
	"PDUPBY" = {ml r."PDUPBY"},
	"PDUPDT" = {ml r."PDUPDT"},
	"PDRCID" = {ml r."PDRCID"},
	"PDTRMD" = {ml r."PDTRMD"}
WHERE "PDAN8" = {ml r."PDAN8"}
	AND "PDCO" = {ml r."PDCO"}
	AND "PDID" = {ml r."PDID"}