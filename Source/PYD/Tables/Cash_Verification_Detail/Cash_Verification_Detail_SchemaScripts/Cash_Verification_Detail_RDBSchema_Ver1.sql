
/* [BUSDTA].[Cash_Verification_Detail] - begins */

/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - Start */
IF OBJECT_ID('[BUSDTA].[Cash_Verification_Detail]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Cash_Verification_Detail]
	(
	  [CashVerificationDetailId] NUMERIC NOT NULL
	, [SettlementDetailId] NUMERIC NOT NULL
	, [CashTypeId] NUMERIC NOT NULL
	, [Quantity] NUMERIC NULL
	, [Amount] NUMERIC NULL
	, [RouteId] NUMERIC NOT NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([CashVerificationDetailId] ASC, [RouteId] ASC)
	)
END
/* TableDDL - [BUSDTA].[Cash_Verification_Detail] - End */
