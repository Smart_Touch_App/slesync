 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."Vehicle_Master"
SET "VehicleNumber" = {ml r."VehicleNumber"}, "VehicleMake" = {ml r."VehicleMake"}, "VehicleColour" = {ml r."VehicleColour"}, "VehicleAxle" = {ml r."VehicleAxle"}, "VehicleFuel" = {ml r."VehicleFuel"}, "VehicleMilage" = {ml r."VehicleMilage"}, "Isactive" = {ml r."Isactive"}, "VehicleManufacturingDt" = {ml r."VehicleManufacturingDt"}, "VehicleExpiryDt" = {ml r."VehicleExpiryDt"}, "VehicleOwner" = {ml r."VehicleOwner"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "VehicleID" = {ml r."VehicleID"} AND "VINNumber" = {ml r."VINNumber"}
 
