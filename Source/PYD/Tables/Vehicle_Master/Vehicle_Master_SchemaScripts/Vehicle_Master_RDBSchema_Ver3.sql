
/* [BUSDTA].[Vehicle_Master] - begins */

/* TableDDL - [BUSDTA].[Vehicle_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Vehicle_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Vehicle_Master]
	(
	  [VehicleID] NUMERIC(8,0) NOT NULL
	, [VINNumber] NVARCHAR(20) NOT NULL
	, [VehicleNumber] decimal (15,0) NULL
	, [VehicleMake] NVARCHAR (30) NULL
	, [VehicleColour] NVARCHAR (20) NULL
	, [VehicleAxle] decimal (2,0) NULL
	, [VehicleFuel] NVARCHAR (10) NULL
	, [VehicleMilage] decimal (15,0) NULL
	, [Isactive] BIT NULL
	, [VehicleManufacturingDt] DATETIME NULL
	, [VehicleExpiryDt] DATE NULL
	, [VehicleOwner] NVARCHAR (25) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL
	, PRIMARY KEY ([VehicleID] ASC, [VINNumber] ASC)
	)
END
/* TableDDL - [BUSDTA].[Vehicle_Master] - End */
