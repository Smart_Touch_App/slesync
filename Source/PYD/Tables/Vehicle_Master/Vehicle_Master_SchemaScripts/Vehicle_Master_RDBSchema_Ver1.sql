/* [BUSDTA].[Vehicle_Master] - begins */

/* TableDDL - [BUSDTA].[Vehicle_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Vehicle_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Vehicle_Master]
	(
	  [VehicleID] NUMERIC NOT NULL
	, [VINNumber] NVARCHAR(20) NOT NULL
	, [VehicleNumber] NUMERIC NULL
	, [VehicleMake] NCHAR(30) NULL
	, [VehicleColour] NCHAR(20) NULL
	, [VehicleAxle] NUMERIC NULL
	, [VehicleFuel] NCHAR(10) NULL
	, [VehicleMilage] NUMERIC NULL
	, [Isactive] BIT NULL
	, [VehicleManufacturingDt] DATETIME NULL
	, [VehicleExpiryDt] DATE NULL
	, [VehicleOwner] NCHAR(25) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([VehicleID] ASC, [VINNumber] ASC)
	)
END
/* TableDDL - [BUSDTA].[Vehicle_Master] - End */
