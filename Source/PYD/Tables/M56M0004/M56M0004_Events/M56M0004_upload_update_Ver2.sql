 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."M56M0004"
SET "RPROUT" = {ml r."RPROUT"}, "RPSTDT" = {ml r."RPSTDT"}, "RPOGDT" = {ml r."RPOGDT"}, "RPSN" = {ml r."RPSN"}, "RPVTTP" = {ml r."RPVTTP"}, "RPSTTP" = {ml r."RPSTTP"}, "RPRSTID" = {ml r."RPRSTID"}, "RPISRSN" = {ml r."RPISRSN"}, "RPACTID" = {ml r."RPACTID"}, "RPRCID" = {ml r."RPRCID"}, "RPCACT" = {ml r."RPCACT"}, "RPPACT" = {ml r."RPPACT"}, "RPCRBY" = {ml r."RPCRBY"}, "RPCRDT" = {ml r."RPCRDT"}, "RPUPBY" = {ml r."RPUPBY"}, "RPUPDT" = {ml r."RPUPDT"}
WHERE "RPAN8" = {ml r."RPAN8"} AND "RPSTID" = {ml r."RPSTID"}
 
