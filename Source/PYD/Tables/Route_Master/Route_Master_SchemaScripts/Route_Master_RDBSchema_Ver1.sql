/* [BUSDTA].[Route_Master] - begins */

/* TableDDL - [BUSDTA].[Route_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Master]
	(
	  [RouteMasterID] NUMERIC NOT NULL
	, [RouteName] VARCHAR(10) NOT NULL
	, [RouteDescription] NCHAR(50) NULL
	, [RouteAdressBookNumber] NUMERIC NOT NULL
	, [BranchNumber] NCHAR(12) NULL
	, [BranchAdressBookNumber] NUMERIC NULL
	, [DefaultUser] NUMERIC NULL
	, [VehicleID] NUMERIC NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([RouteMasterID] ASC, [RouteName] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Master] - End */
