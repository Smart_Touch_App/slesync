/* [BUSDTA].[Route_Master] - begins */

/* TableDDL - [BUSDTA].[Route_Master] - Start */
IF OBJECT_ID('[BUSDTA].[Route_Master]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[Route_Master]
	(
	  [RouteMasterID] NUMERIC(8,0) NOT NULL
	, [RouteName] VARCHAR(10) NOT NULL
	, [RouteDescription] NVARCHAR (50) NULL
	, [RouteAdressBookNumber] NUMERIC(8,0) NOT NULL
	, [BranchNumber] NVARCHAR (12) NULL
	, [BranchAdressBookNumber] NUMERIC(8,0) NULL
	, [DefaultUser] NUMERIC(8,0) NULL
	, [VehicleID] NUMERIC(8,0) NULL
	, [CreatedBy] NUMERIC(8,0) NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC(8,0) NULL
	, [UpdatedDatetime] DATETIME NULL

	, PRIMARY KEY ([RouteMasterID] ASC, [RouteName] ASC)
	)
END
/* TableDDL - [BUSDTA].[Route_Master] - End */
