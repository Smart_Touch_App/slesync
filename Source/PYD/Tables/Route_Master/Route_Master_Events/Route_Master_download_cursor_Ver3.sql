/* 
 
SELECT "BUSDTA"."Route_Master"."RouteMasterID"
,"BUSDTA"."Route_Master"."RouteName"
,"BUSDTA"."Route_Master"."RouteDescription"
,"BUSDTA"."Route_Master"."RouteAdressBookNumber"
,"BUSDTA"."Route_Master"."BranchNumber"
,"BUSDTA"."Route_Master"."BranchAdressBookNumber"
,"BUSDTA"."Route_Master"."DefaultUser"
,"BUSDTA"."Route_Master"."VehicleID"
,"BUSDTA"."Route_Master"."CreatedBy"
,"BUSDTA"."Route_Master"."CreatedDatetime"
,"BUSDTA"."Route_Master"."UpdatedBy"
,"BUSDTA"."Route_Master"."UpdatedDatetime"
,"BUSDTA"."Route_Master"."RouteStatusID"
,"BUSDTA"."Route_Master"."RepnlBranch"
,"BUSDTA"."Route_Master"."RepnlBranchType"

FROM "BUSDTA"."Route_Master"
WHERE "BUSDTA"."Route_Master"."last_modified">= {ml s.last_table_download}
 
*/
