
/* [BUSDTA].[M03011] - begins */

/* TableDDL - [BUSDTA].[M03011] - Start */
IF OBJECT_ID('[BUSDTA].[M03011]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03011]
	(
	  [CSAN8] NUMERIC(8,0) NOT NULL
	, [CSCO] VARCHAR(5) NOT NULL
	, [CSUAMT] NUMERIC(8,4) NULL
	, [CSOBAL] NUMERIC(8,4) NULL
	, [CSCRBY] NCHAR(10) NULL
	, [CSCRDT] DATE NULL
	, [CSUPBY] NCHAR(10) NULL
	, [CSUPDT] DATE NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_M03011_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_M03011] PRIMARY KEY ([CSAN8] ASC, [CSCO] ASC)
	)

END
GO
/* TableDDL - [BUSDTA].[M03011] - End */

/* SHADOW TABLE FOR [BUSDTA].[M03011] - Start */
IF OBJECT_ID('[BUSDTA].[M03011_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[M03011_del]
	(
	  [CSAN8] NUMERIC(8,0)
	, [CSCO] VARCHAR(5)
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([CSAN8] ASC, [CSCO] ASC)
	)

END
Go
/* SHADOW TABLE FOR [BUSDTA].[M03011] - End */
/* TRIGGERS FOR M03011 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.M03011_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.M03011_ins
	ON BUSDTA.M03011 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.M03011_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.M03011_del.CSAN8= inserted.CSAN8 AND BUSDTA.M03011_del.CSCO= inserted.CSCO
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.M03011_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03011_upd
	ON BUSDTA.M03011 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.M03011
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.M03011.CSAN8= inserted.CSAN8 AND BUSDTA.M03011.CSCO= inserted.CSCO');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.M03011_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.M03011_dlt
	ON BUSDTA.M03011 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.M03011_del (CSAN8, CSCO, last_modified )
	SELECT deleted.CSAN8, deleted.CSCO, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR M03011 - END */

