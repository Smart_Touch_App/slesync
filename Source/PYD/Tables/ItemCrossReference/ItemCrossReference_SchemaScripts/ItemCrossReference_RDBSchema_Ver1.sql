/* [BUSDTA].[ItemCrossReference] - begins */

/* TableDDL - [BUSDTA].[ItemCrossReference] - Start */
IF OBJECT_ID('[BUSDTA].[ItemCrossReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemCrossReference]
	(
	  [ItemReferenceID] NUMERIC NOT NULL
	, [CrossReferenceID] NUMERIC NOT NULL
	, [MobileType] NCHAR(10) NULL
	, [EffectiveFrom] DATE NULL
	, [EffectiveThru] DATE NULL
	, [CrossData] NCHAR(50) NULL
	, [AddressNumber] NUMERIC NULL
	, [CrossReferenceType] NCHAR(50) NULL
	, [ItemNumber] NUMERIC NULL
	, [ItemRevisionLevel] NCHAR(50) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	
	, PRIMARY KEY ([ItemReferenceID] ASC, [CrossReferenceID] ASC)
	)
END
/* TableDDL - [BUSDTA].[ItemCrossReference] - End */
