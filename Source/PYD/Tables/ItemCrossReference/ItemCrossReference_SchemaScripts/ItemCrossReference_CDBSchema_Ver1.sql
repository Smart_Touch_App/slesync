/* [BUSDTA].[ItemCrossReference] - begins */

/* TableDDL - [BUSDTA].[ItemCrossReference] - Start */
IF OBJECT_ID('[BUSDTA].[ItemCrossReference]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemCrossReference]
	(
	  [ItemReferenceID] NUMERIC NOT NULL
	, [CrossReferenceID] NUMERIC NOT NULL
	, [MobileType] NCHAR(10) NULL
	, [EffectiveFrom] DATE NULL
	, [EffectiveThru] DATE NULL
	, [CrossData] NCHAR(50) NULL
	, [AddressNumber] NUMERIC NULL
	, [CrossReferenceType] NCHAR(50) NULL
	, [ItemNumber] NUMERIC NULL
	, [ItemRevisionLevel] NCHAR(50) NULL
	, [CreatedBy] NUMERIC NULL
	, [CreatedDatetime] DATETIME NULL
	, [UpdatedBy] NUMERIC NULL
	, [UpdatedDatetime] DATETIME NULL
	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_ITEMCROSSREFERENCE_last_modified DEFAULT(getdate())
	, CONSTRAINT [PK_ItemCrossReference] PRIMARY KEY ([ItemReferenceID] ASC, [CrossReferenceID] ASC)
	)

END
/* TableDDL - [BUSDTA].[ItemCrossReference] - End */

/* SHADOW TABLE FOR [BUSDTA].[ItemCrossReference] - Start */
IF OBJECT_ID('[BUSDTA].[ItemCrossReference_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[ItemCrossReference_del]
	(
	  [ItemReferenceID] NUMERIC
	, [CrossReferenceID] NUMERIC
	, last_modified DATETIME DEFAULT GETDATE()
	, PRIMARY KEY ([ItemReferenceID] ASC, [CrossReferenceID] ASC)
	)

END
/* SHADOW TABLE FOR [BUSDTA].[ItemCrossReference] - End */
/* TRIGGERS FOR ItemCrossReference - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.ItemCrossReference_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.ItemCrossReference_ins
	ON BUSDTA.ItemCrossReference AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.ItemCrossReference_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.ItemCrossReference_del.CrossReferenceID= inserted.CrossReferenceID AND BUSDTA.ItemCrossReference_del.ItemReferenceID= inserted.ItemReferenceID
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.ItemCrossReference_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemCrossReference_upd
	ON BUSDTA.ItemCrossReference AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.ItemCrossReference
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.ItemCrossReference.CrossReferenceID= inserted.CrossReferenceID AND BUSDTA.ItemCrossReference.ItemReferenceID= inserted.ItemReferenceID');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.ItemCrossReference_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.ItemCrossReference_dlt
	ON BUSDTA.ItemCrossReference AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.ItemCrossReference_del (CrossReferenceID, ItemReferenceID, last_modified )
	SELECT deleted.CrossReferenceID, deleted.ItemReferenceID, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR ItemCrossReference - END */
