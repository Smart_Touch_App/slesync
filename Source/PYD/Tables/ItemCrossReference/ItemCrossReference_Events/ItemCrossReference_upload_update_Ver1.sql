 
/* Update the row in the consolidated database. */
UPDATE "BUSDTA"."ItemCrossReference"
SET "MobileType" = {ml r."MobileType"}, "EffectiveFrom" = {ml r."EffectiveFrom"}, "EffectiveThru" = {ml r."EffectiveThru"}, "CrossData" = {ml r."CrossData"}, "AddressNumber" = {ml r."AddressNumber"}, "CrossReferenceType" = {ml r."CrossReferenceType"}, "ItemNumber" = {ml r."ItemNumber"}, "ItemRevisionLevel" = {ml r."ItemRevisionLevel"}, "CreatedBy" = {ml r."CreatedBy"}, "CreatedDatetime" = {ml r."CreatedDatetime"}, "UpdatedBy" = {ml r."UpdatedBy"}, "UpdatedDatetime" = {ml r."UpdatedDatetime"}
WHERE "CrossReferenceID" = {ml r."CrossReferenceID"} AND "ItemReferenceID" = {ml r."ItemReferenceID"}
 
