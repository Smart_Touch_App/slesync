
/* [BUSDTA].[F4092] - begins */

/* TableDDL - [BUSDTA].[F4092] - Start */
IF OBJECT_ID('[BUSDTA].[F4092]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4092]	(	  [GPGPTY] NCHAR(1) NOT NULL	, [GPGPC] NCHAR(8) NOT NULL	, [GPDL01] NCHAR(30) NULL	, [GPGPK1] NCHAR(10) NULL	, [GPGPK2] NCHAR(10) NULL	, [GPGPK3] NCHAR(10) NULL	, [GPGPK4] NCHAR(10) NULL	, [GPGPK5] NCHAR(10) NULL	, [GPGPK6] NCHAR(10) NULL	, [GPGPK7] NCHAR(10) NULL	, [GPGPK8] NCHAR(10) NULL	, [GPGPK9] NCHAR(10) NULL	, [GPGPK10] NCHAR(10) NULL	, [last_modified] DATETIME NOT NULL CONSTRAINT DF_F4092_last_modified DEFAULT(getdate())	, CONSTRAINT [PK_F4092] PRIMARY KEY ([GPGPTY] ASC, [GPGPC] ASC)	)
END
/* TableDDL - [BUSDTA].[F4092] - End */

/* SHADOW TABLE FOR [BUSDTA].[F4092] - Start */
IF OBJECT_ID('[BUSDTA].[F4092_del]') IS NULL
BEGIN

	CREATE TABLE [BUSDTA].[F4092_del]	(	  [GPGPTY] NCHAR(1)	, [GPGPC] NCHAR(8)	, last_modified DATETIME DEFAULT GETDATE()	, PRIMARY KEY ([GPGPTY] ASC, [GPGPC] ASC)	)
END
/* SHADOW TABLE FOR [BUSDTA].[F4092] - End */
/* TRIGGERS FOR F4092 - START */
/* Create the shadow insert trigger */
IF OBJECT_ID('BUSDTA.F4092_ins') IS NULL
BEGIN
	EXEC('
	CREATE TRIGGER BUSDTA.F4092_ins
	ON BUSDTA.F4092 AFTER INSERT
	AS
	/*
	* Delete the row from the shadow delete table.  (This trigger is only needed if deleted
	* primary keys can be re-inserted.)
	*/
	DELETE FROM BUSDTA.F4092_del
		WHERE EXISTS ( SELECT 1
			FROM inserted
			WHERE BUSDTA.F4092_del.GPGPC= inserted.GPGPC AND BUSDTA.F4092_del.GPGPTY= inserted.GPGPTY
		);
	')
END
GO
 
/* Create the shadow update trigger. */
IF OBJECT_ID('BUSDTA.F4092_upd') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4092_upd
	ON BUSDTA.F4092 AFTER UPDATE
AS
/* Update the column last_modified in modified row. */
UPDATE BUSDTA.F4092
	SET last_modified = GETDATE()
	FROM inserted
		WHERE BUSDTA.F4092.GPGPC= inserted.GPGPC AND BUSDTA.F4092.GPGPTY= inserted.GPGPTY');
END
GO
 
/* Create the shadow delete trigger */
IF OBJECT_ID('BUSDTA.F4092_dlt') IS NULL
BEGIN
	EXEC('
CREATE TRIGGER BUSDTA.F4092_dlt
	ON BUSDTA.F4092 AFTER DELETE
AS
/* Insert the row into the shadow delete table. */
INSERT INTO BUSDTA.F4092_del (GPGPC, GPGPTY, last_modified )
	SELECT deleted.GPGPC, deleted.GPGPTY, GETDATE()
	FROM deleted;
	');
END
GO
/* TRIGGERS FOR F4092 - END */
