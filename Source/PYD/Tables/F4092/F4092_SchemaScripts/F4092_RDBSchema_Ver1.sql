------- /*[BUSDTA].[F4092]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."F4092"     (
	"GPGPTY" nvarchar(1) not null,
	"GPGPC" nvarchar(8) not null,
	"GPDL01" nvarchar(30) null,
	"GPGPK1" nvarchar(10) null,
	"GPGPK2" nvarchar(10) null,
	"GPGPK3" nvarchar(10) null,
	"GPGPK4" nvarchar(10) null,
	"GPGPK5" nvarchar(10) null,
	"GPGPK6" nvarchar(10) null,
	"GPGPK7" nvarchar(10) null,
	"GPGPK8" nvarchar(10) null,
	"GPGPK9" nvarchar(10) null,
	"GPGPK10" nvarchar(10) null,
	PRIMARY KEY ("GPGPTY", "GPGPC")
)

------- /*[BUSDTA].[F4092]*/ - End