------- /*[BUSDTA].[M50052]*/ - Start

CREATE TABLE IF NOT EXISTS "BUSDTA"."M50052" (	-- TransactionSyncDetail
    "SDID"                      numeric(8,0) NOT NULL  DEFAULT autoincrement	-- SyncID
   ,"SDTXID"                    numeric(8,0) NOT NULL	-- TransactionID
   ,"SDKEY"                     nvarchar(15) NULL		-- TransactionKey
   ,"SDISYNCD"                  bit NOT NULL			-- Synced
   ,"SDTMSTMP"                  datetime NULL			-- SyncTimestamp
   ,SDCRBY						nvarchar(50)
   ,SDCRDT						datetime
   ,SDUPBY						nvarchar(50)
   ,SDUPDT						datetime
   ,PRIMARY KEY ("SDID" ASC,"SDTXID" ASC) 
)

------- /*[BUSDTA].[M50052]*/ - End