 
/* Date Modified: 11-01-2016 */

SELECT "tblAlliedProspect"."AlliedProspectID"
,"tblAlliedProspect"."ProspectID"
,"tblAlliedProspect"."CompetitorID"
,"tblAlliedProspect"."CategoryID"
,"tblAlliedProspect"."SubCategoryID"
,"tblAlliedProspect"."BrandID"
,"tblAlliedProspect"."UOMID"
,"tblAlliedProspect"."PackSize"
,"tblAlliedProspect"."CS_PK_LB"
,"tblAlliedProspect"."Price"
,"tblAlliedProspect"."UsageMeasurementID"
,"tblAlliedProspect"."CreatedBy"
,"tblAlliedProspect"."CreatedDatetime"
,"tblAlliedProspect"."UpdatedBy"
,"tblAlliedProspect"."UpdatedDatetime"

FROM "dbo"."tblAlliedProspect"
INNER JOIN BUSDTA.Prospect_Master ON "tblAlliedProspect".ProspectID=BUSDTA.Prospect_Master.ProspectId 
WHERE "dbo"."tblAlliedProspect"."last_modified">= {ml s.last_table_download}
and "BUSDTA"."Prospect_Master"."RouteId" = (select RouteMasterId from BUSDTA.route_master where RouteName = {ml s.username})

