
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vw_CTCurrentVersion]'))
DROP VIEW [dbo].[vw_CTCurrentVersion]
GO
CREATE VIEW [dbo].[vw_CTCurrentVersion] 
AS
SELECT CHANGE_TRACKING_CURRENT_VERSION() CTCV
GO


