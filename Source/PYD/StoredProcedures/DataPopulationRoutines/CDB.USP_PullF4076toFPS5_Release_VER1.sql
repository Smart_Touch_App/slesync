


CREATE PROCEDURE [dbo].[USP_PullF4076toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F4076'

	DECLARE @TEMP TABLE(Act varchar(6),FMFRMN numeric(18,0))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F4076)
	BEGIN
	
		INSERT INTO BUSDTA.F4076
		(FMFRMN, FMFML, FMAPRS, FMUPMJ, FMTDAY)
		SELECT FMFRMN, FMFML, FMAPRS, FMUPMJ, FMTDAY
		FROM [JDEData_CRP_SQDR_Model].dbo.F4076
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4076')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4076'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4076',@synchronization_version,GETDATE())		
		
	END
	ELSE

	--Check the table data whether which has been refreshed
	IF EXISTS  (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F4076')
	BEGIN
		TRUNCATE TABLE BUSDTA.F4076
		TRUNCATE TABLE BUSDTA.F4076_del
				
		INSERT INTO BUSDTA.F4076
		(FMFRMN, FMFML, FMAPRS, FMUPMJ, FMTDAY)
		SELECT FMFRMN, FMFML, FMAPRS, FMUPMJ, FMTDAY
		FROM [JDEData_CRP_SQDR_Model].dbo.F4076 
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4076')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4076'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4076',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		
		--INSERT INTO [BUSDTA].[F4076_shadow]([FMFRMN],[FMFML],[FMAPRS],[FMUPMJ],[FMTDAY],[Operation])
		--SELECT CT.FMFRMN,F00.FMFML,F00.FMAPRS,CT.FMUPMJ,CT.FMTDAY,CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4076,@Last_Sync_Version) CT
		--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4076 AS F00 
		--ON CT.FMFRMN = F00.FMFRMN AND CT.FMUPMJ = F00.FMUPMJ AND CT.FMTDAY = F00.FMTDAY

		/***********Check the changes for replicatoin****************/
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES JDEData_CRP_SQDR_Model.dbo.F4076, @Last_Sync_Version)AS CT)
		BEGIN

		WITH CTE AS
		(
		SELECT CT.FMFRMN,F00.FMFML,F00.FMAPRS,CT.FMUPMJ,CT.FMTDAY,CT.SYS_CHANGE_OPERATION AS Operation
		FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4076,@Last_Sync_Version) CT
		LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4076 AS F00 
		ON CT.FMFRMN = F00.FMFRMN AND CT.FMUPMJ = F00.FMUPMJ AND CT.FMTDAY = F00.FMTDAY
		)

			MERGE BUSDTA.F4076 AS TARGET
			USING CTE AS SOURCE 
			ON (TARGET.FMFRMN = SOURCE.FMFRMN ) AND (TARGET.FMUPMJ = SOURCE.FMUPMJ)  
			AND (TARGET.FMTDAY = SOURCE.FMTDAY)

			WHEN MATCHED AND SOURCE.Operation='U' THEN 
			UPDATE 
			SET TARGET.[FMFML] = SOURCE.FMFML
			,TARGET.[FMAPRS] = SOURCE.FMAPRS
			
			WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
			INSERT ([FMFRMN],[FMFML],[FMAPRS],[FMUPMJ],[FMTDAY])
			VALUES (SOURCE.FMFRMN,SOURCE.FMFML,SOURCE.FMAPRS,SOURCE.FMUPMJ,SOURCE.FMTDAY)

			WHEN MATCHED AND SOURCE.Operation='D' THEN 
			DELETE

			OUTPUT $action,
			INSERTED.FMFRMN AS SourceFMFRMN INTO @Temp (Act,FMFRMN);
			SELECT @@ROWCOUNT AS F4076;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4076')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4076'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F4076',@synchronization_version)
		
		END
		--TRUNCATE TABLE [BUSDTA].[F4076_shadow]	
END

END










GO


