
CREATE PROCEDURE [dbo].[USP_PullF4092toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F4092'

	DECLARE @TEMP TABLE(Act varchar(6),GPGPTY nchar(100))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F4092)
	BEGIN
	
		INSERT INTO BUSDTA.F4092
		(GPGPTY, GPGPC, GPDL01, GPGPK1, GPGPK2, GPGPK3, GPGPK4, GPGPK5, GPGPK6, GPGPK7, GPGPK8, GPGPK9, GPGPK10)
		SELECT GPGPTY, GPGPC, GPDL01, GPGPK1, GPGPK2, GPGPK3, GPGPK4, GPGPK5, GPGPK6, GPGPK7, GPGPK8, GPGPK9, GPGPK10 
		FROM  [JDEData_CRP_SQDR_Model].dbo.F4092 
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4092')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4092'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4092',@synchronization_version,GETDATE())		
		
	END
	ELSE

	--Check the table data whether which has been refreshed
	IF EXISTS  (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F4092')
	BEGIN
		TRUNCATE TABLE BUSDTA.F4092
		TRUNCATE TABLE BUSDTA.F4092_del
				
		INSERT INTO BUSDTA.F4092
		(GPGPTY, GPGPC, GPDL01, GPGPK1, GPGPK2, GPGPK3, GPGPK4, GPGPK5, GPGPK6, GPGPK7, GPGPK8, GPGPK9, GPGPK10)
		SELECT GPGPTY, GPGPC, GPDL01, GPGPK1, GPGPK2, GPGPK3, GPGPK4, GPGPK5, GPGPK6, GPGPK7, GPGPK8, GPGPK9, GPGPK10 
		FROM  [JDEData_CRP_SQDR_Model].dbo.F4092

		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4092')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4092'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F4092',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
	
		--INSERT INTO [BUSDTA].[F4092_shadow] ([GPGPTY],[GPGPC],[GPDL01],[GPGPK1],[GPGPK2],[GPGPK3],[GPGPK4],[GPGPK5],[GPGPK6],[GPGPK7]
		--,[GPGPK8],[GPGPK9],[GPGPK10],[Operation])
		--SELECT CT.[GPGPTY],CT.[GPGPC],F00.[GPDL01],F00.[GPGPK1],F00.[GPGPK2],F00.[GPGPK3],F00.[GPGPK4],F00.[GPGPK5],F00.[GPGPK6],F00.[GPGPK7],F00.[GPGPK8]
		--,F00.[GPGPK9],F00.[GPGPK10],CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4092, @Last_Sync_Version) CT
		--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4092 AS F00 ON CT.GPGPTY = F00.GPGPTY AND CT.GPGPC = F00.GPGPC
		
		/***********Check the changes for replicatoin****************/
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES JDEData_CRP_SQDR_Model.dbo.F4092, @Last_Sync_Version)AS CT)
		BEGIN

			WITH CTE AS
			(
			SELECT CT.[GPGPTY],CT.[GPGPC],F00.[GPDL01],F00.[GPGPK1],F00.[GPGPK2],F00.[GPGPK3],F00.[GPGPK4],F00.[GPGPK5],F00.[GPGPK6],F00.[GPGPK7],F00.[GPGPK8]
			,F00.[GPGPK9],F00.[GPGPK10],CT.SYS_CHANGE_OPERATION AS Operation
			FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F4092, @Last_Sync_Version) CT
			LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F4092 AS F00 ON CT.GPGPTY = F00.GPGPTY AND CT.GPGPC = F00.GPGPC
			)

			MERGE BUSDTA.F4092 AS TARGET
			USING CTE AS SOURCE 
			ON (TARGET.GPGPTY = SOURCE.GPGPTY ) AND (TARGET.GPGPC = SOURCE.GPGPC)  

			WHEN MATCHED AND SOURCE.Operation='U' THEN 
			UPDATE 
			SET TARGET.[GPDL01] = SOURCE.GPDL01
					,TARGET.[GPGPK1] = SOURCE.GPGPK1
					,TARGET.[GPGPK2] = SOURCE.GPGPK2
					,TARGET.[GPGPK3] = SOURCE.GPGPK3
					,TARGET.[GPGPK4] = SOURCE.GPGPK4
					,TARGET.[GPGPK5] = SOURCE.GPGPK5
					,TARGET.[GPGPK6] = SOURCE.GPGPK6
					,TARGET.[GPGPK7] = SOURCE.GPGPK7
					,TARGET.[GPGPK8] = SOURCE.GPGPK8
					,TARGET.[GPGPK9] = SOURCE.GPGPK9
					,TARGET.[GPGPK10] = SOURCE.GPGPK10

			WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
			INSERT ([GPGPTY],[GPGPC],[GPDL01],[GPGPK1],[GPGPK2],[GPGPK3],[GPGPK4],[GPGPK5],[GPGPK6],[GPGPK7]
			,[GPGPK8],[GPGPK9],[GPGPK10])
			VALUES (SOURCE.GPGPTY,SOURCE.GPGPC,SOURCE.GPDL01,SOURCE.GPGPK1,SOURCE.GPGPK2,SOURCE.GPGPK3,SOURCE.GPGPK4,SOURCE.GPGPK5,SOURCE.GPGPK6,SOURCE.GPGPK7
			,SOURCE.GPGPK8,SOURCE.GPGPK9,SOURCE.GPGPK10)

			WHEN MATCHED AND SOURCE.Operation='D' THEN DELETE
			OUTPUT $action,
			INSERTED.GPGPTY AS SourceGPGPTY INTO @Temp (Act,GPGPTY);
			SELECT @@ROWCOUNT AS F4092;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F4092')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F4092'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F4092',@synchronization_version)
		
		END
		--TRUNCATE TABLE [BUSDTA].[F4092_shadow]	
END

END









GO


