
CREATE PROCEDURE [dbo].[USP_PullF0014toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F0014'

	DECLARE @TEMP TABLE(Act varchar(6),PNPTC nchar(25))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F0014)
	BEGIN
	
		INSERT INTO BUSDTA.F0014
		(PNPTC, PNPTD, PNDCP, PNDCD, PNNDTP, PNNSP, PNDTPA, PNPXDM, PNPXDD)
		SELECT  PNPTC, PNPTD, PNDCP, PNDCD, PNNDTP, PNNSP, PNDTPA, PNPXDM, PNPXDD
		FROM [JDEData_CRP_SQDR_Model].dbo.F0014
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F0014')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F0014'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F0014',@synchronization_version,GETDATE())		
		
	END
	ELSE

	
	--Check the F0014 data whether which has been refreshed
	IF EXISTS  (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F0014')
	BEGIN
		TRUNCATE TABLE BUSDTA.F0014
		TRUNCATE TABLE BUSDTA.F0014_del
		
		INSERT INTO BUSDTA.F0014
		(PNPTC, PNPTD, PNDCP, PNDCD, PNNDTP, PNNSP, PNDTPA, PNPXDM, PNPXDD)
		SELECT  PNPTC, PNPTD, PNDCP, PNDCD, PNNDTP, PNNSP, PNDTPA, PNPXDM, PNPXDD
		FROM [JDEData_CRP_SQDR_Model].dbo.F0014
						
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F0014')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F0014'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F0014',@synchronization_version,GETDATE())
	END
	ELSE
	--Starts to replicate data when DBMoto is working in mirroring mode
		BEGIN
		----SELECT  @TSQL = '
		--INSERT INTO [BUSDTA].[F0014_shadow](PNDCD,PNDCP,PNDTPA,PNNDTP,PNNSP,PNPTC,PNPTD,PNPXDD,PNPXDM,Operation)
		--SELECT PNDCD,PNDCP,PNDTPA,PNNDTP,PNNSP,CT.PNPTC,PNPTD,PNPXDD,PNPXDM,CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0014,@Last_Sync_Version) CT
		--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F0014 AS F0014 ON CT.PNPTC = F0014.PNPTC
		----'	EXEC (@TSQL)
		
		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0014, @Last_Sync_Version)AS CT)
		BEGIN TRY
		WITH CTE AS
		(
		SELECT PNDCD,PNDCP,PNDTPA,PNNDTP,PNNSP,CT.PNPTC,PNPTD,PNPXDD,PNPXDM,CT.SYS_CHANGE_OPERATION AS Operation
		FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F0014,@Last_Sync_Version) CT
		LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F0014 AS F0014 ON CT.PNPTC = F0014.PNPTC
		)

		MERGE BUSDTA.F0014 AS TARGET
		USING CTE AS SOURCE 
		ON (TARGET.PNPTC = SOURCE.PNPTC)

		WHEN MATCHED AND SOURCE.Operation='U' THEN 
		UPDATE 
		SET 
			TARGET.PNDCD = SOURCE.PNDCD,
			TARGET.PNDCP = SOURCE.PNDCP,
			TARGET.PNDTPA = SOURCE.PNDTPA,
			TARGET.PNNDTP = SOURCE.PNNDTP,
			TARGET.PNNSP = SOURCE.PNNSP,
			TARGET.PNPTD = SOURCE.PNPTD,
			TARGET.PNPXDD = SOURCE.PNPXDD,
			TARGET.PNPXDM = SOURCE.PNPXDM
	
		WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
		INSERT (PNDCD,PNDCP,PNDTPA,PNNDTP,PNNSP,PNPTC,PNPTD,PNPXDD,PNPXDM)
		VALUES (SOURCE.PNDCD,SOURCE.PNDCP,SOURCE.PNDTPA,SOURCE.PNNDTP,SOURCE.PNNSP,SOURCE.PNPTC,SOURCE.PNPTD,SOURCE.PNPXDD,SOURCE.PNPXDM)

		WHEN MATCHED AND SOURCE.Operation='D' THEN 
		DELETE

		OUTPUT $action,
		INSERTED.PNPTC AS SourcePNPTC INTO @TEMP (Act,PNPTC);
		SELECT @@ROWCOUNT AS F0014;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F0014')
			UPDATE [dbo].[tblSyncDetails] 
			SET LastSyncVersion = @synchronization_version 
			WHERE Objectname = 'DBO.F0014'
			ELSE
			INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F0014',@synchronization_version)	
		END TRY
		BEGIN CATCH
		--IF error occued rollback all trans
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F0014')
			UPDATE [dbo].[tblSyncDetails] 
			SET LastSyncVersion = @Last_Sync_Version 
			WHERE Objectname = 'DBO.F0014'
			ELSE
			INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) 
			VALUES('dbo.F0014',@Last_Sync_Version)

		END CATCH

	
END

END








GO

