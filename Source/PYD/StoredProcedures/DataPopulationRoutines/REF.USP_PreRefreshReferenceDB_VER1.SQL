
/*
CREATEDBY						CREATEDON		MODIFIEDBY			MODIFIEDON			COMMENTS
CHENNAI-DEV						26-08-2015		--					--					Refresh data on reference database
*/
--EXEC [dbo].[USP_PreRefreshReferenceDB] 'BUSDTA.F0014'

ALTER PROCEDURE [dbo].[USP_PreRefreshReferenceDB]
@Tablename varchar(100)= NULL
AS
BEGIN
		DECLARE @dynSQL varchar(MAX),@TSName VARCHAR(100)
		DECLARE @ICT INT = 1
		DECLARE @TCT INT = 0

		IF (@Tablename IS NULL)
		BEGIN

			IF OBJECT_ID('tempdb.dbo.#Temp') IS NOT NULL DROP TABLE #Temp
			CREATE TABLE #TEMP(SchemaName VARCHAR(50), TableName VARCHAR(50),SNo int )

			INSERT INTO #TEMP (SchemaName,TableName,SNo)
			SELECT SCHEMA_NAME(O.schema_id),O.name,ROW_NUMBER() over (ORDER BY O.OBJECT_ID) AS Number FROM sys.change_tracking_tables  T
			INNER JOIN SYS.OBJECTS O ON T.object_id = O.object_id

			SELECT @TCT = COUNT(SNo) FROM #TEMP

			WHILE (@ICT <= @TCT )
			BEGIN

				SELECT @TSName = '['+SchemaName+'].['+TableName+']' FROM #Temp WHERE SNo  = @ICT

				SET @dynSQL = 'ALTER TABLE '+@TSName +' DISABLE CHANGE_TRACKING;'
				PRINT @dynSQL
				--EXEC (@dynSQL)

				SET @dynSQL = 'TRUNCATE TABLE '+@TSName 
				PRINT @dynSQL
				--EXEC (@dynSQL)

				SET @dynSQL = 'ALTER TABLE '+@TSName +' ENABLE CHANGE_TRACKING;'
				PRINT @dynSQL
				--EXEC (@dynSQL)
				
				SET @dynSQL = 'IF EXISTS(SELECT 1 FROM dbo.tblRefreshDetails WHERE Objectname = '''+@TSName+''')
				UPDATE dbo.tblRefreshDetails
				SET TruncTimeAtREFDB = GETDATE()
				WHERE Objectname = '''+@TSName+'''
				ELSE
				INSERT INTO dbo.tblRefreshDetails (Objectname,TruncTimeAtREFDB) VALUES('''+@TSName+''',GETDATE())'
				PRINT @dynSQL
				--EXEC (@dynSQL)
				
				SET @ICT = @ICT +1
			END

		END 
		ELSE

		IF EXISTS (SELECT 1 FROM sys.change_tracking_tables  WHERE object_id =OBJECT_ID(@Tablename))
		BEGIN 
			
			SET @dynSQL = 'ALTER TABLE '+@Tablename +' DISABLE CHANGE_TRACKING;'
			--PRINT @dynSQL
			EXEC (@dynSQL)
			
			SET @dynSQL = 'TRUNCATE TABLE ' +@Tablename
			--PRINT @dynSQL
			EXEC (@dynSQL)
			
			SET @dynSQL = 'ALTER TABLE '+@Tablename +' ENABLE CHANGE_TRACKING;'
			--PRINT @dynSQL
			EXEC (@dynSQL)

			SET @dynSQL = 'IF EXISTS(SELECT 1 FROM dbo.tblRefreshDetails WHERE Objectname = '''+@Tablename+''')
			UPDATE dbo.tblRefreshDetails
			SET TruncTimeAtREFDB = GETDATE()
			WHERE Objectname = '''+@Tablename+'''
			ELSE
			INSERT INTO dbo.tblRefreshDetails (Objectname,TruncTimeAtREFDB) VALUES('''+@Tablename+''',GETDATE())'
			EXEC (@dynSQL)
		END
			
		ELSE
		BEGIN		
			SET @dynSQL = 'TRUNCATE TABLE ' +@Tablename
			--PRINT @dynSQL
			EXEC (@dynSQL)
			
			SET @dynSQL = 'ALTER TABLE '+@Tablename +' ENABLE CHANGE_TRACKING;'
			--PRINT @dynSQL
			EXEC (@dynSQL)
				
			SET @dynSQL = 'IF EXISTS(SELECT 1 FROM dbo.tblRefreshDetails WHERE Objectname = '''+@Tablename+''')
			UPDATE dbo.tblRefreshDetails
			SET TruncTimeAtREFDB = GETDATE()
			WHERE Objectname = '''+@Tablename+'''
			ELSE
			INSERT INTO dbo.tblRefreshDetails (Objectname,TruncTimeAtREFDB) VALUES('''+@Tablename+''',GETDATE())'
			EXEC (@dynSQL)
		END
	
END
