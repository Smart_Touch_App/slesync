
CREATE PROCEDURE [dbo].[USP_PullF41002toFPS5_Release]
AS
BEGIN
SET NOCOUNT ON
	DECLARE @Last_Sync_Version BIGINT = 0,@synchronization_version BIGINT,@TSQL VARCHAR(MAX)
	SELECT @Last_Sync_Version= ISNULL(LastSyncVersion,0) FROM [dbo].[tblSyncDetails] WHERE Objectname = 'DBO.F41002'

	DECLARE @TEMP TABLE(Act varchar(6),UMMCU nchar(50))
	
	--SET @synchronization_version = CHANGE_TRACKING_CURRENT_VERSION()
	SELECT @synchronization_version = CTCV FROM [dbo].[ReferenceSyncVersion]
	
	--Check the table for Initial data LOAD
	IF NOT EXISTS (SELECT TOP 1 1 FROM BUSDTA.F41002)
	BEGIN
	
		INSERT INTO BUSDTA.F41002
		(UMMCU, UMITM, UMUM, UMRUM, UMUSTR, UMCONV, UMCNV1)
		SELECT UMMCU, UMITM, UMUM, UMRUM, UMUSTR, UMCONV, UMCNV1 
		FROM  [JDEData_CRP_SQDR_Model].dbo.F41002
		
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F41002')

		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F41002'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F41002',@synchronization_version,GETDATE())		
		
	END
	ELSE

	--Check the table data whether which has been refreshed
	IF EXISTS  (SELECT 1 FROM [dbo].[ReferenceRefreshDetails] R LEFT JOIN [dbo].[tblSyncDetails] S ON S.Objectname = R.Objectname
				WHERE R.TruncTimeAtRefDB > ISNULL(S.TruncTimeAtCDB,0) AND R.Objectname = 'DBO.F41002')
	BEGIN
		TRUNCATE TABLE BUSDTA.F41002
		TRUNCATE TABLE BUSDTA.F41002_del
				
		INSERT INTO BUSDTA.F41002
		(UMMCU, UMITM, UMUM, UMRUM, UMUSTR, UMCONV, UMCNV1)
		SELECT UMMCU, UMITM, UMUM, UMRUM, UMUSTR, UMCONV, UMCNV1 
		FROM  [JDEData_CRP_SQDR_Model].dbo.F41002
	
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F41002')
					
		UPDATE dbo.tblSyncDetails
		SET 
		TruncTimeAtCDB = GETDATE(),		
		LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F41002'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion,TruncTimeAtCDB) VALUES('DBO.F41002',@synchronization_version,GETDATE())
	END
	ELSE
	BEGIN		
		
		
		--INSERT INTO BUSDTA.F41002_Shadow
		--(UMMCU,UMITM,UMUM,UMRUM,UMUSTR,UMCONV,UMCNV1,UMUSER,UMPID,UMJOBN,UMUPMJ,UMTDAY,UMEXPO,UMEXSO,UMPUPC,UMSEPC,Operation)
		--SELECT CT.UMMCU,CT.UMITM,CT.UMUM,CT.UMRUM,UMUSTR,UMCONV,UMCNV1,UMUSER,UMPID,UMJOBN,UMUPMJ,UMTDAY,UMEXPO,UMEXSO,UMPUPC,UMSEPC,CT.SYS_CHANGE_OPERATION
		--FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F41002, @Last_Sync_Version) CT
		--LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F41002 AS F41002 ON F41002.UMMCU = CT.UMMCU
		--AND F41002.UMITM = CT.UMITM AND F41002.UMUM = CT.UMUM AND F41002.UMRUM = CT.UMRUM

		/***********Check the changes for replicatoin****************/
		--IF EXISTS (SELECT TOP 1 1 FROM BUSDTA.F41002_shadow)
		--BEGIN

		IF EXISTS (SELECT TOP 1 1 FROM CHANGETABLE(CHANGES JDEData_CRP_SQDR_Model.dbo.F41002, @Last_Sync_Version)AS CT) 
		BEGIN

		WITH CTE AS
		(
		SELECT CT.UMMCU,CT.UMITM,CT.UMUM,CT.UMRUM,UMUSTR,UMCONV,UMCNV1,UMUSER,UMPID,UMJOBN,UMUPMJ,UMTDAY,UMEXPO,UMEXSO,UMPUPC,UMSEPC,CT.SYS_CHANGE_OPERATION AS Operation
		FROM CHANGETABLE(CHANGES [JDEData_CRP_SQDR_Model].dbo.F41002, @Last_Sync_Version) CT
		LEFT OUTER JOIN [JDEData_CRP_SQDR_Model].dbo.F41002 AS F41002 ON F41002.UMMCU = CT.UMMCU
		AND F41002.UMITM = CT.UMITM AND F41002.UMUM = CT.UMUM AND F41002.UMRUM = CT.UMRUM
		)

		MERGE BUSDTA.F41002 AS TARGET
		USING CTE AS SOURCE 
		ON (TARGET.UMMCU = SOURCE.UMMCU) AND (TARGET.UMITM = SOURCE.UMITM)
		AND (TARGET.UMUM = SOURCE.UMUM) AND (TARGET.UMRUM = SOURCE.UMRUM)
		
		WHEN MATCHED AND SOURCE.Operation='U' THEN 
		UPDATE 
		SET 
			TARGET.UMUSTR = SOURCE.UMUSTR,
			TARGET.UMCONV = SOURCE.UMCONV,
			TARGET.UMCNV1 = SOURCE.UMCNV1
	
		WHEN NOT MATCHED AND SOURCE.Operation='I' THEN 
		INSERT (UMMCU,UMITM,UMUM,UMRUM,UMUSTR,UMCONV,UMCNV1)		
		VALUES (SOURCE.UMMCU,SOURCE.UMITM,SOURCE.UMUM,SOURCE.UMRUM,SOURCE.UMUSTR,SOURCE.UMCONV,SOURCE.UMCNV1)

		WHEN MATCHED AND SOURCE.Operation='D' THEN 
		DELETE

		OUTPUT $action,
		INSERTED.UMMCU AS SourceUMMCU INTO @Temp (Act,UMMCU);
		SELECT @@ROWCOUNT AS F41002;
			
		IF EXISTS(select Objectname from [dbo].tblSyncDetails WHERE Objectname = 'DBO.F41002')
		
		UPDATE [dbo].[tblSyncDetails] 
		SET LastSyncVersion = @synchronization_version
		WHERE Objectname = 'DBO.F41002'
		ELSE
		INSERT INTO [dbo].tblSyncDetails(Objectname,LastSyncVersion) VALUES('dbo.F41002',@synchronization_version)
		
		END
		--TRUNCATE TABLE [BUSDTA].[F41002_shadow]	
END

END









GO


