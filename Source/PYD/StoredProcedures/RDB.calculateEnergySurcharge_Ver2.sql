CREATE OR REPLACE PROCEDURE "BUSDTA"."calculateEnergySurcharge" (CustomerNum varchar(10), OrderTotal numeric(10,4))
BEGIN
	DECLARE  FreightHandlingCode varchar(10);
	DECLARE  SpecialHandlingCode varchar(10);
	DECLARE  DescriptionCode varchar(10);
	DECLARE  HandlingCodeLength INTEGER ;

	Declare str_drsy varchar(10);
	Declare str_drrt varchar(10);
	--Declare UPto varchar(10);
  DECLARE upto numeric(10,4);   DECLARE surcharge numeric(10,4);
  DECLARE FinalSurcharge numeric(10,4); DECLARE PrevUpto numeric(10,4); DECLARE PrevSurcharge numeric(10,4);
	
 --DECLARE qry LONG VARCHAR;
 
     DECLARE cur_surcharge CURSOR FOR
        select  DRDL01,  DRDL02  from busdta.F0005 where DRSY = str_drsy and DRRT = str_drrt;
	set FinalSurcharge = 0;

    Select ltrim(rtrim(AIFRTH)) into FreightHandlingCode 
           from busdta.F03012 where aian8 = CustomerNum;
	IF FreightHandlingCode = '' or FreightHandlingCode is null THEN
		select 0 from dummy;
	END IF;

	select  ltrim(rtrim(DRDL02)), ltrim(rtrim(DRSPHD)) into DescriptionCode, SpecialHandlingCode 
            from busdta.F0005 where DRSY = '42' and DRRT = 'FR' and DRKY like '%'+FreightHandlingCode+'';
	-- Handle for Static Code 
	IF DescriptionCode = 'MB|STATIC' THEN
		set FinalSurcharge =  (select SpecialHandlingCode from dummy);
    -- Logic for UDC values
	ELSEIF DescriptionCode = 'MB|UDC' THEN
		set str_drsy = substring(SpecialHandlingCode, 1, charindex('|',SpecialHandlingCode)-1);
		set str_drrt = substring(SpecialHandlingCode,  charindex('|',SpecialHandlingCode)+1);
        set PrevUpto = 0; set FinalSurcharge = 0;

          set PrevSurcharge = (select CONVERT(numeric(10,4), max(DRDL02)) from busdta.F0005 where DRSY = str_drsy and DRRT = str_drrt);

          OPEN cur_surcharge;
          lp: LOOP
            FETCH NEXT cur_surcharge INTO upto, surcharge;
            IF SQLCODE <> 0 THEN LEAVE lp END IF;
            IF (OrderTotal > PrevUpto and OrderTotal <= upto) OR OrderTotal = 0  THEN
                --PRINT 'Surcharge '+CONVERT( nvarchar, PrevSurcharge );
                set FinalSurcharge = PrevSurcharge;
                LEAVE lp;
            END IF;
            set PrevUpto = upto;
            set Prevsurcharge = surcharge;
          END LOOP;
          CLOSE cur_surcharge;
        IF FinalSurcharge = 0 THEN
            set FinalSurcharge = (select CONVERT(numeric(10,4), min(DRDL02)) from busdta.F0005 where DRSY = str_drsy and DRRT = str_drrt);
        END IF;
    -- No surcharge if description is 0.
	ELSEIF DescriptionCode like '' THEN
		set FinalSurcharge =  0;
    -- This is additional check 
	ELSE
		if OrderTotal <= 100 then
		  select 45 from dummy
		elseif OrderTotal > 100 and OrderTotal <= 250 then
		  select 30 from dummy
		else
		  select 0 from dummy
		end if
	END IF;
select FinalSurcharge from dummy;
END
GO