
CREATE PROCEDURE [dbo].[SaveReserveLog]
(
	@Appuserid INT,
	@RouteId VARCHAR(10)
)
AS
BEGIN
SET NOCOUNT ON;
	INSERT INTO BUSDTA.Reserve_RouteUser(App_user_id, Route_Id) 
	VALUES (@Appuserid,@RouteId)
SET NOCOUNT OFF;
END

GO


