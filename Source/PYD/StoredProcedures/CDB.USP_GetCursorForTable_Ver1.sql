
CREATE PROCEDURE [dbo].[USP_GetCursorForTable]
(
	@version NVARCHAR(100),
	@table NVARCHAR(100)
)
AS
BEGIN
SET NOCOUNT ON
Print '/* ' + @version + ' ' + @table + ' - begins */'

--DECLARE @version NVARCHAR(100)='VER1'
--DECLARE @table NVARCHAR(100)='tblCountries'

/* Download Cursor */
DECLARE @selectStmt NVARCHAR(MAX)
DECLARE @dc NVARCHAR(MAX)
SET @selectStmt = NULL
--SELECT @selectStmt = COALESCE(@selectStmt + ',','') + CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"', COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
SELECT @selectStmt = COALESCE(@selectStmt + ',','') + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"', COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME=@table AND COLUMN_NAME != 'last_modified'
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME, ORDINAL_POSITION

SELECT @dc = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')+CHAR(13) + CHAR(10)+
	'WHERE ' + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'".','"last_modified">= {ml s.last_table_download}')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''download_cursor'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''

--UNION ALL
PRINT 'SELECT ' + @selectStmt
--UNION ALL
PRINT 'FROM ' +@dc
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'

/* Download Cursor -END */

/* Download Delete Cursor */
DECLARE @ddc NVARCHAR(MAX)
DECLARE @pkCols NVARCHAR(MAX)
SET @pkCols = NULL
--SELECT @pkCols =COALESCE(@pkCols + ',','') + CONCAT('"',A.TABLE_CATALOG,'".','"', A.TABLE_SCHEMA,'".','"', A.TABLE_NAME,'_del".','"', B.COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
SELECT @pkCols =COALESCE(@pkCols + ',','') + CONCAT('"', A.TABLE_SCHEMA,'".','"', A.TABLE_NAME,'_del".','"', B.COLUMN_NAME ,'"',CHAR(13) + CHAR(10))
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME
SELECT @ddc = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'_del"')+CHAR(13) + CHAR(10)+
	'WHERE ' + CONCAT('"', TABLE_SCHEMA,'".','"', TABLE_NAME,'_del".','"last_modified">= {ml s.last_table_download}')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

PRINT 'EXEC ml_add_lang_table_script'
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''download_delete_cursor'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT 'SELECT ' + @pkCols
--UNION ALL
PRINT 'FROM ' +@ddc
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'
/* Download Delete Cursor -END */

/* Upload Insert Cursor */
DECLARE @ui NVARCHAR(MAX)
SELECT @ui = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

DECLARE @uiCols NVARCHAR(MAX)
SELECT @uiCols =COALESCE(@uiCols + ', ','') + CONCAT('"', A.COLUMN_NAME ,'"')
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME

DECLARE @uiColML NVARCHAR(MAX)
SELECT @uiColML =COALESCE(@uiColML + ', {ml r.','') + CONCAT('"', A.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified'
ORDER BY A.TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''upload_insert'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT '/* Insert the row into the consolidated database. */'
--UNION ALL
PRINT 'INSERT INTO ' + @ui
--UNION ALL
PRINT '(' + @uiCols +')'
--UNION ALL
PRINT 'VALUES({ml r.' + @uiColML +')'
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'
/* Upload Insert Cursor -END */


/* Upload Update Cursor */
DECLARE @uu NVARCHAR(MAX)
SELECT @uu = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

DECLARE @uuCols NVARCHAR(MAX)
SELECT @uuCols =COALESCE(@uuCols + ', ','') + CONCAT('"', A.COLUMN_NAME ,'" = {ml r."', A.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.COLUMNS AS A
WHERE A.TABLE_NAME=@table AND A.COLUMN_NAME!='last_modified' AND A.COLUMN_NAME NOT IN 
(SELECT B1.COLUMN_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A1, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B1 WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A1.CONSTRAINT_NAME = B1.CONSTRAINT_NAME AND A1.TABLE_NAME=A.TABLE_NAME)
ORDER BY A.TABLE_NAME

DECLARE @uupkCols NVARCHAR(MAX)
SET @uupkCols = NULL
SELECT @uupkCols =COALESCE(@uupkCols + ' AND ','') + CONCAT('"', B.COLUMN_NAME ,'" = {ml r."', B.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'	
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''upload_update'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT '/* Update the row in the consolidated database. */'
--UNION ALL
PRINT 'UPDATE ' + @uu
--UNION ALL
PRINT 'SET ' + @uuCols 
--UNION ALL
PRINT 'WHERE ' +@uupkCols
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'
/* Upload Update Cursor - END */

/* Upload Delete Cursor */
DECLARE @ud NVARCHAR(MAX)
SELECT @ud = CONCAT('"',TABLE_CATALOG,'".','"', TABLE_SCHEMA,'".','"', TABLE_NAME,'"')
FROM INFORMATION_SCHEMA.TABLES 
WHERE TABLE_NAME=@table
ORDER BY TABLE_CATALOG, TABLE_SCHEMA, TABLE_NAME

DECLARE @udCols NVARCHAR(MAX)
SET @udCols = NULL
SELECT @udCols =COALESCE(@udCols + ' AND ','') + CONCAT('"', B.COLUMN_NAME ,'" = {ml r."', B.COLUMN_NAME ,'"}')
FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS A, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE B
WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND A.CONSTRAINT_NAME = B.CONSTRAINT_NAME
AND A.TABLE_NAME=@table
ORDER BY A.TABLE_NAME

--PRINT 'declare @cksm varchar(100);'+CHAR(13) + CHAR(10)+ 'SELECT @cksm= REPLACE(NEWID(),''-'','''');'+CHAR(13) + CHAR(10)+'EXEC ml_add_lang_table_script'
PRINT 'EXEC ml_add_lang_table_script'	
	+CHAR(13) + CHAR(10)+''''+ @version +''','
	+CHAR(13) + CHAR(10)+''''+ @table +''','
	+CHAR(13) + CHAR(10)+'''upload_delete'','
	+CHAR(13) + CHAR(10)+'''sql'','
	+CHAR(13) + CHAR(10)+''''
--UNION ALL
PRINT '/* Delete the row in the consolidated database. */'
--UNION ALL
PRINT 'DELETE FROM ' + @ud
--UNION ALL
PRINT 'WHERE ' +@udCols
--UNION ALL
PRINT ''''
--PRINT ''', @cksm '
PRINT 'GO'

/* Upload Delete Cursor - END */
Print '/* ' + @version + ' ' + @table + ' - ends */'
Print ''
Print ''

SET NOCOUNT OFF
END



GO


