

CREATE PROCEDURE [dbo].[uspCleanupReservation_CDB]
(
	@ExpMins INT=30
)
AS
BEGIN	
	
	UPDATE BUSDTA.Route_Master SET RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='UNREGISTERED')
		FROM BUSDTA.Route_Device_Map AS rdm
		INNER JOIN BUSDTA.Route_Master AS rm ON rm.RouteName=rdm.Route_Id
	WHERE Active=0 AND rdm.last_modified<DATEADD(MINUTE, @ExpMins, GETDATE()) AND
		RouteStatusID =(SELECT RouteStatusID FROM BUSDTA.RouteStatus WHERE RouteStatus='PENDING REGISTRATION')

	--Remove Entry from Table Route_Device_Map
	DELETE FROM BUSDTA.Route_Device_Map WHERE Active=0 AND last_modified<DATEADD(MINUTE, @ExpMins, GETDATE())
END



GO


