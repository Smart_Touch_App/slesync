CREATE OR REPLACE PROCEDURE "BUSDTA"."prepareHistoryRecords"(@shipTo varchar(25))
BEGIN
declare i nvarchar(2);
declare j nvarchar(2);
declare countHist integer;
declare strQuery nvarchar(20000);
drop table if exists busdta.localLastOrders; 
select NUMBER(*) as chronology,SHKCOO,SHDOCO,SHDCTO,SHTRDJ into busdta.localLastOrders from BUSDTA.F42019 
 where 
SHSHAN=@shipTo and 
SHDCTO='SO'  ORDER BY SHTRDJ DESC;
/* Order Item Last */
drop table if exists busdta.localLastItems;
select NUMBER(*)as ranking,SDLITM into busdta.localLastItems from busdta.localLastOrders join busdta.F42119 on SHKCOO=SDKCOO and SHDCTO=SDDCTO and SHDOCO=SDDOCO
 where chronology<=10 and SDLNTY='S ' GROUP BY SDLITM Order by Count(*)Desc,SDLITM;
/* Order History Panel Data */
drop table if exists busdta.localOrderHistoryPanel;
select count(*) into countHist from busdta.localLastOrders;
	IF countHist<> null or countHist<>'' THEN
    // To limit the procedure to generate only 10 history records. The value will of countHist will also affect the size of 'strQuery'
    IF countHist>10  THEN
        set countHist = 10;
    END IF;
		set strQuery = 'select im.imlitm,im.imdsc1, ';
		set i=1;
		WHILE i<=countHist LOOP
			set strQuery = strQuery+'h'+i+'d.sdsoqs/10000 as H'+i+'_Qty';
		   IF i <> countHist THEN
				set strQuery = strQuery+' , ';
			END IF;
			set i = i+1;
		END LOOP;
	
		set strQuery = strQuery+' into busdta.localOrderHistoryPanel from busdta.localLastItems li ';
		set strQuery = strQuery+'join busdta.F4101 im on li.SDLITM=im.IMLITM  ';
		set j=1;
		WHILE j<=countHist LOOP
			set strQuery = strQuery+' join busdta.localLastOrders h'+j+' on h'+j+'.chronology='+j ;
			set strQuery = strQuery+' left join busdta.F42119 h'+j+'d on h'+j+'.SHKCOO=h'+j+'d.SDKCOO and h'+j;
			set strQuery = strQuery+' .SHDCTO=h'+j+'d.SDDCTO and h'+j+'.SHDOCO=h'+j+'d.SDDOCO and im.imlitm=h'+j+'d.sdlitm ';
		set j = j+1;
		END LOOP;
		set strQuery = convert(varchar, strQuery||' Order By ranking;');
	ELSE 
		-- set strQuery = 'select 0 AS ItemCode,0 as AverageQty into busdta.localOrderHistoryPanel from dummy;';
	END IF;
	Execute Immediate strQuery;
END
GO